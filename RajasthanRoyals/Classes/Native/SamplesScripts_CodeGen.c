﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void CloudContentManager::Start()
extern void CloudContentManager_Start_m1A249B4E7155B590DB64A7FB424713C51249F217 ();
// 0x00000002 System.Void CloudContentManager::ShowTargetInfo(System.Boolean)
extern void CloudContentManager_ShowTargetInfo_m24458476585524ACBE1687DF945F1108951B3348 ();
// 0x00000003 System.Void CloudContentManager::HandleTargetFinderResult(Vuforia.TargetFinder_CloudRecoSearchResult)
extern void CloudContentManager_HandleTargetFinderResult_mC45AB69C45934E3838D4518815462624C77356A0 ();
// 0x00000004 UnityEngine.GameObject CloudContentManager::GetValuefromDictionary(System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>,System.String)
extern void CloudContentManager_GetValuefromDictionary_m0D3B289035CF5DB5E90A1892B220D9CAFEBC6F82 ();
// 0x00000005 System.Void CloudContentManager::.ctor()
extern void CloudContentManager__ctor_m31EDA06AF3091DE3C02FCFDF8F50B0065C5E2451 ();
// 0x00000006 System.Void CloudTrackableEventHandler::Start()
extern void CloudTrackableEventHandler_Start_mAE7FDDE4EBD79E641A2ADE85B9B07262C2D258A7 ();
// 0x00000007 System.Void CloudTrackableEventHandler::OnReset()
extern void CloudTrackableEventHandler_OnReset_mC2A6EF8F1D8B3B1B7BA40B0B15BE35ABA36203DD ();
// 0x00000008 System.Void CloudTrackableEventHandler::TargetCreated(Vuforia.TargetFinder_CloudRecoSearchResult)
extern void CloudTrackableEventHandler_TargetCreated_mE9029FEA2AD6B03088BCAD38C4DB56690FB6BF89 ();
// 0x00000009 System.Void CloudTrackableEventHandler::OnTrackingFound()
extern void CloudTrackableEventHandler_OnTrackingFound_m77B08EA135C4D67F42B8B383F33F014A40FF1222 ();
// 0x0000000A System.Void CloudTrackableEventHandler::OnTrackingLost()
extern void CloudTrackableEventHandler_OnTrackingLost_m9804CCF4FA608AF89635AEFA831D838F6D0C3FDA ();
// 0x0000000B System.Void CloudTrackableEventHandler::.ctor()
extern void CloudTrackableEventHandler__ctor_mB8CC0EF8080BFA061AA5A4392CDD9676AB5E05E2 ();
// 0x0000000C System.Void RotateAroundCylinder::Start()
extern void RotateAroundCylinder_Start_m59FE262623E0FB429D18C640F8A750C71628FA0B ();
// 0x0000000D System.Void RotateAroundCylinder::Update()
extern void RotateAroundCylinder_Update_m2FF508C025877EADE94E95DD961E88A87DD3FF5B ();
// 0x0000000E System.Void RotateAroundCylinder::.ctor()
extern void RotateAroundCylinder__ctor_m1BCF6DAB9AE2A50625D73E91682D52BFDF4B5ED8 ();
// 0x0000000F System.Void GroundPlaneUI::Start()
extern void GroundPlaneUI_Start_mF92F8660AA30F5429ACF6DB36FA2DE60C53D46DE ();
// 0x00000010 System.Void GroundPlaneUI::Update()
extern void GroundPlaneUI_Update_m7292300C5A57D23199D77F206D2EB3C22E735DBF ();
// 0x00000011 System.Void GroundPlaneUI::LateUpdate()
extern void GroundPlaneUI_LateUpdate_m864ED171C41985D10CC273F5A89954915E488D36 ();
// 0x00000012 System.Void GroundPlaneUI::OnDestroy()
extern void GroundPlaneUI_OnDestroy_m3288F381C3DA63369E1254A06AA5FF3291748014 ();
// 0x00000013 System.Void GroundPlaneUI::Reset()
extern void GroundPlaneUI_Reset_m6AED6EDE5F8BF1F679C16F46EA2F5F86D91EC433 ();
// 0x00000014 System.Void GroundPlaneUI::UpdateTitle()
extern void GroundPlaneUI_UpdateTitle_m0EF0D27321E7FFAE2E824D5D3C7A63B97E5C7A60 ();
// 0x00000015 System.Boolean GroundPlaneUI::IsCanvasButtonPressed()
extern void GroundPlaneUI_IsCanvasButtonPressed_m407BE2630C9287F930DFB28E1C4A4BA159037AF2 ();
// 0x00000016 System.Void GroundPlaneUI::OnDevicePoseStatusChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_StatusInfo)
extern void GroundPlaneUI_OnDevicePoseStatusChanged_m77524799E8F314CE9F7996462D2FB2B11C125798 ();
// 0x00000017 System.Void GroundPlaneUI::.ctor()
extern void GroundPlaneUI__ctor_m9C2DE966C4004B06B5941488AB9FD663C7169723 ();
// 0x00000018 System.Boolean PlaneManager::get_GroundPlaneHitReceived()
extern void PlaneManager_get_GroundPlaneHitReceived_mF9CBA5A7B9096A9E783C33C536B68C21FDECD66D ();
// 0x00000019 System.Void PlaneManager::set_GroundPlaneHitReceived(System.Boolean)
extern void PlaneManager_set_GroundPlaneHitReceived_m7F09ECE895AFE70BF0BAB0371ADECD576CD3D0B6 ();
// 0x0000001A System.Boolean PlaneManager::get_TrackingStatusIsTrackedAndNormal()
extern void PlaneManager_get_TrackingStatusIsTrackedAndNormal_m98047D808F55564042F3DCE2DB1D199C1F44EC13 ();
// 0x0000001B System.Boolean PlaneManager::get_TrackingStatusIsTrackedOrLimited()
extern void PlaneManager_get_TrackingStatusIsTrackedOrLimited_mC0D4B020CAD365386942FD13E0944FA8EE50E40A ();
// 0x0000001C System.Boolean PlaneManager::get_SurfaceIndicatorVisibilityConditionsMet()
extern void PlaneManager_get_SurfaceIndicatorVisibilityConditionsMet_m9ADC55208694DFD7C62BD3C55F7F71BBFCEF05A9 ();
// 0x0000001D System.Void PlaneManager::Start()
extern void PlaneManager_Start_mFAC04BE252F51631049537E1E9813683068D57F5 ();
// 0x0000001E System.Void PlaneManager::Update()
extern void PlaneManager_Update_m325751E24CF7E935D3DA8A1A65650537E862F60F ();
// 0x0000001F System.Void PlaneManager::LateUpdate()
extern void PlaneManager_LateUpdate_m376838C73FC649748283F8F129BD31E999D0D204 ();
// 0x00000020 System.Void PlaneManager::OnDestroy()
extern void PlaneManager_OnDestroy_m7F625C36FEC6ED0860BEEB6EB5E23FD45CD2C009 ();
// 0x00000021 System.Void PlaneManager::HandleAutomaticHitTest(Vuforia.HitTestResult)
extern void PlaneManager_HandleAutomaticHitTest_m0C0A26ED9A5F91E931D86921AFB7FF981B17B10A ();
// 0x00000022 System.Void PlaneManager::HandleInteractiveHitTest(Vuforia.HitTestResult)
extern void PlaneManager_HandleInteractiveHitTest_mE13B1314C7DFC14ABCDB8EDC072CBF2097F00162 ();
// 0x00000023 System.Void PlaneManager::PlaceObjectInMidAir(UnityEngine.Transform)
extern void PlaneManager_PlaceObjectInMidAir_m0D58B308FD5F951AE118C0ECBBFDA661B4738586 ();
// 0x00000024 System.Void PlaneManager::SetGroundMode(System.Boolean)
extern void PlaneManager_SetGroundMode_mB838C438A2859513331EE6E0F29F477ADA921359 ();
// 0x00000025 System.Void PlaneManager::SetMidAirMode(System.Boolean)
extern void PlaneManager_SetMidAirMode_mEACF5A11514483BD9BBF42D04BE64C8C910590F5 ();
// 0x00000026 System.Void PlaneManager::SetPlacementMode(System.Boolean)
extern void PlaneManager_SetPlacementMode_mA6852C193F105787BE6A7F92A016D5C306F360F8 ();
// 0x00000027 System.Void PlaneManager::ResetScene()
extern void PlaneManager_ResetScene_mB522B800C0131CEE210FFC3315504A355C556C32 ();
// 0x00000028 System.Void PlaneManager::ResetTrackers()
extern void PlaneManager_ResetTrackers_mAF8119784220FD2329DC70DD1A3A26456869567F ();
// 0x00000029 System.Void PlaneManager::SetMode(PlaneManager_PlaneMode)
extern void PlaneManager_SetMode_m6508AF2D3E9128D661FBC3351811E2A21DEEF8C6 ();
// 0x0000002A System.Void PlaneManager::SetSurfaceIndicatorVisible(System.Boolean)
extern void PlaneManager_SetSurfaceIndicatorVisible_m9E804518A3C8FC0451D1ADABEEA66C738FBF33F2 ();
// 0x0000002B System.Void PlaneManager::TimerFinished(System.Object,System.Timers.ElapsedEventArgs)
extern void PlaneManager_TimerFinished_mFD6F695D7C2C98240E457FBE1C8737ADC18C7EC6 ();
// 0x0000002C System.Void PlaneManager::OnVuforiaStarted()
extern void PlaneManager_OnVuforiaStarted_m02A681F3FCD82EE9DC65829E4756D189E9075400 ();
// 0x0000002D System.Void PlaneManager::OnVuforiaPaused(System.Boolean)
extern void PlaneManager_OnVuforiaPaused_mF52BCE32A5968188757C5970E6E8EDB4693CC28D ();
// 0x0000002E System.Void PlaneManager::OnTrackerStarted()
extern void PlaneManager_OnTrackerStarted_m1A8EE6BBE2D4CDAE64690247C718F64D4EB8C1F8 ();
// 0x0000002F System.Void PlaneManager::OnDevicePoseStatusChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_StatusInfo)
extern void PlaneManager_OnDevicePoseStatusChanged_m307BC6AE4D49B512F3A1B0BBAF41E0B1F64E8D77 ();
// 0x00000030 System.Void PlaneManager::.ctor()
extern void PlaneManager__ctor_m1843562DC58F54BB644D7F8E31B529589374E375 ();
// 0x00000031 System.Void PlaneManager::.cctor()
extern void PlaneManager__cctor_m54D96E384F7D3906C9AC03943435F9576C90A1EE ();
// 0x00000032 System.Boolean ProductPlacement::get_IsPlaced()
extern void ProductPlacement_get_IsPlaced_mA590A83EB95A1452535693C82AA7FEB0AC1A00AA ();
// 0x00000033 System.Void ProductPlacement::set_IsPlaced(System.Boolean)
extern void ProductPlacement_set_IsPlaced_m0841EE5E7EFDCD43F4895A8B9E29B9D1599EEDB6 ();
// 0x00000034 System.Boolean ProductPlacement::get_ChairVisibilityConditionsMet()
extern void ProductPlacement_get_ChairVisibilityConditionsMet_m5028A14C25D5C60C378512DBF037B61072A8F392 ();
// 0x00000035 System.Void ProductPlacement::Start()
extern void ProductPlacement_Start_mCF059202B3B8D682133D21F37B02D3587E9C4309 ();
// 0x00000036 System.Void ProductPlacement::Update()
extern void ProductPlacement_Update_m94C34785F198D1F08601830EF52D41D6BA4AB60B ();
// 0x00000037 System.Void ProductPlacement::LateUpdate()
extern void ProductPlacement_LateUpdate_m205B9429FA46CD8778B4059D17E6FE71CC46831E ();
// 0x00000038 System.Void ProductPlacement::Reset()
extern void ProductPlacement_Reset_m19E566EC917318D804A8476128A7485F6DBE08E6 ();
// 0x00000039 System.Void ProductPlacement::SetProductAnchor(UnityEngine.Transform)
extern void ProductPlacement_SetProductAnchor_mB58975E1D5A738B7BFBAC0DB3B113CB9948B497E ();
// 0x0000003A System.Void ProductPlacement::SetupMaterials()
extern void ProductPlacement_SetupMaterials_m5D3D8B4AC0F88981932649FC29AAD6356C85FD6D ();
// 0x0000003B System.Void ProductPlacement::SetupFloor()
extern void ProductPlacement_SetupFloor_m75D9F56A1A6D628B5C9A8B3D2CF78615723E1062 ();
// 0x0000003C System.Void ProductPlacement::SetVisible(System.Boolean)
extern void ProductPlacement_SetVisible_m32EB83E0BFEF45FA5780910262379FB54253554A ();
// 0x0000003D System.Void ProductPlacement::EnablePreviewModeTransparency(System.Boolean)
extern void ProductPlacement_EnablePreviewModeTransparency_m963D6C90A5D96707021509BE0D9B7279FF5558DD ();
// 0x0000003E System.Void ProductPlacement::.ctor()
extern void ProductPlacement__ctor_m2CDE14D5D8EBCE2D0A949D1DA6D813C379FE549A ();
// 0x0000003F System.Boolean TouchHandler::get_DoubleTap()
extern void TouchHandler_get_DoubleTap_m30385B6D53C063BB0DB7728BB25F648716449CBB ();
// 0x00000040 System.Boolean TouchHandler::get_IsSingleFingerStationary()
extern void TouchHandler_get_IsSingleFingerStationary_m8D5A0DFB6DEBF5B0A36FDC2B8BDAB3ED51E2A47B ();
// 0x00000041 System.Boolean TouchHandler::get_IsSingleFingerDragging()
extern void TouchHandler_get_IsSingleFingerDragging_m850D210A0CE3F84ECE02F43A5C303882A9E9A60E ();
// 0x00000042 System.Void TouchHandler::Start()
extern void TouchHandler_Start_mD632DE0E6E43175BA12F1656DA31798436B651A0 ();
// 0x00000043 System.Void TouchHandler::Update()
extern void TouchHandler_Update_mA532DD4125F92F4F9C9FF1353852C47C320FECB5 ();
// 0x00000044 System.Boolean TouchHandler::IsSingleFingerDown()
extern void TouchHandler_IsSingleFingerDown_m76EC3A622B30F3578C9823715B41C2842BA2B2C0 ();
// 0x00000045 System.Void TouchHandler::.ctor()
extern void TouchHandler__ctor_m6C28D981C551E4F30B0905698B7EB85DB9C1AE67 ();
// 0x00000046 System.Void UtilityHelper::RotateTowardCamera(UnityEngine.GameObject)
extern void UtilityHelper_RotateTowardCamera_m967B224FAC40A06B36F3EAA160D5D439F759E1D2 ();
// 0x00000047 System.Void UtilityHelper::EnableRendererColliderCanvas(UnityEngine.GameObject,System.Boolean)
extern void UtilityHelper_EnableRendererColliderCanvas_m97CF532CB9AA52B3701A00BDF2D45E2985E6DA1A ();
// 0x00000048 System.Int32 UtilityHelper::GetNumberOfActiveAnchors()
extern void UtilityHelper_GetNumberOfActiveAnchors_m74F247C2298D141737988E112C83FB76F7C3A5FA ();
// 0x00000049 System.Void ModelSwap::Start()
extern void ModelSwap_Start_mAEF9320E9B6420CB03AFAE5907836D6787848579 ();
// 0x0000004A System.Void ModelSwap::Update()
extern void ModelSwap_Update_m10B296B593D86E787130BEBB9DA3F001F67C4687 ();
// 0x0000004B System.Void ModelSwap::.ctor()
extern void ModelSwap__ctor_m6998B7EEAC510BAD241B659554A9761797E7365E ();
// 0x0000004C System.Void VideoController::Start()
extern void VideoController_Start_m1891079C32DC78429E5CEC33D276D83BCE84C7B7 ();
// 0x0000004D System.Void VideoController::Update()
extern void VideoController_Update_m0816ED36E474C740C98A91B84EBA6680A00E6D03 ();
// 0x0000004E System.Void VideoController::OnApplicationPause(System.Boolean)
extern void VideoController_OnApplicationPause_m0EF9D3BFAF6FF1F07ACE9A5F356D9B81D7A8450D ();
// 0x0000004F System.Void VideoController::Play()
extern void VideoController_Play_m75D0C29EC0A1C28C8A9B241F1D3AEAEA31CE6353 ();
// 0x00000050 System.Void VideoController::Pause()
extern void VideoController_Pause_m88C49D9180B3424D4594ACD97CAFC9071F31AD1E ();
// 0x00000051 System.Void VideoController::PauseAudio(System.Boolean)
extern void VideoController_PauseAudio_mD010EE3CAB4625BFDB42547DDA38CBBC53196AC3 ();
// 0x00000052 System.Void VideoController::ShowPlayButton(System.Boolean)
extern void VideoController_ShowPlayButton_m2980A5174AFBC2939F5B7C8EA4607C7A34796C58 ();
// 0x00000053 System.Void VideoController::LogClipInfo()
extern void VideoController_LogClipInfo_m404BBC89F22E53915F37EF2D8EED70970ADA8932 ();
// 0x00000054 System.Void VideoController::HandleVideoError(UnityEngine.Video.VideoPlayer,System.String)
extern void VideoController_HandleVideoError_mD4A4850E89A7039BC97348DD7C6D7FF995F9A524 ();
// 0x00000055 System.Void VideoController::HandleStartedEvent(UnityEngine.Video.VideoPlayer)
extern void VideoController_HandleStartedEvent_m9B1042A9A2EC1F5D08FA963C493997C3A1B1F621 ();
// 0x00000056 System.Void VideoController::HandlePrepareCompleted(UnityEngine.Video.VideoPlayer)
extern void VideoController_HandlePrepareCompleted_m11BAE1BC62085C2758BA6275884CFB9EA1FCF482 ();
// 0x00000057 System.Void VideoController::HandleSeekCompleted(UnityEngine.Video.VideoPlayer)
extern void VideoController_HandleSeekCompleted_m821948B955C91CA26F87F33725900D26CE1AFE91 ();
// 0x00000058 System.Void VideoController::HandleLoopPointReached(UnityEngine.Video.VideoPlayer)
extern void VideoController_HandleLoopPointReached_m175A725A575E13F2B01BF2D40223CA86790F871F ();
// 0x00000059 System.Void VideoController::.ctor()
extern void VideoController__ctor_m21F2B9C67DD50F1B9E620F1AAD7CAE46BE8E6F50 ();
// 0x0000005A System.Void VideoTrackableEventHandler::OnTrackingLost()
extern void VideoTrackableEventHandler_OnTrackingLost_m5792FA2718BFD3A32012288ED706A6B9143E6A52 ();
// 0x0000005B System.Void VideoTrackableEventHandler::.ctor()
extern void VideoTrackableEventHandler__ctor_m97B8BBDF7E1CD37061B339B7295FBDAB1263E20A ();
// 0x0000005C System.Void ModelTargetsManager::Awake()
extern void ModelTargetsManager_Awake_m0E53268243EBA82EC0AD441A8B7125F0B758D3F2 ();
// 0x0000005D System.Void ModelTargetsManager::Start()
extern void ModelTargetsManager_Start_mEBAE1684831E31D3123C264F35F37F8AC6B3934F ();
// 0x0000005E System.Void ModelTargetsManager::LateUpdate()
extern void ModelTargetsManager_LateUpdate_m96CC0DB5B97DF015402C2642E89C539671E624A8 ();
// 0x0000005F System.Void ModelTargetsManager::OnDestroy()
extern void ModelTargetsManager_OnDestroy_mF476E1D727879DC0F4320965D38F39AB9F82D1B9 ();
// 0x00000060 System.Void ModelTargetsManager::OnVuforiaStarted()
extern void ModelTargetsManager_OnVuforiaStarted_mB97FFF7D19392F6A72736D8C9E94C212A06D8EEA ();
// 0x00000061 System.Void ModelTargetsManager::EnableSymbolicTargetsUI(System.Boolean)
extern void ModelTargetsManager_EnableSymbolicTargetsUI_m624071D1E67C7052CA9B255FF44E018DAF30051A ();
// 0x00000062 System.Void ModelTargetsManager::AddAdvancedModelTargetBehaviour(Vuforia.ModelTargetBehaviour)
extern void ModelTargetsManager_AddAdvancedModelTargetBehaviour_m2AE3D6EEA19B9342A19EE2541EE2D1DD26F63CC0 ();
// 0x00000063 System.Void ModelTargetsManager::SelectDataSetStandard(System.Boolean)
extern void ModelTargetsManager_SelectDataSetStandard_m280A7827F6174F6458854D4945815D1DCF27E845 ();
// 0x00000064 System.Void ModelTargetsManager::SelectDataSetAdvanced(System.Boolean)
extern void ModelTargetsManager_SelectDataSetAdvanced_mA7ED00ED6F4595708DADDC5EDE2870967FDACCB6 ();
// 0x00000065 System.Void ModelTargetsManager::ShowDataSetMenu(System.Boolean)
extern void ModelTargetsManager_ShowDataSetMenu_m8637ED87034974E131887D4F320AD26D5C123323 ();
// 0x00000066 System.Void ModelTargetsManager::CycleGuideView()
extern void ModelTargetsManager_CycleGuideView_m6492DAD6662BAF00758F5FBE468F8433CDD790AC ();
// 0x00000067 System.Void ModelTargetsManager::LoadDataSet(System.String)
extern void ModelTargetsManager_LoadDataSet_m8114D9F82ECBA7CDF725ABA7FBC9DF2F4D3DF756 ();
// 0x00000068 System.Void ModelTargetsManager::DeactivateActiveDataSets(System.Boolean)
extern void ModelTargetsManager_DeactivateActiveDataSets_m3F74FB6638A29D6DC0C46587A75F6BAB75CF908D ();
// 0x00000069 System.Void ModelTargetsManager::ActivateDataSet(System.String)
extern void ModelTargetsManager_ActivateDataSet_mD14B9ED2B11718B2A0E4062F8CAE1CC4A8D07961 ();
// 0x0000006A System.Boolean ModelTargetsManager::ActiveTrackablesExist()
extern void ModelTargetsManager_ActiveTrackablesExist_m73DBF610FE53BDA78C3272F34FE669C59B45E13A ();
// 0x0000006B System.Void ModelTargetsManager::LogActiveDataSets()
extern void ModelTargetsManager_LogActiveDataSets_m8EEEDD6413117C964B16EFBBD0BA7918834410D9 ();
// 0x0000006C System.Void ModelTargetsManager::LogAllDataSets()
extern void ModelTargetsManager_LogAllDataSets_m165A002EBB5BD31A2817ECD5AD097B71FAA200A3 ();
// 0x0000006D System.Void ModelTargetsManager::.ctor()
extern void ModelTargetsManager__ctor_m53DDB8B0C71926E33443080CC34E37AAAE5FC9A6 ();
// 0x0000006E System.Void ModelTargetsUIManager::Start()
extern void ModelTargetsUIManager_Start_m3E8DF0E86FCB2CB3E2DE31EEA648D3227FCFFFC3 ();
// 0x0000006F System.Void ModelTargetsUIManager::Update()
extern void ModelTargetsUIManager_Update_m17B13569B65A4734765BB74F67714E42EF4CF1C6 ();
// 0x00000070 System.Void ModelTargetsUIManager::SetUI(ModelTargetsManager_ModelTargetMode,System.Boolean)
extern void ModelTargetsUIManager_SetUI_m02D1CA3398AABB004836E6B36D33C89A6EEE377E ();
// 0x00000071 System.Void ModelTargetsUIManager::InitSymbolicTargetIcons()
extern void ModelTargetsUIManager_InitSymbolicTargetIcons_m72E76663F9F0B34CAB47437032D1E45A9C038FFB ();
// 0x00000072 System.Void ModelTargetsUIManager::UpdateSymbolicTargetIconFadeCycle()
extern void ModelTargetsUIManager_UpdateSymbolicTargetIconFadeCycle_m313BD8E3376EEDE68074AB532F7FE98DBE19DD67 ();
// 0x00000073 System.Void ModelTargetsUIManager::ResetImageSequenceValues()
extern void ModelTargetsUIManager_ResetImageSequenceValues_m73BF730534D7C8ECDFE63EB394C13726D9AA8507 ();
// 0x00000074 System.Collections.IEnumerator ModelTargetsUIManager::ClearImageSequencePause()
extern void ModelTargetsUIManager_ClearImageSequencePause_m2008ADD108934EE68BD061B90B5923ABC3E1279E ();
// 0x00000075 System.Void ModelTargetsUIManager::.ctor()
extern void ModelTargetsUIManager__ctor_mF09D13875C7D4CDB3EAB6E43DB86CD9CA45F4329 ();
// 0x00000076 System.Boolean ModelTargetsUIManager::<ClearImageSequencePause>b__18_0()
extern void ModelTargetsUIManager_U3CClearImageSequencePauseU3Eb__18_0_m61212E2ED7456C08EC5850072D916CBCB183D64B ();
// 0x00000077 System.Void MT360TrackableEventHandler::Start()
extern void MT360TrackableEventHandler_Start_m42C0B6EAB3A4F8CDA8707E41DB7FD9DF2566A192 ();
// 0x00000078 System.Void MT360TrackableEventHandler::OnTrackingFound()
extern void MT360TrackableEventHandler_OnTrackingFound_m8976F0122C642D44056A9EDF28F0C0E4DD2D9455 ();
// 0x00000079 System.Void MT360TrackableEventHandler::OnTrackingLost()
extern void MT360TrackableEventHandler_OnTrackingLost_m4BF8BF2E73FA2B1D11E4724B27CDAA74E5C296AE ();
// 0x0000007A System.Void MT360TrackableEventHandler::.ctor()
extern void MT360TrackableEventHandler__ctor_m27545E8EBEF44B305ADDAC5D0DAC4A26F4AA59B4 ();
// 0x0000007B System.Void MTAdvancedTrackableEventHandler::Start()
extern void MTAdvancedTrackableEventHandler_Start_m9C01F8AF2E77453EA7A378569A99FEED3EC85154 ();
// 0x0000007C System.Void MTAdvancedTrackableEventHandler::Update()
extern void MTAdvancedTrackableEventHandler_Update_mA1E49F1571DB89AC1E19332DBB7249E66824304B ();
// 0x0000007D System.Void MTAdvancedTrackableEventHandler::OnTrackingFound()
extern void MTAdvancedTrackableEventHandler_OnTrackingFound_m2C70F8603DBAFD256ECF9707B297FC2853E459E1 ();
// 0x0000007E System.Void MTAdvancedTrackableEventHandler::.ctor()
extern void MTAdvancedTrackableEventHandler__ctor_m11682E63231AB14D5EA962CED2354C5659E7B90B ();
// 0x0000007F System.Void CameraDepthEnabler::Start()
extern void CameraDepthEnabler_Start_m822BD8E6B7533937F639A07EAC870F310D41806D ();
// 0x00000080 System.Void CameraDepthEnabler::.ctor()
extern void CameraDepthEnabler__ctor_mB20B9C00F4ACB9E618C9C19287254DCEFA292E79 ();
// 0x00000081 System.Void MaterialSwitch::SwitchMaterial()
extern void MaterialSwitch_SwitchMaterial_m8A6C3B6C215BDCB53C9ED7566CEAE29865570EC7 ();
// 0x00000082 System.Collections.IEnumerator MaterialSwitch::UpdateScanTime(UnityEngine.Material,System.Single)
extern void MaterialSwitch_UpdateScanTime_m2A8787D0A94B7A9085DCC6D6463F1515363F81D1 ();
// 0x00000083 System.Void MaterialSwitch::.ctor()
extern void MaterialSwitch__ctor_m9ECBC19323711F86DC63255DFCDDA0739C361BA9 ();
// 0x00000084 System.Void ModelTargetAugmentationHandler::Start()
extern void ModelTargetAugmentationHandler_Start_m96DB46F32E2EF80286D2C3C8C9498FD306DE4F33 ();
// 0x00000085 System.Void ModelTargetAugmentationHandler::OnDestroy()
extern void ModelTargetAugmentationHandler_OnDestroy_mC0635CBF238A18E251EE9FE30E460445AA6AE143 ();
// 0x00000086 System.Void ModelTargetAugmentationHandler::OnTrackableStatusChanged(Vuforia.TrackableBehaviour_StatusChangeResult)
extern void ModelTargetAugmentationHandler_OnTrackableStatusChanged_mF494BC756CB124092F8975A705400287CD690C9D ();
// 0x00000087 System.Void ModelTargetAugmentationHandler::SwitchMaterial()
extern void ModelTargetAugmentationHandler_SwitchMaterial_mDD6448077EFDC1BB81F7A554ADABE79BC910EDCF ();
// 0x00000088 System.Void ModelTargetAugmentationHandler::PlayParticleSystems()
extern void ModelTargetAugmentationHandler_PlayParticleSystems_mB86077AC3A3324E57EED76511C2936FAE45F95D1 ();
// 0x00000089 System.Void ModelTargetAugmentationHandler::StopParticleSystems()
extern void ModelTargetAugmentationHandler_StopParticleSystems_m28E35603D7606D4CEB8160B74AD2978201AFA8F5 ();
// 0x0000008A System.Void ModelTargetAugmentationHandler::PlayAnimations()
extern void ModelTargetAugmentationHandler_PlayAnimations_m88DD5A3645181BF2A5DDB41369F1E2F2CD44FF75 ();
// 0x0000008B System.Void ModelTargetAugmentationHandler::RewindAnimations()
extern void ModelTargetAugmentationHandler_RewindAnimations_m6E48D7F5A1E92B8971521E4685E17941083B5097 ();
// 0x0000008C System.Void ModelTargetAugmentationHandler::SaveMaterials(UnityEngine.GameObject)
extern void ModelTargetAugmentationHandler_SaveMaterials_m0C720E743879626AE44F783DD8A7B9510DBEDB9E ();
// 0x0000008D System.Void ModelTargetAugmentationHandler::RestoreMaterials(UnityEngine.GameObject)
extern void ModelTargetAugmentationHandler_RestoreMaterials_mDFB07D7EE3B9A420892E5210E5D6213CB7CC47AE ();
// 0x0000008E System.Void ModelTargetAugmentationHandler::SetMaterial(UnityEngine.GameObject,UnityEngine.Material)
extern void ModelTargetAugmentationHandler_SetMaterial_m6940EBAB17CCE874FB336F4535D4AF20F3DADB42 ();
// 0x0000008F System.Void ModelTargetAugmentationHandler::.ctor()
extern void ModelTargetAugmentationHandler__ctor_mFC2FFC3BE1C84A9F7B1C0DD74B9736C51A92BECE ();
// 0x00000090 System.Void SimpleAnimation::add_OnStartEvent(SimpleAnimation_OnAnimationStarted)
extern void SimpleAnimation_add_OnStartEvent_m23B9E031FF8A744E575A9F5A4BC13CAF08C48F80 ();
// 0x00000091 System.Void SimpleAnimation::remove_OnStartEvent(SimpleAnimation_OnAnimationStarted)
extern void SimpleAnimation_remove_OnStartEvent_m28DE82BAF4C62732D425B60F5F9F28D3252AFA78 ();
// 0x00000092 System.Void SimpleAnimation::add_OnCompleteEvent(SimpleAnimation_OnAnimationCompleted)
extern void SimpleAnimation_add_OnCompleteEvent_m57FBA0F8EE09D4C5A02A83E0A52B4A54B0AEACB6 ();
// 0x00000093 System.Void SimpleAnimation::remove_OnCompleteEvent(SimpleAnimation_OnAnimationCompleted)
extern void SimpleAnimation_remove_OnCompleteEvent_m12040E4218027E7B6A44134D9A6D716365668F6A ();
// 0x00000094 System.Void SimpleAnimation::Awake()
extern void SimpleAnimation_Awake_m621C73D72D3124F1368FDBE18ED6BB682C4DD8FB ();
// 0x00000095 System.Void SimpleAnimation::Update()
extern void SimpleAnimation_Update_m2726894C30584D475B67B666081CA089BCA5BC0D ();
// 0x00000096 System.Void SimpleAnimation::Play()
extern void SimpleAnimation_Play_m2E7DD25B77F74FDE058A0CA2686D80B21EF3EC5E ();
// 0x00000097 System.Void SimpleAnimation::Rewind()
extern void SimpleAnimation_Rewind_mE7DD53FA0635E0B8699BAAE2F0B4803CA3CCD0E4 ();
// 0x00000098 System.Void SimpleAnimation::ClearEventHandlers()
extern void SimpleAnimation_ClearEventHandlers_m98F2A8B111941ACDC63D6FCF8468DB87D5A71A9D ();
// 0x00000099 System.Void SimpleAnimation::.ctor()
extern void SimpleAnimation__ctor_m0C73DDCB3B0E92C83D516DF2E4B9A1AC899C830C ();
// 0x0000009A System.Void MultiTargetTrackableEventHandler::OnTrackingFound()
extern void MultiTargetTrackableEventHandler_OnTrackingFound_m08D292F9EFD2DA71240E873019B9620A8588E143 ();
// 0x0000009B System.Void MultiTargetTrackableEventHandler::OnTrackingLost()
extern void MultiTargetTrackableEventHandler_OnTrackingLost_mCD7CA3102D3A0DC123E3C3C3B2A1DEA05DC4AA4A ();
// 0x0000009C System.Void MultiTargetTrackableEventHandler::.ctor()
extern void MultiTargetTrackableEventHandler__ctor_m92F6CA8B472E17DB646D901460A294F3A5390E9E ();
// 0x0000009D System.Void ObjectRecoTrackableEventHandler::OnTrackingFound()
extern void ObjectRecoTrackableEventHandler_OnTrackingFound_mC8CA12662793CD967244A0F6D7861B8731E33AAB ();
// 0x0000009E System.Void ObjectRecoTrackableEventHandler::OnTrackingLost()
extern void ObjectRecoTrackableEventHandler_OnTrackingLost_mA0D0A9397BDC6D2294374DED48E341DE8F34BFF4 ();
// 0x0000009F System.Void ObjectRecoTrackableEventHandler::.ctor()
extern void ObjectRecoTrackableEventHandler__ctor_m623F3A1281245513A42CF005864B0EDDB6F9EED5 ();
// 0x000000A0 System.Void FrameQualityMeter::SetMeter(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color)
extern void FrameQualityMeter_SetMeter_mB10FABEC92E6C050534B75EF8B07CF4200D0211D ();
// 0x000000A1 System.Void FrameQualityMeter::SetQuality(Vuforia.ImageTargetBuilder_FrameQuality)
extern void FrameQualityMeter_SetQuality_m9D69BDD905F0C0168BEE6C9C72B8499F5C78FB53 ();
// 0x000000A2 System.Void FrameQualityMeter::.ctor()
extern void FrameQualityMeter__ctor_mB84A4DD55E895658C2E85F8B7009BC9723EE11CF ();
// 0x000000A3 System.Int32 UDTEventHandler::get_LastTargetIndex()
extern void UDTEventHandler_get_LastTargetIndex_m78FFC8F5665ADECA6B627369E81E2E9CE017F0FD ();
// 0x000000A4 System.Void UDTEventHandler::Start()
extern void UDTEventHandler_Start_mBA3A3CE7BC4A6E5DF4ADA38FB04431B0B175EB33 ();
// 0x000000A5 System.Void UDTEventHandler::OnDestroy()
extern void UDTEventHandler_OnDestroy_m1C094595A93980F1D67A2F66A8CE1B1425811B09 ();
// 0x000000A6 System.Void UDTEventHandler::OnInitialized()
extern void UDTEventHandler_OnInitialized_mFF58AFEEC8EE9B9DA087EBC72F5F62A3DCAD7281 ();
// 0x000000A7 System.Void UDTEventHandler::OnFrameQualityChanged(Vuforia.ImageTargetBuilder_FrameQuality)
extern void UDTEventHandler_OnFrameQualityChanged_mD1F599FD85A0CC3BBA8130B4AC7C1787897B8B33 ();
// 0x000000A8 System.Void UDTEventHandler::OnNewTrackableSource(Vuforia.TrackableSource)
extern void UDTEventHandler_OnNewTrackableSource_mAF1B110BB8555C01BB555E69EADA4F5A3554E90D ();
// 0x000000A9 System.Void UDTEventHandler::BuildNewTarget()
extern void UDTEventHandler_BuildNewTarget_mB24A7CAE15969CFCC90410D9D2DD20EBEDAF34B5 ();
// 0x000000AA System.Void UDTEventHandler::.ctor()
extern void UDTEventHandler__ctor_mDA437972B6FA7AD9E4F9C882D4AE5587DD96128A ();
// 0x000000AB System.Void VirtualButtonEventHandler::Awake()
extern void VirtualButtonEventHandler_Awake_m875247FD2C484B698BC2DBCFC83896F3FC8E29C8 ();
// 0x000000AC System.Void VirtualButtonEventHandler::Destroy()
extern void VirtualButtonEventHandler_Destroy_m77F0306779C65FB04710C19F45132C8B156B0F83 ();
// 0x000000AD System.Void VirtualButtonEventHandler::OnButtonPressed(Vuforia.VirtualButtonBehaviour)
extern void VirtualButtonEventHandler_OnButtonPressed_m6628E092DDD9B8500943C635414C795D79E23E15 ();
// 0x000000AE System.Void VirtualButtonEventHandler::OnButtonReleased(Vuforia.VirtualButtonBehaviour)
extern void VirtualButtonEventHandler_OnButtonReleased_m5A19FD662DA0FAFE99133F124546840338DE71CF ();
// 0x000000AF System.Void VirtualButtonEventHandler::SetVirtualButtonMaterial(UnityEngine.Material)
extern void VirtualButtonEventHandler_SetVirtualButtonMaterial_m62D6D1869B345AF413CE30441257A14154699099 ();
// 0x000000B0 System.Collections.IEnumerator VirtualButtonEventHandler::DelayOnButtonReleasedEvent(System.Single,System.String)
extern void VirtualButtonEventHandler_DelayOnButtonReleasedEvent_mA5843009378A59EDF60BD6B441A419D4DC83ADA2 ();
// 0x000000B1 System.Void VirtualButtonEventHandler::.ctor()
extern void VirtualButtonEventHandler__ctor_mBE6C3E8B422E3294883DA251D68AEFC8A074831A ();
// 0x000000B2 System.Void LookAtObject::Update()
extern void LookAtObject_Update_m5677D4871BB9D1F98868995885276228B1127475 ();
// 0x000000B3 System.Void LookAtObject::.ctor()
extern void LookAtObject__ctor_mA335E6987B020E736FD62B5046B408D6723F6A4B ();
// 0x000000B4 System.Void PanelShowHide::Hide()
extern void PanelShowHide_Hide_m2CF36F60AF2E33B48C256F3EADB4392943B7893E ();
// 0x000000B5 System.Void PanelShowHide::Show(System.String,System.String,System.String,UnityEngine.Sprite)
extern void PanelShowHide_Show_m7F97A9C59FD86D0FDBBBB8377400CE32725CC844 ();
// 0x000000B6 System.Void PanelShowHide::ResetShowTrigger()
extern void PanelShowHide_ResetShowTrigger_m9F50C06354E467C7C4BBC9520EF7F2920DC407A3 ();
// 0x000000B7 System.Void PanelShowHide::.ctor()
extern void PanelShowHide__ctor_m44AA13FA3ADE9435634F72691A9E3B954CBFA05D ();
// 0x000000B8 System.Void VuMarkHandler::Awake()
extern void VuMarkHandler_Awake_mBF60FA215B9502E6256854DCB7D1152ECF03E224 ();
// 0x000000B9 System.Void VuMarkHandler::Start()
extern void VuMarkHandler_Start_mC4E3689BD02198A08C6A65172538F96A13A91B37 ();
// 0x000000BA System.Void VuMarkHandler::Update()
extern void VuMarkHandler_Update_mEA1A7C06F671487EB3E8516EC91C0DE69F106FB9 ();
// 0x000000BB System.Void VuMarkHandler::OnDestroy()
extern void VuMarkHandler_OnDestroy_m06A84B1048566E6E6B01133511FE04FEC65E2B11 ();
// 0x000000BC System.Void VuMarkHandler::OnVuforiaStarted()
extern void VuMarkHandler_OnVuforiaStarted_mCF41E81FD8784E99BF71EBFCAAAD4D78164BBD38 ();
// 0x000000BD System.Void VuMarkHandler::OnVuMarkBehaviourDetected(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_OnVuMarkBehaviourDetected_m00EF6DE1862BD54833F360F2FF4238E63160B7E9 ();
// 0x000000BE System.Collections.IEnumerator VuMarkHandler::OnVuMarkTargetAvailable(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_OnVuMarkTargetAvailable_m52F553B014E26EA6849F94104D5CDF9090544951 ();
// 0x000000BF System.Void VuMarkHandler::OnVuMarkDetected(Vuforia.VuMarkTarget)
extern void VuMarkHandler_OnVuMarkDetected_mD9D8346D52332BFDA10A661BCEA652A5A160AB41 ();
// 0x000000C0 System.Void VuMarkHandler::OnVuMarkLost(Vuforia.VuMarkTarget)
extern void VuMarkHandler_OnVuMarkLost_m68FD0A642A747FB62D2A202051EED925FDE1E2E3 ();
// 0x000000C1 System.String VuMarkHandler::GetVuMarkDataType(Vuforia.VuMarkTarget)
extern void VuMarkHandler_GetVuMarkDataType_m14F5FAA7F060A755A7C4A87ABFE3D404330F536D ();
// 0x000000C2 System.String VuMarkHandler::GetVuMarkId(Vuforia.VuMarkTarget)
extern void VuMarkHandler_GetVuMarkId_mFD567BC0846485F6639BDEBC67154594AB441A94 ();
// 0x000000C3 UnityEngine.Sprite VuMarkHandler::GetVuMarkImage(Vuforia.VuMarkTarget)
extern void VuMarkHandler_GetVuMarkImage_m24BA3B60C81BDA1E8BAC1D9C1DA8E3EFE55502A6 ();
// 0x000000C4 System.String VuMarkHandler::GetNumericVuMarkDescription(Vuforia.VuMarkTarget)
extern void VuMarkHandler_GetNumericVuMarkDescription_m263BBBC1AA93705930EEA18C1D02320323FE4A1D ();
// 0x000000C5 System.Void VuMarkHandler::SetVuMarkInfoForCanvas(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_SetVuMarkInfoForCanvas_mFBBEF06C803232D04EA098B263C03BE186D5F4E6 ();
// 0x000000C6 System.Void VuMarkHandler::SetVuMarkAugmentation(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_SetVuMarkAugmentation_m73122D0FEBDB75F1305438DA776953E58137BFCA ();
// 0x000000C7 System.Void VuMarkHandler::SetVuMarkOpticalSeeThroughConfig(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_SetVuMarkOpticalSeeThroughConfig_m53944D862EF8C9166774F2C4A9E4F36265B827E3 ();
// 0x000000C8 UnityEngine.Texture2D VuMarkHandler::RetrieveStoredTextureForVuMarkTarget(Vuforia.VuMarkTarget)
extern void VuMarkHandler_RetrieveStoredTextureForVuMarkTarget_m7E757E9CEFA73C330F1FEF9279124519D2504405 ();
// 0x000000C9 UnityEngine.Texture2D VuMarkHandler::GenerateTextureFromVuMarkInstanceImage(Vuforia.VuMarkTarget)
extern void VuMarkHandler_GenerateTextureFromVuMarkInstanceImage_m41B1D568D39C84271CC0AEB4528255AF93DB726E ();
// 0x000000CA System.Void VuMarkHandler::GenerateVuMarkBorderOutline(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_GenerateVuMarkBorderOutline_m1D44B6C698239037766978CB7DED5211366CA355 ();
// 0x000000CB System.Void VuMarkHandler::DestroyChildAugmentationsOfTransform(UnityEngine.Transform)
extern void VuMarkHandler_DestroyChildAugmentationsOfTransform_m8D6B616864D5B390DFCE0ACF15B7DFCC297EC459 ();
// 0x000000CC T VuMarkHandler::GetValueFromDictionary(System.Collections.Generic.Dictionary`2<System.String,T>,System.String)
// 0x000000CD System.Void VuMarkHandler::ToggleRenderers(UnityEngine.GameObject,System.Boolean)
extern void VuMarkHandler_ToggleRenderers_m20F75954C911F49530BD4B0BB9DF1311B5E32834 ();
// 0x000000CE System.Void VuMarkHandler::UpdateClosestTarget()
extern void VuMarkHandler_UpdateClosestTarget_m9ABB10DED08B5E0BCFC18F08A3D51ED14A2E6854 ();
// 0x000000CF System.Collections.IEnumerator VuMarkHandler::ShowPanelAfter(System.Single,System.String,System.String,System.String,UnityEngine.Sprite)
extern void VuMarkHandler_ShowPanelAfter_m435541410FD0569A3A829FE195508F6461664B75 ();
// 0x000000D0 System.Void VuMarkHandler::.ctor()
extern void VuMarkHandler__ctor_mC06FD7E39E10350BAD74B424626715A1AF55B95A ();
// 0x000000D1 System.Void VuMarkTrackableEventHandler::Start()
extern void VuMarkTrackableEventHandler_Start_mE122E497406D7EB80C6531B0F59E5DD2E05F9D6B ();
// 0x000000D2 System.Void VuMarkTrackableEventHandler::OnTrackingFound()
extern void VuMarkTrackableEventHandler_OnTrackingFound_mAF0BED951C1B062963C8AEC3CB5FE6D633E1F695 ();
// 0x000000D3 System.Void VuMarkTrackableEventHandler::OnTrackingLost()
extern void VuMarkTrackableEventHandler_OnTrackingLost_m267AC8F198B01972F645BFBEFBE24F100BF77EE8 ();
// 0x000000D4 System.Void VuMarkTrackableEventHandler::OnVuforiaStarted()
extern void VuMarkTrackableEventHandler_OnVuforiaStarted_m07C89FF17D6B86E199F0F0019531BF469CDF599B ();
// 0x000000D5 System.Void VuMarkTrackableEventHandler::OnDisable()
extern void VuMarkTrackableEventHandler_OnDisable_mEB6F282BED1C72D099DC3C06D94EA2A3BDD0BD25 ();
// 0x000000D6 System.Void VuMarkTrackableEventHandler::Update()
extern void VuMarkTrackableEventHandler_Update_mE04B13D1671E438496CD975BE6E84508C66B6FF4 ();
// 0x000000D7 System.Void VuMarkTrackableEventHandler::UpdateVuMarkBorderOutline()
extern void VuMarkTrackableEventHandler_UpdateVuMarkBorderOutline_m6521ECA9C683E3EEE20DC06D0864DAE3AF8B0475 ();
// 0x000000D8 System.Void VuMarkTrackableEventHandler::UpdateCanvasFadeAmount()
extern void VuMarkTrackableEventHandler_UpdateCanvasFadeAmount_mEB91822C9A3314CF9C83A8538A67C55DF126AABC ();
// 0x000000D9 System.Void VuMarkTrackableEventHandler::DestroyChildAugmentationsOfTransform(UnityEngine.Transform)
extern void VuMarkTrackableEventHandler_DestroyChildAugmentationsOfTransform_m1566E1A93E8B22AA08CE42B1802D474D051A5BED ();
// 0x000000DA System.Void VuMarkTrackableEventHandler::OnVuMarkTargetAssigned()
extern void VuMarkTrackableEventHandler_OnVuMarkTargetAssigned_m29622658AC219B27E7E8E8E0C3A3EF4D56B8C30F ();
// 0x000000DB System.Void VuMarkTrackableEventHandler::OnVuMarkTargetLost()
extern void VuMarkTrackableEventHandler_OnVuMarkTargetLost_mFA2B3DFD0337C4D84D61188D9712EDD45E8260C9 ();
// 0x000000DC System.Void VuMarkTrackableEventHandler::.ctor()
extern void VuMarkTrackableEventHandler__ctor_mE7EE7B9BF670EECD2D2AE0298C07E43F82A5348B ();
// 0x000000DD System.Void CloudContentManager_AugmentationObject::.ctor()
extern void AugmentationObject__ctor_m7DA70D9C615A97AB293CC232AEA3B591B59C6E8B ();
// 0x000000DE System.Void ModelTargetsUIManager_<ClearImageSequencePause>d__18::.ctor(System.Int32)
extern void U3CClearImageSequencePauseU3Ed__18__ctor_m5A4BC98A442AB18B3AAEFDA27DDAB53717D0B42B ();
// 0x000000DF System.Void ModelTargetsUIManager_<ClearImageSequencePause>d__18::System.IDisposable.Dispose()
extern void U3CClearImageSequencePauseU3Ed__18_System_IDisposable_Dispose_m9ED956FC8C5154E012C8DEF890C2FD802E76A247 ();
// 0x000000E0 System.Boolean ModelTargetsUIManager_<ClearImageSequencePause>d__18::MoveNext()
extern void U3CClearImageSequencePauseU3Ed__18_MoveNext_m55B3F6961E3B0EDF1A3A8ADCB51CD1ED2DF4F8BC ();
// 0x000000E1 System.Object ModelTargetsUIManager_<ClearImageSequencePause>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClearImageSequencePauseU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D595CA47B19533C538B851809691DE3FAF54998 ();
// 0x000000E2 System.Void ModelTargetsUIManager_<ClearImageSequencePause>d__18::System.Collections.IEnumerator.Reset()
extern void U3CClearImageSequencePauseU3Ed__18_System_Collections_IEnumerator_Reset_m274F8792AECB92D2D1D4E1DB121F41EF0A7C62AB ();
// 0x000000E3 System.Object ModelTargetsUIManager_<ClearImageSequencePause>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CClearImageSequencePauseU3Ed__18_System_Collections_IEnumerator_get_Current_mD6CEB2E9A43EAB322700F89D3FD64D1051C7ADAE ();
// 0x000000E4 System.Void MaterialSwitch_<UpdateScanTime>d__3::.ctor(System.Int32)
extern void U3CUpdateScanTimeU3Ed__3__ctor_m1B3C8A3591BBA8074D9C90E2171AD027B4D294AB ();
// 0x000000E5 System.Void MaterialSwitch_<UpdateScanTime>d__3::System.IDisposable.Dispose()
extern void U3CUpdateScanTimeU3Ed__3_System_IDisposable_Dispose_mBD239454B6FE94F82FD2DEE78E89F5BD72692603 ();
// 0x000000E6 System.Boolean MaterialSwitch_<UpdateScanTime>d__3::MoveNext()
extern void U3CUpdateScanTimeU3Ed__3_MoveNext_m5414EBCF71005B33CECEFF88D872AEEE8CC1FB55 ();
// 0x000000E7 System.Object MaterialSwitch_<UpdateScanTime>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateScanTimeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m648CDD673360C2E6D894C49C29E3B811B3607195 ();
// 0x000000E8 System.Void MaterialSwitch_<UpdateScanTime>d__3::System.Collections.IEnumerator.Reset()
extern void U3CUpdateScanTimeU3Ed__3_System_Collections_IEnumerator_Reset_m0DF0AB876B5B0311A746AFE5FD79AF697C4EC002 ();
// 0x000000E9 System.Object MaterialSwitch_<UpdateScanTime>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateScanTimeU3Ed__3_System_Collections_IEnumerator_get_Current_mCA7A73D4422AE31666E166FDA6B3933B3192BAB2 ();
// 0x000000EA System.Void ModelTargetAugmentationHandler_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m3547D453A607A83B493E279FB7BEF0FDDED4DE54 ();
// 0x000000EB System.Void ModelTargetAugmentationHandler_<>c__DisplayClass9_1::.ctor()
extern void U3CU3Ec__DisplayClass9_1__ctor_m4A5078D83D85D99E05FED937FFE1458546B5FAE3 ();
// 0x000000EC System.Void ModelTargetAugmentationHandler_<>c__DisplayClass9_1::<PlayAnimations>b__0()
extern void U3CU3Ec__DisplayClass9_1_U3CPlayAnimationsU3Eb__0_m237F480E02886B086C157A091113404332ECFC96 ();
// 0x000000ED System.Void ModelTargetAugmentationHandler_<>c__DisplayClass9_1::<PlayAnimations>b__1()
extern void U3CU3Ec__DisplayClass9_1_U3CPlayAnimationsU3Eb__1_mAD9A5C4DCACABD8F86659D93F61D3E5E843E16B3 ();
// 0x000000EE System.Void SimpleAnimation_OnAnimationStarted::.ctor(System.Object,System.IntPtr)
extern void OnAnimationStarted__ctor_m647297EE2B54A619CCC9ED63D71ABA38A6CB37D2 ();
// 0x000000EF System.Void SimpleAnimation_OnAnimationStarted::Invoke()
extern void OnAnimationStarted_Invoke_m83CF6061B32BE0932D2C02DF87D3DE8B92C28CCD ();
// 0x000000F0 System.IAsyncResult SimpleAnimation_OnAnimationStarted::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnAnimationStarted_BeginInvoke_m3966902F3CCF112F07D255AC133FCE40B87AC52C ();
// 0x000000F1 System.Void SimpleAnimation_OnAnimationStarted::EndInvoke(System.IAsyncResult)
extern void OnAnimationStarted_EndInvoke_mBF041185E1B550A3EDD4C2CBDD7867E567568CC5 ();
// 0x000000F2 System.Void SimpleAnimation_OnAnimationCompleted::.ctor(System.Object,System.IntPtr)
extern void OnAnimationCompleted__ctor_mEF056BE1F1E5A52E6D641A5D24062D249564E247 ();
// 0x000000F3 System.Void SimpleAnimation_OnAnimationCompleted::Invoke()
extern void OnAnimationCompleted_Invoke_m76ED2192D0E87595255A275521358091DDB29809 ();
// 0x000000F4 System.IAsyncResult SimpleAnimation_OnAnimationCompleted::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnAnimationCompleted_BeginInvoke_m3B80D2CA51A9CBD14BE75A8B4A9F4E4B73C515C3 ();
// 0x000000F5 System.Void SimpleAnimation_OnAnimationCompleted::EndInvoke(System.IAsyncResult)
extern void OnAnimationCompleted_EndInvoke_m49FDD982625CA5E0BE18D3AC3DA43F231D98EEE8 ();
// 0x000000F6 System.Void VirtualButtonEventHandler_<DelayOnButtonReleasedEvent>d__9::.ctor(System.Int32)
extern void U3CDelayOnButtonReleasedEventU3Ed__9__ctor_mFF1B8C4051DED1C844873346F17E34BC05122FB9 ();
// 0x000000F7 System.Void VirtualButtonEventHandler_<DelayOnButtonReleasedEvent>d__9::System.IDisposable.Dispose()
extern void U3CDelayOnButtonReleasedEventU3Ed__9_System_IDisposable_Dispose_mD6A184C47DA9AA640D526730BA5CCEBFB2D71DAB ();
// 0x000000F8 System.Boolean VirtualButtonEventHandler_<DelayOnButtonReleasedEvent>d__9::MoveNext()
extern void U3CDelayOnButtonReleasedEventU3Ed__9_MoveNext_m8394FFCD3B02F1F73EF3DEF5587E059455B53066 ();
// 0x000000F9 System.Object VirtualButtonEventHandler_<DelayOnButtonReleasedEvent>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayOnButtonReleasedEventU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m983B2D918B82A5386B0E5FFC404FD610C69462D6 ();
// 0x000000FA System.Void VirtualButtonEventHandler_<DelayOnButtonReleasedEvent>d__9::System.Collections.IEnumerator.Reset()
extern void U3CDelayOnButtonReleasedEventU3Ed__9_System_Collections_IEnumerator_Reset_m097640341444F26330B87630F8DEFA4ED8E0489C ();
// 0x000000FB System.Object VirtualButtonEventHandler_<DelayOnButtonReleasedEvent>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CDelayOnButtonReleasedEventU3Ed__9_System_Collections_IEnumerator_get_Current_mF0A11F90E9E677B9E6F94C920C43117955DB2456 ();
// 0x000000FC System.Void VuMarkHandler_AugmentationObject::.ctor()
extern void AugmentationObject__ctor_mCB93CFBD99578CA457AA52952AD253E0E385E9B2 ();
// 0x000000FD System.Void VuMarkHandler_<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m0AC19920AC526CD25A58E26C4825E18E9D672EAC ();
// 0x000000FE System.Boolean VuMarkHandler_<>c__DisplayClass17_0::<OnVuMarkTargetAvailable>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3COnVuMarkTargetAvailableU3Eb__0_mC4E7AEB5902CEC6CB61B0D38ADFF395F849E7CB3 ();
// 0x000000FF System.Void VuMarkHandler_<OnVuMarkTargetAvailable>d__17::.ctor(System.Int32)
extern void U3COnVuMarkTargetAvailableU3Ed__17__ctor_mEF8C14040E2894A61C4171991CE71945349437B4 ();
// 0x00000100 System.Void VuMarkHandler_<OnVuMarkTargetAvailable>d__17::System.IDisposable.Dispose()
extern void U3COnVuMarkTargetAvailableU3Ed__17_System_IDisposable_Dispose_mD4FE814FE527CB9F4C2D4622301C10FE3D1F40B9 ();
// 0x00000101 System.Boolean VuMarkHandler_<OnVuMarkTargetAvailable>d__17::MoveNext()
extern void U3COnVuMarkTargetAvailableU3Ed__17_MoveNext_m6CC0C44D8C3593B1E0F05A9A20B17F35F5845952 ();
// 0x00000102 System.Object VuMarkHandler_<OnVuMarkTargetAvailable>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnVuMarkTargetAvailableU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B977F4D09CB7ACECEE1ECD696ECEC8707203D22 ();
// 0x00000103 System.Void VuMarkHandler_<OnVuMarkTargetAvailable>d__17::System.Collections.IEnumerator.Reset()
extern void U3COnVuMarkTargetAvailableU3Ed__17_System_Collections_IEnumerator_Reset_mE642B2F75C534B5045E8D88B758BBBC2DE20F9AB ();
// 0x00000104 System.Object VuMarkHandler_<OnVuMarkTargetAvailable>d__17::System.Collections.IEnumerator.get_Current()
extern void U3COnVuMarkTargetAvailableU3Ed__17_System_Collections_IEnumerator_get_Current_mD8BE4E609F108DD3A4C2EA3074674B702C179AE4 ();
// 0x00000105 System.Void VuMarkHandler_<ShowPanelAfter>d__34::.ctor(System.Int32)
extern void U3CShowPanelAfterU3Ed__34__ctor_m56E7950A60E1B79DE7DFDCEB17B9E33106D35CD1 ();
// 0x00000106 System.Void VuMarkHandler_<ShowPanelAfter>d__34::System.IDisposable.Dispose()
extern void U3CShowPanelAfterU3Ed__34_System_IDisposable_Dispose_mC3DFC18D9C537AD92084A0010247DF5E0327A1EC ();
// 0x00000107 System.Boolean VuMarkHandler_<ShowPanelAfter>d__34::MoveNext()
extern void U3CShowPanelAfterU3Ed__34_MoveNext_m22C855780908A9E670E7E9CBD47ED31A528E97B8 ();
// 0x00000108 System.Object VuMarkHandler_<ShowPanelAfter>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPanelAfterU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D355975D78CADEC6F6CB60AA6E49B5587B78FC8 ();
// 0x00000109 System.Void VuMarkHandler_<ShowPanelAfter>d__34::System.Collections.IEnumerator.Reset()
extern void U3CShowPanelAfterU3Ed__34_System_Collections_IEnumerator_Reset_mB71198B330E69EDE4238B9CE3831516DF26B1D65 ();
// 0x0000010A System.Object VuMarkHandler_<ShowPanelAfter>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CShowPanelAfterU3Ed__34_System_Collections_IEnumerator_get_Current_mADCA36CD8D8D7E888230855200F2160A3D262235 ();
static Il2CppMethodPointer s_methodPointers[266] = 
{
	CloudContentManager_Start_m1A249B4E7155B590DB64A7FB424713C51249F217,
	CloudContentManager_ShowTargetInfo_m24458476585524ACBE1687DF945F1108951B3348,
	CloudContentManager_HandleTargetFinderResult_mC45AB69C45934E3838D4518815462624C77356A0,
	CloudContentManager_GetValuefromDictionary_m0D3B289035CF5DB5E90A1892B220D9CAFEBC6F82,
	CloudContentManager__ctor_m31EDA06AF3091DE3C02FCFDF8F50B0065C5E2451,
	CloudTrackableEventHandler_Start_mAE7FDDE4EBD79E641A2ADE85B9B07262C2D258A7,
	CloudTrackableEventHandler_OnReset_mC2A6EF8F1D8B3B1B7BA40B0B15BE35ABA36203DD,
	CloudTrackableEventHandler_TargetCreated_mE9029FEA2AD6B03088BCAD38C4DB56690FB6BF89,
	CloudTrackableEventHandler_OnTrackingFound_m77B08EA135C4D67F42B8B383F33F014A40FF1222,
	CloudTrackableEventHandler_OnTrackingLost_m9804CCF4FA608AF89635AEFA831D838F6D0C3FDA,
	CloudTrackableEventHandler__ctor_mB8CC0EF8080BFA061AA5A4392CDD9676AB5E05E2,
	RotateAroundCylinder_Start_m59FE262623E0FB429D18C640F8A750C71628FA0B,
	RotateAroundCylinder_Update_m2FF508C025877EADE94E95DD961E88A87DD3FF5B,
	RotateAroundCylinder__ctor_m1BCF6DAB9AE2A50625D73E91682D52BFDF4B5ED8,
	GroundPlaneUI_Start_mF92F8660AA30F5429ACF6DB36FA2DE60C53D46DE,
	GroundPlaneUI_Update_m7292300C5A57D23199D77F206D2EB3C22E735DBF,
	GroundPlaneUI_LateUpdate_m864ED171C41985D10CC273F5A89954915E488D36,
	GroundPlaneUI_OnDestroy_m3288F381C3DA63369E1254A06AA5FF3291748014,
	GroundPlaneUI_Reset_m6AED6EDE5F8BF1F679C16F46EA2F5F86D91EC433,
	GroundPlaneUI_UpdateTitle_m0EF0D27321E7FFAE2E824D5D3C7A63B97E5C7A60,
	GroundPlaneUI_IsCanvasButtonPressed_m407BE2630C9287F930DFB28E1C4A4BA159037AF2,
	GroundPlaneUI_OnDevicePoseStatusChanged_m77524799E8F314CE9F7996462D2FB2B11C125798,
	GroundPlaneUI__ctor_m9C2DE966C4004B06B5941488AB9FD663C7169723,
	PlaneManager_get_GroundPlaneHitReceived_mF9CBA5A7B9096A9E783C33C536B68C21FDECD66D,
	PlaneManager_set_GroundPlaneHitReceived_m7F09ECE895AFE70BF0BAB0371ADECD576CD3D0B6,
	PlaneManager_get_TrackingStatusIsTrackedAndNormal_m98047D808F55564042F3DCE2DB1D199C1F44EC13,
	PlaneManager_get_TrackingStatusIsTrackedOrLimited_mC0D4B020CAD365386942FD13E0944FA8EE50E40A,
	PlaneManager_get_SurfaceIndicatorVisibilityConditionsMet_m9ADC55208694DFD7C62BD3C55F7F71BBFCEF05A9,
	PlaneManager_Start_mFAC04BE252F51631049537E1E9813683068D57F5,
	PlaneManager_Update_m325751E24CF7E935D3DA8A1A65650537E862F60F,
	PlaneManager_LateUpdate_m376838C73FC649748283F8F129BD31E999D0D204,
	PlaneManager_OnDestroy_m7F625C36FEC6ED0860BEEB6EB5E23FD45CD2C009,
	PlaneManager_HandleAutomaticHitTest_m0C0A26ED9A5F91E931D86921AFB7FF981B17B10A,
	PlaneManager_HandleInteractiveHitTest_mE13B1314C7DFC14ABCDB8EDC072CBF2097F00162,
	PlaneManager_PlaceObjectInMidAir_m0D58B308FD5F951AE118C0ECBBFDA661B4738586,
	PlaneManager_SetGroundMode_mB838C438A2859513331EE6E0F29F477ADA921359,
	PlaneManager_SetMidAirMode_mEACF5A11514483BD9BBF42D04BE64C8C910590F5,
	PlaneManager_SetPlacementMode_mA6852C193F105787BE6A7F92A016D5C306F360F8,
	PlaneManager_ResetScene_mB522B800C0131CEE210FFC3315504A355C556C32,
	PlaneManager_ResetTrackers_mAF8119784220FD2329DC70DD1A3A26456869567F,
	PlaneManager_SetMode_m6508AF2D3E9128D661FBC3351811E2A21DEEF8C6,
	PlaneManager_SetSurfaceIndicatorVisible_m9E804518A3C8FC0451D1ADABEEA66C738FBF33F2,
	PlaneManager_TimerFinished_mFD6F695D7C2C98240E457FBE1C8737ADC18C7EC6,
	PlaneManager_OnVuforiaStarted_m02A681F3FCD82EE9DC65829E4756D189E9075400,
	PlaneManager_OnVuforiaPaused_mF52BCE32A5968188757C5970E6E8EDB4693CC28D,
	PlaneManager_OnTrackerStarted_m1A8EE6BBE2D4CDAE64690247C718F64D4EB8C1F8,
	PlaneManager_OnDevicePoseStatusChanged_m307BC6AE4D49B512F3A1B0BBAF41E0B1F64E8D77,
	PlaneManager__ctor_m1843562DC58F54BB644D7F8E31B529589374E375,
	PlaneManager__cctor_m54D96E384F7D3906C9AC03943435F9576C90A1EE,
	ProductPlacement_get_IsPlaced_mA590A83EB95A1452535693C82AA7FEB0AC1A00AA,
	ProductPlacement_set_IsPlaced_m0841EE5E7EFDCD43F4895A8B9E29B9D1599EEDB6,
	ProductPlacement_get_ChairVisibilityConditionsMet_m5028A14C25D5C60C378512DBF037B61072A8F392,
	ProductPlacement_Start_mCF059202B3B8D682133D21F37B02D3587E9C4309,
	ProductPlacement_Update_m94C34785F198D1F08601830EF52D41D6BA4AB60B,
	ProductPlacement_LateUpdate_m205B9429FA46CD8778B4059D17E6FE71CC46831E,
	ProductPlacement_Reset_m19E566EC917318D804A8476128A7485F6DBE08E6,
	ProductPlacement_SetProductAnchor_mB58975E1D5A738B7BFBAC0DB3B113CB9948B497E,
	ProductPlacement_SetupMaterials_m5D3D8B4AC0F88981932649FC29AAD6356C85FD6D,
	ProductPlacement_SetupFloor_m75D9F56A1A6D628B5C9A8B3D2CF78615723E1062,
	ProductPlacement_SetVisible_m32EB83E0BFEF45FA5780910262379FB54253554A,
	ProductPlacement_EnablePreviewModeTransparency_m963D6C90A5D96707021509BE0D9B7279FF5558DD,
	ProductPlacement__ctor_m2CDE14D5D8EBCE2D0A949D1DA6D813C379FE549A,
	TouchHandler_get_DoubleTap_m30385B6D53C063BB0DB7728BB25F648716449CBB,
	TouchHandler_get_IsSingleFingerStationary_m8D5A0DFB6DEBF5B0A36FDC2B8BDAB3ED51E2A47B,
	TouchHandler_get_IsSingleFingerDragging_m850D210A0CE3F84ECE02F43A5C303882A9E9A60E,
	TouchHandler_Start_mD632DE0E6E43175BA12F1656DA31798436B651A0,
	TouchHandler_Update_mA532DD4125F92F4F9C9FF1353852C47C320FECB5,
	TouchHandler_IsSingleFingerDown_m76EC3A622B30F3578C9823715B41C2842BA2B2C0,
	TouchHandler__ctor_m6C28D981C551E4F30B0905698B7EB85DB9C1AE67,
	UtilityHelper_RotateTowardCamera_m967B224FAC40A06B36F3EAA160D5D439F759E1D2,
	UtilityHelper_EnableRendererColliderCanvas_m97CF532CB9AA52B3701A00BDF2D45E2985E6DA1A,
	UtilityHelper_GetNumberOfActiveAnchors_m74F247C2298D141737988E112C83FB76F7C3A5FA,
	ModelSwap_Start_mAEF9320E9B6420CB03AFAE5907836D6787848579,
	ModelSwap_Update_m10B296B593D86E787130BEBB9DA3F001F67C4687,
	ModelSwap__ctor_m6998B7EEAC510BAD241B659554A9761797E7365E,
	VideoController_Start_m1891079C32DC78429E5CEC33D276D83BCE84C7B7,
	VideoController_Update_m0816ED36E474C740C98A91B84EBA6680A00E6D03,
	VideoController_OnApplicationPause_m0EF9D3BFAF6FF1F07ACE9A5F356D9B81D7A8450D,
	VideoController_Play_m75D0C29EC0A1C28C8A9B241F1D3AEAEA31CE6353,
	VideoController_Pause_m88C49D9180B3424D4594ACD97CAFC9071F31AD1E,
	VideoController_PauseAudio_mD010EE3CAB4625BFDB42547DDA38CBBC53196AC3,
	VideoController_ShowPlayButton_m2980A5174AFBC2939F5B7C8EA4607C7A34796C58,
	VideoController_LogClipInfo_m404BBC89F22E53915F37EF2D8EED70970ADA8932,
	VideoController_HandleVideoError_mD4A4850E89A7039BC97348DD7C6D7FF995F9A524,
	VideoController_HandleStartedEvent_m9B1042A9A2EC1F5D08FA963C493997C3A1B1F621,
	VideoController_HandlePrepareCompleted_m11BAE1BC62085C2758BA6275884CFB9EA1FCF482,
	VideoController_HandleSeekCompleted_m821948B955C91CA26F87F33725900D26CE1AFE91,
	VideoController_HandleLoopPointReached_m175A725A575E13F2B01BF2D40223CA86790F871F,
	VideoController__ctor_m21F2B9C67DD50F1B9E620F1AAD7CAE46BE8E6F50,
	VideoTrackableEventHandler_OnTrackingLost_m5792FA2718BFD3A32012288ED706A6B9143E6A52,
	VideoTrackableEventHandler__ctor_m97B8BBDF7E1CD37061B339B7295FBDAB1263E20A,
	ModelTargetsManager_Awake_m0E53268243EBA82EC0AD441A8B7125F0B758D3F2,
	ModelTargetsManager_Start_mEBAE1684831E31D3123C264F35F37F8AC6B3934F,
	ModelTargetsManager_LateUpdate_m96CC0DB5B97DF015402C2642E89C539671E624A8,
	ModelTargetsManager_OnDestroy_mF476E1D727879DC0F4320965D38F39AB9F82D1B9,
	ModelTargetsManager_OnVuforiaStarted_mB97FFF7D19392F6A72736D8C9E94C212A06D8EEA,
	ModelTargetsManager_EnableSymbolicTargetsUI_m624071D1E67C7052CA9B255FF44E018DAF30051A,
	ModelTargetsManager_AddAdvancedModelTargetBehaviour_m2AE3D6EEA19B9342A19EE2541EE2D1DD26F63CC0,
	ModelTargetsManager_SelectDataSetStandard_m280A7827F6174F6458854D4945815D1DCF27E845,
	ModelTargetsManager_SelectDataSetAdvanced_mA7ED00ED6F4595708DADDC5EDE2870967FDACCB6,
	ModelTargetsManager_ShowDataSetMenu_m8637ED87034974E131887D4F320AD26D5C123323,
	ModelTargetsManager_CycleGuideView_m6492DAD6662BAF00758F5FBE468F8433CDD790AC,
	ModelTargetsManager_LoadDataSet_m8114D9F82ECBA7CDF725ABA7FBC9DF2F4D3DF756,
	ModelTargetsManager_DeactivateActiveDataSets_m3F74FB6638A29D6DC0C46587A75F6BAB75CF908D,
	ModelTargetsManager_ActivateDataSet_mD14B9ED2B11718B2A0E4062F8CAE1CC4A8D07961,
	ModelTargetsManager_ActiveTrackablesExist_m73DBF610FE53BDA78C3272F34FE669C59B45E13A,
	ModelTargetsManager_LogActiveDataSets_m8EEEDD6413117C964B16EFBBD0BA7918834410D9,
	ModelTargetsManager_LogAllDataSets_m165A002EBB5BD31A2817ECD5AD097B71FAA200A3,
	ModelTargetsManager__ctor_m53DDB8B0C71926E33443080CC34E37AAAE5FC9A6,
	ModelTargetsUIManager_Start_m3E8DF0E86FCB2CB3E2DE31EEA648D3227FCFFFC3,
	ModelTargetsUIManager_Update_m17B13569B65A4734765BB74F67714E42EF4CF1C6,
	ModelTargetsUIManager_SetUI_m02D1CA3398AABB004836E6B36D33C89A6EEE377E,
	ModelTargetsUIManager_InitSymbolicTargetIcons_m72E76663F9F0B34CAB47437032D1E45A9C038FFB,
	ModelTargetsUIManager_UpdateSymbolicTargetIconFadeCycle_m313BD8E3376EEDE68074AB532F7FE98DBE19DD67,
	ModelTargetsUIManager_ResetImageSequenceValues_m73BF730534D7C8ECDFE63EB394C13726D9AA8507,
	ModelTargetsUIManager_ClearImageSequencePause_m2008ADD108934EE68BD061B90B5923ABC3E1279E,
	ModelTargetsUIManager__ctor_mF09D13875C7D4CDB3EAB6E43DB86CD9CA45F4329,
	ModelTargetsUIManager_U3CClearImageSequencePauseU3Eb__18_0_m61212E2ED7456C08EC5850072D916CBCB183D64B,
	MT360TrackableEventHandler_Start_m42C0B6EAB3A4F8CDA8707E41DB7FD9DF2566A192,
	MT360TrackableEventHandler_OnTrackingFound_m8976F0122C642D44056A9EDF28F0C0E4DD2D9455,
	MT360TrackableEventHandler_OnTrackingLost_m4BF8BF2E73FA2B1D11E4724B27CDAA74E5C296AE,
	MT360TrackableEventHandler__ctor_m27545E8EBEF44B305ADDAC5D0DAC4A26F4AA59B4,
	MTAdvancedTrackableEventHandler_Start_m9C01F8AF2E77453EA7A378569A99FEED3EC85154,
	MTAdvancedTrackableEventHandler_Update_mA1E49F1571DB89AC1E19332DBB7249E66824304B,
	MTAdvancedTrackableEventHandler_OnTrackingFound_m2C70F8603DBAFD256ECF9707B297FC2853E459E1,
	MTAdvancedTrackableEventHandler__ctor_m11682E63231AB14D5EA962CED2354C5659E7B90B,
	CameraDepthEnabler_Start_m822BD8E6B7533937F639A07EAC870F310D41806D,
	CameraDepthEnabler__ctor_mB20B9C00F4ACB9E618C9C19287254DCEFA292E79,
	MaterialSwitch_SwitchMaterial_m8A6C3B6C215BDCB53C9ED7566CEAE29865570EC7,
	MaterialSwitch_UpdateScanTime_m2A8787D0A94B7A9085DCC6D6463F1515363F81D1,
	MaterialSwitch__ctor_m9ECBC19323711F86DC63255DFCDDA0739C361BA9,
	ModelTargetAugmentationHandler_Start_m96DB46F32E2EF80286D2C3C8C9498FD306DE4F33,
	ModelTargetAugmentationHandler_OnDestroy_mC0635CBF238A18E251EE9FE30E460445AA6AE143,
	ModelTargetAugmentationHandler_OnTrackableStatusChanged_mF494BC756CB124092F8975A705400287CD690C9D,
	ModelTargetAugmentationHandler_SwitchMaterial_mDD6448077EFDC1BB81F7A554ADABE79BC910EDCF,
	ModelTargetAugmentationHandler_PlayParticleSystems_mB86077AC3A3324E57EED76511C2936FAE45F95D1,
	ModelTargetAugmentationHandler_StopParticleSystems_m28E35603D7606D4CEB8160B74AD2978201AFA8F5,
	ModelTargetAugmentationHandler_PlayAnimations_m88DD5A3645181BF2A5DDB41369F1E2F2CD44FF75,
	ModelTargetAugmentationHandler_RewindAnimations_m6E48D7F5A1E92B8971521E4685E17941083B5097,
	ModelTargetAugmentationHandler_SaveMaterials_m0C720E743879626AE44F783DD8A7B9510DBEDB9E,
	ModelTargetAugmentationHandler_RestoreMaterials_mDFB07D7EE3B9A420892E5210E5D6213CB7CC47AE,
	ModelTargetAugmentationHandler_SetMaterial_m6940EBAB17CCE874FB336F4535D4AF20F3DADB42,
	ModelTargetAugmentationHandler__ctor_mFC2FFC3BE1C84A9F7B1C0DD74B9736C51A92BECE,
	SimpleAnimation_add_OnStartEvent_m23B9E031FF8A744E575A9F5A4BC13CAF08C48F80,
	SimpleAnimation_remove_OnStartEvent_m28DE82BAF4C62732D425B60F5F9F28D3252AFA78,
	SimpleAnimation_add_OnCompleteEvent_m57FBA0F8EE09D4C5A02A83E0A52B4A54B0AEACB6,
	SimpleAnimation_remove_OnCompleteEvent_m12040E4218027E7B6A44134D9A6D716365668F6A,
	SimpleAnimation_Awake_m621C73D72D3124F1368FDBE18ED6BB682C4DD8FB,
	SimpleAnimation_Update_m2726894C30584D475B67B666081CA089BCA5BC0D,
	SimpleAnimation_Play_m2E7DD25B77F74FDE058A0CA2686D80B21EF3EC5E,
	SimpleAnimation_Rewind_mE7DD53FA0635E0B8699BAAE2F0B4803CA3CCD0E4,
	SimpleAnimation_ClearEventHandlers_m98F2A8B111941ACDC63D6FCF8468DB87D5A71A9D,
	SimpleAnimation__ctor_m0C73DDCB3B0E92C83D516DF2E4B9A1AC899C830C,
	MultiTargetTrackableEventHandler_OnTrackingFound_m08D292F9EFD2DA71240E873019B9620A8588E143,
	MultiTargetTrackableEventHandler_OnTrackingLost_mCD7CA3102D3A0DC123E3C3C3B2A1DEA05DC4AA4A,
	MultiTargetTrackableEventHandler__ctor_m92F6CA8B472E17DB646D901460A294F3A5390E9E,
	ObjectRecoTrackableEventHandler_OnTrackingFound_mC8CA12662793CD967244A0F6D7861B8731E33AAB,
	ObjectRecoTrackableEventHandler_OnTrackingLost_mA0D0A9397BDC6D2294374DED48E341DE8F34BFF4,
	ObjectRecoTrackableEventHandler__ctor_m623F3A1281245513A42CF005864B0EDDB6F9EED5,
	FrameQualityMeter_SetMeter_mB10FABEC92E6C050534B75EF8B07CF4200D0211D,
	FrameQualityMeter_SetQuality_m9D69BDD905F0C0168BEE6C9C72B8499F5C78FB53,
	FrameQualityMeter__ctor_mB84A4DD55E895658C2E85F8B7009BC9723EE11CF,
	UDTEventHandler_get_LastTargetIndex_m78FFC8F5665ADECA6B627369E81E2E9CE017F0FD,
	UDTEventHandler_Start_mBA3A3CE7BC4A6E5DF4ADA38FB04431B0B175EB33,
	UDTEventHandler_OnDestroy_m1C094595A93980F1D67A2F66A8CE1B1425811B09,
	UDTEventHandler_OnInitialized_mFF58AFEEC8EE9B9DA087EBC72F5F62A3DCAD7281,
	UDTEventHandler_OnFrameQualityChanged_mD1F599FD85A0CC3BBA8130B4AC7C1787897B8B33,
	UDTEventHandler_OnNewTrackableSource_mAF1B110BB8555C01BB555E69EADA4F5A3554E90D,
	UDTEventHandler_BuildNewTarget_mB24A7CAE15969CFCC90410D9D2DD20EBEDAF34B5,
	UDTEventHandler__ctor_mDA437972B6FA7AD9E4F9C882D4AE5587DD96128A,
	VirtualButtonEventHandler_Awake_m875247FD2C484B698BC2DBCFC83896F3FC8E29C8,
	VirtualButtonEventHandler_Destroy_m77F0306779C65FB04710C19F45132C8B156B0F83,
	VirtualButtonEventHandler_OnButtonPressed_m6628E092DDD9B8500943C635414C795D79E23E15,
	VirtualButtonEventHandler_OnButtonReleased_m5A19FD662DA0FAFE99133F124546840338DE71CF,
	VirtualButtonEventHandler_SetVirtualButtonMaterial_m62D6D1869B345AF413CE30441257A14154699099,
	VirtualButtonEventHandler_DelayOnButtonReleasedEvent_mA5843009378A59EDF60BD6B441A419D4DC83ADA2,
	VirtualButtonEventHandler__ctor_mBE6C3E8B422E3294883DA251D68AEFC8A074831A,
	LookAtObject_Update_m5677D4871BB9D1F98868995885276228B1127475,
	LookAtObject__ctor_mA335E6987B020E736FD62B5046B408D6723F6A4B,
	PanelShowHide_Hide_m2CF36F60AF2E33B48C256F3EADB4392943B7893E,
	PanelShowHide_Show_m7F97A9C59FD86D0FDBBBB8377400CE32725CC844,
	PanelShowHide_ResetShowTrigger_m9F50C06354E467C7C4BBC9520EF7F2920DC407A3,
	PanelShowHide__ctor_m44AA13FA3ADE9435634F72691A9E3B954CBFA05D,
	VuMarkHandler_Awake_mBF60FA215B9502E6256854DCB7D1152ECF03E224,
	VuMarkHandler_Start_mC4E3689BD02198A08C6A65172538F96A13A91B37,
	VuMarkHandler_Update_mEA1A7C06F671487EB3E8516EC91C0DE69F106FB9,
	VuMarkHandler_OnDestroy_m06A84B1048566E6E6B01133511FE04FEC65E2B11,
	VuMarkHandler_OnVuforiaStarted_mCF41E81FD8784E99BF71EBFCAAAD4D78164BBD38,
	VuMarkHandler_OnVuMarkBehaviourDetected_m00EF6DE1862BD54833F360F2FF4238E63160B7E9,
	VuMarkHandler_OnVuMarkTargetAvailable_m52F553B014E26EA6849F94104D5CDF9090544951,
	VuMarkHandler_OnVuMarkDetected_mD9D8346D52332BFDA10A661BCEA652A5A160AB41,
	VuMarkHandler_OnVuMarkLost_m68FD0A642A747FB62D2A202051EED925FDE1E2E3,
	VuMarkHandler_GetVuMarkDataType_m14F5FAA7F060A755A7C4A87ABFE3D404330F536D,
	VuMarkHandler_GetVuMarkId_mFD567BC0846485F6639BDEBC67154594AB441A94,
	VuMarkHandler_GetVuMarkImage_m24BA3B60C81BDA1E8BAC1D9C1DA8E3EFE55502A6,
	VuMarkHandler_GetNumericVuMarkDescription_m263BBBC1AA93705930EEA18C1D02320323FE4A1D,
	VuMarkHandler_SetVuMarkInfoForCanvas_mFBBEF06C803232D04EA098B263C03BE186D5F4E6,
	VuMarkHandler_SetVuMarkAugmentation_m73122D0FEBDB75F1305438DA776953E58137BFCA,
	VuMarkHandler_SetVuMarkOpticalSeeThroughConfig_m53944D862EF8C9166774F2C4A9E4F36265B827E3,
	VuMarkHandler_RetrieveStoredTextureForVuMarkTarget_m7E757E9CEFA73C330F1FEF9279124519D2504405,
	VuMarkHandler_GenerateTextureFromVuMarkInstanceImage_m41B1D568D39C84271CC0AEB4528255AF93DB726E,
	VuMarkHandler_GenerateVuMarkBorderOutline_m1D44B6C698239037766978CB7DED5211366CA355,
	VuMarkHandler_DestroyChildAugmentationsOfTransform_m8D6B616864D5B390DFCE0ACF15B7DFCC297EC459,
	NULL,
	VuMarkHandler_ToggleRenderers_m20F75954C911F49530BD4B0BB9DF1311B5E32834,
	VuMarkHandler_UpdateClosestTarget_m9ABB10DED08B5E0BCFC18F08A3D51ED14A2E6854,
	VuMarkHandler_ShowPanelAfter_m435541410FD0569A3A829FE195508F6461664B75,
	VuMarkHandler__ctor_mC06FD7E39E10350BAD74B424626715A1AF55B95A,
	VuMarkTrackableEventHandler_Start_mE122E497406D7EB80C6531B0F59E5DD2E05F9D6B,
	VuMarkTrackableEventHandler_OnTrackingFound_mAF0BED951C1B062963C8AEC3CB5FE6D633E1F695,
	VuMarkTrackableEventHandler_OnTrackingLost_m267AC8F198B01972F645BFBEFBE24F100BF77EE8,
	VuMarkTrackableEventHandler_OnVuforiaStarted_m07C89FF17D6B86E199F0F0019531BF469CDF599B,
	VuMarkTrackableEventHandler_OnDisable_mEB6F282BED1C72D099DC3C06D94EA2A3BDD0BD25,
	VuMarkTrackableEventHandler_Update_mE04B13D1671E438496CD975BE6E84508C66B6FF4,
	VuMarkTrackableEventHandler_UpdateVuMarkBorderOutline_m6521ECA9C683E3EEE20DC06D0864DAE3AF8B0475,
	VuMarkTrackableEventHandler_UpdateCanvasFadeAmount_mEB91822C9A3314CF9C83A8538A67C55DF126AABC,
	VuMarkTrackableEventHandler_DestroyChildAugmentationsOfTransform_m1566E1A93E8B22AA08CE42B1802D474D051A5BED,
	VuMarkTrackableEventHandler_OnVuMarkTargetAssigned_m29622658AC219B27E7E8E8E0C3A3EF4D56B8C30F,
	VuMarkTrackableEventHandler_OnVuMarkTargetLost_mFA2B3DFD0337C4D84D61188D9712EDD45E8260C9,
	VuMarkTrackableEventHandler__ctor_mE7EE7B9BF670EECD2D2AE0298C07E43F82A5348B,
	AugmentationObject__ctor_m7DA70D9C615A97AB293CC232AEA3B591B59C6E8B,
	U3CClearImageSequencePauseU3Ed__18__ctor_m5A4BC98A442AB18B3AAEFDA27DDAB53717D0B42B,
	U3CClearImageSequencePauseU3Ed__18_System_IDisposable_Dispose_m9ED956FC8C5154E012C8DEF890C2FD802E76A247,
	U3CClearImageSequencePauseU3Ed__18_MoveNext_m55B3F6961E3B0EDF1A3A8ADCB51CD1ED2DF4F8BC,
	U3CClearImageSequencePauseU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D595CA47B19533C538B851809691DE3FAF54998,
	U3CClearImageSequencePauseU3Ed__18_System_Collections_IEnumerator_Reset_m274F8792AECB92D2D1D4E1DB121F41EF0A7C62AB,
	U3CClearImageSequencePauseU3Ed__18_System_Collections_IEnumerator_get_Current_mD6CEB2E9A43EAB322700F89D3FD64D1051C7ADAE,
	U3CUpdateScanTimeU3Ed__3__ctor_m1B3C8A3591BBA8074D9C90E2171AD027B4D294AB,
	U3CUpdateScanTimeU3Ed__3_System_IDisposable_Dispose_mBD239454B6FE94F82FD2DEE78E89F5BD72692603,
	U3CUpdateScanTimeU3Ed__3_MoveNext_m5414EBCF71005B33CECEFF88D872AEEE8CC1FB55,
	U3CUpdateScanTimeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m648CDD673360C2E6D894C49C29E3B811B3607195,
	U3CUpdateScanTimeU3Ed__3_System_Collections_IEnumerator_Reset_m0DF0AB876B5B0311A746AFE5FD79AF697C4EC002,
	U3CUpdateScanTimeU3Ed__3_System_Collections_IEnumerator_get_Current_mCA7A73D4422AE31666E166FDA6B3933B3192BAB2,
	U3CU3Ec__DisplayClass9_0__ctor_m3547D453A607A83B493E279FB7BEF0FDDED4DE54,
	U3CU3Ec__DisplayClass9_1__ctor_m4A5078D83D85D99E05FED937FFE1458546B5FAE3,
	U3CU3Ec__DisplayClass9_1_U3CPlayAnimationsU3Eb__0_m237F480E02886B086C157A091113404332ECFC96,
	U3CU3Ec__DisplayClass9_1_U3CPlayAnimationsU3Eb__1_mAD9A5C4DCACABD8F86659D93F61D3E5E843E16B3,
	OnAnimationStarted__ctor_m647297EE2B54A619CCC9ED63D71ABA38A6CB37D2,
	OnAnimationStarted_Invoke_m83CF6061B32BE0932D2C02DF87D3DE8B92C28CCD,
	OnAnimationStarted_BeginInvoke_m3966902F3CCF112F07D255AC133FCE40B87AC52C,
	OnAnimationStarted_EndInvoke_mBF041185E1B550A3EDD4C2CBDD7867E567568CC5,
	OnAnimationCompleted__ctor_mEF056BE1F1E5A52E6D641A5D24062D249564E247,
	OnAnimationCompleted_Invoke_m76ED2192D0E87595255A275521358091DDB29809,
	OnAnimationCompleted_BeginInvoke_m3B80D2CA51A9CBD14BE75A8B4A9F4E4B73C515C3,
	OnAnimationCompleted_EndInvoke_m49FDD982625CA5E0BE18D3AC3DA43F231D98EEE8,
	U3CDelayOnButtonReleasedEventU3Ed__9__ctor_mFF1B8C4051DED1C844873346F17E34BC05122FB9,
	U3CDelayOnButtonReleasedEventU3Ed__9_System_IDisposable_Dispose_mD6A184C47DA9AA640D526730BA5CCEBFB2D71DAB,
	U3CDelayOnButtonReleasedEventU3Ed__9_MoveNext_m8394FFCD3B02F1F73EF3DEF5587E059455B53066,
	U3CDelayOnButtonReleasedEventU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m983B2D918B82A5386B0E5FFC404FD610C69462D6,
	U3CDelayOnButtonReleasedEventU3Ed__9_System_Collections_IEnumerator_Reset_m097640341444F26330B87630F8DEFA4ED8E0489C,
	U3CDelayOnButtonReleasedEventU3Ed__9_System_Collections_IEnumerator_get_Current_mF0A11F90E9E677B9E6F94C920C43117955DB2456,
	AugmentationObject__ctor_mCB93CFBD99578CA457AA52952AD253E0E385E9B2,
	U3CU3Ec__DisplayClass17_0__ctor_m0AC19920AC526CD25A58E26C4825E18E9D672EAC,
	U3CU3Ec__DisplayClass17_0_U3COnVuMarkTargetAvailableU3Eb__0_mC4E7AEB5902CEC6CB61B0D38ADFF395F849E7CB3,
	U3COnVuMarkTargetAvailableU3Ed__17__ctor_mEF8C14040E2894A61C4171991CE71945349437B4,
	U3COnVuMarkTargetAvailableU3Ed__17_System_IDisposable_Dispose_mD4FE814FE527CB9F4C2D4622301C10FE3D1F40B9,
	U3COnVuMarkTargetAvailableU3Ed__17_MoveNext_m6CC0C44D8C3593B1E0F05A9A20B17F35F5845952,
	U3COnVuMarkTargetAvailableU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B977F4D09CB7ACECEE1ECD696ECEC8707203D22,
	U3COnVuMarkTargetAvailableU3Ed__17_System_Collections_IEnumerator_Reset_mE642B2F75C534B5045E8D88B758BBBC2DE20F9AB,
	U3COnVuMarkTargetAvailableU3Ed__17_System_Collections_IEnumerator_get_Current_mD8BE4E609F108DD3A4C2EA3074674B702C179AE4,
	U3CShowPanelAfterU3Ed__34__ctor_m56E7950A60E1B79DE7DFDCEB17B9E33106D35CD1,
	U3CShowPanelAfterU3Ed__34_System_IDisposable_Dispose_mC3DFC18D9C537AD92084A0010247DF5E0327A1EC,
	U3CShowPanelAfterU3Ed__34_MoveNext_m22C855780908A9E670E7E9CBD47ED31A528E97B8,
	U3CShowPanelAfterU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D355975D78CADEC6F6CB60AA6E49B5587B78FC8,
	U3CShowPanelAfterU3Ed__34_System_Collections_IEnumerator_Reset_mB71198B330E69EDE4238B9CE3831516DF26B1D65,
	U3CShowPanelAfterU3Ed__34_System_Collections_IEnumerator_get_Current_mADCA36CD8D8D7E888230855200F2160A3D262235,
};
static const int32_t s_InvokerIndices[266] = 
{
	23,
	31,
	26,
	113,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	169,
	23,
	49,
	795,
	49,
	49,
	114,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	31,
	31,
	31,
	23,
	23,
	32,
	31,
	27,
	23,
	31,
	23,
	169,
	23,
	3,
	114,
	31,
	114,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	31,
	31,
	23,
	49,
	49,
	49,
	23,
	23,
	49,
	23,
	122,
	563,
	131,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	31,
	31,
	23,
	27,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	26,
	31,
	31,
	31,
	23,
	26,
	31,
	26,
	114,
	23,
	23,
	23,
	23,
	23,
	586,
	23,
	23,
	23,
	14,
	23,
	114,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2052,
	23,
	23,
	23,
	2048,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	27,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2053,
	32,
	23,
	10,
	23,
	23,
	23,
	32,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	561,
	23,
	23,
	23,
	23,
	388,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	28,
	26,
	26,
	28,
	28,
	28,
	28,
	26,
	26,
	26,
	28,
	28,
	26,
	26,
	-1,
	401,
	23,
	2054,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	102,
	23,
	113,
	26,
	102,
	23,
	113,
	26,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	23,
	114,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x060000CC, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)3, 16545 },
	{ (Il2CppRGCTXDataType)3, 16546 },
};
extern const Il2CppCodeGenModule g_SamplesScriptsCodeGenModule;
const Il2CppCodeGenModule g_SamplesScriptsCodeGenModule = 
{
	"SamplesScripts.dll",
	266,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
};
