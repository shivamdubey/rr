﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void LoadFromCacheAndDownload::Start()
extern void LoadFromCacheAndDownload_Start_mF32D52234FE00F040B4BB1395F5010588D945F93 ();
// 0x00000002 System.Collections.IEnumerator LoadFromCacheAndDownload::LoadBundle()
extern void LoadFromCacheAndDownload_LoadBundle_mEA5AEBD76F320D6BD91C056CEE91A47823E9417A ();
// 0x00000003 System.Void LoadFromCacheAndDownload::Update()
extern void LoadFromCacheAndDownload_Update_mF461E24ED517DFD4111120A26C307469B2897C33 ();
// 0x00000004 System.Void LoadFromCacheAndDownload::Load()
extern void LoadFromCacheAndDownload_Load_m5B90A4850F9221191EEA833FFEA9E91ECDFB79F4 ();
// 0x00000005 System.Void LoadFromCacheAndDownload::.ctor()
extern void LoadFromCacheAndDownload__ctor_mAC6995A99E872673A0A35772701D9F0B3707B4F9 ();
// 0x00000006 System.Void AnimHandler::Start()
extern void AnimHandler_Start_m6F98063D4F4B868C02BA2D630BB97427825D4AEE ();
// 0x00000007 System.Void AnimHandler::Update()
extern void AnimHandler_Update_mF51A703FBD519577F497DBAAB6D1D677BDE20F7E ();
// 0x00000008 System.Void AnimHandler::Assign_Object()
extern void AnimHandler_Assign_Object_m6C8D276B4F56C4E0FB7D71B1D8A3BA8AFF1B0F76 ();
// 0x00000009 System.Void AnimHandler::ChangeMaterial()
extern void AnimHandler_ChangeMaterial_mFBA2C6E940AD8A7542BE89DD5BD63EBCF3BCC1E7 ();
// 0x0000000A System.Void AnimHandler::OnShotAnimClick()
extern void AnimHandler_OnShotAnimClick_mED339775F0CD76003D3254C8B85EBB2B63C9AD73 ();
// 0x0000000B System.Void AnimHandler::ShotAnim()
extern void AnimHandler_ShotAnim_m0637CD8CD4C897245DB580766E7205D67E2CC2BE ();
// 0x0000000C System.Collections.IEnumerator AnimHandler::ShotAnimEnd()
extern void AnimHandler_ShotAnimEnd_mABAC6E3D370E386EF98569409E2C1F46D4311030 ();
// 0x0000000D System.Void AnimHandler::ResetFunction()
extern void AnimHandler_ResetFunction_m07B899C06B32AC36132E122A96D70A22C0E63168 ();
// 0x0000000E System.Void AnimHandler::OnShotAnimClickAR()
extern void AnimHandler_OnShotAnimClickAR_m89DBB3BEF44AA673E9645F77A7A11C6E445F3313 ();
// 0x0000000F System.Void AnimHandler::OnFound()
extern void AnimHandler_OnFound_m271E8441112C17DD3144F4D1B946EC984857F986 ();
// 0x00000010 System.Void AnimHandler::OnLost()
extern void AnimHandler_OnLost_m556F3916621EBA81FD11D9EE0C7608D55417C0E8 ();
// 0x00000011 System.Collections.IEnumerator AnimHandler::ShotAnimEndAR()
extern void AnimHandler_ShotAnimEndAR_mA5AF671B5EB543BEC519151F699424A5D277E244 ();
// 0x00000012 System.Void AnimHandler::TransMarkerAnimFound()
extern void AnimHandler_TransMarkerAnimFound_mD0786DA63D9D948355DD2D5E50D8E1E1AD33D520 ();
// 0x00000013 System.Void AnimHandler::TransMarkerAnimLost()
extern void AnimHandler_TransMarkerAnimLost_mD77966C793584CCE74161C6AA489E8FD6FEC2DC6 ();
// 0x00000014 System.Collections.IEnumerator AnimHandler::TransShotAnimEnd()
extern void AnimHandler_TransShotAnimEnd_m7A47829F05174B8A7B2FCA9E14ECD157C29A4C7E ();
// 0x00000015 System.Void AnimHandler::.ctor()
extern void AnimHandler__ctor_mD26DDFC6AD28D76BF0A6881F8FB3E99484C41599 ();
// 0x00000016 System.Void FlowerHitHandler::Start()
extern void FlowerHitHandler_Start_m6C0AD31C060B746B00C2F81FEAD55EAB9090D6EE ();
// 0x00000017 System.Void FlowerHitHandler::Update()
extern void FlowerHitHandler_Update_m2F6B78DE2935E34608A4E562ECFEAAFFF1F0B068 ();
// 0x00000018 System.Void FlowerHitHandler::.ctor()
extern void FlowerHitHandler__ctor_m9EBDD10D0E296E578990FAFBA72D7C0C5AE0DF3D ();
// 0x00000019 System.Void JKLaxmiProTracker::Start()
extern void JKLaxmiProTracker_Start_m5043C361FFA04951BB720953D9AE79950554DFBD ();
// 0x0000001A System.Void JKLaxmiProTracker::AnimationAndSoundFunction(System.String,System.Boolean)
extern void JKLaxmiProTracker_AnimationAndSoundFunction_mA06ADFB75D684B92943C49A91D12DCD601B48131 ();
// 0x0000001B System.Void JKLaxmiProTracker::OnPlayerButtonClick(System.Int32)
extern void JKLaxmiProTracker_OnPlayerButtonClick_mBFCCB51E2E3ACB32E7CC0C9184BED807903C4B00 ();
// 0x0000001C System.Void JKLaxmiProTracker::OnPlayerRotateButtonClick(System.Int32)
extern void JKLaxmiProTracker_OnPlayerRotateButtonClick_m32608D1817FDD616F0706C3A86419C2F81A33FC0 ();
// 0x0000001D System.Void JKLaxmiProTracker::OnPlayerAnimationButtonClick(System.Int32)
extern void JKLaxmiProTracker_OnPlayerAnimationButtonClick_mC8770BA4093F5E66DF7454FF4BE35D8B40D6002C ();
// 0x0000001E System.Void JKLaxmiProTracker::.ctor()
extern void JKLaxmiProTracker__ctor_m6B332B92A3D7BF11ECD067C31618EF723B99158B ();
// 0x0000001F System.Void JKLaxmiTracker::Start()
extern void JKLaxmiTracker_Start_m3635D67AF427C997402BDF732B32E549A6E8331B ();
// 0x00000020 System.Void JKLaxmiTracker::AnimationAndSoundFunction(System.String,System.Boolean)
extern void JKLaxmiTracker_AnimationAndSoundFunction_m744B000BE76C1724DCFC1EA7F7648A6C658CD7CC ();
// 0x00000021 System.Void JKLaxmiTracker::OnPlayerButtonClick(System.Int32)
extern void JKLaxmiTracker_OnPlayerButtonClick_m0CB174C29B134E9CBBE9D9FFEC66361D947059B7 ();
// 0x00000022 System.Void JKLaxmiTracker::OnPlayerRotateButtonClick(System.Int32)
extern void JKLaxmiTracker_OnPlayerRotateButtonClick_mFFA78FCA657C95689586A02462007D17B9EC1DEF ();
// 0x00000023 System.Void JKLaxmiTracker::OnPlayerAnimationButtonClick(System.Int32)
extern void JKLaxmiTracker_OnPlayerAnimationButtonClick_mCA07117E200DFDFB91C809B45E6163F73D3D4196 ();
// 0x00000024 System.Void JKLaxmiTracker::.ctor()
extern void JKLaxmiTracker__ctor_m6F17F66BEC947069401804E67597E6F242640CEF ();
// 0x00000025 System.Void Rotate::Start()
extern void Rotate_Start_m31842842A9C0AD4874D72AC2D0BF8EC302CF81B8 ();
// 0x00000026 System.Void Rotate::Update()
extern void Rotate_Update_m2A5FD9FAEFB71B89128F9EA1346CEA14E9BC3C4E ();
// 0x00000027 System.Void Rotate::.ctor()
extern void Rotate__ctor_m634B0959655CD742CA62A6FE82BA86F20966679A ();
// 0x00000028 System.Void RRLogoNewTracker::Start()
extern void RRLogoNewTracker_Start_m445D7555346F50ECBB62972C2DA9B659CB741DC1 ();
// 0x00000029 System.Void RRLogoNewTracker::AnimationAndSoundFunction(System.String,System.Boolean)
extern void RRLogoNewTracker_AnimationAndSoundFunction_m0FE270CC67D718CFB1EDA3ECA7EF48C71D496F0E ();
// 0x0000002A System.Void RRLogoNewTracker::OnPlayerButtonClick(System.Int32)
extern void RRLogoNewTracker_OnPlayerButtonClick_m5E171A827E376B93B8F38577D6C35C551CE02619 ();
// 0x0000002B System.Void RRLogoNewTracker::OnPlayerRotateButtonClick(System.Int32)
extern void RRLogoNewTracker_OnPlayerRotateButtonClick_m45CA9D7D7F5890C60611D49892A282B2A7E5CE91 ();
// 0x0000002C System.Void RRLogoNewTracker::OnPlayerAnimationButtonClick(System.Int32)
extern void RRLogoNewTracker_OnPlayerAnimationButtonClick_mC87FB621A540D804FB5248C84405CE713A8A288B ();
// 0x0000002D System.Void RRLogoNewTracker::.ctor()
extern void RRLogoNewTracker__ctor_m251E3950EE718985296288217CCE91BDAEAB21F1 ();
// 0x0000002E System.Void RRLogoOldTracker::Start()
extern void RRLogoOldTracker_Start_m961CC871B8E29E39A1271A1C91C66A838A3DFB6A ();
// 0x0000002F System.Void RRLogoOldTracker::AnimationAndSoundFunction(System.String,System.Boolean)
extern void RRLogoOldTracker_AnimationAndSoundFunction_mD431FC5449E54A077FBCCF981ED662D263D48807 ();
// 0x00000030 System.Void RRLogoOldTracker::OnPlayerButtonClick(System.Int32)
extern void RRLogoOldTracker_OnPlayerButtonClick_mE8424306E0BDBBB10DD101670701B1C68E8C457C ();
// 0x00000031 System.Void RRLogoOldTracker::OnPlayerRotateButtonClick(System.Int32)
extern void RRLogoOldTracker_OnPlayerRotateButtonClick_mB97B3353E3DE27E5BE0EAEB402EA291EDC628527 ();
// 0x00000032 System.Void RRLogoOldTracker::OnPlayerAnimationButtonClick(System.Int32)
extern void RRLogoOldTracker_OnPlayerAnimationButtonClick_m850F75999DCFE0814B512C5D7FFE359CF2BD1FD7 ();
// 0x00000033 System.Void RRLogoOldTracker::.ctor()
extern void RRLogoOldTracker__ctor_m4A23F2D84B4141495C2D8C1771EEFD2EB2F6E94B ();
// 0x00000034 System.Void SafeArea::Awake()
extern void SafeArea_Awake_mD9BEF89303539D1ADAACFC1D9EE8F6FC99360E6B ();
// 0x00000035 System.Void SafeArea::Update()
extern void SafeArea_Update_m09334741B86BAD44424B057A97B0EF61CBD8A5CA ();
// 0x00000036 System.Void SafeArea::Refresh()
extern void SafeArea_Refresh_m615FC807F2AB0C46A43588B0891BE8E4FC866382 ();
// 0x00000037 UnityEngine.Rect SafeArea::GetSafeArea()
extern void SafeArea_GetSafeArea_mCEBD5B0EDAED35AE574848BB09F1BE0D8ACD0126 ();
// 0x00000038 System.Void SafeArea::ApplySafeArea(UnityEngine.Rect)
extern void SafeArea_ApplySafeArea_m417F46DDCE219213C49D6374B93F857F24E42E62 ();
// 0x00000039 System.Void SafeArea::.ctor()
extern void SafeArea__ctor_mB1238EE624A536CC15578B1730A2360F792A0F9F ();
// 0x0000003A System.Void TrackEventHandlerJKLaxmi::Start()
extern void TrackEventHandlerJKLaxmi_Start_m95BA2DEC26F0A5E54CFEC58AAF3A3A1A730DDEC1 ();
// 0x0000003B System.Void TrackEventHandlerJKLaxmi::OnDestroy()
extern void TrackEventHandlerJKLaxmi_OnDestroy_m0076D7E6E5B00182069AD7F5B249A51288F6AC22 ();
// 0x0000003C System.Void TrackEventHandlerJKLaxmi::Update()
extern void TrackEventHandlerJKLaxmi_Update_m9F7F4D6FD46A2B82A17198CADA604E69A4CC1655 ();
// 0x0000003D System.Void TrackEventHandlerJKLaxmi::OnTrackableStateChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_Status)
extern void TrackEventHandlerJKLaxmi_OnTrackableStateChanged_mCB35E6AE00FE8AF867AD9A442008EC2AD3CEC4D6 ();
// 0x0000003E System.Void TrackEventHandlerJKLaxmi::OnTrackingFound()
extern void TrackEventHandlerJKLaxmi_OnTrackingFound_m615F1CAB299ED1571D7AFEC07AF5C5DB5A061BE7 ();
// 0x0000003F System.Void TrackEventHandlerJKLaxmi::OnTrackingLost()
extern void TrackEventHandlerJKLaxmi_OnTrackingLost_m46B70E824889AE7F5B58513E08075D98CF39B053 ();
// 0x00000040 System.Void TrackEventHandlerJKLaxmi::.ctor()
extern void TrackEventHandlerJKLaxmi__ctor_m81206B0681E1C55B781FE4E2434126EBC8739C3B ();
// 0x00000041 System.Void TrackEventHandlerJKLaxmiPro::Start()
extern void TrackEventHandlerJKLaxmiPro_Start_m397F0CD009B181E325670704DD0CCD4F04B06BE6 ();
// 0x00000042 System.Void TrackEventHandlerJKLaxmiPro::OnDestroy()
extern void TrackEventHandlerJKLaxmiPro_OnDestroy_m27A6F37FB68A69EB2CDD0A8F21EB923A4C0BA570 ();
// 0x00000043 System.Void TrackEventHandlerJKLaxmiPro::Update()
extern void TrackEventHandlerJKLaxmiPro_Update_mD437F796649188BB2FD747F3D549F4B62CA43FDC ();
// 0x00000044 System.Void TrackEventHandlerJKLaxmiPro::OnTrackableStateChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_Status)
extern void TrackEventHandlerJKLaxmiPro_OnTrackableStateChanged_m16AA76F10B19C9C451E04772F3B6D5266AAA7D00 ();
// 0x00000045 System.Void TrackEventHandlerJKLaxmiPro::OnTrackingFound()
extern void TrackEventHandlerJKLaxmiPro_OnTrackingFound_m0D0D2A7E150511A4DA4B41B45CA5CC8A9579C07D ();
// 0x00000046 System.Void TrackEventHandlerJKLaxmiPro::OnTrackingLost()
extern void TrackEventHandlerJKLaxmiPro_OnTrackingLost_m56A69458D08D8F37D551C56F327DBBD19731FD44 ();
// 0x00000047 System.Void TrackEventHandlerJKLaxmiPro::.ctor()
extern void TrackEventHandlerJKLaxmiPro__ctor_m971F0827B8753B53E9A47698A4D06A9C0F402875 ();
// 0x00000048 System.Void TrackEventHandlerRRLogoNew::Start()
extern void TrackEventHandlerRRLogoNew_Start_mF0D1118EF43D5B0F1EB8D68FA802019E08A795D3 ();
// 0x00000049 System.Void TrackEventHandlerRRLogoNew::OnDestroy()
extern void TrackEventHandlerRRLogoNew_OnDestroy_mCD220749E7704E7BFDA5701A62804892057EC087 ();
// 0x0000004A System.Void TrackEventHandlerRRLogoNew::Update()
extern void TrackEventHandlerRRLogoNew_Update_mF1B97259B6F6B525AA6FAD6E8B5C6113BA352051 ();
// 0x0000004B System.Void TrackEventHandlerRRLogoNew::OnTrackableStateChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_Status)
extern void TrackEventHandlerRRLogoNew_OnTrackableStateChanged_m8AA8CCF260F1043C610DD65B06F4777E61DB1638 ();
// 0x0000004C System.Void TrackEventHandlerRRLogoNew::OnTrackingFound()
extern void TrackEventHandlerRRLogoNew_OnTrackingFound_mBDDD08EABE1605AB9A67DBF20961343BB2A24017 ();
// 0x0000004D System.Void TrackEventHandlerRRLogoNew::OnTrackingLost()
extern void TrackEventHandlerRRLogoNew_OnTrackingLost_m100882790426557C55F65F68302022BFDD191EAC ();
// 0x0000004E System.Void TrackEventHandlerRRLogoNew::.ctor()
extern void TrackEventHandlerRRLogoNew__ctor_mB1F4BA23C6D860261E6F60B5F72A273A75867932 ();
// 0x0000004F System.Void TrackEventHandlerRRLogoOld::Start()
extern void TrackEventHandlerRRLogoOld_Start_m58028B9882621D12B328D569AAF0F5E4A6F688A1 ();
// 0x00000050 System.Void TrackEventHandlerRRLogoOld::OnDestroy()
extern void TrackEventHandlerRRLogoOld_OnDestroy_m102F95DC449DD4351D0A532ED6EF263FE43372F0 ();
// 0x00000051 System.Void TrackEventHandlerRRLogoOld::Update()
extern void TrackEventHandlerRRLogoOld_Update_mC16A3E79614A2F4428C1C697F520FC6CA64416BC ();
// 0x00000052 System.Void TrackEventHandlerRRLogoOld::OnTrackableStateChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_Status)
extern void TrackEventHandlerRRLogoOld_OnTrackableStateChanged_mD470EFA12C2E52811E672D6431F86A1E41991775 ();
// 0x00000053 System.Void TrackEventHandlerRRLogoOld::OnTrackingFound()
extern void TrackEventHandlerRRLogoOld_OnTrackingFound_m93965E7DF89A900BAC9EA0FE657FD639742F0AAA ();
// 0x00000054 System.Void TrackEventHandlerRRLogoOld::OnTrackingLost()
extern void TrackEventHandlerRRLogoOld_OnTrackingLost_m54635C86D7FE050BD8E04E8E92E74D8740EE22E0 ();
// 0x00000055 System.Void TrackEventHandlerRRLogoOld::.ctor()
extern void TrackEventHandlerRRLogoOld__ctor_m5F9F090DEB32EE65F5849814EC2475A6069C609B ();
// 0x00000056 System.Void TrackEventHandlerWorldCupOne::Start()
extern void TrackEventHandlerWorldCupOne_Start_m241FE993871965BAD0239A91B41123B674F39C10 ();
// 0x00000057 System.Void TrackEventHandlerWorldCupOne::OnDestroy()
extern void TrackEventHandlerWorldCupOne_OnDestroy_mF06C868CA69A637B1AA3CB7701ADFEFA422D3D2E ();
// 0x00000058 System.Void TrackEventHandlerWorldCupOne::Update()
extern void TrackEventHandlerWorldCupOne_Update_mAFF3356727F1CDF681D8B055749BF1669979864C ();
// 0x00000059 System.Void TrackEventHandlerWorldCupOne::OnTrackableStateChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_Status)
extern void TrackEventHandlerWorldCupOne_OnTrackableStateChanged_m4C9476EF61E220726561BB6AFAAC1C85DDB3ED75 ();
// 0x0000005A System.Void TrackEventHandlerWorldCupOne::OnTrackingFound()
extern void TrackEventHandlerWorldCupOne_OnTrackingFound_mA67BFB5DC6CE2CCA3124747452D5B95267704978 ();
// 0x0000005B System.Void TrackEventHandlerWorldCupOne::OnTrackingLost()
extern void TrackEventHandlerWorldCupOne_OnTrackingLost_mF12C20F6775598BDF424C544B2CC27B2E701A537 ();
// 0x0000005C System.Void TrackEventHandlerWorldCupOne::.ctor()
extern void TrackEventHandlerWorldCupOne__ctor_m376C75CB431E5F7571EACD69286B20CF5A08BBED ();
// 0x0000005D System.Void TrackEventHandlerWorldCupTwo::Start()
extern void TrackEventHandlerWorldCupTwo_Start_m2AB8752DAB38401B5C82C1DADB5B97A926D31589 ();
// 0x0000005E System.Void TrackEventHandlerWorldCupTwo::OnDestroy()
extern void TrackEventHandlerWorldCupTwo_OnDestroy_m75A791E319CF636B146FEE537B7EC5DD2682F459 ();
// 0x0000005F System.Void TrackEventHandlerWorldCupTwo::Update()
extern void TrackEventHandlerWorldCupTwo_Update_mD574A9ED56F89BD8A20E18C2AE0E9B0F7E054382 ();
// 0x00000060 System.Void TrackEventHandlerWorldCupTwo::OnTrackableStateChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_Status)
extern void TrackEventHandlerWorldCupTwo_OnTrackableStateChanged_m843B9D305A7208AE58172D58496CAC78C82CDC79 ();
// 0x00000061 System.Void TrackEventHandlerWorldCupTwo::OnTrackingFound()
extern void TrackEventHandlerWorldCupTwo_OnTrackingFound_m73585600AC2FCA0B8E997C502683E32EB56AA886 ();
// 0x00000062 System.Void TrackEventHandlerWorldCupTwo::OnTrackingLost()
extern void TrackEventHandlerWorldCupTwo_OnTrackingLost_mD6349AD5435B342536208AFCB67EBF5301FB3639 ();
// 0x00000063 System.Void TrackEventHandlerWorldCupTwo::.ctor()
extern void TrackEventHandlerWorldCupTwo__ctor_mA75F02F0D81D9063C10800171EF52265C6D94881 ();
// 0x00000064 System.Void UI_Handler::Awake()
extern void UI_Handler_Awake_mC9DFFBFCEF18031AEEDF0E8850A16105E196A0CB ();
// 0x00000065 System.Void UI_Handler::Start()
extern void UI_Handler_Start_m41A544F025231321FC2EEC746ECEE6D203753CD8 ();
// 0x00000066 System.Void UI_Handler::OnExitClick()
extern void UI_Handler_OnExitClick_m584E6AE28BC79582B9A032ECBB4AD2FF144661A4 ();
// 0x00000067 System.Void UI_Handler::OnReset()
extern void UI_Handler_OnReset_mA7ED1BCBEE72E36143BEA8259BEC3E54E5B1F6E6 ();
// 0x00000068 System.Void UI_Handler::On3DSceneClick()
extern void UI_Handler_On3DSceneClick_m2A5ACCDB7DF4E4E4E4EA738A416378C8FCBF4FD5 ();
// 0x00000069 System.Void UI_Handler::OnARSceneClick()
extern void UI_Handler_OnARSceneClick_mE5C8B143B9FED7FBA703D1393A82E8E884E80986 ();
// 0x0000006A System.Void UI_Handler::OnHomeClick()
extern void UI_Handler_OnHomeClick_m47FDD3426389FF4F3AFCCCF2C842A497399C5890 ();
// 0x0000006B System.Void UI_Handler::OnCaptureClick()
extern void UI_Handler_OnCaptureClick_m6C29B8363D03892BDE3A990D6A2F35C5BE1804F7 ();
// 0x0000006C System.Collections.IEnumerator UI_Handler::TakeScreenshotAndSave()
extern void UI_Handler_TakeScreenshotAndSave_m91BEA244D8F332A3C3B835E4612CA4BE4A11F68F ();
// 0x0000006D System.Void UI_Handler::CamSwitch()
extern void UI_Handler_CamSwitch_m32188B78B2FBA776C403EA5D760878368A5125EF ();
// 0x0000006E System.Void UI_Handler::OnSkipButtonClick()
extern void UI_Handler_OnSkipButtonClick_m95570A90E438B5656A52E79262F05546293DC19E ();
// 0x0000006F System.Void UI_Handler::ResetFunction()
extern void UI_Handler_ResetFunction_mA050539AB12E591B950C3FD8F6DAF88B034112F6 ();
// 0x00000070 System.Void UI_Handler::.ctor()
extern void UI_Handler__ctor_m6DC2E7F9BD502D08A0456C5D09A71AA165F407E9 ();
// 0x00000071 System.Void WorldCupOneTracker::Start()
extern void WorldCupOneTracker_Start_mF6C593BBEAE553C9200ACF0FBA90094169A05DCF ();
// 0x00000072 System.Void WorldCupOneTracker::AnimationAndSoundFunction(System.String,System.Boolean)
extern void WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3 ();
// 0x00000073 System.Void WorldCupOneTracker::OnPlayerButtonClick(System.Int32)
extern void WorldCupOneTracker_OnPlayerButtonClick_m1F83130B2F2EEE2A83A3B08F39032537DF0B287A ();
// 0x00000074 System.Void WorldCupOneTracker::OnPlayerRotateButtonClick(System.Int32)
extern void WorldCupOneTracker_OnPlayerRotateButtonClick_mE413E604F73A811ABB1D44A5C2DDF34ECA4FE552 ();
// 0x00000075 System.Void WorldCupOneTracker::OnPlayerAnimationButtonClick(System.Int32)
extern void WorldCupOneTracker_OnPlayerAnimationButtonClick_m10B4471F1D9E2524485619C04F6BA877895DE9B3 ();
// 0x00000076 System.Void WorldCupOneTracker::.ctor()
extern void WorldCupOneTracker__ctor_mD16845E1E743BF1613B38EC83717AE1683613CA7 ();
// 0x00000077 System.Void WorldCupTwoTracker::Start()
extern void WorldCupTwoTracker_Start_m994B58169EAB8550B3320024CFDA6C4825103467 ();
// 0x00000078 System.Void WorldCupTwoTracker::AnimationAndSoundFunction(System.String,System.Boolean)
extern void WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9 ();
// 0x00000079 System.Void WorldCupTwoTracker::OnPlayerButtonClick(System.Int32)
extern void WorldCupTwoTracker_OnPlayerButtonClick_m299722C1E95EB77FFD9AB56953380C98189CC3D6 ();
// 0x0000007A System.Void WorldCupTwoTracker::OnPlayerRotateButtonClick(System.Int32)
extern void WorldCupTwoTracker_OnPlayerRotateButtonClick_mC46776A5E869E613CDEBB160EF7398C216481237 ();
// 0x0000007B System.Void WorldCupTwoTracker::OnPlayerAnimationButtonClick(System.Int32)
extern void WorldCupTwoTracker_OnPlayerAnimationButtonClick_mAB802B57334169446EB7DD3CFDCB0D124318C5C4 ();
// 0x0000007C System.Void WorldCupTwoTracker::.ctor()
extern void WorldCupTwoTracker__ctor_m0863875911C8AF93266BC44B8B6B972FAB40891C ();
// 0x0000007D ContinuousGesturePhase ContinuousGesture::get_Phase()
extern void ContinuousGesture_get_Phase_mCA8691B331B108539F709328FB9D37FCE5257D63 ();
// 0x0000007E System.Void ContinuousGesture::.ctor()
extern void ContinuousGesture__ctor_mC302AF0C31E49BC40B920B7B5D652E9B1B95F51E ();
// 0x0000007F System.Void ContinuousGestureRecognizer`1::Reset(T)
// 0x00000080 System.Void ContinuousGestureRecognizer`1::OnStateChanged(Gesture)
// 0x00000081 System.Void ContinuousGestureRecognizer`1::.ctor()
// 0x00000082 System.Void DiscreteGesture::.ctor()
extern void DiscreteGesture__ctor_m2A853CD23FF3956E66726A398F1A23A35E65F648 ();
// 0x00000083 System.Void DiscreteGestureRecognizer`1::OnStateChanged(Gesture)
// 0x00000084 System.Void DiscreteGestureRecognizer`1::.ctor()
// 0x00000085 System.String FingerEvent::get_Name()
extern void FingerEvent_get_Name_m63409408D113873479C461A890D91C80B516DF10 ();
// 0x00000086 System.Void FingerEvent::set_Name(System.String)
extern void FingerEvent_set_Name_mDF502F593BB1827140457E5F02A93F9B101DE342 ();
// 0x00000087 FingerEventDetector FingerEvent::get_Detector()
extern void FingerEvent_get_Detector_m0CDB153B3591142F215AA4B31F4F09D0F50C5FAE ();
// 0x00000088 System.Void FingerEvent::set_Detector(FingerEventDetector)
extern void FingerEvent_set_Detector_m233A75210069EAF6F233159179148EB1B5A08983 ();
// 0x00000089 FingerGestures_Finger FingerEvent::get_Finger()
extern void FingerEvent_get_Finger_m8F1446FCFACFE85F775A16B41577AC013BB16886 ();
// 0x0000008A System.Void FingerEvent::set_Finger(FingerGestures_Finger)
extern void FingerEvent_set_Finger_m51947C362E9D4C1D0C8294CD3368C6A991325F93 ();
// 0x0000008B UnityEngine.Vector2 FingerEvent::get_Position()
extern void FingerEvent_get_Position_mA6AAF7687A550CDDFC0DF7B3147FAC39200633A1 ();
// 0x0000008C System.Void FingerEvent::set_Position(UnityEngine.Vector2)
extern void FingerEvent_set_Position_mD3DD55FA50540DDCA13814455A74D0ADAF88D270 ();
// 0x0000008D UnityEngine.GameObject FingerEvent::get_Selection()
extern void FingerEvent_get_Selection_m0EABD79D7BC289B03804FD8FD8EFD4CF431ADA61 ();
// 0x0000008E System.Void FingerEvent::set_Selection(UnityEngine.GameObject)
extern void FingerEvent_set_Selection_m66D6F926B7AC1BC01371C8EA207F30D097DE7F53 ();
// 0x0000008F UnityEngine.RaycastHit FingerEvent::get_Hit()
extern void FingerEvent_get_Hit_m6896095BD0C83580FA2BB3C24FFD78D0528B2ACD ();
// 0x00000090 System.Void FingerEvent::set_Hit(UnityEngine.RaycastHit)
extern void FingerEvent_set_Hit_mCF5DC27BE56356E7B0FA08F37103BBF9808C2898 ();
// 0x00000091 System.Void FingerEvent::.ctor()
extern void FingerEvent__ctor_m3ACBF5A4E5C45B612521B4EC3A5F6BCBEB8E0BE2 ();
// 0x00000092 T FingerEventDetector`1::CreateFingerEvent()
// 0x00000093 System.Type FingerEventDetector`1::GetEventType()
// 0x00000094 System.Void FingerEventDetector`1::Start()
// 0x00000095 System.Void FingerEventDetector`1::InitFingerEventsList(System.Int32)
// 0x00000096 T FingerEventDetector`1::GetEvent(FingerGestures_Finger)
// 0x00000097 T FingerEventDetector`1::GetEvent(System.Int32)
// 0x00000098 System.Void FingerEventDetector`1::.ctor()
// 0x00000099 System.Void FingerEventDetector::ProcessFinger(FingerGestures_Finger)
// 0x0000009A System.Type FingerEventDetector::GetEventType()
// 0x0000009B System.Void FingerEventDetector::Awake()
extern void FingerEventDetector_Awake_m2B06F197BF79A7BACF8B3321307EB451E2813436 ();
// 0x0000009C System.Void FingerEventDetector::Start()
extern void FingerEventDetector_Start_m3E1020381ECFC0900B7E2F11C3D0532A14A5E0E1 ();
// 0x0000009D System.Void FingerEventDetector::Update()
extern void FingerEventDetector_Update_mCD5E9FB21C1585E8F4E889C9A3D596B93CC68256 ();
// 0x0000009E System.Void FingerEventDetector::ProcessFingers()
extern void FingerEventDetector_ProcessFingers_m5C6E9308CF953480B92A72F974F0D4CAB3919EF0 ();
// 0x0000009F System.Void FingerEventDetector::TrySendMessage(FingerEvent)
extern void FingerEventDetector_TrySendMessage_m1C8DBEBBB4B3500F47E960474872D2A532D228C9 ();
// 0x000000A0 UnityEngine.RaycastHit FingerEventDetector::get_LastHit()
extern void FingerEventDetector_get_LastHit_mFFF9FCA6771BE9F2747356E5319FD30C80402F69 ();
// 0x000000A1 UnityEngine.GameObject FingerEventDetector::PickObject(UnityEngine.Vector2)
extern void FingerEventDetector_PickObject_m705E414AA28E694C8E164490BAFC831A55E94B91 ();
// 0x000000A2 System.Void FingerEventDetector::UpdateSelection(FingerEvent)
extern void FingerEventDetector_UpdateSelection_m051A889F92C0D80D1308F48309C8ACB3915A5545 ();
// 0x000000A3 System.Void FingerEventDetector::.ctor()
extern void FingerEventDetector__ctor_m850AC678820E4B143A94E6C2488A8D8A070BE52E ();
// 0x000000A4 System.Void Gesture::add_OnStateChanged(Gesture_EventHandler)
extern void Gesture_add_OnStateChanged_mFE932ED8E23ACA4C6564A3485893CA1BA3FE5593 ();
// 0x000000A5 System.Void Gesture::remove_OnStateChanged(Gesture_EventHandler)
extern void Gesture_remove_OnStateChanged_m4C65E28030625B0575C3E1B812B9867DDB9A0DAE ();
// 0x000000A6 FingerGestures_FingerList Gesture::get_Fingers()
extern void Gesture_get_Fingers_m41CDD968FE8EB03670776F0122F23B02F6361C22 ();
// 0x000000A7 System.Void Gesture::set_Fingers(FingerGestures_FingerList)
extern void Gesture_set_Fingers_m3CCB847A68504C32B2CBD1DA0501F8888DBE7223 ();
// 0x000000A8 GestureRecognizer Gesture::get_Recognizer()
extern void Gesture_get_Recognizer_mD23CAB53D254DC465F2EF391E151482C07EC8F91 ();
// 0x000000A9 System.Void Gesture::set_Recognizer(GestureRecognizer)
extern void Gesture_set_Recognizer_m345C5941675360DFED066337EA5222672C29CE27 ();
// 0x000000AA System.Single Gesture::get_StartTime()
extern void Gesture_get_StartTime_mE29B562C7067967EC80D84BAE6195CAE8CF3677F ();
// 0x000000AB System.Void Gesture::set_StartTime(System.Single)
extern void Gesture_set_StartTime_m946C0B6D49315E12F8336EE5D8019E097BEEA31A ();
// 0x000000AC UnityEngine.Vector2 Gesture::get_StartPosition()
extern void Gesture_get_StartPosition_mCF0DBFDBBF9920DBAC9E53B2B903DC448CABCC51 ();
// 0x000000AD System.Void Gesture::set_StartPosition(UnityEngine.Vector2)
extern void Gesture_set_StartPosition_m72914599C23AC7622FF0CCFE09C89F62A40A4714 ();
// 0x000000AE UnityEngine.Vector2 Gesture::get_Position()
extern void Gesture_get_Position_mCF6CBD40FFE3D90A3B57CD2CC8DE63FCF9F12826 ();
// 0x000000AF System.Void Gesture::set_Position(UnityEngine.Vector2)
extern void Gesture_set_Position_m1A4BD357265D7780D6B980F9F569A37DC15CFE24 ();
// 0x000000B0 GestureRecognitionState Gesture::get_State()
extern void Gesture_get_State_m16C149C72CFDE864601C8B39CA7D57ED1438369A ();
// 0x000000B1 System.Void Gesture::set_State(GestureRecognitionState)
extern void Gesture_set_State_m315EFAC7380E3BC391C5EF22925EA257D91C96C9 ();
// 0x000000B2 GestureRecognitionState Gesture::get_PreviousState()
extern void Gesture_get_PreviousState_mD10D8584386CDF064FFA68B1ED57C788C6BF7989 ();
// 0x000000B3 System.Single Gesture::get_ElapsedTime()
extern void Gesture_get_ElapsedTime_m98B1BFB3D50FFDFB6147454F2F99BBD7300C70B0 ();
// 0x000000B4 UnityEngine.GameObject Gesture::get_StartSelection()
extern void Gesture_get_StartSelection_m2F671A58813B69F4E401DC2337BD85FB886B9A8C ();
// 0x000000B5 System.Void Gesture::set_StartSelection(UnityEngine.GameObject)
extern void Gesture_set_StartSelection_m6CA7D9DA8E5F0FDDDDD2492FCEAE22BB6AAD71DD ();
// 0x000000B6 UnityEngine.GameObject Gesture::get_Selection()
extern void Gesture_get_Selection_m4DF689355463A2749F1315756F8B65C76C588873 ();
// 0x000000B7 System.Void Gesture::set_Selection(UnityEngine.GameObject)
extern void Gesture_set_Selection_m587B0C38111956C258FB62141872AB5481717AF1 ();
// 0x000000B8 UnityEngine.RaycastHit Gesture::get_Hit()
extern void Gesture_get_Hit_m1AF29710F084B5EBE09C54DCB07C83E98846B05C ();
// 0x000000B9 System.Void Gesture::set_Hit(UnityEngine.RaycastHit)
extern void Gesture_set_Hit_m393E60C07E9BF08F0F6F578B65A5CB1DBE978C2F ();
// 0x000000BA UnityEngine.GameObject Gesture::PickObject(ScreenRaycaster,UnityEngine.Vector2)
extern void Gesture_PickObject_mDA2A8A4BD650AE9FFB9AC7F86030F4AA9E5BBA95 ();
// 0x000000BB System.Void Gesture::PickStartSelection(ScreenRaycaster)
extern void Gesture_PickStartSelection_mCCEB59B593D2F6E1BD92AD0D46DFF5B2AA35C49B ();
// 0x000000BC System.Void Gesture::PickSelection(ScreenRaycaster)
extern void Gesture_PickSelection_m65CD0ECD8072C84B93771CF3FDBE37FF3234A8F5 ();
// 0x000000BD System.Void Gesture::.ctor()
extern void Gesture__ctor_m9CDB4E7EE06898FA1D4E47C8CAD6BDD4FBAE5B2D ();
// 0x000000BE System.Void GestureRecognizer`1::add_OnGesture(GestureRecognizer`1_GestureEventHandler<T>)
// 0x000000BF System.Void GestureRecognizer`1::remove_OnGesture(GestureRecognizer`1_GestureEventHandler<T>)
// 0x000000C0 System.Void GestureRecognizer`1::Start()
// 0x000000C1 System.Void GestureRecognizer`1::OnEnable()
// 0x000000C2 System.Void GestureRecognizer`1::InitGestures()
// 0x000000C3 System.Collections.Generic.List`1<T> GestureRecognizer`1::get_Gestures()
// 0x000000C4 System.Boolean GestureRecognizer`1::CanBegin(T,FingerGestures_IFingerList)
// 0x000000C5 System.Void GestureRecognizer`1::OnBegin(T,FingerGestures_IFingerList)
// 0x000000C6 GestureRecognitionState GestureRecognizer`1::OnRecognize(T,FingerGestures_IFingerList)
// 0x000000C7 UnityEngine.GameObject GestureRecognizer`1::GetDefaultSelectionForSendMessage(T)
// 0x000000C8 T GestureRecognizer`1::CreateGesture()
// 0x000000C9 System.Type GestureRecognizer`1::GetGestureType()
// 0x000000CA System.Void GestureRecognizer`1::OnStateChanged(Gesture)
// 0x000000CB T GestureRecognizer`1::FindGestureByCluster(FingerClusterManager_Cluster)
// 0x000000CC T GestureRecognizer`1::MatchActiveGestureToCluster(FingerClusterManager_Cluster)
// 0x000000CD T GestureRecognizer`1::FindFreeGesture()
// 0x000000CE System.Void GestureRecognizer`1::Reset(T)
// 0x000000CF System.Void GestureRecognizer`1::Update()
// 0x000000D0 System.Void GestureRecognizer`1::UpdateUsingClusters()
// 0x000000D1 System.Void GestureRecognizer`1::UpdateExclusive()
// 0x000000D2 System.Void GestureRecognizer`1::UpdatePerFinger()
// 0x000000D3 System.Void GestureRecognizer`1::ProcessCluster(FingerClusterManager_Cluster)
// 0x000000D4 System.Void GestureRecognizer`1::ReleaseFingers(T)
// 0x000000D5 System.Void GestureRecognizer`1::Begin(T,System.Int32,FingerGestures_IFingerList)
// 0x000000D6 FingerGestures_IFingerList GestureRecognizer`1::GetTouches(T)
// 0x000000D7 System.Void GestureRecognizer`1::UpdateGesture(T,FingerGestures_IFingerList)
// 0x000000D8 System.Void GestureRecognizer`1::RaiseEvent(T)
// 0x000000D9 System.Void GestureRecognizer`1::.ctor()
// 0x000000DA System.Void GestureRecognizer`1::.cctor()
// 0x000000DB System.Int32 GestureRecognizer::get_RequiredFingerCount()
extern void GestureRecognizer_get_RequiredFingerCount_m7ABD03069ED10AE00CC6B887D511325471667E34 ();
// 0x000000DC System.Void GestureRecognizer::set_RequiredFingerCount(System.Int32)
extern void GestureRecognizer_set_RequiredFingerCount_m44BCB6BFDDE4041D6DBE7CDC274961C8620CA06A ();
// 0x000000DD System.Boolean GestureRecognizer::get_SupportFingerClustering()
extern void GestureRecognizer_get_SupportFingerClustering_m4249320D5A253047A8AF7341D9812A82DEF93FCC ();
// 0x000000DE GestureResetMode GestureRecognizer::GetDefaultResetMode()
extern void GestureRecognizer_GetDefaultResetMode_m5B30A366400FCA14C53F7F4520869005F67D1E01 ();
// 0x000000DF System.String GestureRecognizer::GetDefaultEventMessageName()
// 0x000000E0 System.Type GestureRecognizer::GetGestureType()
// 0x000000E1 System.Void GestureRecognizer::Awake()
extern void GestureRecognizer_Awake_m00596F35A7FD19987F42A6B720ED3DABE8E0EABD ();
// 0x000000E2 System.Void GestureRecognizer::OnEnable()
extern void GestureRecognizer_OnEnable_mB8AE48594DF44F97F3FD77FD9B4B81EC45073C98 ();
// 0x000000E3 System.Void GestureRecognizer::OnDisable()
extern void GestureRecognizer_OnDisable_m4CC14D05A20A94EB45D5DF590143490D47774A59 ();
// 0x000000E4 System.Void GestureRecognizer::Acquire(FingerGestures_Finger)
extern void GestureRecognizer_Acquire_m008F59EA8DAB6BC91C10FE66001FEB2126678AA3 ();
// 0x000000E5 System.Boolean GestureRecognizer::Release(FingerGestures_Finger)
extern void GestureRecognizer_Release_m88D85196319F3BA5B6B1F496572ABE945B18AB92 ();
// 0x000000E6 System.Void GestureRecognizer::Start()
extern void GestureRecognizer_Start_m697DE04C4CB4EE48EA1ED33263076AE6B3F3E01F ();
// 0x000000E7 System.Boolean GestureRecognizer::Young(FingerGestures_IFingerList)
extern void GestureRecognizer_Young_mF6B5A5F15B5493190E1B14FBC29FD2823BF08EBE ();
// 0x000000E8 System.Void GestureRecognizer::.ctor()
extern void GestureRecognizer__ctor_m5FB025E1ED7A59E415725FAD3A19D5C2D3B229F1 ();
// 0x000000E9 System.Void GestureRecognizer::.cctor()
extern void GestureRecognizer__cctor_m3B0B7D1CDF24F40769346D966EB48CBD0937E965 ();
// 0x000000EA System.Boolean GestureRecognizerDelegate::CanBegin(Gesture,FingerGestures_IFingerList)
// 0x000000EB System.Void GestureRecognizerDelegate::.ctor()
extern void GestureRecognizerDelegate__ctor_m81D6C0DF8DE44F191A9D685763BAB1DA02A8B0C9 ();
// 0x000000EC FingerGestures_IFingerList FingerClusterManager::get_FingersAdded()
extern void FingerClusterManager_get_FingersAdded_m64A774645B86E0889AD7130A8EFF6009795D3398 ();
// 0x000000ED FingerGestures_IFingerList FingerClusterManager::get_FingersRemoved()
extern void FingerClusterManager_get_FingersRemoved_m72DF1CC9AC37526ADB3405E78F9E3005903A470A ();
// 0x000000EE System.Collections.Generic.List`1<FingerClusterManager_Cluster> FingerClusterManager::get_Clusters()
extern void FingerClusterManager_get_Clusters_m08F64C3C9C3BE9EA6EE9546EDCB2B5C33A6272D0 ();
// 0x000000EF System.Collections.Generic.List`1<FingerClusterManager_Cluster> FingerClusterManager::GetClustersPool()
extern void FingerClusterManager_GetClustersPool_m00882933206ACEF2F53A74A4D44ED8CE5BA6FA59 ();
// 0x000000F0 System.Void FingerClusterManager::Awake()
extern void FingerClusterManager_Awake_m424B01EB96FEC6149BC5598225E2B47A134BE64C ();
// 0x000000F1 System.Void FingerClusterManager::Update()
extern void FingerClusterManager_Update_m238C149FF6769DD21A1038CAEA090E4D1B526239 ();
// 0x000000F2 FingerClusterManager_Cluster FingerClusterManager::FindClusterById(System.Int32)
extern void FingerClusterManager_FindClusterById_m647FC1A3DF5571E660F1F256AB6320B6A09E3772 ();
// 0x000000F3 FingerClusterManager_Cluster FingerClusterManager::NewCluster()
extern void FingerClusterManager_NewCluster_mE9295A51E2ACE852041A871A57E76C599EBB4C13 ();
// 0x000000F4 FingerClusterManager_Cluster FingerClusterManager::FindExistingCluster(FingerGestures_Finger)
extern void FingerClusterManager_FindExistingCluster_m515BAFC6EC55064CFECDECE3F8CB43DA727C61D2 ();
// 0x000000F5 System.Void FingerClusterManager::.ctor()
extern void FingerClusterManager__ctor_mFC4CD0168F16FAF2CE24C96181C6BC335A921C58 ();
// 0x000000F6 System.Void ScreenRaycaster::Start()
extern void ScreenRaycaster_Start_m75FB098088330DF46BB4D447ECBCA35DC68D4541 ();
// 0x000000F7 System.Boolean ScreenRaycaster::Raycast(UnityEngine.Vector2,UnityEngine.RaycastHit&)
extern void ScreenRaycaster_Raycast_mF4A45FDC6D42EEF03364C2C782E447366FDE423F ();
// 0x000000F8 System.Boolean ScreenRaycaster::Raycast(UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.RaycastHit&)
extern void ScreenRaycaster_Raycast_m796B5A1E3A9105B99279DB4371715580478A4ABC ();
// 0x000000F9 System.Void ScreenRaycaster::.ctor()
extern void ScreenRaycaster__ctor_mDB8E40D02275699EF5DA735E094912D6C1D6BD44 ();
// 0x000000FA System.Void FingerDownEvent::.ctor()
extern void FingerDownEvent__ctor_mD9ACAEE0A8D16A72B959F9CA5DD4F77CE372A80B ();
// 0x000000FB System.Void FingerDownDetector::add_OnFingerDown(FingerEventDetector`1_FingerEventHandler<FingerDownEvent>)
extern void FingerDownDetector_add_OnFingerDown_m1B162A57B8E07EB74ABB7213568EFD7EACC9629B ();
// 0x000000FC System.Void FingerDownDetector::remove_OnFingerDown(FingerEventDetector`1_FingerEventHandler<FingerDownEvent>)
extern void FingerDownDetector_remove_OnFingerDown_m952E1C3B6E5E677611C5B2B67EAD01EFAC89E90E ();
// 0x000000FD System.Void FingerDownDetector::ProcessFinger(FingerGestures_Finger)
extern void FingerDownDetector_ProcessFinger_mD6768D5250DE356595F850F3BEB53D83E37AB40D ();
// 0x000000FE System.Void FingerDownDetector::.ctor()
extern void FingerDownDetector__ctor_m235555C2CC46EE02D8A766696AAACAE3D7519A36 ();
// 0x000000FF FingerHoverPhase FingerHoverEvent::get_Phase()
extern void FingerHoverEvent_get_Phase_m26DD5514C8250F3BE9FC18BC1678AC03E3D1D5C1 ();
// 0x00000100 System.Void FingerHoverEvent::set_Phase(FingerHoverPhase)
extern void FingerHoverEvent_set_Phase_mE06E7A195A3F0A3146057EC0555C48DFF89C02EA ();
// 0x00000101 System.Void FingerHoverEvent::.ctor()
extern void FingerHoverEvent__ctor_m1ADFCD2A285A72A6A20234A69D63BE9F7BE2080D ();
// 0x00000102 System.Void FingerHoverDetector::add_OnFingerHover(FingerEventDetector`1_FingerEventHandler<FingerHoverEvent>)
extern void FingerHoverDetector_add_OnFingerHover_m30014CA27E530AD8A0CB79F8E94CA0B628006553 ();
// 0x00000103 System.Void FingerHoverDetector::remove_OnFingerHover(FingerEventDetector`1_FingerEventHandler<FingerHoverEvent>)
extern void FingerHoverDetector_remove_OnFingerHover_m96B0DEE943C10C6A4C24D36EF8090249593EB42D ();
// 0x00000104 System.Void FingerHoverDetector::Start()
extern void FingerHoverDetector_Start_m0738184BBC93B3A20DDDFF554561C9426D375090 ();
// 0x00000105 System.Boolean FingerHoverDetector::FireEvent(FingerHoverEvent,FingerHoverPhase)
extern void FingerHoverDetector_FireEvent_m39953252FDEB320C36CDA5493C80581DCBAE92C7 ();
// 0x00000106 System.Void FingerHoverDetector::ProcessFinger(FingerGestures_Finger)
extern void FingerHoverDetector_ProcessFinger_mECADEA973B2F127AB319448412AC3031FD15220F ();
// 0x00000107 System.Void FingerHoverDetector::.ctor()
extern void FingerHoverDetector__ctor_mA69CB503A90C9D6754FC07987943DEE17FADBAB5 ();
// 0x00000108 UnityEngine.Vector2 FingerMotionEvent::get_Position()
extern void FingerMotionEvent_get_Position_mEF3A8F9E78557172420442E5801A3325F9053D3F ();
// 0x00000109 System.Void FingerMotionEvent::set_Position(UnityEngine.Vector2)
extern void FingerMotionEvent_set_Position_m8104773CB80C6F1812AD1D5F24BD1C8A3FE257C0 ();
// 0x0000010A FingerMotionPhase FingerMotionEvent::get_Phase()
extern void FingerMotionEvent_get_Phase_mFF72D4C7CB172AD88272C583DC78FA46941489C4 ();
// 0x0000010B System.Void FingerMotionEvent::set_Phase(FingerMotionPhase)
extern void FingerMotionEvent_set_Phase_m4F1E91064274B45EB13CA939193AEF5223DC7747 ();
// 0x0000010C System.Single FingerMotionEvent::get_ElapsedTime()
extern void FingerMotionEvent_get_ElapsedTime_m5D033EBB953A3C38CBE2C50F439A68CE3A571604 ();
// 0x0000010D System.Void FingerMotionEvent::.ctor()
extern void FingerMotionEvent__ctor_m4533C0330FF8EACAF78810C6789D5287BF45E93D ();
// 0x0000010E System.Void FingerMotionDetector::add_OnFingerMove(FingerEventDetector`1_FingerEventHandler<FingerMotionEvent>)
extern void FingerMotionDetector_add_OnFingerMove_m7F044585A615D81F6D2D856C681CED525CFDC8D0 ();
// 0x0000010F System.Void FingerMotionDetector::remove_OnFingerMove(FingerEventDetector`1_FingerEventHandler<FingerMotionEvent>)
extern void FingerMotionDetector_remove_OnFingerMove_m3DF6C48AD287F2D18EBD9449A8BEE56157E24A99 ();
// 0x00000110 System.Void FingerMotionDetector::add_OnFingerStationary(FingerEventDetector`1_FingerEventHandler<FingerMotionEvent>)
extern void FingerMotionDetector_add_OnFingerStationary_m6AF32E1165E8C2E1F7A1524E0D2BD513D81B68AA ();
// 0x00000111 System.Void FingerMotionDetector::remove_OnFingerStationary(FingerEventDetector`1_FingerEventHandler<FingerMotionEvent>)
extern void FingerMotionDetector_remove_OnFingerStationary_mBCF03CE3802E768D9A1F0E301FFC615B0F21C426 ();
// 0x00000112 System.Boolean FingerMotionDetector::FireEvent(FingerMotionEvent,FingerMotionDetector_EventType,FingerMotionPhase,UnityEngine.Vector2,System.Boolean)
extern void FingerMotionDetector_FireEvent_m5981EFFE5CE596D03794F1D1E33427DB8B90F6D8 ();
// 0x00000113 System.Void FingerMotionDetector::ProcessFinger(FingerGestures_Finger)
extern void FingerMotionDetector_ProcessFinger_m58D7DC79FE40C7CEDA3036DAED27CF834027A7CF ();
// 0x00000114 System.Void FingerMotionDetector::.ctor()
extern void FingerMotionDetector__ctor_m459CD226979040E4B88A85C2E1BC37A4ACD30DE3 ();
// 0x00000115 System.Single FingerUpEvent::get_TimeHeldDown()
extern void FingerUpEvent_get_TimeHeldDown_mFE63F1E41B6246E2A343E28EA98639726038F0C6 ();
// 0x00000116 System.Void FingerUpEvent::set_TimeHeldDown(System.Single)
extern void FingerUpEvent_set_TimeHeldDown_mD43D3FB252AAA946F706248AA3228D80773D0760 ();
// 0x00000117 System.Void FingerUpEvent::.ctor()
extern void FingerUpEvent__ctor_mABAABACD34F5584B4BBC91612D16FB631223D1C4 ();
// 0x00000118 System.Void FingerUpDetector::add_OnFingerUp(FingerEventDetector`1_FingerEventHandler<FingerUpEvent>)
extern void FingerUpDetector_add_OnFingerUp_mD7486255141DAEE5D6E4ABD82ADA7CBCA6F9DC6A ();
// 0x00000119 System.Void FingerUpDetector::remove_OnFingerUp(FingerEventDetector`1_FingerEventHandler<FingerUpEvent>)
extern void FingerUpDetector_remove_OnFingerUp_m22110E67D030CEEE256543851388077B1E992369 ();
// 0x0000011A System.Void FingerUpDetector::ProcessFinger(FingerGestures_Finger)
extern void FingerUpDetector_ProcessFinger_m16CA433996A0CFD49A866FBB5FA920605705A954 ();
// 0x0000011B System.Void FingerUpDetector::.ctor()
extern void FingerUpDetector__ctor_mCD3C0E9BC2277A38D04BF42515A34B9D6F3497DC ();
// 0x0000011C System.Void FingerGestures::add_OnGestureEvent(Gesture_EventHandler)
extern void FingerGestures_add_OnGestureEvent_m175F48313462437E1729010E912C30D7F6BCB9E7 ();
// 0x0000011D System.Void FingerGestures::remove_OnGestureEvent(Gesture_EventHandler)
extern void FingerGestures_remove_OnGestureEvent_m6CE8780D51B5644EDF5CA098C422765DBD6F0127 ();
// 0x0000011E System.Void FingerGestures::add_OnFingerEvent(FingerEventDetector`1_FingerEventHandler<FingerEvent>)
extern void FingerGestures_add_OnFingerEvent_mBCA2903A46BCED402F033BBA659FDD970A222422 ();
// 0x0000011F System.Void FingerGestures::remove_OnFingerEvent(FingerEventDetector`1_FingerEventHandler<FingerEvent>)
extern void FingerGestures_remove_OnFingerEvent_mCD635862E8ED7DEFE770CB007247C5E9FBCA6F3F ();
// 0x00000120 System.Void FingerGestures::FireEvent(Gesture)
extern void FingerGestures_FireEvent_mA8ACF4803832091F765824E8EA0FAE2A6B6C7D08 ();
// 0x00000121 System.Void FingerGestures::FireEvent(FingerEvent)
extern void FingerGestures_FireEvent_m30987EAE0E7F87FD14EDB2AB9040B8CE1190098E ();
// 0x00000122 FingerClusterManager FingerGestures::get_DefaultClusterManager()
extern void FingerGestures_get_DefaultClusterManager_m116179EC846E2644E8272B845FFE83551ED15DAD ();
// 0x00000123 FingerGestures FingerGestures::get_Instance()
extern void FingerGestures_get_Instance_mE2F61A1AAF5667E7B63A2BB56A703E3B49F04C5F ();
// 0x00000124 System.Void FingerGestures::Init()
extern void FingerGestures_Init_mC7D69B854C93B2F14CBAF309A4B346931A8E7223 ();
// 0x00000125 FGInputProvider FingerGestures::get_InputProvider()
extern void FingerGestures_get_InputProvider_m551AE2973695CD95C972DB7FCAAD3750AA539CC1 ();
// 0x00000126 System.Void FingerGestures::InitInputProvider()
extern void FingerGestures_InitInputProvider_m7F5A035DC253B5188BDAEC0F935F3F4F3DA05B87 ();
// 0x00000127 System.Void FingerGestures::InstallInputProvider(FGInputProvider)
extern void FingerGestures_InstallInputProvider_m89F3AE714CC350027C74D88A94F4AE4923AA45B9 ();
// 0x00000128 System.Int32 FingerGestures::get_MaxFingers()
extern void FingerGestures_get_MaxFingers_m89660976520038FE5C73D7F57ED52B49340DB70C ();
// 0x00000129 FingerGestures_Finger FingerGestures::GetFinger(System.Int32)
extern void FingerGestures_GetFinger_m31973639437F2C7EAF80C8BC4C0C512C3CBA3A01 ();
// 0x0000012A FingerGestures_IFingerList FingerGestures::get_Touches()
extern void FingerGestures_get_Touches_m95127542603057B589A104547E2BC9920F122493 ();
// 0x0000012B System.Collections.Generic.List`1<GestureRecognizer> FingerGestures::get_RegisteredGestureRecognizers()
extern void FingerGestures_get_RegisteredGestureRecognizers_mF158C9458086C86015392EF69F7AD0A12DA4CE98 ();
// 0x0000012C System.Void FingerGestures::Register(GestureRecognizer)
extern void FingerGestures_Register_m4249E65AE4FE2B2EA5EC22BA921880863D0EAAF3 ();
// 0x0000012D System.Void FingerGestures::Unregister(GestureRecognizer)
extern void FingerGestures_Unregister_mFA7A61E878D0B180202F039D65109903B14F7AAA ();
// 0x0000012E System.Single FingerGestures::get_PixelDistanceScale()
extern void FingerGestures_get_PixelDistanceScale_m301DC059977B8A9392CABFEF8C7812BF2287144E ();
// 0x0000012F System.Void FingerGestures::set_PixelDistanceScale(System.Single)
extern void FingerGestures_set_PixelDistanceScale_mB4C2B0B71128F396E3A87E119A8DB59A0FCF42DE ();
// 0x00000130 System.Single FingerGestures::GetAdjustedPixelDistance(System.Single)
extern void FingerGestures_GetAdjustedPixelDistance_m91D4DA29029E419EFFAA33955EF35A07C68A0095 ();
// 0x00000131 System.Boolean FingerGestures::IsRetinaDisplay()
extern void FingerGestures_IsRetinaDisplay_m83083C3AE7EA3B637D22D13ABEE4093158E2D1D6 ();
// 0x00000132 System.Void FingerGestures::Awake()
extern void FingerGestures_Awake_m667D5573FE39A3A725986EC746F8DD2646A6A6EE ();
// 0x00000133 System.Void FingerGestures::Start()
extern void FingerGestures_Start_m65727939A0C0A8AEA5A020964026C74F7AA22C8E ();
// 0x00000134 System.Void FingerGestures::OnEnable()
extern void FingerGestures_OnEnable_m682EC64C3473F2C6F84E545F4F8AC60D67644256 ();
// 0x00000135 System.Void FingerGestures::CheckInit()
extern void FingerGestures_CheckInit_mEA07FEF4A7672FFB01B4EC0E9FFE7E231E446E8A ();
// 0x00000136 System.Void FingerGestures::Update()
extern void FingerGestures_Update_m6A7471C4A25541BF8C8423CF9E8E2A68B454274C ();
// 0x00000137 System.Void FingerGestures::InitFingers(System.Int32)
extern void FingerGestures_InitFingers_m671C1EB570BADA913CA264D93F1D3E698AE7A926 ();
// 0x00000138 System.Void FingerGestures::UpdateFingers()
extern void FingerGestures_UpdateFingers_mD6443E0424F81E763FE7C631E130A467E3DBD98C ();
// 0x00000139 FingerGestures_GlobalTouchFilterDelegate FingerGestures::get_GlobalTouchFilter()
extern void FingerGestures_get_GlobalTouchFilter_mD6708903C9AD39C6EB2C3067A42F6A3EBD33C9B2 ();
// 0x0000013A System.Void FingerGestures::set_GlobalTouchFilter(FingerGestures_GlobalTouchFilterDelegate)
extern void FingerGestures_set_GlobalTouchFilter_m3AE82207325DAA013C2569515E947CC485CD5E45 ();
// 0x0000013B System.Boolean FingerGestures::ShouldProcessTouch(System.Int32,UnityEngine.Vector2)
extern void FingerGestures_ShouldProcessTouch_m8EE06423B868044FF957B6AECF53AEC5CFB95C28 ();
// 0x0000013C UnityEngine.Transform FingerGestures::CreateNode(System.String,UnityEngine.Transform)
extern void FingerGestures_CreateNode_m0279934D0ACA132DBFF5A08F69B73B09EBF34FCB ();
// 0x0000013D System.Void FingerGestures::InitNodes()
extern void FingerGestures_InitNodes_m91E26EC3CCBE437C25333C96FF89FB59A9C7898A ();
// 0x0000013E FingerGestures_SwipeDirection FingerGestures::GetSwipeDirection(UnityEngine.Vector2,System.Single)
extern void FingerGestures_GetSwipeDirection_m4DF3DB16CDFF9C4418750A49F76F78766B52C74A ();
// 0x0000013F FingerGestures_SwipeDirection FingerGestures::GetSwipeDirection(UnityEngine.Vector2)
extern void FingerGestures_GetSwipeDirection_m7F39D1534066FD219F10F87A3FAFAC6584487C3B ();
// 0x00000140 System.Boolean FingerGestures::UsingUnityRemote()
extern void FingerGestures_UsingUnityRemote_m26928C0B1869129174A505EB72DF6F2650F05E04 ();
// 0x00000141 System.Boolean FingerGestures::AllFingersMoving(FingerGestures_Finger[])
extern void FingerGestures_AllFingersMoving_m76A80EE1817671AD55065EC255736FF9AEE849C8 ();
// 0x00000142 System.Boolean FingerGestures::FingersMovedInOppositeDirections(FingerGestures_Finger,FingerGestures_Finger,System.Single)
extern void FingerGestures_FingersMovedInOppositeDirections_m667304A1A8774995D8D965FCB254B4D89EA3F5D3 ();
// 0x00000143 System.Single FingerGestures::SignedAngle(UnityEngine.Vector2,UnityEngine.Vector2)
extern void FingerGestures_SignedAngle_m4FF40555C84A4382AABD859DFC66CDA234873413 ();
// 0x00000144 System.Single FingerGestures::NormalizeAngle360(System.Single)
extern void FingerGestures_NormalizeAngle360_mB352E47FCB7FC839152375C4CBCE25286538E86D ();
// 0x00000145 System.Void FingerGestures::.ctor()
extern void FingerGestures__ctor_mBFB9C1508BE9F31D19C5AC3364341E2C387943BF ();
// 0x00000146 System.Void FingerGestures::.cctor()
extern void FingerGestures__cctor_m9635E81EB98795A1EDDC4BBC546C207175EBDFF6 ();
// 0x00000147 UnityEngine.Vector2 DragGesture::get_DeltaMove()
extern void DragGesture_get_DeltaMove_m9B2C10ED529DF57AF88CBAAD6F6A99F973324675 ();
// 0x00000148 System.Void DragGesture::set_DeltaMove(UnityEngine.Vector2)
extern void DragGesture_set_DeltaMove_m1A7791F7E0AD28484C111F82A364485119BBA7E1 ();
// 0x00000149 UnityEngine.Vector2 DragGesture::get_TotalMove()
extern void DragGesture_get_TotalMove_m7B50ACD0110AC96E623FE58E40886C26AFBB0236 ();
// 0x0000014A System.Void DragGesture::.ctor()
extern void DragGesture__ctor_m2900563CF403AFE17F3C5B8C8D5411D0A5B1C169 ();
// 0x0000014B System.String DragRecognizer::GetDefaultEventMessageName()
extern void DragRecognizer_GetDefaultEventMessageName_m8861084A55C8C1C144A43D3F545813E0DDE25355 ();
// 0x0000014C UnityEngine.GameObject DragRecognizer::GetDefaultSelectionForSendMessage(DragGesture)
extern void DragRecognizer_GetDefaultSelectionForSendMessage_mFC999ACE5DB19A0738CB4BB5F0E0F38BA15FD296 ();
// 0x0000014D System.Boolean DragRecognizer::CanBegin(DragGesture,FingerGestures_IFingerList)
extern void DragRecognizer_CanBegin_m93428A47F8DF29254A3383378204BA7E36AA5AE9 ();
// 0x0000014E System.Void DragRecognizer::OnBegin(DragGesture,FingerGestures_IFingerList)
extern void DragRecognizer_OnBegin_m5FCDEE81E98DA278EA7AAA40D5E4C0428227704F ();
// 0x0000014F GestureRecognitionState DragRecognizer::OnRecognize(DragGesture,FingerGestures_IFingerList)
extern void DragRecognizer_OnRecognize_m2EC57B28312197FE0C0B403B3181CD643A13994E ();
// 0x00000150 System.Void DragRecognizer::.ctor()
extern void DragRecognizer__ctor_mF6A590FE0A7D237B87D3E1FA952911F8F5E48303 ();
// 0x00000151 System.Void LongPressGesture::.ctor()
extern void LongPressGesture__ctor_mDFF7C43856BA7605293A45DAA647FD67476B71C4 ();
// 0x00000152 System.String LongPressRecognizer::GetDefaultEventMessageName()
extern void LongPressRecognizer_GetDefaultEventMessageName_mE5B190E04FBA7E738643168887C9F76FDDC00739 ();
// 0x00000153 System.Void LongPressRecognizer::OnBegin(LongPressGesture,FingerGestures_IFingerList)
extern void LongPressRecognizer_OnBegin_m00DFB111D17758B2092257BB6EFFB439E1E94E8D ();
// 0x00000154 GestureRecognitionState LongPressRecognizer::OnRecognize(LongPressGesture,FingerGestures_IFingerList)
extern void LongPressRecognizer_OnRecognize_m9D30AD1EDA93424F8A0C8B81ABFAA38F8D3F5AD3 ();
// 0x00000155 System.Void LongPressRecognizer::.ctor()
extern void LongPressRecognizer__ctor_m3FAC68AEB35D8C3A79A4043A7E68DA8929CE7BF5 ();
// 0x00000156 System.Single PinchGesture::get_Delta()
extern void PinchGesture_get_Delta_m3F973E40169267AEF1DE08EF9185B48732BD205E ();
// 0x00000157 System.Void PinchGesture::set_Delta(System.Single)
extern void PinchGesture_set_Delta_m4BD3AC3922C0C6D65B4407075FAD83E8BAB05B71 ();
// 0x00000158 System.Single PinchGesture::get_Gap()
extern void PinchGesture_get_Gap_m8575E5A83FECA30D79F54340978705FA09BC6297 ();
// 0x00000159 System.Void PinchGesture::set_Gap(System.Single)
extern void PinchGesture_set_Gap_m10EA81A126AD6BF97900F42B1B3C71B85A4CC16F ();
// 0x0000015A System.Void PinchGesture::.ctor()
extern void PinchGesture__ctor_m92CFB9CAD3CC7F3100C877E62E7D142606649CC6 ();
// 0x0000015B System.String PinchRecognizer::GetDefaultEventMessageName()
extern void PinchRecognizer_GetDefaultEventMessageName_m5154827A0C018351BA84E964C9CC1BF26E0D4947 ();
// 0x0000015C System.Int32 PinchRecognizer::get_RequiredFingerCount()
extern void PinchRecognizer_get_RequiredFingerCount_m736926BAED270C7702FFCFDF7BB1B84F925138C5 ();
// 0x0000015D System.Void PinchRecognizer::set_RequiredFingerCount(System.Int32)
extern void PinchRecognizer_set_RequiredFingerCount_mB44371CD36EB70C0B644603E4DFD270B3B008B61 ();
// 0x0000015E System.Boolean PinchRecognizer::get_SupportFingerClustering()
extern void PinchRecognizer_get_SupportFingerClustering_m9FAF3EB463798B073695ADD8611C2FDF78603061 ();
// 0x0000015F UnityEngine.GameObject PinchRecognizer::GetDefaultSelectionForSendMessage(PinchGesture)
extern void PinchRecognizer_GetDefaultSelectionForSendMessage_mF424A8D369D7D967D0D88C2C44DDAA24725C076D ();
// 0x00000160 GestureResetMode PinchRecognizer::GetDefaultResetMode()
extern void PinchRecognizer_GetDefaultResetMode_m1ECD3ED0E6DD2AC09564B5CB2217AEDCBE32A071 ();
// 0x00000161 System.Boolean PinchRecognizer::CanBegin(PinchGesture,FingerGestures_IFingerList)
extern void PinchRecognizer_CanBegin_m4253A2ED737BBBF1276FABB0B1D512E74B0BFCD7 ();
// 0x00000162 System.Void PinchRecognizer::OnBegin(PinchGesture,FingerGestures_IFingerList)
extern void PinchRecognizer_OnBegin_mBF2B94E6CDDBB325F957CBE0D851BCA13ABC064E ();
// 0x00000163 GestureRecognitionState PinchRecognizer::OnRecognize(PinchGesture,FingerGestures_IFingerList)
extern void PinchRecognizer_OnRecognize_mD9D0CAB0B8F6DCC935901EE1257D53E7E9A9B327 ();
// 0x00000164 System.Boolean PinchRecognizer::FingersMovedInOppositeDirections(FingerGestures_Finger,FingerGestures_Finger)
extern void PinchRecognizer_FingersMovedInOppositeDirections_m87DEEA502E1E4DCA8F991C5852EE999E453418F8 ();
// 0x00000165 System.Void PinchRecognizer::.ctor()
extern void PinchRecognizer__ctor_mD0D0B390BAE8C8486305AAAC5C6A0CDF8BF75BE6 ();
// 0x00000166 UnityEngine.Vector2 PointCloudGestureTemplate::get_Size()
extern void PointCloudGestureTemplate_get_Size_m337FDD55BE500A4259FC5D2D5A0B721A3F864816 ();
// 0x00000167 System.Single PointCloudGestureTemplate::get_Width()
extern void PointCloudGestureTemplate_get_Width_m5A05CABFAB4B08272A15596B603E3F2252BB7EAE ();
// 0x00000168 System.Single PointCloudGestureTemplate::get_Height()
extern void PointCloudGestureTemplate_get_Height_m001C49AEB5E7FAC67E9CA38DA29942D9959FCF38 ();
// 0x00000169 System.Void PointCloudGestureTemplate::BeginPoints()
extern void PointCloudGestureTemplate_BeginPoints_m50BBB4985D90BB3768BE50EE099CD6AF6DD1F224 ();
// 0x0000016A System.Void PointCloudGestureTemplate::AddPoint(System.Int32,UnityEngine.Vector2)
extern void PointCloudGestureTemplate_AddPoint_m140EA38D0DBFADA8322AECAE60AF6E3F9B877889 ();
// 0x0000016B System.Void PointCloudGestureTemplate::AddPoint(System.Int32,System.Single,System.Single)
extern void PointCloudGestureTemplate_AddPoint_m89399C519F441E5F0C311BEC2B0AA52129CBCC10 ();
// 0x0000016C System.Void PointCloudGestureTemplate::EndPoints()
extern void PointCloudGestureTemplate_EndPoints_mC498FB3B5B91F5971BB4793090D6C1C704F285D7 ();
// 0x0000016D UnityEngine.Vector2 PointCloudGestureTemplate::GetPosition(System.Int32)
extern void PointCloudGestureTemplate_GetPosition_m938D33114BADFE680A1DD73B4C900AB7FF2DECD3 ();
// 0x0000016E System.Int32 PointCloudGestureTemplate::GetStrokeId(System.Int32)
extern void PointCloudGestureTemplate_GetStrokeId_m6C9340D0D27B1D644022A5EAFFAC26971FC69AEA ();
// 0x0000016F System.Int32 PointCloudGestureTemplate::get_PointCount()
extern void PointCloudGestureTemplate_get_PointCount_m549D4C77EFF3DF6EAEAFA127B568576292239B4C ();
// 0x00000170 System.Int32 PointCloudGestureTemplate::get_StrokeCount()
extern void PointCloudGestureTemplate_get_StrokeCount_m69E0387C81498E8AD5DC4F227F809D4E267CA235 ();
// 0x00000171 System.Void PointCloudGestureTemplate::Normalize()
extern void PointCloudGestureTemplate_Normalize_m3702E2B37ED55066DA04A6A0B6C97C997EC50ABA ();
// 0x00000172 System.Void PointCloudGestureTemplate::MakeDirty()
extern void PointCloudGestureTemplate_MakeDirty_m2DC9117EB702324C36F53882FFA9A61AD7FDCA47 ();
// 0x00000173 System.Void PointCloudGestureTemplate::.ctor()
extern void PointCloudGestureTemplate__ctor_m58E53D53E67621EC57DD65248368DCDCAC7D10B0 ();
// 0x00000174 System.Void PointCloudGesture::.ctor()
extern void PointCloudGesture__ctor_m8CD392B4A8A348DA065FEB53D804FD2C47F3E12D ();
// 0x00000175 System.Void PointCloudRegognizer::Awake()
extern void PointCloudRegognizer_Awake_mC9F9C4201BD3F6797BCD75EECFAD63FEBCE025ED ();
// 0x00000176 PointCloudRegognizer_NormalizedTemplate PointCloudRegognizer::FindNormalizedTemplate(PointCloudGestureTemplate)
extern void PointCloudRegognizer_FindNormalizedTemplate_m036F01EC02E18A4FAD3002DFF24DC38CBB7A4042 ();
// 0x00000177 System.Collections.Generic.List`1<PointCloudRegognizer_Point> PointCloudRegognizer::Normalize(System.Collections.Generic.List`1<PointCloudRegognizer_Point>)
extern void PointCloudRegognizer_Normalize_mF0A3E6A84AD5F4EA416582967984AC7B2FA86912 ();
// 0x00000178 System.Boolean PointCloudRegognizer::AddTemplate(PointCloudGestureTemplate)
extern void PointCloudRegognizer_AddTemplate_m1872E96AF226C154452D078C51E6F2C2A528EB94 ();
// 0x00000179 System.Void PointCloudRegognizer::OnBegin(PointCloudGesture,FingerGestures_IFingerList)
extern void PointCloudRegognizer_OnBegin_m4E595483C8ADA88E24EC8B89EAD5C1BC03240DBD ();
// 0x0000017A System.Boolean PointCloudRegognizer::RecognizePointCloud(PointCloudGesture)
extern void PointCloudRegognizer_RecognizePointCloud_m29DDCD5857EFA4905D4DD9AAB728A5CEEAA73D39 ();
// 0x0000017B System.Single PointCloudRegognizer::GreedyCloudMatch(System.Collections.Generic.List`1<PointCloudRegognizer_Point>,System.Collections.Generic.List`1<PointCloudRegognizer_Point>)
extern void PointCloudRegognizer_GreedyCloudMatch_mF1697E963AA8DE3FBA1F81D0FA022AA93EB768E9 ();
// 0x0000017C System.Single PointCloudRegognizer::CloudDistance(System.Collections.Generic.List`1<PointCloudRegognizer_Point>,System.Collections.Generic.List`1<PointCloudRegognizer_Point>,System.Int32)
extern void PointCloudRegognizer_CloudDistance_m9475728A534487A61AF082B80BE1A34F82114059 ();
// 0x0000017D System.Void PointCloudRegognizer::ResetMatched(System.Int32)
extern void PointCloudRegognizer_ResetMatched_m7934FD8DE2E96B57BCBE31BFCB049633F2A8364D ();
// 0x0000017E GestureRecognitionState PointCloudRegognizer::OnRecognize(PointCloudGesture,FingerGestures_IFingerList)
extern void PointCloudRegognizer_OnRecognize_m126181FE25A17ECCFF816FC5F71FBAE21C6A233E ();
// 0x0000017F System.String PointCloudRegognizer::GetDefaultEventMessageName()
extern void PointCloudRegognizer_GetDefaultEventMessageName_mF888F346E486CB90033DEDCC95FB4E2E9123209D ();
// 0x00000180 System.Void PointCloudRegognizer::OnDrawGizmosSelected()
extern void PointCloudRegognizer_OnDrawGizmosSelected_mB3579B73D28399946856A036B35434FFB283A209 ();
// 0x00000181 System.Void PointCloudRegognizer::DrawNormalizedPointCloud(System.Collections.Generic.List`1<PointCloudRegognizer_Point>,System.Single)
extern void PointCloudRegognizer_DrawNormalizedPointCloud_m56640ACA058E42D8FEB3B22DD2EA6A313CBC10D4 ();
// 0x00000182 System.Void PointCloudRegognizer::.ctor()
extern void PointCloudRegognizer__ctor_m2445BF923392FBDDACBDBAD89CE1937E92C426B9 ();
// 0x00000183 System.Void PointCloudRegognizer::.cctor()
extern void PointCloudRegognizer__cctor_m2753FBBCB2FB76239076DBE49B32AF404B911084 ();
// 0x00000184 UnityEngine.Vector2 SwipeGesture::get_Move()
extern void SwipeGesture_get_Move_m87F0CA1FF3580E08C7B94803463CB3BDEB314CC2 ();
// 0x00000185 System.Void SwipeGesture::set_Move(UnityEngine.Vector2)
extern void SwipeGesture_set_Move_m8819B56852D396F222DF59B14A2D040E1830978D ();
// 0x00000186 System.Single SwipeGesture::get_Velocity()
extern void SwipeGesture_get_Velocity_mE51E8A90D189DA02E4EA59A9888F43865A6F8B70 ();
// 0x00000187 System.Void SwipeGesture::set_Velocity(System.Single)
extern void SwipeGesture_set_Velocity_mDC5D7E935129C5F630FAD38E5FED216DCBCDE8C3 ();
// 0x00000188 FingerGestures_SwipeDirection SwipeGesture::get_Direction()
extern void SwipeGesture_get_Direction_mB02F8DA8135F5B44D56E356209A58208FD4823A2 ();
// 0x00000189 System.Void SwipeGesture::set_Direction(FingerGestures_SwipeDirection)
extern void SwipeGesture_set_Direction_m664F31467D42FC6780DD1486F10E8B1EC6ABB0DD ();
// 0x0000018A System.Void SwipeGesture::.ctor()
extern void SwipeGesture__ctor_m89B3AEB64F14D123190648425B06FBEC67C8E828 ();
// 0x0000018B System.String SwipeRecognizer::GetDefaultEventMessageName()
extern void SwipeRecognizer_GetDefaultEventMessageName_mF1572A6E8CE27DA433C2E108A5D1158535413473 ();
// 0x0000018C System.Boolean SwipeRecognizer::CanBegin(SwipeGesture,FingerGestures_IFingerList)
extern void SwipeRecognizer_CanBegin_mB572BD35BD0F9A7738FA9CFFBD5C8FCB52685302 ();
// 0x0000018D System.Void SwipeRecognizer::OnBegin(SwipeGesture,FingerGestures_IFingerList)
extern void SwipeRecognizer_OnBegin_mB3F4C79F7560443F3323DA438E0B74F86D08ACF0 ();
// 0x0000018E GestureRecognitionState SwipeRecognizer::OnRecognize(SwipeGesture,FingerGestures_IFingerList)
extern void SwipeRecognizer_OnRecognize_m1DD0AC54837974FD4E183D6F76D73C0D3A7FA521 ();
// 0x0000018F System.Boolean SwipeRecognizer::IsValidDirection(FingerGestures_SwipeDirection)
extern void SwipeRecognizer_IsValidDirection_mF8CD160088F6BD8CF7293C2972724DB99EC7D3F9 ();
// 0x00000190 System.Void SwipeRecognizer::.ctor()
extern void SwipeRecognizer__ctor_mA935A13E1C9C5ADE31E334C01446CCF8E15A035A ();
// 0x00000191 System.Int32 TapGesture::get_Taps()
extern void TapGesture_get_Taps_mCBD2C2DB8CAA419F7625712C568847A1436C80B3 ();
// 0x00000192 System.Void TapGesture::set_Taps(System.Int32)
extern void TapGesture_set_Taps_mA3AF859A743F9EBCDC398E8B446E0923E9401CE1 ();
// 0x00000193 System.Void TapGesture::.ctor()
extern void TapGesture__ctor_mD600935C90B4DD008F532942E401D2AD6E59B01F ();
// 0x00000194 System.Boolean TapRecognizer::get_IsMultiTap()
extern void TapRecognizer_get_IsMultiTap_mDEE38EC01EC6DC25BD6B42604C293F1C2710B8B0 ();
// 0x00000195 System.Boolean TapRecognizer::HasTimedOut(TapGesture)
extern void TapRecognizer_HasTimedOut_m8B4D22962E897064DE34895D61FE9EAEF4730E72 ();
// 0x00000196 System.Void TapRecognizer::Reset(TapGesture)
extern void TapRecognizer_Reset_m6663B7BBF33B956A30219019542049F0ADE4329D ();
// 0x00000197 TapGesture TapRecognizer::MatchActiveGestureToCluster(FingerClusterManager_Cluster)
extern void TapRecognizer_MatchActiveGestureToCluster_mA5E92A4A30186DEA561C589A77E42F4C8C3E01A3 ();
// 0x00000198 TapGesture TapRecognizer::FindClosestPendingGesture(UnityEngine.Vector2)
extern void TapRecognizer_FindClosestPendingGesture_m4B664938134EF27D3D5B20EB5996F86247E90D12 ();
// 0x00000199 GestureRecognitionState TapRecognizer::RecognizeSingleTap(TapGesture,FingerGestures_IFingerList)
extern void TapRecognizer_RecognizeSingleTap_m13BA3F3BB83050A77BC82B2B78D8C427B2EEE14F ();
// 0x0000019A GestureRecognitionState TapRecognizer::RecognizeMultiTap(TapGesture,FingerGestures_IFingerList)
extern void TapRecognizer_RecognizeMultiTap_m985A724143377F014F210D17935D1FCC85244C24 ();
// 0x0000019B System.String TapRecognizer::GetDefaultEventMessageName()
extern void TapRecognizer_GetDefaultEventMessageName_m16B07946FAD2560920A71BD8D76AACABBC87A5C1 ();
// 0x0000019C System.Void TapRecognizer::OnBegin(TapGesture,FingerGestures_IFingerList)
extern void TapRecognizer_OnBegin_mEE739FA1361DC4E287D4BEE21574607AFDCF7899 ();
// 0x0000019D GestureRecognitionState TapRecognizer::OnRecognize(TapGesture,FingerGestures_IFingerList)
extern void TapRecognizer_OnRecognize_m65D4A5631B638BAA1788F5DB714CE4EDB4B1FB02 ();
// 0x0000019E System.Void TapRecognizer::.ctor()
extern void TapRecognizer__ctor_m7F346866560018BF0286F66D6DE313614D2F863E ();
// 0x0000019F System.Single TwistGesture::get_DeltaRotation()
extern void TwistGesture_get_DeltaRotation_m3D7F3162B11DE8BB804284C4858450ED88D2E234 ();
// 0x000001A0 System.Void TwistGesture::set_DeltaRotation(System.Single)
extern void TwistGesture_set_DeltaRotation_m416B0DDE1E3DB369F3603DA9D158C94669D20F77 ();
// 0x000001A1 System.Single TwistGesture::get_TotalRotation()
extern void TwistGesture_get_TotalRotation_m9D341DE733C95B8D7A7490CEFA00AD9C9BF91CBC ();
// 0x000001A2 System.Void TwistGesture::set_TotalRotation(System.Single)
extern void TwistGesture_set_TotalRotation_m1D905E69C5D3B7507859E64C94BEAD1AE7C9F637 ();
// 0x000001A3 System.Void TwistGesture::.ctor()
extern void TwistGesture__ctor_m76A30A059C2D659C835C1A4790DA4EB8DF568170 ();
// 0x000001A4 System.String TwistRecognizer::GetDefaultEventMessageName()
extern void TwistRecognizer_GetDefaultEventMessageName_mF7786153A5B741E37848E587DBA2F39034E6C644 ();
// 0x000001A5 System.Int32 TwistRecognizer::get_RequiredFingerCount()
extern void TwistRecognizer_get_RequiredFingerCount_m1C2030C2C36DC8DDEED826904CBC55FE73B32493 ();
// 0x000001A6 System.Void TwistRecognizer::set_RequiredFingerCount(System.Int32)
extern void TwistRecognizer_set_RequiredFingerCount_m0238672B86CB924044FEB96F6B5ADFAB8DC709A0 ();
// 0x000001A7 System.Boolean TwistRecognizer::get_SupportFingerClustering()
extern void TwistRecognizer_get_SupportFingerClustering_m647C994A421BC4B60F1674B92B0564E96ECFD8C8 ();
// 0x000001A8 GestureResetMode TwistRecognizer::GetDefaultResetMode()
extern void TwistRecognizer_GetDefaultResetMode_mF066D28750B6FA5E486584434F15E23924C8FD7A ();
// 0x000001A9 UnityEngine.GameObject TwistRecognizer::GetDefaultSelectionForSendMessage(TwistGesture)
extern void TwistRecognizer_GetDefaultSelectionForSendMessage_mB68E56C00F506BF0281914A9250FEA62BEF36E38 ();
// 0x000001AA System.Boolean TwistRecognizer::CanBegin(TwistGesture,FingerGestures_IFingerList)
extern void TwistRecognizer_CanBegin_m9F5ADD27AE0C1A65E6D6BDD242D73E62CBB6047B ();
// 0x000001AB System.Void TwistRecognizer::OnBegin(TwistGesture,FingerGestures_IFingerList)
extern void TwistRecognizer_OnBegin_mE7C52F0A5B8676E76F0219F3276847BE8C7945D1 ();
// 0x000001AC GestureRecognitionState TwistRecognizer::OnRecognize(TwistGesture,FingerGestures_IFingerList)
extern void TwistRecognizer_OnRecognize_m006006723683B0DFB53AF9FB04EFA41BD568BDB4 ();
// 0x000001AD System.Boolean TwistRecognizer::FingersMovedInOppositeDirections(FingerGestures_Finger,FingerGestures_Finger)
extern void TwistRecognizer_FingersMovedInOppositeDirections_mE1669F2E73404178083495D10D961C67B3458FF8 ();
// 0x000001AE System.Single TwistRecognizer::SignedAngularGap(FingerGestures_Finger,FingerGestures_Finger,UnityEngine.Vector2,UnityEngine.Vector2)
extern void TwistRecognizer_SignedAngularGap_m41B87BFF693B4DF4B3D949B493C38045099DF6A8 ();
// 0x000001AF System.Void TwistRecognizer::.ctor()
extern void TwistRecognizer__ctor_m77C8BF5B07685C6D3E94D2BCDACC01ECC43101EA ();
// 0x000001B0 System.Int32 FGInputProvider::get_MaxSimultaneousFingers()
// 0x000001B1 System.Void FGInputProvider::GetInputState(System.Int32,System.Boolean&,UnityEngine.Vector2&)
// 0x000001B2 System.Void FGInputProvider::.ctor()
extern void FGInputProvider__ctor_m0BB11FB49FE35A586A2B9275C5624240056C86B6 ();
// 0x000001B3 System.Void FGMouseInputProvider::Start()
extern void FGMouseInputProvider_Start_m905BBC6204C1566F7CD5ADF8341C0F4128FC436F ();
// 0x000001B4 System.Void FGMouseInputProvider::Update()
extern void FGMouseInputProvider_Update_mFD367ED3881C1768A5D258DED84980C8F7828DFB ();
// 0x000001B5 System.Void FGMouseInputProvider::UpdatePinchEmulation()
extern void FGMouseInputProvider_UpdatePinchEmulation_m4C666C33FE4404223BB63C8D862ABDDEFC67851E ();
// 0x000001B6 System.Void FGMouseInputProvider::UpdateTwistEmulation()
extern void FGMouseInputProvider_UpdateTwistEmulation_mD0161333C22AA1C4D20BEE31C2C01CC3A2A2E0B9 ();
// 0x000001B7 System.Int32 FGMouseInputProvider::get_MaxSimultaneousFingers()
extern void FGMouseInputProvider_get_MaxSimultaneousFingers_m0642411762D1BB27B109DAFAE625C28BC4D57E7D ();
// 0x000001B8 System.Void FGMouseInputProvider::GetInputState(System.Int32,System.Boolean&,UnityEngine.Vector2&)
extern void FGMouseInputProvider_GetInputState_m0B318531156548529CD1640AAE4FBF30929C2B9A ();
// 0x000001B9 System.Void FGMouseInputProvider::.ctor()
extern void FGMouseInputProvider__ctor_m76B5B0E3B61036A2ECF24FD8B4BD9653FA881F53 ();
// 0x000001BA System.Void FGTouchInputProvider::Start()
extern void FGTouchInputProvider_Start_mCA913CE3D5DADA55A61530B3D137708850572F7F ();
// 0x000001BB System.Void FGTouchInputProvider::Update()
extern void FGTouchInputProvider_Update_m908551835F013DE2826B754DA6FF59E8A7700ABD ();
// 0x000001BC System.Void FGTouchInputProvider::UpdateFingerTouchMap()
extern void FGTouchInputProvider_UpdateFingerTouchMap_m46687B0EDDD6EF4830E533101115668F6DA5371D ();
// 0x000001BD System.Boolean FGTouchInputProvider::HasValidTouch(System.Int32)
extern void FGTouchInputProvider_HasValidTouch_m7A4CFA0D3837102BEAB4E44F9D40DBA02EABDCBC ();
// 0x000001BE UnityEngine.Touch FGTouchInputProvider::GetTouch(System.Int32)
extern void FGTouchInputProvider_GetTouch_mC2247AD7AF212AC9AFD75FA8C8E5EB1026FECEB4 ();
// 0x000001BF System.Int32 FGTouchInputProvider::get_MaxSimultaneousFingers()
extern void FGTouchInputProvider_get_MaxSimultaneousFingers_m8AE4B4CB0868A41565C657DA28C05A75C801FD96 ();
// 0x000001C0 System.Void FGTouchInputProvider::GetInputState(System.Int32,System.Boolean&,UnityEngine.Vector2&)
extern void FGTouchInputProvider_GetInputState_m316A117F95A6D240EA3451CC58AC8561F65FFAC1 ();
// 0x000001C1 System.Void FGTouchInputProvider::.ctor()
extern void FGTouchInputProvider__ctor_mEE4BE4785CFE15000D5CEF84C543283D71C0676C ();
// 0x000001C2 System.Void TBDragView::Awake()
extern void TBDragView_Awake_m71B00EAACF4F25E4AB0F5AA11B2ACE997AC0D920 ();
// 0x000001C3 System.Void TBDragView::Start()
extern void TBDragView_Start_mDA5B612E5740256BFC0BCCA56FE0D3EDB2A87803 ();
// 0x000001C4 System.Boolean TBDragView::get_Dragging()
extern void TBDragView_get_Dragging_m8C0F6C674CE33DDFD52F94A472C0B04E70B3AF51 ();
// 0x000001C5 System.Void TBDragView::OnDrag(DragGesture)
extern void TBDragView_OnDrag_mFAC7122976333520D30800B137C4FD69CCFF430F ();
// 0x000001C6 System.Void TBDragView::Update()
extern void TBDragView_Update_m1FBD10BCE16C5044334894C9F630FF298FB03783 ();
// 0x000001C7 System.Single TBDragView::NormalizePitch(System.Single)
extern void TBDragView_NormalizePitch_m9DB1CFF16B8C98E6FD4DA681FE747374294BB8F7 ();
// 0x000001C8 UnityEngine.Quaternion TBDragView::get_IdealRotation()
extern void TBDragView_get_IdealRotation_m2AEFD72BA646B2DB31BF9799545A999D4B394468 ();
// 0x000001C9 System.Void TBDragView::set_IdealRotation(UnityEngine.Quaternion)
extern void TBDragView_set_IdealRotation_mEA8959FD0E2728EEAC69275426068EBAA536D208 ();
// 0x000001CA System.Void TBDragView::LookAt(UnityEngine.Vector3)
extern void TBDragView_LookAt_m03BA7D1BCD6FF4B2BB3808BDF26D2FB8FB5669F9 ();
// 0x000001CB System.Void TBDragView::.ctor()
extern void TBDragView__ctor_m0D6A5A9260EB34ACEEF238CAE090B45469C6BA86 ();
// 0x000001CC System.Void TBLookAtTap::Awake()
extern void TBLookAtTap_Awake_m5A7AD67EDD1ABE879950E82951B89CDB0BCDA824 ();
// 0x000001CD System.Void TBLookAtTap::Start()
extern void TBLookAtTap_Start_m3212F2DABE340BCFD9F275FEA65810F65333D611 ();
// 0x000001CE System.Void TBLookAtTap::OnTap(TapGesture)
extern void TBLookAtTap_OnTap_mDD8F1BFE530E4BBFE6A454E7B22BC294E05F7C6A ();
// 0x000001CF System.Void TBLookAtTap::.ctor()
extern void TBLookAtTap__ctor_mA65E9D8A4FC111A2B8553CAF416D4EDC9D5114F3 ();
// 0x000001D0 System.Single TBOrbit::get_Distance()
extern void TBOrbit_get_Distance_mE5FE085580F69F74152919D02B703955DE909C03 ();
// 0x000001D1 System.Single TBOrbit::get_IdealDistance()
extern void TBOrbit_get_IdealDistance_m6BCA715D54213C4C3914F4C1B7712C50FCCE835B ();
// 0x000001D2 System.Void TBOrbit::set_IdealDistance(System.Single)
extern void TBOrbit_set_IdealDistance_m7FCF141D84D04B66F0E5C836224DC98D7B5A0D4D ();
// 0x000001D3 System.Single TBOrbit::get_Yaw()
extern void TBOrbit_get_Yaw_mD04B127E5FD5CABA4A1D822014C2F04CBD94609D ();
// 0x000001D4 System.Single TBOrbit::get_IdealYaw()
extern void TBOrbit_get_IdealYaw_m212683D73343105A3435A5B6C864A36B4CEC9FD5 ();
// 0x000001D5 System.Void TBOrbit::set_IdealYaw(System.Single)
extern void TBOrbit_set_IdealYaw_m3852EC8F27C486DA0C578E4B3F84A9CEA985663F ();
// 0x000001D6 System.Single TBOrbit::get_Pitch()
extern void TBOrbit_get_Pitch_mA932AA3B3197DB3ADD3C00D2748B483FEA84FB55 ();
// 0x000001D7 System.Single TBOrbit::get_IdealPitch()
extern void TBOrbit_get_IdealPitch_m78984A0FE189C30A9E1A6885C1B385F43E28A43B ();
// 0x000001D8 System.Void TBOrbit::set_IdealPitch(System.Single)
extern void TBOrbit_set_IdealPitch_m012C2A78264759B76D23EA69316D51CCD6EB2838 ();
// 0x000001D9 UnityEngine.Vector3 TBOrbit::get_IdealPanOffset()
extern void TBOrbit_get_IdealPanOffset_m7C63DF20C4B0C91E4D3AE44DCC2E34B4AC64CDF1 ();
// 0x000001DA System.Void TBOrbit::set_IdealPanOffset(UnityEngine.Vector3)
extern void TBOrbit_set_IdealPanOffset_m9BF6F0DCF2BB878921E685E298B081CE1EC0D2A4 ();
// 0x000001DB UnityEngine.Vector3 TBOrbit::get_PanOffset()
extern void TBOrbit_get_PanOffset_m2935C79AF272994CA3D5435F768BD5AE26E4EDC0 ();
// 0x000001DC System.Void TBOrbit::InstallGestureRecognizers()
extern void TBOrbit_InstallGestureRecognizers_m737ED1B79693328D95E7F7EA0FCF2B27D32CC61F ();
// 0x000001DD System.Void TBOrbit::Start()
extern void TBOrbit_Start_m49521025CDFBDDFA408791F3D5ED9A049979C5FD ();
// 0x000001DE System.Void TBOrbit::OnDrag(DragGesture)
extern void TBOrbit_OnDrag_mEA0C6C8C2B1DAF87B2815E00414035C0CEFFF3DC ();
// 0x000001DF System.Void TBOrbit::OnPinch(PinchGesture)
extern void TBOrbit_OnPinch_m482446A870858574758C0477973F5282F986A634 ();
// 0x000001E0 System.Void TBOrbit::OnTwoFingerDrag(DragGesture)
extern void TBOrbit_OnTwoFingerDrag_m93FFBE25DB907F65F59120DE04B8627327CC476B ();
// 0x000001E1 System.Void TBOrbit::Apply()
extern void TBOrbit_Apply_mD70C449BBA7DA16BDD6E5EC66176EF5E4DB34D8D ();
// 0x000001E2 System.Void TBOrbit::LateUpdate()
extern void TBOrbit_LateUpdate_mC2CE68E99EAFA33787F2049B2C954FB06AD45236 ();
// 0x000001E3 System.Single TBOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern void TBOrbit_ClampAngle_mEEC6F41E13722F0510BE62E202F6E7002B91F1F9 ();
// 0x000001E4 System.Void TBOrbit::ResetPanning()
extern void TBOrbit_ResetPanning_m61C00B2A917391A22FA9DF81C7D09862D114F2C1 ();
// 0x000001E5 System.Void TBOrbit::.ctor()
extern void TBOrbit__ctor_m99DC5B8C8C54993B78B509B6641EB1A1B3469640 ();
// 0x000001E6 System.Void TBPan::add_OnPan(TBPan_PanEventHandler)
extern void TBPan_add_OnPan_m9BBAA3F2EB1575361B697BDE66D0DB43C6260A6A ();
// 0x000001E7 System.Void TBPan::remove_OnPan(TBPan_PanEventHandler)
extern void TBPan_remove_OnPan_m0F4591FD702F04B933C577A2CCD95236E1BEF9DB ();
// 0x000001E8 System.Void TBPan::Awake()
extern void TBPan_Awake_mE3B2605744913576CF8020764AE9DFA8F2D8D2B5 ();
// 0x000001E9 System.Void TBPan::Start()
extern void TBPan_Start_m9CFFED405C5CAC7CD47C653BB95A93DC0E0E6BE1 ();
// 0x000001EA System.Void TBPan::OnDrag(DragGesture)
extern void TBPan_OnDrag_mBD48EE5AB07F4EBC1C19A37535735335872793EF ();
// 0x000001EB System.Void TBPan::Update()
extern void TBPan_Update_mDE2E3BC9F73B6C88CEBBE565C1108DDF50B81FE0 ();
// 0x000001EC UnityEngine.Vector3 TBPan::ConstrainToPanningPlane(UnityEngine.Vector3)
extern void TBPan_ConstrainToPanningPlane_m77719C9418286026532A30F2784FE6E1AB693617 ();
// 0x000001ED System.Void TBPan::TeleportTo(UnityEngine.Vector3)
extern void TBPan_TeleportTo_m3938C17435A0E25A7386C2F1130BADF9AD21625B ();
// 0x000001EE System.Void TBPan::FlyTo(UnityEngine.Vector3)
extern void TBPan_FlyTo_mFE546F60B5BB52AC274CF2D56427F3498659EADA ();
// 0x000001EF UnityEngine.Vector3 TBPan::ConstrainToMoveArea(UnityEngine.Vector3)
extern void TBPan_ConstrainToMoveArea_mD98E7755AC51F97D1F8473F52A24ABBE7DA3DB19 ();
// 0x000001F0 System.Void TBPan::.ctor()
extern void TBPan__ctor_mDEBFFF0A954CEEB023089636FDD4EDE84A5A49E2 ();
// 0x000001F1 UnityEngine.Vector3 TBPinchZoom::get_DefaultPos()
extern void TBPinchZoom_get_DefaultPos_m7DDF1C1D1C23CD4958E2E996402A192DEB4CD838 ();
// 0x000001F2 System.Void TBPinchZoom::set_DefaultPos(UnityEngine.Vector3)
extern void TBPinchZoom_set_DefaultPos_mD45537145F6540981C62D0692D93E660C74CF23D ();
// 0x000001F3 System.Single TBPinchZoom::get_DefaultFov()
extern void TBPinchZoom_get_DefaultFov_m77B8A301B233BA1C86A6FC3DE700DD289F05B723 ();
// 0x000001F4 System.Void TBPinchZoom::set_DefaultFov(System.Single)
extern void TBPinchZoom_set_DefaultFov_m52F3999D4675E5C0D966B57E2B471C798AC08AA0 ();
// 0x000001F5 System.Single TBPinchZoom::get_DefaultOrthoSize()
extern void TBPinchZoom_get_DefaultOrthoSize_m8501696D9C91E3D2383C62A2810FAE881A2DB532 ();
// 0x000001F6 System.Void TBPinchZoom::set_DefaultOrthoSize(System.Single)
extern void TBPinchZoom_set_DefaultOrthoSize_m1E1C9EF709F89E9AE8C0C1D5D036C65C87958C74 ();
// 0x000001F7 System.Single TBPinchZoom::get_ZoomAmount()
extern void TBPinchZoom_get_ZoomAmount_mC5ED2D7E435296614965C1A9E5110624C7B26839 ();
// 0x000001F8 System.Void TBPinchZoom::set_ZoomAmount(System.Single)
extern void TBPinchZoom_set_ZoomAmount_m5D571B3A67F3B190CBAAE25CA30E80C744299E67 ();
// 0x000001F9 System.Single TBPinchZoom::get_ZoomPercent()
extern void TBPinchZoom_get_ZoomPercent_mA220FBC5F974A35BE74CC38DE6895048CE3947F9 ();
// 0x000001FA System.Void TBPinchZoom::Start()
extern void TBPinchZoom_Start_mECA3F2698B639C7EC252C98B3AFD02B528D51695 ();
// 0x000001FB System.Void TBPinchZoom::SetDefaults()
extern void TBPinchZoom_SetDefaults_mED97025E31E10D6F1FF73D53778B3C4E558CEBAE ();
// 0x000001FC System.Void TBPinchZoom::OnPinch(PinchGesture)
extern void TBPinchZoom_OnPinch_m5557CAA00398CC01DBF6FC546279C7459CD428FE ();
// 0x000001FD System.Void TBPinchZoom::.ctor()
extern void TBPinchZoom__ctor_m0C7E9859454B93EFB8099B36E17BF9FDECF26439 ();
// 0x000001FE System.Boolean TBDragToMove::get_Dragging()
extern void TBDragToMove_get_Dragging_m13BD30ABCD8F41D5D8E4F74F19C28E66033CFFE6 ();
// 0x000001FF System.Void TBDragToMove::set_Dragging(System.Boolean)
extern void TBDragToMove_set_Dragging_mE2434925C6E6F3B727270D1AAF40B3F64AB27903 ();
// 0x00000200 System.Void TBDragToMove::Start()
extern void TBDragToMove_Start_m201EE45E8E04FCA2B5F4F1AF03E1D9991D844C0A ();
// 0x00000201 System.Boolean TBDragToMove::ProjectScreenPointOnDragPlane(UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Vector3&)
extern void TBDragToMove_ProjectScreenPointOnDragPlane_mAA852D31E565D7C15E6062F5C6BB692FD95A6F6D ();
// 0x00000202 System.Void TBDragToMove::HandleDrag(DragGesture)
extern void TBDragToMove_HandleDrag_mD26B1354B4113AED65B0442AB6CB21A836FF884C ();
// 0x00000203 System.Void TBDragToMove::FixedUpdate()
extern void TBDragToMove_FixedUpdate_m79FCE2CE036ACCC26D8B1EEAF40B756648BDAC37 ();
// 0x00000204 System.Void TBDragToMove::OnDrag(DragGesture)
extern void TBDragToMove_OnDrag_m81B318CA826400A99A8664B821FC0B22E19421B7 ();
// 0x00000205 System.Void TBDragToMove::OnDisable()
extern void TBDragToMove_OnDisable_m2BCE129FBF45845782EAE46A60C5B95BB4B0E681 ();
// 0x00000206 System.Void TBDragToMove::.ctor()
extern void TBDragToMove__ctor_m8EA5908B5B6783AF88BE3AC2D218FE6B9BB321D6 ();
// 0x00000207 System.Void TBHoverChangeMaterial::Start()
extern void TBHoverChangeMaterial_Start_mF9EFE7682B01B732B1001D536BA7EB61D760F60D ();
// 0x00000208 System.Void TBHoverChangeMaterial::OnFingerHover(FingerHoverEvent)
extern void TBHoverChangeMaterial_OnFingerHover_m531BDDB44384FD6EF4180EF184DA18C20638C0FC ();
// 0x00000209 System.Void TBHoverChangeMaterial::.ctor()
extern void TBHoverChangeMaterial__ctor_mE20D28D976CA1FFFDB5E1E5592808BAED8D50B03 ();
// 0x0000020A System.Void TBHoverChangeScale::Start()
extern void TBHoverChangeScale_Start_m4953B61067C314FEC3EFC98E53AC060FBA5E38D7 ();
// 0x0000020B System.Void TBHoverChangeScale::OnFingerHover(FingerHoverEvent)
extern void TBHoverChangeScale_OnFingerHover_m8F2732AAB6F14E66346A8F7C9F95C94485AFB43B ();
// 0x0000020C System.Void TBHoverChangeScale::.ctor()
extern void TBHoverChangeScale__ctor_mE20D4956CAC7E4709A32C8FDD7C7BF6C156F587B ();
// 0x0000020D System.Single TBPinchToScale::get_ScaleAmount()
extern void TBPinchToScale_get_ScaleAmount_m2068956B7FEF92ED47B114C74EE1381EE9AE4A0B ();
// 0x0000020E System.Void TBPinchToScale::set_ScaleAmount(System.Single)
extern void TBPinchToScale_set_ScaleAmount_mB0B0637C806C36F80CD91F5AE8A76959A5697BCA ();
// 0x0000020F System.Single TBPinchToScale::get_IdealScaleAmount()
extern void TBPinchToScale_get_IdealScaleAmount_mA082A819E1D2AE780129863D3AF81F4CED353DF9 ();
// 0x00000210 System.Void TBPinchToScale::set_IdealScaleAmount(System.Single)
extern void TBPinchToScale_set_IdealScaleAmount_mE9C2C5758153F1EBB9EAFA88B61F37325CDA42D4 ();
// 0x00000211 System.Void TBPinchToScale::Start()
extern void TBPinchToScale_Start_m961759F3FE750E6BCA4C3043B4680EC756298374 ();
// 0x00000212 System.Void TBPinchToScale::Update()
extern void TBPinchToScale_Update_m775C06A9B3FE31BFAB0A6ACCE2A66AA45A87BDF1 ();
// 0x00000213 System.Void TBPinchToScale::OnPinch(PinchGesture)
extern void TBPinchToScale_OnPinch_m59F6AE58FC7B1DA7A7F78CD5EA50E9F08775A81B ();
// 0x00000214 System.Void TBPinchToScale::.ctor()
extern void TBPinchToScale__ctor_m191397A1F682A004904E8740F097B4F356415155 ();
// 0x00000215 UnityEngine.GameObject TBQuickSetup::CreateChildNode(System.String)
extern void TBQuickSetup_CreateChildNode_mC411E1FEE1689513AEBA9C6F4880F2EC12A87FCF ();
// 0x00000216 System.Void TBQuickSetup::Start()
extern void TBQuickSetup_Start_m2591FB573E82E3EC669B16953F75ADAD11F770D6 ();
// 0x00000217 T TBQuickSetup::AddFingerEventDetector(UnityEngine.GameObject)
// 0x00000218 T TBQuickSetup::AddGesture(UnityEngine.GameObject)
// 0x00000219 T TBQuickSetup::AddSingleFingerGesture(UnityEngine.GameObject)
// 0x0000021A T TBQuickSetup::AddTwoFingerGesture(UnityEngine.GameObject)
// 0x0000021B T TBQuickSetup::AddTwoFingerGesture(UnityEngine.GameObject,System.String)
// 0x0000021C System.Void TBQuickSetup::.ctor()
extern void TBQuickSetup__ctor_m7989B7C78EEE8111ADE341B5543D4603474A6293 ();
// 0x0000021D System.Void TBTwistToRotate::Start()
extern void TBTwistToRotate_Start_mA8C93A1183330EE5536CD59432C8017882A8217A ();
// 0x0000021E UnityEngine.Vector3 TBTwistToRotate::GetRotationAxis()
extern void TBTwistToRotate_GetRotationAxis_m09A187E8BCF635C8CCE180294881C6A542754832 ();
// 0x0000021F System.Void TBTwistToRotate::OnTwist(TwistGesture)
extern void TBTwistToRotate_OnTwist_m097ED7D3E35E81D254A8CDFFAE44EA16B5496DF0 ();
// 0x00000220 System.Void TBTwistToRotate::.ctor()
extern void TBTwistToRotate__ctor_m0105BB85D9CFD7D52B8BFD198075B94F3C429639 ();
// 0x00000221 System.Int32 NativeGallery::_NativeGallery_CheckPermission()
extern void NativeGallery__NativeGallery_CheckPermission_m6A0F8861FDB10A7901F2B71DC9DC8C516D382D30 ();
// 0x00000222 System.Int32 NativeGallery::_NativeGallery_RequestPermission()
extern void NativeGallery__NativeGallery_RequestPermission_m5467E875D262F47FB5AA35ADD642F3F89E86AE94 ();
// 0x00000223 System.Int32 NativeGallery::_NativeGallery_CanOpenSettings()
extern void NativeGallery__NativeGallery_CanOpenSettings_mBC81EE459401A771AD2F7D75AFE6F41AB2BA33BF ();
// 0x00000224 System.Void NativeGallery::_NativeGallery_OpenSettings()
extern void NativeGallery__NativeGallery_OpenSettings_m4EB8AE632743DCF907879B1726613DEB3F03776D ();
// 0x00000225 System.Void NativeGallery::_NativeGallery_ImageWriteToAlbum(System.String,System.String)
extern void NativeGallery__NativeGallery_ImageWriteToAlbum_m4AD58389EBBC098363150D3AF1E9AE855D0E873D ();
// 0x00000226 System.Void NativeGallery::_NativeGallery_VideoWriteToAlbum(System.String,System.String)
extern void NativeGallery__NativeGallery_VideoWriteToAlbum_m65ED6475091CE8FB3413BCE8FE946EDD91CD88AF ();
// 0x00000227 System.Void NativeGallery::_NativeGallery_PickImage(System.String,System.Int32)
extern void NativeGallery__NativeGallery_PickImage_mB98113DC9DD09AA4BE6E11BF6B71456BB8CCF193 ();
// 0x00000228 System.Void NativeGallery::_NativeGallery_PickVideo()
extern void NativeGallery__NativeGallery_PickVideo_m127C9DBF9DBDF181BBE44A3289C5A9F87222F9B3 ();
// 0x00000229 System.String NativeGallery::_NativeGallery_GetImageProperties(System.String)
extern void NativeGallery__NativeGallery_GetImageProperties_m94BB08DF2BE2691E01FD438ED4943215BFE5885E ();
// 0x0000022A System.String NativeGallery::_NativeGallery_GetVideoProperties(System.String)
extern void NativeGallery__NativeGallery_GetVideoProperties_mFDDAE4F5F65BC207232274D73369F44377D75633 ();
// 0x0000022B System.String NativeGallery::_NativeGallery_LoadImageAtPath(System.String,System.String,System.Int32)
extern void NativeGallery__NativeGallery_LoadImageAtPath_m9C924424AF1C98129472BA4BD29F10C560A3A050 ();
// 0x0000022C System.String NativeGallery::get_TemporaryImagePath()
extern void NativeGallery_get_TemporaryImagePath_mEAE96C176427B270ED15B3562CB160162F9E6FAA ();
// 0x0000022D System.String NativeGallery::get_IOSSelectedImagePath()
extern void NativeGallery_get_IOSSelectedImagePath_mAA64ADC4A5A5981D47F4C95D0D1E3FCF45198DDE ();
// 0x0000022E NativeGallery_Permission NativeGallery::CheckPermission(System.Boolean)
extern void NativeGallery_CheckPermission_m5D018CD533A1F52CAE5F7BDA64BE4B40D6645EF2 ();
// 0x0000022F NativeGallery_Permission NativeGallery::RequestPermission(System.Boolean)
extern void NativeGallery_RequestPermission_mFE13C3D9F015B5CFF7CDB382236D76F848BA133A ();
// 0x00000230 System.Boolean NativeGallery::CanOpenSettings()
extern void NativeGallery_CanOpenSettings_m1567631902BF8C4EFD0CEC3B366ED0604D6B9885 ();
// 0x00000231 System.Void NativeGallery::OpenSettings()
extern void NativeGallery_OpenSettings_m18B842C8DB49EBE163135FD737226763A8B08331 ();
// 0x00000232 NativeGallery_Permission NativeGallery::SaveImageToGallery(System.Byte[],System.String,System.String,NativeGallery_MediaSaveCallback)
extern void NativeGallery_SaveImageToGallery_mED0868FA4D895AF24FB7BD5F2250A40F903258AD ();
// 0x00000233 NativeGallery_Permission NativeGallery::SaveImageToGallery(System.String,System.String,System.String,NativeGallery_MediaSaveCallback)
extern void NativeGallery_SaveImageToGallery_mB38A1DA4118305F4BA9FDF201B9CFF7D13C0AD4F ();
// 0x00000234 NativeGallery_Permission NativeGallery::SaveImageToGallery(UnityEngine.Texture2D,System.String,System.String,NativeGallery_MediaSaveCallback)
extern void NativeGallery_SaveImageToGallery_m29D2A7A8C05ECB2B99DAF4B82A86FA59E21F6A1A ();
// 0x00000235 NativeGallery_Permission NativeGallery::SaveVideoToGallery(System.Byte[],System.String,System.String,NativeGallery_MediaSaveCallback)
extern void NativeGallery_SaveVideoToGallery_m03FDC15738A2E6DBE77ECA810889A8031C9FC778 ();
// 0x00000236 NativeGallery_Permission NativeGallery::SaveVideoToGallery(System.String,System.String,System.String,NativeGallery_MediaSaveCallback)
extern void NativeGallery_SaveVideoToGallery_m7649D4C134A18732BCF7E8D2C5300FA2A0563205 ();
// 0x00000237 System.Boolean NativeGallery::CanSelectMultipleFilesFromGallery()
extern void NativeGallery_CanSelectMultipleFilesFromGallery_m72A5847CF7FF2C830F36CA51836F7743F285023F ();
// 0x00000238 NativeGallery_Permission NativeGallery::GetImageFromGallery(NativeGallery_MediaPickCallback,System.String,System.String,System.Int32)
extern void NativeGallery_GetImageFromGallery_m216C9B77A1A5C9085A5F60ED408394A44E5C2ED9 ();
// 0x00000239 NativeGallery_Permission NativeGallery::GetVideoFromGallery(NativeGallery_MediaPickCallback,System.String,System.String)
extern void NativeGallery_GetVideoFromGallery_mFC68232D70A047B2148CD1F4682866AF52BF6D55 ();
// 0x0000023A NativeGallery_Permission NativeGallery::GetImagesFromGallery(NativeGallery_MediaPickMultipleCallback,System.String,System.String,System.Int32)
extern void NativeGallery_GetImagesFromGallery_m52307A84BB3CAE28AC4C2BC668B78D6A23FFB283 ();
// 0x0000023B NativeGallery_Permission NativeGallery::GetVideosFromGallery(NativeGallery_MediaPickMultipleCallback,System.String,System.String)
extern void NativeGallery_GetVideosFromGallery_mF4B1ABF9EA0F6E1C34CC1FB286B2241D117147A6 ();
// 0x0000023C System.Boolean NativeGallery::IsMediaPickerBusy()
extern void NativeGallery_IsMediaPickerBusy_m2BBEBAB9DBA14B45C0623326DC755C17E0104B15 ();
// 0x0000023D NativeGallery_Permission NativeGallery::SaveToGallery(System.Byte[],System.String,System.String,System.Boolean,NativeGallery_MediaSaveCallback)
extern void NativeGallery_SaveToGallery_m289F6C68D1E8B6B4172442AA9FAD378EFE648E0D ();
// 0x0000023E NativeGallery_Permission NativeGallery::SaveToGallery(System.String,System.String,System.String,System.Boolean,NativeGallery_MediaSaveCallback)
extern void NativeGallery_SaveToGallery_mFFC16470707CAC17A0A39341804D8FCAC468BA8A ();
// 0x0000023F System.Void NativeGallery::SaveToGalleryInternal(System.String,System.String,System.Boolean,NativeGallery_MediaSaveCallback)
extern void NativeGallery_SaveToGalleryInternal_m4D411FAEC19293304DC4F22C8869C37760D73677 ();
// 0x00000240 System.String NativeGallery::GetSavePath(System.String,System.String)
extern void NativeGallery_GetSavePath_m98C64A60B7F3DC454CF04DEE8E7889450C3AACD6 ();
// 0x00000241 NativeGallery_Permission NativeGallery::GetMediaFromGallery(NativeGallery_MediaPickCallback,System.Boolean,System.String,System.String,System.Int32)
extern void NativeGallery_GetMediaFromGallery_m0D515D8559A7CA0536AB1FA44AC252784FD912F3 ();
// 0x00000242 NativeGallery_Permission NativeGallery::GetMultipleMediaFromGallery(NativeGallery_MediaPickMultipleCallback,System.Boolean,System.String,System.String,System.Int32)
extern void NativeGallery_GetMultipleMediaFromGallery_mF2D3216FA2588691B68299C5459291B611F5DAC3 ();
// 0x00000243 UnityEngine.Texture2D NativeGallery::LoadImageAtPath(System.String,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void NativeGallery_LoadImageAtPath_mF8EA4DF308209C74BE4E65CD6FC34A29C4CD9ECA ();
// 0x00000244 NativeGallery_ImageProperties NativeGallery::GetImageProperties(System.String)
extern void NativeGallery_GetImageProperties_m763AF380AC987AB7257C326B14E8A67607F7D348 ();
// 0x00000245 NativeGallery_VideoProperties NativeGallery::GetVideoProperties(System.String)
extern void NativeGallery_GetVideoProperties_m1ADF41C7D3DF91E001DB8F575B30DC107E6A4159 ();
// 0x00000246 System.Void NativeGallery::.cctor()
extern void NativeGallery__cctor_m9FE86F86E41772CBDD90E3774F48F3B2A197DB41 ();
// 0x00000247 System.Void Astronaut::OnEnter()
extern void Astronaut_OnEnter_m1265E8A21CF20B4BCD6A6FF4DB5FDC2E09637FBB ();
// 0x00000248 System.Void Astronaut::OnExit()
extern void Astronaut_OnExit_m9F47C0BBB3422C5E3EF0C26CB57005D75D71D533 ();
// 0x00000249 System.Void Astronaut::StartDrilling()
extern void Astronaut_StartDrilling_mED1F5F9A91A0A51EDA08BDEF99314899288B42B9 ();
// 0x0000024A System.Void Astronaut::AnimEvt_ScaleUpDrill()
extern void Astronaut_AnimEvt_ScaleUpDrill_m2824CCE17323144AEB720D7CBF68360EAB518062 ();
// 0x0000024B System.Void Astronaut::AnimEvt_ScaleDownDrill()
extern void Astronaut_AnimEvt_ScaleDownDrill_mF4FC35FD5BB27BED121575A51FAB2F29ED43F5E8 ();
// 0x0000024C System.Void Astronaut::AnimEvt_PlayDrillEffect()
extern void Astronaut_AnimEvt_PlayDrillEffect_m08725D89D34C0836E50C4CACE1CE4AF1A2196A8C ();
// 0x0000024D System.Void Astronaut::AnimEvt_StopDrillEffect()
extern void Astronaut_AnimEvt_StopDrillEffect_mABA7781EE3C53ACEA52532A24F6B0607FDC493A6 ();
// 0x0000024E System.Void Astronaut::AnimEvt_StopWaving()
extern void Astronaut_AnimEvt_StopWaving_mB30E88D8094247EEA81849DE7A9331E607753E3A ();
// 0x0000024F System.Void Astronaut::AnimEvt_StartWaving()
extern void Astronaut_AnimEvt_StartWaving_m4331D9DAFB6E3A79A3C21E8C299D8E7C5873671B ();
// 0x00000250 System.Void Astronaut::HandleVirtualButtonPressed()
extern void Astronaut_HandleVirtualButtonPressed_mC9207408B17331D52FDE8E8C6072B29A455D557E ();
// 0x00000251 System.Void Astronaut::HandleVirtualButtonReleased()
extern void Astronaut_HandleVirtualButtonReleased_mC1BD1CE733EE227AEB8EB3C6C4B8571C5F3F79DC ();
// 0x00000252 System.Boolean Astronaut::get_IsDrilling()
extern void Astronaut_get_IsDrilling_mE91BCBBBAE730D0F4BF091066593D622B3A731CF ();
// 0x00000253 System.Void Astronaut::set_IsDrilling(System.Boolean)
extern void Astronaut_set_IsDrilling_mD0E8EA1EF4A006A7C72733F5CC4C5CC1296B2BAA ();
// 0x00000254 System.Boolean Astronaut::get_IsWaving()
extern void Astronaut_get_IsWaving_m12DB912BD8E6E9B040F34C6387FC080C746AD567 ();
// 0x00000255 System.Void Astronaut::set_IsWaving(System.Boolean)
extern void Astronaut_set_IsWaving_m1A2ADE09D8DBE20DDD81A7681768F874043135B3 ();
// 0x00000256 System.Void Astronaut::.ctor()
extern void Astronaut__ctor_m614E1FE435ED399704CD6169D99AFD41EE048642 ();
// 0x00000257 System.Void Augmentation::Start()
extern void Augmentation_Start_mBB26EE08FF087F1092F9A1D196EA0433DFC83EA6 ();
// 0x00000258 System.Void Augmentation::Enable()
extern void Augmentation_Enable_mF6123555D35D6B859B8D0D40170B5D8F261EB9E5 ();
// 0x00000259 System.Void Augmentation::Disable()
extern void Augmentation_Disable_mEB0FBBFD00B76952F950C496E29F33573300EF1A ();
// 0x0000025A System.Void Augmentation::Restore()
extern void Augmentation_Restore_m2E1179ECA869DE07FF0B0F4FDAA6A7B7068D8E46 ();
// 0x0000025B System.Void Augmentation::OnEnter()
extern void Augmentation_OnEnter_m843570501B36E83B377ED7CA0AEC5BA364200B14 ();
// 0x0000025C System.Void Augmentation::OnExit()
extern void Augmentation_OnExit_m8BD837705992F828626D92FAB738665C31FE873E ();
// 0x0000025D System.Void Augmentation::SetRenderersEnabled(System.Boolean)
extern void Augmentation_SetRenderersEnabled_m4DCADDA846925909559E9CE0B080924DBA69BBCF ();
// 0x0000025E System.Void Augmentation::SetCollidersEnabled(System.Boolean)
extern void Augmentation_SetCollidersEnabled_m9425EE6822E9CB0A6AA0896A949A17F368D1DE78 ();
// 0x0000025F System.Collections.IEnumerator Augmentation::WaitForThen(System.Single,System.Action)
extern void Augmentation_WaitForThen_m207FBAC26DCCABBDA7398974A3D8138A1D566EE5 ();
// 0x00000260 System.Void Augmentation::.ctor()
extern void Augmentation__ctor_m945EF46FC3E87A3C4FBE8D311A28ACC9CF5DED60 ();
// 0x00000261 System.Void Drone::OnEnter()
extern void Drone_OnEnter_m00CA9816D12CC2FED05CAA07CCDAED5A9ECA0202 ();
// 0x00000262 System.Void Drone::OnExit()
extern void Drone_OnExit_mB74F5D4B6CDB04C8D5B315475DAF32CE48D30DDF ();
// 0x00000263 System.Void Drone::AnimEvt_StopScanning()
extern void Drone_AnimEvt_StopScanning_m8A8009410142561CB167F685978AD181220BB95A ();
// 0x00000264 System.Void Drone::AnimEvt_StartScanning()
extern void Drone_AnimEvt_StartScanning_m062C31EFC78A11CA24FCCC1AE334E970797BC097 ();
// 0x00000265 System.Void Drone::HandleVirtualButtonPressed()
extern void Drone_HandleVirtualButtonPressed_mF5F7A249429D11B943F34123B1B9AB9019D31662 ();
// 0x00000266 System.Void Drone::HandleVirtualButtonReleased()
extern void Drone_HandleVirtualButtonReleased_m11E37050FA69C62171DC23547876CBB70296ECB8 ();
// 0x00000267 System.Boolean Drone::get_IsFacingObject()
extern void Drone_get_IsFacingObject_m1A5D680C644C5F2009E146E89C7108CC8EF10161 ();
// 0x00000268 System.Void Drone::set_IsFacingObject(System.Boolean)
extern void Drone_set_IsFacingObject_m3E260D366C96464E6A422757FBF49B43231AEF37 ();
// 0x00000269 System.Boolean Drone::get_IsScanning()
extern void Drone_get_IsScanning_m3DEA6A0ADD242670BCE9ABBA5CBF617A7DC88376 ();
// 0x0000026A System.Void Drone::set_IsScanning(System.Boolean)
extern void Drone_set_IsScanning_m1B92CDECDD0DDE8051B85AF4B04625470DC52E85 ();
// 0x0000026B System.Boolean Drone::get_IsShowingLaser()
extern void Drone_get_IsShowingLaser_m016FB95F4D3F772FBFA3DA41D5D48889C691D98E ();
// 0x0000026C System.Void Drone::set_IsShowingLaser(System.Boolean)
extern void Drone_set_IsShowingLaser_mBC5F87DBA8C284F09BE69E142A37EF6A43D824DA ();
// 0x0000026D System.Void Drone::.ctor()
extern void Drone__ctor_mE344ACBD947ED29C5DB5E5E20418175EBA955778 ();
// 0x0000026E System.Void Fissure::HandleVirtualButtonPressed()
extern void Fissure_HandleVirtualButtonPressed_m40F34F6E24A2CD54D7880D638135C7FCAD978F47 ();
// 0x0000026F System.Void Fissure::HandleVirtualButtonReleased()
extern void Fissure_HandleVirtualButtonReleased_mB806B67638FDCCCF72002629D333A8435115F3B3 ();
// 0x00000270 System.Void Fissure::.ctor()
extern void Fissure__ctor_mC1D270895E157B1972DF315028249AA3D9568030 ();
// 0x00000271 System.Boolean Habitat::get_IsHatchOpen()
extern void Habitat_get_IsHatchOpen_mCD079D6FF3CBAE82219F33AF5C0A837DA3E51423 ();
// 0x00000272 System.Void Habitat::set_IsHatchOpen(System.Boolean)
extern void Habitat_set_IsHatchOpen_m926AA2DD5BEEC2F0EFE0C735D0BFFC5D528412AA ();
// 0x00000273 System.Boolean Habitat::get_IsDoorOpen()
extern void Habitat_get_IsDoorOpen_mF496D3A6A8DD016D18412B0EC9B46B870A3FBEF2 ();
// 0x00000274 System.Void Habitat::set_IsDoorOpen(System.Boolean)
extern void Habitat_set_IsDoorOpen_m48319C32BCBC40B25B5723E0E3C8A38AA9942EEE ();
// 0x00000275 System.Void Habitat::Restore()
extern void Habitat_Restore_mAE64E56638471436E7A35D8EB7BC3844FA90D014 ();
// 0x00000276 System.Void Habitat::OnEnter()
extern void Habitat_OnEnter_mF43B038838E9DBC959A8520CC0C8A6CAD49ABD55 ();
// 0x00000277 System.Void Habitat::OpenDoor(System.Boolean)
extern void Habitat_OpenDoor_mC8B939FDA0072516BA4C65166F7020F8C531155C ();
// 0x00000278 System.Void Habitat::.ctor()
extern void Habitat__ctor_m508CA8986B60BF4033E61F5E6318026B4E4EBC13 ();
// 0x00000279 System.Void OxygenTank::Restore()
extern void OxygenTank_Restore_m9BD6C8071B515E494E492429589DE8CF62A9D6DB ();
// 0x0000027A System.Void OxygenTank::OnEnter()
extern void OxygenTank_OnEnter_mB073CED4090EB3CE3C25076CCCE8CAC01BDA84A5 ();
// 0x0000027B System.Void OxygenTank::ShowDetail()
extern void OxygenTank_ShowDetail_m46CE2D709B5F180BDC6BCCDF77E010DBC05F9027 ();
// 0x0000027C System.Void OxygenTank::HideDetail()
extern void OxygenTank_HideDetail_m9A2F848B50C42500D14577F6DFC472E5F8D9711E ();
// 0x0000027D System.Void OxygenTank::HandleVirtualButtonPressed()
extern void OxygenTank_HandleVirtualButtonPressed_m8391607236AB6D2D118F4CB768A4874A013FC0CF ();
// 0x0000027E System.Void OxygenTank::HandleVirtualButtonReleased()
extern void OxygenTank_HandleVirtualButtonReleased_m79B1211A7D8DD58416E4BDC4BFA3A0B7C58702B9 ();
// 0x0000027F System.Void OxygenTank::DoEnter()
extern void OxygenTank_DoEnter_m2B925A90A82DF18C2640F5F765493DCC48D8BCDA ();
// 0x00000280 System.Boolean OxygenTank::get_IsDetailOn()
extern void OxygenTank_get_IsDetailOn_mFC6EB504CF463252F41546768906A7F7A0A4B01D ();
// 0x00000281 System.Void OxygenTank::set_IsDetailOn(System.Boolean)
extern void OxygenTank_set_IsDetailOn_mE08D6DC5C64BA7F821F25410A6EDD261443BAA55 ();
// 0x00000282 System.Void OxygenTank::.ctor()
extern void OxygenTank__ctor_m1DF71084DA171966BA4A2A1581B712F3BD30D62F ();
// 0x00000283 System.Void RockPileController::Awake()
extern void RockPileController_Awake_m9D28FEF5D995FF712DFF131BDEEEF3C44471318A ();
// 0x00000284 System.Void RockPileController::FadeOut()
extern void RockPileController_FadeOut_mAFDC00C183C0C1DD38D757D79FD8696F7DB46902 ();
// 0x00000285 System.Void RockPileController::FadeIn()
extern void RockPileController_FadeIn_m3501124057C9F4BFFC2771997C5F68E513F060D8 ();
// 0x00000286 System.Void RockPileController::.ctor()
extern void RockPileController__ctor_m3F5DAB61F61F074B9CF5F96C571A88FA65F1099A ();
// 0x00000287 System.Void DrillController::Update()
extern void DrillController_Update_m7A2DBB5F074BE5DE7567E613E718D3BC908B9B47 ();
// 0x00000288 System.Void DrillController::.ctor()
extern void DrillController__ctor_mAD0640F95405E58306F0E583A212F9391E4645D1 ();
// 0x00000289 System.Void FadeObject::Awake()
extern void FadeObject_Awake_mEA63791C2C7B0704470C7845C3E7744E6EAA95C1 ();
// 0x0000028A System.Void FadeObject::Update()
extern void FadeObject_Update_m879556E92DBAE199B7EA96FD6A8257A7AF3F1414 ();
// 0x0000028B System.Void FadeObject::SetInitialOpacity(System.Single)
extern void FadeObject_SetInitialOpacity_mE811FB906876DB13872A0395C12E64E1073A968E ();
// 0x0000028C System.Void FadeObject::SetOpacity(System.Single)
extern void FadeObject_SetOpacity_m93FA3C165115AF455C45EC33FB583E43CA3B889D ();
// 0x0000028D System.Void FadeObject::SetRenderingMode(System.Boolean)
extern void FadeObject_SetRenderingMode_mA2C5640425C60DAF556D5075DC5C98F298B3C9D3 ();
// 0x0000028E System.Void FadeObject::.ctor()
extern void FadeObject__ctor_m2C0BB37D9ECA56A5CC18CEB658FA12F6A281B3A1 ();
// 0x0000028F System.Void AstronautStateMachineBehaviour::DoStateEvent(UnityEngine.Animator,System.String)
extern void AstronautStateMachineBehaviour_DoStateEvent_m5C0E38CC88FCF3FD33199D81051B634EA312BFF2 ();
// 0x00000290 System.Type AstronautStateMachineBehaviour::GetTargetType()
extern void AstronautStateMachineBehaviour_GetTargetType_mC5FCE099514B78F9044EFA64AEF8AB3C3CBFFC25 ();
// 0x00000291 System.Void AstronautStateMachineBehaviour::.ctor()
extern void AstronautStateMachineBehaviour__ctor_mC414FDC7D7C569AC0F898165CCD28F753A182705 ();
// 0x00000292 System.Void AugmentationStateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AugmentationStateMachineBehaviour_OnStateEnter_mF0F7D66626AB8A6F4D26B24D3EB31FF15A878CCD ();
// 0x00000293 System.Void AugmentationStateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AugmentationStateMachineBehaviour_OnStateExit_mF9B5AB9C94D8D219FDD12FAA24BD05BFDA8DEBEC ();
// 0x00000294 System.Void AugmentationStateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AugmentationStateMachineBehaviour_OnStateUpdate_mAA55ABCBFCAC71FFA7C69F9CD8825D2A80386D6F ();
// 0x00000295 System.Void AugmentationStateMachineBehaviour::DoStateEvent(UnityEngine.Animator,System.String)
// 0x00000296 System.Type AugmentationStateMachineBehaviour::GetTargetType()
// 0x00000297 System.Action`1<T> AugmentationStateMachineBehaviour::GetMethod(T,System.String)
// 0x00000298 System.Void AugmentationStateMachineBehaviour::AddDelegateToCache(System.Action`1<T>,System.String)
// 0x00000299 System.Void AugmentationStateMachineBehaviour::.ctor()
extern void AugmentationStateMachineBehaviour__ctor_m968E88E7F275F78DA7EB78908C4A3D622152C6E6 ();
// 0x0000029A System.Void AugmentationStateMachineBehaviour::.cctor()
extern void AugmentationStateMachineBehaviour__cctor_mACFB66A8EE2CE87C290AF372C28CAE0F010B8789 ();
// 0x0000029B System.Boolean NativeGalleryNamespace.NGMediaReceiveCallbackiOS::get_IsBusy()
extern void NGMediaReceiveCallbackiOS_get_IsBusy_m1CA1732EB20946FF22EFAD5EFFBD248F85B3F46D ();
// 0x0000029C System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::set_IsBusy(System.Boolean)
extern void NGMediaReceiveCallbackiOS_set_IsBusy_mF3B5F4564B0807EC621E9D773A88EE65A67F07FD ();
// 0x0000029D System.Int32 NativeGalleryNamespace.NGMediaReceiveCallbackiOS::_NativeGallery_IsMediaPickerBusy()
extern void NGMediaReceiveCallbackiOS__NativeGallery_IsMediaPickerBusy_m9E008F04C1CABBCF52A6C63C3DD8CBF60C1FEA7B ();
// 0x0000029E System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::Initialize(NativeGallery_MediaPickCallback)
extern void NGMediaReceiveCallbackiOS_Initialize_m04FB6D655CA4E7B13B2585DA3F0D414058DC8132 ();
// 0x0000029F System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::Update()
extern void NGMediaReceiveCallbackiOS_Update_m71798872CAA675048E8DB8CB4B742FC0A1B7C88C ();
// 0x000002A0 System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::OnMediaReceived(System.String)
extern void NGMediaReceiveCallbackiOS_OnMediaReceived_mAE345C1B38DA02EF1AB0BB1913082B529D286D81 ();
// 0x000002A1 System.Void NativeGalleryNamespace.NGMediaReceiveCallbackiOS::.ctor()
extern void NGMediaReceiveCallbackiOS__ctor_m2965A3D300B4D62C78499FE7B872EFA5E3A1490C ();
// 0x000002A2 System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::Initialize(NativeGallery_MediaSaveCallback)
extern void NGMediaSaveCallbackiOS_Initialize_m3206E390197153C037E1698B19CD1F552EB66194 ();
// 0x000002A3 System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::OnMediaSaveCompleted(System.String)
extern void NGMediaSaveCallbackiOS_OnMediaSaveCompleted_m194CE14B4029AE8D5180B52318C709A7B32B890D ();
// 0x000002A4 System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::OnMediaSaveFailed(System.String)
extern void NGMediaSaveCallbackiOS_OnMediaSaveFailed_m537D39C18B5CB85E683E8B38D3E222B1F686C278 ();
// 0x000002A5 System.Void NativeGalleryNamespace.NGMediaSaveCallbackiOS::.ctor()
extern void NGMediaSaveCallbackiOS__ctor_mAC53D9541A341FB6E9A7766EE2C0A9F34EE174F4 ();
// 0x000002A6 System.Void Lean.Touch.LeanDestroy::Update()
extern void LeanDestroy_Update_mF8A7B5B488ACB75BBEDF562AFCF7A93D294BA3A5 ();
// 0x000002A7 System.Void Lean.Touch.LeanDestroy::DestroyNow()
extern void LeanDestroy_DestroyNow_m444028047AA956152270C46F2DE1264C0EFD3E81 ();
// 0x000002A8 System.Void Lean.Touch.LeanDestroy::.ctor()
extern void LeanDestroy__ctor_m758FBC909E7BCD7FF0C11F5F399E42A09519C3BF ();
// 0x000002A9 System.Void Lean.Touch.LeanDragCamera::MoveToSelection()
extern void LeanDragCamera_MoveToSelection_m1977E8C845757FC138CE4F1034BC847FACF5F7B0 ();
// 0x000002AA System.Void Lean.Touch.LeanDragCamera::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragCamera_AddFinger_m24F4729455607956757363745333CDBBEA75D61E ();
// 0x000002AB System.Void Lean.Touch.LeanDragCamera::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragCamera_RemoveFinger_m9AF9D2989DFEF3E763F5A9287ACC3F0BCB70A2DD ();
// 0x000002AC System.Void Lean.Touch.LeanDragCamera::RemoveAllFingers()
extern void LeanDragCamera_RemoveAllFingers_m610023930CDB313937193FBDF90C7D1B9D949F21 ();
// 0x000002AD System.Void Lean.Touch.LeanDragCamera::Awake()
extern void LeanDragCamera_Awake_mF6560D575DB51AFB2D35787745A1D115964C42E0 ();
// 0x000002AE System.Void Lean.Touch.LeanDragCamera::LateUpdate()
extern void LeanDragCamera_LateUpdate_m0ED3B37DAB02F748A6B1B6CAA51240018FEE7CAE ();
// 0x000002AF System.Void Lean.Touch.LeanDragCamera::.ctor()
extern void LeanDragCamera__ctor_m945AB4E3D7224EEEC4BA96EC2A524643C3B2A882 ();
// 0x000002B0 System.Void Lean.Touch.LeanDragTrail::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTrail_AddFinger_m3938B8A3923CC39D1DC8519BCD22501599AC08F0 ();
// 0x000002B1 System.Void Lean.Touch.LeanDragTrail::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTrail_RemoveFinger_m0CD4D6D556460B5007DA1F6A6A65AE5D2E9212AD ();
// 0x000002B2 System.Void Lean.Touch.LeanDragTrail::RemoveAllFingers()
extern void LeanDragTrail_RemoveAllFingers_m16EAFB4113DA305AB97E93A8782434ACFCAEDBAF ();
// 0x000002B3 System.Void Lean.Touch.LeanDragTrail::Awake()
extern void LeanDragTrail_Awake_m307D8228F7EABDE52FCDB036C2D395DA02F609A4 ();
// 0x000002B4 System.Void Lean.Touch.LeanDragTrail::OnEnable()
extern void LeanDragTrail_OnEnable_m1C3283DBC79D8BD766B12886462A23C001437ACC ();
// 0x000002B5 System.Void Lean.Touch.LeanDragTrail::OnDisable()
extern void LeanDragTrail_OnDisable_mC3557A3D120CAD1848776EA9E7ED54B69380D2D7 ();
// 0x000002B6 System.Void Lean.Touch.LeanDragTrail::Update()
extern void LeanDragTrail_Update_m37EA28C4BAAC9F061D85CEA604028CE2138C21FB ();
// 0x000002B7 System.Void Lean.Touch.LeanDragTrail::UpdateLine(Lean.Touch.LeanDragTrail_FingerData,Lean.Touch.LeanFinger,UnityEngine.LineRenderer)
extern void LeanDragTrail_UpdateLine_m50676A6A1320D9DAF7EADEC284C9B57A15E54E13 ();
// 0x000002B8 System.Void Lean.Touch.LeanDragTrail::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanDragTrail_HandleFingerUp_mD038BEA8B659D7DE067C2FBA042D90F3DF3C9B97 ();
// 0x000002B9 System.Void Lean.Touch.LeanDragTrail::.ctor()
extern void LeanDragTrail__ctor_m110B282844E3C9825A9B661E213FC790978D43BD ();
// 0x000002BA System.Void Lean.Touch.LeanDragTranslate::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslate_AddFinger_m07D215FA35418FB83D41781CFA54BD13EB109E9A ();
// 0x000002BB System.Void Lean.Touch.LeanDragTranslate::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslate_RemoveFinger_m3C86E3F451E0E7B02EDCF5459D1E945EE6DFC1E2 ();
// 0x000002BC System.Void Lean.Touch.LeanDragTranslate::RemoveAllFingers()
extern void LeanDragTranslate_RemoveAllFingers_mD3A5A560517FA88350ACDDDC7B99B9717770F6C2 ();
// 0x000002BD System.Void Lean.Touch.LeanDragTranslate::Awake()
extern void LeanDragTranslate_Awake_m52E59493927D82C08459200D9949E2D995061ED7 ();
// 0x000002BE System.Void Lean.Touch.LeanDragTranslate::Update()
extern void LeanDragTranslate_Update_m7B3A74A95B26058BED511014AEF401F407A951A9 ();
// 0x000002BF System.Void Lean.Touch.LeanDragTranslate::TranslateUI(UnityEngine.Vector2)
extern void LeanDragTranslate_TranslateUI_m7E8CAB2BD7AB2BE98DCA0356F133C56B7892D83E ();
// 0x000002C0 System.Void Lean.Touch.LeanDragTranslate::Translate(UnityEngine.Vector2)
extern void LeanDragTranslate_Translate_m7F402395FB2B5C749BE7AB7DEAB37554FE563EF9 ();
// 0x000002C1 System.Void Lean.Touch.LeanDragTranslate::.ctor()
extern void LeanDragTranslate__ctor_m02088D0CA3FC55BE1362826B756553CB6DB452B8 ();
// 0x000002C2 System.Int32 Lean.Touch.LeanFingerData::Count(System.Collections.Generic.List`1<T>)
// 0x000002C3 System.Boolean Lean.Touch.LeanFingerData::Exists(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger)
// 0x000002C4 System.Void Lean.Touch.LeanFingerData::Remove(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger,System.Collections.Generic.Stack`1<T>)
// 0x000002C5 System.Void Lean.Touch.LeanFingerData::RemoveAll(System.Collections.Generic.List`1<T>,System.Collections.Generic.Stack`1<T>)
// 0x000002C6 T Lean.Touch.LeanFingerData::Find(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger)
// 0x000002C7 T Lean.Touch.LeanFingerData::FindOrCreate(System.Collections.Generic.List`1<T>&,Lean.Touch.LeanFinger)
// 0x000002C8 System.Void Lean.Touch.LeanFingerData::.ctor()
extern void LeanFingerData__ctor_m159D6946AD579652B3CCDE0831D474424579E59E ();
// 0x000002C9 Lean.Touch.LeanFingerDown_LeanFingerEvent Lean.Touch.LeanFingerDown::get_OnFinger()
extern void LeanFingerDown_get_OnFinger_m7DE562F50EEAFFEA842E1012F186B8836F98FB46 ();
// 0x000002CA Lean.Touch.LeanFingerDown_Vector3Event Lean.Touch.LeanFingerDown::get_OnPosition()
extern void LeanFingerDown_get_OnPosition_m636D47F9AE685B92A42C52E48EC3B8CC9F522FB6 ();
// 0x000002CB System.Void Lean.Touch.LeanFingerDown::Awake()
extern void LeanFingerDown_Awake_m4311178FA4CCCB76D8B2446B9F5C78A5F072EF54 ();
// 0x000002CC System.Void Lean.Touch.LeanFingerDown::OnEnable()
extern void LeanFingerDown_OnEnable_mECA843AA2316CF70BF27DDD74C0245CD9F2F7F71 ();
// 0x000002CD System.Void Lean.Touch.LeanFingerDown::OnDisable()
extern void LeanFingerDown_OnDisable_mCC8DE45EF430CC413649765CF46E4F612211B30C ();
// 0x000002CE System.Void Lean.Touch.LeanFingerDown::HandleOnFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerDown_HandleOnFingerDown_m0A95EAAB8A6308A4B4F2EB21CC9722A7CB93A50D ();
// 0x000002CF System.Void Lean.Touch.LeanFingerDown::.ctor()
extern void LeanFingerDown__ctor_mF96DF83185F2085EE110A5C4B6CF59F3E3EC1154 ();
// 0x000002D0 System.Void Lean.Touch.LeanFingerFilter::.ctor(System.Boolean)
extern void LeanFingerFilter__ctor_mBFC4268C31CBAA1164E0FACC436B3698A800F152_AdjustorThunk ();
// 0x000002D1 System.Void Lean.Touch.LeanFingerFilter::.ctor(Lean.Touch.LeanFingerFilter_FilterType,System.Boolean,System.Int32,System.Int32,Lean.Touch.LeanSelectable)
extern void LeanFingerFilter__ctor_m0F736E26D1405F2ACC75939E73F3397D41D2BF4D_AdjustorThunk ();
// 0x000002D2 System.Void Lean.Touch.LeanFingerFilter::UpdateRequiredSelectable(UnityEngine.GameObject)
extern void LeanFingerFilter_UpdateRequiredSelectable_mC6094A9811766962D7C3A2C9AB6C40B9405834E5_AdjustorThunk ();
// 0x000002D3 System.Void Lean.Touch.LeanFingerFilter::AddFinger(Lean.Touch.LeanFinger)
extern void LeanFingerFilter_AddFinger_m09250D81F74E9082F19472A00E737DD3534388C2_AdjustorThunk ();
// 0x000002D4 System.Void Lean.Touch.LeanFingerFilter::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanFingerFilter_RemoveFinger_m812BA1E281903E2295C1DCDEA18FFA84A86F1AC1_AdjustorThunk ();
// 0x000002D5 System.Void Lean.Touch.LeanFingerFilter::RemoveAllFingers()
extern void LeanFingerFilter_RemoveAllFingers_m8F4BF1A119EA7CDC2C248CA54DA11891E582C5A5_AdjustorThunk ();
// 0x000002D6 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanFingerFilter::GetFingers(System.Boolean)
extern void LeanFingerFilter_GetFingers_m7AFBF5C29A7063DA01584945DFCE7E0D3C53A7D2_AdjustorThunk ();
// 0x000002D7 Lean.Touch.LeanFingerOld_LeanFingerEvent Lean.Touch.LeanFingerOld::get_OnFinger()
extern void LeanFingerOld_get_OnFinger_mE6B73F3A61A8307C19114534A1686F9C25517E69 ();
// 0x000002D8 Lean.Touch.LeanFingerOld_Vector3Event Lean.Touch.LeanFingerOld::get_OnPosition()
extern void LeanFingerOld_get_OnPosition_mA749666459962DE98BD046556F6041EEA7F94E05 ();
// 0x000002D9 System.Void Lean.Touch.LeanFingerOld::Start()
extern void LeanFingerOld_Start_mA3296021AC37C901A401C8F036E99404BE76E4E7 ();
// 0x000002DA System.Void Lean.Touch.LeanFingerOld::OnEnable()
extern void LeanFingerOld_OnEnable_mF379DD1C04EEBE6E623A330966855981875BAEED ();
// 0x000002DB System.Void Lean.Touch.LeanFingerOld::OnDisable()
extern void LeanFingerOld_OnDisable_m9096AB13BB3A751F3A0A7BE34154B5078453F25F ();
// 0x000002DC System.Void Lean.Touch.LeanFingerOld::HandleFingerOld(Lean.Touch.LeanFinger)
extern void LeanFingerOld_HandleFingerOld_mE1540BF7FF8B8BDE97C226C005DB682924B7C851 ();
// 0x000002DD System.Void Lean.Touch.LeanFingerOld::.ctor()
extern void LeanFingerOld__ctor_mC2FD7FA8ECDA275A1BCCCC63D314445D2BF37174 ();
// 0x000002DE Lean.Touch.LeanFingerSet_LeanFingerEvent Lean.Touch.LeanFingerSet::get_OnFinger()
extern void LeanFingerSet_get_OnFinger_m4E996961E0D10B4968628DFD1E0F94F9223EE29A ();
// 0x000002DF Lean.Touch.LeanFingerSet_Vector2Event Lean.Touch.LeanFingerSet::get_OnDelta()
extern void LeanFingerSet_get_OnDelta_mE2F138D4718235255469AE70A1E7A410864D7120 ();
// 0x000002E0 Lean.Touch.LeanFingerSet_FloatEvent Lean.Touch.LeanFingerSet::get_OnDistance()
extern void LeanFingerSet_get_OnDistance_m4D268DA548AB2FFA3754719844C1B45E2C0CD5C5 ();
// 0x000002E1 Lean.Touch.LeanFingerSet_Vector3Event Lean.Touch.LeanFingerSet::get_OnWorldFrom()
extern void LeanFingerSet_get_OnWorldFrom_m6C33D60530BF50F8B6F6FA29555C834EA2EF741C ();
// 0x000002E2 Lean.Touch.LeanFingerSet_Vector3Event Lean.Touch.LeanFingerSet::get_OnWorldTo()
extern void LeanFingerSet_get_OnWorldTo_m07BE9D21B3BAE46A70986208F7233C8CBE6FDABC ();
// 0x000002E3 Lean.Touch.LeanFingerSet_Vector3Event Lean.Touch.LeanFingerSet::get_OnWorldDelta()
extern void LeanFingerSet_get_OnWorldDelta_m70CAB2310F7B5F4B5E3CEC045F3CEF7AF37A7E7A ();
// 0x000002E4 Lean.Touch.LeanFingerSet_Vector3Vector3Event Lean.Touch.LeanFingerSet::get_OnWorldFromTo()
extern void LeanFingerSet_get_OnWorldFromTo_m145C14F1E6ABDC2661F9E76FCBA52961909B3579 ();
// 0x000002E5 System.Void Lean.Touch.LeanFingerSet::Awake()
extern void LeanFingerSet_Awake_mD7E3039B5245F28E8BD92D3480ADA11063DD123A ();
// 0x000002E6 System.Void Lean.Touch.LeanFingerSet::OnEnable()
extern void LeanFingerSet_OnEnable_mE7BCDEBE2E4ECEF43A91987C313366743E1DD481 ();
// 0x000002E7 System.Void Lean.Touch.LeanFingerSet::OnDisable()
extern void LeanFingerSet_OnDisable_m2021F3A0FB46ECE2B899A3FF17C782F6CB7A3979 ();
// 0x000002E8 System.Void Lean.Touch.LeanFingerSet::HandleFingerSet(Lean.Touch.LeanFinger)
extern void LeanFingerSet_HandleFingerSet_m9C1E0A9CF2F39D516A84FD62EAFF8A27AC1EEA62 ();
// 0x000002E9 System.Void Lean.Touch.LeanFingerSet::.ctor()
extern void LeanFingerSet__ctor_mE1CEB2E33A1CF073808E7A55C71597D95C04BCE2 ();
// 0x000002EA System.Void Lean.Touch.LeanFingerSwipe::Awake()
extern void LeanFingerSwipe_Awake_mE1CBF341152B57803B9BD06A31F27BDD145B8EA4 ();
// 0x000002EB System.Void Lean.Touch.LeanFingerSwipe::OnEnable()
extern void LeanFingerSwipe_OnEnable_mD2F12AAAFD0C208CBCB22CFCED6AA8F3C1A62BA0 ();
// 0x000002EC System.Void Lean.Touch.LeanFingerSwipe::OnDisable()
extern void LeanFingerSwipe_OnDisable_m744E173982D8F8114E1BE45714ABB4AFA0369533 ();
// 0x000002ED System.Void Lean.Touch.LeanFingerSwipe::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanFingerSwipe_HandleFingerSwipe_m45D9B670A4C7C69BD9E5518593F599B9928254A4 ();
// 0x000002EE System.Void Lean.Touch.LeanFingerSwipe::.ctor()
extern void LeanFingerSwipe__ctor_m387DC3B157686F6E8CCC73EA7CC069821B07A2E5 ();
// 0x000002EF Lean.Touch.LeanFingerTap_LeanFingerEvent Lean.Touch.LeanFingerTap::get_OnFinger()
extern void LeanFingerTap_get_OnFinger_m13DB9AE50245E9870148043BFBA5D8BD45E7B4A0 ();
// 0x000002F0 Lean.Touch.LeanFingerTap_Vector3Event Lean.Touch.LeanFingerTap::get_OnPosition()
extern void LeanFingerTap_get_OnPosition_mDD9EE156E799427B1707DADBB84B4D9D4BD53DAE ();
// 0x000002F1 System.Void Lean.Touch.LeanFingerTap::Start()
extern void LeanFingerTap_Start_m505D06462DA7F79EF11C9296D833EA30093C3046 ();
// 0x000002F2 System.Void Lean.Touch.LeanFingerTap::OnEnable()
extern void LeanFingerTap_OnEnable_m5CF577D7892BEC4E184278A244677BB6B81B7E1C ();
// 0x000002F3 System.Void Lean.Touch.LeanFingerTap::OnDisable()
extern void LeanFingerTap_OnDisable_m385FD2EC0808EED6104A155684639E9D2731C1DB ();
// 0x000002F4 System.Void Lean.Touch.LeanFingerTap::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanFingerTap_HandleFingerTap_m28CDAB4656102E6919616AD3F025C9110CCBB89D ();
// 0x000002F5 System.Void Lean.Touch.LeanFingerTap::.ctor()
extern void LeanFingerTap__ctor_m35E19E4BC414151DFF8D8D5307C530D202CE2C07 ();
// 0x000002F6 Lean.Touch.LeanFingerUp_LeanFingerEvent Lean.Touch.LeanFingerUp::get_OnFinger()
extern void LeanFingerUp_get_OnFinger_m73C02A7A8B171B2E149CF0809973AC7CBE0CEA5B ();
// 0x000002F7 Lean.Touch.LeanFingerUp_Vector3Event Lean.Touch.LeanFingerUp::get_OnPosition()
extern void LeanFingerUp_get_OnPosition_m83CA6425BF3FED1329C29AF8BDDA7F34AEF31B79 ();
// 0x000002F8 System.Void Lean.Touch.LeanFingerUp::Start()
extern void LeanFingerUp_Start_m1BB0CB532B5C0C4C357436645F755BAD17AD73BF ();
// 0x000002F9 System.Void Lean.Touch.LeanFingerUp::OnEnable()
extern void LeanFingerUp_OnEnable_m22D91964FA0BC2872C8576890AEEE1C12B8DCE11 ();
// 0x000002FA System.Void Lean.Touch.LeanFingerUp::OnDisable()
extern void LeanFingerUp_OnDisable_m8ECE14FDA09F2D316B21F6A2C53C38A24542D642 ();
// 0x000002FB System.Void Lean.Touch.LeanFingerUp::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanFingerUp_HandleFingerUp_m71728216C183E47BFE1C1740879ED4D45C80ADB4 ();
// 0x000002FC System.Void Lean.Touch.LeanFingerUp::.ctor()
extern void LeanFingerUp__ctor_mF8DB788A1B5DCA96E23373E38A617518CF07F479 ();
// 0x000002FD System.Void Lean.Touch.LeanInfoText::Display(System.String)
extern void LeanInfoText_Display_m114DAB3AE28FA13CC599C8F61C70C687F4A3A408 ();
// 0x000002FE System.Void Lean.Touch.LeanInfoText::Display(System.String,System.String)
extern void LeanInfoText_Display_m9FE1655C1B5C4F4479EC494DB5B2F27391C41019 ();
// 0x000002FF System.Void Lean.Touch.LeanInfoText::Display(System.Int32)
extern void LeanInfoText_Display_m9FF95233EF6D977478480A91F2BA7878E4B3DE30 ();
// 0x00000300 System.Void Lean.Touch.LeanInfoText::Display(System.Single)
extern void LeanInfoText_Display_m1B1F56CA265E6276221D72563E81BFB50DE9D203 ();
// 0x00000301 System.Void Lean.Touch.LeanInfoText::Display(UnityEngine.Vector2)
extern void LeanInfoText_Display_m77908BB5889DAE56FCFF41C1C7CBEF96563680A6 ();
// 0x00000302 System.Void Lean.Touch.LeanInfoText::Display(UnityEngine.Vector3)
extern void LeanInfoText_Display_m4D45AD8DA9EB52B346905FF8076E23F25C7DE2C2 ();
// 0x00000303 System.Void Lean.Touch.LeanInfoText::Display(UnityEngine.Vector4)
extern void LeanInfoText_Display_mB2518593EAC094E0D87AE7AD70CB0F875F9391DE ();
// 0x00000304 System.Void Lean.Touch.LeanInfoText::Display(System.Int32,System.Int32)
extern void LeanInfoText_Display_mAE5A2992CD9038A4099E4EF8D554D2AA2171804E ();
// 0x00000305 System.Void Lean.Touch.LeanInfoText::.ctor()
extern void LeanInfoText__ctor_mE4D892FED00E8CDF0F36C6A7190E63D754610672 ();
// 0x00000306 System.Int32 Lean.Touch.LeanPath::get_PointCount()
extern void LeanPath_get_PointCount_m35C11E12B474775005CE2FF70AE53831FFCE7931 ();
// 0x00000307 System.Int32 Lean.Touch.LeanPath::GetPointCount(System.Int32)
extern void LeanPath_GetPointCount_m5935B0C45DF23EF5DB558274B8BA7B1BCB62EB0C ();
// 0x00000308 UnityEngine.Vector3 Lean.Touch.LeanPath::GetSmoothedPoint(System.Single)
extern void LeanPath_GetSmoothedPoint_m027D12F3FE44CB50F2FF8BDA15DE8AF71FFB2AEA ();
// 0x00000309 UnityEngine.Vector3 Lean.Touch.LeanPath::GetPoint(System.Int32,System.Int32)
extern void LeanPath_GetPoint_mD4BEBBE17852C2ADA4FFD93FD9C8D61D697CE4C2 ();
// 0x0000030A UnityEngine.Vector3 Lean.Touch.LeanPath::GetPointRaw(System.Int32,System.Int32)
extern void LeanPath_GetPointRaw_mFECB2598561519218FAEF764975262F88F9937E5 ();
// 0x0000030B System.Void Lean.Touch.LeanPath::SetLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_SetLine_m89D54996B2E6BC9C4F7F2DA6EEF93289A62F5DB8 ();
// 0x0000030C System.Boolean Lean.Touch.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
extern void LeanPath_TryGetClosest_mAA923A0DD41F711B55FEC934E5A4AFA981AB0D76 ();
// 0x0000030D System.Boolean Lean.Touch.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_m6FAA821E7586AFB89EB2D9E1386B90031C30B5E8 ();
// 0x0000030E System.Boolean Lean.Touch.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
extern void LeanPath_TryGetClosest_mDBCC49A43A5B2660A77BA3FEA3AAB403BDEC4638 ();
// 0x0000030F System.Boolean Lean.Touch.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_mCBE1D9AFE2F3FA7DADC3EE6242F15817DCB430D6 ();
// 0x00000310 System.Boolean Lean.Touch.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32,System.Single)
extern void LeanPath_TryGetClosest_m977C30B03D3574234F29DCDA17F34BD4E03F2E63 ();
// 0x00000311 UnityEngine.Vector3 Lean.Touch.LeanPath::GetClosestPoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_m6C972C673237A1D5589F87F5C2ABAAD014D34171 ();
// 0x00000312 UnityEngine.Vector3 Lean.Touch.LeanPath::GetClosestPoint(UnityEngine.Ray,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_mF97113C8CD459430D714A28F422DB5C5E2680D47 ();
// 0x00000313 System.Single Lean.Touch.LeanPath::GetClosestDistance(UnityEngine.Ray,UnityEngine.Vector3)
extern void LeanPath_GetClosestDistance_m45A8D4BAEEA0148B8B9AAFCE8968CC137CB437A0 ();
// 0x00000314 System.Int32 Lean.Touch.LeanPath::Mod(System.Int32,System.Int32)
extern void LeanPath_Mod_m76A17AE9AFB5E8E89052AE6C5CF433AF30B995F6 ();
// 0x00000315 System.Single Lean.Touch.LeanPath::CubicInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanPath_CubicInterpolate_mB99E7792710EA750B9F1ABCE7090B16CE59FFE93 ();
// 0x00000316 System.Void Lean.Touch.LeanPath::UpdateVisual()
extern void LeanPath_UpdateVisual_m2FDADC837DB5FA0A7B698E513C545C6DDAC9FBB7 ();
// 0x00000317 System.Void Lean.Touch.LeanPath::Update()
extern void LeanPath_Update_m9E018F03B28B08D50E3CE6D17617BEBBAE998B52 ();
// 0x00000318 System.Void Lean.Touch.LeanPath::.ctor()
extern void LeanPath__ctor_m98D432D34C7AE997ABFB409F72D35413038C47EA ();
// 0x00000319 System.Void Lean.Touch.LeanPath::.cctor()
extern void LeanPath__cctor_m2E47A6C3324C1BD784A4C90656495F48901B3FB0 ();
// 0x0000031A System.Void Lean.Touch.LeanPinchScale::AddFinger(Lean.Touch.LeanFinger)
extern void LeanPinchScale_AddFinger_m1273AC965306C88169F04F9F7FABD5976BA85722 ();
// 0x0000031B System.Void Lean.Touch.LeanPinchScale::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanPinchScale_RemoveFinger_m4E2F7EDE1B7FF8573975D401FA9AAACE89622F97 ();
// 0x0000031C System.Void Lean.Touch.LeanPinchScale::RemoveAllFingers()
extern void LeanPinchScale_RemoveAllFingers_m32657CB2488561B6EF30F5FA4BA2D9160F781F3A ();
// 0x0000031D System.Void Lean.Touch.LeanPinchScale::Awake()
extern void LeanPinchScale_Awake_mCE83B9A2A51E45B2F6F13956A377AC4F52B84A2E ();
// 0x0000031E System.Void Lean.Touch.LeanPinchScale::Update()
extern void LeanPinchScale_Update_m72B854E5A4DD95938C80D193BACA8D66E1A2C30D ();
// 0x0000031F System.Void Lean.Touch.LeanPinchScale::TranslateUI(System.Single,UnityEngine.Vector2)
extern void LeanPinchScale_TranslateUI_mAC2CBC534D6CDB7DE36EE7AFFAB83615B0EE64CB ();
// 0x00000320 System.Void Lean.Touch.LeanPinchScale::Translate(System.Single,UnityEngine.Vector2)
extern void LeanPinchScale_Translate_m4BFE769460A44FA7E0F34A20C2FB46BCEEE50438 ();
// 0x00000321 System.Void Lean.Touch.LeanPinchScale::.ctor()
extern void LeanPinchScale__ctor_m768950FA0976989878D2C9DF9B614E7DD6801C29 ();
// 0x00000322 UnityEngine.Vector3 Lean.Touch.LeanPlane::GetClosest(UnityEngine.Vector3,System.Single)
extern void LeanPlane_GetClosest_m631B2575D2352080AEAEBEB07180E3404B063627 ();
// 0x00000323 System.Boolean Lean.Touch.LeanPlane::TryRaycast(UnityEngine.Ray,UnityEngine.Vector3&,System.Single)
extern void LeanPlane_TryRaycast_m457EDD4F3FD48B073CDA797F1E8066B6EA9164CC ();
// 0x00000324 UnityEngine.Vector3 Lean.Touch.LeanPlane::GetClosest(UnityEngine.Ray)
extern void LeanPlane_GetClosest_m8BAB222D7C665311787C2E5CBCA3E5513F6E1C3D ();
// 0x00000325 System.Boolean Lean.Touch.LeanPlane::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Ray,System.Single&)
extern void LeanPlane_Raycast_mADC045C79A30294C25354F0FB530034BE7CE774B ();
// 0x00000326 System.Void Lean.Touch.LeanPlane::.ctor()
extern void LeanPlane__ctor_mC00262ACDD75831E1AF92A54AA870F8707A35E19 ();
// 0x00000327 System.Void Lean.Touch.LeanPulseScale::Update()
extern void LeanPulseScale_Update_m6BF94E8B09A9D2D3311A07ABE2EBED82A5EB19E1 ();
// 0x00000328 System.Void Lean.Touch.LeanPulseScale::.ctor()
extern void LeanPulseScale__ctor_m6AE3883E6766A883F3BD67620872035415E76AB8 ();
// 0x00000329 System.Void Lean.Touch.LeanRoll::IncrementAngle(System.Single)
extern void LeanRoll_IncrementAngle_mF536F52FA9B006EF5067159A2985A8EA18FFBAF4 ();
// 0x0000032A System.Void Lean.Touch.LeanRoll::DecrementAngle(System.Single)
extern void LeanRoll_DecrementAngle_m889C800A056A59E2AB6449A3887182175DF17A29 ();
// 0x0000032B System.Void Lean.Touch.LeanRoll::RotateToDelta(UnityEngine.Vector2)
extern void LeanRoll_RotateToDelta_mD49D137049B4A03076C501F2C8996558F9AEBD18 ();
// 0x0000032C System.Void Lean.Touch.LeanRoll::Start()
extern void LeanRoll_Start_m6F127017A68134C6F9CEC4C4D4A14621D3457FA0 ();
// 0x0000032D System.Void Lean.Touch.LeanRoll::Update()
extern void LeanRoll_Update_m184088A1F671808FF8968F945B6C475F661BE868 ();
// 0x0000032E System.Void Lean.Touch.LeanRoll::.ctor()
extern void LeanRoll__ctor_m5D15A02CDEB15CDCF0A6F8BE2B9816A1865C7A5B ();
// 0x0000032F System.Void Lean.Touch.LeanScreenDepth::.ctor(Lean.Touch.LeanScreenDepth_ConversionType,System.Int32,System.Single)
extern void LeanScreenDepth__ctor_mA8AD2F7C7E663413E39D40569A36D1227943EE7F_AdjustorThunk ();
// 0x00000330 UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::Convert(UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_Convert_mA14287E3B34DEB00EB318E11763F60734D4308D0_AdjustorThunk ();
// 0x00000331 UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::ConvertDelta(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_ConvertDelta_m193F2AD42B0814532671EFF4525DF3A57D2CF13A_AdjustorThunk ();
// 0x00000332 System.Boolean Lean.Touch.LeanScreenDepth::TryConvert(UnityEngine.Vector3&,UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_TryConvert_m9CA913BCFF65194F6E15EAC194E42DF1CB6088B6_AdjustorThunk ();
// 0x00000333 System.Boolean Lean.Touch.LeanScreenDepth::IsChildOf(UnityEngine.Transform,UnityEngine.Transform)
extern void LeanScreenDepth_IsChildOf_m30A45B6D898B352C8941A875C7761ED372C7CF51 ();
// 0x00000334 System.Void Lean.Touch.LeanScreenDepth::.cctor()
extern void LeanScreenDepth__cctor_m0C791BCD2E20C40C4EE1E2EB1F7FD110E0DC74A5 ();
// 0x00000335 System.Void Lean.Touch.LeanSelect::SelectStartScreenPosition(Lean.Touch.LeanFinger)
extern void LeanSelect_SelectStartScreenPosition_m8CB6B86FB2D2DB58757EF17E99772EAC5817884C ();
// 0x00000336 System.Void Lean.Touch.LeanSelect::SelectScreenPosition(Lean.Touch.LeanFinger)
extern void LeanSelect_SelectScreenPosition_m8F589402D1642C2CC09AB10194FC5548F9185D05 ();
// 0x00000337 System.Void Lean.Touch.LeanSelect::SelectScreenPosition(Lean.Touch.LeanFinger,UnityEngine.Vector2)
extern void LeanSelect_SelectScreenPosition_mEC74807DEBACB3682F1D392403EC67809293FA2C ();
// 0x00000338 System.Int32 Lean.Touch.LeanSelect::GetClosestRaycastHitsIndex(System.Int32)
extern void LeanSelect_GetClosestRaycastHitsIndex_mCF4D2F524F44DF843B8441A0C91D0560E672AA71 ();
// 0x00000339 System.Void Lean.Touch.LeanSelect::TryGetComponent(Lean.Touch.LeanSelect_SelectType,UnityEngine.Vector2,UnityEngine.Component&)
extern void LeanSelect_TryGetComponent_m5F51FB86ED7E8E474D74FDD618373B6600F23EBA ();
// 0x0000033A UnityEngine.Vector2 Lean.Touch.LeanSelect::GetScreenPoint(UnityEngine.Camera,UnityEngine.Transform)
extern void LeanSelect_GetScreenPoint_m7E341BDDD9476781C79DBB8E5A1D26A71301463F ();
// 0x0000033B System.Void Lean.Touch.LeanSelect::Select(Lean.Touch.LeanFinger,UnityEngine.Component)
extern void LeanSelect_Select_m5965E7DCE0F5FA8AB491C3C272DEA4B25477191E ();
// 0x0000033C System.Void Lean.Touch.LeanSelect::Select(Lean.Touch.LeanFinger,Lean.Touch.LeanSelectable)
extern void LeanSelect_Select_m895D156285E0CE4279F84C6DD9AD66279254D2B0 ();
// 0x0000033D System.Void Lean.Touch.LeanSelect::DeselectAll()
extern void LeanSelect_DeselectAll_m3FCD75ACF602C94168E19AA72345FD702B819BE2 ();
// 0x0000033E System.Void Lean.Touch.LeanSelect::OnEnable()
extern void LeanSelect_OnEnable_m17E1F03B0CECF13B98E1839038B03FB57409BE9A ();
// 0x0000033F System.Void Lean.Touch.LeanSelect::OnDisable()
extern void LeanSelect_OnDisable_m2DBB65CCC894A40DA6D5C9B7ACFB035E4A8F4996 ();
// 0x00000340 System.Void Lean.Touch.LeanSelect::.ctor()
extern void LeanSelect__ctor_m579F98408B0D05A52331E815C12AC3AFC771A761 ();
// 0x00000341 System.Void Lean.Touch.LeanSelect::.cctor()
extern void LeanSelect__cctor_mCAF13218768B80266EDF88548DA52D4CD386AE59 ();
// 0x00000342 System.Void Lean.Touch.LeanSelectable::add_OnSelectGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_add_OnSelectGlobal_mC460B7598374FAB6CE975F6FB1E7EEFAA5EAD685 ();
// 0x00000343 System.Void Lean.Touch.LeanSelectable::remove_OnSelectGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_remove_OnSelectGlobal_m104449CF1594F2598337C370538AECB13F8392FA ();
// 0x00000344 System.Void Lean.Touch.LeanSelectable::add_OnSelectSetGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_add_OnSelectSetGlobal_mC87576D613ABF4715F9BC798FDF46D696D3A36BF ();
// 0x00000345 System.Void Lean.Touch.LeanSelectable::remove_OnSelectSetGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_remove_OnSelectSetGlobal_mB687F15B432D8A5B2988820B57BEE97A857BB66F ();
// 0x00000346 System.Void Lean.Touch.LeanSelectable::add_OnSelectUpGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_add_OnSelectUpGlobal_m7D4017C794A87B80064693B925682556B82921A0 ();
// 0x00000347 System.Void Lean.Touch.LeanSelectable::remove_OnSelectUpGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_remove_OnSelectUpGlobal_m452B3CD26F2331D18183354F3C88423BEE180BE4 ();
// 0x00000348 System.Void Lean.Touch.LeanSelectable::add_OnDeselectGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_add_OnDeselectGlobal_m1CD0BD15E4C4E692A8846AA181ACC3253672941A ();
// 0x00000349 System.Void Lean.Touch.LeanSelectable::remove_OnDeselectGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_remove_OnDeselectGlobal_m7FE8C3E84075827365ADCFBD0F241969B418A76D ();
// 0x0000034A Lean.Touch.LeanSelectable_LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelect()
extern void LeanSelectable_get_OnSelect_m2FF28065648083E0E760576BB95C408783B91C7D ();
// 0x0000034B Lean.Touch.LeanSelectable_LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelectSet()
extern void LeanSelectable_get_OnSelectSet_m66318685B6C39B57FF615B1879216522F25AE2D5 ();
// 0x0000034C Lean.Touch.LeanSelectable_LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelectUp()
extern void LeanSelectable_get_OnSelectUp_mB451D6480ED943465EB788E07E985BA35AAA8259 ();
// 0x0000034D UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectable::get_OnDeselect()
extern void LeanSelectable_get_OnDeselect_mFAB0917F0994C3A335D74E3F6760587FA69B8A6D ();
// 0x0000034E System.Void Lean.Touch.LeanSelectable::set_IsSelected(System.Boolean)
extern void LeanSelectable_set_IsSelected_m8C8295E09F9B21DEAEFD7739C3FDECF88A40ED72 ();
// 0x0000034F System.Boolean Lean.Touch.LeanSelectable::get_IsSelected()
extern void LeanSelectable_get_IsSelected_mDEF1B82025D83466A05961F130A3A36EB5802726 ();
// 0x00000350 System.Boolean Lean.Touch.LeanSelectable::get_IsSelectedRaw()
extern void LeanSelectable_get_IsSelectedRaw_m441DF6B745F624030D329D2E8D56318180F68E18 ();
// 0x00000351 System.Int32 Lean.Touch.LeanSelectable::get_IsSelectedCount()
extern void LeanSelectable_get_IsSelectedCount_mB11ACF0AF4E472A93E02581E7AAB7D7A1A0FEBC7 ();
// 0x00000352 Lean.Touch.LeanFinger Lean.Touch.LeanSelectable::get_SelectingFinger()
extern void LeanSelectable_get_SelectingFinger_mBD1F902C82BC2B3D312CE73C849E7DE8C8ED2613 ();
// 0x00000353 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::get_SelectingFingers()
extern void LeanSelectable_get_SelectingFingers_mC36E160B113626D8802083A857E79CE04B9D63D3 ();
// 0x00000354 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::GetFingers(System.Boolean,System.Boolean,System.Int32,Lean.Touch.LeanSelectable)
extern void LeanSelectable_GetFingers_m3A9586F30D597992BE4EF3C2BD9D78F8B9CF633C ();
// 0x00000355 System.Void Lean.Touch.LeanSelectable::GetSelected(System.Collections.Generic.List`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_GetSelected_m81DB52B271ED247B6A3C00E95FE7F8B47A506F1C ();
// 0x00000356 System.Void Lean.Touch.LeanSelectable::Cull(System.Int32)
extern void LeanSelectable_Cull_m8B54A37D2717D66848DAB29814A8D960DB6E7677 ();
// 0x00000357 Lean.Touch.LeanSelectable Lean.Touch.LeanSelectable::FindSelectable(Lean.Touch.LeanFinger)
extern void LeanSelectable_FindSelectable_mC46E61F336454BB0BB2D7C8698DF40BB0FC82E1B ();
// 0x00000358 System.Void Lean.Touch.LeanSelectable::ReplaceSelection(Lean.Touch.LeanFinger,System.Collections.Generic.List`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_ReplaceSelection_m240C6623A87BA70B2A982DF00AAB530E3759DEB8 ();
// 0x00000359 System.Boolean Lean.Touch.LeanSelectable::IsSelectedBy(Lean.Touch.LeanFinger)
extern void LeanSelectable_IsSelectedBy_m3E4CBB44125216660CB26B15B7CA518DF61921FC ();
// 0x0000035A System.Boolean Lean.Touch.LeanSelectable::GetIsSelected(System.Boolean)
extern void LeanSelectable_GetIsSelected_m91507700BC4B2FD83481C16AF04CACFECA6C6227 ();
// 0x0000035B System.Void Lean.Touch.LeanSelectable::Select()
extern void LeanSelectable_Select_m9F7FCC48D696C2D30652C090FD5A3CEB7CA22036 ();
// 0x0000035C System.Void Lean.Touch.LeanSelectable::Select(Lean.Touch.LeanFinger)
extern void LeanSelectable_Select_m4340163D89E713EA2D8D06898B8812A9FD22DCFC ();
// 0x0000035D System.Void Lean.Touch.LeanSelectable::Deselect()
extern void LeanSelectable_Deselect_m3677B356106B62ED823482A38799891047D7B852 ();
// 0x0000035E System.Void Lean.Touch.LeanSelectable::DeselectAll()
extern void LeanSelectable_DeselectAll_m042578CDC470FAB9B61A083C93B83ADD0EC29296 ();
// 0x0000035F System.Void Lean.Touch.LeanSelectable::OnEnable()
extern void LeanSelectable_OnEnable_mE154B0985D3D571F12C5486D4692674B42FCE20E ();
// 0x00000360 System.Void Lean.Touch.LeanSelectable::OnDisable()
extern void LeanSelectable_OnDisable_mF9F40E5E223C7E9E5FDA23938873AE866D0E4F27 ();
// 0x00000361 System.Void Lean.Touch.LeanSelectable::BuildTempSelectables(Lean.Touch.LeanFinger)
extern void LeanSelectable_BuildTempSelectables_m2E970414174E22581745C774558FD70597F90597 ();
// 0x00000362 System.Void Lean.Touch.LeanSelectable::HandleFingerSet(Lean.Touch.LeanFinger)
extern void LeanSelectable_HandleFingerSet_m20AD509AD58239BBC215D83F7A54605B8DEFB507 ();
// 0x00000363 System.Boolean Lean.Touch.LeanSelectable::get_AnyFingersSet()
extern void LeanSelectable_get_AnyFingersSet_m5DC6D0DB741E15B4FEBF49D5213BF5CD96868C95 ();
// 0x00000364 System.Void Lean.Touch.LeanSelectable::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanSelectable_HandleFingerUp_m54F25D8E849D9287CC2FC86FC5B1FBC50E2510C5 ();
// 0x00000365 System.Void Lean.Touch.LeanSelectable::HandleFingerInactive(Lean.Touch.LeanFinger)
extern void LeanSelectable_HandleFingerInactive_m92C9491555F01B4848282E89029B017C019F4BD2 ();
// 0x00000366 System.Void Lean.Touch.LeanSelectable::.ctor()
extern void LeanSelectable__ctor_m13F42644E9E31229FF8724DE75F55C9F565BD5A1 ();
// 0x00000367 System.Void Lean.Touch.LeanSelectable::.cctor()
extern void LeanSelectable__cctor_m97EA10BF63B66BEA764968433ADB94CAA0F65E61 ();
// 0x00000368 Lean.Touch.LeanSelectable Lean.Touch.LeanSelectableBehaviour::get_Selectable()
extern void LeanSelectableBehaviour_get_Selectable_mE3F3D1AA53A1853265FF9BE70803BC783655B273 ();
// 0x00000369 System.Void Lean.Touch.LeanSelectableBehaviour::Register()
extern void LeanSelectableBehaviour_Register_mC02BDC99405A8CB40DFCDAD18228F83980739DEE ();
// 0x0000036A System.Void Lean.Touch.LeanSelectableBehaviour::Register(Lean.Touch.LeanSelectable)
extern void LeanSelectableBehaviour_Register_mB2458CAA97B4527993E5513FABF788AF21BE17D1 ();
// 0x0000036B System.Void Lean.Touch.LeanSelectableBehaviour::Unregister()
extern void LeanSelectableBehaviour_Unregister_mC71A010F690A9FF141D135069718614626DE8196 ();
// 0x0000036C System.Void Lean.Touch.LeanSelectableBehaviour::OnEnable()
extern void LeanSelectableBehaviour_OnEnable_mCEF867C219F5A6CB0F720180C3DA71080419DA3C ();
// 0x0000036D System.Void Lean.Touch.LeanSelectableBehaviour::Start()
extern void LeanSelectableBehaviour_Start_mDC2AFA56C8E42B8C5566125A03738CBCA82772B2 ();
// 0x0000036E System.Void Lean.Touch.LeanSelectableBehaviour::OnDisable()
extern void LeanSelectableBehaviour_OnDisable_m4BBD195762CFD88CE79BF65D50673C2BCFB4CB39 ();
// 0x0000036F System.Void Lean.Touch.LeanSelectableBehaviour::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableBehaviour_OnSelect_m17BBA732DE70A5C42396924679BD72D43E4617CE ();
// 0x00000370 System.Void Lean.Touch.LeanSelectableBehaviour::OnSelectUp(Lean.Touch.LeanFinger)
extern void LeanSelectableBehaviour_OnSelectUp_m71878502F461D29249752DEF6D1F6D6D70765D58 ();
// 0x00000371 System.Void Lean.Touch.LeanSelectableBehaviour::OnDeselect()
extern void LeanSelectableBehaviour_OnDeselect_mB2985F4134D98578811200C44E661724F727DCB4 ();
// 0x00000372 System.Void Lean.Touch.LeanSelectableBehaviour::.ctor()
extern void LeanSelectableBehaviour__ctor_mBB899FC0E6DD2C2EE6BA4CAA5BDDBFBDEE8918AC ();
// 0x00000373 System.Void Lean.Touch.LeanSelectableGraphicColor::Awake()
extern void LeanSelectableGraphicColor_Awake_m23F3DC9CD83ADCE9F86A00D12A68FFCBD2BB94C8 ();
// 0x00000374 System.Void Lean.Touch.LeanSelectableGraphicColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableGraphicColor_OnSelect_m3A8842040AA55D21A568EA84676591CAA7CA9CB4 ();
// 0x00000375 System.Void Lean.Touch.LeanSelectableGraphicColor::OnDeselect()
extern void LeanSelectableGraphicColor_OnDeselect_mBF06233F6E10E195B57DB2D49322D873FBFD2F55 ();
// 0x00000376 System.Void Lean.Touch.LeanSelectableGraphicColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableGraphicColor_ChangeColor_mBEF737F5C679019A6B1DD100EFC5B116B7624109 ();
// 0x00000377 System.Void Lean.Touch.LeanSelectableGraphicColor::.ctor()
extern void LeanSelectableGraphicColor__ctor_m3F031F153741F4D25782F6F36CE79B303D1CD11F ();
// 0x00000378 System.Void Lean.Touch.LeanSelectableRendererColor::Awake()
extern void LeanSelectableRendererColor_Awake_m94FA76D34113A46C79FADF09918CF0D83995110B ();
// 0x00000379 System.Void Lean.Touch.LeanSelectableRendererColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableRendererColor_OnSelect_m29209B299AA70241A31316E806C17FEAF280DEC0 ();
// 0x0000037A System.Void Lean.Touch.LeanSelectableRendererColor::OnDeselect()
extern void LeanSelectableRendererColor_OnDeselect_m55A1C66DBAC602198A7B9C99AF80F1C1CF3E0799 ();
// 0x0000037B System.Void Lean.Touch.LeanSelectableRendererColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableRendererColor_ChangeColor_m58D07D6FCE72EC3492C159E61C4B345FBD028A11 ();
// 0x0000037C System.Void Lean.Touch.LeanSelectableRendererColor::.ctor()
extern void LeanSelectableRendererColor__ctor_m185BCB6867F50842A649D7FCF9F2AF7743086D63 ();
// 0x0000037D System.Void Lean.Touch.LeanSelectableSpriteRendererColor::Awake()
extern void LeanSelectableSpriteRendererColor_Awake_mAF8C52DB81CB7B0360002580AB989EF4123D8BDD ();
// 0x0000037E System.Void Lean.Touch.LeanSelectableSpriteRendererColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableSpriteRendererColor_OnSelect_m23254EE2154E46F702EDF435A8090DB056574381 ();
// 0x0000037F System.Void Lean.Touch.LeanSelectableSpriteRendererColor::OnDeselect()
extern void LeanSelectableSpriteRendererColor_OnDeselect_m8DCE24E9AEBA972AFCB8B9E952F76D25436B51D7 ();
// 0x00000380 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableSpriteRendererColor_ChangeColor_mE925B6B41741B2F97B883BE78C24AF530E39828F ();
// 0x00000381 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::.ctor()
extern void LeanSelectableSpriteRendererColor__ctor_m8EF60DF6EC30930898BD50BD67E6963764BCD837 ();
// 0x00000382 System.Void Lean.Touch.LeanSpawn::Spawn()
extern void LeanSpawn_Spawn_m1D90636F1A728A1F4E92382E639318CDDC805CAA ();
// 0x00000383 System.Void Lean.Touch.LeanSpawn::Spawn(UnityEngine.Vector3)
extern void LeanSpawn_Spawn_mB3854C1825D7112B2A2A948E7FB135597B5BAFBF ();
// 0x00000384 System.Void Lean.Touch.LeanSpawn::.ctor()
extern void LeanSpawn__ctor_m8D3B816F48B9BD13A413F68D47EF6512E901681B ();
// 0x00000385 Lean.Touch.LeanSwipeBase_LeanFingerEvent Lean.Touch.LeanSwipeBase::get_OnFinger()
extern void LeanSwipeBase_get_OnFinger_mB2261EDA0DFF2366E78396181161B8DA3E5A4F8D ();
// 0x00000386 Lean.Touch.LeanSwipeBase_Vector2Event Lean.Touch.LeanSwipeBase::get_OnDelta()
extern void LeanSwipeBase_get_OnDelta_m6BC14BAFA279A35B344533AA411593EFAECAB1B3 ();
// 0x00000387 Lean.Touch.LeanSwipeBase_FloatEvent Lean.Touch.LeanSwipeBase::get_OnDistance()
extern void LeanSwipeBase_get_OnDistance_m58B841675172FE16DF8E7991918C5757582B46C8 ();
// 0x00000388 Lean.Touch.LeanSwipeBase_Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldFrom()
extern void LeanSwipeBase_get_OnWorldFrom_m665B1AB74B086A9C379E404FABC3DDF6BFCBED7C ();
// 0x00000389 Lean.Touch.LeanSwipeBase_Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldTo()
extern void LeanSwipeBase_get_OnWorldTo_m6572FD6F5DD41BDDB66C5BCD47DD80DC98A17B06 ();
// 0x0000038A Lean.Touch.LeanSwipeBase_Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldDelta()
extern void LeanSwipeBase_get_OnWorldDelta_m3A2B45DA76FAF73FF854039BDA0D225E406F7052 ();
// 0x0000038B Lean.Touch.LeanSwipeBase_Vector3Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldFromTo()
extern void LeanSwipeBase_get_OnWorldFromTo_m39B0AEA11081A20324C0B5DE52EE0B465074B3F0 ();
// 0x0000038C System.Boolean Lean.Touch.LeanSwipeBase::AngleIsValid(UnityEngine.Vector2)
extern void LeanSwipeBase_AngleIsValid_m14CDA8EBD9CC7DC05D381E4E3869E0DC3BC444EF ();
// 0x0000038D System.Void Lean.Touch.LeanSwipeBase::HandleFingerSwipe(Lean.Touch.LeanFinger,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanSwipeBase_HandleFingerSwipe_m84E5C39C725B9CF3A6306FE272AC0EFE1B16DE8B ();
// 0x0000038E System.Void Lean.Touch.LeanSwipeBase::.ctor()
extern void LeanSwipeBase__ctor_m45648B5A0A12AA09D03C6F01D86249FAD20C4318 ();
// 0x0000038F System.Void Lean.Touch.LeanTouchEvents::OnEnable()
extern void LeanTouchEvents_OnEnable_m54C1901998D364914C8632D89056FBC5D78FA799 ();
// 0x00000390 System.Void Lean.Touch.LeanTouchEvents::OnDisable()
extern void LeanTouchEvents_OnDisable_mA49DB8DFD064C960863EF880A1F453031CD3C405 ();
// 0x00000391 System.Void Lean.Touch.LeanTouchEvents::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerDown_m16739E77EFA4AD4F0797DA3F5B544A183928B1B3 ();
// 0x00000392 System.Void Lean.Touch.LeanTouchEvents::HandleFingerSet(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerSet_m3609A3100342B1F648ACE44E712A78E68D538B4F ();
// 0x00000393 System.Void Lean.Touch.LeanTouchEvents::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerUp_m5FFAEA1AEC048EF1098EDE08638CB0656CD1D29D ();
// 0x00000394 System.Void Lean.Touch.LeanTouchEvents::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerTap_m94B823177D3CF5922849A5F12334F936D9C5E8AA ();
// 0x00000395 System.Void Lean.Touch.LeanTouchEvents::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerSwipe_mACF109B0E72CBDF522BFEC41947A4B099C557BE2 ();
// 0x00000396 System.Void Lean.Touch.LeanTouchEvents::HandleGesture(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanTouchEvents_HandleGesture_m4841ED6F8E8E48691288BCCAC4B8D295D7540645 ();
// 0x00000397 System.Void Lean.Touch.LeanTouchEvents::.ctor()
extern void LeanTouchEvents__ctor_mA2741A93D716B7267AD8183E017231503E5A6B94 ();
// 0x00000398 System.Void Lean.Touch.LeanTwistRotate::AddFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotate_AddFinger_mF6BBAC92B955DDFBD57D52028C597DF8A110551D ();
// 0x00000399 System.Void Lean.Touch.LeanTwistRotate::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotate_RemoveFinger_m22ECD8971AFE60BC39EF1D091200BF017235F636 ();
// 0x0000039A System.Void Lean.Touch.LeanTwistRotate::RemoveAllFingers()
extern void LeanTwistRotate_RemoveAllFingers_m1D2F33150CA0C9B5D1FE37F04F20795711C4001C ();
// 0x0000039B System.Void Lean.Touch.LeanTwistRotate::Awake()
extern void LeanTwistRotate_Awake_mE47A226208B4625A888723D3ADBD7395D8B786A5 ();
// 0x0000039C System.Void Lean.Touch.LeanTwistRotate::Update()
extern void LeanTwistRotate_Update_mB9EDA12F33015EE7F7E9248DFB847AF376C5DF11 ();
// 0x0000039D System.Void Lean.Touch.LeanTwistRotate::TranslateUI(System.Single,UnityEngine.Vector2)
extern void LeanTwistRotate_TranslateUI_m4D4F4EF26B834092795D83ED4A2F947BC13CF9AB ();
// 0x0000039E System.Void Lean.Touch.LeanTwistRotate::Translate(System.Single,UnityEngine.Vector2)
extern void LeanTwistRotate_Translate_m8F287535A70F388E8406AD8EAA3F5A9C0E15CE7D ();
// 0x0000039F System.Void Lean.Touch.LeanTwistRotate::RotateUI(System.Single)
extern void LeanTwistRotate_RotateUI_m25AE38201B70C7AF3D4DC68CE03412F4E9725445 ();
// 0x000003A0 System.Void Lean.Touch.LeanTwistRotate::Rotate(System.Single)
extern void LeanTwistRotate_Rotate_m327DBB47495304D8A63D592DC3CCB0909B28627E ();
// 0x000003A1 System.Void Lean.Touch.LeanTwistRotate::.ctor()
extern void LeanTwistRotate__ctor_mAA680EC4B6951766F6272DA806353C8D3F0767A6 ();
// 0x000003A2 System.Void Lean.Touch.LeanTwistRotateAxis::AddFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotateAxis_AddFinger_mF4EB435EB73467CD43192371E576E1365F490496 ();
// 0x000003A3 System.Void Lean.Touch.LeanTwistRotateAxis::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotateAxis_RemoveFinger_m9CA82D0AC717663DC3BE8A06B32A0EFFE79398C1 ();
// 0x000003A4 System.Void Lean.Touch.LeanTwistRotateAxis::RemoveAllFingers()
extern void LeanTwistRotateAxis_RemoveAllFingers_mB033D05CFEDCD6A6B5EA5F88BFB3DD510D8ECF68 ();
// 0x000003A5 System.Void Lean.Touch.LeanTwistRotateAxis::Awake()
extern void LeanTwistRotateAxis_Awake_m28ACAE8335CB02BBF8A37EBF5060A8F11D584BFC ();
// 0x000003A6 System.Void Lean.Touch.LeanTwistRotateAxis::Update()
extern void LeanTwistRotateAxis_Update_m94F74F6966232AF05608B5C266528888DD7D63B4 ();
// 0x000003A7 System.Void Lean.Touch.LeanTwistRotateAxis::.ctor()
extern void LeanTwistRotateAxis__ctor_m7EE35206EB9182FAF6B23095FF3A286EB75C08BF ();
// 0x000003A8 System.Boolean Lean.Touch.LeanFinger::get_IsActive()
extern void LeanFinger_get_IsActive_mCC63B4B3C8AB34F997395FE9BFB35C87C33179D9 ();
// 0x000003A9 System.Single Lean.Touch.LeanFinger::get_SnapshotDuration()
extern void LeanFinger_get_SnapshotDuration_mC6B4AFE80D4F115DF135819D7CC5FF78087B936A ();
// 0x000003AA System.Boolean Lean.Touch.LeanFinger::get_IsOverGui()
extern void LeanFinger_get_IsOverGui_m27CD8EC84C3A3E1BA8AD360505874C56EAFF737D ();
// 0x000003AB System.Boolean Lean.Touch.LeanFinger::get_Down()
extern void LeanFinger_get_Down_mC794AB80DB8FCA8AF2970A59E82589D296731DDF ();
// 0x000003AC System.Boolean Lean.Touch.LeanFinger::get_Up()
extern void LeanFinger_get_Up_m286EA975B43946B0205BD8CAB38DDC1831D813B1 ();
// 0x000003AD UnityEngine.Vector2 Lean.Touch.LeanFinger::get_LastSnapshotScreenDelta()
extern void LeanFinger_get_LastSnapshotScreenDelta_m45068F5F8F2000676D34B4ABEDBAE4BD53C5B6C3 ();
// 0x000003AE UnityEngine.Vector2 Lean.Touch.LeanFinger::get_LastSnapshotScaledDelta()
extern void LeanFinger_get_LastSnapshotScaledDelta_m527F35C7674FE8F6EA865FB4D6E000C7D412F4FC ();
// 0x000003AF UnityEngine.Vector2 Lean.Touch.LeanFinger::get_ScreenDelta()
extern void LeanFinger_get_ScreenDelta_m7B809F527C34F3253466B75E608530A7004BDDA2 ();
// 0x000003B0 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_ScaledDelta()
extern void LeanFinger_get_ScaledDelta_m4CB396F460ED375D6094E3FEAA1061C4563A5E38 ();
// 0x000003B1 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_SwipeScreenDelta()
extern void LeanFinger_get_SwipeScreenDelta_m77B23F2EB08E7EBD0AE23780D54D0549F1DE72A5 ();
// 0x000003B2 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_SwipeScaledDelta()
extern void LeanFinger_get_SwipeScaledDelta_mFB187D350D7C08E14F2F2902AC9BBE6C9CD36F01 ();
// 0x000003B3 UnityEngine.Ray Lean.Touch.LeanFinger::GetRay(UnityEngine.Camera)
extern void LeanFinger_GetRay_m76DFF8B71221306A2D675FF44E632A382350D9F3 ();
// 0x000003B4 UnityEngine.Ray Lean.Touch.LeanFinger::GetStartRay(UnityEngine.Camera)
extern void LeanFinger_GetStartRay_m7761B01CF82BAF9E420A25E6393C1A140B911ADB ();
// 0x000003B5 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScreenDelta(System.Single)
extern void LeanFinger_GetSnapshotScreenDelta_m02D2BF648C39E3D620974F16B5321B5F9F685A6A ();
// 0x000003B6 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScaledDelta(System.Single)
extern void LeanFinger_GetSnapshotScaledDelta_m02E31797CEA5BBDF6F045CCB8BFF1D6F434CBD91 ();
// 0x000003B7 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScreenPosition(System.Single)
extern void LeanFinger_GetSnapshotScreenPosition_m8523ADFF6B507F9E9952B281656AD2DC763B6DC6 ();
// 0x000003B8 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetSnapshotWorldPosition(System.Single,System.Single,UnityEngine.Camera)
extern void LeanFinger_GetSnapshotWorldPosition_m12DF66F851E073BB9935114B2422C8AE271DB810 ();
// 0x000003B9 System.Single Lean.Touch.LeanFinger::GetRadians(UnityEngine.Vector2)
extern void LeanFinger_GetRadians_m6F76F20D2800780366E90E3609E61AE8EFB58AB8 ();
// 0x000003BA System.Single Lean.Touch.LeanFinger::GetDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetDegrees_mF2D51A8856BE2AEB83387FC5C76FEC6B90125A73 ();
// 0x000003BB System.Single Lean.Touch.LeanFinger::GetLastRadians(UnityEngine.Vector2)
extern void LeanFinger_GetLastRadians_m1EFE92DD7E39F4E0702779B474228F00B95C4204 ();
// 0x000003BC System.Single Lean.Touch.LeanFinger::GetLastDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetLastDegrees_mECD743918766F89789B06ABDD6A87B0D54ACA8DB ();
// 0x000003BD System.Single Lean.Touch.LeanFinger::GetDeltaRadians(UnityEngine.Vector2)
extern void LeanFinger_GetDeltaRadians_m3A2D02D2CB3D24F76CC76F165A78F2C3E1D89405 ();
// 0x000003BE System.Single Lean.Touch.LeanFinger::GetDeltaRadians(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFinger_GetDeltaRadians_m6B010F29593EA5BE2BFC52683102F9C4EC157523 ();
// 0x000003BF System.Single Lean.Touch.LeanFinger::GetDeltaDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetDeltaDegrees_mD928FC314F607A62BE3137EC54087EC058C410D6 ();
// 0x000003C0 System.Single Lean.Touch.LeanFinger::GetDeltaDegrees(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFinger_GetDeltaDegrees_mC07667C868496C9D1BDA196CCD65C8F49732C737 ();
// 0x000003C1 System.Single Lean.Touch.LeanFinger::GetScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetScreenDistance_m0E57D4448AEDB91EA57FA27A4C9F26ECE3C3229E ();
// 0x000003C2 System.Single Lean.Touch.LeanFinger::GetScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetScaledDistance_m795170666A96D1C30E8F2DEFD74F7A2EF661D1A8 ();
// 0x000003C3 System.Single Lean.Touch.LeanFinger::GetLastScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetLastScreenDistance_mBD91726C445FA114BBCD70BB63E09C83DA06D94A ();
// 0x000003C4 System.Single Lean.Touch.LeanFinger::GetLastScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetLastScaledDistance_m2DED4DAEB091F349DC8FD047BA899BD0A9E44430 ();
// 0x000003C5 System.Single Lean.Touch.LeanFinger::GetStartScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetStartScreenDistance_m79103E21F4D42DEE59978A050DC9CA0BF9D986BF ();
// 0x000003C6 System.Single Lean.Touch.LeanFinger::GetStartScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetStartScaledDistance_mA506EF7A073D1D8A71D17C3973E860F097CEDC8C ();
// 0x000003C7 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetStartWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetStartWorldPosition_m7BB0617A8E70A760F500681FE4522AA0DFE65DD4 ();
// 0x000003C8 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetLastWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetLastWorldPosition_m30E17D240A999024C6BFF4446DF1AD206FD0E15C ();
// 0x000003C9 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldPosition_m0BC212B62E8E9DC10A33D66A6A9F7B33E11FE1A5 ();
// 0x000003CA UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldDelta(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldDelta_mCCAA3761ACAD3BA8FA37C7BBCB99D55BC1C5FC21 ();
// 0x000003CB UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldDelta(System.Single,System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldDelta_m4171A03A6C6C0662249E99BB52EA157EEC924A92 ();
// 0x000003CC System.Void Lean.Touch.LeanFinger::ClearSnapshots(System.Int32)
extern void LeanFinger_ClearSnapshots_mE9E8949942022D73BA03E849D32B1325A8374FA3 ();
// 0x000003CD System.Void Lean.Touch.LeanFinger::RecordSnapshot()
extern void LeanFinger_RecordSnapshot_mAB5607ADD4E332558A62C28E383ABBAA88578E74 ();
// 0x000003CE System.Void Lean.Touch.LeanFinger::.ctor()
extern void LeanFinger__ctor_mF44EE089C3F3584D15469D80CF7D54B07CF3FF63 ();
// 0x000003CF UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenCenter()
extern void LeanGesture_GetScreenCenter_m37C3A3F7C2FEBBB4C675704400F79D7723FA942D ();
// 0x000003D0 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenCenter_m153528341362034489E7D0C19730D2F68F291ED8 ();
// 0x000003D1 System.Boolean Lean.Touch.LeanGesture::TryGetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScreenCenter_m437D51BD4F6DB63A1E7807D7E6C3549B039DE224 ();
// 0x000003D2 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetLastScreenCenter()
extern void LeanGesture_GetLastScreenCenter_m1C165CEDC1A51F3B17228A95A3F4D06F140300B3 ();
// 0x000003D3 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetLastScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScreenCenter_mA1CBC57DF82A9A527DF240199E7D957C5C8C6A4C ();
// 0x000003D4 System.Boolean Lean.Touch.LeanGesture::TryGetLastScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetLastScreenCenter_mD5AA2C7AC03E32BF2174AA07E492B3519E336E47 ();
// 0x000003D5 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetStartScreenCenter()
extern void LeanGesture_GetStartScreenCenter_mF577CB8C669E33BB563A330B26CA44081FEFACE7 ();
// 0x000003D6 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetStartScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScreenCenter_mF89A029DECE5044A3DFD9497BABDB6FDBCAC59E7 ();
// 0x000003D7 System.Boolean Lean.Touch.LeanGesture::TryGetStartScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetStartScreenCenter_mA826DC2BC7A0E2285D45E7BB83E692955E60A3E0 ();
// 0x000003D8 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenDelta()
extern void LeanGesture_GetScreenDelta_mD24464147EE9500A9AA3EA417BB2AEC59F881183 ();
// 0x000003D9 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenDelta_m1EA13DA5DDEF07CD86D22435377FEB4A30B815CF ();
// 0x000003DA System.Boolean Lean.Touch.LeanGesture::TryGetScreenDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScreenDelta_m511B7A35C6165FBA66EB620AB361800A2F73E11A ();
// 0x000003DB UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScaledDelta()
extern void LeanGesture_GetScaledDelta_m5A28890F651193E1F17EAB9DE995CC4B70EE8652 ();
// 0x000003DC UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScaledDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScaledDelta_mA5BD346430C4F795E24335B581E647B5E64403AD ();
// 0x000003DD System.Boolean Lean.Touch.LeanGesture::TryGetScaledDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScaledDelta_m2006FC6D64C9E061F795887FAEB1B91C562324E6 ();
// 0x000003DE UnityEngine.Vector3 Lean.Touch.LeanGesture::GetWorldDelta(System.Single,UnityEngine.Camera)
extern void LeanGesture_GetWorldDelta_m28A9A163A4789F9EAB8B78D5189DFDD21AC88C2D ();
// 0x000003DF UnityEngine.Vector3 Lean.Touch.LeanGesture::GetWorldDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single,UnityEngine.Camera)
extern void LeanGesture_GetWorldDelta_mE063AC165BF76F5ADF7A2F0EC249FA4FF45FEC36 ();
// 0x000003E0 System.Boolean Lean.Touch.LeanGesture::TryGetWorldDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single,UnityEngine.Vector3&,UnityEngine.Camera)
extern void LeanGesture_TryGetWorldDelta_m4096DABCD0C5925A5BD5DBA490B65951FBD93E14 ();
// 0x000003E1 System.Single Lean.Touch.LeanGesture::GetScreenDistance()
extern void LeanGesture_GetScreenDistance_mAEDE196D1C9CABEA07A41B61A0C9C16B19626F53 ();
// 0x000003E2 System.Single Lean.Touch.LeanGesture::GetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenDistance_mF6B566EEAF731C23B2705D667A9617A2D603F5EF ();
// 0x000003E3 System.Single Lean.Touch.LeanGesture::GetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetScreenDistance_m8E87EA8A0231EA66E5BF8B9179BA2F5B89EB36D7 ();
// 0x000003E4 System.Boolean Lean.Touch.LeanGesture::TryGetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetScreenDistance_m4C999C2911C40C13BDAC0D12F10AAF43B9115559 ();
// 0x000003E5 System.Single Lean.Touch.LeanGesture::GetScaledDistance()
extern void LeanGesture_GetScaledDistance_mECA5C99A60B0779CE4A07042A8F214347BD6F932 ();
// 0x000003E6 System.Single Lean.Touch.LeanGesture::GetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScaledDistance_mE9AF8539F34402EA93E6529E87A50BF7FFDEC588 ();
// 0x000003E7 System.Single Lean.Touch.LeanGesture::GetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetScaledDistance_m520C012CCB1D46FBD3C95EA9C052DD5FE91B82E7 ();
// 0x000003E8 System.Boolean Lean.Touch.LeanGesture::TryGetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetScaledDistance_m73C2D79A5B327D4A97CC6011C2030B913F431F32 ();
// 0x000003E9 System.Single Lean.Touch.LeanGesture::GetLastScreenDistance()
extern void LeanGesture_GetLastScreenDistance_m465207ED0648D6D73F0CAEDE89E07CF980139883 ();
// 0x000003EA System.Single Lean.Touch.LeanGesture::GetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScreenDistance_mA691336027AE9E47FED41F48F7762F2B4012645F ();
// 0x000003EB System.Single Lean.Touch.LeanGesture::GetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetLastScreenDistance_m9DD3D45DDDE052DF119EF19AC65EA0DFA46E6065 ();
// 0x000003EC System.Boolean Lean.Touch.LeanGesture::TryGetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetLastScreenDistance_m6B1E58F127C84E13CA45E0A4842E802BCC4FFF6A ();
// 0x000003ED System.Single Lean.Touch.LeanGesture::GetLastScaledDistance()
extern void LeanGesture_GetLastScaledDistance_mB5E2903766A908E2263535C91E06C89C111506A0 ();
// 0x000003EE System.Single Lean.Touch.LeanGesture::GetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScaledDistance_m91A2D0545A8A0D217214929370D74D6FD1864A26 ();
// 0x000003EF System.Single Lean.Touch.LeanGesture::GetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetLastScaledDistance_m464C51B93C8CFE37567E1A0B5E33B4CCEFC97E7F ();
// 0x000003F0 System.Boolean Lean.Touch.LeanGesture::TryGetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetLastScaledDistance_m3BDAF07C0615964CDAFA0D02C57EA38606C75708 ();
// 0x000003F1 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance()
extern void LeanGesture_GetStartScreenDistance_mBCA22635F54BA6AB964BBF6863CC6E74B9BB5B1F ();
// 0x000003F2 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScreenDistance_m3FA56525E6F87D77F30E24A7695D7C126494B05B ();
// 0x000003F3 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetStartScreenDistance_m9FF43E1D2CE46ABFFBC25FC22061998E1E646933 ();
// 0x000003F4 System.Boolean Lean.Touch.LeanGesture::TryGetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetStartScreenDistance_m6C06303CF1F726B513640CC61B60B6E366203701 ();
// 0x000003F5 System.Single Lean.Touch.LeanGesture::GetStartScaledDistance()
extern void LeanGesture_GetStartScaledDistance_mAAE3DDC2AD57847511D5AD70D5489AE9CF8262E7 ();
// 0x000003F6 System.Single Lean.Touch.LeanGesture::GetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScaledDistance_mC3525166EE87AE13193F58765C977B56EFAA88DA ();
// 0x000003F7 System.Single Lean.Touch.LeanGesture::GetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetStartScaledDistance_m82E08A5C0AABA7CDEEB7DF84625BD6E71CF4B4C6 ();
// 0x000003F8 System.Boolean Lean.Touch.LeanGesture::TryGetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetStartScaledDistance_m798DACD98A5D02640F90E1E951F9590093682EC1 ();
// 0x000003F9 System.Single Lean.Touch.LeanGesture::GetPinchScale(System.Single)
extern void LeanGesture_GetPinchScale_m1B2344BE840691081265B194743A7A4B13A79470 ();
// 0x000003FA System.Single Lean.Touch.LeanGesture::GetPinchScale(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single)
extern void LeanGesture_GetPinchScale_m8EAA29932D6C9B1D234FD21A0BC65DFC982A727E ();
// 0x000003FB System.Boolean Lean.Touch.LeanGesture::TryGetPinchScale(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single)
extern void LeanGesture_TryGetPinchScale_m245473873F06E23DF3C262FB11057C2851B6A64B ();
// 0x000003FC System.Single Lean.Touch.LeanGesture::GetPinchRatio(System.Single)
extern void LeanGesture_GetPinchRatio_mA637FED4A16EF0D3C84157682CD5DD537513C168 ();
// 0x000003FD System.Single Lean.Touch.LeanGesture::GetPinchRatio(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single)
extern void LeanGesture_GetPinchRatio_mD3916FABA5C5FEE6CD3040B35893303F3750DB74 ();
// 0x000003FE System.Boolean Lean.Touch.LeanGesture::TryGetPinchRatio(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single)
extern void LeanGesture_TryGetPinchRatio_m9950EDFF3165BC736BC0BF0940DF5FF5170C2F49 ();
// 0x000003FF System.Single Lean.Touch.LeanGesture::GetTwistDegrees()
extern void LeanGesture_GetTwistDegrees_m93431159FE6747CEA576CC75ED2FF307D3292338 ();
// 0x00000400 System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetTwistDegrees_m58C1B6B48D1225D9CC0424F2506AAF434102591E ();
// 0x00000401 System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanGesture_GetTwistDegrees_m76EB2AE9169C405BA682A82EE7D9531294B904DB ();
// 0x00000402 System.Boolean Lean.Touch.LeanGesture::TryGetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetTwistDegrees_m2E7CE5271F73735D8963CEDBA39101F3EC762EE8 ();
// 0x00000403 System.Single Lean.Touch.LeanGesture::GetTwistRadians()
extern void LeanGesture_GetTwistRadians_mAA84476BF790E55FBE8B72C141C84DD049A75C42 ();
// 0x00000404 System.Single Lean.Touch.LeanGesture::GetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetTwistRadians_m16BBC7F9365F906E217790108F2EC2C98A49710A ();
// 0x00000405 System.Single Lean.Touch.LeanGesture::GetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanGesture_GetTwistRadians_m517E7492F60E53097CED52BB7A87FB2E4B4BF50E ();
// 0x00000406 System.Boolean Lean.Touch.LeanGesture::TryGetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetTwistRadians_mB4362950E2DCEA79D05B6FDF8D732FD3491CE017 ();
// 0x00000407 UnityEngine.Vector3 Lean.Touch.LeanSnapshot::GetWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanSnapshot_GetWorldPosition_m618A37581C9A14C75E093D0C662E2B2EEA1768EC ();
// 0x00000408 Lean.Touch.LeanSnapshot Lean.Touch.LeanSnapshot::Pop()
extern void LeanSnapshot_Pop_mBDB6D46A91F554985BCC3071B2DDF147AB587869 ();
// 0x00000409 System.Boolean Lean.Touch.LeanSnapshot::TryGetScreenPosition(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Single,UnityEngine.Vector2&)
extern void LeanSnapshot_TryGetScreenPosition_m96909D25960B97E90608BE16FD63C79AD05DB9FC ();
// 0x0000040A System.Boolean Lean.Touch.LeanSnapshot::TryGetSnapshot(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Int32,System.Single&,UnityEngine.Vector2&)
extern void LeanSnapshot_TryGetSnapshot_mA162D816D9440F32480FAB534F86907175ED5283 ();
// 0x0000040B System.Int32 Lean.Touch.LeanSnapshot::GetLowerIndex(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Single)
extern void LeanSnapshot_GetLowerIndex_mC9B320B53AD22A382AA6D85BA41A7098FBB0FBAE ();
// 0x0000040C System.Void Lean.Touch.LeanSnapshot::.ctor()
extern void LeanSnapshot__ctor_m29A2A5F0922BA4931A84B119D7B8FD80DE1F3559 ();
// 0x0000040D System.Void Lean.Touch.LeanSnapshot::.cctor()
extern void LeanSnapshot__cctor_mE5A06C835C35CD1A09D76B3C5F45944552A829A8 ();
// 0x0000040E System.Void Lean.Touch.LeanTouch::add_OnFingerDown(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerDown_mAD6D901E481055FA098326D04678C46C009DA7C6 ();
// 0x0000040F System.Void Lean.Touch.LeanTouch::remove_OnFingerDown(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerDown_mAC7DDAD27F149E64073412B5E3C1D48FC5CCA2D0 ();
// 0x00000410 System.Void Lean.Touch.LeanTouch::add_OnFingerSet(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerSet_mF6567EDA4FFAB42115785ACE68B4C4699AE7AEBE ();
// 0x00000411 System.Void Lean.Touch.LeanTouch::remove_OnFingerSet(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerSet_mD986A740023E6AE3C6C1093B0BF54D359693408F ();
// 0x00000412 System.Void Lean.Touch.LeanTouch::add_OnFingerUp(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerUp_m672656853F5BDBB4DFF1BEF7FEE4FF28FA25C1DA ();
// 0x00000413 System.Void Lean.Touch.LeanTouch::remove_OnFingerUp(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerUp_m9E4C292CF2D0D60C93F9DA38E858D36E3A727E68 ();
// 0x00000414 System.Void Lean.Touch.LeanTouch::add_OnFingerOld(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerOld_m3FDD59C1AF070FA09C9D2AB6F23AE2DC8135E04E ();
// 0x00000415 System.Void Lean.Touch.LeanTouch::remove_OnFingerOld(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerOld_mCE1697C5B4D4D348709FE10AAFE540371D6A3699 ();
// 0x00000416 System.Void Lean.Touch.LeanTouch::add_OnFingerTap(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerTap_mC0DA8BBFEE3ED8DA36A5EE467CEF7D3FFC9A1455 ();
// 0x00000417 System.Void Lean.Touch.LeanTouch::remove_OnFingerTap(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerTap_m359B1CF629C74D9B17D344BEC42B0AD74E8DCDAF ();
// 0x00000418 System.Void Lean.Touch.LeanTouch::add_OnFingerSwipe(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerSwipe_mCF01BDB87D48507F0975A0034494F1B8511752A1 ();
// 0x00000419 System.Void Lean.Touch.LeanTouch::remove_OnFingerSwipe(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerSwipe_m0ECFB745A17FA08957D28ED1F782BD47EAD6846E ();
// 0x0000041A System.Void Lean.Touch.LeanTouch::add_OnGesture(System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>)
extern void LeanTouch_add_OnGesture_mEA1914326B6319D148429260746A8506FB2A1173 ();
// 0x0000041B System.Void Lean.Touch.LeanTouch::remove_OnGesture(System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>)
extern void LeanTouch_remove_OnGesture_m5E29A5A97FDAB2A9E939744C038A9639CCBE7817 ();
// 0x0000041C System.Void Lean.Touch.LeanTouch::add_OnFingerExpired(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerExpired_m0FAEC5E8C7B4E5BC50CB9877231745B1F9EC26A9 ();
// 0x0000041D System.Void Lean.Touch.LeanTouch::remove_OnFingerExpired(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerExpired_mEB1A8E8CD136F0A67B887E88B383CBDFF05971F3 ();
// 0x0000041E System.Void Lean.Touch.LeanTouch::add_OnFingerInactive(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerInactive_m28DDE042BB5BB86DD79396F03BC43BE05907AB07 ();
// 0x0000041F System.Void Lean.Touch.LeanTouch::remove_OnFingerInactive(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerInactive_m8369AEE01AC33148A6F7D4A371BF550ECA01D35F ();
// 0x00000420 System.Single Lean.Touch.LeanTouch::get_CurrentTapThreshold()
extern void LeanTouch_get_CurrentTapThreshold_m82B0F9B2B8A89688FCD8F7D4B699AB8C7AEC37F2 ();
// 0x00000421 System.Single Lean.Touch.LeanTouch::get_CurrentSwipeThreshold()
extern void LeanTouch_get_CurrentSwipeThreshold_m38C03E29A00B5C6C2318C9A7B680851CBF6BF7F2 ();
// 0x00000422 System.Int32 Lean.Touch.LeanTouch::get_CurrentReferenceDpi()
extern void LeanTouch_get_CurrentReferenceDpi_m59908D8B95671F22C89C3683A8710F4C8673B904 ();
// 0x00000423 UnityEngine.LayerMask Lean.Touch.LeanTouch::get_CurrentGuiLayers()
extern void LeanTouch_get_CurrentGuiLayers_m4435F6ADCA6EEA42B65576559ED0E5240E768707 ();
// 0x00000424 Lean.Touch.LeanTouch Lean.Touch.LeanTouch::get_Instance()
extern void LeanTouch_get_Instance_m7F8B0C399AC8AC56CC2B2138C039B1EAAA3D6911 ();
// 0x00000425 System.Single Lean.Touch.LeanTouch::get_ScalingFactor()
extern void LeanTouch_get_ScalingFactor_m011FA5C02F85074984E3BAE6015DEDC0AE3E5196 ();
// 0x00000426 System.Single Lean.Touch.LeanTouch::get_ScreenFactor()
extern void LeanTouch_get_ScreenFactor_m9C0200769EA75A55B3624F3240ED767A06D46D0A ();
// 0x00000427 System.Boolean Lean.Touch.LeanTouch::get_GuiInUse()
extern void LeanTouch_get_GuiInUse_m16F43865EFB038A7D609587CCC3FA3C2204D6BE9 ();
// 0x00000428 UnityEngine.Camera Lean.Touch.LeanTouch::GetCamera(UnityEngine.Camera,UnityEngine.GameObject)
extern void LeanTouch_GetCamera_m68DD8BF68EDE0611083479E2BD288F265B9D5B44 ();
// 0x00000429 System.Single Lean.Touch.LeanTouch::GetDampenFactor(System.Single,System.Single)
extern void LeanTouch_GetDampenFactor_m9DC6418C421D9216565FBEBA291C4A50BABF4E04 ();
// 0x0000042A System.Boolean Lean.Touch.LeanTouch::PointOverGui(UnityEngine.Vector2)
extern void LeanTouch_PointOverGui_mF8EF2B64A1B7219CEAACB4973D158FCE88131F20 ();
// 0x0000042B System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::RaycastGui(UnityEngine.Vector2)
extern void LeanTouch_RaycastGui_m7C8D73F6390157C64564017A61B529D29C5AB29F ();
// 0x0000042C System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::RaycastGui(UnityEngine.Vector2,UnityEngine.LayerMask)
extern void LeanTouch_RaycastGui_mA0D00DB14CE24FA18B0346076DA42CF63757C818 ();
// 0x0000042D System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::GetFingers(System.Boolean,System.Boolean,System.Int32)
extern void LeanTouch_GetFingers_m44739A10124E36D6012FFB052AEDF43F73973F20 ();
// 0x0000042E System.Void Lean.Touch.LeanTouch::Awake()
extern void LeanTouch_Awake_mD2AB3BB732D7BA081C1CFB82864093360BE4C14C ();
// 0x0000042F System.Void Lean.Touch.LeanTouch::OnEnable()
extern void LeanTouch_OnEnable_m543F1B15F56E6FF84C01343B0F7A1BA2EE0762B7 ();
// 0x00000430 System.Void Lean.Touch.LeanTouch::OnDisable()
extern void LeanTouch_OnDisable_mA2CC5E5C9B13E989F42E2329195B37B194FDCAD7 ();
// 0x00000431 System.Void Lean.Touch.LeanTouch::Update()
extern void LeanTouch_Update_mF9D289FB40E357027982AC6CFF89BA269FD30077 ();
// 0x00000432 System.Void Lean.Touch.LeanTouch::OnGUI()
extern void LeanTouch_OnGUI_m7E8A9F3037014413BBC08589D5312DE4F958E0E2 ();
// 0x00000433 System.Void Lean.Touch.LeanTouch::BeginFingers()
extern void LeanTouch_BeginFingers_m2B2803272A0D2B6ADC5411F1ADB9745CF6149FFE ();
// 0x00000434 System.Void Lean.Touch.LeanTouch::EndFingers()
extern void LeanTouch_EndFingers_mBD43F9D096CA974B8B8429D8A98FBF68B82899C7 ();
// 0x00000435 System.Void Lean.Touch.LeanTouch::PollFingers()
extern void LeanTouch_PollFingers_m498C6D067AF5DDF00AB71DAFF27933D7F10BD22B ();
// 0x00000436 System.Void Lean.Touch.LeanTouch::UpdateEvents()
extern void LeanTouch_UpdateEvents_mADAEB1538B3CBFBA9FE3F3B1FDB1A8893FB743CB ();
// 0x00000437 System.Void Lean.Touch.LeanTouch::AddFinger(System.Int32,UnityEngine.Vector2,System.Single,System.Boolean)
extern void LeanTouch_AddFinger_m54E6B12911C218B2E513A094917D610FBE27E479 ();
// 0x00000438 Lean.Touch.LeanFinger Lean.Touch.LeanTouch::FindFinger(System.Int32)
extern void LeanTouch_FindFinger_m451C4D2CCB2E62DE5A7FA94CEA717ABCEFDA7AED ();
// 0x00000439 System.Int32 Lean.Touch.LeanTouch::FindInactiveFingerIndex(System.Int32)
extern void LeanTouch_FindInactiveFingerIndex_mAAE1FFFEE7088F1C5AD4EA96E72DF5980F60880B ();
// 0x0000043A System.Void Lean.Touch.LeanTouch::.ctor()
extern void LeanTouch__ctor_m35821163E87FD2C3C7FB29DB1C04D22C1A806303 ();
// 0x0000043B System.Void Lean.Touch.LeanTouch::.cctor()
extern void LeanTouch__cctor_m9DBAD251AB1D38BD882F06F6426833F2C2B408EE ();
// 0x0000043C T Lean.Common.LeanHelper::CreateElement(UnityEngine.Transform)
// 0x0000043D System.Single Lean.Common.LeanHelper::DampenFactor(System.Single,System.Single)
extern void LeanHelper_DampenFactor_m09FEB3B752E8FB2F5988D64ECC5FA7DD746F7ADD ();
// 0x0000043E T Lean.Common.LeanHelper::Destroy(T)
// 0x0000043F System.Void Lean.Common.Examples.LeanCircuit::UpdateMesh()
extern void LeanCircuit_UpdateMesh_m62FDE5563B031B795D3D2EE0800F749BE27F3A75 ();
// 0x00000440 System.Void Lean.Common.Examples.LeanCircuit::Populate()
extern void LeanCircuit_Populate_m52201C64A088CC4DE542171E391CB15AF604ABB3 ();
// 0x00000441 System.Void Lean.Common.Examples.LeanCircuit::Start()
extern void LeanCircuit_Start_m050782C1BFDC6C1D238CAFA1BB6CDBA56DF428E0 ();
// 0x00000442 System.Void Lean.Common.Examples.LeanCircuit::AddLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern void LeanCircuit_AddLine_m192FD9DC2177AC9236F77537B11C24ADF735C907 ();
// 0x00000443 System.Void Lean.Common.Examples.LeanCircuit::AddPoint(UnityEngine.Vector3,System.Single,UnityEngine.Color)
extern void LeanCircuit_AddPoint_m7E414AC5ED3070CB00C3433ADAC9551B968A8119 ();
// 0x00000444 System.Void Lean.Common.Examples.LeanCircuit::AddNode(UnityEngine.Vector3)
extern void LeanCircuit_AddNode_mA484BEB844AF60C61136273B65F876AB6631F623 ();
// 0x00000445 System.Void Lean.Common.Examples.LeanCircuit::.ctor()
extern void LeanCircuit__ctor_m4ACCE6F63E778CDB3028C220CEE9079B278E76D4 ();
// 0x00000446 System.Void Lean.Common.Examples.LeanCircuit::.cctor()
extern void LeanCircuit__cctor_mD624E36DF349167D1FCDCC9EAA35F2C72D78EEAE ();
// 0x00000447 UnityEngine.Texture2D Lean.Common.Examples.LeanDocumentation::get_Icon()
extern void LeanDocumentation_get_Icon_mDE468E078B1C592A9C1B31ACF5ED0F21878ABD6F ();
// 0x00000448 System.Void Lean.Common.Examples.LeanDocumentation::.ctor()
extern void LeanDocumentation__ctor_m0F59D864C88F976BFA06C390A2C49241C1E19D67 ();
// 0x00000449 System.Void Lean.Common.Examples.LeanLinkTo::set_Link(Lean.Common.Examples.LeanLinkTo_LinkType)
extern void LeanLinkTo_set_Link_mF58AA9286EE6DA805FBBAC5FAE552FE669141550 ();
// 0x0000044A Lean.Common.Examples.LeanLinkTo_LinkType Lean.Common.Examples.LeanLinkTo::get_Link()
extern void LeanLinkTo_get_Link_m1EEB4340AA0D6AA34AC5DCC668D6C6D2C57164B0 ();
// 0x0000044B System.Void Lean.Common.Examples.LeanLinkTo::Update()
extern void LeanLinkTo_Update_mCC1F424F06C5E5E58545ECB0DC6A6B4EC74C6A54 ();
// 0x0000044C System.Void Lean.Common.Examples.LeanLinkTo::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void LeanLinkTo_OnPointerClick_m4CCD9E622354AF37AD8C7F6916499BDA3ACD76DD ();
// 0x0000044D System.Int32 Lean.Common.Examples.LeanLinkTo::GetCurrentLevel()
extern void LeanLinkTo_GetCurrentLevel_m0811F7E70375466B3ADB77570296F49C9490D473 ();
// 0x0000044E System.Int32 Lean.Common.Examples.LeanLinkTo::GetLevelCount()
extern void LeanLinkTo_GetLevelCount_m7C23062B191B24EA1F4864522B70E6D6B6D7FA38 ();
// 0x0000044F System.Void Lean.Common.Examples.LeanLinkTo::LoadLevel(System.Int32)
extern void LeanLinkTo_LoadLevel_m2F12A3EF4B2BEEA666536C7E639EC751A1EC83FE ();
// 0x00000450 System.Void Lean.Common.Examples.LeanLinkTo::.ctor()
extern void LeanLinkTo__ctor_mE6E34A9637F063C9A3264525620F00C8BAE07DA9 ();
// 0x00000451 System.Void LoadFromCacheAndDownload_<LoadBundle>d__9::.ctor(System.Int32)
extern void U3CLoadBundleU3Ed__9__ctor_m9DB1CEF2A9592E64FF4C54FE27159A4BE3DCBDBF ();
// 0x00000452 System.Void LoadFromCacheAndDownload_<LoadBundle>d__9::System.IDisposable.Dispose()
extern void U3CLoadBundleU3Ed__9_System_IDisposable_Dispose_mFAEC968ADCEE1CD3B977ACADE10F75D19FEC0F48 ();
// 0x00000453 System.Boolean LoadFromCacheAndDownload_<LoadBundle>d__9::MoveNext()
extern void U3CLoadBundleU3Ed__9_MoveNext_m57EFE19E1D39BD0C5E01E97DE965F3DA21F3A08E ();
// 0x00000454 System.Object LoadFromCacheAndDownload_<LoadBundle>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadBundleU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m97DD39AC7EBD803B86EE1312D504A199C44293C2 ();
// 0x00000455 System.Void LoadFromCacheAndDownload_<LoadBundle>d__9::System.Collections.IEnumerator.Reset()
extern void U3CLoadBundleU3Ed__9_System_Collections_IEnumerator_Reset_mEF7E2248587DE9ABC40B6A3763F8F27D98820401 ();
// 0x00000456 System.Object LoadFromCacheAndDownload_<LoadBundle>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CLoadBundleU3Ed__9_System_Collections_IEnumerator_get_Current_m27732D1CBB06625A3129B8708DC3D4E5AA2CD675 ();
// 0x00000457 System.Void AnimHandler_<ShotAnimEnd>d__26::.ctor(System.Int32)
extern void U3CShotAnimEndU3Ed__26__ctor_m34DE0EC666E7BFD629E29BCDBC6AFFD877BCEFF2 ();
// 0x00000458 System.Void AnimHandler_<ShotAnimEnd>d__26::System.IDisposable.Dispose()
extern void U3CShotAnimEndU3Ed__26_System_IDisposable_Dispose_m3649F821B248433A1B4CDDAD1672AA00C8614009 ();
// 0x00000459 System.Boolean AnimHandler_<ShotAnimEnd>d__26::MoveNext()
extern void U3CShotAnimEndU3Ed__26_MoveNext_m2BB211C625B7F14A6858EF311A10386DFE003369 ();
// 0x0000045A System.Object AnimHandler_<ShotAnimEnd>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShotAnimEndU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m984758DBDCA4108A2E0633F98DD1AA0CB2A8E689 ();
// 0x0000045B System.Void AnimHandler_<ShotAnimEnd>d__26::System.Collections.IEnumerator.Reset()
extern void U3CShotAnimEndU3Ed__26_System_Collections_IEnumerator_Reset_m07CD4B224EF25AD5109D27035CA1B74B4123E667 ();
// 0x0000045C System.Object AnimHandler_<ShotAnimEnd>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CShotAnimEndU3Ed__26_System_Collections_IEnumerator_get_Current_mC5D2D6382194180037E4BF43B46E1297C572F888 ();
// 0x0000045D System.Void AnimHandler_<ShotAnimEndAR>d__31::.ctor(System.Int32)
extern void U3CShotAnimEndARU3Ed__31__ctor_m1354EF68EB72B065CCAE6A5C0E255B9730ED45DD ();
// 0x0000045E System.Void AnimHandler_<ShotAnimEndAR>d__31::System.IDisposable.Dispose()
extern void U3CShotAnimEndARU3Ed__31_System_IDisposable_Dispose_m008CBA2153CEC486F22E5DDD267BF39A2C5D6292 ();
// 0x0000045F System.Boolean AnimHandler_<ShotAnimEndAR>d__31::MoveNext()
extern void U3CShotAnimEndARU3Ed__31_MoveNext_m9F17AC16EE745FF8839F831F80589AD6C989AFA8 ();
// 0x00000460 System.Object AnimHandler_<ShotAnimEndAR>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShotAnimEndARU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD720681B127C8629994884A7952A5B1A88ACA2BF ();
// 0x00000461 System.Void AnimHandler_<ShotAnimEndAR>d__31::System.Collections.IEnumerator.Reset()
extern void U3CShotAnimEndARU3Ed__31_System_Collections_IEnumerator_Reset_mDBF1B2E95FBF23F2AEBAB29E56489352ADC2D49E ();
// 0x00000462 System.Object AnimHandler_<ShotAnimEndAR>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CShotAnimEndARU3Ed__31_System_Collections_IEnumerator_get_Current_mF0E0084AD74EF00CECE88CD2018C90E794CC48C0 ();
// 0x00000463 System.Void AnimHandler_<TransShotAnimEnd>d__34::.ctor(System.Int32)
extern void U3CTransShotAnimEndU3Ed__34__ctor_m4C703244FEC69809C9BDD2AD962E0DEFC5834316 ();
// 0x00000464 System.Void AnimHandler_<TransShotAnimEnd>d__34::System.IDisposable.Dispose()
extern void U3CTransShotAnimEndU3Ed__34_System_IDisposable_Dispose_mA5BC48104B79A3A96037965E6871C1C93FC8A703 ();
// 0x00000465 System.Boolean AnimHandler_<TransShotAnimEnd>d__34::MoveNext()
extern void U3CTransShotAnimEndU3Ed__34_MoveNext_mE0CE599C585D16E3EA668D4BE3975C930B1E0203 ();
// 0x00000466 System.Object AnimHandler_<TransShotAnimEnd>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransShotAnimEndU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B9361CB0004A39DABF715E7DA4A130F09E9C384 ();
// 0x00000467 System.Void AnimHandler_<TransShotAnimEnd>d__34::System.Collections.IEnumerator.Reset()
extern void U3CTransShotAnimEndU3Ed__34_System_Collections_IEnumerator_Reset_m0E3D406283FABC99E831D77754E847675E4A9089 ();
// 0x00000468 System.Object AnimHandler_<TransShotAnimEnd>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CTransShotAnimEndU3Ed__34_System_Collections_IEnumerator_get_Current_m93BA4A85AB5C9DABF5E5EC5FC69B9C6EF7F95C38 ();
// 0x00000469 System.Void UI_Handler_<TakeScreenshotAndSave>d__12::.ctor(System.Int32)
extern void U3CTakeScreenshotAndSaveU3Ed__12__ctor_mE4ECA813184992818F861079C0D0AF3DB077616D ();
// 0x0000046A System.Void UI_Handler_<TakeScreenshotAndSave>d__12::System.IDisposable.Dispose()
extern void U3CTakeScreenshotAndSaveU3Ed__12_System_IDisposable_Dispose_m0B01AFD3FFD412794F788643981EF8761A544056 ();
// 0x0000046B System.Boolean UI_Handler_<TakeScreenshotAndSave>d__12::MoveNext()
extern void U3CTakeScreenshotAndSaveU3Ed__12_MoveNext_mCDF28BC374BEF35FA986F261A6A918F61045D239 ();
// 0x0000046C System.Object UI_Handler_<TakeScreenshotAndSave>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m621132C7554A10AF0038A508745FF0FBE6B8693C ();
// 0x0000046D System.Void UI_Handler_<TakeScreenshotAndSave>d__12::System.Collections.IEnumerator.Reset()
extern void U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_IEnumerator_Reset_mCD6C81342223466277E49C045FF45BBCBEA58DC5 ();
// 0x0000046E System.Object UI_Handler_<TakeScreenshotAndSave>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_IEnumerator_get_Current_m12B05A4869F5B19BADC1E46BDCB4141BC5F4153B ();
// 0x0000046F System.Void FingerEventDetector`1_FingerEventHandler::.ctor(System.Object,System.IntPtr)
// 0x00000470 System.Void FingerEventDetector`1_FingerEventHandler::Invoke(T)
// 0x00000471 System.IAsyncResult FingerEventDetector`1_FingerEventHandler::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x00000472 System.Void FingerEventDetector`1_FingerEventHandler::EndInvoke(System.IAsyncResult)
// 0x00000473 System.Void Gesture_EventHandler::.ctor(System.Object,System.IntPtr)
extern void EventHandler__ctor_mF2D7ECEC16E656E310C9A9E0821F92B1A8F946A6 ();
// 0x00000474 System.Void Gesture_EventHandler::Invoke(Gesture)
extern void EventHandler_Invoke_m27849E53870CF49BA84DB9A69601E0668947B8F0 ();
// 0x00000475 System.IAsyncResult Gesture_EventHandler::BeginInvoke(Gesture,System.AsyncCallback,System.Object)
extern void EventHandler_BeginInvoke_mD65889040C063DA6F036B6A6FBAD36301FEF7503 ();
// 0x00000476 System.Void Gesture_EventHandler::EndInvoke(System.IAsyncResult)
extern void EventHandler_EndInvoke_mBA37A2B17D3F3D5E3C09E0B9E01F158A72FD7052 ();
// 0x00000477 System.Void GestureRecognizer`1_GestureEventHandler::.ctor(System.Object,System.IntPtr)
// 0x00000478 System.Void GestureRecognizer`1_GestureEventHandler::Invoke(T)
// 0x00000479 System.IAsyncResult GestureRecognizer`1_GestureEventHandler::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x0000047A System.Void GestureRecognizer`1_GestureEventHandler::EndInvoke(System.IAsyncResult)
// 0x0000047B System.Void GestureRecognizer`1_<>c__DisplayClass17_0::.ctor()
// 0x0000047C System.Boolean GestureRecognizer`1_<>c__DisplayClass17_0::<FindGestureByCluster>b__0(T)
// 0x0000047D System.Void GestureRecognizer`1_<>c::.cctor()
// 0x0000047E System.Void GestureRecognizer`1_<>c::.ctor()
// 0x0000047F System.Boolean GestureRecognizer`1_<>c::<FindFreeGesture>b__19_0(T)
// 0x00000480 System.Void FingerClusterManager_Cluster::Reset()
extern void Cluster_Reset_mFCCBECAD7D2197EDFF6783AC5A7A1CDDF13468AD ();
// 0x00000481 System.Void FingerClusterManager_Cluster::.ctor()
extern void Cluster__ctor_m3A08C653BD75B2272E095501DBB6791B8CC04464 ();
// 0x00000482 System.Void FingerClusterManager_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m459996655C106102249CEDE73643E0BAD6230EEF ();
// 0x00000483 System.Boolean FingerClusterManager_<>c__DisplayClass18_0::<FindClusterById>b__0(FingerClusterManager_Cluster)
extern void U3CU3Ec__DisplayClass18_0_U3CFindClusterByIdU3Eb__0_m8F69AAC937320B83F312479DEF0319CEBFAF02B7 ();
// 0x00000484 System.Void FingerGestures_InputProviderEvent::.ctor()
extern void InputProviderEvent__ctor_m0AEE8426E33532B1E4FF3A51EBEA97D6EE417162 ();
// 0x00000485 System.Int32 FingerGestures_Finger::get_Index()
extern void Finger_get_Index_mC781179F5228495E4E712FA231DB622D23D047AD ();
// 0x00000486 System.Boolean FingerGestures_Finger::get_IsDown()
extern void Finger_get_IsDown_mFB1A682A228C2C0CBAC6F14E951CBA4571B659FB ();
// 0x00000487 FingerGestures_FingerPhase FingerGestures_Finger::get_Phase()
extern void Finger_get_Phase_mC0FEA52D1E3616F92DA0FBEF81291D4D55D9994E ();
// 0x00000488 FingerGestures_FingerPhase FingerGestures_Finger::get_PreviousPhase()
extern void Finger_get_PreviousPhase_m74BE8F480FE2058BA1794E0DB135F37C912CE1C9 ();
// 0x00000489 System.Boolean FingerGestures_Finger::get_WasDown()
extern void Finger_get_WasDown_mFB9EE9910243344428612C50E351C3C2D7846FEA ();
// 0x0000048A System.Boolean FingerGestures_Finger::get_IsMoving()
extern void Finger_get_IsMoving_m46C93B1C53AF8D57476A860943A995D7A557138D ();
// 0x0000048B System.Boolean FingerGestures_Finger::get_WasMoving()
extern void Finger_get_WasMoving_mA008997EC29AFA406CAA206E952C47399AF1FC14 ();
// 0x0000048C System.Boolean FingerGestures_Finger::get_IsStationary()
extern void Finger_get_IsStationary_m71F9B80579B17B709078B7927F542E08C2678F3C ();
// 0x0000048D System.Boolean FingerGestures_Finger::get_WasStationary()
extern void Finger_get_WasStationary_m0462329554477BDA6B1B89C8CD1E50EFC90F3FD0 ();
// 0x0000048E System.Boolean FingerGestures_Finger::get_Moved()
extern void Finger_get_Moved_m096260CD4116E0485FF62AF19683ED4DA7048907 ();
// 0x0000048F System.Single FingerGestures_Finger::get_StarTime()
extern void Finger_get_StarTime_m2A81BD271B597F071FB0E1ED33157902F3F99FA9 ();
// 0x00000490 UnityEngine.Vector2 FingerGestures_Finger::get_StartPosition()
extern void Finger_get_StartPosition_mAAE9424F2ACFD924E293DA00610E2BCE0B39BED4 ();
// 0x00000491 UnityEngine.Vector2 FingerGestures_Finger::get_Position()
extern void Finger_get_Position_m55D65B4EC87B9BD75EF265375162A1A9AFA32DE0 ();
// 0x00000492 UnityEngine.Vector2 FingerGestures_Finger::get_PreviousPosition()
extern void Finger_get_PreviousPosition_mB33AEC9F577929EF86FF44BD573E9F9C9CCAF5FC ();
// 0x00000493 UnityEngine.Vector2 FingerGestures_Finger::get_DeltaPosition()
extern void Finger_get_DeltaPosition_mBD956CBBB334AC1E12613BBBAB08FE48ADE639CF ();
// 0x00000494 System.Single FingerGestures_Finger::get_DistanceFromStart()
extern void Finger_get_DistanceFromStart_m219915EDFDBB0B7FA9530AE10781B3673E9C3257 ();
// 0x00000495 System.Boolean FingerGestures_Finger::get_IsFiltered()
extern void Finger_get_IsFiltered_m007743AAD85699258F1BC0789B5AD426D56C1DF4 ();
// 0x00000496 System.Single FingerGestures_Finger::get_TimeStationary()
extern void Finger_get_TimeStationary_m5F29DAF06D9ECE0CED2964DC0A24A3CDB7701666 ();
// 0x00000497 System.Collections.Generic.List`1<GestureRecognizer> FingerGestures_Finger::get_GestureRecognizers()
extern void Finger_get_GestureRecognizers_mA412521C0BCBF5E051D6EAF80584E1EFA834BF44 ();
// 0x00000498 System.Collections.Generic.Dictionary`2<System.String,System.Object> FingerGestures_Finger::get_ExtendedProperties()
extern void Finger_get_ExtendedProperties_m88FFA7CA5922695316CAC2BA5975C97DBCD4434A ();
// 0x00000499 System.Void FingerGestures_Finger::.ctor(System.Int32)
extern void Finger__ctor_m83F0B7426542D8D8A72388CDD66661EDF67316B0 ();
// 0x0000049A System.String FingerGestures_Finger::ToString()
extern void Finger_ToString_m49995A733400A5B3E81DD73D481F3EA0729BF220 ();
// 0x0000049B System.Void FingerGestures_Finger::Update(System.Boolean,UnityEngine.Vector2)
extern void Finger_Update_mE198A0632C13C9B6B7F9456E81FDAE86F388B513 ();
// 0x0000049C System.Void FingerGestures_GlobalTouchFilterDelegate::.ctor(System.Object,System.IntPtr)
extern void GlobalTouchFilterDelegate__ctor_m2FDE30D11BA6F771234E509188208D4534D8C201 ();
// 0x0000049D System.Boolean FingerGestures_GlobalTouchFilterDelegate::Invoke(System.Int32,UnityEngine.Vector2)
extern void GlobalTouchFilterDelegate_Invoke_mA2ED1AC9B86FC470E46A1FFCB41F08BC06F9FDEF ();
// 0x0000049E System.IAsyncResult FingerGestures_GlobalTouchFilterDelegate::BeginInvoke(System.Int32,UnityEngine.Vector2,System.AsyncCallback,System.Object)
extern void GlobalTouchFilterDelegate_BeginInvoke_m0B7D97C482F9FE60C8526945B55870CA3E1E83BE ();
// 0x0000049F System.Boolean FingerGestures_GlobalTouchFilterDelegate::EndInvoke(System.IAsyncResult)
extern void GlobalTouchFilterDelegate_EndInvoke_m3C2524637D802E1B6E50602C9C600C56B9088C32 ();
// 0x000004A0 FingerGestures_Finger FingerGestures_IFingerList::get_Item(System.Int32)
// 0x000004A1 System.Int32 FingerGestures_IFingerList::get_Count()
// 0x000004A2 UnityEngine.Vector2 FingerGestures_IFingerList::GetAverageStartPosition()
// 0x000004A3 UnityEngine.Vector2 FingerGestures_IFingerList::GetAveragePosition()
// 0x000004A4 UnityEngine.Vector2 FingerGestures_IFingerList::GetAveragePreviousPosition()
// 0x000004A5 System.Single FingerGestures_IFingerList::GetAverageDistanceFromStart()
// 0x000004A6 FingerGestures_Finger FingerGestures_IFingerList::GetOldest()
// 0x000004A7 System.Boolean FingerGestures_IFingerList::AllMoving()
// 0x000004A8 System.Boolean FingerGestures_IFingerList::MovingInSameDirection(System.Single)
// 0x000004A9 System.Void FingerGestures_FingerList::.ctor()
extern void FingerList__ctor_m7AED2F6A8076D19569082AC5D93A4268488E16D2 ();
// 0x000004AA System.Void FingerGestures_FingerList::.ctor(System.Collections.Generic.List`1<FingerGestures_Finger>)
extern void FingerList__ctor_m8C266E08A11041F458FF34C0D48ACDBD45AB9A33 ();
// 0x000004AB FingerGestures_Finger FingerGestures_FingerList::get_Item(System.Int32)
extern void FingerList_get_Item_m94E3A9725D8DBEDF74C26868116E75B6F7ED5FC9 ();
// 0x000004AC System.Int32 FingerGestures_FingerList::get_Count()
extern void FingerList_get_Count_m1E6324860C7603A0A163BE4F3D7D8F1C6B721B42 ();
// 0x000004AD System.Collections.Generic.IEnumerator`1<FingerGestures_Finger> FingerGestures_FingerList::GetEnumerator()
extern void FingerList_GetEnumerator_m3CD8CA11CAC5A89D39588C45AD0D0C5009E20AB1 ();
// 0x000004AE System.Collections.IEnumerator FingerGestures_FingerList::System.Collections.IEnumerable.GetEnumerator()
extern void FingerList_System_Collections_IEnumerable_GetEnumerator_m2EEDDAA7D86E2E7C63D00348E874643D81987FFF ();
// 0x000004AF System.Void FingerGestures_FingerList::Add(FingerGestures_Finger)
extern void FingerList_Add_m4E9E2A571B3250F506D11D6FD519133652B8FFE1 ();
// 0x000004B0 System.Boolean FingerGestures_FingerList::Remove(FingerGestures_Finger)
extern void FingerList_Remove_mD7BF90AD64A9D853AED4090AF8140D2512973F75 ();
// 0x000004B1 System.Boolean FingerGestures_FingerList::Contains(FingerGestures_Finger)
extern void FingerList_Contains_m27F037C7685DE35F112BCE094067036C747DC6D8 ();
// 0x000004B2 System.Void FingerGestures_FingerList::AddRange(System.Collections.Generic.IEnumerable`1<FingerGestures_Finger>)
extern void FingerList_AddRange_mC06B617096EAF3BC98EAF4ABB0BDD4D56E9E7C47 ();
// 0x000004B3 System.Void FingerGestures_FingerList::Clear()
extern void FingerList_Clear_m7778B1AE6DBACE13E257E94261DCB23FB497E5E5 ();
// 0x000004B4 UnityEngine.Vector2 FingerGestures_FingerList::AverageVector(FingerGestures_FingerList_FingerPropertyGetterDelegate`1<UnityEngine.Vector2>)
extern void FingerList_AverageVector_m83D03D228961E98AA67F088FCFAF04F90BE9A473 ();
// 0x000004B5 System.Single FingerGestures_FingerList::AverageFloat(FingerGestures_FingerList_FingerPropertyGetterDelegate`1<System.Single>)
extern void FingerList_AverageFloat_m87A269AF8CC5157E2521D6722A5E698620AD6E5D ();
// 0x000004B6 UnityEngine.Vector2 FingerGestures_FingerList::GetFingerStartPosition(FingerGestures_Finger)
extern void FingerList_GetFingerStartPosition_mF067428CB682BD1F5A678BE3078CE544E65C6E52 ();
// 0x000004B7 UnityEngine.Vector2 FingerGestures_FingerList::GetFingerPosition(FingerGestures_Finger)
extern void FingerList_GetFingerPosition_m8F3C376B3E4904ADDCCDF4709A7A8F1BD7F03A24 ();
// 0x000004B8 UnityEngine.Vector2 FingerGestures_FingerList::GetFingerPreviousPosition(FingerGestures_Finger)
extern void FingerList_GetFingerPreviousPosition_m3B743773C1BE8C2CBC012D0ACB28BD7C6B40328F ();
// 0x000004B9 System.Single FingerGestures_FingerList::GetFingerDistanceFromStart(FingerGestures_Finger)
extern void FingerList_GetFingerDistanceFromStart_mE7D9937DBCE19485CF40E2D9E2F2D6EEE269C76A ();
// 0x000004BA UnityEngine.Vector2 FingerGestures_FingerList::GetAverageStartPosition()
extern void FingerList_GetAverageStartPosition_m096CAACC8139F3843F21091A583995B9477BEC6E ();
// 0x000004BB UnityEngine.Vector2 FingerGestures_FingerList::GetAveragePosition()
extern void FingerList_GetAveragePosition_m2F1246B791C7781DBE0994CE3C72323DB80A70FE ();
// 0x000004BC UnityEngine.Vector2 FingerGestures_FingerList::GetAveragePreviousPosition()
extern void FingerList_GetAveragePreviousPosition_m252AE475CD3E8FD3540DD039D70308D8E9F18624 ();
// 0x000004BD System.Single FingerGestures_FingerList::GetAverageDistanceFromStart()
extern void FingerList_GetAverageDistanceFromStart_m1D67040684E791F71FAF9A2FD0139BED7987C4F3 ();
// 0x000004BE FingerGestures_Finger FingerGestures_FingerList::GetOldest()
extern void FingerList_GetOldest_mBD6328AFAD4F716B354D0223AECFDA471DF4581E ();
// 0x000004BF System.Boolean FingerGestures_FingerList::MovingInSameDirection(System.Single)
extern void FingerList_MovingInSameDirection_m063053B37E7BDF2D6F620C28E9D0BE16AF6AD9C9 ();
// 0x000004C0 System.Boolean FingerGestures_FingerList::AllMoving()
extern void FingerList_AllMoving_mBC0189D845D8F5B3448682EE6577A161D80908F5 ();
// 0x000004C1 System.Void PointCloudRegognizer_Point::.ctor(System.Int32,UnityEngine.Vector2)
extern void Point__ctor_m180060A42155920D221AF1955B040B0CD829E8ED_AdjustorThunk ();
// 0x000004C2 System.Void PointCloudRegognizer_Point::.ctor(System.Int32,System.Single,System.Single)
extern void Point__ctor_m67C8BCB74264107D37F9D4F87AE79E5B1ED7E648_AdjustorThunk ();
// 0x000004C3 System.Void PointCloudRegognizer_NormalizedTemplate::.ctor()
extern void NormalizedTemplate__ctor_m7BBAE42EBC9076D5462E6128273D037FD7CA57CB ();
// 0x000004C4 System.Void PointCloudRegognizer_GestureNormalizer::.ctor()
extern void GestureNormalizer__ctor_mFC63331C7E9123AE627CC69EDBFA7F4FDA453809 ();
// 0x000004C5 System.Collections.Generic.List`1<PointCloudRegognizer_Point> PointCloudRegognizer_GestureNormalizer::Apply(System.Collections.Generic.List`1<PointCloudRegognizer_Point>,System.Int32)
extern void GestureNormalizer_Apply_m84D307A78C15A57C39750CA79304B0143A9F17E7 ();
// 0x000004C6 System.Collections.Generic.List`1<PointCloudRegognizer_Point> PointCloudRegognizer_GestureNormalizer::Resample(System.Collections.Generic.List`1<PointCloudRegognizer_Point>,System.Int32)
extern void GestureNormalizer_Resample_m5D07D707551B59AD2E00FACCBF50CEB8DCF58FAB ();
// 0x000004C7 System.Single PointCloudRegognizer_GestureNormalizer::PathLength(System.Collections.Generic.List`1<PointCloudRegognizer_Point>)
extern void GestureNormalizer_PathLength_mF6EE7BF422B49A6CAC17175A2AFF193976F79C81 ();
// 0x000004C8 System.Void PointCloudRegognizer_GestureNormalizer::Scale(System.Collections.Generic.List`1<PointCloudRegognizer_Point>)
extern void GestureNormalizer_Scale_m7C62517507431BE2B757867ED89ACDBD9CF27308 ();
// 0x000004C9 System.Void PointCloudRegognizer_GestureNormalizer::TranslateToOrigin(System.Collections.Generic.List`1<PointCloudRegognizer_Point>)
extern void GestureNormalizer_TranslateToOrigin_m683AB61C1963279F75A7681E5651C0E8E4099DFB ();
// 0x000004CA UnityEngine.Vector2 PointCloudRegognizer_GestureNormalizer::Centroid(System.Collections.Generic.List`1<PointCloudRegognizer_Point>)
extern void GestureNormalizer_Centroid_m27461441D62D3E9E207C582D1D4BB9C8E3BAEC62 ();
// 0x000004CB System.Void PointCloudRegognizer_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m3043EBCB1F86072B1F702CFB2E5859DEDFAC9761 ();
// 0x000004CC System.Boolean PointCloudRegognizer_<>c__DisplayClass10_0::<FindNormalizedTemplate>b__0(PointCloudRegognizer_NormalizedTemplate)
extern void U3CU3Ec__DisplayClass10_0_U3CFindNormalizedTemplateU3Eb__0_m3E5A1B015A9A6765B03BB6181804640F0A60CF80 ();
// 0x000004CD System.Void TBOrbit_<>c::.cctor()
extern void U3CU3Ec__cctor_m24B3098B532C9CCE35C6BD304046205854F6A991 ();
// 0x000004CE System.Void TBOrbit_<>c::.ctor()
extern void U3CU3Ec__ctor_m7BBFAE45DF6B48C4071442316BD3D3B0CC854B10 ();
// 0x000004CF System.Boolean TBOrbit_<>c::<InstallGestureRecognizers>b__52_0(GestureRecognizer)
extern void U3CU3Ec_U3CInstallGestureRecognizersU3Eb__52_0_mD9AEB31075683873A6D9257C9FC856D309F41521 ();
// 0x000004D0 System.Boolean TBOrbit_<>c::<InstallGestureRecognizers>b__52_1(GestureRecognizer)
extern void U3CU3Ec_U3CInstallGestureRecognizersU3Eb__52_1_m3DA0BB81806EB1E8DB1C0CAC721CB6BD78C45B1A ();
// 0x000004D1 System.Boolean TBOrbit_<>c::<InstallGestureRecognizers>b__52_2(GestureRecognizer)
extern void U3CU3Ec_U3CInstallGestureRecognizersU3Eb__52_2_mE3493C2DA947D7F5BEA0273D595E49265B49E909 ();
// 0x000004D2 System.Void TBPan_PanEventHandler::.ctor(System.Object,System.IntPtr)
extern void PanEventHandler__ctor_m949D346D331D68F0C294765324D5950C78DED8A5 ();
// 0x000004D3 System.Void TBPan_PanEventHandler::Invoke(TBPan,UnityEngine.Vector3)
extern void PanEventHandler_Invoke_m156218E902162709CB2B218B43DF7C2F380F5DF7 ();
// 0x000004D4 System.IAsyncResult TBPan_PanEventHandler::BeginInvoke(TBPan,UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void PanEventHandler_BeginInvoke_m3B80C67B8D1F52222DC37069F15E6178EA3D754B ();
// 0x000004D5 System.Void TBPan_PanEventHandler::EndInvoke(System.IAsyncResult)
extern void PanEventHandler_EndInvoke_m55BB719742CEDB55247B40568F52498BD2A721DE ();
// 0x000004D6 System.Void NativeGallery_ImageProperties::.ctor(System.Int32,System.Int32,System.String,NativeGallery_ImageOrientation)
extern void ImageProperties__ctor_m50D620AF0B15FA57F34EDDF7A4A4084D859C8CF8_AdjustorThunk ();
// 0x000004D7 System.Void NativeGallery_VideoProperties::.ctor(System.Int32,System.Int32,System.Int64,System.Single)
extern void VideoProperties__ctor_mFFE46E5EE26EB279B7D28D60187FDD5B374DF2E3_AdjustorThunk ();
// 0x000004D8 System.Void NativeGallery_MediaSaveCallback::.ctor(System.Object,System.IntPtr)
extern void MediaSaveCallback__ctor_m4828FB6C10A1B1648CB67A0365916474F9AA4767 ();
// 0x000004D9 System.Void NativeGallery_MediaSaveCallback::Invoke(System.String)
extern void MediaSaveCallback_Invoke_m59C758F8247D68A3D649AEC124CA0B94DBBA4A59 ();
// 0x000004DA System.IAsyncResult NativeGallery_MediaSaveCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MediaSaveCallback_BeginInvoke_m552765D029E8437A3207DF853843ECBD1328BA3A ();
// 0x000004DB System.Void NativeGallery_MediaSaveCallback::EndInvoke(System.IAsyncResult)
extern void MediaSaveCallback_EndInvoke_m65AF9C9D3382E1DEB63E13F1A934427A59839FE8 ();
// 0x000004DC System.Void NativeGallery_MediaPickCallback::.ctor(System.Object,System.IntPtr)
extern void MediaPickCallback__ctor_mE60E8E3F8CFB5FBFB773660006A569F26BA7DFED ();
// 0x000004DD System.Void NativeGallery_MediaPickCallback::Invoke(System.String)
extern void MediaPickCallback_Invoke_m786BDC23FBF9A2E1FCFF094DD5BD14AEDC429742 ();
// 0x000004DE System.IAsyncResult NativeGallery_MediaPickCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MediaPickCallback_BeginInvoke_m162B1ABA1B63F23137CB1765C7924FDC05FBBEE8 ();
// 0x000004DF System.Void NativeGallery_MediaPickCallback::EndInvoke(System.IAsyncResult)
extern void MediaPickCallback_EndInvoke_m4594A4A3136818FDD8676A2645E3AEDA6300A032 ();
// 0x000004E0 System.Void NativeGallery_MediaPickMultipleCallback::.ctor(System.Object,System.IntPtr)
extern void MediaPickMultipleCallback__ctor_m2B18FCF912D2A5630BBACFAC8262E1C7F5F44D56 ();
// 0x000004E1 System.Void NativeGallery_MediaPickMultipleCallback::Invoke(System.String[])
extern void MediaPickMultipleCallback_Invoke_m66769A4E767AB564957473A03B03917FE0B18734 ();
// 0x000004E2 System.IAsyncResult NativeGallery_MediaPickMultipleCallback::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern void MediaPickMultipleCallback_BeginInvoke_m59F9CD0A772912B7BB3435C1641F49F63649B1F8 ();
// 0x000004E3 System.Void NativeGallery_MediaPickMultipleCallback::EndInvoke(System.IAsyncResult)
extern void MediaPickMultipleCallback_EndInvoke_mB627F3433B6B29241EAD8B600CB8CF0BB86247CD ();
// 0x000004E4 System.Void Augmentation_<WaitForThen>d__13::.ctor(System.Int32)
extern void U3CWaitForThenU3Ed__13__ctor_m9289F929E0DC9A02ABE1223B74C7E7DFA0ECA9B4 ();
// 0x000004E5 System.Void Augmentation_<WaitForThen>d__13::System.IDisposable.Dispose()
extern void U3CWaitForThenU3Ed__13_System_IDisposable_Dispose_m3D6753B01F9ACBD31D68E1A998317BD0FD57943D ();
// 0x000004E6 System.Boolean Augmentation_<WaitForThen>d__13::MoveNext()
extern void U3CWaitForThenU3Ed__13_MoveNext_m73BE245F8C19AD314815A098CE4D8747A0480E21 ();
// 0x000004E7 System.Object Augmentation_<WaitForThen>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForThenU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB73157555D4D47293AD9A1ACA244551BE71DF00 ();
// 0x000004E8 System.Void Augmentation_<WaitForThen>d__13::System.Collections.IEnumerator.Reset()
extern void U3CWaitForThenU3Ed__13_System_Collections_IEnumerator_Reset_m29C45E5A0D61FF31ED030C203EB59274D3D66758 ();
// 0x000004E9 System.Object Augmentation_<WaitForThen>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForThenU3Ed__13_System_Collections_IEnumerator_get_Current_m4B15C342E892F135137C8CB6F1C5CA55A8FD4308 ();
// 0x000004EA System.Void Augmentation_<>c::.cctor()
extern void U3CU3Ec__cctor_m42A0D3C4C32F9AF5F2B1ED549F350C3A68FB0549 ();
// 0x000004EB System.Void Augmentation_<>c::.ctor()
extern void U3CU3Ec__ctor_m33BF7F0480C410341FD6FEA61803CB97F7C58061 ();
// 0x000004EC System.Void Augmentation_<>c::<.ctor>b__14_0()
extern void U3CU3Ec_U3C_ctorU3Eb__14_0_mEFCADA3426388352F20386F407CB1B8D3BE0EAE5 ();
// 0x000004ED System.Void Augmentation_<>c::<.ctor>b__14_1()
extern void U3CU3Ec_U3C_ctorU3Eb__14_1_m1D70FB686B14F9A858F6E1C7207B4183A187E179 ();
// 0x000004EE System.Void Lean.Touch.LeanDragTrail_FingerData::.ctor()
extern void FingerData__ctor_m958420B280F124B18A1EE09D6043AA097B0F2A52 ();
// 0x000004EF System.Void Lean.Touch.LeanFingerDown_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m240731BD45F683FE9FC117347F0ED4626C3D24F6 ();
// 0x000004F0 System.Void Lean.Touch.LeanFingerDown_Vector3Event::.ctor()
extern void Vector3Event__ctor_m5B08217E4EFF53B05A165305B345D2201A11C34E ();
// 0x000004F1 System.Void Lean.Touch.LeanFingerOld_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mF2F82DC9A6390D4C5C6F1810A29F0EBDD8D039DC ();
// 0x000004F2 System.Void Lean.Touch.LeanFingerOld_Vector3Event::.ctor()
extern void Vector3Event__ctor_m783F60F4FEDEC60DA828C3494B30528A1D15287E ();
// 0x000004F3 System.Void Lean.Touch.LeanFingerSet_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mC2EDFBF8B8201551EAB90B9DC51A38BE93DC6F36 ();
// 0x000004F4 System.Void Lean.Touch.LeanFingerSet_FloatEvent::.ctor()
extern void FloatEvent__ctor_m55B0C3A9958469B2566D2FF50D450D958C0746F6 ();
// 0x000004F5 System.Void Lean.Touch.LeanFingerSet_Vector2Event::.ctor()
extern void Vector2Event__ctor_m6D4AA794E04F10D72A4125741702326713F65018 ();
// 0x000004F6 System.Void Lean.Touch.LeanFingerSet_Vector3Event::.ctor()
extern void Vector3Event__ctor_m5A7FED7217E52F55C90D8483A5737364752356B3 ();
// 0x000004F7 System.Void Lean.Touch.LeanFingerSet_Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_mC725A539F1B59C1C6658F158832C3A7E34C3F5AE ();
// 0x000004F8 System.Void Lean.Touch.LeanFingerTap_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mB426C3E58D460DDC3390C8669AB2C432A9A9F142 ();
// 0x000004F9 System.Void Lean.Touch.LeanFingerTap_Vector3Event::.ctor()
extern void Vector3Event__ctor_m2ADAEC68F844784F3F4DE471CE0FDBBCFC9A7D5C ();
// 0x000004FA System.Void Lean.Touch.LeanFingerUp_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m03B5D101B3A3B4F5D743FE6A0BDE2C2852043FA9 ();
// 0x000004FB System.Void Lean.Touch.LeanFingerUp_Vector3Event::.ctor()
extern void Vector3Event__ctor_mF38D6FF8658299B0A684715E5FE46198F6EB6771 ();
// 0x000004FC System.Void Lean.Touch.LeanSelectable_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mFBD47862EF4515B27A1684FF4EADA41B1D413D79 ();
// 0x000004FD System.Void Lean.Touch.LeanSwipeBase_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mE8081565DC33AB6A984E77B4F854F7E62832A52A ();
// 0x000004FE System.Void Lean.Touch.LeanSwipeBase_FloatEvent::.ctor()
extern void FloatEvent__ctor_mB28E6C59B5FBED30471BFECCBADC731CBEF73585 ();
// 0x000004FF System.Void Lean.Touch.LeanSwipeBase_Vector2Event::.ctor()
extern void Vector2Event__ctor_m68F38FCE3DF6CD53113F243D77A9A813F93F56D7 ();
// 0x00000500 System.Void Lean.Touch.LeanSwipeBase_Vector3Event::.ctor()
extern void Vector3Event__ctor_m8584FD2B5AD4EC33E0152C08D0E231F62FABC5C7 ();
// 0x00000501 System.Void Lean.Touch.LeanSwipeBase_Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_m664EAC04BB50FCB67F1CBB329137B236DFC6A051 ();
// 0x00000502 System.Void Lean.Common.Examples.LeanCircuit_Path::.ctor()
extern void Path__ctor_m49A7B4E18B095A63BB32F2E09FE99BC54C87F552 ();
// 0x00000503 System.Boolean Lean.Common.Examples.LeanCircuit_Node::Increment(UnityEngine.Vector3)
extern void Node_Increment_m5223A3C76083C8449A585B94C6665003E1C93CFF ();
// 0x00000504 System.Void Lean.Common.Examples.LeanCircuit_Node::.ctor()
extern void Node__ctor_m4C8F1B861459D5988487680E3B44FA5D89C65C6C ();
// 0x00000505 System.Void FingerGestures_FingerList_FingerPropertyGetterDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x00000506 T FingerGestures_FingerList_FingerPropertyGetterDelegate`1::Invoke(FingerGestures_Finger)
// 0x00000507 System.IAsyncResult FingerGestures_FingerList_FingerPropertyGetterDelegate`1::BeginInvoke(FingerGestures_Finger,System.AsyncCallback,System.Object)
// 0x00000508 T FingerGestures_FingerList_FingerPropertyGetterDelegate`1::EndInvoke(System.IAsyncResult)
static Il2CppMethodPointer s_methodPointers[1288] = 
{
	LoadFromCacheAndDownload_Start_mF32D52234FE00F040B4BB1395F5010588D945F93,
	LoadFromCacheAndDownload_LoadBundle_mEA5AEBD76F320D6BD91C056CEE91A47823E9417A,
	LoadFromCacheAndDownload_Update_mF461E24ED517DFD4111120A26C307469B2897C33,
	LoadFromCacheAndDownload_Load_m5B90A4850F9221191EEA833FFEA9E91ECDFB79F4,
	LoadFromCacheAndDownload__ctor_mAC6995A99E872673A0A35772701D9F0B3707B4F9,
	AnimHandler_Start_m6F98063D4F4B868C02BA2D630BB97427825D4AEE,
	AnimHandler_Update_mF51A703FBD519577F497DBAAB6D1D677BDE20F7E,
	AnimHandler_Assign_Object_m6C8D276B4F56C4E0FB7D71B1D8A3BA8AFF1B0F76,
	AnimHandler_ChangeMaterial_mFBA2C6E940AD8A7542BE89DD5BD63EBCF3BCC1E7,
	AnimHandler_OnShotAnimClick_mED339775F0CD76003D3254C8B85EBB2B63C9AD73,
	AnimHandler_ShotAnim_m0637CD8CD4C897245DB580766E7205D67E2CC2BE,
	AnimHandler_ShotAnimEnd_mABAC6E3D370E386EF98569409E2C1F46D4311030,
	AnimHandler_ResetFunction_m07B899C06B32AC36132E122A96D70A22C0E63168,
	AnimHandler_OnShotAnimClickAR_m89DBB3BEF44AA673E9645F77A7A11C6E445F3313,
	AnimHandler_OnFound_m271E8441112C17DD3144F4D1B946EC984857F986,
	AnimHandler_OnLost_m556F3916621EBA81FD11D9EE0C7608D55417C0E8,
	AnimHandler_ShotAnimEndAR_mA5AF671B5EB543BEC519151F699424A5D277E244,
	AnimHandler_TransMarkerAnimFound_mD0786DA63D9D948355DD2D5E50D8E1E1AD33D520,
	AnimHandler_TransMarkerAnimLost_mD77966C793584CCE74161C6AA489E8FD6FEC2DC6,
	AnimHandler_TransShotAnimEnd_m7A47829F05174B8A7B2FCA9E14ECD157C29A4C7E,
	AnimHandler__ctor_mD26DDFC6AD28D76BF0A6881F8FB3E99484C41599,
	FlowerHitHandler_Start_m6C0AD31C060B746B00C2F81FEAD55EAB9090D6EE,
	FlowerHitHandler_Update_m2F6B78DE2935E34608A4E562ECFEAAFFF1F0B068,
	FlowerHitHandler__ctor_m9EBDD10D0E296E578990FAFBA72D7C0C5AE0DF3D,
	JKLaxmiProTracker_Start_m5043C361FFA04951BB720953D9AE79950554DFBD,
	JKLaxmiProTracker_AnimationAndSoundFunction_mA06ADFB75D684B92943C49A91D12DCD601B48131,
	JKLaxmiProTracker_OnPlayerButtonClick_mBFCCB51E2E3ACB32E7CC0C9184BED807903C4B00,
	JKLaxmiProTracker_OnPlayerRotateButtonClick_m32608D1817FDD616F0706C3A86419C2F81A33FC0,
	JKLaxmiProTracker_OnPlayerAnimationButtonClick_mC8770BA4093F5E66DF7454FF4BE35D8B40D6002C,
	JKLaxmiProTracker__ctor_m6B332B92A3D7BF11ECD067C31618EF723B99158B,
	JKLaxmiTracker_Start_m3635D67AF427C997402BDF732B32E549A6E8331B,
	JKLaxmiTracker_AnimationAndSoundFunction_m744B000BE76C1724DCFC1EA7F7648A6C658CD7CC,
	JKLaxmiTracker_OnPlayerButtonClick_m0CB174C29B134E9CBBE9D9FFEC66361D947059B7,
	JKLaxmiTracker_OnPlayerRotateButtonClick_mFFA78FCA657C95689586A02462007D17B9EC1DEF,
	JKLaxmiTracker_OnPlayerAnimationButtonClick_mCA07117E200DFDFB91C809B45E6163F73D3D4196,
	JKLaxmiTracker__ctor_m6F17F66BEC947069401804E67597E6F242640CEF,
	Rotate_Start_m31842842A9C0AD4874D72AC2D0BF8EC302CF81B8,
	Rotate_Update_m2A5FD9FAEFB71B89128F9EA1346CEA14E9BC3C4E,
	Rotate__ctor_m634B0959655CD742CA62A6FE82BA86F20966679A,
	RRLogoNewTracker_Start_m445D7555346F50ECBB62972C2DA9B659CB741DC1,
	RRLogoNewTracker_AnimationAndSoundFunction_m0FE270CC67D718CFB1EDA3ECA7EF48C71D496F0E,
	RRLogoNewTracker_OnPlayerButtonClick_m5E171A827E376B93B8F38577D6C35C551CE02619,
	RRLogoNewTracker_OnPlayerRotateButtonClick_m45CA9D7D7F5890C60611D49892A282B2A7E5CE91,
	RRLogoNewTracker_OnPlayerAnimationButtonClick_mC87FB621A540D804FB5248C84405CE713A8A288B,
	RRLogoNewTracker__ctor_m251E3950EE718985296288217CCE91BDAEAB21F1,
	RRLogoOldTracker_Start_m961CC871B8E29E39A1271A1C91C66A838A3DFB6A,
	RRLogoOldTracker_AnimationAndSoundFunction_mD431FC5449E54A077FBCCF981ED662D263D48807,
	RRLogoOldTracker_OnPlayerButtonClick_mE8424306E0BDBBB10DD101670701B1C68E8C457C,
	RRLogoOldTracker_OnPlayerRotateButtonClick_mB97B3353E3DE27E5BE0EAEB402EA291EDC628527,
	RRLogoOldTracker_OnPlayerAnimationButtonClick_m850F75999DCFE0814B512C5D7FFE359CF2BD1FD7,
	RRLogoOldTracker__ctor_m4A23F2D84B4141495C2D8C1771EEFD2EB2F6E94B,
	SafeArea_Awake_mD9BEF89303539D1ADAACFC1D9EE8F6FC99360E6B,
	SafeArea_Update_m09334741B86BAD44424B057A97B0EF61CBD8A5CA,
	SafeArea_Refresh_m615FC807F2AB0C46A43588B0891BE8E4FC866382,
	SafeArea_GetSafeArea_mCEBD5B0EDAED35AE574848BB09F1BE0D8ACD0126,
	SafeArea_ApplySafeArea_m417F46DDCE219213C49D6374B93F857F24E42E62,
	SafeArea__ctor_mB1238EE624A536CC15578B1730A2360F792A0F9F,
	TrackEventHandlerJKLaxmi_Start_m95BA2DEC26F0A5E54CFEC58AAF3A3A1A730DDEC1,
	TrackEventHandlerJKLaxmi_OnDestroy_m0076D7E6E5B00182069AD7F5B249A51288F6AC22,
	TrackEventHandlerJKLaxmi_Update_m9F7F4D6FD46A2B82A17198CADA604E69A4CC1655,
	TrackEventHandlerJKLaxmi_OnTrackableStateChanged_mCB35E6AE00FE8AF867AD9A442008EC2AD3CEC4D6,
	TrackEventHandlerJKLaxmi_OnTrackingFound_m615F1CAB299ED1571D7AFEC07AF5C5DB5A061BE7,
	TrackEventHandlerJKLaxmi_OnTrackingLost_m46B70E824889AE7F5B58513E08075D98CF39B053,
	TrackEventHandlerJKLaxmi__ctor_m81206B0681E1C55B781FE4E2434126EBC8739C3B,
	TrackEventHandlerJKLaxmiPro_Start_m397F0CD009B181E325670704DD0CCD4F04B06BE6,
	TrackEventHandlerJKLaxmiPro_OnDestroy_m27A6F37FB68A69EB2CDD0A8F21EB923A4C0BA570,
	TrackEventHandlerJKLaxmiPro_Update_mD437F796649188BB2FD747F3D549F4B62CA43FDC,
	TrackEventHandlerJKLaxmiPro_OnTrackableStateChanged_m16AA76F10B19C9C451E04772F3B6D5266AAA7D00,
	TrackEventHandlerJKLaxmiPro_OnTrackingFound_m0D0D2A7E150511A4DA4B41B45CA5CC8A9579C07D,
	TrackEventHandlerJKLaxmiPro_OnTrackingLost_m56A69458D08D8F37D551C56F327DBBD19731FD44,
	TrackEventHandlerJKLaxmiPro__ctor_m971F0827B8753B53E9A47698A4D06A9C0F402875,
	TrackEventHandlerRRLogoNew_Start_mF0D1118EF43D5B0F1EB8D68FA802019E08A795D3,
	TrackEventHandlerRRLogoNew_OnDestroy_mCD220749E7704E7BFDA5701A62804892057EC087,
	TrackEventHandlerRRLogoNew_Update_mF1B97259B6F6B525AA6FAD6E8B5C6113BA352051,
	TrackEventHandlerRRLogoNew_OnTrackableStateChanged_m8AA8CCF260F1043C610DD65B06F4777E61DB1638,
	TrackEventHandlerRRLogoNew_OnTrackingFound_mBDDD08EABE1605AB9A67DBF20961343BB2A24017,
	TrackEventHandlerRRLogoNew_OnTrackingLost_m100882790426557C55F65F68302022BFDD191EAC,
	TrackEventHandlerRRLogoNew__ctor_mB1F4BA23C6D860261E6F60B5F72A273A75867932,
	TrackEventHandlerRRLogoOld_Start_m58028B9882621D12B328D569AAF0F5E4A6F688A1,
	TrackEventHandlerRRLogoOld_OnDestroy_m102F95DC449DD4351D0A532ED6EF263FE43372F0,
	TrackEventHandlerRRLogoOld_Update_mC16A3E79614A2F4428C1C697F520FC6CA64416BC,
	TrackEventHandlerRRLogoOld_OnTrackableStateChanged_mD470EFA12C2E52811E672D6431F86A1E41991775,
	TrackEventHandlerRRLogoOld_OnTrackingFound_m93965E7DF89A900BAC9EA0FE657FD639742F0AAA,
	TrackEventHandlerRRLogoOld_OnTrackingLost_m54635C86D7FE050BD8E04E8E92E74D8740EE22E0,
	TrackEventHandlerRRLogoOld__ctor_m5F9F090DEB32EE65F5849814EC2475A6069C609B,
	TrackEventHandlerWorldCupOne_Start_m241FE993871965BAD0239A91B41123B674F39C10,
	TrackEventHandlerWorldCupOne_OnDestroy_mF06C868CA69A637B1AA3CB7701ADFEFA422D3D2E,
	TrackEventHandlerWorldCupOne_Update_mAFF3356727F1CDF681D8B055749BF1669979864C,
	TrackEventHandlerWorldCupOne_OnTrackableStateChanged_m4C9476EF61E220726561BB6AFAAC1C85DDB3ED75,
	TrackEventHandlerWorldCupOne_OnTrackingFound_mA67BFB5DC6CE2CCA3124747452D5B95267704978,
	TrackEventHandlerWorldCupOne_OnTrackingLost_mF12C20F6775598BDF424C544B2CC27B2E701A537,
	TrackEventHandlerWorldCupOne__ctor_m376C75CB431E5F7571EACD69286B20CF5A08BBED,
	TrackEventHandlerWorldCupTwo_Start_m2AB8752DAB38401B5C82C1DADB5B97A926D31589,
	TrackEventHandlerWorldCupTwo_OnDestroy_m75A791E319CF636B146FEE537B7EC5DD2682F459,
	TrackEventHandlerWorldCupTwo_Update_mD574A9ED56F89BD8A20E18C2AE0E9B0F7E054382,
	TrackEventHandlerWorldCupTwo_OnTrackableStateChanged_m843B9D305A7208AE58172D58496CAC78C82CDC79,
	TrackEventHandlerWorldCupTwo_OnTrackingFound_m73585600AC2FCA0B8E997C502683E32EB56AA886,
	TrackEventHandlerWorldCupTwo_OnTrackingLost_mD6349AD5435B342536208AFCB67EBF5301FB3639,
	TrackEventHandlerWorldCupTwo__ctor_mA75F02F0D81D9063C10800171EF52265C6D94881,
	UI_Handler_Awake_mC9DFFBFCEF18031AEEDF0E8850A16105E196A0CB,
	UI_Handler_Start_m41A544F025231321FC2EEC746ECEE6D203753CD8,
	UI_Handler_OnExitClick_m584E6AE28BC79582B9A032ECBB4AD2FF144661A4,
	UI_Handler_OnReset_mA7ED1BCBEE72E36143BEA8259BEC3E54E5B1F6E6,
	UI_Handler_On3DSceneClick_m2A5ACCDB7DF4E4E4E4EA738A416378C8FCBF4FD5,
	UI_Handler_OnARSceneClick_mE5C8B143B9FED7FBA703D1393A82E8E884E80986,
	UI_Handler_OnHomeClick_m47FDD3426389FF4F3AFCCCF2C842A497399C5890,
	UI_Handler_OnCaptureClick_m6C29B8363D03892BDE3A990D6A2F35C5BE1804F7,
	UI_Handler_TakeScreenshotAndSave_m91BEA244D8F332A3C3B835E4612CA4BE4A11F68F,
	UI_Handler_CamSwitch_m32188B78B2FBA776C403EA5D760878368A5125EF,
	UI_Handler_OnSkipButtonClick_m95570A90E438B5656A52E79262F05546293DC19E,
	UI_Handler_ResetFunction_mA050539AB12E591B950C3FD8F6DAF88B034112F6,
	UI_Handler__ctor_m6DC2E7F9BD502D08A0456C5D09A71AA165F407E9,
	WorldCupOneTracker_Start_mF6C593BBEAE553C9200ACF0FBA90094169A05DCF,
	WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3,
	WorldCupOneTracker_OnPlayerButtonClick_m1F83130B2F2EEE2A83A3B08F39032537DF0B287A,
	WorldCupOneTracker_OnPlayerRotateButtonClick_mE413E604F73A811ABB1D44A5C2DDF34ECA4FE552,
	WorldCupOneTracker_OnPlayerAnimationButtonClick_m10B4471F1D9E2524485619C04F6BA877895DE9B3,
	WorldCupOneTracker__ctor_mD16845E1E743BF1613B38EC83717AE1683613CA7,
	WorldCupTwoTracker_Start_m994B58169EAB8550B3320024CFDA6C4825103467,
	WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9,
	WorldCupTwoTracker_OnPlayerButtonClick_m299722C1E95EB77FFD9AB56953380C98189CC3D6,
	WorldCupTwoTracker_OnPlayerRotateButtonClick_mC46776A5E869E613CDEBB160EF7398C216481237,
	WorldCupTwoTracker_OnPlayerAnimationButtonClick_mAB802B57334169446EB7DD3CFDCB0D124318C5C4,
	WorldCupTwoTracker__ctor_m0863875911C8AF93266BC44B8B6B972FAB40891C,
	ContinuousGesture_get_Phase_mCA8691B331B108539F709328FB9D37FCE5257D63,
	ContinuousGesture__ctor_mC302AF0C31E49BC40B920B7B5D652E9B1B95F51E,
	NULL,
	NULL,
	NULL,
	DiscreteGesture__ctor_m2A853CD23FF3956E66726A398F1A23A35E65F648,
	NULL,
	NULL,
	FingerEvent_get_Name_m63409408D113873479C461A890D91C80B516DF10,
	FingerEvent_set_Name_mDF502F593BB1827140457E5F02A93F9B101DE342,
	FingerEvent_get_Detector_m0CDB153B3591142F215AA4B31F4F09D0F50C5FAE,
	FingerEvent_set_Detector_m233A75210069EAF6F233159179148EB1B5A08983,
	FingerEvent_get_Finger_m8F1446FCFACFE85F775A16B41577AC013BB16886,
	FingerEvent_set_Finger_m51947C362E9D4C1D0C8294CD3368C6A991325F93,
	FingerEvent_get_Position_mA6AAF7687A550CDDFC0DF7B3147FAC39200633A1,
	FingerEvent_set_Position_mD3DD55FA50540DDCA13814455A74D0ADAF88D270,
	FingerEvent_get_Selection_m0EABD79D7BC289B03804FD8FD8EFD4CF431ADA61,
	FingerEvent_set_Selection_m66D6F926B7AC1BC01371C8EA207F30D097DE7F53,
	FingerEvent_get_Hit_m6896095BD0C83580FA2BB3C24FFD78D0528B2ACD,
	FingerEvent_set_Hit_mCF5DC27BE56356E7B0FA08F37103BBF9808C2898,
	FingerEvent__ctor_m3ACBF5A4E5C45B612521B4EC3A5F6BCBEB8E0BE2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FingerEventDetector_Awake_m2B06F197BF79A7BACF8B3321307EB451E2813436,
	FingerEventDetector_Start_m3E1020381ECFC0900B7E2F11C3D0532A14A5E0E1,
	FingerEventDetector_Update_mCD5E9FB21C1585E8F4E889C9A3D596B93CC68256,
	FingerEventDetector_ProcessFingers_m5C6E9308CF953480B92A72F974F0D4CAB3919EF0,
	FingerEventDetector_TrySendMessage_m1C8DBEBBB4B3500F47E960474872D2A532D228C9,
	FingerEventDetector_get_LastHit_mFFF9FCA6771BE9F2747356E5319FD30C80402F69,
	FingerEventDetector_PickObject_m705E414AA28E694C8E164490BAFC831A55E94B91,
	FingerEventDetector_UpdateSelection_m051A889F92C0D80D1308F48309C8ACB3915A5545,
	FingerEventDetector__ctor_m850AC678820E4B143A94E6C2488A8D8A070BE52E,
	Gesture_add_OnStateChanged_mFE932ED8E23ACA4C6564A3485893CA1BA3FE5593,
	Gesture_remove_OnStateChanged_m4C65E28030625B0575C3E1B812B9867DDB9A0DAE,
	Gesture_get_Fingers_m41CDD968FE8EB03670776F0122F23B02F6361C22,
	Gesture_set_Fingers_m3CCB847A68504C32B2CBD1DA0501F8888DBE7223,
	Gesture_get_Recognizer_mD23CAB53D254DC465F2EF391E151482C07EC8F91,
	Gesture_set_Recognizer_m345C5941675360DFED066337EA5222672C29CE27,
	Gesture_get_StartTime_mE29B562C7067967EC80D84BAE6195CAE8CF3677F,
	Gesture_set_StartTime_m946C0B6D49315E12F8336EE5D8019E097BEEA31A,
	Gesture_get_StartPosition_mCF0DBFDBBF9920DBAC9E53B2B903DC448CABCC51,
	Gesture_set_StartPosition_m72914599C23AC7622FF0CCFE09C89F62A40A4714,
	Gesture_get_Position_mCF6CBD40FFE3D90A3B57CD2CC8DE63FCF9F12826,
	Gesture_set_Position_m1A4BD357265D7780D6B980F9F569A37DC15CFE24,
	Gesture_get_State_m16C149C72CFDE864601C8B39CA7D57ED1438369A,
	Gesture_set_State_m315EFAC7380E3BC391C5EF22925EA257D91C96C9,
	Gesture_get_PreviousState_mD10D8584386CDF064FFA68B1ED57C788C6BF7989,
	Gesture_get_ElapsedTime_m98B1BFB3D50FFDFB6147454F2F99BBD7300C70B0,
	Gesture_get_StartSelection_m2F671A58813B69F4E401DC2337BD85FB886B9A8C,
	Gesture_set_StartSelection_m6CA7D9DA8E5F0FDDDDD2492FCEAE22BB6AAD71DD,
	Gesture_get_Selection_m4DF689355463A2749F1315756F8B65C76C588873,
	Gesture_set_Selection_m587B0C38111956C258FB62141872AB5481717AF1,
	Gesture_get_Hit_m1AF29710F084B5EBE09C54DCB07C83E98846B05C,
	Gesture_set_Hit_m393E60C07E9BF08F0F6F578B65A5CB1DBE978C2F,
	Gesture_PickObject_mDA2A8A4BD650AE9FFB9AC7F86030F4AA9E5BBA95,
	Gesture_PickStartSelection_mCCEB59B593D2F6E1BD92AD0D46DFF5B2AA35C49B,
	Gesture_PickSelection_m65CD0ECD8072C84B93771CF3FDBE37FF3234A8F5,
	Gesture__ctor_m9CDB4E7EE06898FA1D4E47C8CAD6BDD4FBAE5B2D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GestureRecognizer_get_RequiredFingerCount_m7ABD03069ED10AE00CC6B887D511325471667E34,
	GestureRecognizer_set_RequiredFingerCount_m44BCB6BFDDE4041D6DBE7CDC274961C8620CA06A,
	GestureRecognizer_get_SupportFingerClustering_m4249320D5A253047A8AF7341D9812A82DEF93FCC,
	GestureRecognizer_GetDefaultResetMode_m5B30A366400FCA14C53F7F4520869005F67D1E01,
	NULL,
	NULL,
	GestureRecognizer_Awake_m00596F35A7FD19987F42A6B720ED3DABE8E0EABD,
	GestureRecognizer_OnEnable_mB8AE48594DF44F97F3FD77FD9B4B81EC45073C98,
	GestureRecognizer_OnDisable_m4CC14D05A20A94EB45D5DF590143490D47774A59,
	GestureRecognizer_Acquire_m008F59EA8DAB6BC91C10FE66001FEB2126678AA3,
	GestureRecognizer_Release_m88D85196319F3BA5B6B1F496572ABE945B18AB92,
	GestureRecognizer_Start_m697DE04C4CB4EE48EA1ED33263076AE6B3F3E01F,
	GestureRecognizer_Young_mF6B5A5F15B5493190E1B14FBC29FD2823BF08EBE,
	GestureRecognizer__ctor_m5FB025E1ED7A59E415725FAD3A19D5C2D3B229F1,
	GestureRecognizer__cctor_m3B0B7D1CDF24F40769346D966EB48CBD0937E965,
	NULL,
	GestureRecognizerDelegate__ctor_m81D6C0DF8DE44F191A9D685763BAB1DA02A8B0C9,
	FingerClusterManager_get_FingersAdded_m64A774645B86E0889AD7130A8EFF6009795D3398,
	FingerClusterManager_get_FingersRemoved_m72DF1CC9AC37526ADB3405E78F9E3005903A470A,
	FingerClusterManager_get_Clusters_m08F64C3C9C3BE9EA6EE9546EDCB2B5C33A6272D0,
	FingerClusterManager_GetClustersPool_m00882933206ACEF2F53A74A4D44ED8CE5BA6FA59,
	FingerClusterManager_Awake_m424B01EB96FEC6149BC5598225E2B47A134BE64C,
	FingerClusterManager_Update_m238C149FF6769DD21A1038CAEA090E4D1B526239,
	FingerClusterManager_FindClusterById_m647FC1A3DF5571E660F1F256AB6320B6A09E3772,
	FingerClusterManager_NewCluster_mE9295A51E2ACE852041A871A57E76C599EBB4C13,
	FingerClusterManager_FindExistingCluster_m515BAFC6EC55064CFECDECE3F8CB43DA727C61D2,
	FingerClusterManager__ctor_mFC4CD0168F16FAF2CE24C96181C6BC335A921C58,
	ScreenRaycaster_Start_m75FB098088330DF46BB4D447ECBCA35DC68D4541,
	ScreenRaycaster_Raycast_mF4A45FDC6D42EEF03364C2C782E447366FDE423F,
	ScreenRaycaster_Raycast_m796B5A1E3A9105B99279DB4371715580478A4ABC,
	ScreenRaycaster__ctor_mDB8E40D02275699EF5DA735E094912D6C1D6BD44,
	FingerDownEvent__ctor_mD9ACAEE0A8D16A72B959F9CA5DD4F77CE372A80B,
	FingerDownDetector_add_OnFingerDown_m1B162A57B8E07EB74ABB7213568EFD7EACC9629B,
	FingerDownDetector_remove_OnFingerDown_m952E1C3B6E5E677611C5B2B67EAD01EFAC89E90E,
	FingerDownDetector_ProcessFinger_mD6768D5250DE356595F850F3BEB53D83E37AB40D,
	FingerDownDetector__ctor_m235555C2CC46EE02D8A766696AAACAE3D7519A36,
	FingerHoverEvent_get_Phase_m26DD5514C8250F3BE9FC18BC1678AC03E3D1D5C1,
	FingerHoverEvent_set_Phase_mE06E7A195A3F0A3146057EC0555C48DFF89C02EA,
	FingerHoverEvent__ctor_m1ADFCD2A285A72A6A20234A69D63BE9F7BE2080D,
	FingerHoverDetector_add_OnFingerHover_m30014CA27E530AD8A0CB79F8E94CA0B628006553,
	FingerHoverDetector_remove_OnFingerHover_m96B0DEE943C10C6A4C24D36EF8090249593EB42D,
	FingerHoverDetector_Start_m0738184BBC93B3A20DDDFF554561C9426D375090,
	FingerHoverDetector_FireEvent_m39953252FDEB320C36CDA5493C80581DCBAE92C7,
	FingerHoverDetector_ProcessFinger_mECADEA973B2F127AB319448412AC3031FD15220F,
	FingerHoverDetector__ctor_mA69CB503A90C9D6754FC07987943DEE17FADBAB5,
	FingerMotionEvent_get_Position_mEF3A8F9E78557172420442E5801A3325F9053D3F,
	FingerMotionEvent_set_Position_m8104773CB80C6F1812AD1D5F24BD1C8A3FE257C0,
	FingerMotionEvent_get_Phase_mFF72D4C7CB172AD88272C583DC78FA46941489C4,
	FingerMotionEvent_set_Phase_m4F1E91064274B45EB13CA939193AEF5223DC7747,
	FingerMotionEvent_get_ElapsedTime_m5D033EBB953A3C38CBE2C50F439A68CE3A571604,
	FingerMotionEvent__ctor_m4533C0330FF8EACAF78810C6789D5287BF45E93D,
	FingerMotionDetector_add_OnFingerMove_m7F044585A615D81F6D2D856C681CED525CFDC8D0,
	FingerMotionDetector_remove_OnFingerMove_m3DF6C48AD287F2D18EBD9449A8BEE56157E24A99,
	FingerMotionDetector_add_OnFingerStationary_m6AF32E1165E8C2E1F7A1524E0D2BD513D81B68AA,
	FingerMotionDetector_remove_OnFingerStationary_mBCF03CE3802E768D9A1F0E301FFC615B0F21C426,
	FingerMotionDetector_FireEvent_m5981EFFE5CE596D03794F1D1E33427DB8B90F6D8,
	FingerMotionDetector_ProcessFinger_m58D7DC79FE40C7CEDA3036DAED27CF834027A7CF,
	FingerMotionDetector__ctor_m459CD226979040E4B88A85C2E1BC37A4ACD30DE3,
	FingerUpEvent_get_TimeHeldDown_mFE63F1E41B6246E2A343E28EA98639726038F0C6,
	FingerUpEvent_set_TimeHeldDown_mD43D3FB252AAA946F706248AA3228D80773D0760,
	FingerUpEvent__ctor_mABAABACD34F5584B4BBC91612D16FB631223D1C4,
	FingerUpDetector_add_OnFingerUp_mD7486255141DAEE5D6E4ABD82ADA7CBCA6F9DC6A,
	FingerUpDetector_remove_OnFingerUp_m22110E67D030CEEE256543851388077B1E992369,
	FingerUpDetector_ProcessFinger_m16CA433996A0CFD49A866FBB5FA920605705A954,
	FingerUpDetector__ctor_mCD3C0E9BC2277A38D04BF42515A34B9D6F3497DC,
	FingerGestures_add_OnGestureEvent_m175F48313462437E1729010E912C30D7F6BCB9E7,
	FingerGestures_remove_OnGestureEvent_m6CE8780D51B5644EDF5CA098C422765DBD6F0127,
	FingerGestures_add_OnFingerEvent_mBCA2903A46BCED402F033BBA659FDD970A222422,
	FingerGestures_remove_OnFingerEvent_mCD635862E8ED7DEFE770CB007247C5E9FBCA6F3F,
	FingerGestures_FireEvent_mA8ACF4803832091F765824E8EA0FAE2A6B6C7D08,
	FingerGestures_FireEvent_m30987EAE0E7F87FD14EDB2AB9040B8CE1190098E,
	FingerGestures_get_DefaultClusterManager_m116179EC846E2644E8272B845FFE83551ED15DAD,
	FingerGestures_get_Instance_mE2F61A1AAF5667E7B63A2BB56A703E3B49F04C5F,
	FingerGestures_Init_mC7D69B854C93B2F14CBAF309A4B346931A8E7223,
	FingerGestures_get_InputProvider_m551AE2973695CD95C972DB7FCAAD3750AA539CC1,
	FingerGestures_InitInputProvider_m7F5A035DC253B5188BDAEC0F935F3F4F3DA05B87,
	FingerGestures_InstallInputProvider_m89F3AE714CC350027C74D88A94F4AE4923AA45B9,
	FingerGestures_get_MaxFingers_m89660976520038FE5C73D7F57ED52B49340DB70C,
	FingerGestures_GetFinger_m31973639437F2C7EAF80C8BC4C0C512C3CBA3A01,
	FingerGestures_get_Touches_m95127542603057B589A104547E2BC9920F122493,
	FingerGestures_get_RegisteredGestureRecognizers_mF158C9458086C86015392EF69F7AD0A12DA4CE98,
	FingerGestures_Register_m4249E65AE4FE2B2EA5EC22BA921880863D0EAAF3,
	FingerGestures_Unregister_mFA7A61E878D0B180202F039D65109903B14F7AAA,
	FingerGestures_get_PixelDistanceScale_m301DC059977B8A9392CABFEF8C7812BF2287144E,
	FingerGestures_set_PixelDistanceScale_mB4C2B0B71128F396E3A87E119A8DB59A0FCF42DE,
	FingerGestures_GetAdjustedPixelDistance_m91D4DA29029E419EFFAA33955EF35A07C68A0095,
	FingerGestures_IsRetinaDisplay_m83083C3AE7EA3B637D22D13ABEE4093158E2D1D6,
	FingerGestures_Awake_m667D5573FE39A3A725986EC746F8DD2646A6A6EE,
	FingerGestures_Start_m65727939A0C0A8AEA5A020964026C74F7AA22C8E,
	FingerGestures_OnEnable_m682EC64C3473F2C6F84E545F4F8AC60D67644256,
	FingerGestures_CheckInit_mEA07FEF4A7672FFB01B4EC0E9FFE7E231E446E8A,
	FingerGestures_Update_m6A7471C4A25541BF8C8423CF9E8E2A68B454274C,
	FingerGestures_InitFingers_m671C1EB570BADA913CA264D93F1D3E698AE7A926,
	FingerGestures_UpdateFingers_mD6443E0424F81E763FE7C631E130A467E3DBD98C,
	FingerGestures_get_GlobalTouchFilter_mD6708903C9AD39C6EB2C3067A42F6A3EBD33C9B2,
	FingerGestures_set_GlobalTouchFilter_m3AE82207325DAA013C2569515E947CC485CD5E45,
	FingerGestures_ShouldProcessTouch_m8EE06423B868044FF957B6AECF53AEC5CFB95C28,
	FingerGestures_CreateNode_m0279934D0ACA132DBFF5A08F69B73B09EBF34FCB,
	FingerGestures_InitNodes_m91E26EC3CCBE437C25333C96FF89FB59A9C7898A,
	FingerGestures_GetSwipeDirection_m4DF3DB16CDFF9C4418750A49F76F78766B52C74A,
	FingerGestures_GetSwipeDirection_m7F39D1534066FD219F10F87A3FAFAC6584487C3B,
	FingerGestures_UsingUnityRemote_m26928C0B1869129174A505EB72DF6F2650F05E04,
	FingerGestures_AllFingersMoving_m76A80EE1817671AD55065EC255736FF9AEE849C8,
	FingerGestures_FingersMovedInOppositeDirections_m667304A1A8774995D8D965FCB254B4D89EA3F5D3,
	FingerGestures_SignedAngle_m4FF40555C84A4382AABD859DFC66CDA234873413,
	FingerGestures_NormalizeAngle360_mB352E47FCB7FC839152375C4CBCE25286538E86D,
	FingerGestures__ctor_mBFB9C1508BE9F31D19C5AC3364341E2C387943BF,
	FingerGestures__cctor_m9635E81EB98795A1EDDC4BBC546C207175EBDFF6,
	DragGesture_get_DeltaMove_m9B2C10ED529DF57AF88CBAAD6F6A99F973324675,
	DragGesture_set_DeltaMove_m1A7791F7E0AD28484C111F82A364485119BBA7E1,
	DragGesture_get_TotalMove_m7B50ACD0110AC96E623FE58E40886C26AFBB0236,
	DragGesture__ctor_m2900563CF403AFE17F3C5B8C8D5411D0A5B1C169,
	DragRecognizer_GetDefaultEventMessageName_m8861084A55C8C1C144A43D3F545813E0DDE25355,
	DragRecognizer_GetDefaultSelectionForSendMessage_mFC999ACE5DB19A0738CB4BB5F0E0F38BA15FD296,
	DragRecognizer_CanBegin_m93428A47F8DF29254A3383378204BA7E36AA5AE9,
	DragRecognizer_OnBegin_m5FCDEE81E98DA278EA7AAA40D5E4C0428227704F,
	DragRecognizer_OnRecognize_m2EC57B28312197FE0C0B403B3181CD643A13994E,
	DragRecognizer__ctor_mF6A590FE0A7D237B87D3E1FA952911F8F5E48303,
	LongPressGesture__ctor_mDFF7C43856BA7605293A45DAA647FD67476B71C4,
	LongPressRecognizer_GetDefaultEventMessageName_mE5B190E04FBA7E738643168887C9F76FDDC00739,
	LongPressRecognizer_OnBegin_m00DFB111D17758B2092257BB6EFFB439E1E94E8D,
	LongPressRecognizer_OnRecognize_m9D30AD1EDA93424F8A0C8B81ABFAA38F8D3F5AD3,
	LongPressRecognizer__ctor_m3FAC68AEB35D8C3A79A4043A7E68DA8929CE7BF5,
	PinchGesture_get_Delta_m3F973E40169267AEF1DE08EF9185B48732BD205E,
	PinchGesture_set_Delta_m4BD3AC3922C0C6D65B4407075FAD83E8BAB05B71,
	PinchGesture_get_Gap_m8575E5A83FECA30D79F54340978705FA09BC6297,
	PinchGesture_set_Gap_m10EA81A126AD6BF97900F42B1B3C71B85A4CC16F,
	PinchGesture__ctor_m92CFB9CAD3CC7F3100C877E62E7D142606649CC6,
	PinchRecognizer_GetDefaultEventMessageName_m5154827A0C018351BA84E964C9CC1BF26E0D4947,
	PinchRecognizer_get_RequiredFingerCount_m736926BAED270C7702FFCFDF7BB1B84F925138C5,
	PinchRecognizer_set_RequiredFingerCount_mB44371CD36EB70C0B644603E4DFD270B3B008B61,
	PinchRecognizer_get_SupportFingerClustering_m9FAF3EB463798B073695ADD8611C2FDF78603061,
	PinchRecognizer_GetDefaultSelectionForSendMessage_mF424A8D369D7D967D0D88C2C44DDAA24725C076D,
	PinchRecognizer_GetDefaultResetMode_m1ECD3ED0E6DD2AC09564B5CB2217AEDCBE32A071,
	PinchRecognizer_CanBegin_m4253A2ED737BBBF1276FABB0B1D512E74B0BFCD7,
	PinchRecognizer_OnBegin_mBF2B94E6CDDBB325F957CBE0D851BCA13ABC064E,
	PinchRecognizer_OnRecognize_mD9D0CAB0B8F6DCC935901EE1257D53E7E9A9B327,
	PinchRecognizer_FingersMovedInOppositeDirections_m87DEEA502E1E4DCA8F991C5852EE999E453418F8,
	PinchRecognizer__ctor_mD0D0B390BAE8C8486305AAAC5C6A0CDF8BF75BE6,
	PointCloudGestureTemplate_get_Size_m337FDD55BE500A4259FC5D2D5A0B721A3F864816,
	PointCloudGestureTemplate_get_Width_m5A05CABFAB4B08272A15596B603E3F2252BB7EAE,
	PointCloudGestureTemplate_get_Height_m001C49AEB5E7FAC67E9CA38DA29942D9959FCF38,
	PointCloudGestureTemplate_BeginPoints_m50BBB4985D90BB3768BE50EE099CD6AF6DD1F224,
	PointCloudGestureTemplate_AddPoint_m140EA38D0DBFADA8322AECAE60AF6E3F9B877889,
	PointCloudGestureTemplate_AddPoint_m89399C519F441E5F0C311BEC2B0AA52129CBCC10,
	PointCloudGestureTemplate_EndPoints_mC498FB3B5B91F5971BB4793090D6C1C704F285D7,
	PointCloudGestureTemplate_GetPosition_m938D33114BADFE680A1DD73B4C900AB7FF2DECD3,
	PointCloudGestureTemplate_GetStrokeId_m6C9340D0D27B1D644022A5EAFFAC26971FC69AEA,
	PointCloudGestureTemplate_get_PointCount_m549D4C77EFF3DF6EAEAFA127B568576292239B4C,
	PointCloudGestureTemplate_get_StrokeCount_m69E0387C81498E8AD5DC4F227F809D4E267CA235,
	PointCloudGestureTemplate_Normalize_m3702E2B37ED55066DA04A6A0B6C97C997EC50ABA,
	PointCloudGestureTemplate_MakeDirty_m2DC9117EB702324C36F53882FFA9A61AD7FDCA47,
	PointCloudGestureTemplate__ctor_m58E53D53E67621EC57DD65248368DCDCAC7D10B0,
	PointCloudGesture__ctor_m8CD392B4A8A348DA065FEB53D804FD2C47F3E12D,
	PointCloudRegognizer_Awake_mC9F9C4201BD3F6797BCD75EECFAD63FEBCE025ED,
	PointCloudRegognizer_FindNormalizedTemplate_m036F01EC02E18A4FAD3002DFF24DC38CBB7A4042,
	PointCloudRegognizer_Normalize_mF0A3E6A84AD5F4EA416582967984AC7B2FA86912,
	PointCloudRegognizer_AddTemplate_m1872E96AF226C154452D078C51E6F2C2A528EB94,
	PointCloudRegognizer_OnBegin_m4E595483C8ADA88E24EC8B89EAD5C1BC03240DBD,
	PointCloudRegognizer_RecognizePointCloud_m29DDCD5857EFA4905D4DD9AAB728A5CEEAA73D39,
	PointCloudRegognizer_GreedyCloudMatch_mF1697E963AA8DE3FBA1F81D0FA022AA93EB768E9,
	PointCloudRegognizer_CloudDistance_m9475728A534487A61AF082B80BE1A34F82114059,
	PointCloudRegognizer_ResetMatched_m7934FD8DE2E96B57BCBE31BFCB049633F2A8364D,
	PointCloudRegognizer_OnRecognize_m126181FE25A17ECCFF816FC5F71FBAE21C6A233E,
	PointCloudRegognizer_GetDefaultEventMessageName_mF888F346E486CB90033DEDCC95FB4E2E9123209D,
	PointCloudRegognizer_OnDrawGizmosSelected_mB3579B73D28399946856A036B35434FFB283A209,
	PointCloudRegognizer_DrawNormalizedPointCloud_m56640ACA058E42D8FEB3B22DD2EA6A313CBC10D4,
	PointCloudRegognizer__ctor_m2445BF923392FBDDACBDBAD89CE1937E92C426B9,
	PointCloudRegognizer__cctor_m2753FBBCB2FB76239076DBE49B32AF404B911084,
	SwipeGesture_get_Move_m87F0CA1FF3580E08C7B94803463CB3BDEB314CC2,
	SwipeGesture_set_Move_m8819B56852D396F222DF59B14A2D040E1830978D,
	SwipeGesture_get_Velocity_mE51E8A90D189DA02E4EA59A9888F43865A6F8B70,
	SwipeGesture_set_Velocity_mDC5D7E935129C5F630FAD38E5FED216DCBCDE8C3,
	SwipeGesture_get_Direction_mB02F8DA8135F5B44D56E356209A58208FD4823A2,
	SwipeGesture_set_Direction_m664F31467D42FC6780DD1486F10E8B1EC6ABB0DD,
	SwipeGesture__ctor_m89B3AEB64F14D123190648425B06FBEC67C8E828,
	SwipeRecognizer_GetDefaultEventMessageName_mF1572A6E8CE27DA433C2E108A5D1158535413473,
	SwipeRecognizer_CanBegin_mB572BD35BD0F9A7738FA9CFFBD5C8FCB52685302,
	SwipeRecognizer_OnBegin_mB3F4C79F7560443F3323DA438E0B74F86D08ACF0,
	SwipeRecognizer_OnRecognize_m1DD0AC54837974FD4E183D6F76D73C0D3A7FA521,
	SwipeRecognizer_IsValidDirection_mF8CD160088F6BD8CF7293C2972724DB99EC7D3F9,
	SwipeRecognizer__ctor_mA935A13E1C9C5ADE31E334C01446CCF8E15A035A,
	TapGesture_get_Taps_mCBD2C2DB8CAA419F7625712C568847A1436C80B3,
	TapGesture_set_Taps_mA3AF859A743F9EBCDC398E8B446E0923E9401CE1,
	TapGesture__ctor_mD600935C90B4DD008F532942E401D2AD6E59B01F,
	TapRecognizer_get_IsMultiTap_mDEE38EC01EC6DC25BD6B42604C293F1C2710B8B0,
	TapRecognizer_HasTimedOut_m8B4D22962E897064DE34895D61FE9EAEF4730E72,
	TapRecognizer_Reset_m6663B7BBF33B956A30219019542049F0ADE4329D,
	TapRecognizer_MatchActiveGestureToCluster_mA5E92A4A30186DEA561C589A77E42F4C8C3E01A3,
	TapRecognizer_FindClosestPendingGesture_m4B664938134EF27D3D5B20EB5996F86247E90D12,
	TapRecognizer_RecognizeSingleTap_m13BA3F3BB83050A77BC82B2B78D8C427B2EEE14F,
	TapRecognizer_RecognizeMultiTap_m985A724143377F014F210D17935D1FCC85244C24,
	TapRecognizer_GetDefaultEventMessageName_m16B07946FAD2560920A71BD8D76AACABBC87A5C1,
	TapRecognizer_OnBegin_mEE739FA1361DC4E287D4BEE21574607AFDCF7899,
	TapRecognizer_OnRecognize_m65D4A5631B638BAA1788F5DB714CE4EDB4B1FB02,
	TapRecognizer__ctor_m7F346866560018BF0286F66D6DE313614D2F863E,
	TwistGesture_get_DeltaRotation_m3D7F3162B11DE8BB804284C4858450ED88D2E234,
	TwistGesture_set_DeltaRotation_m416B0DDE1E3DB369F3603DA9D158C94669D20F77,
	TwistGesture_get_TotalRotation_m9D341DE733C95B8D7A7490CEFA00AD9C9BF91CBC,
	TwistGesture_set_TotalRotation_m1D905E69C5D3B7507859E64C94BEAD1AE7C9F637,
	TwistGesture__ctor_m76A30A059C2D659C835C1A4790DA4EB8DF568170,
	TwistRecognizer_GetDefaultEventMessageName_mF7786153A5B741E37848E587DBA2F39034E6C644,
	TwistRecognizer_get_RequiredFingerCount_m1C2030C2C36DC8DDEED826904CBC55FE73B32493,
	TwistRecognizer_set_RequiredFingerCount_m0238672B86CB924044FEB96F6B5ADFAB8DC709A0,
	TwistRecognizer_get_SupportFingerClustering_m647C994A421BC4B60F1674B92B0564E96ECFD8C8,
	TwistRecognizer_GetDefaultResetMode_mF066D28750B6FA5E486584434F15E23924C8FD7A,
	TwistRecognizer_GetDefaultSelectionForSendMessage_mB68E56C00F506BF0281914A9250FEA62BEF36E38,
	TwistRecognizer_CanBegin_m9F5ADD27AE0C1A65E6D6BDD242D73E62CBB6047B,
	TwistRecognizer_OnBegin_mE7C52F0A5B8676E76F0219F3276847BE8C7945D1,
	TwistRecognizer_OnRecognize_m006006723683B0DFB53AF9FB04EFA41BD568BDB4,
	TwistRecognizer_FingersMovedInOppositeDirections_mE1669F2E73404178083495D10D961C67B3458FF8,
	TwistRecognizer_SignedAngularGap_m41B87BFF693B4DF4B3D949B493C38045099DF6A8,
	TwistRecognizer__ctor_m77C8BF5B07685C6D3E94D2BCDACC01ECC43101EA,
	NULL,
	NULL,
	FGInputProvider__ctor_m0BB11FB49FE35A586A2B9275C5624240056C86B6,
	FGMouseInputProvider_Start_m905BBC6204C1566F7CD5ADF8341C0F4128FC436F,
	FGMouseInputProvider_Update_mFD367ED3881C1768A5D258DED84980C8F7828DFB,
	FGMouseInputProvider_UpdatePinchEmulation_m4C666C33FE4404223BB63C8D862ABDDEFC67851E,
	FGMouseInputProvider_UpdateTwistEmulation_mD0161333C22AA1C4D20BEE31C2C01CC3A2A2E0B9,
	FGMouseInputProvider_get_MaxSimultaneousFingers_m0642411762D1BB27B109DAFAE625C28BC4D57E7D,
	FGMouseInputProvider_GetInputState_m0B318531156548529CD1640AAE4FBF30929C2B9A,
	FGMouseInputProvider__ctor_m76B5B0E3B61036A2ECF24FD8B4BD9653FA881F53,
	FGTouchInputProvider_Start_mCA913CE3D5DADA55A61530B3D137708850572F7F,
	FGTouchInputProvider_Update_m908551835F013DE2826B754DA6FF59E8A7700ABD,
	FGTouchInputProvider_UpdateFingerTouchMap_m46687B0EDDD6EF4830E533101115668F6DA5371D,
	FGTouchInputProvider_HasValidTouch_m7A4CFA0D3837102BEAB4E44F9D40DBA02EABDCBC,
	FGTouchInputProvider_GetTouch_mC2247AD7AF212AC9AFD75FA8C8E5EB1026FECEB4,
	FGTouchInputProvider_get_MaxSimultaneousFingers_m8AE4B4CB0868A41565C657DA28C05A75C801FD96,
	FGTouchInputProvider_GetInputState_m316A117F95A6D240EA3451CC58AC8561F65FFAC1,
	FGTouchInputProvider__ctor_mEE4BE4785CFE15000D5CEF84C543283D71C0676C,
	TBDragView_Awake_m71B00EAACF4F25E4AB0F5AA11B2ACE997AC0D920,
	TBDragView_Start_mDA5B612E5740256BFC0BCCA56FE0D3EDB2A87803,
	TBDragView_get_Dragging_m8C0F6C674CE33DDFD52F94A472C0B04E70B3AF51,
	TBDragView_OnDrag_mFAC7122976333520D30800B137C4FD69CCFF430F,
	TBDragView_Update_m1FBD10BCE16C5044334894C9F630FF298FB03783,
	TBDragView_NormalizePitch_m9DB1CFF16B8C98E6FD4DA681FE747374294BB8F7,
	TBDragView_get_IdealRotation_m2AEFD72BA646B2DB31BF9799545A999D4B394468,
	TBDragView_set_IdealRotation_mEA8959FD0E2728EEAC69275426068EBAA536D208,
	TBDragView_LookAt_m03BA7D1BCD6FF4B2BB3808BDF26D2FB8FB5669F9,
	TBDragView__ctor_m0D6A5A9260EB34ACEEF238CAE090B45469C6BA86,
	TBLookAtTap_Awake_m5A7AD67EDD1ABE879950E82951B89CDB0BCDA824,
	TBLookAtTap_Start_m3212F2DABE340BCFD9F275FEA65810F65333D611,
	TBLookAtTap_OnTap_mDD8F1BFE530E4BBFE6A454E7B22BC294E05F7C6A,
	TBLookAtTap__ctor_mA65E9D8A4FC111A2B8553CAF416D4EDC9D5114F3,
	TBOrbit_get_Distance_mE5FE085580F69F74152919D02B703955DE909C03,
	TBOrbit_get_IdealDistance_m6BCA715D54213C4C3914F4C1B7712C50FCCE835B,
	TBOrbit_set_IdealDistance_m7FCF141D84D04B66F0E5C836224DC98D7B5A0D4D,
	TBOrbit_get_Yaw_mD04B127E5FD5CABA4A1D822014C2F04CBD94609D,
	TBOrbit_get_IdealYaw_m212683D73343105A3435A5B6C864A36B4CEC9FD5,
	TBOrbit_set_IdealYaw_m3852EC8F27C486DA0C578E4B3F84A9CEA985663F,
	TBOrbit_get_Pitch_mA932AA3B3197DB3ADD3C00D2748B483FEA84FB55,
	TBOrbit_get_IdealPitch_m78984A0FE189C30A9E1A6885C1B385F43E28A43B,
	TBOrbit_set_IdealPitch_m012C2A78264759B76D23EA69316D51CCD6EB2838,
	TBOrbit_get_IdealPanOffset_m7C63DF20C4B0C91E4D3AE44DCC2E34B4AC64CDF1,
	TBOrbit_set_IdealPanOffset_m9BF6F0DCF2BB878921E685E298B081CE1EC0D2A4,
	TBOrbit_get_PanOffset_m2935C79AF272994CA3D5435F768BD5AE26E4EDC0,
	TBOrbit_InstallGestureRecognizers_m737ED1B79693328D95E7F7EA0FCF2B27D32CC61F,
	TBOrbit_Start_m49521025CDFBDDFA408791F3D5ED9A049979C5FD,
	TBOrbit_OnDrag_mEA0C6C8C2B1DAF87B2815E00414035C0CEFFF3DC,
	TBOrbit_OnPinch_m482446A870858574758C0477973F5282F986A634,
	TBOrbit_OnTwoFingerDrag_m93FFBE25DB907F65F59120DE04B8627327CC476B,
	TBOrbit_Apply_mD70C449BBA7DA16BDD6E5EC66176EF5E4DB34D8D,
	TBOrbit_LateUpdate_mC2CE68E99EAFA33787F2049B2C954FB06AD45236,
	TBOrbit_ClampAngle_mEEC6F41E13722F0510BE62E202F6E7002B91F1F9,
	TBOrbit_ResetPanning_m61C00B2A917391A22FA9DF81C7D09862D114F2C1,
	TBOrbit__ctor_m99DC5B8C8C54993B78B509B6641EB1A1B3469640,
	TBPan_add_OnPan_m9BBAA3F2EB1575361B697BDE66D0DB43C6260A6A,
	TBPan_remove_OnPan_m0F4591FD702F04B933C577A2CCD95236E1BEF9DB,
	TBPan_Awake_mE3B2605744913576CF8020764AE9DFA8F2D8D2B5,
	TBPan_Start_m9CFFED405C5CAC7CD47C653BB95A93DC0E0E6BE1,
	TBPan_OnDrag_mBD48EE5AB07F4EBC1C19A37535735335872793EF,
	TBPan_Update_mDE2E3BC9F73B6C88CEBBE565C1108DDF50B81FE0,
	TBPan_ConstrainToPanningPlane_m77719C9418286026532A30F2784FE6E1AB693617,
	TBPan_TeleportTo_m3938C17435A0E25A7386C2F1130BADF9AD21625B,
	TBPan_FlyTo_mFE546F60B5BB52AC274CF2D56427F3498659EADA,
	TBPan_ConstrainToMoveArea_mD98E7755AC51F97D1F8473F52A24ABBE7DA3DB19,
	TBPan__ctor_mDEBFFF0A954CEEB023089636FDD4EDE84A5A49E2,
	TBPinchZoom_get_DefaultPos_m7DDF1C1D1C23CD4958E2E996402A192DEB4CD838,
	TBPinchZoom_set_DefaultPos_mD45537145F6540981C62D0692D93E660C74CF23D,
	TBPinchZoom_get_DefaultFov_m77B8A301B233BA1C86A6FC3DE700DD289F05B723,
	TBPinchZoom_set_DefaultFov_m52F3999D4675E5C0D966B57E2B471C798AC08AA0,
	TBPinchZoom_get_DefaultOrthoSize_m8501696D9C91E3D2383C62A2810FAE881A2DB532,
	TBPinchZoom_set_DefaultOrthoSize_m1E1C9EF709F89E9AE8C0C1D5D036C65C87958C74,
	TBPinchZoom_get_ZoomAmount_mC5ED2D7E435296614965C1A9E5110624C7B26839,
	TBPinchZoom_set_ZoomAmount_m5D571B3A67F3B190CBAAE25CA30E80C744299E67,
	TBPinchZoom_get_ZoomPercent_mA220FBC5F974A35BE74CC38DE6895048CE3947F9,
	TBPinchZoom_Start_mECA3F2698B639C7EC252C98B3AFD02B528D51695,
	TBPinchZoom_SetDefaults_mED97025E31E10D6F1FF73D53778B3C4E558CEBAE,
	TBPinchZoom_OnPinch_m5557CAA00398CC01DBF6FC546279C7459CD428FE,
	TBPinchZoom__ctor_m0C7E9859454B93EFB8099B36E17BF9FDECF26439,
	TBDragToMove_get_Dragging_m13BD30ABCD8F41D5D8E4F74F19C28E66033CFFE6,
	TBDragToMove_set_Dragging_mE2434925C6E6F3B727270D1AAF40B3F64AB27903,
	TBDragToMove_Start_m201EE45E8E04FCA2B5F4F1AF03E1D9991D844C0A,
	TBDragToMove_ProjectScreenPointOnDragPlane_mAA852D31E565D7C15E6062F5C6BB692FD95A6F6D,
	TBDragToMove_HandleDrag_mD26B1354B4113AED65B0442AB6CB21A836FF884C,
	TBDragToMove_FixedUpdate_m79FCE2CE036ACCC26D8B1EEAF40B756648BDAC37,
	TBDragToMove_OnDrag_m81B318CA826400A99A8664B821FC0B22E19421B7,
	TBDragToMove_OnDisable_m2BCE129FBF45845782EAE46A60C5B95BB4B0E681,
	TBDragToMove__ctor_m8EA5908B5B6783AF88BE3AC2D218FE6B9BB321D6,
	TBHoverChangeMaterial_Start_mF9EFE7682B01B732B1001D536BA7EB61D760F60D,
	TBHoverChangeMaterial_OnFingerHover_m531BDDB44384FD6EF4180EF184DA18C20638C0FC,
	TBHoverChangeMaterial__ctor_mE20D28D976CA1FFFDB5E1E5592808BAED8D50B03,
	TBHoverChangeScale_Start_m4953B61067C314FEC3EFC98E53AC060FBA5E38D7,
	TBHoverChangeScale_OnFingerHover_m8F2732AAB6F14E66346A8F7C9F95C94485AFB43B,
	TBHoverChangeScale__ctor_mE20D4956CAC7E4709A32C8FDD7C7BF6C156F587B,
	TBPinchToScale_get_ScaleAmount_m2068956B7FEF92ED47B114C74EE1381EE9AE4A0B,
	TBPinchToScale_set_ScaleAmount_mB0B0637C806C36F80CD91F5AE8A76959A5697BCA,
	TBPinchToScale_get_IdealScaleAmount_mA082A819E1D2AE780129863D3AF81F4CED353DF9,
	TBPinchToScale_set_IdealScaleAmount_mE9C2C5758153F1EBB9EAFA88B61F37325CDA42D4,
	TBPinchToScale_Start_m961759F3FE750E6BCA4C3043B4680EC756298374,
	TBPinchToScale_Update_m775C06A9B3FE31BFAB0A6ACCE2A66AA45A87BDF1,
	TBPinchToScale_OnPinch_m59F6AE58FC7B1DA7A7F78CD5EA50E9F08775A81B,
	TBPinchToScale__ctor_m191397A1F682A004904E8740F097B4F356415155,
	TBQuickSetup_CreateChildNode_mC411E1FEE1689513AEBA9C6F4880F2EC12A87FCF,
	TBQuickSetup_Start_m2591FB573E82E3EC669B16953F75ADAD11F770D6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TBQuickSetup__ctor_m7989B7C78EEE8111ADE341B5543D4603474A6293,
	TBTwistToRotate_Start_mA8C93A1183330EE5536CD59432C8017882A8217A,
	TBTwistToRotate_GetRotationAxis_m09A187E8BCF635C8CCE180294881C6A542754832,
	TBTwistToRotate_OnTwist_m097ED7D3E35E81D254A8CDFFAE44EA16B5496DF0,
	TBTwistToRotate__ctor_m0105BB85D9CFD7D52B8BFD198075B94F3C429639,
	NativeGallery__NativeGallery_CheckPermission_m6A0F8861FDB10A7901F2B71DC9DC8C516D382D30,
	NativeGallery__NativeGallery_RequestPermission_m5467E875D262F47FB5AA35ADD642F3F89E86AE94,
	NativeGallery__NativeGallery_CanOpenSettings_mBC81EE459401A771AD2F7D75AFE6F41AB2BA33BF,
	NativeGallery__NativeGallery_OpenSettings_m4EB8AE632743DCF907879B1726613DEB3F03776D,
	NativeGallery__NativeGallery_ImageWriteToAlbum_m4AD58389EBBC098363150D3AF1E9AE855D0E873D,
	NativeGallery__NativeGallery_VideoWriteToAlbum_m65ED6475091CE8FB3413BCE8FE946EDD91CD88AF,
	NativeGallery__NativeGallery_PickImage_mB98113DC9DD09AA4BE6E11BF6B71456BB8CCF193,
	NativeGallery__NativeGallery_PickVideo_m127C9DBF9DBDF181BBE44A3289C5A9F87222F9B3,
	NativeGallery__NativeGallery_GetImageProperties_m94BB08DF2BE2691E01FD438ED4943215BFE5885E,
	NativeGallery__NativeGallery_GetVideoProperties_mFDDAE4F5F65BC207232274D73369F44377D75633,
	NativeGallery__NativeGallery_LoadImageAtPath_m9C924424AF1C98129472BA4BD29F10C560A3A050,
	NativeGallery_get_TemporaryImagePath_mEAE96C176427B270ED15B3562CB160162F9E6FAA,
	NativeGallery_get_IOSSelectedImagePath_mAA64ADC4A5A5981D47F4C95D0D1E3FCF45198DDE,
	NativeGallery_CheckPermission_m5D018CD533A1F52CAE5F7BDA64BE4B40D6645EF2,
	NativeGallery_RequestPermission_mFE13C3D9F015B5CFF7CDB382236D76F848BA133A,
	NativeGallery_CanOpenSettings_m1567631902BF8C4EFD0CEC3B366ED0604D6B9885,
	NativeGallery_OpenSettings_m18B842C8DB49EBE163135FD737226763A8B08331,
	NativeGallery_SaveImageToGallery_mED0868FA4D895AF24FB7BD5F2250A40F903258AD,
	NativeGallery_SaveImageToGallery_mB38A1DA4118305F4BA9FDF201B9CFF7D13C0AD4F,
	NativeGallery_SaveImageToGallery_m29D2A7A8C05ECB2B99DAF4B82A86FA59E21F6A1A,
	NativeGallery_SaveVideoToGallery_m03FDC15738A2E6DBE77ECA810889A8031C9FC778,
	NativeGallery_SaveVideoToGallery_m7649D4C134A18732BCF7E8D2C5300FA2A0563205,
	NativeGallery_CanSelectMultipleFilesFromGallery_m72A5847CF7FF2C830F36CA51836F7743F285023F,
	NativeGallery_GetImageFromGallery_m216C9B77A1A5C9085A5F60ED408394A44E5C2ED9,
	NativeGallery_GetVideoFromGallery_mFC68232D70A047B2148CD1F4682866AF52BF6D55,
	NativeGallery_GetImagesFromGallery_m52307A84BB3CAE28AC4C2BC668B78D6A23FFB283,
	NativeGallery_GetVideosFromGallery_mF4B1ABF9EA0F6E1C34CC1FB286B2241D117147A6,
	NativeGallery_IsMediaPickerBusy_m2BBEBAB9DBA14B45C0623326DC755C17E0104B15,
	NativeGallery_SaveToGallery_m289F6C68D1E8B6B4172442AA9FAD378EFE648E0D,
	NativeGallery_SaveToGallery_mFFC16470707CAC17A0A39341804D8FCAC468BA8A,
	NativeGallery_SaveToGalleryInternal_m4D411FAEC19293304DC4F22C8869C37760D73677,
	NativeGallery_GetSavePath_m98C64A60B7F3DC454CF04DEE8E7889450C3AACD6,
	NativeGallery_GetMediaFromGallery_m0D515D8559A7CA0536AB1FA44AC252784FD912F3,
	NativeGallery_GetMultipleMediaFromGallery_mF2D3216FA2588691B68299C5459291B611F5DAC3,
	NativeGallery_LoadImageAtPath_mF8EA4DF308209C74BE4E65CD6FC34A29C4CD9ECA,
	NativeGallery_GetImageProperties_m763AF380AC987AB7257C326B14E8A67607F7D348,
	NativeGallery_GetVideoProperties_m1ADF41C7D3DF91E001DB8F575B30DC107E6A4159,
	NativeGallery__cctor_m9FE86F86E41772CBDD90E3774F48F3B2A197DB41,
	Astronaut_OnEnter_m1265E8A21CF20B4BCD6A6FF4DB5FDC2E09637FBB,
	Astronaut_OnExit_m9F47C0BBB3422C5E3EF0C26CB57005D75D71D533,
	Astronaut_StartDrilling_mED1F5F9A91A0A51EDA08BDEF99314899288B42B9,
	Astronaut_AnimEvt_ScaleUpDrill_m2824CCE17323144AEB720D7CBF68360EAB518062,
	Astronaut_AnimEvt_ScaleDownDrill_mF4FC35FD5BB27BED121575A51FAB2F29ED43F5E8,
	Astronaut_AnimEvt_PlayDrillEffect_m08725D89D34C0836E50C4CACE1CE4AF1A2196A8C,
	Astronaut_AnimEvt_StopDrillEffect_mABA7781EE3C53ACEA52532A24F6B0607FDC493A6,
	Astronaut_AnimEvt_StopWaving_mB30E88D8094247EEA81849DE7A9331E607753E3A,
	Astronaut_AnimEvt_StartWaving_m4331D9DAFB6E3A79A3C21E8C299D8E7C5873671B,
	Astronaut_HandleVirtualButtonPressed_mC9207408B17331D52FDE8E8C6072B29A455D557E,
	Astronaut_HandleVirtualButtonReleased_mC1BD1CE733EE227AEB8EB3C6C4B8571C5F3F79DC,
	Astronaut_get_IsDrilling_mE91BCBBBAE730D0F4BF091066593D622B3A731CF,
	Astronaut_set_IsDrilling_mD0E8EA1EF4A006A7C72733F5CC4C5CC1296B2BAA,
	Astronaut_get_IsWaving_m12DB912BD8E6E9B040F34C6387FC080C746AD567,
	Astronaut_set_IsWaving_m1A2ADE09D8DBE20DDD81A7681768F874043135B3,
	Astronaut__ctor_m614E1FE435ED399704CD6169D99AFD41EE048642,
	Augmentation_Start_mBB26EE08FF087F1092F9A1D196EA0433DFC83EA6,
	Augmentation_Enable_mF6123555D35D6B859B8D0D40170B5D8F261EB9E5,
	Augmentation_Disable_mEB0FBBFD00B76952F950C496E29F33573300EF1A,
	Augmentation_Restore_m2E1179ECA869DE07FF0B0F4FDAA6A7B7068D8E46,
	Augmentation_OnEnter_m843570501B36E83B377ED7CA0AEC5BA364200B14,
	Augmentation_OnExit_m8BD837705992F828626D92FAB738665C31FE873E,
	Augmentation_SetRenderersEnabled_m4DCADDA846925909559E9CE0B080924DBA69BBCF,
	Augmentation_SetCollidersEnabled_m9425EE6822E9CB0A6AA0896A949A17F368D1DE78,
	Augmentation_WaitForThen_m207FBAC26DCCABBDA7398974A3D8138A1D566EE5,
	Augmentation__ctor_m945EF46FC3E87A3C4FBE8D311A28ACC9CF5DED60,
	Drone_OnEnter_m00CA9816D12CC2FED05CAA07CCDAED5A9ECA0202,
	Drone_OnExit_mB74F5D4B6CDB04C8D5B315475DAF32CE48D30DDF,
	Drone_AnimEvt_StopScanning_m8A8009410142561CB167F685978AD181220BB95A,
	Drone_AnimEvt_StartScanning_m062C31EFC78A11CA24FCCC1AE334E970797BC097,
	Drone_HandleVirtualButtonPressed_mF5F7A249429D11B943F34123B1B9AB9019D31662,
	Drone_HandleVirtualButtonReleased_m11E37050FA69C62171DC23547876CBB70296ECB8,
	Drone_get_IsFacingObject_m1A5D680C644C5F2009E146E89C7108CC8EF10161,
	Drone_set_IsFacingObject_m3E260D366C96464E6A422757FBF49B43231AEF37,
	Drone_get_IsScanning_m3DEA6A0ADD242670BCE9ABBA5CBF617A7DC88376,
	Drone_set_IsScanning_m1B92CDECDD0DDE8051B85AF4B04625470DC52E85,
	Drone_get_IsShowingLaser_m016FB95F4D3F772FBFA3DA41D5D48889C691D98E,
	Drone_set_IsShowingLaser_mBC5F87DBA8C284F09BE69E142A37EF6A43D824DA,
	Drone__ctor_mE344ACBD947ED29C5DB5E5E20418175EBA955778,
	Fissure_HandleVirtualButtonPressed_m40F34F6E24A2CD54D7880D638135C7FCAD978F47,
	Fissure_HandleVirtualButtonReleased_mB806B67638FDCCCF72002629D333A8435115F3B3,
	Fissure__ctor_mC1D270895E157B1972DF315028249AA3D9568030,
	Habitat_get_IsHatchOpen_mCD079D6FF3CBAE82219F33AF5C0A837DA3E51423,
	Habitat_set_IsHatchOpen_m926AA2DD5BEEC2F0EFE0C735D0BFFC5D528412AA,
	Habitat_get_IsDoorOpen_mF496D3A6A8DD016D18412B0EC9B46B870A3FBEF2,
	Habitat_set_IsDoorOpen_m48319C32BCBC40B25B5723E0E3C8A38AA9942EEE,
	Habitat_Restore_mAE64E56638471436E7A35D8EB7BC3844FA90D014,
	Habitat_OnEnter_mF43B038838E9DBC959A8520CC0C8A6CAD49ABD55,
	Habitat_OpenDoor_mC8B939FDA0072516BA4C65166F7020F8C531155C,
	Habitat__ctor_m508CA8986B60BF4033E61F5E6318026B4E4EBC13,
	OxygenTank_Restore_m9BD6C8071B515E494E492429589DE8CF62A9D6DB,
	OxygenTank_OnEnter_mB073CED4090EB3CE3C25076CCCE8CAC01BDA84A5,
	OxygenTank_ShowDetail_m46CE2D709B5F180BDC6BCCDF77E010DBC05F9027,
	OxygenTank_HideDetail_m9A2F848B50C42500D14577F6DFC472E5F8D9711E,
	OxygenTank_HandleVirtualButtonPressed_m8391607236AB6D2D118F4CB768A4874A013FC0CF,
	OxygenTank_HandleVirtualButtonReleased_m79B1211A7D8DD58416E4BDC4BFA3A0B7C58702B9,
	OxygenTank_DoEnter_m2B925A90A82DF18C2640F5F765493DCC48D8BCDA,
	OxygenTank_get_IsDetailOn_mFC6EB504CF463252F41546768906A7F7A0A4B01D,
	OxygenTank_set_IsDetailOn_mE08D6DC5C64BA7F821F25410A6EDD261443BAA55,
	OxygenTank__ctor_m1DF71084DA171966BA4A2A1581B712F3BD30D62F,
	RockPileController_Awake_m9D28FEF5D995FF712DFF131BDEEEF3C44471318A,
	RockPileController_FadeOut_mAFDC00C183C0C1DD38D757D79FD8696F7DB46902,
	RockPileController_FadeIn_m3501124057C9F4BFFC2771997C5F68E513F060D8,
	RockPileController__ctor_m3F5DAB61F61F074B9CF5F96C571A88FA65F1099A,
	DrillController_Update_m7A2DBB5F074BE5DE7567E613E718D3BC908B9B47,
	DrillController__ctor_mAD0640F95405E58306F0E583A212F9391E4645D1,
	FadeObject_Awake_mEA63791C2C7B0704470C7845C3E7744E6EAA95C1,
	FadeObject_Update_m879556E92DBAE199B7EA96FD6A8257A7AF3F1414,
	FadeObject_SetInitialOpacity_mE811FB906876DB13872A0395C12E64E1073A968E,
	FadeObject_SetOpacity_m93FA3C165115AF455C45EC33FB583E43CA3B889D,
	FadeObject_SetRenderingMode_mA2C5640425C60DAF556D5075DC5C98F298B3C9D3,
	FadeObject__ctor_m2C0BB37D9ECA56A5CC18CEB658FA12F6A281B3A1,
	AstronautStateMachineBehaviour_DoStateEvent_m5C0E38CC88FCF3FD33199D81051B634EA312BFF2,
	AstronautStateMachineBehaviour_GetTargetType_mC5FCE099514B78F9044EFA64AEF8AB3C3CBFFC25,
	AstronautStateMachineBehaviour__ctor_mC414FDC7D7C569AC0F898165CCD28F753A182705,
	AugmentationStateMachineBehaviour_OnStateEnter_mF0F7D66626AB8A6F4D26B24D3EB31FF15A878CCD,
	AugmentationStateMachineBehaviour_OnStateExit_mF9B5AB9C94D8D219FDD12FAA24BD05BFDA8DEBEC,
	AugmentationStateMachineBehaviour_OnStateUpdate_mAA55ABCBFCAC71FFA7C69F9CD8825D2A80386D6F,
	NULL,
	NULL,
	NULL,
	NULL,
	AugmentationStateMachineBehaviour__ctor_m968E88E7F275F78DA7EB78908C4A3D622152C6E6,
	AugmentationStateMachineBehaviour__cctor_mACFB66A8EE2CE87C290AF372C28CAE0F010B8789,
	NGMediaReceiveCallbackiOS_get_IsBusy_m1CA1732EB20946FF22EFAD5EFFBD248F85B3F46D,
	NGMediaReceiveCallbackiOS_set_IsBusy_mF3B5F4564B0807EC621E9D773A88EE65A67F07FD,
	NGMediaReceiveCallbackiOS__NativeGallery_IsMediaPickerBusy_m9E008F04C1CABBCF52A6C63C3DD8CBF60C1FEA7B,
	NGMediaReceiveCallbackiOS_Initialize_m04FB6D655CA4E7B13B2585DA3F0D414058DC8132,
	NGMediaReceiveCallbackiOS_Update_m71798872CAA675048E8DB8CB4B742FC0A1B7C88C,
	NGMediaReceiveCallbackiOS_OnMediaReceived_mAE345C1B38DA02EF1AB0BB1913082B529D286D81,
	NGMediaReceiveCallbackiOS__ctor_m2965A3D300B4D62C78499FE7B872EFA5E3A1490C,
	NGMediaSaveCallbackiOS_Initialize_m3206E390197153C037E1698B19CD1F552EB66194,
	NGMediaSaveCallbackiOS_OnMediaSaveCompleted_m194CE14B4029AE8D5180B52318C709A7B32B890D,
	NGMediaSaveCallbackiOS_OnMediaSaveFailed_m537D39C18B5CB85E683E8B38D3E222B1F686C278,
	NGMediaSaveCallbackiOS__ctor_mAC53D9541A341FB6E9A7766EE2C0A9F34EE174F4,
	LeanDestroy_Update_mF8A7B5B488ACB75BBEDF562AFCF7A93D294BA3A5,
	LeanDestroy_DestroyNow_m444028047AA956152270C46F2DE1264C0EFD3E81,
	LeanDestroy__ctor_m758FBC909E7BCD7FF0C11F5F399E42A09519C3BF,
	LeanDragCamera_MoveToSelection_m1977E8C845757FC138CE4F1034BC847FACF5F7B0,
	LeanDragCamera_AddFinger_m24F4729455607956757363745333CDBBEA75D61E,
	LeanDragCamera_RemoveFinger_m9AF9D2989DFEF3E763F5A9287ACC3F0BCB70A2DD,
	LeanDragCamera_RemoveAllFingers_m610023930CDB313937193FBDF90C7D1B9D949F21,
	LeanDragCamera_Awake_mF6560D575DB51AFB2D35787745A1D115964C42E0,
	LeanDragCamera_LateUpdate_m0ED3B37DAB02F748A6B1B6CAA51240018FEE7CAE,
	LeanDragCamera__ctor_m945AB4E3D7224EEEC4BA96EC2A524643C3B2A882,
	LeanDragTrail_AddFinger_m3938B8A3923CC39D1DC8519BCD22501599AC08F0,
	LeanDragTrail_RemoveFinger_m0CD4D6D556460B5007DA1F6A6A65AE5D2E9212AD,
	LeanDragTrail_RemoveAllFingers_m16EAFB4113DA305AB97E93A8782434ACFCAEDBAF,
	LeanDragTrail_Awake_m307D8228F7EABDE52FCDB036C2D395DA02F609A4,
	LeanDragTrail_OnEnable_m1C3283DBC79D8BD766B12886462A23C001437ACC,
	LeanDragTrail_OnDisable_mC3557A3D120CAD1848776EA9E7ED54B69380D2D7,
	LeanDragTrail_Update_m37EA28C4BAAC9F061D85CEA604028CE2138C21FB,
	LeanDragTrail_UpdateLine_m50676A6A1320D9DAF7EADEC284C9B57A15E54E13,
	LeanDragTrail_HandleFingerUp_mD038BEA8B659D7DE067C2FBA042D90F3DF3C9B97,
	LeanDragTrail__ctor_m110B282844E3C9825A9B661E213FC790978D43BD,
	LeanDragTranslate_AddFinger_m07D215FA35418FB83D41781CFA54BD13EB109E9A,
	LeanDragTranslate_RemoveFinger_m3C86E3F451E0E7B02EDCF5459D1E945EE6DFC1E2,
	LeanDragTranslate_RemoveAllFingers_mD3A5A560517FA88350ACDDDC7B99B9717770F6C2,
	LeanDragTranslate_Awake_m52E59493927D82C08459200D9949E2D995061ED7,
	LeanDragTranslate_Update_m7B3A74A95B26058BED511014AEF401F407A951A9,
	LeanDragTranslate_TranslateUI_m7E8CAB2BD7AB2BE98DCA0356F133C56B7892D83E,
	LeanDragTranslate_Translate_m7F402395FB2B5C749BE7AB7DEAB37554FE563EF9,
	LeanDragTranslate__ctor_m02088D0CA3FC55BE1362826B756553CB6DB452B8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LeanFingerData__ctor_m159D6946AD579652B3CCDE0831D474424579E59E,
	LeanFingerDown_get_OnFinger_m7DE562F50EEAFFEA842E1012F186B8836F98FB46,
	LeanFingerDown_get_OnPosition_m636D47F9AE685B92A42C52E48EC3B8CC9F522FB6,
	LeanFingerDown_Awake_m4311178FA4CCCB76D8B2446B9F5C78A5F072EF54,
	LeanFingerDown_OnEnable_mECA843AA2316CF70BF27DDD74C0245CD9F2F7F71,
	LeanFingerDown_OnDisable_mCC8DE45EF430CC413649765CF46E4F612211B30C,
	LeanFingerDown_HandleOnFingerDown_m0A95EAAB8A6308A4B4F2EB21CC9722A7CB93A50D,
	LeanFingerDown__ctor_mF96DF83185F2085EE110A5C4B6CF59F3E3EC1154,
	LeanFingerFilter__ctor_mBFC4268C31CBAA1164E0FACC436B3698A800F152_AdjustorThunk,
	LeanFingerFilter__ctor_m0F736E26D1405F2ACC75939E73F3397D41D2BF4D_AdjustorThunk,
	LeanFingerFilter_UpdateRequiredSelectable_mC6094A9811766962D7C3A2C9AB6C40B9405834E5_AdjustorThunk,
	LeanFingerFilter_AddFinger_m09250D81F74E9082F19472A00E737DD3534388C2_AdjustorThunk,
	LeanFingerFilter_RemoveFinger_m812BA1E281903E2295C1DCDEA18FFA84A86F1AC1_AdjustorThunk,
	LeanFingerFilter_RemoveAllFingers_m8F4BF1A119EA7CDC2C248CA54DA11891E582C5A5_AdjustorThunk,
	LeanFingerFilter_GetFingers_m7AFBF5C29A7063DA01584945DFCE7E0D3C53A7D2_AdjustorThunk,
	LeanFingerOld_get_OnFinger_mE6B73F3A61A8307C19114534A1686F9C25517E69,
	LeanFingerOld_get_OnPosition_mA749666459962DE98BD046556F6041EEA7F94E05,
	LeanFingerOld_Start_mA3296021AC37C901A401C8F036E99404BE76E4E7,
	LeanFingerOld_OnEnable_mF379DD1C04EEBE6E623A330966855981875BAEED,
	LeanFingerOld_OnDisable_m9096AB13BB3A751F3A0A7BE34154B5078453F25F,
	LeanFingerOld_HandleFingerOld_mE1540BF7FF8B8BDE97C226C005DB682924B7C851,
	LeanFingerOld__ctor_mC2FD7FA8ECDA275A1BCCCC63D314445D2BF37174,
	LeanFingerSet_get_OnFinger_m4E996961E0D10B4968628DFD1E0F94F9223EE29A,
	LeanFingerSet_get_OnDelta_mE2F138D4718235255469AE70A1E7A410864D7120,
	LeanFingerSet_get_OnDistance_m4D268DA548AB2FFA3754719844C1B45E2C0CD5C5,
	LeanFingerSet_get_OnWorldFrom_m6C33D60530BF50F8B6F6FA29555C834EA2EF741C,
	LeanFingerSet_get_OnWorldTo_m07BE9D21B3BAE46A70986208F7233C8CBE6FDABC,
	LeanFingerSet_get_OnWorldDelta_m70CAB2310F7B5F4B5E3CEC045F3CEF7AF37A7E7A,
	LeanFingerSet_get_OnWorldFromTo_m145C14F1E6ABDC2661F9E76FCBA52961909B3579,
	LeanFingerSet_Awake_mD7E3039B5245F28E8BD92D3480ADA11063DD123A,
	LeanFingerSet_OnEnable_mE7BCDEBE2E4ECEF43A91987C313366743E1DD481,
	LeanFingerSet_OnDisable_m2021F3A0FB46ECE2B899A3FF17C782F6CB7A3979,
	LeanFingerSet_HandleFingerSet_m9C1E0A9CF2F39D516A84FD62EAFF8A27AC1EEA62,
	LeanFingerSet__ctor_mE1CEB2E33A1CF073808E7A55C71597D95C04BCE2,
	LeanFingerSwipe_Awake_mE1CBF341152B57803B9BD06A31F27BDD145B8EA4,
	LeanFingerSwipe_OnEnable_mD2F12AAAFD0C208CBCB22CFCED6AA8F3C1A62BA0,
	LeanFingerSwipe_OnDisable_m744E173982D8F8114E1BE45714ABB4AFA0369533,
	LeanFingerSwipe_HandleFingerSwipe_m45D9B670A4C7C69BD9E5518593F599B9928254A4,
	LeanFingerSwipe__ctor_m387DC3B157686F6E8CCC73EA7CC069821B07A2E5,
	LeanFingerTap_get_OnFinger_m13DB9AE50245E9870148043BFBA5D8BD45E7B4A0,
	LeanFingerTap_get_OnPosition_mDD9EE156E799427B1707DADBB84B4D9D4BD53DAE,
	LeanFingerTap_Start_m505D06462DA7F79EF11C9296D833EA30093C3046,
	LeanFingerTap_OnEnable_m5CF577D7892BEC4E184278A244677BB6B81B7E1C,
	LeanFingerTap_OnDisable_m385FD2EC0808EED6104A155684639E9D2731C1DB,
	LeanFingerTap_HandleFingerTap_m28CDAB4656102E6919616AD3F025C9110CCBB89D,
	LeanFingerTap__ctor_m35E19E4BC414151DFF8D8D5307C530D202CE2C07,
	LeanFingerUp_get_OnFinger_m73C02A7A8B171B2E149CF0809973AC7CBE0CEA5B,
	LeanFingerUp_get_OnPosition_m83CA6425BF3FED1329C29AF8BDDA7F34AEF31B79,
	LeanFingerUp_Start_m1BB0CB532B5C0C4C357436645F755BAD17AD73BF,
	LeanFingerUp_OnEnable_m22D91964FA0BC2872C8576890AEEE1C12B8DCE11,
	LeanFingerUp_OnDisable_m8ECE14FDA09F2D316B21F6A2C53C38A24542D642,
	LeanFingerUp_HandleFingerUp_m71728216C183E47BFE1C1740879ED4D45C80ADB4,
	LeanFingerUp__ctor_mF8DB788A1B5DCA96E23373E38A617518CF07F479,
	LeanInfoText_Display_m114DAB3AE28FA13CC599C8F61C70C687F4A3A408,
	LeanInfoText_Display_m9FE1655C1B5C4F4479EC494DB5B2F27391C41019,
	LeanInfoText_Display_m9FF95233EF6D977478480A91F2BA7878E4B3DE30,
	LeanInfoText_Display_m1B1F56CA265E6276221D72563E81BFB50DE9D203,
	LeanInfoText_Display_m77908BB5889DAE56FCFF41C1C7CBEF96563680A6,
	LeanInfoText_Display_m4D45AD8DA9EB52B346905FF8076E23F25C7DE2C2,
	LeanInfoText_Display_mB2518593EAC094E0D87AE7AD70CB0F875F9391DE,
	LeanInfoText_Display_mAE5A2992CD9038A4099E4EF8D554D2AA2171804E,
	LeanInfoText__ctor_mE4D892FED00E8CDF0F36C6A7190E63D754610672,
	LeanPath_get_PointCount_m35C11E12B474775005CE2FF70AE53831FFCE7931,
	LeanPath_GetPointCount_m5935B0C45DF23EF5DB558274B8BA7B1BCB62EB0C,
	LeanPath_GetSmoothedPoint_m027D12F3FE44CB50F2FF8BDA15DE8AF71FFB2AEA,
	LeanPath_GetPoint_mD4BEBBE17852C2ADA4FFD93FD9C8D61D697CE4C2,
	LeanPath_GetPointRaw_mFECB2598561519218FAEF764975262F88F9937E5,
	LeanPath_SetLine_m89D54996B2E6BC9C4F7F2DA6EEF93289A62F5DB8,
	LeanPath_TryGetClosest_mAA923A0DD41F711B55FEC934E5A4AFA981AB0D76,
	LeanPath_TryGetClosest_m6FAA821E7586AFB89EB2D9E1386B90031C30B5E8,
	LeanPath_TryGetClosest_mDBCC49A43A5B2660A77BA3FEA3AAB403BDEC4638,
	LeanPath_TryGetClosest_mCBE1D9AFE2F3FA7DADC3EE6242F15817DCB430D6,
	LeanPath_TryGetClosest_m977C30B03D3574234F29DCDA17F34BD4E03F2E63,
	LeanPath_GetClosestPoint_m6C972C673237A1D5589F87F5C2ABAAD014D34171,
	LeanPath_GetClosestPoint_mF97113C8CD459430D714A28F422DB5C5E2680D47,
	LeanPath_GetClosestDistance_m45A8D4BAEEA0148B8B9AAFCE8968CC137CB437A0,
	LeanPath_Mod_m76A17AE9AFB5E8E89052AE6C5CF433AF30B995F6,
	LeanPath_CubicInterpolate_mB99E7792710EA750B9F1ABCE7090B16CE59FFE93,
	LeanPath_UpdateVisual_m2FDADC837DB5FA0A7B698E513C545C6DDAC9FBB7,
	LeanPath_Update_m9E018F03B28B08D50E3CE6D17617BEBBAE998B52,
	LeanPath__ctor_m98D432D34C7AE997ABFB409F72D35413038C47EA,
	LeanPath__cctor_m2E47A6C3324C1BD784A4C90656495F48901B3FB0,
	LeanPinchScale_AddFinger_m1273AC965306C88169F04F9F7FABD5976BA85722,
	LeanPinchScale_RemoveFinger_m4E2F7EDE1B7FF8573975D401FA9AAACE89622F97,
	LeanPinchScale_RemoveAllFingers_m32657CB2488561B6EF30F5FA4BA2D9160F781F3A,
	LeanPinchScale_Awake_mCE83B9A2A51E45B2F6F13956A377AC4F52B84A2E,
	LeanPinchScale_Update_m72B854E5A4DD95938C80D193BACA8D66E1A2C30D,
	LeanPinchScale_TranslateUI_mAC2CBC534D6CDB7DE36EE7AFFAB83615B0EE64CB,
	LeanPinchScale_Translate_m4BFE769460A44FA7E0F34A20C2FB46BCEEE50438,
	LeanPinchScale__ctor_m768950FA0976989878D2C9DF9B614E7DD6801C29,
	LeanPlane_GetClosest_m631B2575D2352080AEAEBEB07180E3404B063627,
	LeanPlane_TryRaycast_m457EDD4F3FD48B073CDA797F1E8066B6EA9164CC,
	LeanPlane_GetClosest_m8BAB222D7C665311787C2E5CBCA3E5513F6E1C3D,
	LeanPlane_Raycast_mADC045C79A30294C25354F0FB530034BE7CE774B,
	LeanPlane__ctor_mC00262ACDD75831E1AF92A54AA870F8707A35E19,
	LeanPulseScale_Update_m6BF94E8B09A9D2D3311A07ABE2EBED82A5EB19E1,
	LeanPulseScale__ctor_m6AE3883E6766A883F3BD67620872035415E76AB8,
	LeanRoll_IncrementAngle_mF536F52FA9B006EF5067159A2985A8EA18FFBAF4,
	LeanRoll_DecrementAngle_m889C800A056A59E2AB6449A3887182175DF17A29,
	LeanRoll_RotateToDelta_mD49D137049B4A03076C501F2C8996558F9AEBD18,
	LeanRoll_Start_m6F127017A68134C6F9CEC4C4D4A14621D3457FA0,
	LeanRoll_Update_m184088A1F671808FF8968F945B6C475F661BE868,
	LeanRoll__ctor_m5D15A02CDEB15CDCF0A6F8BE2B9816A1865C7A5B,
	LeanScreenDepth__ctor_mA8AD2F7C7E663413E39D40569A36D1227943EE7F_AdjustorThunk,
	LeanScreenDepth_Convert_mA14287E3B34DEB00EB318E11763F60734D4308D0_AdjustorThunk,
	LeanScreenDepth_ConvertDelta_m193F2AD42B0814532671EFF4525DF3A57D2CF13A_AdjustorThunk,
	LeanScreenDepth_TryConvert_m9CA913BCFF65194F6E15EAC194E42DF1CB6088B6_AdjustorThunk,
	LeanScreenDepth_IsChildOf_m30A45B6D898B352C8941A875C7761ED372C7CF51,
	LeanScreenDepth__cctor_m0C791BCD2E20C40C4EE1E2EB1F7FD110E0DC74A5,
	LeanSelect_SelectStartScreenPosition_m8CB6B86FB2D2DB58757EF17E99772EAC5817884C,
	LeanSelect_SelectScreenPosition_m8F589402D1642C2CC09AB10194FC5548F9185D05,
	LeanSelect_SelectScreenPosition_mEC74807DEBACB3682F1D392403EC67809293FA2C,
	LeanSelect_GetClosestRaycastHitsIndex_mCF4D2F524F44DF843B8441A0C91D0560E672AA71,
	LeanSelect_TryGetComponent_m5F51FB86ED7E8E474D74FDD618373B6600F23EBA,
	LeanSelect_GetScreenPoint_m7E341BDDD9476781C79DBB8E5A1D26A71301463F,
	LeanSelect_Select_m5965E7DCE0F5FA8AB491C3C272DEA4B25477191E,
	LeanSelect_Select_m895D156285E0CE4279F84C6DD9AD66279254D2B0,
	LeanSelect_DeselectAll_m3FCD75ACF602C94168E19AA72345FD702B819BE2,
	LeanSelect_OnEnable_m17E1F03B0CECF13B98E1839038B03FB57409BE9A,
	LeanSelect_OnDisable_m2DBB65CCC894A40DA6D5C9B7ACFB035E4A8F4996,
	LeanSelect__ctor_m579F98408B0D05A52331E815C12AC3AFC771A761,
	LeanSelect__cctor_mCAF13218768B80266EDF88548DA52D4CD386AE59,
	LeanSelectable_add_OnSelectGlobal_mC460B7598374FAB6CE975F6FB1E7EEFAA5EAD685,
	LeanSelectable_remove_OnSelectGlobal_m104449CF1594F2598337C370538AECB13F8392FA,
	LeanSelectable_add_OnSelectSetGlobal_mC87576D613ABF4715F9BC798FDF46D696D3A36BF,
	LeanSelectable_remove_OnSelectSetGlobal_mB687F15B432D8A5B2988820B57BEE97A857BB66F,
	LeanSelectable_add_OnSelectUpGlobal_m7D4017C794A87B80064693B925682556B82921A0,
	LeanSelectable_remove_OnSelectUpGlobal_m452B3CD26F2331D18183354F3C88423BEE180BE4,
	LeanSelectable_add_OnDeselectGlobal_m1CD0BD15E4C4E692A8846AA181ACC3253672941A,
	LeanSelectable_remove_OnDeselectGlobal_m7FE8C3E84075827365ADCFBD0F241969B418A76D,
	LeanSelectable_get_OnSelect_m2FF28065648083E0E760576BB95C408783B91C7D,
	LeanSelectable_get_OnSelectSet_m66318685B6C39B57FF615B1879216522F25AE2D5,
	LeanSelectable_get_OnSelectUp_mB451D6480ED943465EB788E07E985BA35AAA8259,
	LeanSelectable_get_OnDeselect_mFAB0917F0994C3A335D74E3F6760587FA69B8A6D,
	LeanSelectable_set_IsSelected_m8C8295E09F9B21DEAEFD7739C3FDECF88A40ED72,
	LeanSelectable_get_IsSelected_mDEF1B82025D83466A05961F130A3A36EB5802726,
	LeanSelectable_get_IsSelectedRaw_m441DF6B745F624030D329D2E8D56318180F68E18,
	LeanSelectable_get_IsSelectedCount_mB11ACF0AF4E472A93E02581E7AAB7D7A1A0FEBC7,
	LeanSelectable_get_SelectingFinger_mBD1F902C82BC2B3D312CE73C849E7DE8C8ED2613,
	LeanSelectable_get_SelectingFingers_mC36E160B113626D8802083A857E79CE04B9D63D3,
	LeanSelectable_GetFingers_m3A9586F30D597992BE4EF3C2BD9D78F8B9CF633C,
	LeanSelectable_GetSelected_m81DB52B271ED247B6A3C00E95FE7F8B47A506F1C,
	LeanSelectable_Cull_m8B54A37D2717D66848DAB29814A8D960DB6E7677,
	LeanSelectable_FindSelectable_mC46E61F336454BB0BB2D7C8698DF40BB0FC82E1B,
	LeanSelectable_ReplaceSelection_m240C6623A87BA70B2A982DF00AAB530E3759DEB8,
	LeanSelectable_IsSelectedBy_m3E4CBB44125216660CB26B15B7CA518DF61921FC,
	LeanSelectable_GetIsSelected_m91507700BC4B2FD83481C16AF04CACFECA6C6227,
	LeanSelectable_Select_m9F7FCC48D696C2D30652C090FD5A3CEB7CA22036,
	LeanSelectable_Select_m4340163D89E713EA2D8D06898B8812A9FD22DCFC,
	LeanSelectable_Deselect_m3677B356106B62ED823482A38799891047D7B852,
	LeanSelectable_DeselectAll_m042578CDC470FAB9B61A083C93B83ADD0EC29296,
	LeanSelectable_OnEnable_mE154B0985D3D571F12C5486D4692674B42FCE20E,
	LeanSelectable_OnDisable_mF9F40E5E223C7E9E5FDA23938873AE866D0E4F27,
	LeanSelectable_BuildTempSelectables_m2E970414174E22581745C774558FD70597F90597,
	LeanSelectable_HandleFingerSet_m20AD509AD58239BBC215D83F7A54605B8DEFB507,
	LeanSelectable_get_AnyFingersSet_m5DC6D0DB741E15B4FEBF49D5213BF5CD96868C95,
	LeanSelectable_HandleFingerUp_m54F25D8E849D9287CC2FC86FC5B1FBC50E2510C5,
	LeanSelectable_HandleFingerInactive_m92C9491555F01B4848282E89029B017C019F4BD2,
	LeanSelectable__ctor_m13F42644E9E31229FF8724DE75F55C9F565BD5A1,
	LeanSelectable__cctor_m97EA10BF63B66BEA764968433ADB94CAA0F65E61,
	LeanSelectableBehaviour_get_Selectable_mE3F3D1AA53A1853265FF9BE70803BC783655B273,
	LeanSelectableBehaviour_Register_mC02BDC99405A8CB40DFCDAD18228F83980739DEE,
	LeanSelectableBehaviour_Register_mB2458CAA97B4527993E5513FABF788AF21BE17D1,
	LeanSelectableBehaviour_Unregister_mC71A010F690A9FF141D135069718614626DE8196,
	LeanSelectableBehaviour_OnEnable_mCEF867C219F5A6CB0F720180C3DA71080419DA3C,
	LeanSelectableBehaviour_Start_mDC2AFA56C8E42B8C5566125A03738CBCA82772B2,
	LeanSelectableBehaviour_OnDisable_m4BBD195762CFD88CE79BF65D50673C2BCFB4CB39,
	LeanSelectableBehaviour_OnSelect_m17BBA732DE70A5C42396924679BD72D43E4617CE,
	LeanSelectableBehaviour_OnSelectUp_m71878502F461D29249752DEF6D1F6D6D70765D58,
	LeanSelectableBehaviour_OnDeselect_mB2985F4134D98578811200C44E661724F727DCB4,
	LeanSelectableBehaviour__ctor_mBB899FC0E6DD2C2EE6BA4CAA5BDDBFBDEE8918AC,
	LeanSelectableGraphicColor_Awake_m23F3DC9CD83ADCE9F86A00D12A68FFCBD2BB94C8,
	LeanSelectableGraphicColor_OnSelect_m3A8842040AA55D21A568EA84676591CAA7CA9CB4,
	LeanSelectableGraphicColor_OnDeselect_mBF06233F6E10E195B57DB2D49322D873FBFD2F55,
	LeanSelectableGraphicColor_ChangeColor_mBEF737F5C679019A6B1DD100EFC5B116B7624109,
	LeanSelectableGraphicColor__ctor_m3F031F153741F4D25782F6F36CE79B303D1CD11F,
	LeanSelectableRendererColor_Awake_m94FA76D34113A46C79FADF09918CF0D83995110B,
	LeanSelectableRendererColor_OnSelect_m29209B299AA70241A31316E806C17FEAF280DEC0,
	LeanSelectableRendererColor_OnDeselect_m55A1C66DBAC602198A7B9C99AF80F1C1CF3E0799,
	LeanSelectableRendererColor_ChangeColor_m58D07D6FCE72EC3492C159E61C4B345FBD028A11,
	LeanSelectableRendererColor__ctor_m185BCB6867F50842A649D7FCF9F2AF7743086D63,
	LeanSelectableSpriteRendererColor_Awake_mAF8C52DB81CB7B0360002580AB989EF4123D8BDD,
	LeanSelectableSpriteRendererColor_OnSelect_m23254EE2154E46F702EDF435A8090DB056574381,
	LeanSelectableSpriteRendererColor_OnDeselect_m8DCE24E9AEBA972AFCB8B9E952F76D25436B51D7,
	LeanSelectableSpriteRendererColor_ChangeColor_mE925B6B41741B2F97B883BE78C24AF530E39828F,
	LeanSelectableSpriteRendererColor__ctor_m8EF60DF6EC30930898BD50BD67E6963764BCD837,
	LeanSpawn_Spawn_m1D90636F1A728A1F4E92382E639318CDDC805CAA,
	LeanSpawn_Spawn_mB3854C1825D7112B2A2A948E7FB135597B5BAFBF,
	LeanSpawn__ctor_m8D3B816F48B9BD13A413F68D47EF6512E901681B,
	LeanSwipeBase_get_OnFinger_mB2261EDA0DFF2366E78396181161B8DA3E5A4F8D,
	LeanSwipeBase_get_OnDelta_m6BC14BAFA279A35B344533AA411593EFAECAB1B3,
	LeanSwipeBase_get_OnDistance_m58B841675172FE16DF8E7991918C5757582B46C8,
	LeanSwipeBase_get_OnWorldFrom_m665B1AB74B086A9C379E404FABC3DDF6BFCBED7C,
	LeanSwipeBase_get_OnWorldTo_m6572FD6F5DD41BDDB66C5BCD47DD80DC98A17B06,
	LeanSwipeBase_get_OnWorldDelta_m3A2B45DA76FAF73FF854039BDA0D225E406F7052,
	LeanSwipeBase_get_OnWorldFromTo_m39B0AEA11081A20324C0B5DE52EE0B465074B3F0,
	LeanSwipeBase_AngleIsValid_m14CDA8EBD9CC7DC05D381E4E3869E0DC3BC444EF,
	LeanSwipeBase_HandleFingerSwipe_m84E5C39C725B9CF3A6306FE272AC0EFE1B16DE8B,
	LeanSwipeBase__ctor_m45648B5A0A12AA09D03C6F01D86249FAD20C4318,
	LeanTouchEvents_OnEnable_m54C1901998D364914C8632D89056FBC5D78FA799,
	LeanTouchEvents_OnDisable_mA49DB8DFD064C960863EF880A1F453031CD3C405,
	LeanTouchEvents_HandleFingerDown_m16739E77EFA4AD4F0797DA3F5B544A183928B1B3,
	LeanTouchEvents_HandleFingerSet_m3609A3100342B1F648ACE44E712A78E68D538B4F,
	LeanTouchEvents_HandleFingerUp_m5FFAEA1AEC048EF1098EDE08638CB0656CD1D29D,
	LeanTouchEvents_HandleFingerTap_m94B823177D3CF5922849A5F12334F936D9C5E8AA,
	LeanTouchEvents_HandleFingerSwipe_mACF109B0E72CBDF522BFEC41947A4B099C557BE2,
	LeanTouchEvents_HandleGesture_m4841ED6F8E8E48691288BCCAC4B8D295D7540645,
	LeanTouchEvents__ctor_mA2741A93D716B7267AD8183E017231503E5A6B94,
	LeanTwistRotate_AddFinger_mF6BBAC92B955DDFBD57D52028C597DF8A110551D,
	LeanTwistRotate_RemoveFinger_m22ECD8971AFE60BC39EF1D091200BF017235F636,
	LeanTwistRotate_RemoveAllFingers_m1D2F33150CA0C9B5D1FE37F04F20795711C4001C,
	LeanTwistRotate_Awake_mE47A226208B4625A888723D3ADBD7395D8B786A5,
	LeanTwistRotate_Update_mB9EDA12F33015EE7F7E9248DFB847AF376C5DF11,
	LeanTwistRotate_TranslateUI_m4D4F4EF26B834092795D83ED4A2F947BC13CF9AB,
	LeanTwistRotate_Translate_m8F287535A70F388E8406AD8EAA3F5A9C0E15CE7D,
	LeanTwistRotate_RotateUI_m25AE38201B70C7AF3D4DC68CE03412F4E9725445,
	LeanTwistRotate_Rotate_m327DBB47495304D8A63D592DC3CCB0909B28627E,
	LeanTwistRotate__ctor_mAA680EC4B6951766F6272DA806353C8D3F0767A6,
	LeanTwistRotateAxis_AddFinger_mF4EB435EB73467CD43192371E576E1365F490496,
	LeanTwistRotateAxis_RemoveFinger_m9CA82D0AC717663DC3BE8A06B32A0EFFE79398C1,
	LeanTwistRotateAxis_RemoveAllFingers_mB033D05CFEDCD6A6B5EA5F88BFB3DD510D8ECF68,
	LeanTwistRotateAxis_Awake_m28ACAE8335CB02BBF8A37EBF5060A8F11D584BFC,
	LeanTwistRotateAxis_Update_m94F74F6966232AF05608B5C266528888DD7D63B4,
	LeanTwistRotateAxis__ctor_m7EE35206EB9182FAF6B23095FF3A286EB75C08BF,
	LeanFinger_get_IsActive_mCC63B4B3C8AB34F997395FE9BFB35C87C33179D9,
	LeanFinger_get_SnapshotDuration_mC6B4AFE80D4F115DF135819D7CC5FF78087B936A,
	LeanFinger_get_IsOverGui_m27CD8EC84C3A3E1BA8AD360505874C56EAFF737D,
	LeanFinger_get_Down_mC794AB80DB8FCA8AF2970A59E82589D296731DDF,
	LeanFinger_get_Up_m286EA975B43946B0205BD8CAB38DDC1831D813B1,
	LeanFinger_get_LastSnapshotScreenDelta_m45068F5F8F2000676D34B4ABEDBAE4BD53C5B6C3,
	LeanFinger_get_LastSnapshotScaledDelta_m527F35C7674FE8F6EA865FB4D6E000C7D412F4FC,
	LeanFinger_get_ScreenDelta_m7B809F527C34F3253466B75E608530A7004BDDA2,
	LeanFinger_get_ScaledDelta_m4CB396F460ED375D6094E3FEAA1061C4563A5E38,
	LeanFinger_get_SwipeScreenDelta_m77B23F2EB08E7EBD0AE23780D54D0549F1DE72A5,
	LeanFinger_get_SwipeScaledDelta_mFB187D350D7C08E14F2F2902AC9BBE6C9CD36F01,
	LeanFinger_GetRay_m76DFF8B71221306A2D675FF44E632A382350D9F3,
	LeanFinger_GetStartRay_m7761B01CF82BAF9E420A25E6393C1A140B911ADB,
	LeanFinger_GetSnapshotScreenDelta_m02D2BF648C39E3D620974F16B5321B5F9F685A6A,
	LeanFinger_GetSnapshotScaledDelta_m02E31797CEA5BBDF6F045CCB8BFF1D6F434CBD91,
	LeanFinger_GetSnapshotScreenPosition_m8523ADFF6B507F9E9952B281656AD2DC763B6DC6,
	LeanFinger_GetSnapshotWorldPosition_m12DF66F851E073BB9935114B2422C8AE271DB810,
	LeanFinger_GetRadians_m6F76F20D2800780366E90E3609E61AE8EFB58AB8,
	LeanFinger_GetDegrees_mF2D51A8856BE2AEB83387FC5C76FEC6B90125A73,
	LeanFinger_GetLastRadians_m1EFE92DD7E39F4E0702779B474228F00B95C4204,
	LeanFinger_GetLastDegrees_mECD743918766F89789B06ABDD6A87B0D54ACA8DB,
	LeanFinger_GetDeltaRadians_m3A2D02D2CB3D24F76CC76F165A78F2C3E1D89405,
	LeanFinger_GetDeltaRadians_m6B010F29593EA5BE2BFC52683102F9C4EC157523,
	LeanFinger_GetDeltaDegrees_mD928FC314F607A62BE3137EC54087EC058C410D6,
	LeanFinger_GetDeltaDegrees_mC07667C868496C9D1BDA196CCD65C8F49732C737,
	LeanFinger_GetScreenDistance_m0E57D4448AEDB91EA57FA27A4C9F26ECE3C3229E,
	LeanFinger_GetScaledDistance_m795170666A96D1C30E8F2DEFD74F7A2EF661D1A8,
	LeanFinger_GetLastScreenDistance_mBD91726C445FA114BBCD70BB63E09C83DA06D94A,
	LeanFinger_GetLastScaledDistance_m2DED4DAEB091F349DC8FD047BA899BD0A9E44430,
	LeanFinger_GetStartScreenDistance_m79103E21F4D42DEE59978A050DC9CA0BF9D986BF,
	LeanFinger_GetStartScaledDistance_mA506EF7A073D1D8A71D17C3973E860F097CEDC8C,
	LeanFinger_GetStartWorldPosition_m7BB0617A8E70A760F500681FE4522AA0DFE65DD4,
	LeanFinger_GetLastWorldPosition_m30E17D240A999024C6BFF4446DF1AD206FD0E15C,
	LeanFinger_GetWorldPosition_m0BC212B62E8E9DC10A33D66A6A9F7B33E11FE1A5,
	LeanFinger_GetWorldDelta_mCCAA3761ACAD3BA8FA37C7BBCB99D55BC1C5FC21,
	LeanFinger_GetWorldDelta_m4171A03A6C6C0662249E99BB52EA157EEC924A92,
	LeanFinger_ClearSnapshots_mE9E8949942022D73BA03E849D32B1325A8374FA3,
	LeanFinger_RecordSnapshot_mAB5607ADD4E332558A62C28E383ABBAA88578E74,
	LeanFinger__ctor_mF44EE089C3F3584D15469D80CF7D54B07CF3FF63,
	LeanGesture_GetScreenCenter_m37C3A3F7C2FEBBB4C675704400F79D7723FA942D,
	LeanGesture_GetScreenCenter_m153528341362034489E7D0C19730D2F68F291ED8,
	LeanGesture_TryGetScreenCenter_m437D51BD4F6DB63A1E7807D7E6C3549B039DE224,
	LeanGesture_GetLastScreenCenter_m1C165CEDC1A51F3B17228A95A3F4D06F140300B3,
	LeanGesture_GetLastScreenCenter_mA1CBC57DF82A9A527DF240199E7D957C5C8C6A4C,
	LeanGesture_TryGetLastScreenCenter_mD5AA2C7AC03E32BF2174AA07E492B3519E336E47,
	LeanGesture_GetStartScreenCenter_mF577CB8C669E33BB563A330B26CA44081FEFACE7,
	LeanGesture_GetStartScreenCenter_mF89A029DECE5044A3DFD9497BABDB6FDBCAC59E7,
	LeanGesture_TryGetStartScreenCenter_mA826DC2BC7A0E2285D45E7BB83E692955E60A3E0,
	LeanGesture_GetScreenDelta_mD24464147EE9500A9AA3EA417BB2AEC59F881183,
	LeanGesture_GetScreenDelta_m1EA13DA5DDEF07CD86D22435377FEB4A30B815CF,
	LeanGesture_TryGetScreenDelta_m511B7A35C6165FBA66EB620AB361800A2F73E11A,
	LeanGesture_GetScaledDelta_m5A28890F651193E1F17EAB9DE995CC4B70EE8652,
	LeanGesture_GetScaledDelta_mA5BD346430C4F795E24335B581E647B5E64403AD,
	LeanGesture_TryGetScaledDelta_m2006FC6D64C9E061F795887FAEB1B91C562324E6,
	LeanGesture_GetWorldDelta_m28A9A163A4789F9EAB8B78D5189DFDD21AC88C2D,
	LeanGesture_GetWorldDelta_mE063AC165BF76F5ADF7A2F0EC249FA4FF45FEC36,
	LeanGesture_TryGetWorldDelta_m4096DABCD0C5925A5BD5DBA490B65951FBD93E14,
	LeanGesture_GetScreenDistance_mAEDE196D1C9CABEA07A41B61A0C9C16B19626F53,
	LeanGesture_GetScreenDistance_mF6B566EEAF731C23B2705D667A9617A2D603F5EF,
	LeanGesture_GetScreenDistance_m8E87EA8A0231EA66E5BF8B9179BA2F5B89EB36D7,
	LeanGesture_TryGetScreenDistance_m4C999C2911C40C13BDAC0D12F10AAF43B9115559,
	LeanGesture_GetScaledDistance_mECA5C99A60B0779CE4A07042A8F214347BD6F932,
	LeanGesture_GetScaledDistance_mE9AF8539F34402EA93E6529E87A50BF7FFDEC588,
	LeanGesture_GetScaledDistance_m520C012CCB1D46FBD3C95EA9C052DD5FE91B82E7,
	LeanGesture_TryGetScaledDistance_m73C2D79A5B327D4A97CC6011C2030B913F431F32,
	LeanGesture_GetLastScreenDistance_m465207ED0648D6D73F0CAEDE89E07CF980139883,
	LeanGesture_GetLastScreenDistance_mA691336027AE9E47FED41F48F7762F2B4012645F,
	LeanGesture_GetLastScreenDistance_m9DD3D45DDDE052DF119EF19AC65EA0DFA46E6065,
	LeanGesture_TryGetLastScreenDistance_m6B1E58F127C84E13CA45E0A4842E802BCC4FFF6A,
	LeanGesture_GetLastScaledDistance_mB5E2903766A908E2263535C91E06C89C111506A0,
	LeanGesture_GetLastScaledDistance_m91A2D0545A8A0D217214929370D74D6FD1864A26,
	LeanGesture_GetLastScaledDistance_m464C51B93C8CFE37567E1A0B5E33B4CCEFC97E7F,
	LeanGesture_TryGetLastScaledDistance_m3BDAF07C0615964CDAFA0D02C57EA38606C75708,
	LeanGesture_GetStartScreenDistance_mBCA22635F54BA6AB964BBF6863CC6E74B9BB5B1F,
	LeanGesture_GetStartScreenDistance_m3FA56525E6F87D77F30E24A7695D7C126494B05B,
	LeanGesture_GetStartScreenDistance_m9FF43E1D2CE46ABFFBC25FC22061998E1E646933,
	LeanGesture_TryGetStartScreenDistance_m6C06303CF1F726B513640CC61B60B6E366203701,
	LeanGesture_GetStartScaledDistance_mAAE3DDC2AD57847511D5AD70D5489AE9CF8262E7,
	LeanGesture_GetStartScaledDistance_mC3525166EE87AE13193F58765C977B56EFAA88DA,
	LeanGesture_GetStartScaledDistance_m82E08A5C0AABA7CDEEB7DF84625BD6E71CF4B4C6,
	LeanGesture_TryGetStartScaledDistance_m798DACD98A5D02640F90E1E951F9590093682EC1,
	LeanGesture_GetPinchScale_m1B2344BE840691081265B194743A7A4B13A79470,
	LeanGesture_GetPinchScale_m8EAA29932D6C9B1D234FD21A0BC65DFC982A727E,
	LeanGesture_TryGetPinchScale_m245473873F06E23DF3C262FB11057C2851B6A64B,
	LeanGesture_GetPinchRatio_mA637FED4A16EF0D3C84157682CD5DD537513C168,
	LeanGesture_GetPinchRatio_mD3916FABA5C5FEE6CD3040B35893303F3750DB74,
	LeanGesture_TryGetPinchRatio_m9950EDFF3165BC736BC0BF0940DF5FF5170C2F49,
	LeanGesture_GetTwistDegrees_m93431159FE6747CEA576CC75ED2FF307D3292338,
	LeanGesture_GetTwistDegrees_m58C1B6B48D1225D9CC0424F2506AAF434102591E,
	LeanGesture_GetTwistDegrees_m76EB2AE9169C405BA682A82EE7D9531294B904DB,
	LeanGesture_TryGetTwistDegrees_m2E7CE5271F73735D8963CEDBA39101F3EC762EE8,
	LeanGesture_GetTwistRadians_mAA84476BF790E55FBE8B72C141C84DD049A75C42,
	LeanGesture_GetTwistRadians_m16BBC7F9365F906E217790108F2EC2C98A49710A,
	LeanGesture_GetTwistRadians_m517E7492F60E53097CED52BB7A87FB2E4B4BF50E,
	LeanGesture_TryGetTwistRadians_mB4362950E2DCEA79D05B6FDF8D732FD3491CE017,
	LeanSnapshot_GetWorldPosition_m618A37581C9A14C75E093D0C662E2B2EEA1768EC,
	LeanSnapshot_Pop_mBDB6D46A91F554985BCC3071B2DDF147AB587869,
	LeanSnapshot_TryGetScreenPosition_m96909D25960B97E90608BE16FD63C79AD05DB9FC,
	LeanSnapshot_TryGetSnapshot_mA162D816D9440F32480FAB534F86907175ED5283,
	LeanSnapshot_GetLowerIndex_mC9B320B53AD22A382AA6D85BA41A7098FBB0FBAE,
	LeanSnapshot__ctor_m29A2A5F0922BA4931A84B119D7B8FD80DE1F3559,
	LeanSnapshot__cctor_mE5A06C835C35CD1A09D76B3C5F45944552A829A8,
	LeanTouch_add_OnFingerDown_mAD6D901E481055FA098326D04678C46C009DA7C6,
	LeanTouch_remove_OnFingerDown_mAC7DDAD27F149E64073412B5E3C1D48FC5CCA2D0,
	LeanTouch_add_OnFingerSet_mF6567EDA4FFAB42115785ACE68B4C4699AE7AEBE,
	LeanTouch_remove_OnFingerSet_mD986A740023E6AE3C6C1093B0BF54D359693408F,
	LeanTouch_add_OnFingerUp_m672656853F5BDBB4DFF1BEF7FEE4FF28FA25C1DA,
	LeanTouch_remove_OnFingerUp_m9E4C292CF2D0D60C93F9DA38E858D36E3A727E68,
	LeanTouch_add_OnFingerOld_m3FDD59C1AF070FA09C9D2AB6F23AE2DC8135E04E,
	LeanTouch_remove_OnFingerOld_mCE1697C5B4D4D348709FE10AAFE540371D6A3699,
	LeanTouch_add_OnFingerTap_mC0DA8BBFEE3ED8DA36A5EE467CEF7D3FFC9A1455,
	LeanTouch_remove_OnFingerTap_m359B1CF629C74D9B17D344BEC42B0AD74E8DCDAF,
	LeanTouch_add_OnFingerSwipe_mCF01BDB87D48507F0975A0034494F1B8511752A1,
	LeanTouch_remove_OnFingerSwipe_m0ECFB745A17FA08957D28ED1F782BD47EAD6846E,
	LeanTouch_add_OnGesture_mEA1914326B6319D148429260746A8506FB2A1173,
	LeanTouch_remove_OnGesture_m5E29A5A97FDAB2A9E939744C038A9639CCBE7817,
	LeanTouch_add_OnFingerExpired_m0FAEC5E8C7B4E5BC50CB9877231745B1F9EC26A9,
	LeanTouch_remove_OnFingerExpired_mEB1A8E8CD136F0A67B887E88B383CBDFF05971F3,
	LeanTouch_add_OnFingerInactive_m28DDE042BB5BB86DD79396F03BC43BE05907AB07,
	LeanTouch_remove_OnFingerInactive_m8369AEE01AC33148A6F7D4A371BF550ECA01D35F,
	LeanTouch_get_CurrentTapThreshold_m82B0F9B2B8A89688FCD8F7D4B699AB8C7AEC37F2,
	LeanTouch_get_CurrentSwipeThreshold_m38C03E29A00B5C6C2318C9A7B680851CBF6BF7F2,
	LeanTouch_get_CurrentReferenceDpi_m59908D8B95671F22C89C3683A8710F4C8673B904,
	LeanTouch_get_CurrentGuiLayers_m4435F6ADCA6EEA42B65576559ED0E5240E768707,
	LeanTouch_get_Instance_m7F8B0C399AC8AC56CC2B2138C039B1EAAA3D6911,
	LeanTouch_get_ScalingFactor_m011FA5C02F85074984E3BAE6015DEDC0AE3E5196,
	LeanTouch_get_ScreenFactor_m9C0200769EA75A55B3624F3240ED767A06D46D0A,
	LeanTouch_get_GuiInUse_m16F43865EFB038A7D609587CCC3FA3C2204D6BE9,
	LeanTouch_GetCamera_m68DD8BF68EDE0611083479E2BD288F265B9D5B44,
	LeanTouch_GetDampenFactor_m9DC6418C421D9216565FBEBA291C4A50BABF4E04,
	LeanTouch_PointOverGui_mF8EF2B64A1B7219CEAACB4973D158FCE88131F20,
	LeanTouch_RaycastGui_m7C8D73F6390157C64564017A61B529D29C5AB29F,
	LeanTouch_RaycastGui_mA0D00DB14CE24FA18B0346076DA42CF63757C818,
	LeanTouch_GetFingers_m44739A10124E36D6012FFB052AEDF43F73973F20,
	LeanTouch_Awake_mD2AB3BB732D7BA081C1CFB82864093360BE4C14C,
	LeanTouch_OnEnable_m543F1B15F56E6FF84C01343B0F7A1BA2EE0762B7,
	LeanTouch_OnDisable_mA2CC5E5C9B13E989F42E2329195B37B194FDCAD7,
	LeanTouch_Update_mF9D289FB40E357027982AC6CFF89BA269FD30077,
	LeanTouch_OnGUI_m7E8A9F3037014413BBC08589D5312DE4F958E0E2,
	LeanTouch_BeginFingers_m2B2803272A0D2B6ADC5411F1ADB9745CF6149FFE,
	LeanTouch_EndFingers_mBD43F9D096CA974B8B8429D8A98FBF68B82899C7,
	LeanTouch_PollFingers_m498C6D067AF5DDF00AB71DAFF27933D7F10BD22B,
	LeanTouch_UpdateEvents_mADAEB1538B3CBFBA9FE3F3B1FDB1A8893FB743CB,
	LeanTouch_AddFinger_m54E6B12911C218B2E513A094917D610FBE27E479,
	LeanTouch_FindFinger_m451C4D2CCB2E62DE5A7FA94CEA717ABCEFDA7AED,
	LeanTouch_FindInactiveFingerIndex_mAAE1FFFEE7088F1C5AD4EA96E72DF5980F60880B,
	LeanTouch__ctor_m35821163E87FD2C3C7FB29DB1C04D22C1A806303,
	LeanTouch__cctor_m9DBAD251AB1D38BD882F06F6426833F2C2B408EE,
	NULL,
	LeanHelper_DampenFactor_m09FEB3B752E8FB2F5988D64ECC5FA7DD746F7ADD,
	NULL,
	LeanCircuit_UpdateMesh_m62FDE5563B031B795D3D2EE0800F749BE27F3A75,
	LeanCircuit_Populate_m52201C64A088CC4DE542171E391CB15AF604ABB3,
	LeanCircuit_Start_m050782C1BFDC6C1D238CAFA1BB6CDBA56DF428E0,
	LeanCircuit_AddLine_m192FD9DC2177AC9236F77537B11C24ADF735C907,
	LeanCircuit_AddPoint_m7E414AC5ED3070CB00C3433ADAC9551B968A8119,
	LeanCircuit_AddNode_mA484BEB844AF60C61136273B65F876AB6631F623,
	LeanCircuit__ctor_m4ACCE6F63E778CDB3028C220CEE9079B278E76D4,
	LeanCircuit__cctor_mD624E36DF349167D1FCDCC9EAA35F2C72D78EEAE,
	LeanDocumentation_get_Icon_mDE468E078B1C592A9C1B31ACF5ED0F21878ABD6F,
	LeanDocumentation__ctor_m0F59D864C88F976BFA06C390A2C49241C1E19D67,
	LeanLinkTo_set_Link_mF58AA9286EE6DA805FBBAC5FAE552FE669141550,
	LeanLinkTo_get_Link_m1EEB4340AA0D6AA34AC5DCC668D6C6D2C57164B0,
	LeanLinkTo_Update_mCC1F424F06C5E5E58545ECB0DC6A6B4EC74C6A54,
	LeanLinkTo_OnPointerClick_m4CCD9E622354AF37AD8C7F6916499BDA3ACD76DD,
	LeanLinkTo_GetCurrentLevel_m0811F7E70375466B3ADB77570296F49C9490D473,
	LeanLinkTo_GetLevelCount_m7C23062B191B24EA1F4864522B70E6D6B6D7FA38,
	LeanLinkTo_LoadLevel_m2F12A3EF4B2BEEA666536C7E639EC751A1EC83FE,
	LeanLinkTo__ctor_mE6E34A9637F063C9A3264525620F00C8BAE07DA9,
	U3CLoadBundleU3Ed__9__ctor_m9DB1CEF2A9592E64FF4C54FE27159A4BE3DCBDBF,
	U3CLoadBundleU3Ed__9_System_IDisposable_Dispose_mFAEC968ADCEE1CD3B977ACADE10F75D19FEC0F48,
	U3CLoadBundleU3Ed__9_MoveNext_m57EFE19E1D39BD0C5E01E97DE965F3DA21F3A08E,
	U3CLoadBundleU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m97DD39AC7EBD803B86EE1312D504A199C44293C2,
	U3CLoadBundleU3Ed__9_System_Collections_IEnumerator_Reset_mEF7E2248587DE9ABC40B6A3763F8F27D98820401,
	U3CLoadBundleU3Ed__9_System_Collections_IEnumerator_get_Current_m27732D1CBB06625A3129B8708DC3D4E5AA2CD675,
	U3CShotAnimEndU3Ed__26__ctor_m34DE0EC666E7BFD629E29BCDBC6AFFD877BCEFF2,
	U3CShotAnimEndU3Ed__26_System_IDisposable_Dispose_m3649F821B248433A1B4CDDAD1672AA00C8614009,
	U3CShotAnimEndU3Ed__26_MoveNext_m2BB211C625B7F14A6858EF311A10386DFE003369,
	U3CShotAnimEndU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m984758DBDCA4108A2E0633F98DD1AA0CB2A8E689,
	U3CShotAnimEndU3Ed__26_System_Collections_IEnumerator_Reset_m07CD4B224EF25AD5109D27035CA1B74B4123E667,
	U3CShotAnimEndU3Ed__26_System_Collections_IEnumerator_get_Current_mC5D2D6382194180037E4BF43B46E1297C572F888,
	U3CShotAnimEndARU3Ed__31__ctor_m1354EF68EB72B065CCAE6A5C0E255B9730ED45DD,
	U3CShotAnimEndARU3Ed__31_System_IDisposable_Dispose_m008CBA2153CEC486F22E5DDD267BF39A2C5D6292,
	U3CShotAnimEndARU3Ed__31_MoveNext_m9F17AC16EE745FF8839F831F80589AD6C989AFA8,
	U3CShotAnimEndARU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD720681B127C8629994884A7952A5B1A88ACA2BF,
	U3CShotAnimEndARU3Ed__31_System_Collections_IEnumerator_Reset_mDBF1B2E95FBF23F2AEBAB29E56489352ADC2D49E,
	U3CShotAnimEndARU3Ed__31_System_Collections_IEnumerator_get_Current_mF0E0084AD74EF00CECE88CD2018C90E794CC48C0,
	U3CTransShotAnimEndU3Ed__34__ctor_m4C703244FEC69809C9BDD2AD962E0DEFC5834316,
	U3CTransShotAnimEndU3Ed__34_System_IDisposable_Dispose_mA5BC48104B79A3A96037965E6871C1C93FC8A703,
	U3CTransShotAnimEndU3Ed__34_MoveNext_mE0CE599C585D16E3EA668D4BE3975C930B1E0203,
	U3CTransShotAnimEndU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B9361CB0004A39DABF715E7DA4A130F09E9C384,
	U3CTransShotAnimEndU3Ed__34_System_Collections_IEnumerator_Reset_m0E3D406283FABC99E831D77754E847675E4A9089,
	U3CTransShotAnimEndU3Ed__34_System_Collections_IEnumerator_get_Current_m93BA4A85AB5C9DABF5E5EC5FC69B9C6EF7F95C38,
	U3CTakeScreenshotAndSaveU3Ed__12__ctor_mE4ECA813184992818F861079C0D0AF3DB077616D,
	U3CTakeScreenshotAndSaveU3Ed__12_System_IDisposable_Dispose_m0B01AFD3FFD412794F788643981EF8761A544056,
	U3CTakeScreenshotAndSaveU3Ed__12_MoveNext_mCDF28BC374BEF35FA986F261A6A918F61045D239,
	U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m621132C7554A10AF0038A508745FF0FBE6B8693C,
	U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_IEnumerator_Reset_mCD6C81342223466277E49C045FF45BBCBEA58DC5,
	U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_IEnumerator_get_Current_m12B05A4869F5B19BADC1E46BDCB4141BC5F4153B,
	NULL,
	NULL,
	NULL,
	NULL,
	EventHandler__ctor_mF2D7ECEC16E656E310C9A9E0821F92B1A8F946A6,
	EventHandler_Invoke_m27849E53870CF49BA84DB9A69601E0668947B8F0,
	EventHandler_BeginInvoke_mD65889040C063DA6F036B6A6FBAD36301FEF7503,
	EventHandler_EndInvoke_mBA37A2B17D3F3D5E3C09E0B9E01F158A72FD7052,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Cluster_Reset_mFCCBECAD7D2197EDFF6783AC5A7A1CDDF13468AD,
	Cluster__ctor_m3A08C653BD75B2272E095501DBB6791B8CC04464,
	U3CU3Ec__DisplayClass18_0__ctor_m459996655C106102249CEDE73643E0BAD6230EEF,
	U3CU3Ec__DisplayClass18_0_U3CFindClusterByIdU3Eb__0_m8F69AAC937320B83F312479DEF0319CEBFAF02B7,
	InputProviderEvent__ctor_m0AEE8426E33532B1E4FF3A51EBEA97D6EE417162,
	Finger_get_Index_mC781179F5228495E4E712FA231DB622D23D047AD,
	Finger_get_IsDown_mFB1A682A228C2C0CBAC6F14E951CBA4571B659FB,
	Finger_get_Phase_mC0FEA52D1E3616F92DA0FBEF81291D4D55D9994E,
	Finger_get_PreviousPhase_m74BE8F480FE2058BA1794E0DB135F37C912CE1C9,
	Finger_get_WasDown_mFB9EE9910243344428612C50E351C3C2D7846FEA,
	Finger_get_IsMoving_m46C93B1C53AF8D57476A860943A995D7A557138D,
	Finger_get_WasMoving_mA008997EC29AFA406CAA206E952C47399AF1FC14,
	Finger_get_IsStationary_m71F9B80579B17B709078B7927F542E08C2678F3C,
	Finger_get_WasStationary_m0462329554477BDA6B1B89C8CD1E50EFC90F3FD0,
	Finger_get_Moved_m096260CD4116E0485FF62AF19683ED4DA7048907,
	Finger_get_StarTime_m2A81BD271B597F071FB0E1ED33157902F3F99FA9,
	Finger_get_StartPosition_mAAE9424F2ACFD924E293DA00610E2BCE0B39BED4,
	Finger_get_Position_m55D65B4EC87B9BD75EF265375162A1A9AFA32DE0,
	Finger_get_PreviousPosition_mB33AEC9F577929EF86FF44BD573E9F9C9CCAF5FC,
	Finger_get_DeltaPosition_mBD956CBBB334AC1E12613BBBAB08FE48ADE639CF,
	Finger_get_DistanceFromStart_m219915EDFDBB0B7FA9530AE10781B3673E9C3257,
	Finger_get_IsFiltered_m007743AAD85699258F1BC0789B5AD426D56C1DF4,
	Finger_get_TimeStationary_m5F29DAF06D9ECE0CED2964DC0A24A3CDB7701666,
	Finger_get_GestureRecognizers_mA412521C0BCBF5E051D6EAF80584E1EFA834BF44,
	Finger_get_ExtendedProperties_m88FFA7CA5922695316CAC2BA5975C97DBCD4434A,
	Finger__ctor_m83F0B7426542D8D8A72388CDD66661EDF67316B0,
	Finger_ToString_m49995A733400A5B3E81DD73D481F3EA0729BF220,
	Finger_Update_mE198A0632C13C9B6B7F9456E81FDAE86F388B513,
	GlobalTouchFilterDelegate__ctor_m2FDE30D11BA6F771234E509188208D4534D8C201,
	GlobalTouchFilterDelegate_Invoke_mA2ED1AC9B86FC470E46A1FFCB41F08BC06F9FDEF,
	GlobalTouchFilterDelegate_BeginInvoke_m0B7D97C482F9FE60C8526945B55870CA3E1E83BE,
	GlobalTouchFilterDelegate_EndInvoke_m3C2524637D802E1B6E50602C9C600C56B9088C32,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FingerList__ctor_m7AED2F6A8076D19569082AC5D93A4268488E16D2,
	FingerList__ctor_m8C266E08A11041F458FF34C0D48ACDBD45AB9A33,
	FingerList_get_Item_m94E3A9725D8DBEDF74C26868116E75B6F7ED5FC9,
	FingerList_get_Count_m1E6324860C7603A0A163BE4F3D7D8F1C6B721B42,
	FingerList_GetEnumerator_m3CD8CA11CAC5A89D39588C45AD0D0C5009E20AB1,
	FingerList_System_Collections_IEnumerable_GetEnumerator_m2EEDDAA7D86E2E7C63D00348E874643D81987FFF,
	FingerList_Add_m4E9E2A571B3250F506D11D6FD519133652B8FFE1,
	FingerList_Remove_mD7BF90AD64A9D853AED4090AF8140D2512973F75,
	FingerList_Contains_m27F037C7685DE35F112BCE094067036C747DC6D8,
	FingerList_AddRange_mC06B617096EAF3BC98EAF4ABB0BDD4D56E9E7C47,
	FingerList_Clear_m7778B1AE6DBACE13E257E94261DCB23FB497E5E5,
	FingerList_AverageVector_m83D03D228961E98AA67F088FCFAF04F90BE9A473,
	FingerList_AverageFloat_m87A269AF8CC5157E2521D6722A5E698620AD6E5D,
	FingerList_GetFingerStartPosition_mF067428CB682BD1F5A678BE3078CE544E65C6E52,
	FingerList_GetFingerPosition_m8F3C376B3E4904ADDCCDF4709A7A8F1BD7F03A24,
	FingerList_GetFingerPreviousPosition_m3B743773C1BE8C2CBC012D0ACB28BD7C6B40328F,
	FingerList_GetFingerDistanceFromStart_mE7D9937DBCE19485CF40E2D9E2F2D6EEE269C76A,
	FingerList_GetAverageStartPosition_m096CAACC8139F3843F21091A583995B9477BEC6E,
	FingerList_GetAveragePosition_m2F1246B791C7781DBE0994CE3C72323DB80A70FE,
	FingerList_GetAveragePreviousPosition_m252AE475CD3E8FD3540DD039D70308D8E9F18624,
	FingerList_GetAverageDistanceFromStart_m1D67040684E791F71FAF9A2FD0139BED7987C4F3,
	FingerList_GetOldest_mBD6328AFAD4F716B354D0223AECFDA471DF4581E,
	FingerList_MovingInSameDirection_m063053B37E7BDF2D6F620C28E9D0BE16AF6AD9C9,
	FingerList_AllMoving_mBC0189D845D8F5B3448682EE6577A161D80908F5,
	Point__ctor_m180060A42155920D221AF1955B040B0CD829E8ED_AdjustorThunk,
	Point__ctor_m67C8BCB74264107D37F9D4F87AE79E5B1ED7E648_AdjustorThunk,
	NormalizedTemplate__ctor_m7BBAE42EBC9076D5462E6128273D037FD7CA57CB,
	GestureNormalizer__ctor_mFC63331C7E9123AE627CC69EDBFA7F4FDA453809,
	GestureNormalizer_Apply_m84D307A78C15A57C39750CA79304B0143A9F17E7,
	GestureNormalizer_Resample_m5D07D707551B59AD2E00FACCBF50CEB8DCF58FAB,
	GestureNormalizer_PathLength_mF6EE7BF422B49A6CAC17175A2AFF193976F79C81,
	GestureNormalizer_Scale_m7C62517507431BE2B757867ED89ACDBD9CF27308,
	GestureNormalizer_TranslateToOrigin_m683AB61C1963279F75A7681E5651C0E8E4099DFB,
	GestureNormalizer_Centroid_m27461441D62D3E9E207C582D1D4BB9C8E3BAEC62,
	U3CU3Ec__DisplayClass10_0__ctor_m3043EBCB1F86072B1F702CFB2E5859DEDFAC9761,
	U3CU3Ec__DisplayClass10_0_U3CFindNormalizedTemplateU3Eb__0_m3E5A1B015A9A6765B03BB6181804640F0A60CF80,
	U3CU3Ec__cctor_m24B3098B532C9CCE35C6BD304046205854F6A991,
	U3CU3Ec__ctor_m7BBFAE45DF6B48C4071442316BD3D3B0CC854B10,
	U3CU3Ec_U3CInstallGestureRecognizersU3Eb__52_0_mD9AEB31075683873A6D9257C9FC856D309F41521,
	U3CU3Ec_U3CInstallGestureRecognizersU3Eb__52_1_m3DA0BB81806EB1E8DB1C0CAC721CB6BD78C45B1A,
	U3CU3Ec_U3CInstallGestureRecognizersU3Eb__52_2_mE3493C2DA947D7F5BEA0273D595E49265B49E909,
	PanEventHandler__ctor_m949D346D331D68F0C294765324D5950C78DED8A5,
	PanEventHandler_Invoke_m156218E902162709CB2B218B43DF7C2F380F5DF7,
	PanEventHandler_BeginInvoke_m3B80C67B8D1F52222DC37069F15E6178EA3D754B,
	PanEventHandler_EndInvoke_m55BB719742CEDB55247B40568F52498BD2A721DE,
	ImageProperties__ctor_m50D620AF0B15FA57F34EDDF7A4A4084D859C8CF8_AdjustorThunk,
	VideoProperties__ctor_mFFE46E5EE26EB279B7D28D60187FDD5B374DF2E3_AdjustorThunk,
	MediaSaveCallback__ctor_m4828FB6C10A1B1648CB67A0365916474F9AA4767,
	MediaSaveCallback_Invoke_m59C758F8247D68A3D649AEC124CA0B94DBBA4A59,
	MediaSaveCallback_BeginInvoke_m552765D029E8437A3207DF853843ECBD1328BA3A,
	MediaSaveCallback_EndInvoke_m65AF9C9D3382E1DEB63E13F1A934427A59839FE8,
	MediaPickCallback__ctor_mE60E8E3F8CFB5FBFB773660006A569F26BA7DFED,
	MediaPickCallback_Invoke_m786BDC23FBF9A2E1FCFF094DD5BD14AEDC429742,
	MediaPickCallback_BeginInvoke_m162B1ABA1B63F23137CB1765C7924FDC05FBBEE8,
	MediaPickCallback_EndInvoke_m4594A4A3136818FDD8676A2645E3AEDA6300A032,
	MediaPickMultipleCallback__ctor_m2B18FCF912D2A5630BBACFAC8262E1C7F5F44D56,
	MediaPickMultipleCallback_Invoke_m66769A4E767AB564957473A03B03917FE0B18734,
	MediaPickMultipleCallback_BeginInvoke_m59F9CD0A772912B7BB3435C1641F49F63649B1F8,
	MediaPickMultipleCallback_EndInvoke_mB627F3433B6B29241EAD8B600CB8CF0BB86247CD,
	U3CWaitForThenU3Ed__13__ctor_m9289F929E0DC9A02ABE1223B74C7E7DFA0ECA9B4,
	U3CWaitForThenU3Ed__13_System_IDisposable_Dispose_m3D6753B01F9ACBD31D68E1A998317BD0FD57943D,
	U3CWaitForThenU3Ed__13_MoveNext_m73BE245F8C19AD314815A098CE4D8747A0480E21,
	U3CWaitForThenU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB73157555D4D47293AD9A1ACA244551BE71DF00,
	U3CWaitForThenU3Ed__13_System_Collections_IEnumerator_Reset_m29C45E5A0D61FF31ED030C203EB59274D3D66758,
	U3CWaitForThenU3Ed__13_System_Collections_IEnumerator_get_Current_m4B15C342E892F135137C8CB6F1C5CA55A8FD4308,
	U3CU3Ec__cctor_m42A0D3C4C32F9AF5F2B1ED549F350C3A68FB0549,
	U3CU3Ec__ctor_m33BF7F0480C410341FD6FEA61803CB97F7C58061,
	U3CU3Ec_U3C_ctorU3Eb__14_0_mEFCADA3426388352F20386F407CB1B8D3BE0EAE5,
	U3CU3Ec_U3C_ctorU3Eb__14_1_m1D70FB686B14F9A858F6E1C7207B4183A187E179,
	FingerData__ctor_m958420B280F124B18A1EE09D6043AA097B0F2A52,
	LeanFingerEvent__ctor_m240731BD45F683FE9FC117347F0ED4626C3D24F6,
	Vector3Event__ctor_m5B08217E4EFF53B05A165305B345D2201A11C34E,
	LeanFingerEvent__ctor_mF2F82DC9A6390D4C5C6F1810A29F0EBDD8D039DC,
	Vector3Event__ctor_m783F60F4FEDEC60DA828C3494B30528A1D15287E,
	LeanFingerEvent__ctor_mC2EDFBF8B8201551EAB90B9DC51A38BE93DC6F36,
	FloatEvent__ctor_m55B0C3A9958469B2566D2FF50D450D958C0746F6,
	Vector2Event__ctor_m6D4AA794E04F10D72A4125741702326713F65018,
	Vector3Event__ctor_m5A7FED7217E52F55C90D8483A5737364752356B3,
	Vector3Vector3Event__ctor_mC725A539F1B59C1C6658F158832C3A7E34C3F5AE,
	LeanFingerEvent__ctor_mB426C3E58D460DDC3390C8669AB2C432A9A9F142,
	Vector3Event__ctor_m2ADAEC68F844784F3F4DE471CE0FDBBCFC9A7D5C,
	LeanFingerEvent__ctor_m03B5D101B3A3B4F5D743FE6A0BDE2C2852043FA9,
	Vector3Event__ctor_mF38D6FF8658299B0A684715E5FE46198F6EB6771,
	LeanFingerEvent__ctor_mFBD47862EF4515B27A1684FF4EADA41B1D413D79,
	LeanFingerEvent__ctor_mE8081565DC33AB6A984E77B4F854F7E62832A52A,
	FloatEvent__ctor_mB28E6C59B5FBED30471BFECCBADC731CBEF73585,
	Vector2Event__ctor_m68F38FCE3DF6CD53113F243D77A9A813F93F56D7,
	Vector3Event__ctor_m8584FD2B5AD4EC33E0152C08D0E231F62FABC5C7,
	Vector3Vector3Event__ctor_m664EAC04BB50FCB67F1CBB329137B236DFC6A051,
	Path__ctor_m49A7B4E18B095A63BB32F2E09FE99BC54C87F552,
	Node_Increment_m5223A3C76083C8449A585B94C6665003E1C93CFF,
	Node__ctor_m4C8F1B861459D5988487680E3B44FA5D89C65C6C,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[1288] = 
{
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	401,
	32,
	32,
	32,
	23,
	23,
	401,
	32,
	32,
	32,
	23,
	23,
	23,
	23,
	23,
	401,
	32,
	32,
	32,
	23,
	23,
	401,
	32,
	32,
	32,
	23,
	23,
	23,
	23,
	1099,
	1642,
	23,
	23,
	23,
	23,
	169,
	23,
	23,
	23,
	23,
	23,
	23,
	169,
	23,
	23,
	23,
	23,
	23,
	23,
	169,
	23,
	23,
	23,
	23,
	23,
	23,
	169,
	23,
	23,
	23,
	23,
	23,
	23,
	169,
	23,
	23,
	23,
	23,
	23,
	23,
	169,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	401,
	32,
	32,
	32,
	23,
	23,
	401,
	32,
	32,
	32,
	23,
	10,
	23,
	-1,
	-1,
	-1,
	23,
	-1,
	-1,
	14,
	26,
	14,
	26,
	14,
	26,
	1155,
	1156,
	14,
	26,
	2055,
	2056,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	14,
	23,
	23,
	23,
	23,
	26,
	2055,
	1245,
	26,
	23,
	26,
	26,
	14,
	26,
	14,
	26,
	679,
	289,
	1155,
	1156,
	1155,
	1156,
	10,
	32,
	10,
	679,
	14,
	26,
	14,
	26,
	2055,
	2056,
	2057,
	26,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	10,
	32,
	114,
	10,
	14,
	14,
	23,
	23,
	23,
	26,
	9,
	23,
	9,
	23,
	3,
	115,
	23,
	14,
	14,
	14,
	14,
	23,
	23,
	34,
	14,
	28,
	23,
	23,
	2058,
	2059,
	23,
	23,
	26,
	26,
	26,
	23,
	10,
	32,
	23,
	26,
	26,
	23,
	435,
	26,
	23,
	1155,
	1156,
	10,
	32,
	679,
	23,
	26,
	26,
	26,
	26,
	2060,
	26,
	23,
	679,
	289,
	23,
	26,
	26,
	26,
	23,
	122,
	122,
	122,
	122,
	122,
	122,
	4,
	4,
	23,
	14,
	23,
	26,
	10,
	43,
	4,
	4,
	122,
	122,
	1173,
	1184,
	386,
	114,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	4,
	122,
	2061,
	113,
	23,
	2062,
	2063,
	49,
	94,
	1882,
	1300,
	386,
	23,
	3,
	1155,
	1156,
	1155,
	23,
	14,
	28,
	115,
	27,
	41,
	23,
	23,
	14,
	27,
	41,
	23,
	679,
	289,
	679,
	289,
	23,
	14,
	10,
	32,
	114,
	28,
	10,
	115,
	27,
	41,
	115,
	23,
	1155,
	679,
	679,
	23,
	1190,
	2066,
	23,
	1758,
	37,
	10,
	10,
	23,
	23,
	23,
	23,
	23,
	28,
	28,
	9,
	27,
	9,
	2067,
	2068,
	133,
	41,
	14,
	23,
	858,
	23,
	3,
	1155,
	1156,
	679,
	289,
	10,
	32,
	23,
	14,
	115,
	27,
	41,
	30,
	23,
	10,
	32,
	23,
	114,
	9,
	26,
	28,
	1245,
	41,
	41,
	14,
	27,
	41,
	23,
	679,
	289,
	679,
	289,
	23,
	14,
	10,
	32,
	114,
	10,
	28,
	115,
	27,
	41,
	115,
	2069,
	23,
	10,
	2070,
	23,
	23,
	23,
	23,
	23,
	10,
	2070,
	23,
	23,
	23,
	23,
	30,
	1743,
	10,
	2070,
	23,
	23,
	23,
	114,
	26,
	23,
	386,
	1259,
	1362,
	1147,
	23,
	23,
	23,
	26,
	23,
	679,
	679,
	289,
	679,
	679,
	289,
	679,
	679,
	289,
	1146,
	1147,
	1146,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	1295,
	23,
	23,
	26,
	26,
	23,
	23,
	26,
	23,
	1103,
	1147,
	1147,
	1103,
	23,
	1146,
	1147,
	679,
	289,
	679,
	289,
	679,
	289,
	679,
	23,
	23,
	26,
	23,
	114,
	31,
	23,
	2072,
	26,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	679,
	289,
	679,
	289,
	23,
	23,
	26,
	23,
	28,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	1146,
	26,
	23,
	131,
	131,
	131,
	3,
	134,
	134,
	322,
	3,
	0,
	0,
	502,
	4,
	4,
	227,
	227,
	49,
	3,
	731,
	731,
	731,
	731,
	731,
	49,
	2073,
	148,
	2073,
	148,
	49,
	2074,
	2074,
	2049,
	1,
	2075,
	2075,
	2076,
	2077,
	2078,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	31,
	114,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	31,
	561,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	31,
	114,
	31,
	114,
	31,
	23,
	23,
	23,
	23,
	114,
	31,
	114,
	31,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	289,
	289,
	31,
	23,
	27,
	14,
	23,
	1550,
	1550,
	1550,
	27,
	14,
	-1,
	-1,
	23,
	3,
	49,
	795,
	131,
	122,
	23,
	26,
	23,
	122,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	168,
	26,
	23,
	26,
	26,
	23,
	23,
	23,
	1156,
	1156,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	31,
	2080,
	26,
	26,
	26,
	23,
	319,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	26,
	27,
	32,
	289,
	1156,
	1147,
	1757,
	169,
	23,
	10,
	37,
	1151,
	2081,
	2081,
	1144,
	2082,
	2083,
	2084,
	2085,
	2086,
	2087,
	2088,
	2089,
	56,
	2090,
	23,
	23,
	23,
	3,
	26,
	26,
	23,
	23,
	23,
	2091,
	2091,
	23,
	2092,
	1379,
	2093,
	2094,
	23,
	23,
	23,
	289,
	289,
	1156,
	23,
	23,
	23,
	1761,
	2095,
	2096,
	2097,
	111,
	3,
	26,
	26,
	1195,
	21,
	2098,
	2099,
	27,
	27,
	23,
	23,
	23,
	23,
	3,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	14,
	14,
	14,
	14,
	31,
	114,
	114,
	131,
	14,
	14,
	2100,
	122,
	133,
	0,
	134,
	9,
	187,
	23,
	26,
	23,
	3,
	23,
	23,
	122,
	122,
	114,
	122,
	122,
	23,
	3,
	14,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	26,
	23,
	1181,
	23,
	23,
	26,
	23,
	1181,
	23,
	23,
	26,
	23,
	1181,
	23,
	23,
	1147,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	1157,
	2101,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	26,
	26,
	23,
	23,
	23,
	2091,
	2091,
	289,
	289,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	114,
	679,
	114,
	114,
	114,
	1155,
	1155,
	1155,
	1155,
	1155,
	1155,
	2102,
	2102,
	2103,
	2103,
	2103,
	2104,
	1809,
	1809,
	1809,
	1809,
	1809,
	2105,
	1809,
	2105,
	1809,
	1809,
	1809,
	1809,
	1809,
	1809,
	2106,
	2106,
	2106,
	2106,
	2104,
	32,
	23,
	23,
	1308,
	1369,
	189,
	1308,
	1369,
	189,
	1308,
	1369,
	189,
	1308,
	1369,
	189,
	1308,
	1369,
	189,
	2107,
	2108,
	2109,
	1173,
	1140,
	2110,
	2111,
	1173,
	1140,
	2110,
	2111,
	1173,
	1140,
	2110,
	2111,
	1173,
	1140,
	2110,
	2111,
	1173,
	1140,
	2110,
	2111,
	1173,
	1140,
	2110,
	2111,
	386,
	2112,
	2113,
	386,
	2112,
	2113,
	1173,
	1140,
	2114,
	2115,
	1173,
	1140,
	2114,
	2115,
	2106,
	4,
	2116,
	2117,
	1964,
	23,
	3,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	122,
	1173,
	1173,
	131,
	2118,
	4,
	1173,
	1173,
	49,
	1,
	387,
	2119,
	2120,
	2121,
	2122,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2123,
	34,
	37,
	23,
	3,
	-1,
	387,
	-1,
	23,
	23,
	23,
	2124,
	2125,
	1147,
	23,
	3,
	14,
	23,
	32,
	10,
	23,
	26,
	131,
	131,
	133,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	102,
	26,
	177,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	9,
	23,
	10,
	114,
	10,
	10,
	114,
	114,
	114,
	114,
	114,
	114,
	679,
	1155,
	1155,
	1155,
	1155,
	679,
	114,
	679,
	14,
	14,
	32,
	14,
	2064,
	102,
	2061,
	2065,
	9,
	34,
	10,
	1155,
	1155,
	1155,
	679,
	14,
	114,
	433,
	23,
	26,
	34,
	10,
	14,
	14,
	26,
	9,
	9,
	26,
	23,
	1807,
	192,
	1369,
	1369,
	1369,
	1140,
	1155,
	1155,
	1155,
	679,
	14,
	433,
	114,
	1190,
	2066,
	23,
	23,
	58,
	58,
	1140,
	122,
	122,
	1369,
	23,
	9,
	3,
	23,
	9,
	9,
	9,
	102,
	1889,
	2071,
	26,
	61,
	2079,
	102,
	26,
	177,
	26,
	102,
	26,
	177,
	26,
	102,
	26,
	177,
	26,
	32,
	23,
	114,
	14,
	23,
	14,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1158,
	23,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[21] = 
{
	{ 0x02000016, { 0, 6 } },
	{ 0x02000018, { 6, 5 } },
	{ 0x0200001A, { 11, 10 } },
	{ 0x0200001F, { 21, 42 } },
	{ 0x02000089, { 63, 1 } },
	{ 0x0200008A, { 64, 4 } },
	{ 0x06000217, { 68, 2 } },
	{ 0x06000218, { 70, 2 } },
	{ 0x06000219, { 72, 2 } },
	{ 0x0600021A, { 74, 2 } },
	{ 0x0600021B, { 76, 2 } },
	{ 0x06000297, { 78, 5 } },
	{ 0x06000298, { 83, 1 } },
	{ 0x060002C2, { 84, 3 } },
	{ 0x060002C3, { 87, 3 } },
	{ 0x060002C4, { 90, 5 } },
	{ 0x060002C5, { 95, 4 } },
	{ 0x060002C6, { 99, 3 } },
	{ 0x060002C7, { 102, 7 } },
	{ 0x0600043C, { 109, 3 } },
	{ 0x0600043E, { 112, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[113] = 
{
	{ (Il2CppRGCTXDataType)3, 16547 },
	{ (Il2CppRGCTXDataType)3, 16548 },
	{ (Il2CppRGCTXDataType)2, 21533 },
	{ (Il2CppRGCTXDataType)3, 16549 },
	{ (Il2CppRGCTXDataType)3, 16550 },
	{ (Il2CppRGCTXDataType)2, 21532 },
	{ (Il2CppRGCTXDataType)3, 16551 },
	{ (Il2CppRGCTXDataType)2, 21538 },
	{ (Il2CppRGCTXDataType)3, 16552 },
	{ (Il2CppRGCTXDataType)3, 16553 },
	{ (Il2CppRGCTXDataType)2, 21537 },
	{ (Il2CppRGCTXDataType)3, 16554 },
	{ (Il2CppRGCTXDataType)1, 21545 },
	{ (Il2CppRGCTXDataType)3, 16555 },
	{ (Il2CppRGCTXDataType)2, 22334 },
	{ (Il2CppRGCTXDataType)3, 16556 },
	{ (Il2CppRGCTXDataType)3, 16557 },
	{ (Il2CppRGCTXDataType)2, 21545 },
	{ (Il2CppRGCTXDataType)3, 16558 },
	{ (Il2CppRGCTXDataType)3, 16559 },
	{ (Il2CppRGCTXDataType)3, 16560 },
	{ (Il2CppRGCTXDataType)2, 21568 },
	{ (Il2CppRGCTXDataType)3, 16561 },
	{ (Il2CppRGCTXDataType)3, 16562 },
	{ (Il2CppRGCTXDataType)2, 21570 },
	{ (Il2CppRGCTXDataType)3, 16563 },
	{ (Il2CppRGCTXDataType)3, 16564 },
	{ (Il2CppRGCTXDataType)2, 21569 },
	{ (Il2CppRGCTXDataType)3, 16565 },
	{ (Il2CppRGCTXDataType)3, 16566 },
	{ (Il2CppRGCTXDataType)3, 16567 },
	{ (Il2CppRGCTXDataType)1, 21569 },
	{ (Il2CppRGCTXDataType)2, 22335 },
	{ (Il2CppRGCTXDataType)3, 16568 },
	{ (Il2CppRGCTXDataType)3, 16569 },
	{ (Il2CppRGCTXDataType)2, 22336 },
	{ (Il2CppRGCTXDataType)3, 16570 },
	{ (Il2CppRGCTXDataType)3, 16571 },
	{ (Il2CppRGCTXDataType)2, 22337 },
	{ (Il2CppRGCTXDataType)3, 16572 },
	{ (Il2CppRGCTXDataType)3, 16573 },
	{ (Il2CppRGCTXDataType)3, 16574 },
	{ (Il2CppRGCTXDataType)3, 16575 },
	{ (Il2CppRGCTXDataType)3, 16576 },
	{ (Il2CppRGCTXDataType)3, 16577 },
	{ (Il2CppRGCTXDataType)3, 16578 },
	{ (Il2CppRGCTXDataType)3, 16579 },
	{ (Il2CppRGCTXDataType)3, 16580 },
	{ (Il2CppRGCTXDataType)3, 16581 },
	{ (Il2CppRGCTXDataType)2, 22338 },
	{ (Il2CppRGCTXDataType)3, 16582 },
	{ (Il2CppRGCTXDataType)3, 16583 },
	{ (Il2CppRGCTXDataType)3, 16584 },
	{ (Il2CppRGCTXDataType)3, 16585 },
	{ (Il2CppRGCTXDataType)2, 22339 },
	{ (Il2CppRGCTXDataType)3, 16586 },
	{ (Il2CppRGCTXDataType)3, 16587 },
	{ (Il2CppRGCTXDataType)3, 16588 },
	{ (Il2CppRGCTXDataType)3, 16589 },
	{ (Il2CppRGCTXDataType)3, 16590 },
	{ (Il2CppRGCTXDataType)3, 16591 },
	{ (Il2CppRGCTXDataType)3, 16592 },
	{ (Il2CppRGCTXDataType)3, 16593 },
	{ (Il2CppRGCTXDataType)2, 21580 },
	{ (Il2CppRGCTXDataType)2, 22340 },
	{ (Il2CppRGCTXDataType)3, 16594 },
	{ (Il2CppRGCTXDataType)2, 22340 },
	{ (Il2CppRGCTXDataType)2, 21584 },
	{ (Il2CppRGCTXDataType)3, 16595 },
	{ (Il2CppRGCTXDataType)2, 21742 },
	{ (Il2CppRGCTXDataType)3, 16596 },
	{ (Il2CppRGCTXDataType)2, 21743 },
	{ (Il2CppRGCTXDataType)3, 16597 },
	{ (Il2CppRGCTXDataType)2, 21744 },
	{ (Il2CppRGCTXDataType)3, 16598 },
	{ (Il2CppRGCTXDataType)2, 21745 },
	{ (Il2CppRGCTXDataType)3, 16599 },
	{ (Il2CppRGCTXDataType)2, 21746 },
	{ (Il2CppRGCTXDataType)1, 21817 },
	{ (Il2CppRGCTXDataType)2, 21818 },
	{ (Il2CppRGCTXDataType)2, 21817 },
	{ (Il2CppRGCTXDataType)1, 21818 },
	{ (Il2CppRGCTXDataType)3, 16600 },
	{ (Il2CppRGCTXDataType)1, 21820 },
	{ (Il2CppRGCTXDataType)3, 16601 },
	{ (Il2CppRGCTXDataType)3, 16602 },
	{ (Il2CppRGCTXDataType)2, 21843 },
	{ (Il2CppRGCTXDataType)3, 16603 },
	{ (Il2CppRGCTXDataType)3, 16604 },
	{ (Il2CppRGCTXDataType)2, 21845 },
	{ (Il2CppRGCTXDataType)3, 16605 },
	{ (Il2CppRGCTXDataType)3, 16606 },
	{ (Il2CppRGCTXDataType)2, 21847 },
	{ (Il2CppRGCTXDataType)3, 16607 },
	{ (Il2CppRGCTXDataType)3, 16608 },
	{ (Il2CppRGCTXDataType)3, 16609 },
	{ (Il2CppRGCTXDataType)3, 16610 },
	{ (Il2CppRGCTXDataType)3, 16611 },
	{ (Il2CppRGCTXDataType)3, 16612 },
	{ (Il2CppRGCTXDataType)3, 16613 },
	{ (Il2CppRGCTXDataType)3, 16614 },
	{ (Il2CppRGCTXDataType)2, 21855 },
	{ (Il2CppRGCTXDataType)2, 21857 },
	{ (Il2CppRGCTXDataType)3, 16615 },
	{ (Il2CppRGCTXDataType)3, 16616 },
	{ (Il2CppRGCTXDataType)3, 16617 },
	{ (Il2CppRGCTXDataType)2, 21858 },
	{ (Il2CppRGCTXDataType)3, 16618 },
	{ (Il2CppRGCTXDataType)3, 16619 },
	{ (Il2CppRGCTXDataType)1, 22037 },
	{ (Il2CppRGCTXDataType)3, 16620 },
	{ (Il2CppRGCTXDataType)2, 22037 },
	{ (Il2CppRGCTXDataType)2, 22038 },
};
extern const Il2CppCodeGenModule g_VuforiaSamplesCodeGenModule;
const Il2CppCodeGenModule g_VuforiaSamplesCodeGenModule = 
{
	"VuforiaSamples.dll",
	1288,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	21,
	s_rgctxIndices,
	113,
	s_rgctxValues,
	NULL,
};
