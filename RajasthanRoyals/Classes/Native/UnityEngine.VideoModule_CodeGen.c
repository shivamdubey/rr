﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.Video.VideoClip::.ctor()
extern void VideoClip__ctor_m9BB7ECD808FCBD3DA63594134BEEC12B17852117 ();
// 0x00000002 System.String UnityEngine.Video.VideoClip::get_originalPath()
extern void VideoClip_get_originalPath_m2A5B5BD6026E06E89487B1D6FE3ACF4BED358F79 ();
// 0x00000003 System.UInt64 UnityEngine.Video.VideoClip::get_frameCount()
extern void VideoClip_get_frameCount_m4C975B2BE9AAFC46D6DC3C6CFDA2F8F53BA8F812 ();
// 0x00000004 System.Double UnityEngine.Video.VideoClip::get_frameRate()
extern void VideoClip_get_frameRate_m810D26D2FABD566884970B63F33647DAF1FE34F0 ();
// 0x00000005 System.Double UnityEngine.Video.VideoClip::get_length()
extern void VideoClip_get_length_m33EC3F6A3D2F851ECC2B672D1603F956AB1887FB ();
// 0x00000006 System.UInt32 UnityEngine.Video.VideoClip::get_width()
extern void VideoClip_get_width_m89215785A9E7EEB59F7A23BC64C3D7F446621E2E ();
// 0x00000007 System.UInt32 UnityEngine.Video.VideoClip::get_height()
extern void VideoClip_get_height_mFCE61A0B0DD212D9A9773AE66148D25270656819 ();
// 0x00000008 System.UInt16 UnityEngine.Video.VideoClip::get_audioTrackCount()
extern void VideoClip_get_audioTrackCount_mF0CCBF177B3DC5338F603F4D94429787D8CFAD83 ();
// 0x00000009 UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Video.VideoClipPlayable::GetHandle()
extern void VideoClipPlayable_GetHandle_m5B2EFD8CFE93DB4D2EE29A600E598233471DF242_AdjustorThunk ();
// 0x0000000A System.Boolean UnityEngine.Experimental.Video.VideoClipPlayable::Equals(UnityEngine.Experimental.Video.VideoClipPlayable)
extern void VideoClipPlayable_Equals_m2F07E6EBA96043274F2384F18E43B66035058BC2_AdjustorThunk ();
// 0x0000000B UnityEngine.Video.VideoClip UnityEngine.Video.VideoPlayer::get_clip()
extern void VideoPlayer_get_clip_mA2C3AC016BB7B09855C56A11843AE60EB4D36B76 ();
// 0x0000000C System.Void UnityEngine.Video.VideoPlayer::Play()
extern void VideoPlayer_Play_m2BCD775F42A36AC291C7B32D9E4D934EF0B91257 ();
// 0x0000000D System.Void UnityEngine.Video.VideoPlayer::Pause()
extern void VideoPlayer_Pause_m62EE5660CFA287D78BB3FE815CA5649242509B93 ();
// 0x0000000E System.Boolean UnityEngine.Video.VideoPlayer::get_isPlaying()
extern void VideoPlayer_get_isPlaying_mC7CFE17762C14F2AFB1D73500317B9D25A7395DF ();
// 0x0000000F System.Int64 UnityEngine.Video.VideoPlayer::get_frame()
extern void VideoPlayer_get_frame_mB7F5972A74C2D4039855454F552AD08BF12F30A0 ();
// 0x00000010 System.UInt64 UnityEngine.Video.VideoPlayer::get_frameCount()
extern void VideoPlayer_get_frameCount_m89C61BE7B88F1A573FA42C2A7564230A2234F709 ();
// 0x00000011 System.UInt16 UnityEngine.Video.VideoPlayer::get_audioTrackCount()
extern void VideoPlayer_get_audioTrackCount_m9B51B6DCE2D782A177500AC4C5DC645813FE7C44 ();
// 0x00000012 UnityEngine.AudioSource UnityEngine.Video.VideoPlayer::GetTargetAudioSource(System.UInt16)
extern void VideoPlayer_GetTargetAudioSource_m3D0D953E21725ADEDB001F9C63A5281608D18CCE ();
// 0x00000013 System.Void UnityEngine.Video.VideoPlayer::add_prepareCompleted(UnityEngine.Video.VideoPlayer_EventHandler)
extern void VideoPlayer_add_prepareCompleted_mA09867482AAB164B8AA6820FFE57E3F391CB8FE4 ();
// 0x00000014 System.Void UnityEngine.Video.VideoPlayer::remove_prepareCompleted(UnityEngine.Video.VideoPlayer_EventHandler)
extern void VideoPlayer_remove_prepareCompleted_m6BBFD6B47A7627DA597319ADA50403B8C0F02186 ();
// 0x00000015 System.Void UnityEngine.Video.VideoPlayer::add_loopPointReached(UnityEngine.Video.VideoPlayer_EventHandler)
extern void VideoPlayer_add_loopPointReached_m67619E2B83DC46D8DA3DF0CFAC24399FA0A2D932 ();
// 0x00000016 System.Void UnityEngine.Video.VideoPlayer::remove_loopPointReached(UnityEngine.Video.VideoPlayer_EventHandler)
extern void VideoPlayer_remove_loopPointReached_m1E4A0D894AA6C597E9E7EC9F389069BF6E9E2F0B ();
// 0x00000017 System.Void UnityEngine.Video.VideoPlayer::add_started(UnityEngine.Video.VideoPlayer_EventHandler)
extern void VideoPlayer_add_started_m97B60BBA14F176FAC93E2713AB9B0B1D1E094207 ();
// 0x00000018 System.Void UnityEngine.Video.VideoPlayer::remove_started(UnityEngine.Video.VideoPlayer_EventHandler)
extern void VideoPlayer_remove_started_m7F677227C0FE32FBFCC830D2D0507A455210FB1F ();
// 0x00000019 System.Void UnityEngine.Video.VideoPlayer::add_errorReceived(UnityEngine.Video.VideoPlayer_ErrorEventHandler)
extern void VideoPlayer_add_errorReceived_mABB9E416B6E5F505A4F408CB041BE9FE1597FC5D ();
// 0x0000001A System.Void UnityEngine.Video.VideoPlayer::remove_errorReceived(UnityEngine.Video.VideoPlayer_ErrorEventHandler)
extern void VideoPlayer_remove_errorReceived_m94B7BA2DE9A008839683C9A3311A01A36CEDAB88 ();
// 0x0000001B System.Void UnityEngine.Video.VideoPlayer::add_seekCompleted(UnityEngine.Video.VideoPlayer_EventHandler)
extern void VideoPlayer_add_seekCompleted_m6884EFA42474E5E146989B4DB9E624EF10FE270C ();
// 0x0000001C System.Void UnityEngine.Video.VideoPlayer::remove_seekCompleted(UnityEngine.Video.VideoPlayer_EventHandler)
extern void VideoPlayer_remove_seekCompleted_mF6B1BD552840E9ACA57E5FA3275F564EF83D5F95 ();
// 0x0000001D System.Void UnityEngine.Video.VideoPlayer::InvokePrepareCompletedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokePrepareCompletedCallback_Internal_m4CFD7054C97BE95CAC055CF18466E90D060E9B53 ();
// 0x0000001E System.Void UnityEngine.Video.VideoPlayer::InvokeFrameReadyCallback_Internal(UnityEngine.Video.VideoPlayer,System.Int64)
extern void VideoPlayer_InvokeFrameReadyCallback_Internal_m4F62FC3695CFC72045E3C90503D541AE6E023EE8 ();
// 0x0000001F System.Void UnityEngine.Video.VideoPlayer::InvokeLoopPointReachedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeLoopPointReachedCallback_Internal_m1A07EB382FD3CE673FF171A11047BEC54A6BB9AB ();
// 0x00000020 System.Void UnityEngine.Video.VideoPlayer::InvokeStartedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeStartedCallback_Internal_m28D5BF153FC37959C225292BE859135FF778C8BB ();
// 0x00000021 System.Void UnityEngine.Video.VideoPlayer::InvokeFrameDroppedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeFrameDroppedCallback_Internal_m669EAEF06893B53351EE81C3858BD62661228C58 ();
// 0x00000022 System.Void UnityEngine.Video.VideoPlayer::InvokeErrorReceivedCallback_Internal(UnityEngine.Video.VideoPlayer,System.String)
extern void VideoPlayer_InvokeErrorReceivedCallback_Internal_mF7849030756F4A2B5226437C165ECDFE6E52E385 ();
// 0x00000023 System.Void UnityEngine.Video.VideoPlayer::InvokeSeekCompletedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeSeekCompletedCallback_Internal_mFFB686F5BF044F61495CCF2EFB3517857F98C078 ();
// 0x00000024 System.Void UnityEngine.Video.VideoPlayer::InvokeClockResyncOccurredCallback_Internal(UnityEngine.Video.VideoPlayer,System.Double)
extern void VideoPlayer_InvokeClockResyncOccurredCallback_Internal_m21D2B32DDE5ED48F6D6F2ECA9B7A0A2724AF9D6F ();
// 0x00000025 System.Void UnityEngine.Video.VideoPlayer_EventHandler::.ctor(System.Object,System.IntPtr)
extern void EventHandler__ctor_mA31DCA369A8B7C473F6CE19F6B53D6F3FAF7D6A7 ();
// 0x00000026 System.Void UnityEngine.Video.VideoPlayer_EventHandler::Invoke(UnityEngine.Video.VideoPlayer)
extern void EventHandler_Invoke_m137A7D976F198147AD939AEF51E157107A3B1FBC ();
// 0x00000027 System.IAsyncResult UnityEngine.Video.VideoPlayer_EventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.AsyncCallback,System.Object)
extern void EventHandler_BeginInvoke_mCA1B5193B15F3D56BCB40A8DDBA703724040F348 ();
// 0x00000028 System.Void UnityEngine.Video.VideoPlayer_EventHandler::EndInvoke(System.IAsyncResult)
extern void EventHandler_EndInvoke_m95A975D9455F92F836E7E19BDB85538B1EBF4067 ();
// 0x00000029 System.Void UnityEngine.Video.VideoPlayer_ErrorEventHandler::.ctor(System.Object,System.IntPtr)
extern void ErrorEventHandler__ctor_m9E9B3A7A439858703258976491E29057CB17F534 ();
// 0x0000002A System.Void UnityEngine.Video.VideoPlayer_ErrorEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.String)
extern void ErrorEventHandler_Invoke_m0A812811B673439792D99C125EE4FFE5E358EF6C ();
// 0x0000002B System.IAsyncResult UnityEngine.Video.VideoPlayer_ErrorEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.String,System.AsyncCallback,System.Object)
extern void ErrorEventHandler_BeginInvoke_mD4C6F60629C221D7702E40D460818B90032FBAA7 ();
// 0x0000002C System.Void UnityEngine.Video.VideoPlayer_ErrorEventHandler::EndInvoke(System.IAsyncResult)
extern void ErrorEventHandler_EndInvoke_m7ABF3F8E15D2EF4AE6961324B66208A7FD127295 ();
// 0x0000002D System.Void UnityEngine.Video.VideoPlayer_FrameReadyEventHandler::.ctor(System.Object,System.IntPtr)
extern void FrameReadyEventHandler__ctor_m7DFDBF9203E8F9FC1093E1655C5E2695623D7E3D ();
// 0x0000002E System.Void UnityEngine.Video.VideoPlayer_FrameReadyEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.Int64)
extern void FrameReadyEventHandler_Invoke_m88D0AC1BED08D66B6CFA18DA23C58D10795DDA70 ();
// 0x0000002F System.IAsyncResult UnityEngine.Video.VideoPlayer_FrameReadyEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.Int64,System.AsyncCallback,System.Object)
extern void FrameReadyEventHandler_BeginInvoke_m5DA99DFE61C78E158FF79535447F7649FC09E5F1 ();
// 0x00000030 System.Void UnityEngine.Video.VideoPlayer_FrameReadyEventHandler::EndInvoke(System.IAsyncResult)
extern void FrameReadyEventHandler_EndInvoke_mC54DCEDB2F8CB30CBC6CD1590E2C08150E3E0CFF ();
// 0x00000031 System.Void UnityEngine.Video.VideoPlayer_TimeEventHandler::.ctor(System.Object,System.IntPtr)
extern void TimeEventHandler__ctor_mF41715E69B793B1C7DCA3A619CFB05097466523F ();
// 0x00000032 System.Void UnityEngine.Video.VideoPlayer_TimeEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.Double)
extern void TimeEventHandler_Invoke_m278E51F2838EC435606BE1CB3AD0E881505FAE10 ();
// 0x00000033 System.IAsyncResult UnityEngine.Video.VideoPlayer_TimeEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.Double,System.AsyncCallback,System.Object)
extern void TimeEventHandler_BeginInvoke_m184CF1FDB1D643F00FE3C60982ED62EC4888F21D ();
// 0x00000034 System.Void UnityEngine.Video.VideoPlayer_TimeEventHandler::EndInvoke(System.IAsyncResult)
extern void TimeEventHandler_EndInvoke_m2759449ABAAE5711D31E414D5906AB042725F7AA ();
static Il2CppMethodPointer s_methodPointers[52] = 
{
	VideoClip__ctor_m9BB7ECD808FCBD3DA63594134BEEC12B17852117,
	VideoClip_get_originalPath_m2A5B5BD6026E06E89487B1D6FE3ACF4BED358F79,
	VideoClip_get_frameCount_m4C975B2BE9AAFC46D6DC3C6CFDA2F8F53BA8F812,
	VideoClip_get_frameRate_m810D26D2FABD566884970B63F33647DAF1FE34F0,
	VideoClip_get_length_m33EC3F6A3D2F851ECC2B672D1603F956AB1887FB,
	VideoClip_get_width_m89215785A9E7EEB59F7A23BC64C3D7F446621E2E,
	VideoClip_get_height_mFCE61A0B0DD212D9A9773AE66148D25270656819,
	VideoClip_get_audioTrackCount_mF0CCBF177B3DC5338F603F4D94429787D8CFAD83,
	VideoClipPlayable_GetHandle_m5B2EFD8CFE93DB4D2EE29A600E598233471DF242_AdjustorThunk,
	VideoClipPlayable_Equals_m2F07E6EBA96043274F2384F18E43B66035058BC2_AdjustorThunk,
	VideoPlayer_get_clip_mA2C3AC016BB7B09855C56A11843AE60EB4D36B76,
	VideoPlayer_Play_m2BCD775F42A36AC291C7B32D9E4D934EF0B91257,
	VideoPlayer_Pause_m62EE5660CFA287D78BB3FE815CA5649242509B93,
	VideoPlayer_get_isPlaying_mC7CFE17762C14F2AFB1D73500317B9D25A7395DF,
	VideoPlayer_get_frame_mB7F5972A74C2D4039855454F552AD08BF12F30A0,
	VideoPlayer_get_frameCount_m89C61BE7B88F1A573FA42C2A7564230A2234F709,
	VideoPlayer_get_audioTrackCount_m9B51B6DCE2D782A177500AC4C5DC645813FE7C44,
	VideoPlayer_GetTargetAudioSource_m3D0D953E21725ADEDB001F9C63A5281608D18CCE,
	VideoPlayer_add_prepareCompleted_mA09867482AAB164B8AA6820FFE57E3F391CB8FE4,
	VideoPlayer_remove_prepareCompleted_m6BBFD6B47A7627DA597319ADA50403B8C0F02186,
	VideoPlayer_add_loopPointReached_m67619E2B83DC46D8DA3DF0CFAC24399FA0A2D932,
	VideoPlayer_remove_loopPointReached_m1E4A0D894AA6C597E9E7EC9F389069BF6E9E2F0B,
	VideoPlayer_add_started_m97B60BBA14F176FAC93E2713AB9B0B1D1E094207,
	VideoPlayer_remove_started_m7F677227C0FE32FBFCC830D2D0507A455210FB1F,
	VideoPlayer_add_errorReceived_mABB9E416B6E5F505A4F408CB041BE9FE1597FC5D,
	VideoPlayer_remove_errorReceived_m94B7BA2DE9A008839683C9A3311A01A36CEDAB88,
	VideoPlayer_add_seekCompleted_m6884EFA42474E5E146989B4DB9E624EF10FE270C,
	VideoPlayer_remove_seekCompleted_mF6B1BD552840E9ACA57E5FA3275F564EF83D5F95,
	VideoPlayer_InvokePrepareCompletedCallback_Internal_m4CFD7054C97BE95CAC055CF18466E90D060E9B53,
	VideoPlayer_InvokeFrameReadyCallback_Internal_m4F62FC3695CFC72045E3C90503D541AE6E023EE8,
	VideoPlayer_InvokeLoopPointReachedCallback_Internal_m1A07EB382FD3CE673FF171A11047BEC54A6BB9AB,
	VideoPlayer_InvokeStartedCallback_Internal_m28D5BF153FC37959C225292BE859135FF778C8BB,
	VideoPlayer_InvokeFrameDroppedCallback_Internal_m669EAEF06893B53351EE81C3858BD62661228C58,
	VideoPlayer_InvokeErrorReceivedCallback_Internal_mF7849030756F4A2B5226437C165ECDFE6E52E385,
	VideoPlayer_InvokeSeekCompletedCallback_Internal_mFFB686F5BF044F61495CCF2EFB3517857F98C078,
	VideoPlayer_InvokeClockResyncOccurredCallback_Internal_m21D2B32DDE5ED48F6D6F2ECA9B7A0A2724AF9D6F,
	EventHandler__ctor_mA31DCA369A8B7C473F6CE19F6B53D6F3FAF7D6A7,
	EventHandler_Invoke_m137A7D976F198147AD939AEF51E157107A3B1FBC,
	EventHandler_BeginInvoke_mCA1B5193B15F3D56BCB40A8DDBA703724040F348,
	EventHandler_EndInvoke_m95A975D9455F92F836E7E19BDB85538B1EBF4067,
	ErrorEventHandler__ctor_m9E9B3A7A439858703258976491E29057CB17F534,
	ErrorEventHandler_Invoke_m0A812811B673439792D99C125EE4FFE5E358EF6C,
	ErrorEventHandler_BeginInvoke_mD4C6F60629C221D7702E40D460818B90032FBAA7,
	ErrorEventHandler_EndInvoke_m7ABF3F8E15D2EF4AE6961324B66208A7FD127295,
	FrameReadyEventHandler__ctor_m7DFDBF9203E8F9FC1093E1655C5E2695623D7E3D,
	FrameReadyEventHandler_Invoke_m88D0AC1BED08D66B6CFA18DA23C58D10795DDA70,
	FrameReadyEventHandler_BeginInvoke_m5DA99DFE61C78E158FF79535447F7649FC09E5F1,
	FrameReadyEventHandler_EndInvoke_mC54DCEDB2F8CB30CBC6CD1590E2C08150E3E0CFF,
	TimeEventHandler__ctor_mF41715E69B793B1C7DCA3A619CFB05097466523F,
	TimeEventHandler_Invoke_m278E51F2838EC435606BE1CB3AD0E881505FAE10,
	TimeEventHandler_BeginInvoke_m184CF1FDB1D643F00FE3C60982ED62EC4888F21D,
	TimeEventHandler_EndInvoke_m2759449ABAAE5711D31E414D5906AB042725F7AA,
};
static const int32_t s_InvokerIndices[52] = 
{
	23,
	14,
	142,
	405,
	405,
	10,
	10,
	212,
	1112,
	1661,
	14,
	23,
	23,
	114,
	142,
	142,
	212,
	614,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	122,
	120,
	122,
	122,
	122,
	134,
	122,
	1662,
	102,
	26,
	177,
	26,
	102,
	27,
	213,
	26,
	102,
	141,
	1663,
	26,
	102,
	1570,
	1664,
	26,
};
extern const Il2CppCodeGenModule g_UnityEngine_VideoModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VideoModuleCodeGenModule = 
{
	"UnityEngine.VideoModule.dll",
	52,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
