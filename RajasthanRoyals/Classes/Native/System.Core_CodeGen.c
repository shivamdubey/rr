﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000008 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000009 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x0000000A System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000010 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfType(System.Collections.IEnumerable)
// 0x00000012 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfTypeIterator(System.Collections.IEnumerable)
// 0x00000013 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000014 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000015 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000017 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000018 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000019 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001A TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000001B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000001C System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001D System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001E System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000020 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000021 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000022 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000023 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000024 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000025 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000026 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000027 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000028 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000029 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002A System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000002B System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000002C System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000002D System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000002F System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000030 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000031 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000035 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000038 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000039 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000003A System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000003D System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003E System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000003F System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000040 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000041 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000043 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000044 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000045 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000046 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000047 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000048 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000049 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x0000004A System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000004B System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000004C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000004D System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::.ctor(System.Int32)
// 0x0000004E System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.IDisposable.Dispose()
// 0x0000004F System.Boolean System.Linq.Enumerable_<SelectIterator>d__5`2::MoveNext()
// 0x00000050 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::<>m__Finally1()
// 0x00000051 TResult System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000052 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.Reset()
// 0x00000053 System.Object System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.get_Current()
// 0x00000054 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000055 System.Collections.IEnumerator System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000056 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000057 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000058 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000059 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000005A System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::.ctor(System.Int32)
// 0x0000005B System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.IDisposable.Dispose()
// 0x0000005C System.Boolean System.Linq.Enumerable_<OfTypeIterator>d__97`1::MoveNext()
// 0x0000005D System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::<>m__Finally1()
// 0x0000005E TResult System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000005F System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.Reset()
// 0x00000060 System.Object System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.get_Current()
// 0x00000061 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000062 System.Collections.IEnumerator System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000063 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000064 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000065 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x00000066 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000067 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000068 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000069 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x0000006A System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000006B System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006C System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x0000006D System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000006E System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000006F System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000070 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000071 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000072 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000073 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000074 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000075 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000076 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000077 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000078 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000079 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000007A System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000007B System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000007C System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000007D System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000007E System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000007F System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000080 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000081 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000082 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000083 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000084 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000085 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000086 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000087 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000088 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000089 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000008A System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x0000008B System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000008C System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000008D System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000008E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000008F System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000090 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000091 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000092 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000093 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000094 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000095 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000096 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000097 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000098 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000099 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000009A System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000009B System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x0000009C System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x0000009D System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000009E System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000009F System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000A0 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000A1 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000A2 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000A3 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000A4 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000A5 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000A6 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000A7 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000A8 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000A9 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[169] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[169] = 
{
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[57] = 
{
	{ 0x02000004, { 94, 4 } },
	{ 0x02000005, { 98, 9 } },
	{ 0x02000006, { 109, 7 } },
	{ 0x02000007, { 118, 10 } },
	{ 0x02000008, { 130, 11 } },
	{ 0x02000009, { 144, 9 } },
	{ 0x0200000A, { 156, 12 } },
	{ 0x0200000B, { 171, 9 } },
	{ 0x0200000C, { 180, 1 } },
	{ 0x0200000D, { 181, 2 } },
	{ 0x0200000E, { 183, 6 } },
	{ 0x0200000F, { 189, 6 } },
	{ 0x02000010, { 195, 2 } },
	{ 0x02000012, { 197, 3 } },
	{ 0x02000013, { 202, 5 } },
	{ 0x02000014, { 207, 7 } },
	{ 0x02000015, { 214, 3 } },
	{ 0x02000016, { 217, 7 } },
	{ 0x02000017, { 224, 4 } },
	{ 0x02000018, { 228, 34 } },
	{ 0x0200001A, { 262, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 10 } },
	{ 0x06000007, { 20, 1 } },
	{ 0x06000008, { 21, 2 } },
	{ 0x06000009, { 23, 5 } },
	{ 0x0600000A, { 28, 5 } },
	{ 0x0600000B, { 33, 2 } },
	{ 0x0600000C, { 35, 1 } },
	{ 0x0600000D, { 36, 3 } },
	{ 0x0600000E, { 39, 2 } },
	{ 0x0600000F, { 41, 1 } },
	{ 0x06000010, { 42, 7 } },
	{ 0x06000011, { 49, 1 } },
	{ 0x06000012, { 50, 2 } },
	{ 0x06000013, { 52, 2 } },
	{ 0x06000014, { 54, 2 } },
	{ 0x06000015, { 56, 4 } },
	{ 0x06000016, { 60, 4 } },
	{ 0x06000017, { 64, 3 } },
	{ 0x06000018, { 67, 4 } },
	{ 0x06000019, { 71, 3 } },
	{ 0x0600001A, { 74, 3 } },
	{ 0x0600001B, { 77, 1 } },
	{ 0x0600001C, { 78, 1 } },
	{ 0x0600001D, { 79, 3 } },
	{ 0x0600001E, { 82, 3 } },
	{ 0x0600001F, { 85, 2 } },
	{ 0x06000020, { 87, 2 } },
	{ 0x06000021, { 89, 5 } },
	{ 0x06000031, { 107, 2 } },
	{ 0x06000036, { 116, 2 } },
	{ 0x0600003B, { 128, 2 } },
	{ 0x06000041, { 141, 3 } },
	{ 0x06000046, { 153, 3 } },
	{ 0x0600004B, { 168, 3 } },
	{ 0x06000071, { 200, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[264] = 
{
	{ (Il2CppRGCTXDataType)2, 22202 },
	{ (Il2CppRGCTXDataType)3, 16209 },
	{ (Il2CppRGCTXDataType)2, 22203 },
	{ (Il2CppRGCTXDataType)2, 22204 },
	{ (Il2CppRGCTXDataType)3, 16210 },
	{ (Il2CppRGCTXDataType)2, 22205 },
	{ (Il2CppRGCTXDataType)2, 22206 },
	{ (Il2CppRGCTXDataType)3, 16211 },
	{ (Il2CppRGCTXDataType)2, 22207 },
	{ (Il2CppRGCTXDataType)3, 16212 },
	{ (Il2CppRGCTXDataType)2, 22208 },
	{ (Il2CppRGCTXDataType)3, 16213 },
	{ (Il2CppRGCTXDataType)2, 22209 },
	{ (Il2CppRGCTXDataType)2, 22210 },
	{ (Il2CppRGCTXDataType)3, 16214 },
	{ (Il2CppRGCTXDataType)2, 22211 },
	{ (Il2CppRGCTXDataType)2, 22212 },
	{ (Il2CppRGCTXDataType)3, 16215 },
	{ (Il2CppRGCTXDataType)2, 22213 },
	{ (Il2CppRGCTXDataType)3, 16216 },
	{ (Il2CppRGCTXDataType)3, 16217 },
	{ (Il2CppRGCTXDataType)2, 22214 },
	{ (Il2CppRGCTXDataType)3, 16218 },
	{ (Il2CppRGCTXDataType)2, 22215 },
	{ (Il2CppRGCTXDataType)3, 16219 },
	{ (Il2CppRGCTXDataType)3, 16220 },
	{ (Il2CppRGCTXDataType)2, 16313 },
	{ (Il2CppRGCTXDataType)3, 16221 },
	{ (Il2CppRGCTXDataType)2, 22216 },
	{ (Il2CppRGCTXDataType)3, 16222 },
	{ (Il2CppRGCTXDataType)3, 16223 },
	{ (Il2CppRGCTXDataType)2, 16320 },
	{ (Il2CppRGCTXDataType)3, 16224 },
	{ (Il2CppRGCTXDataType)2, 22217 },
	{ (Il2CppRGCTXDataType)3, 16225 },
	{ (Il2CppRGCTXDataType)3, 16226 },
	{ (Il2CppRGCTXDataType)2, 22218 },
	{ (Il2CppRGCTXDataType)3, 16227 },
	{ (Il2CppRGCTXDataType)3, 16228 },
	{ (Il2CppRGCTXDataType)2, 16335 },
	{ (Il2CppRGCTXDataType)3, 16229 },
	{ (Il2CppRGCTXDataType)3, 16230 },
	{ (Il2CppRGCTXDataType)2, 16350 },
	{ (Il2CppRGCTXDataType)3, 16231 },
	{ (Il2CppRGCTXDataType)2, 16343 },
	{ (Il2CppRGCTXDataType)2, 22219 },
	{ (Il2CppRGCTXDataType)3, 16232 },
	{ (Il2CppRGCTXDataType)3, 16233 },
	{ (Il2CppRGCTXDataType)3, 16234 },
	{ (Il2CppRGCTXDataType)3, 16235 },
	{ (Il2CppRGCTXDataType)2, 22220 },
	{ (Il2CppRGCTXDataType)3, 16236 },
	{ (Il2CppRGCTXDataType)2, 16355 },
	{ (Il2CppRGCTXDataType)3, 16237 },
	{ (Il2CppRGCTXDataType)2, 22221 },
	{ (Il2CppRGCTXDataType)3, 16238 },
	{ (Il2CppRGCTXDataType)2, 22222 },
	{ (Il2CppRGCTXDataType)2, 22223 },
	{ (Il2CppRGCTXDataType)2, 16359 },
	{ (Il2CppRGCTXDataType)2, 22224 },
	{ (Il2CppRGCTXDataType)2, 22225 },
	{ (Il2CppRGCTXDataType)2, 22226 },
	{ (Il2CppRGCTXDataType)2, 16361 },
	{ (Il2CppRGCTXDataType)2, 22227 },
	{ (Il2CppRGCTXDataType)2, 16363 },
	{ (Il2CppRGCTXDataType)2, 22228 },
	{ (Il2CppRGCTXDataType)3, 16239 },
	{ (Il2CppRGCTXDataType)2, 22229 },
	{ (Il2CppRGCTXDataType)2, 22230 },
	{ (Il2CppRGCTXDataType)2, 16366 },
	{ (Il2CppRGCTXDataType)2, 22231 },
	{ (Il2CppRGCTXDataType)2, 16368 },
	{ (Il2CppRGCTXDataType)2, 22232 },
	{ (Il2CppRGCTXDataType)3, 16240 },
	{ (Il2CppRGCTXDataType)2, 22233 },
	{ (Il2CppRGCTXDataType)2, 16371 },
	{ (Il2CppRGCTXDataType)2, 22234 },
	{ (Il2CppRGCTXDataType)2, 22235 },
	{ (Il2CppRGCTXDataType)2, 16375 },
	{ (Il2CppRGCTXDataType)2, 16377 },
	{ (Il2CppRGCTXDataType)2, 22236 },
	{ (Il2CppRGCTXDataType)3, 16241 },
	{ (Il2CppRGCTXDataType)2, 16380 },
	{ (Il2CppRGCTXDataType)2, 22237 },
	{ (Il2CppRGCTXDataType)3, 16242 },
	{ (Il2CppRGCTXDataType)2, 22238 },
	{ (Il2CppRGCTXDataType)2, 16383 },
	{ (Il2CppRGCTXDataType)2, 22239 },
	{ (Il2CppRGCTXDataType)3, 16243 },
	{ (Il2CppRGCTXDataType)3, 16244 },
	{ (Il2CppRGCTXDataType)2, 22240 },
	{ (Il2CppRGCTXDataType)2, 16387 },
	{ (Il2CppRGCTXDataType)2, 22241 },
	{ (Il2CppRGCTXDataType)2, 16389 },
	{ (Il2CppRGCTXDataType)3, 16245 },
	{ (Il2CppRGCTXDataType)3, 16246 },
	{ (Il2CppRGCTXDataType)2, 16392 },
	{ (Il2CppRGCTXDataType)3, 16247 },
	{ (Il2CppRGCTXDataType)3, 16248 },
	{ (Il2CppRGCTXDataType)2, 16404 },
	{ (Il2CppRGCTXDataType)2, 22242 },
	{ (Il2CppRGCTXDataType)3, 16249 },
	{ (Il2CppRGCTXDataType)3, 16250 },
	{ (Il2CppRGCTXDataType)2, 16406 },
	{ (Il2CppRGCTXDataType)2, 22100 },
	{ (Il2CppRGCTXDataType)3, 16251 },
	{ (Il2CppRGCTXDataType)3, 16252 },
	{ (Il2CppRGCTXDataType)2, 22243 },
	{ (Il2CppRGCTXDataType)3, 16253 },
	{ (Il2CppRGCTXDataType)3, 16254 },
	{ (Il2CppRGCTXDataType)2, 16416 },
	{ (Il2CppRGCTXDataType)2, 22244 },
	{ (Il2CppRGCTXDataType)3, 16255 },
	{ (Il2CppRGCTXDataType)3, 16256 },
	{ (Il2CppRGCTXDataType)3, 15629 },
	{ (Il2CppRGCTXDataType)3, 16257 },
	{ (Il2CppRGCTXDataType)2, 22245 },
	{ (Il2CppRGCTXDataType)3, 16258 },
	{ (Il2CppRGCTXDataType)3, 16259 },
	{ (Il2CppRGCTXDataType)2, 16428 },
	{ (Il2CppRGCTXDataType)2, 22246 },
	{ (Il2CppRGCTXDataType)3, 16260 },
	{ (Il2CppRGCTXDataType)3, 16261 },
	{ (Il2CppRGCTXDataType)3, 16262 },
	{ (Il2CppRGCTXDataType)3, 16263 },
	{ (Il2CppRGCTXDataType)3, 16264 },
	{ (Il2CppRGCTXDataType)3, 15635 },
	{ (Il2CppRGCTXDataType)3, 16265 },
	{ (Il2CppRGCTXDataType)2, 22247 },
	{ (Il2CppRGCTXDataType)3, 16266 },
	{ (Il2CppRGCTXDataType)3, 16267 },
	{ (Il2CppRGCTXDataType)2, 16441 },
	{ (Il2CppRGCTXDataType)2, 22248 },
	{ (Il2CppRGCTXDataType)3, 16268 },
	{ (Il2CppRGCTXDataType)3, 16269 },
	{ (Il2CppRGCTXDataType)2, 16443 },
	{ (Il2CppRGCTXDataType)2, 22249 },
	{ (Il2CppRGCTXDataType)3, 16270 },
	{ (Il2CppRGCTXDataType)3, 16271 },
	{ (Il2CppRGCTXDataType)2, 22250 },
	{ (Il2CppRGCTXDataType)3, 16272 },
	{ (Il2CppRGCTXDataType)3, 16273 },
	{ (Il2CppRGCTXDataType)2, 22251 },
	{ (Il2CppRGCTXDataType)3, 16274 },
	{ (Il2CppRGCTXDataType)3, 16275 },
	{ (Il2CppRGCTXDataType)2, 16458 },
	{ (Il2CppRGCTXDataType)2, 22252 },
	{ (Il2CppRGCTXDataType)3, 16276 },
	{ (Il2CppRGCTXDataType)3, 16277 },
	{ (Il2CppRGCTXDataType)3, 16278 },
	{ (Il2CppRGCTXDataType)3, 15646 },
	{ (Il2CppRGCTXDataType)2, 22253 },
	{ (Il2CppRGCTXDataType)3, 16279 },
	{ (Il2CppRGCTXDataType)3, 16280 },
	{ (Il2CppRGCTXDataType)2, 22254 },
	{ (Il2CppRGCTXDataType)3, 16281 },
	{ (Il2CppRGCTXDataType)3, 16282 },
	{ (Il2CppRGCTXDataType)2, 16474 },
	{ (Il2CppRGCTXDataType)2, 22255 },
	{ (Il2CppRGCTXDataType)3, 16283 },
	{ (Il2CppRGCTXDataType)3, 16284 },
	{ (Il2CppRGCTXDataType)3, 16285 },
	{ (Il2CppRGCTXDataType)3, 16286 },
	{ (Il2CppRGCTXDataType)3, 16287 },
	{ (Il2CppRGCTXDataType)3, 16288 },
	{ (Il2CppRGCTXDataType)3, 15652 },
	{ (Il2CppRGCTXDataType)2, 22256 },
	{ (Il2CppRGCTXDataType)3, 16289 },
	{ (Il2CppRGCTXDataType)3, 16290 },
	{ (Il2CppRGCTXDataType)2, 22257 },
	{ (Il2CppRGCTXDataType)3, 16291 },
	{ (Il2CppRGCTXDataType)3, 16292 },
	{ (Il2CppRGCTXDataType)2, 22258 },
	{ (Il2CppRGCTXDataType)2, 22259 },
	{ (Il2CppRGCTXDataType)3, 16293 },
	{ (Il2CppRGCTXDataType)3, 16294 },
	{ (Il2CppRGCTXDataType)2, 16491 },
	{ (Il2CppRGCTXDataType)2, 22260 },
	{ (Il2CppRGCTXDataType)3, 16295 },
	{ (Il2CppRGCTXDataType)3, 16296 },
	{ (Il2CppRGCTXDataType)3, 16297 },
	{ (Il2CppRGCTXDataType)3, 16298 },
	{ (Il2CppRGCTXDataType)3, 16299 },
	{ (Il2CppRGCTXDataType)3, 16300 },
	{ (Il2CppRGCTXDataType)2, 16515 },
	{ (Il2CppRGCTXDataType)3, 16301 },
	{ (Il2CppRGCTXDataType)2, 22261 },
	{ (Il2CppRGCTXDataType)3, 16302 },
	{ (Il2CppRGCTXDataType)3, 16303 },
	{ (Il2CppRGCTXDataType)3, 16304 },
	{ (Il2CppRGCTXDataType)2, 16523 },
	{ (Il2CppRGCTXDataType)3, 16305 },
	{ (Il2CppRGCTXDataType)2, 22262 },
	{ (Il2CppRGCTXDataType)3, 16306 },
	{ (Il2CppRGCTXDataType)3, 16307 },
	{ (Il2CppRGCTXDataType)2, 22263 },
	{ (Il2CppRGCTXDataType)2, 22264 },
	{ (Il2CppRGCTXDataType)2, 22265 },
	{ (Il2CppRGCTXDataType)3, 16308 },
	{ (Il2CppRGCTXDataType)3, 16309 },
	{ (Il2CppRGCTXDataType)2, 22266 },
	{ (Il2CppRGCTXDataType)3, 16310 },
	{ (Il2CppRGCTXDataType)2, 22267 },
	{ (Il2CppRGCTXDataType)3, 16311 },
	{ (Il2CppRGCTXDataType)3, 16312 },
	{ (Il2CppRGCTXDataType)3, 16313 },
	{ (Il2CppRGCTXDataType)2, 16552 },
	{ (Il2CppRGCTXDataType)3, 16314 },
	{ (Il2CppRGCTXDataType)2, 16560 },
	{ (Il2CppRGCTXDataType)3, 16315 },
	{ (Il2CppRGCTXDataType)2, 22268 },
	{ (Il2CppRGCTXDataType)2, 22269 },
	{ (Il2CppRGCTXDataType)3, 16316 },
	{ (Il2CppRGCTXDataType)3, 16317 },
	{ (Il2CppRGCTXDataType)3, 16318 },
	{ (Il2CppRGCTXDataType)3, 16319 },
	{ (Il2CppRGCTXDataType)3, 16320 },
	{ (Il2CppRGCTXDataType)3, 16321 },
	{ (Il2CppRGCTXDataType)2, 16576 },
	{ (Il2CppRGCTXDataType)2, 22270 },
	{ (Il2CppRGCTXDataType)3, 16322 },
	{ (Il2CppRGCTXDataType)3, 16323 },
	{ (Il2CppRGCTXDataType)2, 16580 },
	{ (Il2CppRGCTXDataType)3, 16324 },
	{ (Il2CppRGCTXDataType)2, 22271 },
	{ (Il2CppRGCTXDataType)2, 16590 },
	{ (Il2CppRGCTXDataType)2, 16588 },
	{ (Il2CppRGCTXDataType)2, 22272 },
	{ (Il2CppRGCTXDataType)3, 16325 },
	{ (Il2CppRGCTXDataType)2, 22273 },
	{ (Il2CppRGCTXDataType)3, 16326 },
	{ (Il2CppRGCTXDataType)3, 16327 },
	{ (Il2CppRGCTXDataType)2, 16597 },
	{ (Il2CppRGCTXDataType)3, 16328 },
	{ (Il2CppRGCTXDataType)2, 16597 },
	{ (Il2CppRGCTXDataType)3, 16329 },
	{ (Il2CppRGCTXDataType)2, 16614 },
	{ (Il2CppRGCTXDataType)3, 16330 },
	{ (Il2CppRGCTXDataType)3, 16331 },
	{ (Il2CppRGCTXDataType)3, 16332 },
	{ (Il2CppRGCTXDataType)2, 22274 },
	{ (Il2CppRGCTXDataType)3, 16333 },
	{ (Il2CppRGCTXDataType)3, 16334 },
	{ (Il2CppRGCTXDataType)3, 16335 },
	{ (Il2CppRGCTXDataType)2, 16594 },
	{ (Il2CppRGCTXDataType)3, 16336 },
	{ (Il2CppRGCTXDataType)3, 16337 },
	{ (Il2CppRGCTXDataType)2, 16599 },
	{ (Il2CppRGCTXDataType)3, 16338 },
	{ (Il2CppRGCTXDataType)1, 22275 },
	{ (Il2CppRGCTXDataType)2, 16598 },
	{ (Il2CppRGCTXDataType)3, 16339 },
	{ (Il2CppRGCTXDataType)1, 16598 },
	{ (Il2CppRGCTXDataType)1, 16594 },
	{ (Il2CppRGCTXDataType)2, 22274 },
	{ (Il2CppRGCTXDataType)2, 16598 },
	{ (Il2CppRGCTXDataType)2, 16596 },
	{ (Il2CppRGCTXDataType)2, 16600 },
	{ (Il2CppRGCTXDataType)3, 16340 },
	{ (Il2CppRGCTXDataType)3, 16341 },
	{ (Il2CppRGCTXDataType)3, 16342 },
	{ (Il2CppRGCTXDataType)2, 16595 },
	{ (Il2CppRGCTXDataType)3, 16343 },
	{ (Il2CppRGCTXDataType)2, 16610 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	169,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	57,
	s_rgctxIndices,
	264,
	s_rgctxValues,
	NULL,
};
