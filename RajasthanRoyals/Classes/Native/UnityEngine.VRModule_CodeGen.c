﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean UnityEngine.XR.XRDevice::get_isPresent()
extern void XRDevice_get_isPresent_m5B3D1AC4D4D14CB1AEA0FC3625B6ADE05915DDA0 ();
// 0x00000002 System.Void UnityEngine.XR.XRDevice::InvokeDeviceLoaded(System.String)
extern void XRDevice_InvokeDeviceLoaded_mD5D5577A4E03D0474FAFBB2596B698B6A8B5FD11 ();
// 0x00000003 System.Void UnityEngine.XR.XRDevice::.cctor()
extern void XRDevice__cctor_m4FE111291FBDF43A481045CBABECF9AEC70B5EC9 ();
// 0x00000004 System.Boolean UnityEngine.XR.WSA.HolographicSettings::get_IsDisplayOpaque()
extern void HolographicSettings_get_IsDisplayOpaque_m1195AD8CB69591E29DEE780894136EEEA7D7A418 ();
static Il2CppMethodPointer s_methodPointers[4] = 
{
	XRDevice_get_isPresent_m5B3D1AC4D4D14CB1AEA0FC3625B6ADE05915DDA0,
	XRDevice_InvokeDeviceLoaded_mD5D5577A4E03D0474FAFBB2596B698B6A8B5FD11,
	XRDevice__cctor_m4FE111291FBDF43A481045CBABECF9AEC70B5EC9,
	HolographicSettings_get_IsDisplayOpaque_m1195AD8CB69591E29DEE780894136EEEA7D7A418,
};
static const int32_t s_InvokerIndices[4] = 
{
	49,
	122,
	3,
	49,
};
extern const Il2CppCodeGenModule g_UnityEngine_VRModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VRModuleCodeGenModule = 
{
	"UnityEngine.VRModule.dll",
	4,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
