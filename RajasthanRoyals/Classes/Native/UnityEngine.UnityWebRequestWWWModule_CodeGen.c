﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.WWW::.ctor(System.String,System.String,UnityEngine.Hash128,System.UInt32)
extern void WWW__ctor_mFA0F91AF10ED6792FCE0DC6A89F4AB3D308FA4C2 ();
// 0x00000002 UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,System.Int32)
extern void WWW_LoadFromCacheOrDownload_mB4C50AF1649A0976B8B354D793D945AC42CE4170 ();
// 0x00000003 UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,System.Int32,System.UInt32)
extern void WWW_LoadFromCacheOrDownload_m09928D23AFA50A3F33179B7B931D08949D43965C ();
// 0x00000004 UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128,System.UInt32)
extern void WWW_LoadFromCacheOrDownload_m1018CDF26B997EFA8AC848237A447D3409659870 ();
// 0x00000005 UnityEngine.AssetBundle UnityEngine.WWW::get_assetBundle()
extern void WWW_get_assetBundle_m3276DE6B6151D92A0B3E516D131386386053F864 ();
// 0x00000006 System.Byte[] UnityEngine.WWW::get_bytes()
extern void WWW_get_bytes_m9C9B2E62AC6733A39EDBF61E5C751438EC41126C ();
// 0x00000007 System.Single UnityEngine.WWW::get_progress()
extern void WWW_get_progress_m0C27444D041C224A9958FED51723BCA9F9FCDABF ();
// 0x00000008 System.String UnityEngine.WWW::get_url()
extern void WWW_get_url_m201062308E01D69B4AED10B7BBA6B345F8E42089 ();
// 0x00000009 System.Boolean UnityEngine.WWW::get_keepWaiting()
extern void WWW_get_keepWaiting_m2C52E54F48964EFD711C55A514E738CDF5D2EEB4 ();
// 0x0000000A System.Void UnityEngine.WWW::Dispose()
extern void WWW_Dispose_m481A801C09AEF9D4B7EE5BD680ECE80C0B1E0F66 ();
// 0x0000000B System.Boolean UnityEngine.WWW::WaitUntilDoneIfPossible()
extern void WWW_WaitUntilDoneIfPossible_mBCA182F05B2F71D4F1816951748AFAF1C4FFD5D0 ();
static Il2CppMethodPointer s_methodPointers[11] = 
{
	WWW__ctor_mFA0F91AF10ED6792FCE0DC6A89F4AB3D308FA4C2,
	WWW_LoadFromCacheOrDownload_mB4C50AF1649A0976B8B354D793D945AC42CE4170,
	WWW_LoadFromCacheOrDownload_m09928D23AFA50A3F33179B7B931D08949D43965C,
	WWW_LoadFromCacheOrDownload_m1018CDF26B997EFA8AC848237A447D3409659870,
	WWW_get_assetBundle_m3276DE6B6151D92A0B3E516D131386386053F864,
	WWW_get_bytes_m9C9B2E62AC6733A39EDBF61E5C751438EC41126C,
	WWW_get_progress_m0C27444D041C224A9958FED51723BCA9F9FCDABF,
	WWW_get_url_m201062308E01D69B4AED10B7BBA6B345F8E42089,
	WWW_get_keepWaiting_m2C52E54F48964EFD711C55A514E738CDF5D2EEB4,
	WWW_Dispose_m481A801C09AEF9D4B7EE5BD680ECE80C0B1E0F66,
	WWW_WaitUntilDoneIfPossible_mBCA182F05B2F71D4F1816951748AFAF1C4FFD5D0,
};
static const int32_t s_InvokerIndices[11] = 
{
	1510,
	164,
	163,
	1660,
	14,
	14,
	679,
	14,
	114,
	23,
	114,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestWWWModule.dll",
	11,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
