﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AssetBundle::.ctor()
extern void AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4 ();
// 0x00000002 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromMemory_Internal(System.Byte[],System.UInt32)
extern void AssetBundle_LoadFromMemory_Internal_m3854485EA17DAC51E79039B9BB71C474397CC674 ();
// 0x00000003 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromMemory(System.Byte[])
extern void AssetBundle_LoadFromMemory_m04117B6ACE5C67F4709BA321153CB584DB93CC52 ();
// 0x00000004 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String)
extern void AssetBundle_LoadAsset_m5D5EFC8B7A27AE31CCDA12F652664513F7BF8A29 ();
// 0x00000005 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String,System.Type)
extern void AssetBundle_LoadAsset_m36BA6CCFE773DFCF4490EB4910022B74BB6BF283 ();
// 0x00000006 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
extern void AssetBundle_LoadAsset_Internal_m7B24CA02317176FEADA41DE03CFF849D92B2392A ();
// 0x00000007 System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern void AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3 ();
static Il2CppMethodPointer s_methodPointers[7] = 
{
	AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4,
	AssetBundle_LoadFromMemory_Internal_m3854485EA17DAC51E79039B9BB71C474397CC674,
	AssetBundle_LoadFromMemory_m04117B6ACE5C67F4709BA321153CB584DB93CC52,
	AssetBundle_LoadAsset_m5D5EFC8B7A27AE31CCDA12F652664513F7BF8A29,
	AssetBundle_LoadAsset_m36BA6CCFE773DFCF4490EB4910022B74BB6BF283,
	AssetBundle_LoadAsset_Internal_m7B24CA02317176FEADA41DE03CFF849D92B2392A,
	AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3,
};
static const int32_t s_InvokerIndices[7] = 
{
	23,
	164,
	0,
	28,
	113,
	113,
	31,
};
extern const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	7,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
