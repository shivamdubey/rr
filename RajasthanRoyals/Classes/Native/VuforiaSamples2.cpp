﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// ContinuousGesture
struct ContinuousGesture_tA44EC65F086D6C9B7CBC53AAA6607A158407EA98;
// ContinuousGestureRecognizer`1<System.Object>
struct ContinuousGestureRecognizer_1_t2F998AA060B41D66917B55C04E45ACF5DD5D2F03;
// ContinuousGestureRecognizer`1<TwistGesture>
struct ContinuousGestureRecognizer_1_t5D48D6FBF95C0BE2E63E8CCE3BD2173CEAD947A3;
// FingerClusterManager
struct FingerClusterManager_t16E0EC9BEC723B183FF603EF03B44B6F64164411;
// FingerGestures/Finger
struct Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435;
// FingerGestures/FingerList
struct FingerList_t3FF8FE51353DF9B29A0A3B4A9E24205999A331E5;
// FingerGestures/Finger[]
struct FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C;
// FingerGestures/IFingerList
struct IFingerList_t09E148C8FA751B1E829E3AA97CD4DAC2EF6DB7A1;
// Gesture
struct Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD;
// Gesture/EventHandler
struct EventHandler_tB9FFE1B33272D79B3EC8911DECCFB134311664F5;
// GestureRecognizer
struct GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409;
// GestureRecognizerDelegate
struct GestureRecognizerDelegate_t3A639EBACB0FB218573197B53DE850ED76AEB817;
// GestureRecognizer`1/GestureEventHandler<TwistGesture>
struct GestureEventHandler_t89F6412E29BCC50D7A15211D9EFE5DB9B187CE9C;
// GestureRecognizer`1<System.Object>
struct GestureRecognizer_1_t4F4DBAD6EA16DC12D158BD385245E1729F0FA36C;
// GestureRecognizer`1<TwistGesture>
struct GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7;
// JKLaxmiProTracker
struct JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B;
// JKLaxmiTracker
struct JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56;
// NativeGallery/MediaSaveCallback
struct MediaSaveCallback_t489912AD9BCB0FD2B00ADCC25F89B8715D2F219E;
// RRLogoNewTracker
struct RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C;
// RRLogoOldTracker
struct RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6;
// ScreenRaycaster
struct ScreenRaycaster_tD10F7BB06DA9839FDDC7CE3B21CEA6D698A78F2E;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA;
// System.Collections.Generic.ICollection`1<System.Action`1<Vuforia.TrackableBehaviour/StatusChangeResult>>
struct ICollection_1_t2F4FA45DD22999A1539BE136FF09F0ADD3399002;
// System.Collections.Generic.ICollection`1<System.Action`1<Vuforia.TrackableBehaviour/StatusInfoChangeResult>>
struct ICollection_1_t660EB7B2F3F89D4C2C786552DD91205E09886CBC;
// System.Collections.Generic.List`1<GestureRecognizer>
struct List_1_t7F1C7D71AD9020276386F8397335C9062E3017FD;
// System.Collections.Generic.List`1<TwistGesture>
struct List_1_t08436B744622AC4133A3FF725BC02613EE626B3C;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t4654F3D4310BED7B5F902B36EB09610B09E6AF5E;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TBOrbit
struct TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638;
// TrackEventHandlerRRLogoNew
struct TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601;
// TrackEventHandlerRRLogoOld
struct TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC;
// TrackEventHandlerWorldCupOne
struct TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF;
// TrackEventHandlerWorldCupTwo
struct TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05;
// TwistGesture
struct TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1;
// TwistRecognizer
struct TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F;
// UI_Handler
struct UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0;
// UI_Handler/<TakeScreenshotAndSave>d__12
struct U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A;
// UnityEngine.Animation
struct Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE;
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Renderer[]
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t9460928CD061E83FD8942A0ED1865D2451A5E248;
// Vuforia.Trackable
struct Trackable_t2A23C572321E7D4FEAC9A1019DFA0AA144FC9B8F;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4;
// WorldCupOneTracker
struct WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9;
// WorldCupTwoTracker
struct WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF;

IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FingerGestures_t9DE618166E636E7EC25F553F0E3B01AA6513C95A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IFingerList_t09E148C8FA751B1E829E3AA97CD4DAC2EF6DB7A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NativeGallery_tC3173DEEAA2D55A1D49692A7842083EF31CD702B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Permission_t4CB85837235D3D73E44E0937DFBD99BC7D466C91_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral015932202FD61E451C6F831D0D0535A969937B1D;
IL2CPP_EXTERN_C String_t* _stringLiteral1163609AE835154467B744B94AEDBF951BC71257;
IL2CPP_EXTERN_C String_t* _stringLiteral19240DE083AA9C9CB77116EF24DE8D203AB3A53A;
IL2CPP_EXTERN_C String_t* _stringLiteral2338BEB372E67F5564A8B8066F54718A3E9E3155;
IL2CPP_EXTERN_C String_t* _stringLiteral5F1E3AFE2CF259DD3F9C6FB6F22932BD11509FCD;
IL2CPP_EXTERN_C String_t* _stringLiteral73E1AF46B7D5AACD372A6870A6845EBECF861716;
IL2CPP_EXTERN_C String_t* _stringLiteral74D8330FF7EE2BF01889D92730C9FB1CC78C788F;
IL2CPP_EXTERN_C String_t* _stringLiteral760D542CE497A54CB36ACFB2A671DD229132CF03;
IL2CPP_EXTERN_C String_t* _stringLiteral9E253470C876EE6D5C720EB777AEB82D4C26E28F;
IL2CPP_EXTERN_C String_t* _stringLiteralB3034EC567C2321A383D655A57D2CB638F893719;
IL2CPP_EXTERN_C String_t* _stringLiteralD8F3DAF68846C2D999956AB061674C6E36560643;
IL2CPP_EXTERN_C String_t* _stringLiteralE53A10BF18D590A4D354358810000744DAF6ACFD;
IL2CPP_EXTERN_C String_t* _stringLiteralEABCFFD8D5120B660823E2C294A8DC252DA5EA29;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisTrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_m81E3785465C5B36522D217B045A33CD65B28B229_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ContinuousGestureRecognizer_1__ctor_m12C4DCFD6DFA8F812F6177460F971C36A304DD81_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GestureRecognizer_1_CanBegin_m7450DF4EDF69CE6F64B046BA547843D4C37A68E6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GestureRecognizer_1_RaiseEvent_mAFE9CABB4E3EEBB3535EB148C18051F460CDF538_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_IEnumerator_Reset_mCD6C81342223466277E49C045FF45BBCBEA58DC5_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerRRLogoNew_OnDestroy_mCD220749E7704E7BFDA5701A62804892057EC087_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerRRLogoNew_OnTrackableStateChanged_m8AA8CCF260F1043C610DD65B06F4777E61DB1638_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerRRLogoNew_OnTrackingFound_mBDDD08EABE1605AB9A67DBF20961343BB2A24017_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerRRLogoNew_OnTrackingLost_m100882790426557C55F65F68302022BFDD191EAC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerRRLogoNew_Start_mF0D1118EF43D5B0F1EB8D68FA802019E08A795D3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerRRLogoOld_OnDestroy_m102F95DC449DD4351D0A532ED6EF263FE43372F0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerRRLogoOld_OnTrackableStateChanged_mD470EFA12C2E52811E672D6431F86A1E41991775_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerRRLogoOld_OnTrackingFound_m93965E7DF89A900BAC9EA0FE657FD639742F0AAA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerRRLogoOld_OnTrackingLost_m54635C86D7FE050BD8E04E8E92E74D8740EE22E0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerRRLogoOld_Start_m58028B9882621D12B328D569AAF0F5E4A6F688A1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerWorldCupOne_OnDestroy_mF06C868CA69A637B1AA3CB7701ADFEFA422D3D2E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerWorldCupOne_OnTrackableStateChanged_m4C9476EF61E220726561BB6AFAAC1C85DDB3ED75_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerWorldCupOne_OnTrackingFound_mA67BFB5DC6CE2CCA3124747452D5B95267704978_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerWorldCupOne_OnTrackingLost_mF12C20F6775598BDF424C544B2CC27B2E701A537_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerWorldCupOne_Start_m241FE993871965BAD0239A91B41123B674F39C10_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerWorldCupTwo_OnDestroy_m75A791E319CF636B146FEE537B7EC5DD2682F459_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerWorldCupTwo_OnTrackableStateChanged_m843B9D305A7208AE58172D58496CAC78C82CDC79_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerWorldCupTwo_OnTrackingFound_m73585600AC2FCA0B8E997C502683E32EB56AA886_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerWorldCupTwo_OnTrackingLost_mD6349AD5435B342536208AFCB67EBF5301FB3639_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackEventHandlerWorldCupTwo_Start_m2AB8752DAB38401B5C82C1DADB5B97A926D31589_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwistRecognizer_CanBegin_m9F5ADD27AE0C1A65E6D6BDD242D73E62CBB6047B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwistRecognizer_FingersMovedInOppositeDirections_mE1669F2E73404178083495D10D961C67B3458FF8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwistRecognizer_GetDefaultEventMessageName_mF7786153A5B741E37848E587DBA2F39034E6C644_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwistRecognizer_OnBegin_mE7C52F0A5B8676E76F0219F3276847BE8C7945D1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwistRecognizer_OnRecognize_m006006723683B0DFB53AF9FB04EFA41BD568BDB4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwistRecognizer_SignedAngularGap_m41B87BFF693B4DF4B3D949B493C38045099DF6A8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwistRecognizer__ctor_m77C8BF5B07685C6D3E94D2BCDACC01ECC43101EA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwistRecognizer_set_RequiredFingerCount_m0238672B86CB924044FEB96F6B5ADFAB8DC709A0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CTakeScreenshotAndSaveU3Ed__12_MoveNext_mCDF28BC374BEF35FA986F261A6A918F61045D239_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_IEnumerator_Reset_mCD6C81342223466277E49C045FF45BBCBEA58DC5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UI_Handler_Awake_mC9DFFBFCEF18031AEEDF0E8850A16105E196A0CB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UI_Handler_On3DSceneClick_m2A5ACCDB7DF4E4E4E4EA738A416378C8FCBF4FD5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UI_Handler_OnARSceneClick_mE5C8B143B9FED7FBA703D1393A82E8E884E80986_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UI_Handler_OnHomeClick_m47FDD3426389FF4F3AFCCCF2C842A497399C5890_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UI_Handler_OnReset_mA7ED1BCBEE72E36143BEA8259BEC3E54E5B1F6E6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UI_Handler_TakeScreenshotAndSave_m91BEA244D8F332A3C3B835E4612CA4BE4A11F68F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldCupOneTracker_OnPlayerAnimationButtonClick_m10B4471F1D9E2524485619C04F6BA877895DE9B3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldCupOneTracker_OnPlayerButtonClick_m1F83130B2F2EEE2A83A3B08F39032537DF0B287A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldCupOneTracker_OnPlayerRotateButtonClick_mE413E604F73A811ABB1D44A5C2DDF34ECA4FE552_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldCupOneTracker_Start_mF6C593BBEAE553C9200ACF0FBA90094169A05DCF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldCupTwoTracker_OnPlayerAnimationButtonClick_mAB802B57334169446EB7DD3CFDCB0D124318C5C4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldCupTwoTracker_OnPlayerButtonClick_m299722C1E95EB77FFD9AB56953380C98189CC3D6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldCupTwoTracker_OnPlayerRotateButtonClick_mC46776A5E869E613CDEBB160EF7398C216481237_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldCupTwoTracker_Start_m994B58169EAB8550B3320024CFDA6C4825103467_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
struct CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129;
struct ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A;
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UI_Handler_<TakeScreenshotAndSave>d__12
struct  U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A  : public RuntimeObject
{
public:
	// System.Int32 UI_Handler_<TakeScreenshotAndSave>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UI_Handler_<TakeScreenshotAndSave>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}
};


// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Mathf
struct  Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB__padding[1];
	};

public:
};

struct Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_StaticFields
{
public:
	// System.Single UnityEngine.Mathf::Epsilon
	float ___Epsilon_0;

public:
	inline static int32_t get_offset_of_Epsilon_0() { return static_cast<int32_t>(offsetof(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_StaticFields, ___Epsilon_0)); }
	inline float get_Epsilon_0() const { return ___Epsilon_0; }
	inline float* get_address_of_Epsilon_0() { return &___Epsilon_0; }
	inline void set_Epsilon_0(float value)
	{
		___Epsilon_0 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:

public:
};


// FingerGestures_FingerPhase
struct  FingerPhase_t572DC60F7D4847861CA994EEA35E0934689B7FC2 
{
public:
	// System.Int32 FingerGestures_FingerPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FingerPhase_t572DC60F7D4847861CA994EEA35E0934689B7FC2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GestureRecognitionState
struct  GestureRecognitionState_t8A62645DF1E0B36B44099BC6949E1C5952433E12 
{
public:
	// System.Int32 GestureRecognitionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GestureRecognitionState_t8A62645DF1E0B36B44099BC6949E1C5952433E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GestureRecognizer_SelectionType
struct  SelectionType_t3E160B88C25BAD6C8AB2C11ADAC6568A65131262 
{
public:
	// System.Int32 GestureRecognizer_SelectionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionType_t3E160B88C25BAD6C8AB2C11ADAC6568A65131262, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GestureResetMode
struct  GestureResetMode_tB08BC8DF349A6B49183AC378C2ADE8A48C0006C3 
{
public:
	// System.Int32 GestureResetMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GestureResetMode_tB08BC8DF349A6B49183AC378C2ADE8A48C0006C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NativeGallery_Permission
struct  Permission_t4CB85837235D3D73E44E0937DFBD99BC7D466C91 
{
public:
	// System.Int32 NativeGallery_Permission::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Permission_t4CB85837235D3D73E44E0937DFBD99BC7D466C91, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t75F0B96794398942671B8315D2A9AC25C40A22D5 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneMode_t75F0B96794398942671B8315D2A9AC25C40A22D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureFormat
struct  TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.TrackableBehaviour_Status
struct  Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.TrackableBehaviour_StatusInfo
struct  StatusInfo_t5507FB8CC09640E7771385EBE27221431A2FEB4E 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_StatusInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StatusInfo_t5507FB8CC09640E7771385EBE27221431A2FEB4E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FingerGestures_Finger
struct  Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435  : public RuntimeObject
{
public:
	// System.Int32 FingerGestures_Finger::index
	int32_t ___index_0;
	// FingerGestures_FingerPhase FingerGestures_Finger::phase
	int32_t ___phase_1;
	// FingerGestures_FingerPhase FingerGestures_Finger::prevPhase
	int32_t ___prevPhase_2;
	// UnityEngine.Vector2 FingerGestures_Finger::pos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pos_3;
	// UnityEngine.Vector2 FingerGestures_Finger::startPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___startPos_4;
	// UnityEngine.Vector2 FingerGestures_Finger::prevPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___prevPos_5;
	// UnityEngine.Vector2 FingerGestures_Finger::deltaPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___deltaPos_6;
	// System.Single FingerGestures_Finger::startTime
	float ___startTime_7;
	// System.Single FingerGestures_Finger::lastMoveTime
	float ___lastMoveTime_8;
	// System.Single FingerGestures_Finger::distFromStart
	float ___distFromStart_9;
	// System.Boolean FingerGestures_Finger::moved
	bool ___moved_10;
	// System.Boolean FingerGestures_Finger::filteredOut
	bool ___filteredOut_11;
	// UnityEngine.Collider FingerGestures_Finger::collider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___collider_12;
	// UnityEngine.Collider FingerGestures_Finger::prevCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___prevCollider_13;
	// System.Single FingerGestures_Finger::elapsedTimeStationary
	float ___elapsedTimeStationary_14;
	// System.Collections.Generic.List`1<GestureRecognizer> FingerGestures_Finger::gestureRecognizers
	List_1_t7F1C7D71AD9020276386F8397335C9062E3017FD * ___gestureRecognizers_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> FingerGestures_Finger::extendedProperties
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___extendedProperties_16;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_phase_1() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___phase_1)); }
	inline int32_t get_phase_1() const { return ___phase_1; }
	inline int32_t* get_address_of_phase_1() { return &___phase_1; }
	inline void set_phase_1(int32_t value)
	{
		___phase_1 = value;
	}

	inline static int32_t get_offset_of_prevPhase_2() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___prevPhase_2)); }
	inline int32_t get_prevPhase_2() const { return ___prevPhase_2; }
	inline int32_t* get_address_of_prevPhase_2() { return &___prevPhase_2; }
	inline void set_prevPhase_2(int32_t value)
	{
		___prevPhase_2 = value;
	}

	inline static int32_t get_offset_of_pos_3() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___pos_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_pos_3() const { return ___pos_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_pos_3() { return &___pos_3; }
	inline void set_pos_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___pos_3 = value;
	}

	inline static int32_t get_offset_of_startPos_4() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___startPos_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_startPos_4() const { return ___startPos_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_startPos_4() { return &___startPos_4; }
	inline void set_startPos_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___startPos_4 = value;
	}

	inline static int32_t get_offset_of_prevPos_5() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___prevPos_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_prevPos_5() const { return ___prevPos_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_prevPos_5() { return &___prevPos_5; }
	inline void set_prevPos_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___prevPos_5 = value;
	}

	inline static int32_t get_offset_of_deltaPos_6() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___deltaPos_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_deltaPos_6() const { return ___deltaPos_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_deltaPos_6() { return &___deltaPos_6; }
	inline void set_deltaPos_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___deltaPos_6 = value;
	}

	inline static int32_t get_offset_of_startTime_7() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___startTime_7)); }
	inline float get_startTime_7() const { return ___startTime_7; }
	inline float* get_address_of_startTime_7() { return &___startTime_7; }
	inline void set_startTime_7(float value)
	{
		___startTime_7 = value;
	}

	inline static int32_t get_offset_of_lastMoveTime_8() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___lastMoveTime_8)); }
	inline float get_lastMoveTime_8() const { return ___lastMoveTime_8; }
	inline float* get_address_of_lastMoveTime_8() { return &___lastMoveTime_8; }
	inline void set_lastMoveTime_8(float value)
	{
		___lastMoveTime_8 = value;
	}

	inline static int32_t get_offset_of_distFromStart_9() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___distFromStart_9)); }
	inline float get_distFromStart_9() const { return ___distFromStart_9; }
	inline float* get_address_of_distFromStart_9() { return &___distFromStart_9; }
	inline void set_distFromStart_9(float value)
	{
		___distFromStart_9 = value;
	}

	inline static int32_t get_offset_of_moved_10() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___moved_10)); }
	inline bool get_moved_10() const { return ___moved_10; }
	inline bool* get_address_of_moved_10() { return &___moved_10; }
	inline void set_moved_10(bool value)
	{
		___moved_10 = value;
	}

	inline static int32_t get_offset_of_filteredOut_11() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___filteredOut_11)); }
	inline bool get_filteredOut_11() const { return ___filteredOut_11; }
	inline bool* get_address_of_filteredOut_11() { return &___filteredOut_11; }
	inline void set_filteredOut_11(bool value)
	{
		___filteredOut_11 = value;
	}

	inline static int32_t get_offset_of_collider_12() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___collider_12)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_collider_12() const { return ___collider_12; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_collider_12() { return &___collider_12; }
	inline void set_collider_12(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___collider_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collider_12), (void*)value);
	}

	inline static int32_t get_offset_of_prevCollider_13() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___prevCollider_13)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_prevCollider_13() const { return ___prevCollider_13; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_prevCollider_13() { return &___prevCollider_13; }
	inline void set_prevCollider_13(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___prevCollider_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prevCollider_13), (void*)value);
	}

	inline static int32_t get_offset_of_elapsedTimeStationary_14() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___elapsedTimeStationary_14)); }
	inline float get_elapsedTimeStationary_14() const { return ___elapsedTimeStationary_14; }
	inline float* get_address_of_elapsedTimeStationary_14() { return &___elapsedTimeStationary_14; }
	inline void set_elapsedTimeStationary_14(float value)
	{
		___elapsedTimeStationary_14 = value;
	}

	inline static int32_t get_offset_of_gestureRecognizers_15() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___gestureRecognizers_15)); }
	inline List_1_t7F1C7D71AD9020276386F8397335C9062E3017FD * get_gestureRecognizers_15() const { return ___gestureRecognizers_15; }
	inline List_1_t7F1C7D71AD9020276386F8397335C9062E3017FD ** get_address_of_gestureRecognizers_15() { return &___gestureRecognizers_15; }
	inline void set_gestureRecognizers_15(List_1_t7F1C7D71AD9020276386F8397335C9062E3017FD * value)
	{
		___gestureRecognizers_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gestureRecognizers_15), (void*)value);
	}

	inline static int32_t get_offset_of_extendedProperties_16() { return static_cast<int32_t>(offsetof(Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435, ___extendedProperties_16)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_extendedProperties_16() const { return ___extendedProperties_16; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_extendedProperties_16() { return &___extendedProperties_16; }
	inline void set_extendedProperties_16(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___extendedProperties_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extendedProperties_16), (void*)value);
	}
};


// Gesture
struct  Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD  : public RuntimeObject
{
public:
	// Gesture_EventHandler Gesture::OnStateChanged
	EventHandler_tB9FFE1B33272D79B3EC8911DECCFB134311664F5 * ___OnStateChanged_0;
	// System.Int32 Gesture::ClusterId
	int32_t ___ClusterId_1;
	// GestureRecognizer Gesture::recognizer
	GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409 * ___recognizer_2;
	// System.Single Gesture::startTime
	float ___startTime_3;
	// UnityEngine.Vector2 Gesture::startPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___startPosition_4;
	// UnityEngine.Vector2 Gesture::position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position_5;
	// GestureRecognitionState Gesture::state
	int32_t ___state_6;
	// GestureRecognitionState Gesture::prevState
	int32_t ___prevState_7;
	// FingerGestures_FingerList Gesture::fingers
	FingerList_t3FF8FE51353DF9B29A0A3B4A9E24205999A331E5 * ___fingers_8;
	// UnityEngine.GameObject Gesture::startSelection
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___startSelection_9;
	// UnityEngine.GameObject Gesture::selection
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___selection_10;
	// UnityEngine.RaycastHit Gesture::lastHit
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  ___lastHit_11;

public:
	inline static int32_t get_offset_of_OnStateChanged_0() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___OnStateChanged_0)); }
	inline EventHandler_tB9FFE1B33272D79B3EC8911DECCFB134311664F5 * get_OnStateChanged_0() const { return ___OnStateChanged_0; }
	inline EventHandler_tB9FFE1B33272D79B3EC8911DECCFB134311664F5 ** get_address_of_OnStateChanged_0() { return &___OnStateChanged_0; }
	inline void set_OnStateChanged_0(EventHandler_tB9FFE1B33272D79B3EC8911DECCFB134311664F5 * value)
	{
		___OnStateChanged_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStateChanged_0), (void*)value);
	}

	inline static int32_t get_offset_of_ClusterId_1() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___ClusterId_1)); }
	inline int32_t get_ClusterId_1() const { return ___ClusterId_1; }
	inline int32_t* get_address_of_ClusterId_1() { return &___ClusterId_1; }
	inline void set_ClusterId_1(int32_t value)
	{
		___ClusterId_1 = value;
	}

	inline static int32_t get_offset_of_recognizer_2() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___recognizer_2)); }
	inline GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409 * get_recognizer_2() const { return ___recognizer_2; }
	inline GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409 ** get_address_of_recognizer_2() { return &___recognizer_2; }
	inline void set_recognizer_2(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409 * value)
	{
		___recognizer_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recognizer_2), (void*)value);
	}

	inline static int32_t get_offset_of_startTime_3() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___startTime_3)); }
	inline float get_startTime_3() const { return ___startTime_3; }
	inline float* get_address_of_startTime_3() { return &___startTime_3; }
	inline void set_startTime_3(float value)
	{
		___startTime_3 = value;
	}

	inline static int32_t get_offset_of_startPosition_4() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___startPosition_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_startPosition_4() const { return ___startPosition_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_startPosition_4() { return &___startPosition_4; }
	inline void set_startPosition_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___startPosition_4 = value;
	}

	inline static int32_t get_offset_of_position_5() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___position_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_position_5() const { return ___position_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_position_5() { return &___position_5; }
	inline void set_position_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___position_5 = value;
	}

	inline static int32_t get_offset_of_state_6() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___state_6)); }
	inline int32_t get_state_6() const { return ___state_6; }
	inline int32_t* get_address_of_state_6() { return &___state_6; }
	inline void set_state_6(int32_t value)
	{
		___state_6 = value;
	}

	inline static int32_t get_offset_of_prevState_7() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___prevState_7)); }
	inline int32_t get_prevState_7() const { return ___prevState_7; }
	inline int32_t* get_address_of_prevState_7() { return &___prevState_7; }
	inline void set_prevState_7(int32_t value)
	{
		___prevState_7 = value;
	}

	inline static int32_t get_offset_of_fingers_8() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___fingers_8)); }
	inline FingerList_t3FF8FE51353DF9B29A0A3B4A9E24205999A331E5 * get_fingers_8() const { return ___fingers_8; }
	inline FingerList_t3FF8FE51353DF9B29A0A3B4A9E24205999A331E5 ** get_address_of_fingers_8() { return &___fingers_8; }
	inline void set_fingers_8(FingerList_t3FF8FE51353DF9B29A0A3B4A9E24205999A331E5 * value)
	{
		___fingers_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fingers_8), (void*)value);
	}

	inline static int32_t get_offset_of_startSelection_9() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___startSelection_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_startSelection_9() const { return ___startSelection_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_startSelection_9() { return &___startSelection_9; }
	inline void set_startSelection_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___startSelection_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startSelection_9), (void*)value);
	}

	inline static int32_t get_offset_of_selection_10() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___selection_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_selection_10() const { return ___selection_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_selection_10() { return &___selection_10; }
	inline void set_selection_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___selection_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selection_10), (void*)value);
	}

	inline static int32_t get_offset_of_lastHit_11() { return static_cast<int32_t>(offsetof(Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD, ___lastHit_11)); }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  get_lastHit_11() const { return ___lastHit_11; }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * get_address_of_lastHit_11() { return &___lastHit_11; }
	inline void set_lastHit_11(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  value)
	{
		___lastHit_11 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// ContinuousGesture
struct  ContinuousGesture_tA44EC65F086D6C9B7CBC53AAA6607A158407EA98  : public Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD
{
public:

public:
};


// NativeGallery_MediaSaveCallback
struct  MediaSaveCallback_t489912AD9BCB0FD2B00ADCC25F89B8715D2F219E  : public MulticastDelegate_t
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Collider
struct  Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// TwistGesture
struct  TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1  : public ContinuousGesture_tA44EC65F086D6C9B7CBC53AAA6607A158407EA98
{
public:
	// System.Single TwistGesture::deltaRotation
	float ___deltaRotation_12;
	// System.Single TwistGesture::totalRotation
	float ___totalRotation_13;

public:
	inline static int32_t get_offset_of_deltaRotation_12() { return static_cast<int32_t>(offsetof(TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1, ___deltaRotation_12)); }
	inline float get_deltaRotation_12() const { return ___deltaRotation_12; }
	inline float* get_address_of_deltaRotation_12() { return &___deltaRotation_12; }
	inline void set_deltaRotation_12(float value)
	{
		___deltaRotation_12 = value;
	}

	inline static int32_t get_offset_of_totalRotation_13() { return static_cast<int32_t>(offsetof(TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1, ___totalRotation_13)); }
	inline float get_totalRotation_13() const { return ___totalRotation_13; }
	inline float* get_address_of_totalRotation_13() { return &___totalRotation_13; }
	inline void set_totalRotation_13(float value)
	{
		___totalRotation_13 = value;
	}
};


// UnityEngine.Animation
struct  Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.Canvas
struct  Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields
{
public:
	// UnityEngine.Canvas_WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * ___willRenderCanvases_4;

public:
	inline static int32_t get_offset_of_willRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields, ___willRenderCanvases_4)); }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * get_willRenderCanvases_4() const { return ___willRenderCanvases_4; }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE ** get_address_of_willRenderCanvases_4() { return &___willRenderCanvases_4; }
	inline void set_willRenderCanvases_4(WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * value)
	{
		___willRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___willRenderCanvases_4), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// GestureRecognizer
struct  GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 GestureRecognizer::requiredFingerCount
	int32_t ___requiredFingerCount_5;
	// System.Int32 GestureRecognizer::MaxSimultaneousGestures
	int32_t ___MaxSimultaneousGestures_6;
	// GestureResetMode GestureRecognizer::ResetMode
	int32_t ___ResetMode_7;
	// ScreenRaycaster GestureRecognizer::Raycaster
	ScreenRaycaster_tD10F7BB06DA9839FDDC7CE3B21CEA6D698A78F2E * ___Raycaster_8;
	// FingerClusterManager GestureRecognizer::ClusterManager
	FingerClusterManager_t16E0EC9BEC723B183FF603EF03B44B6F64164411 * ___ClusterManager_9;
	// GestureRecognizerDelegate GestureRecognizer::Delegate
	GestureRecognizerDelegate_t3A639EBACB0FB218573197B53DE850ED76AEB817 * ___Delegate_10;
	// System.Boolean GestureRecognizer::UseSendMessage
	bool ___UseSendMessage_11;
	// System.String GestureRecognizer::EventMessageName
	String_t* ___EventMessageName_12;
	// UnityEngine.GameObject GestureRecognizer::EventMessageTarget
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___EventMessageTarget_13;
	// GestureRecognizer_SelectionType GestureRecognizer::SendMessageToSelection
	int32_t ___SendMessageToSelection_14;
	// System.Boolean GestureRecognizer::IsExclusive
	bool ___IsExclusive_15;

public:
	inline static int32_t get_offset_of_requiredFingerCount_5() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___requiredFingerCount_5)); }
	inline int32_t get_requiredFingerCount_5() const { return ___requiredFingerCount_5; }
	inline int32_t* get_address_of_requiredFingerCount_5() { return &___requiredFingerCount_5; }
	inline void set_requiredFingerCount_5(int32_t value)
	{
		___requiredFingerCount_5 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousGestures_6() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___MaxSimultaneousGestures_6)); }
	inline int32_t get_MaxSimultaneousGestures_6() const { return ___MaxSimultaneousGestures_6; }
	inline int32_t* get_address_of_MaxSimultaneousGestures_6() { return &___MaxSimultaneousGestures_6; }
	inline void set_MaxSimultaneousGestures_6(int32_t value)
	{
		___MaxSimultaneousGestures_6 = value;
	}

	inline static int32_t get_offset_of_ResetMode_7() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___ResetMode_7)); }
	inline int32_t get_ResetMode_7() const { return ___ResetMode_7; }
	inline int32_t* get_address_of_ResetMode_7() { return &___ResetMode_7; }
	inline void set_ResetMode_7(int32_t value)
	{
		___ResetMode_7 = value;
	}

	inline static int32_t get_offset_of_Raycaster_8() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___Raycaster_8)); }
	inline ScreenRaycaster_tD10F7BB06DA9839FDDC7CE3B21CEA6D698A78F2E * get_Raycaster_8() const { return ___Raycaster_8; }
	inline ScreenRaycaster_tD10F7BB06DA9839FDDC7CE3B21CEA6D698A78F2E ** get_address_of_Raycaster_8() { return &___Raycaster_8; }
	inline void set_Raycaster_8(ScreenRaycaster_tD10F7BB06DA9839FDDC7CE3B21CEA6D698A78F2E * value)
	{
		___Raycaster_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Raycaster_8), (void*)value);
	}

	inline static int32_t get_offset_of_ClusterManager_9() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___ClusterManager_9)); }
	inline FingerClusterManager_t16E0EC9BEC723B183FF603EF03B44B6F64164411 * get_ClusterManager_9() const { return ___ClusterManager_9; }
	inline FingerClusterManager_t16E0EC9BEC723B183FF603EF03B44B6F64164411 ** get_address_of_ClusterManager_9() { return &___ClusterManager_9; }
	inline void set_ClusterManager_9(FingerClusterManager_t16E0EC9BEC723B183FF603EF03B44B6F64164411 * value)
	{
		___ClusterManager_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ClusterManager_9), (void*)value);
	}

	inline static int32_t get_offset_of_Delegate_10() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___Delegate_10)); }
	inline GestureRecognizerDelegate_t3A639EBACB0FB218573197B53DE850ED76AEB817 * get_Delegate_10() const { return ___Delegate_10; }
	inline GestureRecognizerDelegate_t3A639EBACB0FB218573197B53DE850ED76AEB817 ** get_address_of_Delegate_10() { return &___Delegate_10; }
	inline void set_Delegate_10(GestureRecognizerDelegate_t3A639EBACB0FB218573197B53DE850ED76AEB817 * value)
	{
		___Delegate_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Delegate_10), (void*)value);
	}

	inline static int32_t get_offset_of_UseSendMessage_11() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___UseSendMessage_11)); }
	inline bool get_UseSendMessage_11() const { return ___UseSendMessage_11; }
	inline bool* get_address_of_UseSendMessage_11() { return &___UseSendMessage_11; }
	inline void set_UseSendMessage_11(bool value)
	{
		___UseSendMessage_11 = value;
	}

	inline static int32_t get_offset_of_EventMessageName_12() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___EventMessageName_12)); }
	inline String_t* get_EventMessageName_12() const { return ___EventMessageName_12; }
	inline String_t** get_address_of_EventMessageName_12() { return &___EventMessageName_12; }
	inline void set_EventMessageName_12(String_t* value)
	{
		___EventMessageName_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EventMessageName_12), (void*)value);
	}

	inline static int32_t get_offset_of_EventMessageTarget_13() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___EventMessageTarget_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_EventMessageTarget_13() const { return ___EventMessageTarget_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_EventMessageTarget_13() { return &___EventMessageTarget_13; }
	inline void set_EventMessageTarget_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___EventMessageTarget_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EventMessageTarget_13), (void*)value);
	}

	inline static int32_t get_offset_of_SendMessageToSelection_14() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___SendMessageToSelection_14)); }
	inline int32_t get_SendMessageToSelection_14() const { return ___SendMessageToSelection_14; }
	inline int32_t* get_address_of_SendMessageToSelection_14() { return &___SendMessageToSelection_14; }
	inline void set_SendMessageToSelection_14(int32_t value)
	{
		___SendMessageToSelection_14 = value;
	}

	inline static int32_t get_offset_of_IsExclusive_15() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409, ___IsExclusive_15)); }
	inline bool get_IsExclusive_15() const { return ___IsExclusive_15; }
	inline bool* get_address_of_IsExclusive_15() { return &___IsExclusive_15; }
	inline void set_IsExclusive_15(bool value)
	{
		___IsExclusive_15 = value;
	}
};

struct GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409_StaticFields
{
public:
	// FingerGestures_IFingerList GestureRecognizer::EmptyFingerList
	RuntimeObject* ___EmptyFingerList_4;

public:
	inline static int32_t get_offset_of_EmptyFingerList_4() { return static_cast<int32_t>(offsetof(GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409_StaticFields, ___EmptyFingerList_4)); }
	inline RuntimeObject* get_EmptyFingerList_4() const { return ___EmptyFingerList_4; }
	inline RuntimeObject** get_address_of_EmptyFingerList_4() { return &___EmptyFingerList_4; }
	inline void set_EmptyFingerList_4(RuntimeObject* value)
	{
		___EmptyFingerList_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyFingerList_4), (void*)value);
	}
};


// JKLaxmiProTracker
struct  JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] JKLaxmiProTracker::TrackerName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___TrackerName_5;
	// UnityEngine.GameObject[] JKLaxmiProTracker::mainObject
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainObject_6;
	// UnityEngine.GameObject JKLaxmiProTracker::JKLaxmiUIPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___JKLaxmiUIPanel_7;
	// UnityEngine.GameObject[] JKLaxmiProTracker::mainRotateButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainRotateButton_8;
	// UnityEngine.GameObject[] JKLaxmiProTracker::animationButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___animationButton_9;

public:
	inline static int32_t get_offset_of_TrackerName_5() { return static_cast<int32_t>(offsetof(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B, ___TrackerName_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_TrackerName_5() const { return ___TrackerName_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_TrackerName_5() { return &___TrackerName_5; }
	inline void set_TrackerName_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___TrackerName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrackerName_5), (void*)value);
	}

	inline static int32_t get_offset_of_mainObject_6() { return static_cast<int32_t>(offsetof(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B, ___mainObject_6)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainObject_6() const { return ___mainObject_6; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainObject_6() { return &___mainObject_6; }
	inline void set_mainObject_6(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_JKLaxmiUIPanel_7() { return static_cast<int32_t>(offsetof(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B, ___JKLaxmiUIPanel_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_JKLaxmiUIPanel_7() const { return ___JKLaxmiUIPanel_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_JKLaxmiUIPanel_7() { return &___JKLaxmiUIPanel_7; }
	inline void set_JKLaxmiUIPanel_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___JKLaxmiUIPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JKLaxmiUIPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_mainRotateButton_8() { return static_cast<int32_t>(offsetof(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B, ___mainRotateButton_8)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainRotateButton_8() const { return ___mainRotateButton_8; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainRotateButton_8() { return &___mainRotateButton_8; }
	inline void set_mainRotateButton_8(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainRotateButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainRotateButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_animationButton_9() { return static_cast<int32_t>(offsetof(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B, ___animationButton_9)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_animationButton_9() const { return ___animationButton_9; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_animationButton_9() { return &___animationButton_9; }
	inline void set_animationButton_9(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___animationButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationButton_9), (void*)value);
	}
};

struct JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B_StaticFields
{
public:
	// JKLaxmiProTracker JKLaxmiProTracker::JKLaxmiProTrackerthis
	JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * ___JKLaxmiProTrackerthis_4;

public:
	inline static int32_t get_offset_of_JKLaxmiProTrackerthis_4() { return static_cast<int32_t>(offsetof(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B_StaticFields, ___JKLaxmiProTrackerthis_4)); }
	inline JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * get_JKLaxmiProTrackerthis_4() const { return ___JKLaxmiProTrackerthis_4; }
	inline JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B ** get_address_of_JKLaxmiProTrackerthis_4() { return &___JKLaxmiProTrackerthis_4; }
	inline void set_JKLaxmiProTrackerthis_4(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * value)
	{
		___JKLaxmiProTrackerthis_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JKLaxmiProTrackerthis_4), (void*)value);
	}
};


// JKLaxmiTracker
struct  JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] JKLaxmiTracker::TrackerName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___TrackerName_5;
	// UnityEngine.GameObject[] JKLaxmiTracker::mainObject
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainObject_6;
	// UnityEngine.GameObject JKLaxmiTracker::JKLaxmiUIPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___JKLaxmiUIPanel_7;
	// UnityEngine.GameObject[] JKLaxmiTracker::mainRotateButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainRotateButton_8;
	// UnityEngine.GameObject[] JKLaxmiTracker::animationButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___animationButton_9;

public:
	inline static int32_t get_offset_of_TrackerName_5() { return static_cast<int32_t>(offsetof(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56, ___TrackerName_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_TrackerName_5() const { return ___TrackerName_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_TrackerName_5() { return &___TrackerName_5; }
	inline void set_TrackerName_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___TrackerName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrackerName_5), (void*)value);
	}

	inline static int32_t get_offset_of_mainObject_6() { return static_cast<int32_t>(offsetof(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56, ___mainObject_6)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainObject_6() const { return ___mainObject_6; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainObject_6() { return &___mainObject_6; }
	inline void set_mainObject_6(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_JKLaxmiUIPanel_7() { return static_cast<int32_t>(offsetof(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56, ___JKLaxmiUIPanel_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_JKLaxmiUIPanel_7() const { return ___JKLaxmiUIPanel_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_JKLaxmiUIPanel_7() { return &___JKLaxmiUIPanel_7; }
	inline void set_JKLaxmiUIPanel_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___JKLaxmiUIPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JKLaxmiUIPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_mainRotateButton_8() { return static_cast<int32_t>(offsetof(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56, ___mainRotateButton_8)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainRotateButton_8() const { return ___mainRotateButton_8; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainRotateButton_8() { return &___mainRotateButton_8; }
	inline void set_mainRotateButton_8(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainRotateButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainRotateButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_animationButton_9() { return static_cast<int32_t>(offsetof(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56, ___animationButton_9)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_animationButton_9() const { return ___animationButton_9; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_animationButton_9() { return &___animationButton_9; }
	inline void set_animationButton_9(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___animationButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationButton_9), (void*)value);
	}
};

struct JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56_StaticFields
{
public:
	// JKLaxmiTracker JKLaxmiTracker::JKLaxmiTrackerthis
	JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * ___JKLaxmiTrackerthis_4;

public:
	inline static int32_t get_offset_of_JKLaxmiTrackerthis_4() { return static_cast<int32_t>(offsetof(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56_StaticFields, ___JKLaxmiTrackerthis_4)); }
	inline JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * get_JKLaxmiTrackerthis_4() const { return ___JKLaxmiTrackerthis_4; }
	inline JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 ** get_address_of_JKLaxmiTrackerthis_4() { return &___JKLaxmiTrackerthis_4; }
	inline void set_JKLaxmiTrackerthis_4(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * value)
	{
		___JKLaxmiTrackerthis_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JKLaxmiTrackerthis_4), (void*)value);
	}
};


// RRLogoNewTracker
struct  RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] RRLogoNewTracker::TrackerName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___TrackerName_5;
	// UnityEngine.GameObject[] RRLogoNewTracker::mainObject
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainObject_6;
	// UnityEngine.GameObject RRLogoNewTracker::JKLaxmiUIPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___JKLaxmiUIPanel_7;
	// UnityEngine.GameObject[] RRLogoNewTracker::mainRotateButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainRotateButton_8;
	// UnityEngine.GameObject[] RRLogoNewTracker::animationButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___animationButton_9;

public:
	inline static int32_t get_offset_of_TrackerName_5() { return static_cast<int32_t>(offsetof(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C, ___TrackerName_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_TrackerName_5() const { return ___TrackerName_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_TrackerName_5() { return &___TrackerName_5; }
	inline void set_TrackerName_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___TrackerName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrackerName_5), (void*)value);
	}

	inline static int32_t get_offset_of_mainObject_6() { return static_cast<int32_t>(offsetof(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C, ___mainObject_6)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainObject_6() const { return ___mainObject_6; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainObject_6() { return &___mainObject_6; }
	inline void set_mainObject_6(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_JKLaxmiUIPanel_7() { return static_cast<int32_t>(offsetof(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C, ___JKLaxmiUIPanel_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_JKLaxmiUIPanel_7() const { return ___JKLaxmiUIPanel_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_JKLaxmiUIPanel_7() { return &___JKLaxmiUIPanel_7; }
	inline void set_JKLaxmiUIPanel_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___JKLaxmiUIPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JKLaxmiUIPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_mainRotateButton_8() { return static_cast<int32_t>(offsetof(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C, ___mainRotateButton_8)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainRotateButton_8() const { return ___mainRotateButton_8; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainRotateButton_8() { return &___mainRotateButton_8; }
	inline void set_mainRotateButton_8(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainRotateButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainRotateButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_animationButton_9() { return static_cast<int32_t>(offsetof(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C, ___animationButton_9)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_animationButton_9() const { return ___animationButton_9; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_animationButton_9() { return &___animationButton_9; }
	inline void set_animationButton_9(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___animationButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationButton_9), (void*)value);
	}
};

struct RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C_StaticFields
{
public:
	// RRLogoNewTracker RRLogoNewTracker::RRLogoNewTrackerthis
	RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * ___RRLogoNewTrackerthis_4;

public:
	inline static int32_t get_offset_of_RRLogoNewTrackerthis_4() { return static_cast<int32_t>(offsetof(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C_StaticFields, ___RRLogoNewTrackerthis_4)); }
	inline RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * get_RRLogoNewTrackerthis_4() const { return ___RRLogoNewTrackerthis_4; }
	inline RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C ** get_address_of_RRLogoNewTrackerthis_4() { return &___RRLogoNewTrackerthis_4; }
	inline void set_RRLogoNewTrackerthis_4(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * value)
	{
		___RRLogoNewTrackerthis_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RRLogoNewTrackerthis_4), (void*)value);
	}
};


// RRLogoOldTracker
struct  RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] RRLogoOldTracker::TrackerName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___TrackerName_5;
	// UnityEngine.GameObject[] RRLogoOldTracker::mainObject
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainObject_6;
	// UnityEngine.GameObject RRLogoOldTracker::JKLaxmiUIPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___JKLaxmiUIPanel_7;
	// UnityEngine.GameObject[] RRLogoOldTracker::mainRotateButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainRotateButton_8;
	// UnityEngine.GameObject[] RRLogoOldTracker::animationButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___animationButton_9;

public:
	inline static int32_t get_offset_of_TrackerName_5() { return static_cast<int32_t>(offsetof(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6, ___TrackerName_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_TrackerName_5() const { return ___TrackerName_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_TrackerName_5() { return &___TrackerName_5; }
	inline void set_TrackerName_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___TrackerName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrackerName_5), (void*)value);
	}

	inline static int32_t get_offset_of_mainObject_6() { return static_cast<int32_t>(offsetof(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6, ___mainObject_6)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainObject_6() const { return ___mainObject_6; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainObject_6() { return &___mainObject_6; }
	inline void set_mainObject_6(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_JKLaxmiUIPanel_7() { return static_cast<int32_t>(offsetof(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6, ___JKLaxmiUIPanel_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_JKLaxmiUIPanel_7() const { return ___JKLaxmiUIPanel_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_JKLaxmiUIPanel_7() { return &___JKLaxmiUIPanel_7; }
	inline void set_JKLaxmiUIPanel_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___JKLaxmiUIPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JKLaxmiUIPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_mainRotateButton_8() { return static_cast<int32_t>(offsetof(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6, ___mainRotateButton_8)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainRotateButton_8() const { return ___mainRotateButton_8; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainRotateButton_8() { return &___mainRotateButton_8; }
	inline void set_mainRotateButton_8(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainRotateButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainRotateButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_animationButton_9() { return static_cast<int32_t>(offsetof(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6, ___animationButton_9)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_animationButton_9() const { return ___animationButton_9; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_animationButton_9() { return &___animationButton_9; }
	inline void set_animationButton_9(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___animationButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationButton_9), (void*)value);
	}
};

struct RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6_StaticFields
{
public:
	// RRLogoOldTracker RRLogoOldTracker::RRLogoOldTrackerthis
	RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * ___RRLogoOldTrackerthis_4;

public:
	inline static int32_t get_offset_of_RRLogoOldTrackerthis_4() { return static_cast<int32_t>(offsetof(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6_StaticFields, ___RRLogoOldTrackerthis_4)); }
	inline RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * get_RRLogoOldTrackerthis_4() const { return ___RRLogoOldTrackerthis_4; }
	inline RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 ** get_address_of_RRLogoOldTrackerthis_4() { return &___RRLogoOldTrackerthis_4; }
	inline void set_RRLogoOldTrackerthis_4(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * value)
	{
		___RRLogoOldTrackerthis_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RRLogoOldTrackerthis_4), (void*)value);
	}
};


// TBOrbit
struct  TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform TBOrbit::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_4;
	// System.Single TBOrbit::initialDistance
	float ___initialDistance_5;
	// System.Single TBOrbit::minDistance
	float ___minDistance_6;
	// System.Single TBOrbit::maxDistance
	float ___maxDistance_7;
	// System.Single TBOrbit::yawSensitivity
	float ___yawSensitivity_8;
	// System.Single TBOrbit::pitchSensitivity
	float ___pitchSensitivity_9;
	// System.Boolean TBOrbit::clampYawAngle
	bool ___clampYawAngle_10;
	// System.Single TBOrbit::minYaw
	float ___minYaw_11;
	// System.Single TBOrbit::maxYaw
	float ___maxYaw_12;
	// System.Boolean TBOrbit::clampPitchAngle
	bool ___clampPitchAngle_13;
	// System.Single TBOrbit::minPitch
	float ___minPitch_14;
	// System.Single TBOrbit::maxPitch
	float ___maxPitch_15;
	// System.Boolean TBOrbit::allowPinchZoom
	bool ___allowPinchZoom_16;
	// System.Single TBOrbit::pinchZoomSensitivity
	float ___pinchZoomSensitivity_17;
	// System.Boolean TBOrbit::smoothMotion
	bool ___smoothMotion_18;
	// System.Single TBOrbit::smoothZoomSpeed
	float ___smoothZoomSpeed_19;
	// System.Single TBOrbit::smoothOrbitSpeed
	float ___smoothOrbitSpeed_20;
	// System.Boolean TBOrbit::allowPanning
	bool ___allowPanning_21;
	// System.Boolean TBOrbit::invertPanningDirections
	bool ___invertPanningDirections_22;
	// System.Single TBOrbit::panningSensitivity
	float ___panningSensitivity_23;
	// UnityEngine.Transform TBOrbit::panningPlane
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___panningPlane_24;
	// System.Boolean TBOrbit::smoothPanning
	bool ___smoothPanning_25;
	// System.Single TBOrbit::smoothPanningSpeed
	float ___smoothPanningSpeed_26;
	// System.Single TBOrbit::distance
	float ___distance_27;
	// System.Single TBOrbit::yaw
	float ___yaw_28;
	// System.Single TBOrbit::pitch
	float ___pitch_29;
	// System.Single TBOrbit::idealDistance
	float ___idealDistance_30;
	// System.Single TBOrbit::idealYaw
	float ___idealYaw_31;
	// System.Single TBOrbit::idealPitch
	float ___idealPitch_32;
	// UnityEngine.Vector3 TBOrbit::idealPanOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___idealPanOffset_33;
	// UnityEngine.Vector3 TBOrbit::panOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___panOffset_34;
	// System.Single TBOrbit::nextDragTime
	float ___nextDragTime_35;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___target_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_4() const { return ___target_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_4), (void*)value);
	}

	inline static int32_t get_offset_of_initialDistance_5() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___initialDistance_5)); }
	inline float get_initialDistance_5() const { return ___initialDistance_5; }
	inline float* get_address_of_initialDistance_5() { return &___initialDistance_5; }
	inline void set_initialDistance_5(float value)
	{
		___initialDistance_5 = value;
	}

	inline static int32_t get_offset_of_minDistance_6() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___minDistance_6)); }
	inline float get_minDistance_6() const { return ___minDistance_6; }
	inline float* get_address_of_minDistance_6() { return &___minDistance_6; }
	inline void set_minDistance_6(float value)
	{
		___minDistance_6 = value;
	}

	inline static int32_t get_offset_of_maxDistance_7() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___maxDistance_7)); }
	inline float get_maxDistance_7() const { return ___maxDistance_7; }
	inline float* get_address_of_maxDistance_7() { return &___maxDistance_7; }
	inline void set_maxDistance_7(float value)
	{
		___maxDistance_7 = value;
	}

	inline static int32_t get_offset_of_yawSensitivity_8() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___yawSensitivity_8)); }
	inline float get_yawSensitivity_8() const { return ___yawSensitivity_8; }
	inline float* get_address_of_yawSensitivity_8() { return &___yawSensitivity_8; }
	inline void set_yawSensitivity_8(float value)
	{
		___yawSensitivity_8 = value;
	}

	inline static int32_t get_offset_of_pitchSensitivity_9() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___pitchSensitivity_9)); }
	inline float get_pitchSensitivity_9() const { return ___pitchSensitivity_9; }
	inline float* get_address_of_pitchSensitivity_9() { return &___pitchSensitivity_9; }
	inline void set_pitchSensitivity_9(float value)
	{
		___pitchSensitivity_9 = value;
	}

	inline static int32_t get_offset_of_clampYawAngle_10() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___clampYawAngle_10)); }
	inline bool get_clampYawAngle_10() const { return ___clampYawAngle_10; }
	inline bool* get_address_of_clampYawAngle_10() { return &___clampYawAngle_10; }
	inline void set_clampYawAngle_10(bool value)
	{
		___clampYawAngle_10 = value;
	}

	inline static int32_t get_offset_of_minYaw_11() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___minYaw_11)); }
	inline float get_minYaw_11() const { return ___minYaw_11; }
	inline float* get_address_of_minYaw_11() { return &___minYaw_11; }
	inline void set_minYaw_11(float value)
	{
		___minYaw_11 = value;
	}

	inline static int32_t get_offset_of_maxYaw_12() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___maxYaw_12)); }
	inline float get_maxYaw_12() const { return ___maxYaw_12; }
	inline float* get_address_of_maxYaw_12() { return &___maxYaw_12; }
	inline void set_maxYaw_12(float value)
	{
		___maxYaw_12 = value;
	}

	inline static int32_t get_offset_of_clampPitchAngle_13() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___clampPitchAngle_13)); }
	inline bool get_clampPitchAngle_13() const { return ___clampPitchAngle_13; }
	inline bool* get_address_of_clampPitchAngle_13() { return &___clampPitchAngle_13; }
	inline void set_clampPitchAngle_13(bool value)
	{
		___clampPitchAngle_13 = value;
	}

	inline static int32_t get_offset_of_minPitch_14() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___minPitch_14)); }
	inline float get_minPitch_14() const { return ___minPitch_14; }
	inline float* get_address_of_minPitch_14() { return &___minPitch_14; }
	inline void set_minPitch_14(float value)
	{
		___minPitch_14 = value;
	}

	inline static int32_t get_offset_of_maxPitch_15() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___maxPitch_15)); }
	inline float get_maxPitch_15() const { return ___maxPitch_15; }
	inline float* get_address_of_maxPitch_15() { return &___maxPitch_15; }
	inline void set_maxPitch_15(float value)
	{
		___maxPitch_15 = value;
	}

	inline static int32_t get_offset_of_allowPinchZoom_16() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___allowPinchZoom_16)); }
	inline bool get_allowPinchZoom_16() const { return ___allowPinchZoom_16; }
	inline bool* get_address_of_allowPinchZoom_16() { return &___allowPinchZoom_16; }
	inline void set_allowPinchZoom_16(bool value)
	{
		___allowPinchZoom_16 = value;
	}

	inline static int32_t get_offset_of_pinchZoomSensitivity_17() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___pinchZoomSensitivity_17)); }
	inline float get_pinchZoomSensitivity_17() const { return ___pinchZoomSensitivity_17; }
	inline float* get_address_of_pinchZoomSensitivity_17() { return &___pinchZoomSensitivity_17; }
	inline void set_pinchZoomSensitivity_17(float value)
	{
		___pinchZoomSensitivity_17 = value;
	}

	inline static int32_t get_offset_of_smoothMotion_18() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___smoothMotion_18)); }
	inline bool get_smoothMotion_18() const { return ___smoothMotion_18; }
	inline bool* get_address_of_smoothMotion_18() { return &___smoothMotion_18; }
	inline void set_smoothMotion_18(bool value)
	{
		___smoothMotion_18 = value;
	}

	inline static int32_t get_offset_of_smoothZoomSpeed_19() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___smoothZoomSpeed_19)); }
	inline float get_smoothZoomSpeed_19() const { return ___smoothZoomSpeed_19; }
	inline float* get_address_of_smoothZoomSpeed_19() { return &___smoothZoomSpeed_19; }
	inline void set_smoothZoomSpeed_19(float value)
	{
		___smoothZoomSpeed_19 = value;
	}

	inline static int32_t get_offset_of_smoothOrbitSpeed_20() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___smoothOrbitSpeed_20)); }
	inline float get_smoothOrbitSpeed_20() const { return ___smoothOrbitSpeed_20; }
	inline float* get_address_of_smoothOrbitSpeed_20() { return &___smoothOrbitSpeed_20; }
	inline void set_smoothOrbitSpeed_20(float value)
	{
		___smoothOrbitSpeed_20 = value;
	}

	inline static int32_t get_offset_of_allowPanning_21() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___allowPanning_21)); }
	inline bool get_allowPanning_21() const { return ___allowPanning_21; }
	inline bool* get_address_of_allowPanning_21() { return &___allowPanning_21; }
	inline void set_allowPanning_21(bool value)
	{
		___allowPanning_21 = value;
	}

	inline static int32_t get_offset_of_invertPanningDirections_22() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___invertPanningDirections_22)); }
	inline bool get_invertPanningDirections_22() const { return ___invertPanningDirections_22; }
	inline bool* get_address_of_invertPanningDirections_22() { return &___invertPanningDirections_22; }
	inline void set_invertPanningDirections_22(bool value)
	{
		___invertPanningDirections_22 = value;
	}

	inline static int32_t get_offset_of_panningSensitivity_23() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___panningSensitivity_23)); }
	inline float get_panningSensitivity_23() const { return ___panningSensitivity_23; }
	inline float* get_address_of_panningSensitivity_23() { return &___panningSensitivity_23; }
	inline void set_panningSensitivity_23(float value)
	{
		___panningSensitivity_23 = value;
	}

	inline static int32_t get_offset_of_panningPlane_24() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___panningPlane_24)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_panningPlane_24() const { return ___panningPlane_24; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_panningPlane_24() { return &___panningPlane_24; }
	inline void set_panningPlane_24(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___panningPlane_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___panningPlane_24), (void*)value);
	}

	inline static int32_t get_offset_of_smoothPanning_25() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___smoothPanning_25)); }
	inline bool get_smoothPanning_25() const { return ___smoothPanning_25; }
	inline bool* get_address_of_smoothPanning_25() { return &___smoothPanning_25; }
	inline void set_smoothPanning_25(bool value)
	{
		___smoothPanning_25 = value;
	}

	inline static int32_t get_offset_of_smoothPanningSpeed_26() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___smoothPanningSpeed_26)); }
	inline float get_smoothPanningSpeed_26() const { return ___smoothPanningSpeed_26; }
	inline float* get_address_of_smoothPanningSpeed_26() { return &___smoothPanningSpeed_26; }
	inline void set_smoothPanningSpeed_26(float value)
	{
		___smoothPanningSpeed_26 = value;
	}

	inline static int32_t get_offset_of_distance_27() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___distance_27)); }
	inline float get_distance_27() const { return ___distance_27; }
	inline float* get_address_of_distance_27() { return &___distance_27; }
	inline void set_distance_27(float value)
	{
		___distance_27 = value;
	}

	inline static int32_t get_offset_of_yaw_28() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___yaw_28)); }
	inline float get_yaw_28() const { return ___yaw_28; }
	inline float* get_address_of_yaw_28() { return &___yaw_28; }
	inline void set_yaw_28(float value)
	{
		___yaw_28 = value;
	}

	inline static int32_t get_offset_of_pitch_29() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___pitch_29)); }
	inline float get_pitch_29() const { return ___pitch_29; }
	inline float* get_address_of_pitch_29() { return &___pitch_29; }
	inline void set_pitch_29(float value)
	{
		___pitch_29 = value;
	}

	inline static int32_t get_offset_of_idealDistance_30() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___idealDistance_30)); }
	inline float get_idealDistance_30() const { return ___idealDistance_30; }
	inline float* get_address_of_idealDistance_30() { return &___idealDistance_30; }
	inline void set_idealDistance_30(float value)
	{
		___idealDistance_30 = value;
	}

	inline static int32_t get_offset_of_idealYaw_31() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___idealYaw_31)); }
	inline float get_idealYaw_31() const { return ___idealYaw_31; }
	inline float* get_address_of_idealYaw_31() { return &___idealYaw_31; }
	inline void set_idealYaw_31(float value)
	{
		___idealYaw_31 = value;
	}

	inline static int32_t get_offset_of_idealPitch_32() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___idealPitch_32)); }
	inline float get_idealPitch_32() const { return ___idealPitch_32; }
	inline float* get_address_of_idealPitch_32() { return &___idealPitch_32; }
	inline void set_idealPitch_32(float value)
	{
		___idealPitch_32 = value;
	}

	inline static int32_t get_offset_of_idealPanOffset_33() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___idealPanOffset_33)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_idealPanOffset_33() const { return ___idealPanOffset_33; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_idealPanOffset_33() { return &___idealPanOffset_33; }
	inline void set_idealPanOffset_33(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___idealPanOffset_33 = value;
	}

	inline static int32_t get_offset_of_panOffset_34() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___panOffset_34)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_panOffset_34() const { return ___panOffset_34; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_panOffset_34() { return &___panOffset_34; }
	inline void set_panOffset_34(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___panOffset_34 = value;
	}

	inline static int32_t get_offset_of_nextDragTime_35() { return static_cast<int32_t>(offsetof(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638, ___nextDragTime_35)); }
	inline float get_nextDragTime_35() const { return ___nextDragTime_35; }
	inline float* get_address_of_nextDragTime_35() { return &___nextDragTime_35; }
	inline void set_nextDragTime_35(float value)
	{
		___nextDragTime_35 = value;
	}
};


// TrackEventHandlerRRLogoNew
struct  TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vuforia.TrackableBehaviour TrackEventHandlerRRLogoNew::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour_Status TrackEventHandlerRRLogoNew::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour_Status TrackEventHandlerRRLogoNew::m_NewStatus
	int32_t ___m_NewStatus_6;
	// RRLogoNewTracker TrackEventHandlerRRLogoNew::rrlogoNew
	RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * ___rrlogoNew_7;
	// JKLaxmiTracker TrackEventHandlerRRLogoNew::jkL
	JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * ___jkL_8;
	// JKLaxmiProTracker TrackEventHandlerRRLogoNew::jklPro
	JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * ___jklPro_9;
	// RRLogoOldTracker TrackEventHandlerRRLogoNew::rrLogoOld
	RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * ___rrLogoOld_10;
	// WorldCupOneTracker TrackEventHandlerRRLogoNew::wcOne
	WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * ___wcOne_11;
	// WorldCupTwoTracker TrackEventHandlerRRLogoNew::wcTwo
	WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * ___wcTwo_12;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mTrackableBehaviour_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}

	inline static int32_t get_offset_of_rrlogoNew_7() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601, ___rrlogoNew_7)); }
	inline RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * get_rrlogoNew_7() const { return ___rrlogoNew_7; }
	inline RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C ** get_address_of_rrlogoNew_7() { return &___rrlogoNew_7; }
	inline void set_rrlogoNew_7(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * value)
	{
		___rrlogoNew_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rrlogoNew_7), (void*)value);
	}

	inline static int32_t get_offset_of_jkL_8() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601, ___jkL_8)); }
	inline JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * get_jkL_8() const { return ___jkL_8; }
	inline JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 ** get_address_of_jkL_8() { return &___jkL_8; }
	inline void set_jkL_8(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * value)
	{
		___jkL_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jkL_8), (void*)value);
	}

	inline static int32_t get_offset_of_jklPro_9() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601, ___jklPro_9)); }
	inline JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * get_jklPro_9() const { return ___jklPro_9; }
	inline JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B ** get_address_of_jklPro_9() { return &___jklPro_9; }
	inline void set_jklPro_9(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * value)
	{
		___jklPro_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jklPro_9), (void*)value);
	}

	inline static int32_t get_offset_of_rrLogoOld_10() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601, ___rrLogoOld_10)); }
	inline RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * get_rrLogoOld_10() const { return ___rrLogoOld_10; }
	inline RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 ** get_address_of_rrLogoOld_10() { return &___rrLogoOld_10; }
	inline void set_rrLogoOld_10(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * value)
	{
		___rrLogoOld_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rrLogoOld_10), (void*)value);
	}

	inline static int32_t get_offset_of_wcOne_11() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601, ___wcOne_11)); }
	inline WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * get_wcOne_11() const { return ___wcOne_11; }
	inline WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 ** get_address_of_wcOne_11() { return &___wcOne_11; }
	inline void set_wcOne_11(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * value)
	{
		___wcOne_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wcOne_11), (void*)value);
	}

	inline static int32_t get_offset_of_wcTwo_12() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601, ___wcTwo_12)); }
	inline WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * get_wcTwo_12() const { return ___wcTwo_12; }
	inline WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF ** get_address_of_wcTwo_12() { return &___wcTwo_12; }
	inline void set_wcTwo_12(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * value)
	{
		___wcTwo_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wcTwo_12), (void*)value);
	}
};


// TrackEventHandlerRRLogoOld
struct  TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vuforia.TrackableBehaviour TrackEventHandlerRRLogoOld::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour_Status TrackEventHandlerRRLogoOld::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour_Status TrackEventHandlerRRLogoOld::m_NewStatus
	int32_t ___m_NewStatus_6;
	// RRLogoOldTracker TrackEventHandlerRRLogoOld::rrlogoOld
	RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * ___rrlogoOld_7;
	// JKLaxmiTracker TrackEventHandlerRRLogoOld::jkL
	JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * ___jkL_8;
	// JKLaxmiProTracker TrackEventHandlerRRLogoOld::jklPro
	JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * ___jklPro_9;
	// RRLogoNewTracker TrackEventHandlerRRLogoOld::rrLogoNew
	RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * ___rrLogoNew_10;
	// WorldCupOneTracker TrackEventHandlerRRLogoOld::wcOne
	WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * ___wcOne_11;
	// WorldCupTwoTracker TrackEventHandlerRRLogoOld::wcTwo
	WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * ___wcTwo_12;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mTrackableBehaviour_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}

	inline static int32_t get_offset_of_rrlogoOld_7() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC, ___rrlogoOld_7)); }
	inline RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * get_rrlogoOld_7() const { return ___rrlogoOld_7; }
	inline RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 ** get_address_of_rrlogoOld_7() { return &___rrlogoOld_7; }
	inline void set_rrlogoOld_7(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * value)
	{
		___rrlogoOld_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rrlogoOld_7), (void*)value);
	}

	inline static int32_t get_offset_of_jkL_8() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC, ___jkL_8)); }
	inline JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * get_jkL_8() const { return ___jkL_8; }
	inline JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 ** get_address_of_jkL_8() { return &___jkL_8; }
	inline void set_jkL_8(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * value)
	{
		___jkL_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jkL_8), (void*)value);
	}

	inline static int32_t get_offset_of_jklPro_9() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC, ___jklPro_9)); }
	inline JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * get_jklPro_9() const { return ___jklPro_9; }
	inline JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B ** get_address_of_jklPro_9() { return &___jklPro_9; }
	inline void set_jklPro_9(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * value)
	{
		___jklPro_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jklPro_9), (void*)value);
	}

	inline static int32_t get_offset_of_rrLogoNew_10() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC, ___rrLogoNew_10)); }
	inline RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * get_rrLogoNew_10() const { return ___rrLogoNew_10; }
	inline RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C ** get_address_of_rrLogoNew_10() { return &___rrLogoNew_10; }
	inline void set_rrLogoNew_10(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * value)
	{
		___rrLogoNew_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rrLogoNew_10), (void*)value);
	}

	inline static int32_t get_offset_of_wcOne_11() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC, ___wcOne_11)); }
	inline WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * get_wcOne_11() const { return ___wcOne_11; }
	inline WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 ** get_address_of_wcOne_11() { return &___wcOne_11; }
	inline void set_wcOne_11(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * value)
	{
		___wcOne_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wcOne_11), (void*)value);
	}

	inline static int32_t get_offset_of_wcTwo_12() { return static_cast<int32_t>(offsetof(TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC, ___wcTwo_12)); }
	inline WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * get_wcTwo_12() const { return ___wcTwo_12; }
	inline WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF ** get_address_of_wcTwo_12() { return &___wcTwo_12; }
	inline void set_wcTwo_12(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * value)
	{
		___wcTwo_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wcTwo_12), (void*)value);
	}
};


// TrackEventHandlerWorldCupOne
struct  TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vuforia.TrackableBehaviour TrackEventHandlerWorldCupOne::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour_Status TrackEventHandlerWorldCupOne::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour_Status TrackEventHandlerWorldCupOne::m_NewStatus
	int32_t ___m_NewStatus_6;
	// RRLogoOldTracker TrackEventHandlerWorldCupOne::rrlogoOld
	RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * ___rrlogoOld_7;
	// JKLaxmiTracker TrackEventHandlerWorldCupOne::jkL
	JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * ___jkL_8;
	// JKLaxmiProTracker TrackEventHandlerWorldCupOne::jklPro
	JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * ___jklPro_9;
	// RRLogoNewTracker TrackEventHandlerWorldCupOne::rrLogoNew
	RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * ___rrLogoNew_10;
	// WorldCupOneTracker TrackEventHandlerWorldCupOne::wcOne
	WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * ___wcOne_11;
	// WorldCupTwoTracker TrackEventHandlerWorldCupOne::wcTwo
	WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * ___wcTwo_12;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mTrackableBehaviour_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}

	inline static int32_t get_offset_of_rrlogoOld_7() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF, ___rrlogoOld_7)); }
	inline RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * get_rrlogoOld_7() const { return ___rrlogoOld_7; }
	inline RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 ** get_address_of_rrlogoOld_7() { return &___rrlogoOld_7; }
	inline void set_rrlogoOld_7(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * value)
	{
		___rrlogoOld_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rrlogoOld_7), (void*)value);
	}

	inline static int32_t get_offset_of_jkL_8() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF, ___jkL_8)); }
	inline JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * get_jkL_8() const { return ___jkL_8; }
	inline JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 ** get_address_of_jkL_8() { return &___jkL_8; }
	inline void set_jkL_8(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * value)
	{
		___jkL_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jkL_8), (void*)value);
	}

	inline static int32_t get_offset_of_jklPro_9() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF, ___jklPro_9)); }
	inline JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * get_jklPro_9() const { return ___jklPro_9; }
	inline JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B ** get_address_of_jklPro_9() { return &___jklPro_9; }
	inline void set_jklPro_9(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * value)
	{
		___jklPro_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jklPro_9), (void*)value);
	}

	inline static int32_t get_offset_of_rrLogoNew_10() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF, ___rrLogoNew_10)); }
	inline RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * get_rrLogoNew_10() const { return ___rrLogoNew_10; }
	inline RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C ** get_address_of_rrLogoNew_10() { return &___rrLogoNew_10; }
	inline void set_rrLogoNew_10(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * value)
	{
		___rrLogoNew_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rrLogoNew_10), (void*)value);
	}

	inline static int32_t get_offset_of_wcOne_11() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF, ___wcOne_11)); }
	inline WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * get_wcOne_11() const { return ___wcOne_11; }
	inline WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 ** get_address_of_wcOne_11() { return &___wcOne_11; }
	inline void set_wcOne_11(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * value)
	{
		___wcOne_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wcOne_11), (void*)value);
	}

	inline static int32_t get_offset_of_wcTwo_12() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF, ___wcTwo_12)); }
	inline WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * get_wcTwo_12() const { return ___wcTwo_12; }
	inline WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF ** get_address_of_wcTwo_12() { return &___wcTwo_12; }
	inline void set_wcTwo_12(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * value)
	{
		___wcTwo_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wcTwo_12), (void*)value);
	}
};


// TrackEventHandlerWorldCupTwo
struct  TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vuforia.TrackableBehaviour TrackEventHandlerWorldCupTwo::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour_Status TrackEventHandlerWorldCupTwo::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour_Status TrackEventHandlerWorldCupTwo::m_NewStatus
	int32_t ___m_NewStatus_6;
	// RRLogoOldTracker TrackEventHandlerWorldCupTwo::rrlogoOld
	RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * ___rrlogoOld_7;
	// JKLaxmiTracker TrackEventHandlerWorldCupTwo::jkL
	JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * ___jkL_8;
	// JKLaxmiProTracker TrackEventHandlerWorldCupTwo::jklPro
	JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * ___jklPro_9;
	// RRLogoNewTracker TrackEventHandlerWorldCupTwo::rrLogoNew
	RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * ___rrLogoNew_10;
	// WorldCupOneTracker TrackEventHandlerWorldCupTwo::wcOne
	WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * ___wcOne_11;
	// WorldCupTwoTracker TrackEventHandlerWorldCupTwo::wcTwo
	WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * ___wcTwo_12;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mTrackableBehaviour_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}

	inline static int32_t get_offset_of_rrlogoOld_7() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05, ___rrlogoOld_7)); }
	inline RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * get_rrlogoOld_7() const { return ___rrlogoOld_7; }
	inline RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 ** get_address_of_rrlogoOld_7() { return &___rrlogoOld_7; }
	inline void set_rrlogoOld_7(RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * value)
	{
		___rrlogoOld_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rrlogoOld_7), (void*)value);
	}

	inline static int32_t get_offset_of_jkL_8() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05, ___jkL_8)); }
	inline JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * get_jkL_8() const { return ___jkL_8; }
	inline JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 ** get_address_of_jkL_8() { return &___jkL_8; }
	inline void set_jkL_8(JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * value)
	{
		___jkL_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jkL_8), (void*)value);
	}

	inline static int32_t get_offset_of_jklPro_9() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05, ___jklPro_9)); }
	inline JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * get_jklPro_9() const { return ___jklPro_9; }
	inline JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B ** get_address_of_jklPro_9() { return &___jklPro_9; }
	inline void set_jklPro_9(JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * value)
	{
		___jklPro_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jklPro_9), (void*)value);
	}

	inline static int32_t get_offset_of_rrLogoNew_10() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05, ___rrLogoNew_10)); }
	inline RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * get_rrLogoNew_10() const { return ___rrLogoNew_10; }
	inline RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C ** get_address_of_rrLogoNew_10() { return &___rrLogoNew_10; }
	inline void set_rrLogoNew_10(RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * value)
	{
		___rrLogoNew_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rrLogoNew_10), (void*)value);
	}

	inline static int32_t get_offset_of_wcOne_11() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05, ___wcOne_11)); }
	inline WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * get_wcOne_11() const { return ___wcOne_11; }
	inline WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 ** get_address_of_wcOne_11() { return &___wcOne_11; }
	inline void set_wcOne_11(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * value)
	{
		___wcOne_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wcOne_11), (void*)value);
	}

	inline static int32_t get_offset_of_wcTwo_12() { return static_cast<int32_t>(offsetof(TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05, ___wcTwo_12)); }
	inline WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * get_wcTwo_12() const { return ___wcTwo_12; }
	inline WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF ** get_address_of_wcTwo_12() { return &___wcTwo_12; }
	inline void set_wcTwo_12(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * value)
	{
		___wcTwo_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wcTwo_12), (void*)value);
	}
};


// UI_Handler
struct  UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TBOrbit UI_Handler::tbidel
	TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * ___tbidel_4;
	// System.Boolean UI_Handler::on
	bool ___on_6;
	// System.Int32 UI_Handler::onOff
	int32_t ___onOff_7;

public:
	inline static int32_t get_offset_of_tbidel_4() { return static_cast<int32_t>(offsetof(UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0, ___tbidel_4)); }
	inline TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * get_tbidel_4() const { return ___tbidel_4; }
	inline TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 ** get_address_of_tbidel_4() { return &___tbidel_4; }
	inline void set_tbidel_4(TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * value)
	{
		___tbidel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tbidel_4), (void*)value);
	}

	inline static int32_t get_offset_of_on_6() { return static_cast<int32_t>(offsetof(UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0, ___on_6)); }
	inline bool get_on_6() const { return ___on_6; }
	inline bool* get_address_of_on_6() { return &___on_6; }
	inline void set_on_6(bool value)
	{
		___on_6 = value;
	}

	inline static int32_t get_offset_of_onOff_7() { return static_cast<int32_t>(offsetof(UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0, ___onOff_7)); }
	inline int32_t get_onOff_7() const { return ___onOff_7; }
	inline int32_t* get_address_of_onOff_7() { return &___onOff_7; }
	inline void set_onOff_7(int32_t value)
	{
		___onOff_7 = value;
	}
};

struct UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0_StaticFields
{
public:
	// UI_Handler UI_Handler::instance
	UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0_StaticFields, ___instance_5)); }
	inline UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * get_instance_5() const { return ___instance_5; }
	inline UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_5), (void*)value);
	}
};


// Vuforia.VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_tF7F5C56A8081EC294AB1D066607A1BEE140F7F75  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// WorldCupOneTracker
struct  WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] WorldCupOneTracker::TrackerName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___TrackerName_5;
	// UnityEngine.GameObject[] WorldCupOneTracker::mainObject
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainObject_6;
	// UnityEngine.GameObject WorldCupOneTracker::JKLaxmiUIPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___JKLaxmiUIPanel_7;
	// UnityEngine.GameObject[] WorldCupOneTracker::mainRotateButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainRotateButton_8;
	// UnityEngine.GameObject[] WorldCupOneTracker::animationButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___animationButton_9;

public:
	inline static int32_t get_offset_of_TrackerName_5() { return static_cast<int32_t>(offsetof(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9, ___TrackerName_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_TrackerName_5() const { return ___TrackerName_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_TrackerName_5() { return &___TrackerName_5; }
	inline void set_TrackerName_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___TrackerName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrackerName_5), (void*)value);
	}

	inline static int32_t get_offset_of_mainObject_6() { return static_cast<int32_t>(offsetof(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9, ___mainObject_6)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainObject_6() const { return ___mainObject_6; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainObject_6() { return &___mainObject_6; }
	inline void set_mainObject_6(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_JKLaxmiUIPanel_7() { return static_cast<int32_t>(offsetof(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9, ___JKLaxmiUIPanel_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_JKLaxmiUIPanel_7() const { return ___JKLaxmiUIPanel_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_JKLaxmiUIPanel_7() { return &___JKLaxmiUIPanel_7; }
	inline void set_JKLaxmiUIPanel_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___JKLaxmiUIPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JKLaxmiUIPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_mainRotateButton_8() { return static_cast<int32_t>(offsetof(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9, ___mainRotateButton_8)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainRotateButton_8() const { return ___mainRotateButton_8; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainRotateButton_8() { return &___mainRotateButton_8; }
	inline void set_mainRotateButton_8(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainRotateButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainRotateButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_animationButton_9() { return static_cast<int32_t>(offsetof(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9, ___animationButton_9)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_animationButton_9() const { return ___animationButton_9; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_animationButton_9() { return &___animationButton_9; }
	inline void set_animationButton_9(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___animationButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationButton_9), (void*)value);
	}
};

struct WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9_StaticFields
{
public:
	// WorldCupOneTracker WorldCupOneTracker::WorldCupOneTrackerthis
	WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * ___WorldCupOneTrackerthis_4;

public:
	inline static int32_t get_offset_of_WorldCupOneTrackerthis_4() { return static_cast<int32_t>(offsetof(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9_StaticFields, ___WorldCupOneTrackerthis_4)); }
	inline WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * get_WorldCupOneTrackerthis_4() const { return ___WorldCupOneTrackerthis_4; }
	inline WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 ** get_address_of_WorldCupOneTrackerthis_4() { return &___WorldCupOneTrackerthis_4; }
	inline void set_WorldCupOneTrackerthis_4(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * value)
	{
		___WorldCupOneTrackerthis_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WorldCupOneTrackerthis_4), (void*)value);
	}
};


// WorldCupTwoTracker
struct  WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] WorldCupTwoTracker::TrackerName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___TrackerName_5;
	// UnityEngine.GameObject[] WorldCupTwoTracker::mainObject
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainObject_6;
	// UnityEngine.GameObject WorldCupTwoTracker::JKLaxmiUIPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___JKLaxmiUIPanel_7;
	// UnityEngine.GameObject[] WorldCupTwoTracker::mainRotateButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___mainRotateButton_8;
	// UnityEngine.GameObject[] WorldCupTwoTracker::animationButton
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___animationButton_9;

public:
	inline static int32_t get_offset_of_TrackerName_5() { return static_cast<int32_t>(offsetof(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF, ___TrackerName_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_TrackerName_5() const { return ___TrackerName_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_TrackerName_5() { return &___TrackerName_5; }
	inline void set_TrackerName_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___TrackerName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrackerName_5), (void*)value);
	}

	inline static int32_t get_offset_of_mainObject_6() { return static_cast<int32_t>(offsetof(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF, ___mainObject_6)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainObject_6() const { return ___mainObject_6; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainObject_6() { return &___mainObject_6; }
	inline void set_mainObject_6(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_JKLaxmiUIPanel_7() { return static_cast<int32_t>(offsetof(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF, ___JKLaxmiUIPanel_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_JKLaxmiUIPanel_7() const { return ___JKLaxmiUIPanel_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_JKLaxmiUIPanel_7() { return &___JKLaxmiUIPanel_7; }
	inline void set_JKLaxmiUIPanel_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___JKLaxmiUIPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JKLaxmiUIPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_mainRotateButton_8() { return static_cast<int32_t>(offsetof(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF, ___mainRotateButton_8)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_mainRotateButton_8() const { return ___mainRotateButton_8; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_mainRotateButton_8() { return &___mainRotateButton_8; }
	inline void set_mainRotateButton_8(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___mainRotateButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainRotateButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_animationButton_9() { return static_cast<int32_t>(offsetof(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF, ___animationButton_9)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_animationButton_9() const { return ___animationButton_9; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_animationButton_9() { return &___animationButton_9; }
	inline void set_animationButton_9(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___animationButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationButton_9), (void*)value);
	}
};

struct WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF_StaticFields
{
public:
	// WorldCupTwoTracker WorldCupTwoTracker::WorldCupTwoTrackerthis
	WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * ___WorldCupTwoTrackerthis_4;

public:
	inline static int32_t get_offset_of_WorldCupTwoTrackerthis_4() { return static_cast<int32_t>(offsetof(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF_StaticFields, ___WorldCupTwoTrackerthis_4)); }
	inline WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * get_WorldCupTwoTrackerthis_4() const { return ___WorldCupTwoTrackerthis_4; }
	inline WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF ** get_address_of_WorldCupTwoTrackerthis_4() { return &___WorldCupTwoTrackerthis_4; }
	inline void set_WorldCupTwoTrackerthis_4(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * value)
	{
		___WorldCupTwoTrackerthis_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WorldCupTwoTrackerthis_4), (void*)value);
	}
};


// GestureRecognizer`1<TwistGesture>
struct  GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7  : public GestureRecognizer_tDB209B08E5D9A7BC7FF0D64CC4DEFE32268A2409
{
public:
	// System.Collections.Generic.List`1<T> GestureRecognizer`1::gestures
	List_1_t08436B744622AC4133A3FF725BC02613EE626B3C * ___gestures_16;
	// GestureRecognizer`1_GestureEventHandler<T> GestureRecognizer`1::OnGesture
	GestureEventHandler_t89F6412E29BCC50D7A15211D9EFE5DB9B187CE9C * ___OnGesture_17;

public:
	inline static int32_t get_offset_of_gestures_16() { return static_cast<int32_t>(offsetof(GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7, ___gestures_16)); }
	inline List_1_t08436B744622AC4133A3FF725BC02613EE626B3C * get_gestures_16() const { return ___gestures_16; }
	inline List_1_t08436B744622AC4133A3FF725BC02613EE626B3C ** get_address_of_gestures_16() { return &___gestures_16; }
	inline void set_gestures_16(List_1_t08436B744622AC4133A3FF725BC02613EE626B3C * value)
	{
		___gestures_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gestures_16), (void*)value);
	}

	inline static int32_t get_offset_of_OnGesture_17() { return static_cast<int32_t>(offsetof(GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7, ___OnGesture_17)); }
	inline GestureEventHandler_t89F6412E29BCC50D7A15211D9EFE5DB9B187CE9C * get_OnGesture_17() const { return ___OnGesture_17; }
	inline GestureEventHandler_t89F6412E29BCC50D7A15211D9EFE5DB9B187CE9C ** get_address_of_OnGesture_17() { return &___OnGesture_17; }
	inline void set_OnGesture_17(GestureEventHandler_t89F6412E29BCC50D7A15211D9EFE5DB9B187CE9C * value)
	{
		___OnGesture_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnGesture_17), (void*)value);
	}
};

struct GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7_StaticFields
{
public:
	// FingerGestures_FingerList GestureRecognizer`1::tempTouchList
	FingerList_t3FF8FE51353DF9B29A0A3B4A9E24205999A331E5 * ___tempTouchList_18;

public:
	inline static int32_t get_offset_of_tempTouchList_18() { return static_cast<int32_t>(offsetof(GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7_StaticFields, ___tempTouchList_18)); }
	inline FingerList_t3FF8FE51353DF9B29A0A3B4A9E24205999A331E5 * get_tempTouchList_18() const { return ___tempTouchList_18; }
	inline FingerList_t3FF8FE51353DF9B29A0A3B4A9E24205999A331E5 ** get_address_of_tempTouchList_18() { return &___tempTouchList_18; }
	inline void set_tempTouchList_18(FingerList_t3FF8FE51353DF9B29A0A3B4A9E24205999A331E5 * value)
	{
		___tempTouchList_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tempTouchList_18), (void*)value);
	}
};


// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4  : public VuforiaMonoBehaviour_tF7F5C56A8081EC294AB1D066607A1BEE140F7F75
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_4;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_5;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_6;
	// Vuforia.TrackableBehaviour_Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_7;
	// Vuforia.TrackableBehaviour_StatusInfo Vuforia.TrackableBehaviour::mStatusInfo
	int32_t ___mStatusInfo_8;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_9;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_t4654F3D4310BED7B5F902B36EB09610B09E6AF5E * ___mTrackableEventHandlers_10;
	// System.Collections.Generic.ICollection`1<System.Action`1<Vuforia.TrackableBehaviour_StatusChangeResult>> Vuforia.TrackableBehaviour::mStatusChangedEventHandlers
	RuntimeObject* ___mStatusChangedEventHandlers_11;
	// System.Collections.Generic.ICollection`1<System.Action`1<Vuforia.TrackableBehaviour_StatusInfoChangeResult>> Vuforia.TrackableBehaviour::mStatusInfoChangedEventHandlers
	RuntimeObject* ___mStatusInfoChangedEventHandlers_12;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___U3CTimeStampU3Ek__BackingField_4)); }
	inline double get_U3CTimeStampU3Ek__BackingField_4() const { return ___U3CTimeStampU3Ek__BackingField_4; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_4() { return &___U3CTimeStampU3Ek__BackingField_4; }
	inline void set_U3CTimeStampU3Ek__BackingField_4(double value)
	{
		___U3CTimeStampU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackableName_5)); }
	inline String_t* get_mTrackableName_5() const { return ___mTrackableName_5; }
	inline String_t** get_address_of_mTrackableName_5() { return &___mTrackableName_5; }
	inline void set_mTrackableName_5(String_t* value)
	{
		___mTrackableName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mTrackableName_5), (void*)value);
	}

	inline static int32_t get_offset_of_mInitializedInEditor_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mInitializedInEditor_6)); }
	inline bool get_mInitializedInEditor_6() const { return ___mInitializedInEditor_6; }
	inline bool* get_address_of_mInitializedInEditor_6() { return &___mInitializedInEditor_6; }
	inline void set_mInitializedInEditor_6(bool value)
	{
		___mInitializedInEditor_6 = value;
	}

	inline static int32_t get_offset_of_mStatus_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mStatus_7)); }
	inline int32_t get_mStatus_7() const { return ___mStatus_7; }
	inline int32_t* get_address_of_mStatus_7() { return &___mStatus_7; }
	inline void set_mStatus_7(int32_t value)
	{
		___mStatus_7 = value;
	}

	inline static int32_t get_offset_of_mStatusInfo_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mStatusInfo_8)); }
	inline int32_t get_mStatusInfo_8() const { return ___mStatusInfo_8; }
	inline int32_t* get_address_of_mStatusInfo_8() { return &___mStatusInfo_8; }
	inline void set_mStatusInfo_8(int32_t value)
	{
		___mStatusInfo_8 = value;
	}

	inline static int32_t get_offset_of_mTrackable_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackable_9)); }
	inline RuntimeObject* get_mTrackable_9() const { return ___mTrackable_9; }
	inline RuntimeObject** get_address_of_mTrackable_9() { return &___mTrackable_9; }
	inline void set_mTrackable_9(RuntimeObject* value)
	{
		___mTrackable_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mTrackable_9), (void*)value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_10() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackableEventHandlers_10)); }
	inline List_1_t4654F3D4310BED7B5F902B36EB09610B09E6AF5E * get_mTrackableEventHandlers_10() const { return ___mTrackableEventHandlers_10; }
	inline List_1_t4654F3D4310BED7B5F902B36EB09610B09E6AF5E ** get_address_of_mTrackableEventHandlers_10() { return &___mTrackableEventHandlers_10; }
	inline void set_mTrackableEventHandlers_10(List_1_t4654F3D4310BED7B5F902B36EB09610B09E6AF5E * value)
	{
		___mTrackableEventHandlers_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mTrackableEventHandlers_10), (void*)value);
	}

	inline static int32_t get_offset_of_mStatusChangedEventHandlers_11() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mStatusChangedEventHandlers_11)); }
	inline RuntimeObject* get_mStatusChangedEventHandlers_11() const { return ___mStatusChangedEventHandlers_11; }
	inline RuntimeObject** get_address_of_mStatusChangedEventHandlers_11() { return &___mStatusChangedEventHandlers_11; }
	inline void set_mStatusChangedEventHandlers_11(RuntimeObject* value)
	{
		___mStatusChangedEventHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mStatusChangedEventHandlers_11), (void*)value);
	}

	inline static int32_t get_offset_of_mStatusInfoChangedEventHandlers_12() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mStatusInfoChangedEventHandlers_12)); }
	inline RuntimeObject* get_mStatusInfoChangedEventHandlers_12() const { return ___mStatusInfoChangedEventHandlers_12; }
	inline RuntimeObject** get_address_of_mStatusInfoChangedEventHandlers_12() { return &___mStatusInfoChangedEventHandlers_12; }
	inline void set_mStatusInfoChangedEventHandlers_12(RuntimeObject* value)
	{
		___mStatusInfoChangedEventHandlers_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mStatusInfoChangedEventHandlers_12), (void*)value);
	}
};


// ContinuousGestureRecognizer`1<TwistGesture>
struct  ContinuousGestureRecognizer_1_t5D48D6FBF95C0BE2E63E8CCE3BD2173CEAD947A3  : public GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7
{
public:

public:
};


// TwistRecognizer
struct  TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F  : public ContinuousGestureRecognizer_1_t5D48D6FBF95C0BE2E63E8CCE3BD2173CEAD947A3
{
public:
	// System.Single TwistRecognizer::MinDOT
	float ___MinDOT_19;
	// System.Single TwistRecognizer::MinRotation
	float ___MinRotation_20;

public:
	inline static int32_t get_offset_of_MinDOT_19() { return static_cast<int32_t>(offsetof(TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F, ___MinDOT_19)); }
	inline float get_MinDOT_19() const { return ___MinDOT_19; }
	inline float* get_address_of_MinDOT_19() { return &___MinDOT_19; }
	inline void set_MinDOT_19(float value)
	{
		___MinDOT_19 = value;
	}

	inline static int32_t get_offset_of_MinRotation_20() { return static_cast<int32_t>(offsetof(TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F, ___MinRotation_20)); }
	inline float get_MinRotation_20() const { return ___MinRotation_20; }
	inline float* get_address_of_MinRotation_20() { return &___MinRotation_20; }
	inline void set_MinRotation_20(float value)
	{
		___MinRotation_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * m_Items[1];

public:
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * m_Items[1];

public:
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * m_Items[1];

public:
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// FingerGestures_Finger[]
struct FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * m_Items[1];

public:
	inline Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * m_Items[1];

public:
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Component_GetComponentsInChildren_TisRuntimeObject_m47600682044DD98895C5990A94AC982C05645DC3_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, bool ___includeInactive0, const RuntimeMethod* method);
// System.Boolean GestureRecognizer`1<System.Object>::CanBegin(T,FingerGestures/IFingerList)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GestureRecognizer_1_CanBegin_m09E49F2FC8890623934032D059EF2A55E075FCBF_gshared (GestureRecognizer_1_t4F4DBAD6EA16DC12D158BD385245E1729F0FA36C * __this, RuntimeObject * ___gesture0, RuntimeObject* ___touches1, const RuntimeMethod* method);
// System.Void GestureRecognizer`1<System.Object>::RaiseEvent(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GestureRecognizer_1_RaiseEvent_m8011438F9D73321E8B08EAC38BF1C672775D2D36_gshared (GestureRecognizer_1_t4F4DBAD6EA16DC12D158BD385245E1729F0FA36C * __this, RuntimeObject * ___gesture0, const RuntimeMethod* method);
// System.Void ContinuousGestureRecognizer`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContinuousGestureRecognizer_1__ctor_m8FC08C69CBA6DFBDF54C13AD4DB1261E9BAB57F6_gshared (ContinuousGestureRecognizer_1_t2F998AA060B41D66917B55C04E45ACF5DD5D2F03 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mE03C66715289D7957CA068A675826B7EE0887BE3_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);

// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * Component_GetComponent_TisTrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_m81E3785465C5B36522D217B045A33CD65B28B229 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___exists0, const RuntimeMethod* method);
// System.Void Vuforia.TrackableBehaviour::RegisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackableBehaviour_RegisterTrackableEventHandler_m58152773889A643E8AB361E370CAD8E3F3905AE8 (TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * __this, RuntimeObject* ___trackableEventHandler0, const RuntimeMethod* method);
// System.Boolean Vuforia.TrackableBehaviour::UnregisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TrackableBehaviour_UnregisterTrackableEventHandler_m5BD1DF2C4294D63D909BB9545EF4009D34123A76 (TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * __this, RuntimeObject* ___trackableEventHandler0, const RuntimeMethod* method);
// System.String Vuforia.TrackableBehaviour::get_TrackableName()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline (TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, bool ___includeInactive0, const RuntimeMethod* method)
{
	return ((  RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m47600682044DD98895C5990A94AC982C05645DC3_gshared)(__this, ___includeInactive0, method);
}
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
inline ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, bool ___includeInactive0, const RuntimeMethod* method)
{
	return ((  ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m47600682044DD98895C5990A94AC982C05645DC3_gshared)(__this, ___includeInactive0, method);
}
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Canvas>(System.Boolean)
inline CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, bool ___includeInactive0, const RuntimeMethod* method)
{
	return ((  CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m47600682044DD98895C5990A94AC982C05645DC3_gshared)(__this, ___includeInactive0, method);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void RRLogoNewTracker::AnimationAndSoundFunction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RRLogoNewTracker_AnimationAndSoundFunction_m0FE270CC67D718CFB1EDA3ECA7EF48C71D496F0E (RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * __this, String_t* ___name0, bool ___condition1, const RuntimeMethod* method);
// System.Void JKLaxmiProTracker::AnimationAndSoundFunction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JKLaxmiProTracker_AnimationAndSoundFunction_mA06ADFB75D684B92943C49A91D12DCD601B48131 (JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * __this, String_t* ___name0, bool ___condition1, const RuntimeMethod* method);
// System.Void JKLaxmiTracker::AnimationAndSoundFunction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JKLaxmiTracker_AnimationAndSoundFunction_m744B000BE76C1724DCFC1EA7F7648A6C658CD7CC (JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * __this, String_t* ___name0, bool ___condition1, const RuntimeMethod* method);
// System.Void RRLogoOldTracker::AnimationAndSoundFunction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RRLogoOldTracker_AnimationAndSoundFunction_mD431FC5449E54A077FBCCF981ED662D263D48807 (RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * __this, String_t* ___name0, bool ___condition1, const RuntimeMethod* method);
// System.Void WorldCupOneTracker::AnimationAndSoundFunction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3 (WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * __this, String_t* ___name0, bool ___condition1, const RuntimeMethod* method);
// System.Void WorldCupTwoTracker::AnimationAndSoundFunction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9 (WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * __this, String_t* ___name0, bool ___condition1, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void ContinuousGesture::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContinuousGesture__ctor_mC302AF0C31E49BC40B920B7B5D652E9B1B95F51E (ContinuousGesture_tA44EC65F086D6C9B7CBC53AAA6607A158407EA98 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (RuntimeObject * ___message0, const RuntimeMethod* method);
// UnityEngine.GameObject Gesture::get_StartSelection()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Gesture_get_StartSelection_m2F671A58813B69F4E401DC2337BD85FB886B9A8C_inline (Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD * __this, const RuntimeMethod* method);
// System.Boolean GestureRecognizer`1<TwistGesture>::CanBegin(T,FingerGestures/IFingerList)
inline bool GestureRecognizer_1_CanBegin_m7450DF4EDF69CE6F64B046BA547843D4C37A68E6 (GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7 * __this, TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * ___gesture0, RuntimeObject* ___touches1, const RuntimeMethod* method)
{
	return ((  bool (*) (GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7 *, TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 *, RuntimeObject*, const RuntimeMethod*))GestureRecognizer_1_CanBegin_m09E49F2FC8890623934032D059EF2A55E075FCBF_gshared)(__this, ___gesture0, ___touches1, method);
}
// System.Boolean FingerGestures::AllFingersMoving(FingerGestures/Finger[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FingerGestures_AllFingersMoving_m76A80EE1817671AD55065EC255736FF9AEE849C8 (FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C* ___fingers0, const RuntimeMethod* method);
// System.Boolean TwistRecognizer::FingersMovedInOppositeDirections(FingerGestures/Finger,FingerGestures/Finger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TwistRecognizer_FingersMovedInOppositeDirections_mE1669F2E73404178083495D10D961C67B3458FF8 (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * ___finger00, Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * ___finger11, const RuntimeMethod* method);
// UnityEngine.Vector2 FingerGestures/Finger::get_StartPosition()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Finger_get_StartPosition_mAAE9424F2ACFD924E293DA00610E2BCE0B39BED4_inline (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * __this, const RuntimeMethod* method);
// System.Single TwistRecognizer::SignedAngularGap(FingerGestures/Finger,FingerGestures/Finger,UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TwistRecognizer_SignedAngularGap_m41B87BFF693B4DF4B3D949B493C38045099DF6A8 (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * ___finger00, Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * ___finger11, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___refPos02, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___refPos13, const RuntimeMethod* method);
// UnityEngine.Vector2 FingerGestures/Finger::get_Position()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Finger_get_Position_m55D65B4EC87B9BD75EF265375162A1A9AFA32DE0_inline (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(System.Single,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Multiply_m2E30A54E315810911DFC2E25C700757A68AC1F38 (float ___d0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a1, const RuntimeMethod* method);
// System.Void Gesture::set_StartPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Gesture_set_StartPosition_m72914599C23AC7622FF0CCFE09C89F62A40A4714_inline (Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 Gesture::get_StartPosition()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Gesture_get_StartPosition_mCF0DBFDBBF9920DBAC9E53B2B903DC448CABCC51_inline (Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD * __this, const RuntimeMethod* method);
// System.Void Gesture::set_Position(UnityEngine.Vector2)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Gesture_set_Position_m1A4BD357265D7780D6B980F9F569A37DC15CFE24_inline (Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method);
// System.Void TwistGesture::set_TotalRotation(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TwistGesture_set_TotalRotation_m1D905E69C5D3B7507859E64C94BEAD1AE7C9F637_inline (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, float ___value0, const RuntimeMethod* method);
// System.Void TwistGesture::set_DeltaRotation(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TwistGesture_set_DeltaRotation_m416B0DDE1E3DB369F3603DA9D158C94669D20F77_inline (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, float ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 FingerGestures/Finger::get_PreviousPosition()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Finger_get_PreviousPosition_mB33AEC9F577929EF86FF44BD573E9F9C9CCAF5FC_inline (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * __this, const RuntimeMethod* method);
// System.Single TwistGesture::get_DeltaRotation()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float TwistGesture_get_DeltaRotation_m3D7F3162B11DE8BB804284C4858450ED88D2E234_inline (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, const RuntimeMethod* method);
// System.Single TwistGesture::get_TotalRotation()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float TwistGesture_get_TotalRotation_m9D341DE733C95B8D7A7490CEFA00AD9C9BF91CBC_inline (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, const RuntimeMethod* method);
// System.Void GestureRecognizer`1<TwistGesture>::RaiseEvent(T)
inline void GestureRecognizer_1_RaiseEvent_mAFE9CABB4E3EEBB3535EB148C18051F460CDF538 (GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7 * __this, TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * ___gesture0, const RuntimeMethod* method)
{
	((  void (*) (GestureRecognizer_1_tFA8BF54B61300967D12E45E0F9F2F420C27D6EA7 *, TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 *, const RuntimeMethod*))GestureRecognizer_1_RaiseEvent_m8011438F9D73321E8B08EAC38BF1C672775D2D36_gshared)(__this, ___gesture0, method);
}
// System.Boolean FingerGestures::FingersMovedInOppositeDirections(FingerGestures/Finger,FingerGestures/Finger,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FingerGestures_FingersMovedInOppositeDirections_m667304A1A8774995D8D965FCB254B4D89EA3F5D3 (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * ___finger00, Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * ___finger11, float ___minDOT2, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, const RuntimeMethod* method);
// System.Single FingerGestures::SignedAngle(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FingerGestures_SignedAngle_m4FF40555C84A4382AABD859DFC66CDA234873413 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___from0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___to1, const RuntimeMethod* method);
// System.Void ContinuousGestureRecognizer`1<TwistGesture>::.ctor()
inline void ContinuousGestureRecognizer_1__ctor_m12C4DCFD6DFA8F812F6177460F971C36A304DD81 (ContinuousGestureRecognizer_1_t5D48D6FBF95C0BE2E63E8CCE3BD2173CEAD947A3 * __this, const RuntimeMethod* method)
{
	((  void (*) (ContinuousGestureRecognizer_1_t5D48D6FBF95C0BE2E63E8CCE3BD2173CEAD947A3 *, const RuntimeMethod*))ContinuousGestureRecognizer_1__ctor_m8FC08C69CBA6DFBDF54C13AD4DB1261E9BAB57F6_gshared)(__this, method);
}
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95 (const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m9721867D46BC827D58271AD235267B0B0865F115 (String_t* ___sceneName0, int32_t ___mode1, const RuntimeMethod* method);
// System.Collections.IEnumerator UI_Handler::TakeScreenshotAndSave()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UI_Handler_TakeScreenshotAndSave_m91BEA244D8F332A3C3B835E4612CA4BE4A11F68F (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void UI_Handler/<TakeScreenshotAndSave>d__12::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTakeScreenshotAndSaveU3Ed__12__ctor_mE4ECA813184992818F861079C0D0AF3DB077616D (U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void TBOrbit::ResetPanning()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TBOrbit_ResetPanning_m61C00B2A917391A22FA9DF81C7D09862D114F2C1 (TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * __this, const RuntimeMethod* method);
// System.Void TBOrbit::set_IdealYaw(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TBOrbit_set_IdealYaw_m3852EC8F27C486DA0C578E4B3F84A9CEA985663F (TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * __this, float ___value0, const RuntimeMethod* method);
// System.Void TBOrbit::set_IdealPitch(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TBOrbit_set_IdealPitch_m012C2A78264759B76D23EA69316D51CCD6EB2838 (TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * __this, float ___value0, const RuntimeMethod* method);
// System.Void TBOrbit::set_IdealDistance(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TBOrbit_set_IdealDistance_m7FCF141D84D04B66F0E5C836224DC98D7B5A0D4D (TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * __this, float ___value0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m1ED1C11E41D0ACC8CFCABBD25946CF0BD16D4F61 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___source0, int32_t ___destX1, int32_t ___destY2, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method);
// NativeGallery/Permission NativeGallery::SaveImageToGallery(UnityEngine.Texture2D,System.String,System.String,NativeGallery/MediaSaveCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_SaveImageToGallery_m29D2A7A8C05ECB2B99DAF4B82A86FA59E21F6A1A (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___image0, String_t* ___album1, String_t* ___filenameFormatted2, MediaSaveCallback_t489912AD9BCB0FD2B00ADCC25F89B8715D2F219E * ___callback3, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animation>()
inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mE03C66715289D7957CA068A675826B7EE0887BE3_gshared)(__this, method);
}
// System.Boolean UnityEngine.Animation::Play(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Animation_Play_m743CCCF04B64977460915D9E7007B0859BDF6AE9 (Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * __this, String_t* ___animation0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animation::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Animation_Play_mEC9F6C1F931E11D4D69043AD44CC71698B776A51 (Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrackEventHandlerRRLogoNew::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoNew_Start_mF0D1118EF43D5B0F1EB8D68FA802019E08A795D3 (TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerRRLogoNew_Start_mF0D1118EF43D5B0F1EB8D68FA802019E08A795D3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_0 = Component_GetComponent_TisTrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_m81E3785465C5B36522D217B045A33CD65B28B229(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_m81E3785465C5B36522D217B045A33CD65B28B229_RuntimeMethod_var);
		__this->set_mTrackableBehaviour_4(L_0);
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_1 = __this->get_mTrackableBehaviour_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_3 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m58152773889A643E8AB361E370CAD8E3F3905AE8(L_3, __this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void TrackEventHandlerRRLogoNew::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoNew_OnDestroy_mCD220749E7704E7BFDA5701A62804892057EC087 (TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerRRLogoNew_OnDestroy_mCD220749E7704E7BFDA5701A62804892057EC087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_0 = __this->get_mTrackableBehaviour_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_2 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_2);
		TrackableBehaviour_UnregisterTrackableEventHandler_m5BD1DF2C4294D63D909BB9545EF4009D34123A76(L_2, __this, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.Void TrackEventHandlerRRLogoNew::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoNew_Update_mF1B97259B6F6B525AA6FAD6E8B5C6113BA352051 (TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void TrackEventHandlerRRLogoNew::OnTrackableStateChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_Status)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoNew_OnTrackableStateChanged_m8AA8CCF260F1043C610DD65B06F4777E61DB1638 (TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerRRLogoNew_OnTrackableStateChanged_m8AA8CCF260F1043C610DD65B06F4777E61DB1638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_2 = ___newStatus1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0032;
		}
	}

IL_000c:
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_3 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_3);
		String_t* L_4 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_3, /*hidden argument*/NULL);
		String_t* L_5 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralE53A10BF18D590A4D354358810000744DAF6ACFD, L_4, _stringLiteral74D8330FF7EE2BF01889D92730C9FB1CC78C788F, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_5, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(6 /* System.Void TrackEventHandlerRRLogoNew::OnTrackingFound() */, __this);
		return;
	}

IL_0032:
	{
		int32_t L_6 = ___previousStatus0;
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_7 = ___newStatus1;
		if (L_7)
		{
			goto IL_005f;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_8 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_8);
		String_t* L_9 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_8, /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralE53A10BF18D590A4D354358810000744DAF6ACFD, L_9, _stringLiteralD8F3DAF68846C2D999956AB061674C6E36560643, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_10, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(7 /* System.Void TrackEventHandlerRRLogoNew::OnTrackingLost() */, __this);
		return;
	}

IL_005f:
	{
		VirtActionInvoker0::Invoke(7 /* System.Void TrackEventHandlerRRLogoNew::OnTrackingLost() */, __this);
		return;
	}
}
// System.Void TrackEventHandlerRRLogoNew::OnTrackingFound()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoNew_OnTrackingFound_mBDDD08EABE1605AB9A67DBF20961343BB2A24017 (TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerRRLogoNew_OnTrackingFound_mBDDD08EABE1605AB9A67DBF20961343BB2A24017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_0 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_1 = NULL;
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* V_2 = NULL;
	int32_t V_3 = 0;
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_4 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_5 = NULL;
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F_RuntimeMethod_var);
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047_RuntimeMethod_var);
		V_0 = L_1;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_2 = Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0029;
	}

IL_001c:
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_6, (bool)1, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0029:
	{
		int32_t L_8 = V_3;
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_10 = V_0;
		V_4 = L_10;
		V_3 = 0;
		goto IL_0044;
	}

IL_0036:
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_11 = V_4;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_14, (bool)1, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_16 = V_3;
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_17 = V_4;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_18 = V_1;
		V_5 = L_18;
		V_3 = 0;
		goto IL_0060;
	}

IL_0052:
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_19 = V_5;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_22, (bool)1, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0060:
	{
		int32_t L_24 = V_3;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_25 = V_5;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * L_26 = __this->get_rrlogoNew_7();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_27 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_27);
		String_t* L_28 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		RRLogoNewTracker_AnimationAndSoundFunction_m0FE270CC67D718CFB1EDA3ECA7EF48C71D496F0E(L_26, L_28, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventHandlerRRLogoNew::OnTrackingLost()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoNew_OnTrackingLost_m100882790426557C55F65F68302022BFDD191EAC (TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerRRLogoNew_OnTrackingLost_m100882790426557C55F65F68302022BFDD191EAC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_0 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_1 = NULL;
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* V_2 = NULL;
	int32_t V_3 = 0;
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_4 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_5 = NULL;
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F_RuntimeMethod_var);
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047_RuntimeMethod_var);
		V_0 = L_1;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_2 = Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0029;
	}

IL_001c:
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_6, (bool)0, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0029:
	{
		int32_t L_8 = V_3;
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_10 = V_0;
		V_4 = L_10;
		V_3 = 0;
		goto IL_0044;
	}

IL_0036:
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_11 = V_4;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_14, (bool)0, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_16 = V_3;
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_17 = V_4;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_18 = V_1;
		V_5 = L_18;
		V_3 = 0;
		goto IL_0060;
	}

IL_0052:
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_19 = V_5;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_22, (bool)0, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0060:
	{
		int32_t L_24 = V_3;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_25 = V_5;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * L_26 = __this->get_rrlogoNew_7();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_27 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_27);
		String_t* L_28 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		RRLogoNewTracker_AnimationAndSoundFunction_m0FE270CC67D718CFB1EDA3ECA7EF48C71D496F0E(L_26, L_28, (bool)0, /*hidden argument*/NULL);
		JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * L_29 = __this->get_jklPro_9();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_30 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		JKLaxmiProTracker_AnimationAndSoundFunction_mA06ADFB75D684B92943C49A91D12DCD601B48131(L_29, L_31, (bool)0, /*hidden argument*/NULL);
		JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * L_32 = __this->get_jkL_8();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_33 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_33);
		String_t* L_34 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		JKLaxmiTracker_AnimationAndSoundFunction_m744B000BE76C1724DCFC1EA7F7648A6C658CD7CC(L_32, L_34, (bool)0, /*hidden argument*/NULL);
		RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * L_35 = __this->get_rrLogoOld_10();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_36 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_36);
		String_t* L_37 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		RRLogoOldTracker_AnimationAndSoundFunction_mD431FC5449E54A077FBCCF981ED662D263D48807(L_35, L_37, (bool)0, /*hidden argument*/NULL);
		WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * L_38 = __this->get_wcOne_11();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_39 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_39);
		String_t* L_40 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3(L_38, L_40, (bool)0, /*hidden argument*/NULL);
		WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * L_41 = __this->get_wcTwo_12();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_42 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_42);
		String_t* L_43 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9(L_41, L_43, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventHandlerRRLogoNew::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoNew__ctor_mB1F4BA23C6D860261E6F60B5F72A273A75867932 (TrackEventHandlerRRLogoNew_tBF953337848CD37EDE1ACDF2EE0F884D7CDFD601 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrackEventHandlerRRLogoOld::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoOld_Start_m58028B9882621D12B328D569AAF0F5E4A6F688A1 (TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerRRLogoOld_Start_m58028B9882621D12B328D569AAF0F5E4A6F688A1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_0 = Component_GetComponent_TisTrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_m81E3785465C5B36522D217B045A33CD65B28B229(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_m81E3785465C5B36522D217B045A33CD65B28B229_RuntimeMethod_var);
		__this->set_mTrackableBehaviour_4(L_0);
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_1 = __this->get_mTrackableBehaviour_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_3 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m58152773889A643E8AB361E370CAD8E3F3905AE8(L_3, __this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void TrackEventHandlerRRLogoOld::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoOld_OnDestroy_m102F95DC449DD4351D0A532ED6EF263FE43372F0 (TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerRRLogoOld_OnDestroy_m102F95DC449DD4351D0A532ED6EF263FE43372F0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_0 = __this->get_mTrackableBehaviour_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_2 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_2);
		TrackableBehaviour_UnregisterTrackableEventHandler_m5BD1DF2C4294D63D909BB9545EF4009D34123A76(L_2, __this, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.Void TrackEventHandlerRRLogoOld::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoOld_Update_mC16A3E79614A2F4428C1C697F520FC6CA64416BC (TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void TrackEventHandlerRRLogoOld::OnTrackableStateChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_Status)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoOld_OnTrackableStateChanged_mD470EFA12C2E52811E672D6431F86A1E41991775 (TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerRRLogoOld_OnTrackableStateChanged_mD470EFA12C2E52811E672D6431F86A1E41991775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_2 = ___newStatus1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0032;
		}
	}

IL_000c:
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_3 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_3);
		String_t* L_4 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_3, /*hidden argument*/NULL);
		String_t* L_5 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralE53A10BF18D590A4D354358810000744DAF6ACFD, L_4, _stringLiteral74D8330FF7EE2BF01889D92730C9FB1CC78C788F, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_5, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(6 /* System.Void TrackEventHandlerRRLogoOld::OnTrackingFound() */, __this);
		return;
	}

IL_0032:
	{
		int32_t L_6 = ___previousStatus0;
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_7 = ___newStatus1;
		if (L_7)
		{
			goto IL_005f;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_8 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_8);
		String_t* L_9 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_8, /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralE53A10BF18D590A4D354358810000744DAF6ACFD, L_9, _stringLiteralD8F3DAF68846C2D999956AB061674C6E36560643, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_10, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(7 /* System.Void TrackEventHandlerRRLogoOld::OnTrackingLost() */, __this);
		return;
	}

IL_005f:
	{
		VirtActionInvoker0::Invoke(7 /* System.Void TrackEventHandlerRRLogoOld::OnTrackingLost() */, __this);
		return;
	}
}
// System.Void TrackEventHandlerRRLogoOld::OnTrackingFound()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoOld_OnTrackingFound_m93965E7DF89A900BAC9EA0FE657FD639742F0AAA (TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerRRLogoOld_OnTrackingFound_m93965E7DF89A900BAC9EA0FE657FD639742F0AAA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_0 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_1 = NULL;
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* V_2 = NULL;
	int32_t V_3 = 0;
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_4 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_5 = NULL;
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F_RuntimeMethod_var);
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047_RuntimeMethod_var);
		V_0 = L_1;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_2 = Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0029;
	}

IL_001c:
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_6, (bool)1, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0029:
	{
		int32_t L_8 = V_3;
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_10 = V_0;
		V_4 = L_10;
		V_3 = 0;
		goto IL_0044;
	}

IL_0036:
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_11 = V_4;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_14, (bool)1, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_16 = V_3;
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_17 = V_4;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_18 = V_1;
		V_5 = L_18;
		V_3 = 0;
		goto IL_0060;
	}

IL_0052:
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_19 = V_5;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_22, (bool)1, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0060:
	{
		int32_t L_24 = V_3;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_25 = V_5;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * L_26 = __this->get_rrlogoOld_7();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_27 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_27);
		String_t* L_28 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		RRLogoOldTracker_AnimationAndSoundFunction_mD431FC5449E54A077FBCCF981ED662D263D48807(L_26, L_28, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventHandlerRRLogoOld::OnTrackingLost()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoOld_OnTrackingLost_m54635C86D7FE050BD8E04E8E92E74D8740EE22E0 (TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerRRLogoOld_OnTrackingLost_m54635C86D7FE050BD8E04E8E92E74D8740EE22E0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_0 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_1 = NULL;
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* V_2 = NULL;
	int32_t V_3 = 0;
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_4 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_5 = NULL;
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F_RuntimeMethod_var);
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047_RuntimeMethod_var);
		V_0 = L_1;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_2 = Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0029;
	}

IL_001c:
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_6, (bool)0, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0029:
	{
		int32_t L_8 = V_3;
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_10 = V_0;
		V_4 = L_10;
		V_3 = 0;
		goto IL_0044;
	}

IL_0036:
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_11 = V_4;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_14, (bool)0, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_16 = V_3;
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_17 = V_4;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_18 = V_1;
		V_5 = L_18;
		V_3 = 0;
		goto IL_0060;
	}

IL_0052:
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_19 = V_5;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_22, (bool)0, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0060:
	{
		int32_t L_24 = V_3;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_25 = V_5;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * L_26 = __this->get_rrlogoOld_7();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_27 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_27);
		String_t* L_28 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		RRLogoOldTracker_AnimationAndSoundFunction_mD431FC5449E54A077FBCCF981ED662D263D48807(L_26, L_28, (bool)0, /*hidden argument*/NULL);
		JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * L_29 = __this->get_jklPro_9();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_30 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		JKLaxmiProTracker_AnimationAndSoundFunction_mA06ADFB75D684B92943C49A91D12DCD601B48131(L_29, L_31, (bool)0, /*hidden argument*/NULL);
		JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * L_32 = __this->get_jkL_8();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_33 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_33);
		String_t* L_34 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		JKLaxmiTracker_AnimationAndSoundFunction_m744B000BE76C1724DCFC1EA7F7648A6C658CD7CC(L_32, L_34, (bool)0, /*hidden argument*/NULL);
		RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * L_35 = __this->get_rrLogoNew_10();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_36 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_36);
		String_t* L_37 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		RRLogoNewTracker_AnimationAndSoundFunction_m0FE270CC67D718CFB1EDA3ECA7EF48C71D496F0E(L_35, L_37, (bool)0, /*hidden argument*/NULL);
		WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * L_38 = __this->get_wcOne_11();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_39 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_39);
		String_t* L_40 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3(L_38, L_40, (bool)0, /*hidden argument*/NULL);
		WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * L_41 = __this->get_wcTwo_12();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_42 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_42);
		String_t* L_43 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9(L_41, L_43, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventHandlerRRLogoOld::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerRRLogoOld__ctor_m5F9F090DEB32EE65F5849814EC2475A6069C609B (TrackEventHandlerRRLogoOld_t8A574D51E69B3F2CCF18702B5CCDBE5EFC84F9BC * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrackEventHandlerWorldCupOne::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupOne_Start_m241FE993871965BAD0239A91B41123B674F39C10 (TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerWorldCupOne_Start_m241FE993871965BAD0239A91B41123B674F39C10_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_0 = Component_GetComponent_TisTrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_m81E3785465C5B36522D217B045A33CD65B28B229(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_m81E3785465C5B36522D217B045A33CD65B28B229_RuntimeMethod_var);
		__this->set_mTrackableBehaviour_4(L_0);
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_1 = __this->get_mTrackableBehaviour_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_3 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m58152773889A643E8AB361E370CAD8E3F3905AE8(L_3, __this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void TrackEventHandlerWorldCupOne::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupOne_OnDestroy_mF06C868CA69A637B1AA3CB7701ADFEFA422D3D2E (TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerWorldCupOne_OnDestroy_mF06C868CA69A637B1AA3CB7701ADFEFA422D3D2E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_0 = __this->get_mTrackableBehaviour_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_2 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_2);
		TrackableBehaviour_UnregisterTrackableEventHandler_m5BD1DF2C4294D63D909BB9545EF4009D34123A76(L_2, __this, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.Void TrackEventHandlerWorldCupOne::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupOne_Update_mAFF3356727F1CDF681D8B055749BF1669979864C (TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void TrackEventHandlerWorldCupOne::OnTrackableStateChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_Status)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupOne_OnTrackableStateChanged_m4C9476EF61E220726561BB6AFAAC1C85DDB3ED75 (TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerWorldCupOne_OnTrackableStateChanged_m4C9476EF61E220726561BB6AFAAC1C85DDB3ED75_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_2 = ___newStatus1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0032;
		}
	}

IL_000c:
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_3 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_3);
		String_t* L_4 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_3, /*hidden argument*/NULL);
		String_t* L_5 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralE53A10BF18D590A4D354358810000744DAF6ACFD, L_4, _stringLiteral74D8330FF7EE2BF01889D92730C9FB1CC78C788F, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_5, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(6 /* System.Void TrackEventHandlerWorldCupOne::OnTrackingFound() */, __this);
		return;
	}

IL_0032:
	{
		int32_t L_6 = ___previousStatus0;
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_7 = ___newStatus1;
		if (L_7)
		{
			goto IL_005f;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_8 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_8);
		String_t* L_9 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_8, /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralE53A10BF18D590A4D354358810000744DAF6ACFD, L_9, _stringLiteralD8F3DAF68846C2D999956AB061674C6E36560643, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_10, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(7 /* System.Void TrackEventHandlerWorldCupOne::OnTrackingLost() */, __this);
		return;
	}

IL_005f:
	{
		VirtActionInvoker0::Invoke(7 /* System.Void TrackEventHandlerWorldCupOne::OnTrackingLost() */, __this);
		return;
	}
}
// System.Void TrackEventHandlerWorldCupOne::OnTrackingFound()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupOne_OnTrackingFound_mA67BFB5DC6CE2CCA3124747452D5B95267704978 (TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerWorldCupOne_OnTrackingFound_mA67BFB5DC6CE2CCA3124747452D5B95267704978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_0 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_1 = NULL;
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* V_2 = NULL;
	int32_t V_3 = 0;
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_4 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_5 = NULL;
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F_RuntimeMethod_var);
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047_RuntimeMethod_var);
		V_0 = L_1;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_2 = Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0029;
	}

IL_001c:
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_6, (bool)1, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0029:
	{
		int32_t L_8 = V_3;
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_10 = V_0;
		V_4 = L_10;
		V_3 = 0;
		goto IL_0044;
	}

IL_0036:
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_11 = V_4;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_14, (bool)1, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_16 = V_3;
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_17 = V_4;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_18 = V_1;
		V_5 = L_18;
		V_3 = 0;
		goto IL_0060;
	}

IL_0052:
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_19 = V_5;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_22, (bool)1, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0060:
	{
		int32_t L_24 = V_3;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_25 = V_5;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * L_26 = __this->get_wcOne_11();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_27 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_27);
		String_t* L_28 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3(L_26, L_28, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventHandlerWorldCupOne::OnTrackingLost()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupOne_OnTrackingLost_mF12C20F6775598BDF424C544B2CC27B2E701A537 (TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerWorldCupOne_OnTrackingLost_mF12C20F6775598BDF424C544B2CC27B2E701A537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_0 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_1 = NULL;
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* V_2 = NULL;
	int32_t V_3 = 0;
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_4 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_5 = NULL;
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F_RuntimeMethod_var);
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047_RuntimeMethod_var);
		V_0 = L_1;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_2 = Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0029;
	}

IL_001c:
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_6, (bool)0, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0029:
	{
		int32_t L_8 = V_3;
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_10 = V_0;
		V_4 = L_10;
		V_3 = 0;
		goto IL_0044;
	}

IL_0036:
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_11 = V_4;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_14, (bool)0, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_16 = V_3;
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_17 = V_4;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_18 = V_1;
		V_5 = L_18;
		V_3 = 0;
		goto IL_0060;
	}

IL_0052:
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_19 = V_5;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_22, (bool)0, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0060:
	{
		int32_t L_24 = V_3;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_25 = V_5;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * L_26 = __this->get_rrlogoOld_7();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_27 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_27);
		String_t* L_28 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		RRLogoOldTracker_AnimationAndSoundFunction_mD431FC5449E54A077FBCCF981ED662D263D48807(L_26, L_28, (bool)0, /*hidden argument*/NULL);
		JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * L_29 = __this->get_jklPro_9();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_30 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		JKLaxmiProTracker_AnimationAndSoundFunction_mA06ADFB75D684B92943C49A91D12DCD601B48131(L_29, L_31, (bool)0, /*hidden argument*/NULL);
		JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * L_32 = __this->get_jkL_8();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_33 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_33);
		String_t* L_34 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		JKLaxmiTracker_AnimationAndSoundFunction_m744B000BE76C1724DCFC1EA7F7648A6C658CD7CC(L_32, L_34, (bool)0, /*hidden argument*/NULL);
		RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * L_35 = __this->get_rrLogoNew_10();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_36 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_36);
		String_t* L_37 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		RRLogoNewTracker_AnimationAndSoundFunction_m0FE270CC67D718CFB1EDA3ECA7EF48C71D496F0E(L_35, L_37, (bool)0, /*hidden argument*/NULL);
		WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * L_38 = __this->get_wcOne_11();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_39 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_39);
		String_t* L_40 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3(L_38, L_40, (bool)0, /*hidden argument*/NULL);
		WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * L_41 = __this->get_wcTwo_12();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_42 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_42);
		String_t* L_43 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9(L_41, L_43, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventHandlerWorldCupOne::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupOne__ctor_m376C75CB431E5F7571EACD69286B20CF5A08BBED (TrackEventHandlerWorldCupOne_t437B06DC6F6B206D6EC88B193F576DB11C1F6BDF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrackEventHandlerWorldCupTwo::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupTwo_Start_m2AB8752DAB38401B5C82C1DADB5B97A926D31589 (TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerWorldCupTwo_Start_m2AB8752DAB38401B5C82C1DADB5B97A926D31589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_0 = Component_GetComponent_TisTrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_m81E3785465C5B36522D217B045A33CD65B28B229(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_m81E3785465C5B36522D217B045A33CD65B28B229_RuntimeMethod_var);
		__this->set_mTrackableBehaviour_4(L_0);
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_1 = __this->get_mTrackableBehaviour_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_3 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m58152773889A643E8AB361E370CAD8E3F3905AE8(L_3, __this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void TrackEventHandlerWorldCupTwo::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupTwo_OnDestroy_m75A791E319CF636B146FEE537B7EC5DD2682F459 (TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerWorldCupTwo_OnDestroy_m75A791E319CF636B146FEE537B7EC5DD2682F459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_0 = __this->get_mTrackableBehaviour_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_2 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_2);
		TrackableBehaviour_UnregisterTrackableEventHandler_m5BD1DF2C4294D63D909BB9545EF4009D34123A76(L_2, __this, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.Void TrackEventHandlerWorldCupTwo::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupTwo_Update_mD574A9ED56F89BD8A20E18C2AE0E9B0F7E054382 (TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void TrackEventHandlerWorldCupTwo::OnTrackableStateChanged(Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_Status)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupTwo_OnTrackableStateChanged_m843B9D305A7208AE58172D58496CAC78C82CDC79 (TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerWorldCupTwo_OnTrackableStateChanged_m843B9D305A7208AE58172D58496CAC78C82CDC79_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_2 = ___newStatus1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0032;
		}
	}

IL_000c:
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_3 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_3);
		String_t* L_4 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_3, /*hidden argument*/NULL);
		String_t* L_5 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralE53A10BF18D590A4D354358810000744DAF6ACFD, L_4, _stringLiteral74D8330FF7EE2BF01889D92730C9FB1CC78C788F, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_5, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(6 /* System.Void TrackEventHandlerWorldCupTwo::OnTrackingFound() */, __this);
		return;
	}

IL_0032:
	{
		int32_t L_6 = ___previousStatus0;
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_7 = ___newStatus1;
		if (L_7)
		{
			goto IL_005f;
		}
	}
	{
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_8 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_8);
		String_t* L_9 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_8, /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralE53A10BF18D590A4D354358810000744DAF6ACFD, L_9, _stringLiteralD8F3DAF68846C2D999956AB061674C6E36560643, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_10, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(7 /* System.Void TrackEventHandlerWorldCupTwo::OnTrackingLost() */, __this);
		return;
	}

IL_005f:
	{
		VirtActionInvoker0::Invoke(7 /* System.Void TrackEventHandlerWorldCupTwo::OnTrackingLost() */, __this);
		return;
	}
}
// System.Void TrackEventHandlerWorldCupTwo::OnTrackingFound()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupTwo_OnTrackingFound_m73585600AC2FCA0B8E997C502683E32EB56AA886 (TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerWorldCupTwo_OnTrackingFound_m73585600AC2FCA0B8E997C502683E32EB56AA886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_0 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_1 = NULL;
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* V_2 = NULL;
	int32_t V_3 = 0;
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_4 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_5 = NULL;
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F_RuntimeMethod_var);
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047_RuntimeMethod_var);
		V_0 = L_1;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_2 = Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0029;
	}

IL_001c:
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_6, (bool)1, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0029:
	{
		int32_t L_8 = V_3;
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_10 = V_0;
		V_4 = L_10;
		V_3 = 0;
		goto IL_0044;
	}

IL_0036:
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_11 = V_4;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_14, (bool)1, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_16 = V_3;
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_17 = V_4;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_18 = V_1;
		V_5 = L_18;
		V_3 = 0;
		goto IL_0060;
	}

IL_0052:
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_19 = V_5;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_22, (bool)1, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0060:
	{
		int32_t L_24 = V_3;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_25 = V_5;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * L_26 = __this->get_wcTwo_12();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_27 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_27);
		String_t* L_28 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9(L_26, L_28, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventHandlerWorldCupTwo::OnTrackingLost()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupTwo_OnTrackingLost_mD6349AD5435B342536208AFCB67EBF5301FB3639 (TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackEventHandlerWorldCupTwo_OnTrackingLost_mD6349AD5435B342536208AFCB67EBF5301FB3639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_0 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_1 = NULL;
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* V_2 = NULL;
	int32_t V_3 = 0;
	ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* V_4 = NULL;
	CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* V_5 = NULL;
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mBBE5710DB941BDD90C838F333EB584AAC8B9B73F_RuntimeMethod_var);
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m7690C204F64DA6D651AA724E2656C12499849047_RuntimeMethod_var);
		V_0 = L_1;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_2 = Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_m0DCB451DDA382B4BF6D882CBA43DBDD200C1FCF2_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0029;
	}

IL_001c:
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_6, (bool)0, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0029:
	{
		int32_t L_8 = V_3;
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_10 = V_0;
		V_4 = L_10;
		V_3 = 0;
		goto IL_0044;
	}

IL_0036:
	{
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_11 = V_4;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_14, (bool)0, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_16 = V_3;
		ColliderU5BU5D_t2814A7DE0594A145A9E2C23548AF5B6E9DF8402A* L_17 = V_4;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_18 = V_1;
		V_5 = L_18;
		V_3 = 0;
		goto IL_0060;
	}

IL_0052:
	{
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_19 = V_5;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_22, (bool)0, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0060:
	{
		int32_t L_24 = V_3;
		CanvasU5BU5D_t69253447FFB59DF7EE8408C1DB31C3E6CF80C129* L_25 = V_5;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		RRLogoOldTracker_tE6C054C0804DD721D6E4313EC996F0B885F44EB6 * L_26 = __this->get_rrlogoOld_7();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_27 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_27);
		String_t* L_28 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		RRLogoOldTracker_AnimationAndSoundFunction_mD431FC5449E54A077FBCCF981ED662D263D48807(L_26, L_28, (bool)0, /*hidden argument*/NULL);
		JKLaxmiProTracker_t4EF37F5F1AE168619109B1C6E856B1FC35468A8B * L_29 = __this->get_jklPro_9();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_30 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		JKLaxmiProTracker_AnimationAndSoundFunction_mA06ADFB75D684B92943C49A91D12DCD601B48131(L_29, L_31, (bool)0, /*hidden argument*/NULL);
		JKLaxmiTracker_t7E9710690DB8F416484BEAD65936D9E8F0BEBF56 * L_32 = __this->get_jkL_8();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_33 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_33);
		String_t* L_34 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		JKLaxmiTracker_AnimationAndSoundFunction_m744B000BE76C1724DCFC1EA7F7648A6C658CD7CC(L_32, L_34, (bool)0, /*hidden argument*/NULL);
		RRLogoNewTracker_t848015AE5BFA41529E28411C97D5236C42685E8C * L_35 = __this->get_rrLogoNew_10();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_36 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_36);
		String_t* L_37 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		RRLogoNewTracker_AnimationAndSoundFunction_m0FE270CC67D718CFB1EDA3ECA7EF48C71D496F0E(L_35, L_37, (bool)0, /*hidden argument*/NULL);
		WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * L_38 = __this->get_wcOne_11();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_39 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_39);
		String_t* L_40 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3(L_38, L_40, (bool)0, /*hidden argument*/NULL);
		WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * L_41 = __this->get_wcTwo_12();
		TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * L_42 = __this->get_mTrackableBehaviour_4();
		NullCheck(L_42);
		String_t* L_43 = TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline(L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9(L_41, L_43, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventHandlerWorldCupTwo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackEventHandlerWorldCupTwo__ctor_mA75F02F0D81D9063C10800171EF52265C6D94881 (TrackEventHandlerWorldCupTwo_tBA0B8FB13157A6A16616B8C11B1331358C690D05 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single TwistGesture::get_DeltaRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TwistGesture_get_DeltaRotation_m3D7F3162B11DE8BB804284C4858450ED88D2E234 (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_deltaRotation_12();
		return L_0;
	}
}
// System.Void TwistGesture::set_DeltaRotation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwistGesture_set_DeltaRotation_m416B0DDE1E3DB369F3603DA9D158C94669D20F77 (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_deltaRotation_12(L_0);
		return;
	}
}
// System.Single TwistGesture::get_TotalRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TwistGesture_get_TotalRotation_m9D341DE733C95B8D7A7490CEFA00AD9C9BF91CBC (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_totalRotation_13();
		return L_0;
	}
}
// System.Void TwistGesture::set_TotalRotation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwistGesture_set_TotalRotation_m1D905E69C5D3B7507859E64C94BEAD1AE7C9F637 (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_totalRotation_13(L_0);
		return;
	}
}
// System.Void TwistGesture::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwistGesture__ctor_m76A30A059C2D659C835C1A4790DA4EB8DF568170 (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, const RuntimeMethod* method)
{
	{
		ContinuousGesture__ctor_mC302AF0C31E49BC40B920B7B5D652E9B1B95F51E(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TwistRecognizer::GetDefaultEventMessageName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TwistRecognizer_GetDefaultEventMessageName_mF7786153A5B741E37848E587DBA2F39034E6C644 (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistRecognizer_GetDefaultEventMessageName_mF7786153A5B741E37848E587DBA2F39034E6C644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral015932202FD61E451C6F831D0D0535A969937B1D;
	}
}
// System.Int32 TwistRecognizer::get_RequiredFingerCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TwistRecognizer_get_RequiredFingerCount_m1C2030C2C36DC8DDEED826904CBC55FE73B32493 (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, const RuntimeMethod* method)
{
	{
		return 2;
	}
}
// System.Void TwistRecognizer::set_RequiredFingerCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwistRecognizer_set_RequiredFingerCount_m0238672B86CB924044FEB96F6B5ADFAB8DC709A0 (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistRecognizer_set_RequiredFingerCount_m0238672B86CB924044FEB96F6B5ADFAB8DC709A0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(_stringLiteralB3034EC567C2321A383D655A57D2CB638F893719, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TwistRecognizer::get_SupportFingerClustering()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TwistRecognizer_get_SupportFingerClustering_m647C994A421BC4B60F1674B92B0564E96ECFD8C8 (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// GestureResetMode TwistRecognizer::GetDefaultResetMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TwistRecognizer_GetDefaultResetMode_mF066D28750B6FA5E486584434F15E23924C8FD7A (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(1);
	}
}
// UnityEngine.GameObject TwistRecognizer::GetDefaultSelectionForSendMessage(TwistGesture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * TwistRecognizer_GetDefaultSelectionForSendMessage_mB68E56C00F506BF0281914A9250FEA62BEF36E38 (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * ___gesture0, const RuntimeMethod* method)
{
	{
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Gesture_get_StartSelection_m2F671A58813B69F4E401DC2337BD85FB886B9A8C_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean TwistRecognizer::CanBegin(TwistGesture,FingerGestures_IFingerList)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TwistRecognizer_CanBegin_m9F5ADD27AE0C1A65E6D6BDD242D73E62CBB6047B (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * ___gesture0, RuntimeObject* ___touches1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistRecognizer_CanBegin_m9F5ADD27AE0C1A65E6D6BDD242D73E62CBB6047B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * V_0 = NULL;
	Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * V_1 = NULL;
	{
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_0 = ___gesture0;
		RuntimeObject* L_1 = ___touches1;
		bool L_2 = GestureRecognizer_1_CanBegin_m7450DF4EDF69CE6F64B046BA547843D4C37A68E6(__this, L_0, L_1, /*hidden argument*/GestureRecognizer_1_CanBegin_m7450DF4EDF69CE6F64B046BA547843D4C37A68E6_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_000c;
		}
	}
	{
		return (bool)0;
	}

IL_000c:
	{
		RuntimeObject* L_3 = ___touches1;
		NullCheck(L_3);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_4 = InterfaceFuncInvoker1< Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 *, int32_t >::Invoke(0 /* FingerGestures/Finger FingerGestures/IFingerList::get_Item(System.Int32) */, IFingerList_t09E148C8FA751B1E829E3AA97CD4DAC2EF6DB7A1_il2cpp_TypeInfo_var, L_3, 0);
		V_0 = L_4;
		RuntimeObject* L_5 = ___touches1;
		NullCheck(L_5);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_6 = InterfaceFuncInvoker1< Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 *, int32_t >::Invoke(0 /* FingerGestures/Finger FingerGestures/IFingerList::get_Item(System.Int32) */, IFingerList_t09E148C8FA751B1E829E3AA97CD4DAC2EF6DB7A1_il2cpp_TypeInfo_var, L_5, 1);
		V_1 = L_6;
		FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C* L_7 = (FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C*)(FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C*)SZArrayNew(FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C_il2cpp_TypeInfo_var, (uint32_t)2);
		FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C* L_8 = L_7;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 *)L_9);
		FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C* L_10 = L_8;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_11 = V_1;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 *)L_11);
		IL2CPP_RUNTIME_CLASS_INIT(FingerGestures_t9DE618166E636E7EC25F553F0E3B01AA6513C95A_il2cpp_TypeInfo_var);
		bool L_12 = FingerGestures_AllFingersMoving_m76A80EE1817671AD55065EC255736FF9AEE849C8(L_10, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0033;
		}
	}
	{
		return (bool)0;
	}

IL_0033:
	{
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_13 = V_0;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_14 = V_1;
		bool L_15 = TwistRecognizer_FingersMovedInOppositeDirections_mE1669F2E73404178083495D10D961C67B3458FF8(__this, L_13, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_003f;
		}
	}
	{
		return (bool)0;
	}

IL_003f:
	{
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_16 = V_0;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_17 = V_1;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_18 = V_0;
		NullCheck(L_18);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = Finger_get_StartPosition_mAAE9424F2ACFD924E293DA00610E2BCE0B39BED4_inline(L_18, /*hidden argument*/NULL);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_20 = V_1;
		NullCheck(L_20);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_21 = Finger_get_StartPosition_mAAE9424F2ACFD924E293DA00610E2BCE0B39BED4_inline(L_20, /*hidden argument*/NULL);
		float L_22 = TwistRecognizer_SignedAngularGap_m41B87BFF693B4DF4B3D949B493C38045099DF6A8(L_16, L_17, L_19, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_23 = fabsf(L_22);
		float L_24 = __this->get_MinRotation_20();
		if ((!(((float)L_23) < ((float)L_24))))
		{
			goto IL_0061;
		}
	}
	{
		return (bool)0;
	}

IL_0061:
	{
		return (bool)1;
	}
}
// System.Void TwistRecognizer::OnBegin(TwistGesture,FingerGestures_IFingerList)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwistRecognizer_OnBegin_mE7C52F0A5B8676E76F0219F3276847BE8C7945D1 (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * ___gesture0, RuntimeObject* ___touches1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistRecognizer_OnBegin_mE7C52F0A5B8676E76F0219F3276847BE8C7945D1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * V_0 = NULL;
	Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * V_1 = NULL;
	{
		RuntimeObject* L_0 = ___touches1;
		NullCheck(L_0);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_1 = InterfaceFuncInvoker1< Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 *, int32_t >::Invoke(0 /* FingerGestures/Finger FingerGestures/IFingerList::get_Item(System.Int32) */, IFingerList_t09E148C8FA751B1E829E3AA97CD4DAC2EF6DB7A1_il2cpp_TypeInfo_var, L_0, 0);
		V_0 = L_1;
		RuntimeObject* L_2 = ___touches1;
		NullCheck(L_2);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_3 = InterfaceFuncInvoker1< Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 *, int32_t >::Invoke(0 /* FingerGestures/Finger FingerGestures/IFingerList::get_Item(System.Int32) */, IFingerList_t09E148C8FA751B1E829E3AA97CD4DAC2EF6DB7A1_il2cpp_TypeInfo_var, L_2, 1);
		V_1 = L_3;
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_4 = ___gesture0;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_5 = V_0;
		NullCheck(L_5);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = Finger_get_Position_m55D65B4EC87B9BD75EF265375162A1A9AFA32DE0_inline(L_5, /*hidden argument*/NULL);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_7 = V_1;
		NullCheck(L_7);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = Finger_get_Position_m55D65B4EC87B9BD75EF265375162A1A9AFA32DE0_inline(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_6, L_8, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = Vector2_op_Multiply_m2E30A54E315810911DFC2E25C700757A68AC1F38((0.5f), L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		Gesture_set_StartPosition_m72914599C23AC7622FF0CCFE09C89F62A40A4714_inline(L_4, L_10, /*hidden argument*/NULL);
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_11 = ___gesture0;
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_12 = ___gesture0;
		NullCheck(L_12);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_13 = Gesture_get_StartPosition_mCF0DBFDBBF9920DBAC9E53B2B903DC448CABCC51_inline(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Gesture_set_Position_m1A4BD357265D7780D6B980F9F569A37DC15CFE24_inline(L_11, L_13, /*hidden argument*/NULL);
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_14 = ___gesture0;
		NullCheck(L_14);
		TwistGesture_set_TotalRotation_m1D905E69C5D3B7507859E64C94BEAD1AE7C9F637_inline(L_14, (0.0f), /*hidden argument*/NULL);
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_15 = ___gesture0;
		NullCheck(L_15);
		TwistGesture_set_DeltaRotation_m416B0DDE1E3DB369F3603DA9D158C94669D20F77_inline(L_15, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// GestureRecognitionState TwistRecognizer::OnRecognize(TwistGesture,FingerGestures_IFingerList)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TwistRecognizer_OnRecognize_m006006723683B0DFB53AF9FB04EFA41BD568BDB4 (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * ___gesture0, RuntimeObject* ___touches1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistRecognizer_OnRecognize_m006006723683B0DFB53AF9FB04EFA41BD568BDB4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * V_0 = NULL;
	Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * V_1 = NULL;
	{
		RuntimeObject* L_0 = ___touches1;
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 FingerGestures/IFingerList::get_Count() */, IFingerList_t09E148C8FA751B1E829E3AA97CD4DAC2EF6DB7A1_il2cpp_TypeInfo_var, L_0);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 GestureRecognizer::get_RequiredFingerCount() */, __this);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_3 = ___gesture0;
		NullCheck(L_3);
		TwistGesture_set_DeltaRotation_m416B0DDE1E3DB369F3603DA9D158C94669D20F77_inline(L_3, (0.0f), /*hidden argument*/NULL);
		RuntimeObject* L_4 = ___touches1;
		NullCheck(L_4);
		int32_t L_5 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 FingerGestures/IFingerList::get_Count() */, IFingerList_t09E148C8FA751B1E829E3AA97CD4DAC2EF6DB7A1_il2cpp_TypeInfo_var, L_4);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 GestureRecognizer::get_RequiredFingerCount() */, __this);
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			goto IL_0029;
		}
	}
	{
		return (int32_t)(4);
	}

IL_0029:
	{
		return (int32_t)(3);
	}

IL_002b:
	{
		RuntimeObject* L_7 = ___touches1;
		NullCheck(L_7);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_8 = InterfaceFuncInvoker1< Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 *, int32_t >::Invoke(0 /* FingerGestures/Finger FingerGestures/IFingerList::get_Item(System.Int32) */, IFingerList_t09E148C8FA751B1E829E3AA97CD4DAC2EF6DB7A1_il2cpp_TypeInfo_var, L_7, 0);
		V_0 = L_8;
		RuntimeObject* L_9 = ___touches1;
		NullCheck(L_9);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_10 = InterfaceFuncInvoker1< Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 *, int32_t >::Invoke(0 /* FingerGestures/Finger FingerGestures/IFingerList::get_Item(System.Int32) */, IFingerList_t09E148C8FA751B1E829E3AA97CD4DAC2EF6DB7A1_il2cpp_TypeInfo_var, L_9, 1);
		V_1 = L_10;
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_11 = ___gesture0;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_12 = V_0;
		NullCheck(L_12);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_13 = Finger_get_Position_m55D65B4EC87B9BD75EF265375162A1A9AFA32DE0_inline(L_12, /*hidden argument*/NULL);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_14 = V_1;
		NullCheck(L_14);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_15 = Finger_get_Position_m55D65B4EC87B9BD75EF265375162A1A9AFA32DE0_inline(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_16 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_13, L_15, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_17 = Vector2_op_Multiply_m2E30A54E315810911DFC2E25C700757A68AC1F38((0.5f), L_16, /*hidden argument*/NULL);
		NullCheck(L_11);
		Gesture_set_Position_m1A4BD357265D7780D6B980F9F569A37DC15CFE24_inline(L_11, L_17, /*hidden argument*/NULL);
		FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C* L_18 = (FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C*)(FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C*)SZArrayNew(FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C_il2cpp_TypeInfo_var, (uint32_t)2);
		FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C* L_19 = L_18;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_20 = V_0;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 *)L_20);
		FingerU5BU5D_t58393EECD77E3991033DA7D51F5D357E6A3B229C* L_21 = L_19;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 *)L_22);
		IL2CPP_RUNTIME_CLASS_INIT(FingerGestures_t9DE618166E636E7EC25F553F0E3B01AA6513C95A_il2cpp_TypeInfo_var);
		bool L_23 = FingerGestures_AllFingersMoving_m76A80EE1817671AD55065EC255736FF9AEE849C8(L_21, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_0073;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0073:
	{
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_24 = ___gesture0;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_25 = V_0;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_26 = V_1;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_27 = V_0;
		NullCheck(L_27);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_28 = Finger_get_PreviousPosition_mB33AEC9F577929EF86FF44BD573E9F9C9CCAF5FC_inline(L_27, /*hidden argument*/NULL);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_29 = V_1;
		NullCheck(L_29);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_30 = Finger_get_PreviousPosition_mB33AEC9F577929EF86FF44BD573E9F9C9CCAF5FC_inline(L_29, /*hidden argument*/NULL);
		float L_31 = TwistRecognizer_SignedAngularGap_m41B87BFF693B4DF4B3D949B493C38045099DF6A8(L_25, L_26, L_28, L_30, /*hidden argument*/NULL);
		NullCheck(L_24);
		TwistGesture_set_DeltaRotation_m416B0DDE1E3DB369F3603DA9D158C94669D20F77_inline(L_24, L_31, /*hidden argument*/NULL);
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_32 = ___gesture0;
		NullCheck(L_32);
		float L_33 = TwistGesture_get_DeltaRotation_m3D7F3162B11DE8BB804284C4858450ED88D2E234_inline(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_34 = fabsf(L_33);
		float L_35 = ((Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_StaticFields*)il2cpp_codegen_static_fields_for(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var))->get_Epsilon_0();
		if ((!(((float)L_34) > ((float)L_35))))
		{
			goto IL_00b8;
		}
	}
	{
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_36 = ___gesture0;
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_37 = L_36;
		NullCheck(L_37);
		float L_38 = TwistGesture_get_TotalRotation_m9D341DE733C95B8D7A7490CEFA00AD9C9BF91CBC_inline(L_37, /*hidden argument*/NULL);
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_39 = ___gesture0;
		NullCheck(L_39);
		float L_40 = TwistGesture_get_DeltaRotation_m3D7F3162B11DE8BB804284C4858450ED88D2E234_inline(L_39, /*hidden argument*/NULL);
		NullCheck(L_37);
		TwistGesture_set_TotalRotation_m1D905E69C5D3B7507859E64C94BEAD1AE7C9F637_inline(L_37, ((float)il2cpp_codegen_add((float)L_38, (float)L_40)), /*hidden argument*/NULL);
		TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * L_41 = ___gesture0;
		GestureRecognizer_1_RaiseEvent_mAFE9CABB4E3EEBB3535EB148C18051F460CDF538(__this, L_41, /*hidden argument*/GestureRecognizer_1_RaiseEvent_mAFE9CABB4E3EEBB3535EB148C18051F460CDF538_RuntimeMethod_var);
	}

IL_00b8:
	{
		return (int32_t)(2);
	}
}
// System.Boolean TwistRecognizer::FingersMovedInOppositeDirections(FingerGestures_Finger,FingerGestures_Finger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TwistRecognizer_FingersMovedInOppositeDirections_mE1669F2E73404178083495D10D961C67B3458FF8 (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * ___finger00, Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * ___finger11, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistRecognizer_FingersMovedInOppositeDirections_mE1669F2E73404178083495D10D961C67B3458FF8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_0 = ___finger00;
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_1 = ___finger11;
		float L_2 = __this->get_MinDOT_19();
		IL2CPP_RUNTIME_CLASS_INIT(FingerGestures_t9DE618166E636E7EC25F553F0E3B01AA6513C95A_il2cpp_TypeInfo_var);
		bool L_3 = FingerGestures_FingersMovedInOppositeDirections_m667304A1A8774995D8D965FCB254B4D89EA3F5D3(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single TwistRecognizer::SignedAngularGap(FingerGestures_Finger,FingerGestures_Finger,UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TwistRecognizer_SignedAngularGap_m41B87BFF693B4DF4B3D949B493C38045099DF6A8 (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * ___finger00, Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * ___finger11, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___refPos02, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___refPos13, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistRecognizer_SignedAngularGap_m41B87BFF693B4DF4B3D949B493C38045099DF6A8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_0 = ___finger00;
		NullCheck(L_0);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = Finger_get_Position_m55D65B4EC87B9BD75EF265375162A1A9AFA32DE0_inline(L_0, /*hidden argument*/NULL);
		Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * L_2 = ___finger11;
		NullCheck(L_2);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = Finger_get_Position_m55D65B4EC87B9BD75EF265375162A1A9AFA32DE0_inline(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_1, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_2), /*hidden argument*/NULL);
		V_0 = L_5;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = ___refPos02;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = ___refPos13;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_2), /*hidden argument*/NULL);
		V_1 = L_9;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FingerGestures_t9DE618166E636E7EC25F553F0E3B01AA6513C95A_il2cpp_TypeInfo_var);
		float L_12 = FingerGestures_SignedAngle_m4FF40555C84A4382AABD859DFC66CDA234873413(L_10, L_11, /*hidden argument*/NULL);
		return ((float)il2cpp_codegen_multiply((float)(57.29578f), (float)L_12));
	}
}
// System.Void TwistRecognizer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwistRecognizer__ctor_m77C8BF5B07685C6D3E94D2BCDACC01ECC43101EA (TwistRecognizer_t0AE30155D44D67CB2939D60F48595482C542C76F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistRecognizer__ctor_m77C8BF5B07685C6D3E94D2BCDACC01ECC43101EA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_MinDOT_19((-0.7f));
		__this->set_MinRotation_20((1.0f));
		ContinuousGestureRecognizer_1__ctor_m12C4DCFD6DFA8F812F6177460F971C36A304DD81(__this, /*hidden argument*/ContinuousGestureRecognizer_1__ctor_m12C4DCFD6DFA8F812F6177460F971C36A304DD81_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UI_Handler::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_Awake_mC9DFFBFCEF18031AEEDF0E8850A16105E196A0CB (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Handler_Awake_mC9DFFBFCEF18031AEEDF0E8850A16105E196A0CB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0_StaticFields*)il2cpp_codegen_static_fields_for(UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0_il2cpp_TypeInfo_var))->set_instance_5(__this);
		return;
	}
}
// System.Void UI_Handler::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_Start_m41A544F025231321FC2EEC746ECEE6D203753CD8 (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UI_Handler::OnExitClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_OnExitClick_m584E6AE28BC79582B9A032ECBB4AD2FF144661A4 (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	{
		Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95(/*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Handler::OnReset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_OnReset_mA7ED1BCBEE72E36143BEA8259BEC3E54E5B1F6E6 (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Handler_OnReset_mA7ED1BCBEE72E36143BEA8259BEC3E54E5B1F6E6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m9721867D46BC827D58271AD235267B0B0865F115(_stringLiteral5F1E3AFE2CF259DD3F9C6FB6F22932BD11509FCD, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Handler::On3DSceneClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_On3DSceneClick_m2A5ACCDB7DF4E4E4E4EA738A416378C8FCBF4FD5 (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Handler_On3DSceneClick_m2A5ACCDB7DF4E4E4E4EA738A416378C8FCBF4FD5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m9721867D46BC827D58271AD235267B0B0865F115(_stringLiteral19240DE083AA9C9CB77116EF24DE8D203AB3A53A, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Handler::OnARSceneClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_OnARSceneClick_mE5C8B143B9FED7FBA703D1393A82E8E884E80986 (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Handler_OnARSceneClick_mE5C8B143B9FED7FBA703D1393A82E8E884E80986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m9721867D46BC827D58271AD235267B0B0865F115(_stringLiteral1163609AE835154467B744B94AEDBF951BC71257, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Handler::OnHomeClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_OnHomeClick_m47FDD3426389FF4F3AFCCCF2C842A497399C5890 (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Handler_OnHomeClick_m47FDD3426389FF4F3AFCCCF2C842A497399C5890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m9721867D46BC827D58271AD235267B0B0865F115(_stringLiteralEABCFFD8D5120B660823E2C294A8DC252DA5EA29, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Handler::OnCaptureClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_OnCaptureClick_m6C29B8363D03892BDE3A990D6A2F35C5BE1804F7 (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = UI_Handler_TakeScreenshotAndSave_m91BEA244D8F332A3C3B835E4612CA4BE4A11F68F(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UI_Handler::TakeScreenshotAndSave()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UI_Handler_TakeScreenshotAndSave_m91BEA244D8F332A3C3B835E4612CA4BE4A11F68F (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Handler_TakeScreenshotAndSave_m91BEA244D8F332A3C3B835E4612CA4BE4A11F68F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A * L_0 = (U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A *)il2cpp_codegen_object_new(U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A_il2cpp_TypeInfo_var);
		U3CTakeScreenshotAndSaveU3Ed__12__ctor_mE4ECA813184992818F861079C0D0AF3DB077616D(L_0, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UI_Handler::CamSwitch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_CamSwitch_m32188B78B2FBA776C403EA5D760878368A5125EF (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UI_Handler::OnSkipButtonClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_OnSkipButtonClick_m95570A90E438B5656A52E79262F05546293DC19E (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UI_Handler::ResetFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler_ResetFunction_mA050539AB12E591B950C3FD8F6DAF88B034112F6 (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	{
		TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * L_0 = __this->get_tbidel_4();
		NullCheck(L_0);
		TBOrbit_ResetPanning_m61C00B2A917391A22FA9DF81C7D09862D114F2C1(L_0, /*hidden argument*/NULL);
		TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * L_1 = __this->get_tbidel_4();
		NullCheck(L_1);
		TBOrbit_set_IdealYaw_m3852EC8F27C486DA0C578E4B3F84A9CEA985663F(L_1, (0.0f), /*hidden argument*/NULL);
		TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * L_2 = __this->get_tbidel_4();
		NullCheck(L_2);
		TBOrbit_set_IdealPitch_m012C2A78264759B76D23EA69316D51CCD6EB2838(L_2, (18.0f), /*hidden argument*/NULL);
		TBOrbit_t581F6AF5F4B81F6EAACED5EA276A10730CED0638 * L_3 = __this->get_tbidel_4();
		NullCheck(L_3);
		TBOrbit_set_IdealDistance_m7FCF141D84D04B66F0E5C836224DC98D7B5A0D4D(L_3, (55.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Handler::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UI_Handler__ctor_m6DC2E7F9BD502D08A0456C5D09A71AA165F407E9 (UI_Handler_t87581B224D4EE6F1B1A72DC968DC4C271CC1CFB0 * __this, const RuntimeMethod* method)
{
	{
		__this->set_on_6((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UI_Handler_<TakeScreenshotAndSave>d__12::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTakeScreenshotAndSaveU3Ed__12__ctor_mE4ECA813184992818F861079C0D0AF3DB077616D (U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void UI_Handler_<TakeScreenshotAndSave>d__12::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTakeScreenshotAndSaveU3Ed__12_System_IDisposable_Dispose_m0B01AFD3FFD412794F788643981EF8761A544056 (U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UI_Handler_<TakeScreenshotAndSave>d__12::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTakeScreenshotAndSaveU3Ed__12_MoveNext_mCDF28BC374BEF35FA986F261A6A918F61045D239 (U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeScreenshotAndSaveU3Ed__12_MoveNext_mCDF28BC374BEF35FA986F261A6A918F61045D239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * L_3 = (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA *)il2cpp_codegen_object_new(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B(L_3, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_3);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_002b:
	{
		__this->set_U3CU3E1__state_0((-1));
		int32_t L_4 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_6 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_6, L_4, L_5, 3, (bool)0, /*hidden argument*/NULL);
		V_1 = L_6;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_7 = V_1;
		int32_t L_8 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		int32_t L_9 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_10), (0.0f), (0.0f), (((float)((float)L_8))), (((float)((float)L_9))), /*hidden argument*/NULL);
		NullCheck(L_7);
		Texture2D_ReadPixels_m1ED1C11E41D0ACC8CFCABBD25946CF0BD16D4F61(L_7, L_10, 0, 0, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_11 = V_1;
		NullCheck(L_11);
		Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA(L_11, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(NativeGallery_tC3173DEEAA2D55A1D49692A7842083EF31CD702B_il2cpp_TypeInfo_var);
		int32_t L_13 = NativeGallery_SaveImageToGallery_m29D2A7A8C05ECB2B99DAF4B82A86FA59E21F6A1A(L_12, _stringLiteral73E1AF46B7D5AACD372A6870A6845EBECF861716, _stringLiteral2338BEB372E67F5564A8B8066F54718A3E9E3155, (MediaSaveCallback_t489912AD9BCB0FD2B00ADCC25F89B8715D2F219E *)NULL, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Permission_t4CB85837235D3D73E44E0937DFBD99BC7D466C91_il2cpp_TypeInfo_var, &L_14);
		String_t* L_16 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral760D542CE497A54CB36ACFB2A671DD229132CF03, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_16, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_17, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Object UI_Handler_<TakeScreenshotAndSave>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m621132C7554A10AF0038A508745FF0FBE6B8693C (U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UI_Handler_<TakeScreenshotAndSave>d__12::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_IEnumerator_Reset_mCD6C81342223466277E49C045FF45BBCBEA58DC5 (U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_IEnumerator_Reset_mCD6C81342223466277E49C045FF45BBCBEA58DC5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_IEnumerator_Reset_mCD6C81342223466277E49C045FF45BBCBEA58DC5_RuntimeMethod_var);
	}
}
// System.Object UI_Handler_<TakeScreenshotAndSave>d__12::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTakeScreenshotAndSaveU3Ed__12_System_Collections_IEnumerator_get_Current_m12B05A4869F5B19BADC1E46BDCB4141BC5F4153B (U3CTakeScreenshotAndSaveU3Ed__12_tC82E2B463D13B3989BBF0EA25016FA122581902A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldCupOneTracker::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupOneTracker_Start_mF6C593BBEAE553C9200ACF0FBA90094169A05DCF (WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCupOneTracker_Start_mF6C593BBEAE553C9200ACF0FBA90094169A05DCF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9_StaticFields*)il2cpp_codegen_static_fields_for(WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9_il2cpp_TypeInfo_var))->set_WorldCupOneTrackerthis_4(__this);
		return;
	}
}
// System.Void WorldCupOneTracker::AnimationAndSoundFunction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3 (WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * __this, String_t* ___name0, bool ___condition1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCupOneTracker_AnimationAndSoundFunction_m6871A94971A32A2CCABBDD21AA834852868454A3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0117;
	}

IL_0007:
	{
		bool L_0 = ___condition1;
		if (!L_0)
		{
			goto IL_0045;
		}
	}
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_1 = __this->get_mainObject_6();
		NullCheck(L_1);
		int32_t L_2 = 0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get_JKLaxmiUIPanel_7();
		NullCheck(L_4);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_4, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_5 = __this->get_mainRotateButton_8();
		NullCheck(L_5);
		int32_t L_6 = 0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_7, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_8 = __this->get_animationButton_9();
		NullCheck(L_8);
		int32_t L_9 = 0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_10, (bool)1, /*hidden argument*/NULL);
		goto IL_0113;
	}

IL_0045:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_11 = __this->get_mainObject_6();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_14, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_15 = __this->get_JKLaxmiUIPanel_7();
		NullCheck(L_15);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_15, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_16 = __this->get_mainRotateButton_8();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_19);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_19, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_20 = __this->get_animationButton_9();
		NullCheck(L_20);
		int32_t L_21 = 0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_22, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_23 = __this->get_mainObject_6();
		int32_t L_24 = V_0;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_26);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_27 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_26, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28;
		memset((&L_28), 0, sizeof(L_28));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_28), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_27, L_28, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_29 = __this->get_mainObject_6();
		int32_t L_30 = V_0;
		NullCheck(L_29);
		int32_t L_31 = L_30;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_33 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_34 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_33, L_34, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_35 = __this->get_mainObject_6();
		int32_t L_36 = V_0;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_38);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_39 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_38, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_40;
		memset((&L_40), 0, sizeof(L_40));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_40), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_39, L_40, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_41 = __this->get_mainObject_6();
		int32_t L_42 = V_0;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_44);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_45 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_44, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_46;
		memset((&L_46), 0, sizeof(L_46));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_46), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_45, L_46, /*hidden argument*/NULL);
	}

IL_0113:
	{
		int32_t L_47 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1));
	}

IL_0117:
	{
		int32_t L_48 = V_0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_49 = __this->get_TrackerName_5();
		NullCheck(L_49);
		if ((((int32_t)L_48) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_49)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WorldCupOneTracker::OnPlayerButtonClick(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupOneTracker_OnPlayerButtonClick_m1F83130B2F2EEE2A83A3B08F39032537DF0B287A (WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * __this, int32_t ___btn0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCupOneTracker_OnPlayerButtonClick_m1F83130B2F2EEE2A83A3B08F39032537DF0B287A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0105;
	}

IL_0007:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_0 = __this->get_mainObject_6();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_4 = __this->get_mainObject_6();
		int32_t L_5 = ___btn0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0065;
		}
	}
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_9 = __this->get_mainObject_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_12, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_13 = __this->get_mainObject_6();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * L_17 = GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8(L_16, /*hidden argument*/GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8_RuntimeMethod_var);
		NullCheck(L_17);
		Animation_Play_m743CCCF04B64977460915D9E7007B0859BDF6AE9(L_17, _stringLiteral9E253470C876EE6D5C720EB777AEB82D4C26E28F, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_18 = __this->get_mainRotateButton_8();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_21, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_22 = __this->get_animationButton_9();
		int32_t L_23 = V_0;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_25, (bool)1, /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_0065:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_26 = __this->get_mainObject_6();
		int32_t L_27 = V_0;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_29, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_30 = __this->get_mainRotateButton_8();
		int32_t L_31 = V_0;
		NullCheck(L_30);
		int32_t L_32 = L_31;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_33);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_33, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_34 = __this->get_animationButton_9();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_37, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_38 = __this->get_mainObject_6();
		int32_t L_39 = V_0;
		NullCheck(L_38);
		int32_t L_40 = L_39;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		NullCheck(L_41);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_42 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_41, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_43;
		memset((&L_43), 0, sizeof(L_43));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_43), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_42);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_42, L_43, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_44 = __this->get_mainObject_6();
		int32_t L_45 = V_0;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_48 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_49 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_48, L_49, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_50 = __this->get_mainObject_6();
		int32_t L_51 = V_0;
		NullCheck(L_50);
		int32_t L_52 = L_51;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		NullCheck(L_53);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_54 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_53, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_55;
		memset((&L_55), 0, sizeof(L_55));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_55), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_54, L_55, /*hidden argument*/NULL);
	}

IL_0101:
	{
		int32_t L_56 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_56, (int32_t)1));
	}

IL_0105:
	{
		int32_t L_57 = V_0;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_58 = __this->get_mainObject_6();
		NullCheck(L_58);
		if ((((int32_t)L_57) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_58)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WorldCupOneTracker::OnPlayerRotateButtonClick(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupOneTracker_OnPlayerRotateButtonClick_mE413E604F73A811ABB1D44A5C2DDF34ECA4FE552 (WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * __this, int32_t ___btn0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCupOneTracker_OnPlayerRotateButtonClick_mE413E604F73A811ABB1D44A5C2DDF34ECA4FE552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0108;
	}

IL_0007:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_0 = __this->get_mainObject_6();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_4 = __this->get_mainObject_6();
		int32_t L_5 = ___btn0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0092;
		}
	}
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_9 = __this->get_mainObject_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_14 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((-90.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_13, L_14, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_15 = __this->get_mainObject_6();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_18);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_18, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_20), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_19, L_20, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_21 = __this->get_mainObject_6();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_24);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_25 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_24, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26;
		memset((&L_26), 0, sizeof(L_26));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_26), (0.0f), (0.3f), (-0.9f), /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_25, L_26, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_0092:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_27 = __this->get_mainObject_6();
		int32_t L_28 = V_0;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck(L_30);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_31 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_30, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32;
		memset((&L_32), 0, sizeof(L_32));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_32), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_31, L_32, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_33 = __this->get_mainObject_6();
		int32_t L_34 = V_0;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_36);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_37 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_38 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_37, L_38, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_39 = __this->get_mainObject_6();
		int32_t L_40 = V_0;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		NullCheck(L_42);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_43 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_42, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_44;
		memset((&L_44), 0, sizeof(L_44));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_44), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_43, L_44, /*hidden argument*/NULL);
	}

IL_0104:
	{
		int32_t L_45 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
	}

IL_0108:
	{
		int32_t L_46 = V_0;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_47 = __this->get_mainObject_6();
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_47)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WorldCupOneTracker::OnPlayerAnimationButtonClick(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupOneTracker_OnPlayerAnimationButtonClick_m10B4471F1D9E2524485619C04F6BA877895DE9B3 (WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * __this, int32_t ___btn0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCupOneTracker_OnPlayerAnimationButtonClick_m10B4471F1D9E2524485619C04F6BA877895DE9B3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_004c;
	}

IL_0004:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_0 = __this->get_mainObject_6();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_4 = __this->get_mainObject_6();
		int32_t L_5 = ___btn0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0030;
		}
	}
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_9 = __this->get_mainObject_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * L_13 = GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8(L_12, /*hidden argument*/GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8_RuntimeMethod_var);
		NullCheck(L_13);
		Animation_Play_mEC9F6C1F931E11D4D69043AD44CC71698B776A51(L_13, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_0030:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_14 = __this->get_mainObject_6();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * L_18 = GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8(L_17, /*hidden argument*/GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8_RuntimeMethod_var);
		NullCheck(L_18);
		Animation_Play_m743CCCF04B64977460915D9E7007B0859BDF6AE9(L_18, _stringLiteral9E253470C876EE6D5C720EB777AEB82D4C26E28F, /*hidden argument*/NULL);
	}

IL_0048:
	{
		int32_t L_19 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_004c:
	{
		int32_t L_20 = V_0;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_21 = __this->get_mainObject_6();
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_21)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void WorldCupOneTracker::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupOneTracker__ctor_mD16845E1E743BF1613B38EC83717AE1683613CA7 (WorldCupOneTracker_t7833279742FCBED4AACB48B01965742022662EC9 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldCupTwoTracker::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupTwoTracker_Start_m994B58169EAB8550B3320024CFDA6C4825103467 (WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCupTwoTracker_Start_m994B58169EAB8550B3320024CFDA6C4825103467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF_StaticFields*)il2cpp_codegen_static_fields_for(WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF_il2cpp_TypeInfo_var))->set_WorldCupTwoTrackerthis_4(__this);
		return;
	}
}
// System.Void WorldCupTwoTracker::AnimationAndSoundFunction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9 (WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * __this, String_t* ___name0, bool ___condition1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCupTwoTracker_AnimationAndSoundFunction_m55AC7A7FCFB9B53C44B9D6D4E9389CE858E0E6F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0117;
	}

IL_0007:
	{
		bool L_0 = ___condition1;
		if (!L_0)
		{
			goto IL_0045;
		}
	}
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_1 = __this->get_mainObject_6();
		NullCheck(L_1);
		int32_t L_2 = 0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get_JKLaxmiUIPanel_7();
		NullCheck(L_4);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_4, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_5 = __this->get_mainRotateButton_8();
		NullCheck(L_5);
		int32_t L_6 = 0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_7, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_8 = __this->get_animationButton_9();
		NullCheck(L_8);
		int32_t L_9 = 0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_10, (bool)1, /*hidden argument*/NULL);
		goto IL_0113;
	}

IL_0045:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_11 = __this->get_mainObject_6();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_14, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_15 = __this->get_JKLaxmiUIPanel_7();
		NullCheck(L_15);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_15, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_16 = __this->get_mainRotateButton_8();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_19);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_19, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_20 = __this->get_animationButton_9();
		NullCheck(L_20);
		int32_t L_21 = 0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_22, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_23 = __this->get_mainObject_6();
		int32_t L_24 = V_0;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_26);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_27 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_26, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28;
		memset((&L_28), 0, sizeof(L_28));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_28), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_27, L_28, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_29 = __this->get_mainObject_6();
		int32_t L_30 = V_0;
		NullCheck(L_29);
		int32_t L_31 = L_30;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_33 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_34 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_33, L_34, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_35 = __this->get_mainObject_6();
		int32_t L_36 = V_0;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_38);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_39 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_38, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_40;
		memset((&L_40), 0, sizeof(L_40));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_40), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_39, L_40, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_41 = __this->get_mainObject_6();
		int32_t L_42 = V_0;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_44);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_45 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_44, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_46;
		memset((&L_46), 0, sizeof(L_46));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_46), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_45, L_46, /*hidden argument*/NULL);
	}

IL_0113:
	{
		int32_t L_47 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1));
	}

IL_0117:
	{
		int32_t L_48 = V_0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_49 = __this->get_TrackerName_5();
		NullCheck(L_49);
		if ((((int32_t)L_48) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_49)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WorldCupTwoTracker::OnPlayerButtonClick(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupTwoTracker_OnPlayerButtonClick_m299722C1E95EB77FFD9AB56953380C98189CC3D6 (WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * __this, int32_t ___btn0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCupTwoTracker_OnPlayerButtonClick_m299722C1E95EB77FFD9AB56953380C98189CC3D6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0105;
	}

IL_0007:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_0 = __this->get_mainObject_6();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_4 = __this->get_mainObject_6();
		int32_t L_5 = ___btn0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0065;
		}
	}
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_9 = __this->get_mainObject_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_12, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_13 = __this->get_mainObject_6();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * L_17 = GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8(L_16, /*hidden argument*/GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8_RuntimeMethod_var);
		NullCheck(L_17);
		Animation_Play_m743CCCF04B64977460915D9E7007B0859BDF6AE9(L_17, _stringLiteral9E253470C876EE6D5C720EB777AEB82D4C26E28F, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_18 = __this->get_mainRotateButton_8();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_21, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_22 = __this->get_animationButton_9();
		int32_t L_23 = V_0;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_25, (bool)1, /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_0065:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_26 = __this->get_mainObject_6();
		int32_t L_27 = V_0;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_29, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_30 = __this->get_mainRotateButton_8();
		int32_t L_31 = V_0;
		NullCheck(L_30);
		int32_t L_32 = L_31;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_33);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_33, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_34 = __this->get_animationButton_9();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_37, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_38 = __this->get_mainObject_6();
		int32_t L_39 = V_0;
		NullCheck(L_38);
		int32_t L_40 = L_39;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		NullCheck(L_41);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_42 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_41, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_43;
		memset((&L_43), 0, sizeof(L_43));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_43), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_42);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_42, L_43, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_44 = __this->get_mainObject_6();
		int32_t L_45 = V_0;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_48 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_49 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_48, L_49, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_50 = __this->get_mainObject_6();
		int32_t L_51 = V_0;
		NullCheck(L_50);
		int32_t L_52 = L_51;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		NullCheck(L_53);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_54 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_53, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_55;
		memset((&L_55), 0, sizeof(L_55));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_55), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_54, L_55, /*hidden argument*/NULL);
	}

IL_0101:
	{
		int32_t L_56 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_56, (int32_t)1));
	}

IL_0105:
	{
		int32_t L_57 = V_0;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_58 = __this->get_mainObject_6();
		NullCheck(L_58);
		if ((((int32_t)L_57) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_58)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WorldCupTwoTracker::OnPlayerRotateButtonClick(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupTwoTracker_OnPlayerRotateButtonClick_mC46776A5E869E613CDEBB160EF7398C216481237 (WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * __this, int32_t ___btn0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCupTwoTracker_OnPlayerRotateButtonClick_mC46776A5E869E613CDEBB160EF7398C216481237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0108;
	}

IL_0007:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_0 = __this->get_mainObject_6();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_4 = __this->get_mainObject_6();
		int32_t L_5 = ___btn0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0092;
		}
	}
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_9 = __this->get_mainObject_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_14 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((-90.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_13, L_14, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_15 = __this->get_mainObject_6();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_18);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_18, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_20), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_19, L_20, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_21 = __this->get_mainObject_6();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_24);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_25 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_24, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26;
		memset((&L_26), 0, sizeof(L_26));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_26), (0.0f), (0.3f), (-0.9f), /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_25, L_26, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_0092:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_27 = __this->get_mainObject_6();
		int32_t L_28 = V_0;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck(L_30);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_31 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_30, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32;
		memset((&L_32), 0, sizeof(L_32));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_32), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_31, L_32, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_33 = __this->get_mainObject_6();
		int32_t L_34 = V_0;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_36);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_37 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_38 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_37, L_38, /*hidden argument*/NULL);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_39 = __this->get_mainObject_6();
		int32_t L_40 = V_0;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		NullCheck(L_42);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_43 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_42, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_44;
		memset((&L_44), 0, sizeof(L_44));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_44), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_43, L_44, /*hidden argument*/NULL);
	}

IL_0104:
	{
		int32_t L_45 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
	}

IL_0108:
	{
		int32_t L_46 = V_0;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_47 = __this->get_mainObject_6();
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_47)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WorldCupTwoTracker::OnPlayerAnimationButtonClick(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupTwoTracker_OnPlayerAnimationButtonClick_mAB802B57334169446EB7DD3CFDCB0D124318C5C4 (WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * __this, int32_t ___btn0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCupTwoTracker_OnPlayerAnimationButtonClick_mAB802B57334169446EB7DD3CFDCB0D124318C5C4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_004c;
	}

IL_0004:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_0 = __this->get_mainObject_6();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_4 = __this->get_mainObject_6();
		int32_t L_5 = ___btn0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0030;
		}
	}
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_9 = __this->get_mainObject_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * L_13 = GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8(L_12, /*hidden argument*/GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8_RuntimeMethod_var);
		NullCheck(L_13);
		Animation_Play_mEC9F6C1F931E11D4D69043AD44CC71698B776A51(L_13, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_0030:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_14 = __this->get_mainObject_6();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * L_18 = GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8(L_17, /*hidden argument*/GameObject_GetComponent_TisAnimation_tCFC171459D159DDEC6500B55543A76219D49BB9C_mC132405E14FD78B76E2EAD53C6FC89F328BEF9E8_RuntimeMethod_var);
		NullCheck(L_18);
		Animation_Play_m743CCCF04B64977460915D9E7007B0859BDF6AE9(L_18, _stringLiteral9E253470C876EE6D5C720EB777AEB82D4C26E28F, /*hidden argument*/NULL);
	}

IL_0048:
	{
		int32_t L_19 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_004c:
	{
		int32_t L_20 = V_0;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_21 = __this->get_mainObject_6();
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_21)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void WorldCupTwoTracker::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldCupTwoTracker__ctor_m0863875911C8AF93266BC44B8B6B972FAB40891C (WorldCupTwoTracker_tB65791E797A6E9FCD4EEDC0E9CB5C178EAB203DF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* TrackableBehaviour_get_TrackableName_m04FE2B8C75F6E62F42C6121E61EFB7C3B6676835_inline (TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_mTrackableName_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Gesture_get_StartSelection_m2F671A58813B69F4E401DC2337BD85FB886B9A8C_inline (Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD * __this, const RuntimeMethod* method)
{
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_startSelection_9();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Finger_get_StartPosition_mAAE9424F2ACFD924E293DA00610E2BCE0B39BED4_inline (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * __this, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_startPos_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Finger_get_Position_m55D65B4EC87B9BD75EF265375162A1A9AFA32DE0_inline (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * __this, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_pos_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Gesture_set_StartPosition_m72914599C23AC7622FF0CCFE09C89F62A40A4714_inline (Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___value0;
		__this->set_startPosition_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Gesture_get_StartPosition_mCF0DBFDBBF9920DBAC9E53B2B903DC448CABCC51_inline (Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD * __this, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_startPosition_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Gesture_set_Position_m1A4BD357265D7780D6B980F9F569A37DC15CFE24_inline (Gesture_t582A04A476150206506CAAB54FFFCF5B7A01BFDD * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___value0;
		__this->set_position_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TwistGesture_set_TotalRotation_m1D905E69C5D3B7507859E64C94BEAD1AE7C9F637_inline (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_totalRotation_13(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TwistGesture_set_DeltaRotation_m416B0DDE1E3DB369F3603DA9D158C94669D20F77_inline (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_deltaRotation_12(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Finger_get_PreviousPosition_mB33AEC9F577929EF86FF44BD573E9F9C9CCAF5FC_inline (Finger_t7098C9E2F7DAB1660D72ED2E2AED6FF326F5C435 * __this, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_prevPos_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float TwistGesture_get_DeltaRotation_m3D7F3162B11DE8BB804284C4858450ED88D2E234_inline (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_deltaRotation_12();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float TwistGesture_get_TotalRotation_m9D341DE733C95B8D7A7490CEFA00AD9C9BF91CBC_inline (TwistGesture_tF329F07167A766D3D55EA1E26E581A9A5A59E9E1 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_totalRotation_13();
		return L_0;
	}
}
