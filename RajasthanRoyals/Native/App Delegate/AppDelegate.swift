//
//  AppDelegate.swift
//  Cricket Club
//
//  Created by Sagar Nandha on 05/07/18.
//  Copyright © 2018 Sagar Nandha. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Reachability
import UserNotifications
import Firebase
import FirebaseMessaging
import Reachability
import KRProgressHUD
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookCore
import FacebookLogin
import Fabric
import Crashlytics
import TwitterKit
import Google
import GoogleSignIn

class AppDelegate: NSObject, UNUserNotificationCenterDelegate, GIDSignInDelegate {
    
    var window: UIWindow?
    var storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    @objc var webpageURL: NSURL!
    @objc var userInfo = [String: AnyObject]()
    
    @objc var isLoadHomeScreen = false
    var isInSplashScreen = false
    var isCheckVersionFirstTimeCall = false
    var isEmailVerifyFirstTime = false
    var isCallPointsAPI = false
    
    var reachable: Reachability!
    @objc var deviceToken = ""
    @objc var authKey = ""
    
    var userDetails = [String: AnyObject]()
    var generalSettings = [String: AnyObject]()
    
    var podcastList = [[String: AnyObject]]()
    var podcastIndex = 0
    
    var maskShopCartItem = 0
    var marchandiseShopCartItem = 0
    
    var myStoriesList = [[String: AnyObject]]()
    var storiesList = [[String: AnyObject]]()
    var storyList = [[String: AnyObject]]()
    var mainBenefitsList = [[String: AnyObject]]()
    
    func startApplication() {
        self.window = UIApplication.shared.keyWindow
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarTintColor = UIColor().alertButtonColor
           
        KRProgressHUD.set(activityIndicatorViewColors: [UIColor(named: "Pink_Color")!])
        if let font = UIFont.init(name: "Futura", size: 14.0) {
            KRProgressHUD.set(font: font)
        }
        
        UITextField.appearance().tintColor = UIColor().alertButtonColor
        UITextView.appearance().tintColor = UIColor().alertButtonColor

        // Customize the colors
        AMTabView.settings.ballColor = UIColor.init(hex: "FFFFFF")
        AMTabView.settings.tabColor = #colorLiteral(red: 0.9960784314, green: 0.9960784314, blue: 0.9960784314, alpha: 1)
        AMTabView.settings.selectedTabTintColor = UIColor(named: "Pink_Color")!
        AMTabView.settings.unSelectedTabTintColor = UIColor.init(hex: "605E63")

        // Chnage the animation duration
        AMTabView.settings.animationDuration = 1

        do {
           try reachable = Reachability.init()
        }
        catch {
           print("error in init Reachability")
        }

        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: Notification.Name.reachabilityChanged, object: reachable)
        do {
           try reachable.startNotifier()
        }
        catch {
           print("could not start reachability notifier")
        }

//        self.clearAllNotifications()
        
        //Initialize Google Sign-In
        GIDSignIn.sharedInstance()?.clientID = googleClientId
        
        //Intialization Twitter
        TWTRTwitter.sharedInstance().start(withConsumerKey: twitterConsumerKey, consumerSecret: twitterConsumerSecret)
        
        if let deviceToken = defaults.value(forKey: kDeviceToken) as? String {
            BaseViewController.sharedInstance.appDelegate.deviceToken = deviceToken
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if self.isInSplashScreen {
            if let authKey = defaults.value(forKey: kAuthKey) as? String {
                BaseViewController.sharedInstance.appDelegate.authKey = authKey
                
                if let data = UserDefaults.standard.object(forKey: kUserDetails) as? Data {
                    if let userDetails = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String: AnyObject] {
                        BaseViewController.sharedInstance.appDelegate.userDetails = userDetails
                    }
                }
                
                if BaseViewController.sharedInstance.appDelegate.authKey == "" {
                    BaseViewController.sharedInstance.navigateToWelcomScreen()
                }
                else {
                    BaseViewController.sharedInstance.navigateToHomeScreen()
                }
            }
            else {
                BaseViewController.sharedInstance.navigateToWelcomScreen()
            }
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("UserInfo : \(response.notification.request.content.userInfo)")
        
        if response.notification.request.content.userInfo.count > 0 {
            self.setScreenFromNotificationResponse(dictNotification: response.notification.request.content.userInfo)
        }
    }
    
    //This method will call when app is in Foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("UserInfo : \(notification.request.content.userInfo)")
        
        completionHandler(
            [UNNotificationPresentationOptions.alert,
             UNNotificationPresentationOptions.sound,
             UNNotificationPresentationOptions.badge])
    }
    
    func setScreenFromNotificationResponse(dictNotification : [AnyHashable : Any]) {
//        let _ : Dictionary<String, AnyObject> = dictNotification["aps"] as! Dictionary<String, AnyObject>
        if (defaults.value(forKey: kUserDetails) as? Data) != nil { //If Already Login User then only
            if let _ : String = dictNotification["gcm.notification.type"] as? String {
            }
        }
    }
    
    func clearAllNotifications() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            //            center.removeAllPendingNotificationRequests() // To remove all   pending notifications which are not delivered yet but scheduled.
            center.removeAllDeliveredNotifications() // To remove all delivered notifications
        }
        else {
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }
    
    //MARK: - Reachability Method
    @objc func reachabilityChanged(note: NSNotification) {
        let reachable = note.object as! Reachability

        if reachable.connection != .unavailable {
            if reachable.connection != .wifi {
                print("Reachable via WiFi")
            }
            else {
                print("Reachable via Cellular")
            }
        }
        else {
            displayAlert(title: AlertViewTitle, message: NoInternet)
        }
    }
    
    func displayAlert(title : String, message : String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.tintColor = UIColor().alertButtonColor
        alert.addAction(UIAlertAction(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .cancel, handler: nil));
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    //GIDSignInDelegate Method
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print(error as Any)
    }
}
