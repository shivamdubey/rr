//
//  ViewController.swift
//  YouTubePlayerExample
//
//  Created by Giles Van Gruisen on 1/31/15.
//  Copyright (c) 2015 Giles Van Gruisen. All rights reserved.
//

import UIKit
import YouTubePlayer

class YouTubePlayerViewController: BaseViewController {

    @IBOutlet var playerView: YouTubePlayerView!
    
    var videoId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.playerView.playerVars = ["playsinline": "1", "controls": "0", "showinfo": "0"] as YouTubePlayerView.YouTubePlayerParameters
        self.playerView.loadVideoID(videoId)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
