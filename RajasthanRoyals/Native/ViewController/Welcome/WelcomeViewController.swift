//
//  OTPViewController.swift
//  RR
//
//  Created by Sagar on 15/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit
import CarbonKit

class WelcomeViewController: BaseViewController {
    
    @IBOutlet weak var vwTarget: UIView!
    
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "UpdateFirebseDeviceToken"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateFirebseDeviceToken), name: NSNotification.Name(rawValue: "UpdateFirebseDeviceToken"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SelectSpacificIndex"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectSpacifiIndex(notification:)), name: NSNotification.Name(rawValue: "SelectSpacificIndex"), object: nil)
        
        self.carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: ["", "", ""], delegate: self)
        carbonTabSwipeNavigation.toolbarHeight.constant = 0
        carbonTabSwipeNavigation.delegate = self
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: self.vwTarget)
        
        if defaults.string(forKey: "isAppAlreadyLaunchedOnce") == nil {
            BaseViewController.sharedInstance.storeAppInstallDataOnCRM()
            BaseViewController.sharedInstance.appInstalled()
        }
    }
    
    @objc func updateFirebseDeviceToken() {
        if let authKey = defaults.value(forKey: kAuthKey) as? String {
            BaseViewController.sharedInstance.appDelegate.authKey = authKey
            if !BaseViewController.sharedInstance.appDelegate.authKey.isEmpty {
                BaseViewController.sharedInstance.updateFirebaseToken()
            }
            else {
                if defaults.string(forKey: "isAppAlreadyLaunchedOnce") == nil {
                    BaseViewController.sharedInstance.storeAppInstallDataOnCRM()
                }
                BaseViewController.sharedInstance.appInstalled()
            }
        }
        else {
            if defaults.string(forKey: "isAppAlreadyLaunchedOnce") == nil {
                BaseViewController.sharedInstance.storeAppInstallDataOnCRM()
            }
            BaseViewController.sharedInstance.appInstalled()
        }
    }
    
    @objc func selectSpacifiIndex(notification: NSNotification) {
        if let userInfo = notification.userInfo as? [String: AnyObject] {
            if let spacificIndexStr = userInfo[kTabbarIndexios] as? String, !spacificIndexStr.isEmpty {
                if let spacificIndex = Int(spacificIndexStr) {
                    self.carbonTabSwipeNavigation.currentTabIndex = UInt(spacificIndex)
                }
            }
        }
    }
}

extension WelcomeViewController: CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
            let viewController = self.newStoryBoard.instantiateViewController(withIdentifier: "NewWalkthroughViewController") as! NewWalkthroughViewController
            return viewController
        }
        else if index == 1 {
            let viewController = self.newStoryBoard.instantiateViewController(withIdentifier: "BenefitsViewController") as! BenefitsViewController
            return viewController
        }
        else {
            let viewController = self.newStoryBoard.instantiateViewController(withIdentifier: "BenefitsSecondViewController") as! BenefitsSecondViewController
            return viewController
        }
    }
}
