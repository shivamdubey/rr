//
//  OTPViewController.swift
//  RR
//
//  Created by Sagar on 15/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class BenefitsSecondViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var banefitsCV: UICollectionView!
    @IBOutlet weak var btnSkip: UIButton!
    
    var benefitsList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        self.addFirebaseCustomeEvent(eventName: "BenefitsSecondScreen")
        
        self.banefitsCV.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.benefitsList.removeAll()
        for i in (BaseViewController.sharedInstance.appDelegate.mainBenefitsList.count / 2)..<BaseViewController.sharedInstance.appDelegate.mainBenefitsList.count {
            self.benefitsList.append(BaseViewController.sharedInstance.appDelegate.mainBenefitsList[i])
        }
        self.banefitsCV.reloadData()
    }
    
    func setupLocalizationText() {
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Welcome_lbl_ExploreTheRoyalsApp")
        self.btnSkip.setTitle(self.getLocalizeTextForKey(keyName: "Welcome_btn_Next"), for: .normal)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            print("you reached end of the table")
            self.btnSkip.isHidden = false
        }
        else {
            self.btnSkip.isHidden = true
        }
    }
    
    @IBAction func btnSkip_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension BenefitsSecondViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return benefitsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.benefitsList[indexPath.item]["is_fullbanner"] as? String == kYes {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BenefitsImageCell", for: indexPath) as! BenefitsImageCell
            
            if let imageUrl = benefitsList[indexPath.item][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgBanefits.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BenefitsCell", for: indexPath) as! BenefitsCell
            
            if let imageUrl = benefitsList[indexPath.item][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgBanefits.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            
            cell.lblTitle.text = benefitsList[indexPath.item][kTitle] as? String
            cell.lblDescription.text = benefitsList[indexPath.item][kDescription] as? String
            cell.lblEarnCoin.text = benefitsList[indexPath.item]["earn_coins"] as? String
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.benefitsList[indexPath.item]["is_fullbanner"] as? String == kYes {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension BenefitsSecondViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 2
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        if self.benefitsList[indexPath.item]["is_fullbanner"] as? String == kYes {
            return CGSize(width: widthPerItem * 2, height: 125)
        }
        return CGSize(width: widthPerItem, height: 240)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}
