//
//  OTPViewController.swift
//  RR
//
//  Created by Sagar on 15/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

extension String {
    var length: Int {
        return count
    }

    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }

    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }

    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }

    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
}

class TShirtNumberViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var imgTopArrow: UIImageView!
    @IBOutlet weak var lblFirstNumber: UILabel!
    @IBOutlet weak var imgTopArrow1: UIImageView!
    @IBOutlet weak var imgBottomArrow: UIImageView!
    @IBOutlet weak var lblSecondNumber: UILabel!
    @IBOutlet weak var imgBottomArrow1: UIImageView!
    @IBOutlet weak var imgTshirt: UIImageView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnConfirm: CustomButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet weak var lblNumberTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblNameBottomConstraint: NSLayoutConstraint!
    
    var squadNumber = ""
    var firstNumber = 0
    var secondNumber = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        self.addFirebaseCustomeEvent(eventName: "TShirtNumberScreen")
        
        if IS_IPHONEXMax {
            self.lblNumberTopConstraint.constant = 125
            self.lblNameBottomConstraint.constant = -115
        }
        
        self.txtFirstName.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_lbl_FirstName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtFirstName.text = BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String ?? ""
        self.lblName.text = (BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String ?? "?").uppercased()
        
        self.imgTopArrow.rotate(degrees: 60)
        self.imgTopArrow1.rotate(degrees: 60)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.squadNumber.isEmpty {
            if self.squadNumber.count > 2 {
                self.lblFirstNumber.text = self.squadNumber[1]
                self.lblSecondNumber.text = self.squadNumber[2]
                
                self.firstNumber = Int(self.lblFirstNumber.text ?? "0") ?? 0
                self.secondNumber = Int(self.lblSecondNumber.text ?? "0") ?? 0
            }
            else if self.squadNumber.count > 1 {
                self.lblFirstNumber.text = self.squadNumber[0]
                self.lblSecondNumber.text = self.squadNumber[1]
                
                self.firstNumber = Int(self.lblFirstNumber.text ?? "0") ?? 0
                self.secondNumber = Int(self.lblSecondNumber.text ?? "0") ?? 0
            }
            else {
                self.lblFirstNumber.text = "0"
                self.lblSecondNumber.text = self.squadNumber[0]
                
                self.firstNumber = 0
                self.secondNumber = Int(self.lblSecondNumber.text ?? "0") ?? 0
            }
        }
        else {
            self.firstNumber = 0
            self.secondNumber = 0
        }
        
        self.lblNumber.text = "\(self.firstNumber)\(self.secondNumber)"
    }
    
    func setupLocalizationText() {
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "TShirtName_lbl_Title")
        self.lblMessage.text = self.getLocalizeTextForKey(keyName: "TShirtName_lbl_FirstName")
        self.btnConfirm.setTitle(self.getLocalizeTextForKey(keyName: "btn_Confirm").uppercased(), for: .normal)
        self.btnSkip.setTitle(self.getLocalizeTextForKey(keyName: "Welcome_btn_Next"), for: .normal)
    }
    
    @IBAction func btnTopArrlow_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        if button.tag == 1 {
            if self.firstNumber < 9 {
                self.firstNumber = self.firstNumber + 1
            }
        }
        else {
            if self.secondNumber < 9 {
                self.secondNumber = self.secondNumber + 1
            }
        }
        
        self.lblFirstNumber.text = "\(self.firstNumber)"
        self.lblSecondNumber.text = "\(self.secondNumber)"
        self.lblNumber.text = "\(self.firstNumber)\(self.secondNumber)"
        self.squadNumber = self.lblNumber.text ?? "00"
    }
    
    @IBAction func btnBottomArrlow_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        if button.tag == 3 {
            if self.firstNumber > 0 {
                self.firstNumber = self.firstNumber - 1
            }
        }
        else {
            if self.secondNumber > 0 {
                self.secondNumber = self.secondNumber - 1
            }
        }
        
        self.lblFirstNumber.text = "\(self.firstNumber)"
        self.lblSecondNumber.text = "\(self.secondNumber)"
        self.lblNumber.text = "\(self.firstNumber)\(self.secondNumber)"
        self.squadNumber = self.lblNumber.text ?? "00"
    }
    
    @IBAction func btnConfirm_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        self.setTShirtNumberAndName()
    }
    
    @IBAction func btnSkip_Clicked(_ button: UIButton) {
        BaseViewController.sharedInstance.navigateToHomeScreen()
    }
}

extension TShirtNumberViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtFirstName.isFirstResponder {
            txtFirstName.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.txtFirstName.text?.trim().count == 0 {
            self.lblName.text = "?"
        }
        else {
            self.lblName.text = self.txtFirstName.text?.uppercased()
        }
        
        if self.txtFirstName.text?.trim().count != 0 {
            self.btnConfirm.isEnabled = true
            self.btnConfirm.borderWidth = 0
            self.btnConfirm.borderColor = .clear
            self.btnConfirm.backgroundColor = UIColor().alertButtonColor.withAlphaComponent(0.45)
            self.btnConfirm.setTitleColor(.white, for: .normal)
        }
        else {
            self.btnConfirm.isEnabled = false
            self.btnConfirm.borderWidth = 1
            self.btnConfirm.borderColor = UIColor.init(hex: "979797")
            self.btnConfirm.backgroundColor = .clear
            self.btnConfirm.setTitleColor(UIColor.init(hex: "979797"), for: .normal)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFirstName {
            return range.location < 10
        }
        return true
    }
}

extension TShirtNumberViewController {
    func setTShirtNumberAndName() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "Appuser[\(kUserLuckynumber)]": self.squadNumber,
                         "Appuser[\(kTShirtName)]": self.txtFirstName.text!] as [String : Any]
             
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.UPDATEPROFILE.rawValue, method: .post, parameters: param) {
                    (response, error) in
                if (error == nil) {
                    let responseData = response as! [String : AnyObject]
                     
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                    }
                    
                    BaseViewController.sharedInstance.navigateToHomeScreen()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(error!.domain)")
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
