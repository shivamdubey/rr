//
//  OTPViewController.swift
//  RR
//
//  Created by Sagar on 15/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class NewWalkthroughViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnViewBenefits: UIButton!
    @IBOutlet weak var btnSignup: CustomButton!
    @IBOutlet weak var btnLogin: CustomButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
    }
    
    func setupLocalizationText() {
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Welcome_lbl_HeaderTitle")
        self.lblMessage.text = self.getLocalizeTextForKey(keyName: "Welcome_lbl_Message")
        self.btnSignup.setTitle(self.getLocalizeTextForKey(keyName: "Signup_lbl_Signup"), for: .normal)
        self.btnLogin.setTitle(self.getLocalizeTextForKey(keyName: "Login_lbl_Login"), for: .normal)
        self.btnViewBenefits.setTitle(self.getLocalizeTextForKey(keyName: "Welcome_btn_ExploreNow"), for: .normal)
    }
    
    @IBAction func btnViewBenefits_Clicked(_ button: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("SelectSpacificIndex"), object: nil, userInfo: [kTabbarIndexios: "1"])
    }
    
    @IBAction func btnSignup_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnLogin_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
