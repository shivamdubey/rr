//
//  OTPViewController.swift
//  RR
//
//  Created by Sagar on 15/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class FavouritePlayerViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgPlayer: CustomImageView!
    @IBOutlet weak var lblSwipeToLeft: UILabel!
    @IBOutlet weak var playerCV: UICollectionView!
    @IBOutlet weak var imgLeftArrow: UIImageView!
    @IBOutlet weak var imgRightArrow: UIImageView!
    @IBOutlet weak var pinkView: CustomView!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var btnConfirm: CustomButton!
    @IBOutlet weak var btnSkip: CustomButton!
    
    var playerList = [[String: AnyObject]]()
    var selectedPlayerId = 0
    var squadNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        self.addFirebaseCustomeEvent(eventName: "FavouritePlayerScreen")
        
        self.btnConfirm.isEnabled = false
        self.btnConfirm.borderWidth = 1
        self.btnConfirm.borderColor = UIColor.init(hex: "979797")
        self.btnConfirm.backgroundColor = .clear
        self.btnConfirm.setTitleColor(UIColor.init(hex: "979797"), for: .normal)
        
        self.imgLeftArrow.rotate(degrees: -30)
        self.imgRightArrow.rotate(degrees: 30)
        
        self.getPlayerList(isShowHud: true)
    }
    
    func setupLocalizationText() {
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "FavouritePlayer_lbl_Title")
        self.lblSwipeToLeft.text = self.getLocalizeTextForKey(keyName: "FavouritePlayer_lbl_SwipeToSelect")
        self.btnConfirm.setTitle(self.getLocalizeTextForKey(keyName: "btn_Confirm").uppercased(), for: .normal)
        self.btnSkip.setTitle(self.getLocalizeTextForKey(keyName: "Welcome_btn_Next"), for: .normal)
    }
    
    @IBAction func btnConfirm_Clicked(_ button: UIButton) {
        self.setFavouritePlayer()
    }
    
    @IBAction func btnSkip_Clicked(_ button: UIButton) {
        let viewController = self.newStoryBoard.instantiateViewController(withIdentifier: "TShirtNumberViewController") as! TShirtNumberViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension FavouritePlayerViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playerList.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerCell", for: indexPath) as! PlayerCell
        
        if indexPath.item == 0 {
            cell.imgPlayer.image = #imageLiteral(resourceName: "user_placeholder")
        }
        else {
            if let imageUrl = playerList[indexPath.item - 1]["web_image"] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "user_placeholder"))
                }
            }
        }
        
        return cell
    }
}

extension FavouritePlayerViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.playerCV.contentOffset
        visibleRect.size = self.playerCV.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.playerCV.indexPathForItem(at: visiblePoint) else { return }
        
        self.imgLeftArrow.isHidden = false
        self.pinkView.isHidden = false
        self.imgRightArrow.isHidden = false
        self.lblPlayerName.isHidden = false
        
        if indexPath.item == 0 {
            self.imgPlayer.image = #imageLiteral(resourceName: "user_placeholder")
            self.lblPlayerName.text = ""
            self.selectedPlayerId = 0
            self.squadNumber = ""
            
            self.btnConfirm.isEnabled = false
            self.btnConfirm.borderWidth = 1
            self.btnConfirm.borderColor = UIColor.init(hex: "979797")
            self.btnConfirm.backgroundColor = .clear
            self.btnConfirm.setTitleColor(UIColor.init(hex: "979797"), for: .normal)
        }
        else {
            self.btnConfirm.isEnabled = true
            self.btnConfirm.borderWidth = 0
            self.btnConfirm.borderColor = .clear
            self.btnConfirm.backgroundColor = UIColor().alertButtonColor.withAlphaComponent(0.45)
            self.btnConfirm.setTitleColor(.white, for: .normal)
            
            if let imageUrl = self.playerList[indexPath.item - 1]["web_image"] as? String {
                if let url = URL(string: imageUrl) {
                    self.imgPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "user_placeholder"))
                }
            }
            
            self.lblPlayerName.text = "\(self.playerList[indexPath.item - 1][kFirstName] as? String ?? "")\n\(self.playerList[indexPath.item - 1][kLastName] as? String ?? "")".uppercased()
            
            self.selectedPlayerId = self.playerList[indexPath.item - 1][kPlayerId] as? Int ?? 0
            self.squadNumber = "\(self.playerList[indexPath.item - 1][kPlayersNumber] as? Int ?? 0)"
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.imgLeftArrow.isHidden = true
        self.pinkView.isHidden = true
        self.imgRightArrow.isHidden = true
        self.lblPlayerName.isHidden = true
    }
}

class PlayerCell: UICollectionViewCell {
    @IBOutlet weak var imgPlayer: CustomImageView!
}

extension FavouritePlayerViewController {
    func getPlayerList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: 1, kPageSize: 50]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETPLAYERLIST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let players = responseData[kData] as? [[String : AnyObject]] {
                        self.playerList = players
                    }
                    
                    self.playerCV.reloadData()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func setFavouritePlayer() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "players_id": self.selectedPlayerId] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.FAVOURITEPLAYER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                    }
                    
                    let viewController = self.newStoryBoard.instantiateViewController(withIdentifier: "TShirtNumberViewController") as! TShirtNumberViewController
                    viewController.squadNumber = self.squadNumber
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
