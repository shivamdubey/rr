//
//  ViewController.swift
//  <Project Name>
//
//  Created by Sagar Nandha
//  Copyright © 2021 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class WelcomeBackViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblWelcomeBack: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblWelcomeBack.text = self.getLocalizeTextForKey(keyName: "Welcome_lbl_WelcomeBack")
        
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        
        if hour > 0 && hour < 12 {
            self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Welcome_lbl_GoodMorning")
        }
        else if hour >= 12 && hour < 16 {
            self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Welcome_lbl_GoodAftrenoon")
        }
        else if hour >= 16 && hour < 21 {
            self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Welcome_lbl_GoodEvening")
        }
        else {
            self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Welcome_lbl_GoodEvening")
        }
        
        self.lblFirstName.text = (BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String ?? "").uppercased()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            BaseViewController.sharedInstance.navigateToHomeScreen()
        }
    }
}
