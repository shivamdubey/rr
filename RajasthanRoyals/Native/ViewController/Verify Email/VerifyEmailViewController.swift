//
//  ReferFriendViewController.swift
//  CV Builder
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 AmitMacBook. All rights reserved.
//

import UIKit

class VerifyEmailViewController: BaseViewController {

    @IBOutlet weak var emailPopup: CustomView!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var btnNotNow: CustomButton!
    @IBOutlet weak var btnVerifyEmail: CustomButton!
    
    @IBOutlet weak var otpPopup: CustomView!
    @IBOutlet weak var imgOTP: UIImageView!
    @IBOutlet weak var lblMessageOTP: UILabel!
    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var btnNotNowOTP: CustomButton!
    @IBOutlet weak var btnVerifyOTP: CustomButton!
    
    var isOTPEntered = false
    var otpNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setOTPView()
        
        self.lblMessage.text = self.getLocalizeTextForKey(keyName: "VerifyEmail_lbl_EmailMessage")
        self.txtEmail.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Login_placeholder_EmailAddress"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.btnNotNow.setTitle(self.getLocalizeTextForKey(keyName: "VerifyEmail_btn_NotNow"), for: .normal)
        self.btnVerifyEmail.setTitle(self.getLocalizeTextForKey(keyName: "VerifyEmail_btn_VerifyEmail"), for: .normal)
        
        self.lblMessageOTP.text = self.getLocalizeTextForKey(keyName: "VerifyEmail_lbl_OTPMessage")
        self.btnNotNowOTP.setTitle(self.getLocalizeTextForKey(keyName: "VerifyEmail_btn_NotNow"), for: .normal)
        self.btnVerifyOTP.setTitle(self.getLocalizeTextForKey(keyName: "VerifyEmail_btn_Verify"), for: .normal)
        
        self.txtEmail.text = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? ""
    }
    
    func setOTPView() {
        otpView.otpFieldSize = 60
        otpView.otpFieldsCount = 4
        otpView.otpFieldDisplayType = .circular
        otpView.cursorColor = UIColor.white
        otpView.otpFieldBorderWidth = 0.5
        otpView.otpFieldDefaultBorderColor = UIColor.white
        otpView.otpFieldEnteredBorderColor = UIColor.white
        otpView.tintColor = UIColor.white
        otpView.otpFieldFont = UIFont(name: "Futura-Medium", size: 24)!
        otpView.delegate = self
        otpView.becomeFirstResponder()
        
        //Create the UI
        otpView.initalizeUI()
    }
    
    @IBAction func btnNotNow_Clicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnVerifyEmail_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        if self.txtEmail.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_EmailAddress"))
        }
        else if !self.isValidEmail(str: txtEmail.text!) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidEmailAddress"))
        }
        else {
            self.callEmailVerifyAPI()
        }
    }
    
    @IBAction func btnVerifyOTP_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        if !self.isOTPEntered {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_OTP"))
        }
        else {
            self.verifyEmailOtp()
        }
    }
}

extension VerifyEmailViewController {
    func callEmailVerifyAPI() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "Appuser[email]": txtEmail.text!]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.EMAILVERIFY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    self.emailPopup.isHidden = true
                    self.imgEmail.isHidden = true
                    
                    self.otpPopup.isHidden = false
                    self.imgOTP.isHidden = false
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func verifyEmailOtp() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kOTP: otpNumber, kEmail: txtEmail.text!]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.EMAILOTPVERIFY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                        
                        if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                            let editor = BatchUser.editor()
                            editor.setIdentifier(email)
                            editor.save()
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                            self.callRegisterCustomEventAPI()
                        }
                    }
                    
                    self.dismiss(animated: true, completion: nil)
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension VerifyEmailViewController: VPMOTPViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) {
        self.isOTPEntered = hasEntered
    }
    
    func enteredOTP(otpString: String) {
        print("OTPString: \(otpString)")
        otpNumber = otpString
    }
}
