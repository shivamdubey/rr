//
//  AuctionMyTeamViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 08/02/21.
//

import UIKit

class SelectedMyTeamViewController: BaseViewController {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgSecretAuction: UIImageView!
    @IBOutlet weak var lblMyTeam: UILabel!
    @IBOutlet weak var tblViewMyTeam: UITableView!
    @IBOutlet weak var btnMakeChanges: CustomButton!
    
    var auctionRules = [String: AnyObject]()
    var selectedPlayerList = [[String: AnyObject]]()
    var retainPlayerList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
    }
    
    func setupLocalizationText() {
        self.lblMyTeam.text = self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_MyBuys")
        self.btnMakeChanges.setTitle(self.getLocalizeTextForKey(keyName: "SecretAuction_btn_MakeChanges"), for: .normal)
    }
    
    @IBAction func btnMakeChanges_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AuctionPlayerViewController") as! AuctionPlayerViewController
        viewController.auctionRules = self.auctionRules
        viewController.selectedPlayerList = self.selectedPlayerList
        viewController.retainPlayerList = self.retainPlayerList
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension SelectedMyTeamViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return retainPlayerList.count + selectedPlayerList.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(translationX: 0, y: cell.contentView.frame.height)
        UIView.animate(withDuration: 0.5, delay: 0.05 * Double(indexPath.row), animations: {
            cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: cell.contentView.frame.height)
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AuctionPlayerTblCell") as! AuctionPlayerTblCell
        
        var playerDetails = [String: AnyObject]()
        if retainPlayerList.count > indexPath.row {
            playerDetails = retainPlayerList[indexPath.row]
        }
        else {
            playerDetails = selectedPlayerList[indexPath.row - retainPlayerList.count]
        }
        
        if let imageUrl = playerDetails[kPlayerRolsImage] as? String {
            if let url = URL(string: imageUrl) {
                let imgPhoto = UIImageView()
                imgPhoto.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"), options: nil, progressBlock: nil) { (image, error, type, url) in
                    if let downloadImage = image {
                        cell.imgPlayerType.image = downloadImage.withTintColor(UIColor().themeDarkPinkColor)
                    }
                }
            }
        }
        
        cell.lblName.text = playerDetails[kPlayersName] as? String ?? ""
        cell.lblPrice.text = "\(playerDetails[kAmount] as? Double ?? 0.0) \(AUCTIONVALUE)"
        
        if let imageUrl = playerDetails[kCountryImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgAway.kf.setImage(with: url, placeholder:  #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        cell.btnAdd.addTarget(self, action: #selector(btnRemove_Clicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func btnRemove_Clicked(_ sender: AnyObject) {
        let position = sender.convert(CGPoint.zero, to: tblViewMyTeam)
        if let indexPath = tblViewMyTeam.indexPathForRow(at: position) {
            self.selectedPlayerList.remove(at: indexPath.row)
            self.tblViewMyTeam.deleteRows(at: [indexPath], with: .right)
        }
    }
}
