//
//  AuctionPlayerViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 08/02/21.
//

import UIKit
import RPCircularProgress
import AVFoundation

enum PLAYERTYPE : String {
    case OVERSEAS         = "Overseas"
    case INDIAN_CAPPED    = "Indian Capped"
    case INDIAN_UNCAPPED  = "Indian Uncapped"
}

enum PLAYERROLE : String {
    case BATSMAN          = "Batsman"
    case BOWLER           = "Bowler"
    case ALLROUNDER       = "AllRounder"
    case WICKETKEEPER     = "WicketKeeper"
}

class AuctionPlayerViewController: BaseViewController {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgSecretAuction: UIImageView!

    @IBOutlet weak var btnInfo: UIButton!

    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnOverseas: CustomButton!
    @IBOutlet weak var btnIndianCapped: CustomButton!
    @IBOutlet weak var btnIndianUncapped: CustomButton!
    
    @IBOutlet weak var btnBatsman: UIButton!
    @IBOutlet weak var btnAllRounder: UIButton!
    @IBOutlet weak var btnBowler: UIButton!
    @IBOutlet weak var btnWiketKeeper: UIButton!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblViewPlayer: UITableView!

    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var squadCircularProgressView: RPCircularProgress!
    @IBOutlet weak var lblSquadTitle: UILabel!
    @IBOutlet weak var lblSquadValue: UILabel!
    
    @IBOutlet weak var walletCircularProgressView: RPCircularProgress!
    @IBOutlet weak var lblWalletTitle: UILabel!
    @IBOutlet weak var lblWalletValue: UILabel!
    
    @IBOutlet weak var btnPreviewTeam: CustomButton!
    
    var auctionRules = [String: AnyObject]()
    var retainPlayerList = [[String: AnyObject]]()
    
    var totalAmount = 0.0
    var totalPlayer = 0
    var overseasPlayerLimit = 0
    var selectedPlayerType = PLAYERTYPE.OVERSEAS.rawValue
    var selectedPlayerRole = PLAYERROLE.BATSMAN.rawValue
    
    var allPlayerList = [[String: AnyObject]]()
    var mainPlayerList = [[String: AnyObject]]()
    var playerList = [[String: AnyObject]]()
    
    var isPriceAscendingOrder = true
    var selectedPlayerList = [[String: AnyObject]]()
    
    var audioPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        if let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField {
            textFieldInsideSearchBar.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_SearchByName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            textFieldInsideSearchBar.textColor = .white
            textFieldInsideSearchBar.font = UIFont.init(name: String().themeMediumFontName, size: 12.0)
            
            if let searchIcon = textFieldInsideSearchBar.leftView as? UIImageView {
                searchIcon.tintColor = .white
            }
            
            if let clearButton = textFieldInsideSearchBar.value(forKey: "clearButton") as? UIButton {
                clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
                clearButton.tintColor = UIColor.white
            }
        }
        
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        self.btnOverseas_Clicked(sender: self.btnOverseas)
        
        self.setProgressAnimation(progress: self.squadCircularProgressView)
        self.setProgressAnimation(progress: self.walletCircularProgressView)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            let gradientLayer = CAGradientLayer()
            gradientLayer.opacity = 0.7
            gradientLayer.colors = [UIColor.init(hex: "F03A9D"), UIColor.init(hex: "812E94"), UIColor.init(hex: "0F218B")].map { $0.cgColor }

            if case .horizontal = GradientColorDirection.vertical {
                gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
                gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
            }

            gradientLayer.bounds = self.bottomView.bounds
            gradientLayer.anchorPoint = CGPoint.zero
            self.bottomView.layer.cornerRadius = 25
            self.bottomView.layer.masksToBounds = true
            self.bottomView.layer.addSublayer(gradientLayer)
        }
        
        self.totalAmount = self.auctionRules[kTotalAmount] as? Double ?? 0
        self.totalPlayer = self.auctionRules[kTotalPlayer] as? Int ?? 0
        self.overseasPlayerLimit = self.auctionRules[kOverseasPlayerLimit] as? Int ?? 0
        
        self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
        self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount)
        
        self.getAuctionPlayers()
        
        do {
            if let fileURL = Bundle.main.path(forResource: "Humer", ofType: "wav") {
                self.audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: fileURL))
            }
            else {
                print("No file with specified name exists")
            }
        }
        catch let error {
            print("Can't play the audio file failed with an error \(error.localizedDescription)")
        }
    }
    
    func setupLocalizationText() {
        self.btnOverseas.setTitle(self.getLocalizeTextForKey(keyName: "SecretAuction_btn_Overseas"), for: .normal)
        self.btnIndianCapped.setTitle(self.getLocalizeTextForKey(keyName: "SecretAuction_btn_IndianCapped"), for: .normal)
        self.btnIndianUncapped.setTitle(self.getLocalizeTextForKey(keyName: "SecretAuction_btn_IndianUncapped"), for: .normal)
        self.lblSquadTitle.text = self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_Squad")
        self.lblWalletTitle.text = self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_Wallet")
        self.btnPreviewTeam.setTitle(self.getLocalizeTextForKey(keyName: "SecretAuction_btn_PreviewBuys"), for: .normal)
    }
    
    func setProgressAnimation(progress: RPCircularProgress) {
        progress.innerTintColor = .clear
        progress.progressTintColor = UIColor().alertButtonColor
        progress.trackTintColor = .white
        progress.thicknessRatio = 0.1
    }
    
    func filterPlayerList()  {
        self.view.endEditing(true)
        self.searchBar.text = ""
        
        let playerListTypewise = self.allPlayerList.filter{$0[kPlayerType] as? String == self.selectedPlayerType}
        self.mainPlayerList = playerListTypewise.filter{$0[kRols] as? String == self.selectedPlayerRole}
        self.playerList = playerListTypewise.filter{$0[kRols] as? String == self.selectedPlayerRole}
        
        self.mainPlayerList = self.mainPlayerList.sorted(by: { ($0[kAmount] as? Double ?? 0.0) > ($1[kAmount] as? Double ?? 0.0) })
        self.playerList = self.playerList.sorted(by: { ($0[kAmount] as? Double ?? 0.0) > ($1[kAmount] as? Double ?? 0.0) })
        
        self.tblViewPlayer.reloadData()
    }
    
    func resetAuctionSelection() {
        self.selectedPlayerList.removeAll()
        self.tblViewPlayer.reloadData()
        
        self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
        self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount)
        
        self.squadCircularProgressView.updateProgress(0.0, animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
        self.walletCircularProgressView.updateProgress(0.0, animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
    }
    
    @IBAction func btnBack_Clicked(sender: UIButton) {
        for viewController in self.navigationController!.viewControllers {
            if viewController.isKind(of: AuctionHomeViewController.self) {
                self.navigationController?.popToViewController(viewController, animated: true)
                break
            }
        }
    }
    
    @IBAction func btnReset_Clicked(sender: UIButton) {
        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "Are you sure you want to reset all selections?", preferredStyle: .alert)
        alert.view.tintColor = UIColor().alertButtonColor
        
        let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Yes"), style: .default, handler: { (action) in
            self.resetAuctionSelection()
        })
        
        let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_No"), style: .default, handler: { (action) in
        })
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnOverseas_Clicked(sender: UIButton) {
        self.btnOverseas.backgroundColor = SelectedButtonBGColor
        self.btnIndianCapped.backgroundColor = DeSelectedButtonBGColor
        self.btnIndianUncapped.backgroundColor = DeSelectedButtonBGColor
        
        self.btnOverseas.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnIndianCapped.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnIndianUncapped.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.selectedPlayerType = PLAYERTYPE.OVERSEAS.rawValue
        self.filterPlayerList()
    }
    
    @IBAction func btnIndianCapped_Clicked(sender: UIButton) {
        self.btnOverseas.backgroundColor = DeSelectedButtonBGColor
        self.btnIndianCapped.backgroundColor = SelectedButtonBGColor
        self.btnIndianUncapped.backgroundColor = DeSelectedButtonBGColor
        
        self.btnOverseas.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnIndianCapped.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnIndianUncapped.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.selectedPlayerType = PLAYERTYPE.INDIAN_CAPPED.rawValue
        self.filterPlayerList()
    }
    
    @IBAction func btnIndianUncapped_Clicked(sender: UIButton) {
        self.btnOverseas.backgroundColor = DeSelectedButtonBGColor
        self.btnIndianCapped.backgroundColor = DeSelectedButtonBGColor
        self.btnIndianUncapped.backgroundColor = SelectedButtonBGColor
        
        self.btnOverseas.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnIndianCapped.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnIndianUncapped.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.selectedPlayerType = PLAYERTYPE.INDIAN_UNCAPPED.rawValue
        self.filterPlayerList()
    }
    
    @IBAction func btnBatsman_Clicked(sender: UIButton) {
        self.btnBatsman.setImage(UIImage(named: "batsman-sel"), for: .normal)
        self.btnAllRounder.setImage(UIImage(named: "allrounder-unsel"), for: .normal)
        self.btnBowler.setImage(UIImage(named: "bowler-unsel"), for: .normal)
        self.btnWiketKeeper.setImage(UIImage(named: "wk-unsel"), for: .normal)
        
        self.selectedPlayerRole = PLAYERROLE.BATSMAN.rawValue
        self.filterPlayerList()
    }
    
    @IBAction func btnAllRounder_Clicked(sender: UIButton) {
        self.btnBatsman.setImage(UIImage(named: "batsman-unsel"), for: .normal)
        self.btnAllRounder.setImage(UIImage(named: "allrounder-sel"), for: .normal)
        self.btnBowler.setImage(UIImage(named: "bowler-unsel"), for: .normal)
        self.btnWiketKeeper.setImage(UIImage(named: "wk-unsel"), for: .normal)
        
        self.selectedPlayerRole = PLAYERROLE.ALLROUNDER.rawValue
        self.filterPlayerList()
    }
    
    @IBAction func btnBowler_Clicked(sender: UIButton) {
        self.btnBatsman.setImage(UIImage(named: "batsman-unsel"), for: .normal)
        self.btnAllRounder.setImage(UIImage(named: "allrounder-unsel"), for: .normal)
        self.btnBowler.setImage(UIImage(named: "bowler-sel"), for: .normal)
        self.btnWiketKeeper.setImage(UIImage(named: "wk-unsel"), for: .normal)
        
        self.selectedPlayerRole = PLAYERROLE.BOWLER.rawValue
        self.filterPlayerList()
    }
    
    @IBAction func btnWiketKeeper_Clicked(sender: UIButton) {
        self.btnBatsman.setImage(UIImage(named: "batsman-unsel"), for: .normal)
        self.btnAllRounder.setImage(UIImage(named: "allrounder-unsel"), for: .normal)
        self.btnBowler.setImage(UIImage(named: "bowler-unsel"), for: .normal)
        self.btnWiketKeeper.setImage(UIImage(named: "wk-sel"), for: .normal)
        
        self.selectedPlayerRole = PLAYERROLE.WICKETKEEPER.rawValue
        self.filterPlayerList()
    }
    
    @IBAction func btnInfo_Clicked(sender: UIButton) {
        let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "AuctionFAQRulesViewController") as! AuctionFAQRulesViewController
        viewController.termsConsitionDescription = self.auctionRules["terms_conditions"] as? String ?? ""
        viewController.rulesDescription = self.auctionRules[kDescription] as? String ?? ""
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnPreviewTeam_Clicked(sender: UIButton) {
        if self.selectedPlayerList.count > 0 {
            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "AuctionMyTeamViewController") as! AuctionMyTeamViewController
            viewController.delegate = self
            viewController.auctionRules = self.auctionRules
            viewController.selectedPlayerList = self.selectedPlayerList
            viewController.retainPlayerList = self.retainPlayerList
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension AuctionPlayerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playerList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AuctionPlayerTblCell") as! AuctionPlayerTblCell
        
        cell.btnSortPrice.isHidden = true
        if indexPath.row == 0 {
            cell.btnSortPrice.isHidden = false
        }
        
        let playerDetails = playerList[indexPath.row]
        
        if let imageUrl = playerDetails[kPlayerRolsImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgPlayerType.kf.setImage(with: url, placeholder:  #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        cell.lblName.text = playerDetails[kPlayersName] as? String ?? ""
        cell.lblPrice.text = "\(playerDetails[kAmount] as? Double ?? 0.0) \(AUCTIONVALUE)"
        
        if let imageUrl = playerDetails[kCountryImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgAway.kf.setImage(with: url, placeholder:  #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        //Find the index
        let array = self.selectedPlayerList as Array
        let matchIndex = array.firstIndex {
            if $0[kAuctioPlayerId] as? Int == playerDetails[kAuctioPlayerId] as? Int {
                return true
            }
            return false
        }
        
        if matchIndex != nil {
            cell.btnAdd.isSelected = true
            if let playerTypeImage = cell.imgPlayerType.image {
                cell.imgPlayerType.image = playerTypeImage.withTintColor(UIColor().themeDarkPinkColor)
            }
        }
        else {
            cell.btnAdd.isSelected = false
            if let playerTypeImage = cell.imgPlayerType.image {
                cell.imgPlayerType.image = playerTypeImage.withTintColor(.white)
            }
        }
        
        cell.btnSortPrice.addTarget(self, action: #selector(btnSortPrice(_:)), for: .touchUpInside)
        cell.btnAdd.addTarget(self, action: #selector(btnAdd_Clicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectPlayer(indexPath: indexPath)
    }
    
    @objc func btnSortPrice(_ sender: AnyObject) {
        self.isPriceAscendingOrder = !self.isPriceAscendingOrder
        if self.isPriceAscendingOrder {
            self.mainPlayerList = self.mainPlayerList.sorted(by: { ($0[kAmount] as? Double ?? 0.0) > ($1[kAmount] as? Double ?? 0.0) })
            self.playerList = self.playerList.sorted(by: { ($0[kAmount] as? Double ?? 0.0) > ($1[kAmount] as? Double ?? 0.0) })
        }
        else {
            self.mainPlayerList = self.mainPlayerList.sorted(by: { ($0[kAmount] as? Double ?? 0.0) < ($1[kAmount] as? Double ?? 0.0) })
            self.playerList = self.playerList.sorted(by: { ($0[kAmount] as? Double ?? 0.0) < ($1[kAmount] as? Double ?? 0.0) })
        }
        
        self.tblViewPlayer.reloadData()
    }
    
    @objc func btnAdd_Clicked(_ sender: AnyObject) {
        let position = sender.convert(CGPoint.zero, to: tblViewPlayer)
        if let indexPath = tblViewPlayer.indexPathForRow(at: position) {
            self.selectPlayer(indexPath: indexPath)
        }
    }
    
    func selectPlayer(indexPath: IndexPath) {
//        self.audioPlayer.play()
        
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        let cell = tblViewPlayer.cellForRow(at: indexPath) as! AuctionPlayerTblCell
        
        if let playerId = self.playerList[indexPath.row][kAuctioPlayerId] as? Int {
            if self.totalPlayer > self.selectedPlayerList.count {
                let selectedOverseasPlayerList = self.selectedPlayerList.filter{$0[kPlayerType] as? String == PLAYERTYPE.OVERSEAS.rawValue}
                if self.playerList[indexPath.row][kPlayerType] as? String == PLAYERTYPE.OVERSEAS.rawValue {
                    if self.overseasPlayerLimit > selectedOverseasPlayerList.count {
                        var selectedPlayerTotalAmount = 0.0
                        self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
                        selectedPlayerTotalAmount = selectedPlayerTotalAmount + (self.playerList[indexPath.row][kAmount] as? Double ?? 0.0)
                        
                        if self.totalAmount >= selectedPlayerTotalAmount {
                            cell.btnAdd.isSelected = !cell.btnAdd.isSelected
                            
                            if cell.btnAdd.isSelected {
                                if let playerTypeImage = cell.imgPlayerType.image {
                                    cell.imgPlayerType.image = playerTypeImage.withTintColor(UIColor().themeDarkPinkColor)
                                }
                            }
                            else {
                                if let playerTypeImage = cell.imgPlayerType.image {
                                    cell.imgPlayerType.image = playerTypeImage.withTintColor(.white)
                                }
                            }
                            
                            //Find the index
                            let array = self.selectedPlayerList as Array
                            let matchIndex = array.firstIndex {
                                if $0[kAuctioPlayerId] as? Int == playerId {
                                    return true
                                }
                                return false
                            }
                            
                            if matchIndex != nil {
                                self.selectedPlayerList.remove(at: matchIndex!)
                            }
                            else {
                                self.selectedPlayerList.append(self.playerList[indexPath.row])
                            }
                            
                            selectedPlayerTotalAmount = 0.0
                            self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
                            
                            self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
                            self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount - selectedPlayerTotalAmount)
                            
                            self.squadCircularProgressView.updateProgress(CGFloat(self.selectedPlayerList.count) / CGFloat(self.totalPlayer), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                            self.walletCircularProgressView.updateProgress(CGFloat(selectedPlayerTotalAmount) / CGFloat(self.totalAmount), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                        }
                        else {
                            if cell.btnAdd.isSelected {
                                cell.btnAdd.isSelected = !cell.btnAdd.isSelected
                                
                                if cell.btnAdd.isSelected {
                                    if let playerTypeImage = cell.imgPlayerType.image {
                                        cell.imgPlayerType.image = playerTypeImage.withTintColor(UIColor().themeDarkPinkColor)
                                    }
                                }
                                else {
                                    if let playerTypeImage = cell.imgPlayerType.image {
                                        cell.imgPlayerType.image = playerTypeImage.withTintColor(.white)
                                    }
                                }
                                
                                //Find the index
                                let array = self.selectedPlayerList as Array
                                let matchIndex = array.firstIndex {
                                    if $0[kAuctioPlayerId] as? Int == playerId {
                                        return true
                                    }
                                    return false
                                }
                                
                                if matchIndex != nil {
                                    self.selectedPlayerList.remove(at: matchIndex!)
                                }
                                else {
                                    self.selectedPlayerList.append(self.playerList[indexPath.row])
                                }
                                
                                selectedPlayerTotalAmount = 0.0
                                self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
                                
                                self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
                                self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount - selectedPlayerTotalAmount)
                                
                                self.squadCircularProgressView.updateProgress(CGFloat(self.selectedPlayerList.count) / CGFloat(self.totalPlayer), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                                self.walletCircularProgressView.updateProgress(CGFloat(selectedPlayerTotalAmount) / CGFloat(self.totalAmount), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                            }
                            else {
                                UIAlertController().alertViewWithTitleAndMessage(self, message: "\(self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_WalletLimit")) \(self.totalAmount) \(AUCTIONVALUE).")
                            }
                        }
                    }
                    else {
                        if cell.btnAdd.isSelected {
                            cell.btnAdd.isSelected = !cell.btnAdd.isSelected
                            
                            if cell.btnAdd.isSelected {
                                if let playerTypeImage = cell.imgPlayerType.image {
                                    cell.imgPlayerType.image = playerTypeImage.withTintColor(UIColor().themeDarkPinkColor)
                                }
                            }
                            else {
                                if let playerTypeImage = cell.imgPlayerType.image {
                                    cell.imgPlayerType.image = playerTypeImage.withTintColor(.white)
                                }
                            }
                            
                            //Find the index
                            let array = self.selectedPlayerList as Array
                            let matchIndex = array.firstIndex {
                                if $0[kAuctioPlayerId] as? Int == playerId {
                                    return true
                                }
                                return false
                            }
                            
                            if matchIndex != nil {
                                self.selectedPlayerList.remove(at: matchIndex!)
                            }
                            else {
                                self.selectedPlayerList.append(self.playerList[indexPath.row])
                            }
                            
                            var selectedPlayerTotalAmount = 0.0
                            self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
                            
                            self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
                            self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount - selectedPlayerTotalAmount)
                            
                            self.squadCircularProgressView.updateProgress(CGFloat(self.selectedPlayerList.count) / CGFloat(self.totalPlayer), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                            self.walletCircularProgressView.updateProgress(CGFloat(selectedPlayerTotalAmount) / CGFloat(self.totalAmount), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                        }
                        else {
                            UIAlertController().alertViewWithTitleAndMessage(self, message: "\(self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_MaximumLimit")) \(self.overseasPlayerLimit) \(self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_OverseasPlayers"))")
                        }
                    }
                }
                else {
                    var selectedPlayerTotalAmount = 0.0
                    self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
                    selectedPlayerTotalAmount = selectedPlayerTotalAmount + (self.playerList[indexPath.row][kAmount] as? Double ?? 0.0)
                    
                    if self.totalAmount >= selectedPlayerTotalAmount {
                        cell.btnAdd.isSelected = !cell.btnAdd.isSelected
                        
                        if cell.btnAdd.isSelected {
                            if let playerTypeImage = cell.imgPlayerType.image {
                                cell.imgPlayerType.image = playerTypeImage.withTintColor(UIColor().themeDarkPinkColor)
                            }
                        }
                        else {
                            if let playerTypeImage = cell.imgPlayerType.image {
                                cell.imgPlayerType.image = playerTypeImage.withTintColor(.white)
                            }
                        }
                        
                        //Find the index
                        let array = self.selectedPlayerList as Array
                        let matchIndex = array.firstIndex {
                            if $0[kAuctioPlayerId] as? Int == playerId {
                                return true
                            }
                            return false
                        }
                        
                        if matchIndex != nil {
                            self.selectedPlayerList.remove(at: matchIndex!)
                        }
                        else {
                            self.selectedPlayerList.append(self.playerList[indexPath.row])
                        }
                        
                        selectedPlayerTotalAmount = 0.0
                        self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
                        
                        self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
                        self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount - selectedPlayerTotalAmount)
                        
                        self.squadCircularProgressView.updateProgress(CGFloat(self.selectedPlayerList.count) / CGFloat(self.totalPlayer), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                        self.walletCircularProgressView.updateProgress(CGFloat(selectedPlayerTotalAmount) / CGFloat(self.totalAmount), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                    }
                    else {
                        if cell.btnAdd.isSelected {
                            cell.btnAdd.isSelected = !cell.btnAdd.isSelected
                            
                            if cell.btnAdd.isSelected {
                                if let playerTypeImage = cell.imgPlayerType.image {
                                    cell.imgPlayerType.image = playerTypeImage.withTintColor(UIColor().themeDarkPinkColor)
                                }
                            }
                            else {
                                if let playerTypeImage = cell.imgPlayerType.image {
                                    cell.imgPlayerType.image = playerTypeImage.withTintColor(.white)
                                }
                            }
                            
                            //Find the index
                            let array = self.selectedPlayerList as Array
                            let matchIndex = array.firstIndex {
                                if $0[kAuctioPlayerId] as? Int == playerId {
                                    return true
                                }
                                return false
                            }
                            
                            if matchIndex != nil {
                                self.selectedPlayerList.remove(at: matchIndex!)
                            }
                            else {
                                self.selectedPlayerList.append(self.playerList[indexPath.row])
                            }
                            
                            selectedPlayerTotalAmount = 0.0
                            self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
                            
                            self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
                            self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount - selectedPlayerTotalAmount)
                            
                            self.squadCircularProgressView.updateProgress(CGFloat(self.selectedPlayerList.count) / CGFloat(self.totalPlayer), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                            self.walletCircularProgressView.updateProgress(CGFloat(selectedPlayerTotalAmount) / CGFloat(self.totalAmount), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                        }
                        else {
                            UIAlertController().alertViewWithTitleAndMessage(self, message: "\(self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_WalletLimit")) \(self.totalAmount) \(AUCTIONVALUE).")
                        }
                    }
                }
            }
            else {
                if cell.btnAdd.isSelected {
                    cell.btnAdd.isSelected = !cell.btnAdd.isSelected
                    
                    if cell.btnAdd.isSelected {
                        if let playerTypeImage = cell.imgPlayerType.image {
                            cell.imgPlayerType.image = playerTypeImage.withTintColor(UIColor().themeDarkPinkColor)
                        }
                    }
                    else {
                        if let playerTypeImage = cell.imgPlayerType.image {
                            cell.imgPlayerType.image = playerTypeImage.withTintColor(.white)
                        }
                    }
                    
                    //Find the index
                    let array = self.selectedPlayerList as Array
                    let matchIndex = array.firstIndex {
                        if $0[kAuctioPlayerId] as? Int == playerId {
                            return true
                        }
                        return false
                    }
                    
                    if matchIndex != nil {
                        self.selectedPlayerList.remove(at: matchIndex!)
                    }
                    else {
                        self.selectedPlayerList.append(self.playerList[indexPath.row])
                    }
                    
                    var selectedPlayerTotalAmount = 0.0
                    self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
                    
                    self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
                    self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount - selectedPlayerTotalAmount)
                    
                    self.squadCircularProgressView.updateProgress(CGFloat(self.selectedPlayerList.count) / CGFloat(self.totalPlayer), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                    self.walletCircularProgressView.updateProgress(CGFloat(selectedPlayerTotalAmount) / CGFloat(self.totalAmount), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_SelectMaxPlayer")) \(self.totalPlayer) \(self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_Players"))")
                }
            }
        }
    }
}

class AuctionPlayerTblCell: UITableViewCell {
    @IBOutlet weak var imgPlayerType: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnSortPrice: UIButton!
    @IBOutlet weak var imgAway: UIImageView!
    @IBOutlet weak var btnAdd: UIButton!
}

extension AuctionPlayerViewController {
    func getAuctionPlayers() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETAUCTIONPLAYER.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.allPlayerList = data
                    }
                    
                    self.filterPlayerList()
                    
                    self.getMyAuctionTeam()
                }
            }
        }
    }
    
    func getMyAuctionTeam() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETMYACTIONTEAM.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.selectedPlayerList = data
                        
                        self.tblViewPlayer.reloadData()
                        
                        var selectedPlayerTotalAmount = 0.0
                        self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
                        
                        self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
                        self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount - selectedPlayerTotalAmount)
                        
                        self.squadCircularProgressView.updateProgress(CGFloat(self.selectedPlayerList.count) / CGFloat(self.totalPlayer), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                        self.walletCircularProgressView.updateProgress(CGFloat(selectedPlayerTotalAmount) / CGFloat(self.totalAmount), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension AuctionPlayerViewController: ResetAuctionSelectionDelegate {
    func resetAuction(selectedPlayerList: [[String : AnyObject]]) {
        self.selectedPlayerList = selectedPlayerList
        
        self.tblViewPlayer.reloadData()
        
        var selectedPlayerTotalAmount = 0.0
        self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
        
        self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
        self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount - selectedPlayerTotalAmount)
        
        self.squadCircularProgressView.updateProgress(CGFloat(self.selectedPlayerList.count) / CGFloat(self.totalPlayer), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
        self.walletCircularProgressView.updateProgress(CGFloat(selectedPlayerTotalAmount) / CGFloat(self.totalAmount), animated: true, initialDelay: 0.2, duration: 0.7, completion: nil)
    }
}

extension AuctionPlayerViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var newText = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
        
        if newText.count >= 1 {
            newText = newText.replacingOccurrences(of: "\n", with: "")
            self.playerList = self.mainPlayerList.filter({ ($0[kPlayersName] as? String ?? "").contains(newText) })
        }
        else if newText.count == 0 {
            self.playerList = self.mainPlayerList
        }

        self.tblViewPlayer.reloadData()
        
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        self.view.endEditing(true)
        
        self.filterPlayerList()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = false
        return true
    }
}

extension Sequence  {
    func sum<T: AdditiveArithmetic>(_ predicate: (Element) -> T) -> T { reduce(.zero) { $0 + predicate($1) } }
}
