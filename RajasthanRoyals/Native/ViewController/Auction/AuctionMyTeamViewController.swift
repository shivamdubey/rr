//
//  AuctionMyTeamViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 08/02/21.
//

import UIKit
import RPCircularProgress

protocol ResetAuctionSelectionDelegate {
    func resetAuction(selectedPlayerList: [[String: AnyObject]])
}

class AuctionMyTeamViewController: BaseViewController {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgSecretAuction: UIImageView!
    
    @IBOutlet weak var lblMyTeam: UILabel!

    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var btnReset: UIButton!

    @IBOutlet weak var tblViewMyTeam: UITableView!
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var squadCircularProgressView: RPCircularProgress!
    @IBOutlet weak var lblSquadTitle: UILabel!
    @IBOutlet weak var lblSquadValue: UILabel!
    
    @IBOutlet weak var walletCircularProgressView: RPCircularProgress!
    @IBOutlet weak var lblWalletTitle: UILabel!
    @IBOutlet weak var lblWalletValue: UILabel!
    
    @IBOutlet weak var btnSubmitTeam: CustomButton!
    
    var delegate: ResetAuctionSelectionDelegate!
    var auctionRules = [String: AnyObject]()
    var retainPlayerList = [[String: AnyObject]]()
    var selectedPlayerList = [[String: AnyObject]]()
    
    var totalAmount = 0.0
    var totalPlayer = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        self.setProgressAnimation(progress: self.squadCircularProgressView)
        self.setProgressAnimation(progress: self.walletCircularProgressView)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            let gradientLayer = CAGradientLayer()
            gradientLayer.opacity = 0.5
            gradientLayer.colors = [UIColor.init(hex: "F03A9D"), UIColor.init(hex: "812E94"), UIColor.init(hex: "0F218B")].map { $0.cgColor }

            if case .horizontal = GradientColorDirection.vertical {
                gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
                gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
            }

            gradientLayer.bounds = self.bottomView.bounds
            gradientLayer.anchorPoint = CGPoint.zero
            self.bottomView.layer.cornerRadius = 25
            self.bottomView.layer.masksToBounds = true
            self.bottomView.layer.addSublayer(gradientLayer)
        }
        
        self.totalAmount = self.auctionRules[kTotalAmount] as? Double ?? 0.0
        self.totalPlayer = self.auctionRules[kTotalPlayer] as? Int ?? 0
        
        var selectedPlayerTotalAmount = 0.0
        self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
        
        self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
        self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount - selectedPlayerTotalAmount)
        
        self.squadCircularProgressView.updateProgress(CGFloat(self.selectedPlayerList.count) / CGFloat(totalPlayer), animated: true, initialDelay: 0.0, duration: 1.0, completion: nil)
        self.walletCircularProgressView.updateProgress(CGFloat(selectedPlayerTotalAmount) / CGFloat(totalAmount), animated: true, initialDelay: 0.0, duration: 1.0, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.delegate != nil {
            self.delegate.resetAuction(selectedPlayerList: self.selectedPlayerList)
        }
    }
    
    func setupLocalizationText() {
        self.lblMyTeam.text = self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_MyBuys")
        self.lblSquadTitle.text = self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_Squad")
        self.lblWalletTitle.text = self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_Wallet")
        self.btnSubmitTeam.setTitle(self.getLocalizeTextForKey(keyName: "SecretAuction_btn_SubmitTeam"), for: .normal)
    }
    
    func setProgressAnimation(progress: RPCircularProgress) {
        progress.innerTintColor = .clear
        progress.progressTintColor = UIColor().alertButtonColor
        progress.trackTintColor = .white
        progress.thicknessRatio = 0.1
    }
    
    @IBAction func btnInfo_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AuctionFAQRulesViewController") as! AuctionFAQRulesViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnReset_Clicked(sender: UIButton) {
        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_ResetAllSelections"), preferredStyle: .alert)
        alert.view.tintColor = UIColor().alertButtonColor
        
        let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Yes"), style: .default, handler: { (action) in
            self.selectedPlayerList.removeAll()
            self.navigationController?.popViewController(animated: true)
        })
        
        let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_No"), style: .default, handler: { (action) in
        })
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSubmitTeam_Clicked(sender: UIButton) {
        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_SubmitTeam"), preferredStyle: .alert)
        alert.view.tintColor = UIColor().alertButtonColor
        
        let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Yes"), style: .default, handler: { (action) in
            let arrayPlayerIds = self.selectedPlayerList.map { $0[kAuctioPlayerId] as? Int }
            let playerIds = arrayPlayerIds.map { String($0 ?? 0) }
            self.submitAuctionPlayer(playerIds: playerIds.joined(separator: ","))
        })
        
        let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_No"), style: .default, handler: { (action) in
        })
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension AuctionMyTeamViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return retainPlayerList.count + selectedPlayerList.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(translationX: 0, y: cell.contentView.frame.height)
        UIView.animate(withDuration: 0.5, delay: 0.05 * Double(indexPath.row), animations: {
            cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: cell.contentView.frame.height)
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AuctionPlayerTblCell") as! AuctionPlayerTblCell
        
        var playerDetails = [String: AnyObject]()
        if retainPlayerList.count > indexPath.row {
            cell.btnAdd.isHidden = true
            playerDetails = retainPlayerList[indexPath.row]
        }
        else {
            cell.btnAdd.isHidden = false
            playerDetails = selectedPlayerList[indexPath.row - retainPlayerList.count]
        }
        
        if let imageUrl = playerDetails[kPlayerRolsImage] as? String {
            if let url = URL(string: imageUrl) {
                let imgPhoto = UIImageView()
                imgPhoto.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"), options: nil, progressBlock: nil) { (image, error, type, url) in
                    if let downloadImage = image {
                        cell.imgPlayerType.image = downloadImage.withTintColor(UIColor().themeDarkPinkColor)
                    }
                }
            }
        }
        
        cell.lblName.text = playerDetails[kPlayersName] as? String ?? ""
        cell.lblPrice.text = "\(playerDetails[kAmount] as? Double ?? 0.0) \(AUCTIONVALUE)"
        
        if let imageUrl = playerDetails[kCountryImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgAway.kf.setImage(with: url, placeholder:  #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        cell.btnAdd.addTarget(self, action: #selector(btnRemove_Clicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func btnRemove_Clicked(_ sender: AnyObject) {
        let position = sender.convert(CGPoint.zero, to: tblViewMyTeam)
        if let indexPath = tblViewMyTeam.indexPathForRow(at: position) {
            let newIndexPath = IndexPath.init(row: indexPath.row - retainPlayerList.count, section: indexPath.section)
            self.selectedPlayerList.remove(at: newIndexPath.row)
            self.tblViewMyTeam.deleteRows(at: [indexPath], with: .right)
            
            var selectedPlayerTotalAmount = 0.0
            self.selectedPlayerList.forEach({ selectedPlayerTotalAmount += ($0[kAmount] as? Double ?? 0.0)})
            
            self.lblSquadValue.text = "\(self.selectedPlayerList.count)/\(self.totalPlayer)"
            self.lblWalletValue.text = String(format: "%.2f \(AUCTIONVALUE)", self.totalAmount - selectedPlayerTotalAmount)
            
            self.squadCircularProgressView.updateProgress(CGFloat(self.selectedPlayerList.count) / CGFloat(self.totalPlayer), animated: true, initialDelay: 0.0, duration: 1.0, completion: nil)
            self.walletCircularProgressView.updateProgress(CGFloat(selectedPlayerTotalAmount) / CGFloat(self.totalAmount), animated: true, initialDelay: 0.0, duration: 1.0, completion: nil)
            
            if self.selectedPlayerList.count == 0 {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension AuctionMyTeamViewController {
    func submitAuctionPlayer(playerIds: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "answer": playerIds] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.SETAUCTIONUSERANSWER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let message = responseData[kMessage] as? String {
                        if responseData["is_share"] as? String == kYes {
                            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                            alert.view.tintColor = UIColor().alertButtonColor
                            
                            let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_No"), style: .default, handler: { (action) in
                                let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_ThankYouMessage"), preferredStyle: .alert)
                                alert.view.tintColor = UIColor().alertButtonColor
                                
                                let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                    self.navigationController?.popToRootViewController(animated: true)
                                })
                                alert.addAction(hideAction)
                                self.present(alert, animated: true, completion: nil)
                            })
                            
                            let shareAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Share"), style: .default, handler: { (action) in
                                if let screenShotImage = self.takeScreenshot(theView: self.view) {
                                    if let appUrl = URL.init(string: "https://bit.ly/2OAdhCB") {
                                        let textToShare = "My #SecretAuction picks for the Royals squad! Complete the squad with the players you think will join the Royals.\nDownload App now by clicking the link.\n" + "\(appUrl)"
                                        let objectsToShare = [screenShotImage, textToShare] as [Any]
                                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                                        self.present(activityVC, animated: true, completion: nil)
                                    }
                                    else {
                                        let textToShare = "My #SecretAuction picks for the Royals squad! Complete the squad with the players you think will join the Royals."
                                        let objectsToShare = [screenShotImage, textToShare] as [Any]
                                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                                        self.present(activityVC, animated: true, completion: nil)
                                    }
                                }
                            })
                            
                            alert.addAction(shareAction)
                            alert.addAction(noAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        else {
                            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                            alert.view.tintColor = UIColor().alertButtonColor
                            
                            let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                self.navigationController?.popToRootViewController(animated: true)
                            })
                            alert.addAction(hideAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func takeScreenshot(theView: UIView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(theView.frame.size, true, 0.0)
        theView.drawHierarchy(in: theView.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
