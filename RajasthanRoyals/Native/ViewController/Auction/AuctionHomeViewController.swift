//
//  AuctionHomeViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 08/02/21.
//

import UIKit

class AuctionHomeViewController: BaseViewController {

    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgSecretAuction: UIImageView!
    @IBOutlet weak var gifView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnHowToPlay: UIButton!
    @IBOutlet weak var lblAnd: UILabel!
    @IBOutlet weak var btnTC: UIButton!
    @IBOutlet weak var btnBuyPlayers: CustomButton!
    @IBOutlet weak var lblWinMerchandise: UILabel!
    
    var audioPlayer = AVAudioPlayer()
    var auctionRules = [String: AnyObject]()
    var selectedPlayerList = [[String: AnyObject]]()
    var retainPlayerList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        self.getAuctionRules()
        self.getMyAuctionTeam()
        self.getRetainPlayers()
        
        do {
            if let fileURL = Bundle.main.path(forResource: "Humer", ofType: "wav") {
                self.audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: fileURL))
            }
            else {
                print("No file with specified name exists")
            }
        }
        catch let error {
            print("Can't play the audio file failed with an error \(error.localizedDescription)")
        }
    }
    
    func setupLocalizationText() {
        self.btnHowToPlay.setTitle(self.getLocalizeTextForKey(keyName: "SecretAuction_btn_HowToPlay"), for: .normal)
        self.lblAnd.text = self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_and")
        self.btnTC.setTitle(self.getLocalizeTextForKey(keyName: "SecretAuction_btn_TCs"), for: .normal)
        self.btnBuyPlayers.setTitle(self.getLocalizeTextForKey(keyName: "SecretAuction_btn_PlayNow"), for: .normal)
        self.lblWinMerchandise.text = self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_WinRoyalMerchandise")
    }
    
    @IBAction func btnBuyPlayers_Clicked(sender: UIButton) {
        self.audioPlayer.play()
        
        self.imgSecretAuction.isHidden = true
        self.gifView.isHidden = false
        
        do {
            let gif = try UIImage(gifName: "secret_auction.gif")
            let imageview = UIImageView(gifImage: gif, loopCount: 1) // Use -1 for infinite loop
            imageview.contentMode = .scaleAspectFit
            imageview.delegate = self
            imageview.frame = self.gifView.bounds
            self.gifView.addSubview(imageview)
        }
        catch {
            print(error)
        }
    }
    
    @IBAction func btnHowToPlay_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AuctionFAQRulesViewController") as! AuctionFAQRulesViewController
        viewController.termsConsitionDescription = self.auctionRules["terms_conditions"] as? String ?? ""
        viewController.rulesDescription = self.auctionRules[kDescription] as? String ?? ""
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnTC_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        viewController.strTitle = self.getLocalizeTextForKey(keyName: "SecretAuction_lbl_TermsAndConditions")
        viewController.strDescription = self.auctionRules["terms_conditions"] as? String ?? ""
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension AuctionHomeViewController {
    func getAuctionRules() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: false, URL: baseURLWithoutAuth, apiName: APINAME.GETAUCTIONROULS.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.auctionRules = data
                        
                        self.lblMessage.text = self.auctionRules[kTitle] as? String ?? ""
                    }
                }
            }
        }
    }
    
    func getMyAuctionTeam() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETMYACTIONTEAM.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.selectedPlayerList = data
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getRetainPlayers() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["is_retained": kYes]
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETAUCTIONPLAYER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.retainPlayerList = data
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension AuctionHomeViewController: SwiftyGifDelegate {
    func gifURLDidFinish(sender: UIImageView) {
        print("gifURLDidFinish")
    }

    func gifURLDidFail(sender: UIImageView) {
        print("gifURLDidFail")
    }

    func gifDidStart(sender: UIImageView) {
        print("gifDidStart")
    }
    
    func gifDidLoop(sender: UIImageView) {
        print("gifDidLoop")
    }
    
    func gifDidStop(sender: UIImageView) {
        print("gifDidStop")
        self.imgSecretAuction.isHidden = false
        self.gifView.isHidden = true
        
        for subView in self.gifView.subviews {
            subView.removeFromSuperview()
        }
        
        if self.selectedPlayerList.count > 0 {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SelectedMyTeamViewController") as! SelectedMyTeamViewController
            viewController.auctionRules = self.auctionRules
            viewController.selectedPlayerList = self.selectedPlayerList
            viewController.retainPlayerList = self.retainPlayerList
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AuctionPlayerViewController") as! AuctionPlayerViewController
            viewController.auctionRules = self.auctionRules
            viewController.retainPlayerList = self.retainPlayerList
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
