//
//  ReferFriendViewController.swift
//  RR
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 Sagar Nandha. All rights reserved.
//

import UIKit
import KRProgressHUD
import Alamofire

class LiveScoreViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var segmentView: CustomView!
    @IBOutlet weak var btnLeaderboard: CustomButton!
    @IBOutlet weak var btnLive: CustomButton!
    @IBOutlet weak var btnScorecard: CustomButton!
    
    @IBOutlet weak var liveScoreView: CustomView!
    @IBOutlet weak var lblLive: UILabel!
    @IBOutlet weak var imgFirstTeam: UIImageView!
    @IBOutlet weak var lblFirstInnings: UILabel!
    @IBOutlet weak var lblFirstInningsScore: UILabel!
    @IBOutlet weak var lblFirstInningsOvers: UILabel!
    @IBOutlet weak var imgSecondTeam: UIImageView!
    @IBOutlet weak var lblSecondInnings: UILabel!
    @IBOutlet weak var lblSecondInningsScore: UILabel!
    @IBOutlet weak var lblSecondInningsOvers: UILabel!
    
    @IBOutlet weak var tblLive: UITableView!
    @IBOutlet weak var tblLiveScore: UITableView!
    
    @IBOutlet weak var segmentViewTopConstraint: NSLayoutConstraint!
    
    var timer: Timer!
    var isFromLive = false
    var matchDetails = [String: AnyObject]()
    var matchId = ""
    
    var isLiveSelected = true
    var selectedIndex = 0
    
    //Scorecard Scren
    var inningsList = [[String: AnyObject]]()
    
    var inningObject = [String: AnyObject]()
    var batsmenList = [[String: AnyObject]]()
    var bowlerList = [[String: AnyObject]]()
    
    //Live Screen
    var firstBatsmanDetails = [String: AnyObject]()
    var secondBatsmanDetails = [String: AnyObject]()
    var bowlerDetails = [String: AnyObject]()
    var commentryList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.segmentView.backgroundColor = SegmentViewBGColor
        self.btnLive_Clicked(sender: self.btnLive)
        
        self.addFirebaseCustomeEvent(eventName: "LiveScoreScreen")
        
        self.tblLive.isHidden = false
        self.tblLiveScore.isHidden = true
        
        self.setupLocalizationText()
        
        if !self.matchDetails.isEmpty {
            self.setMatchDetails()
        }
        else {
            self.getMatchDetails()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimer()
    }
    
    func setupLocalizationText() {
        if self.isFromLive {
            self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Title")
        }
        else {
            self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LiveScore_btn_Scorecard")
        }
        self.btnLive.setTitle(self.getLocalizeTextForKey(keyName: "LiveScore_btn_Live"), for: .normal)
        self.btnScorecard.setTitle(self.getLocalizeTextForKey(keyName: "LiveScore_btn_Scorecard"), for: .normal)
        self.lblLive.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Live")
        self.lblFirstInnings.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_FirstInnings")
        self.lblSecondInnings.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_SecondInnings")
    }
    
    //Set Match Details
    func setMatchDetails() {
        self.matchId = matchDetails[kMatchId] as? String ?? ""
        
        self.btnLeaderboard.isHidden = true
        if !isFromLive {
            let firstTeamShortName = matchDetails[kFirstTeamShortName] as? String ?? ""
            let secondTeamShortName = matchDetails[kSecondTeamShortName] as? String ?? ""
            
            if (firstTeamShortName.lowercased() == "rr" || secondTeamShortName.lowercased() == "rr") {
                self.btnLeaderboard.isHidden = false
            }
            
            self.lblLive.isHidden = true
            self.segmentView.isHidden = true
            self.segmentViewTopConstraint.constant = -40
            
            self.tblLive.isHidden = true
            self.tblLiveScore.isHidden = false
        }
        else {
            self.startTimer()
        }
        
        self.getLiveScore(isShowHud: true)
    }
    
    //Start Timer
    func startTimer() {
        if self.timer == nil {
            self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.updateTimeValue), userInfo: nil, repeats: true)
        }
    }
    
    //Stop Timer
    func stopTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    //Update Time
    @objc func updateTimeValue() {
        self.getLiveScore(isShowHud: false)
    }
    
    @IBAction func btnLeaderboard_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PastMatchLeaderboardViewController") as! PastMatchLeaderboardViewController
        viewController.matchId = matchDetails[kMatch_Id] as? Int ?? 0
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnLive_Clicked(sender: UIButton) {
        self.isLiveSelected = true
        
        self.btnLive.backgroundColor = SelectedButtonBGColor
        self.btnScorecard.backgroundColor = DeSelectedButtonBGColor
        self.btnLive.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnScorecard.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.tblLive.isHidden = false
        self.tblLiveScore.isHidden = true
        
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Live Scorecard")
    }
    
    @IBAction func btnScorecard_Clicked(sender: UIButton) {
        self.isLiveSelected = false
        
        self.btnLive.backgroundColor = DeSelectedButtonBGColor
        self.btnScorecard.backgroundColor = SelectedButtonBGColor
        self.btnLive.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnScorecard.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.tblLive.isHidden = true
        self.tblLiveScore.isHidden = false
        
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Commentary")
    }
}

extension LiveScoreViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblLiveScore {
            return inningsList.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblLiveScore {
            if selectedIndex == section {
                return 1 + batsmenList.count + 1 + 1 + bowlerList.count //BatsmanHeader + Batsman + Extra&Total + BowlerHeader + Bowler
            }
            else {
                return 0
            }
        }
        else {
            return 1 + 2 + 1 + 1 + 1 + commentryList.count //BatsmanHeader + Batsman(2) + BowlerHeader + Bowler + Commentry + commentry
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tblLiveScore {
            return 40
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveScoreHeaderCell") as! LiveScoreHeaderCell
        
        cell.lblTeamName.text = self.inningsList[section][kBattingTeamName] as? String ?? ""
        
        cell.imgArrow.image = #imageLiteral(resourceName: "arrow_down_ic").imageWithColor(color: .white)
        if section == self.selectedIndex {
            cell.imgArrow.image = #imageLiteral(resourceName: "arrow_left_ic").imageWithColor(color: .white)
        }
        
        cell.btnHeader.tag = section
        cell.btnHeader.addTarget(self, action: #selector(btnHeader_Clicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblLiveScore {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BatsmanTitleCell") as! BatsmanTitleCell
                
                cell.lblTitle.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Batsman")
                
                return cell
            }
            else if indexPath.row < batsmenList.count + 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BatsmanCell") as! BatsmanCell
                
                if batsmenList.count > indexPath.row - 1 {
                    if let id = batsmenList[indexPath.row - 1][kNameC] as? String {
                        cell.lblName.text = id
                    }
                    
                    if let howOut = batsmenList[indexPath.row - 1][kHowOut] as? String {
                        cell.lblOutby.text = howOut
                        if howOut == "" || howOut == "not out" {
                            cell.lblOutby.isHidden = true
                            cell.lblNameTopConstraint.constant = 12.5
                        }
                        else {
                            cell.lblOutby.isHidden = false
                            cell.lblNameTopConstraint.constant = 5
                        }
                    }
                    
                    if let run = batsmenList[indexPath.row - 1][kRunsCcored] as? String {
                        cell.lblRun.text = run == "" ? "-" : run
                    }
                    
                    if let ball = batsmenList[indexPath.row - 1][kBallsFaced] as? String {
                        cell.lblBall.text = ball == "" ? "-" : ball
                    }
                    
                    if let four = batsmenList[indexPath.row - 1][kFoursScored] as? String {
                        cell.lbl4s.text = four == "" ? "-" : four
                    }
                    
                    if let six = batsmenList[indexPath.row - 1][kSixesScored] as? String {
                        cell.lbl6s.text = six == "" ? "-" : six
                    }
                    
                    if let strikeRate = batsmenList[indexPath.row - 1][kStrikeRate1] as? String {
                        cell.lblSR.text = strikeRate == "" ? "-" : strikeRate
                    }
                }
                
                return cell
            }
            else if indexPath.row >= batsmenList.count && indexPath.row <= batsmenList.count + 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ExtraTotalCell") as! ExtraTotalCell
               
                cell.lblExtra.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Extras")
                cell.lblTotal.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Total")
                
                if let extras = self.inningObject[kExtras] as? [String: AnyObject] {
                    if let totalExtras = extras[kTotalExtras] as? String {
                        cell.lblExtraValue.text = totalExtras == "" ? "0" : totalExtras
                    }
                }
                
                if let total = self.inningObject[kTotal] as? [String: AnyObject] {
                    if let runs = total[kRunsCcored] as? String {
                        if let wickets = total[kWicketsC] as? String {
                            if runs != "" {
                                cell.lblTotalValue.text = runs + "/" + wickets
                            }
                            else {
                                cell.lblTotalValue.text = "0/0"
                            }
                        }
                    }
                }
                
                return cell
            }
            else if indexPath.row <= batsmenList.count + 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BowlerTitleCell") as! BowlerTitleCell
                
                cell.lblTitle.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Bowler")
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BowlerCell") as! BowlerCell
                
                let index = indexPath.row - (batsmenList.count + 3)
                
                if bowlerList.count > index {
                    if let id = bowlerList[index][kNameC] as? String {
                        cell.lblName.text = id
                    }
                    
                    if let over = bowlerList[index][kOversBowled] as? String {
                        if let balls = bowlerList[index][kBallsBowled] as? String {
                            cell.lblOver.text = over + "." + balls
                        }
                    }
                    
                    if let maiden = bowlerList[index][kMaidensBowled] as? String {
                        cell.lblMaiden.text = maiden
                    }
                    
                    if let run = bowlerList[index][kRunsConceded] as? String {
                        cell.lblRun.text = run
                    }
                    
                    if let wicket = bowlerList[index][kWicketsTaken] as? String {
                        cell.lblWicket.text = wicket
                    }
                    
                    if let economy = bowlerList[index][kEconomy1] as? String {
                        cell.lblEco.text = economy
                    }
                }
                
                return cell
            }
        }
        else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BatsmanTitleCell") as! BatsmanTitleCell
                
                cell.lblTitle.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Batsman")
                
                return cell
            }
            else if indexPath.row < 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BatsmanCell") as! BatsmanCell
                
                if indexPath.row == 1 {
                    if let id = firstBatsmanDetails[kNameC] as? String {
                        cell.lblName.text = id
                    }
                    else {
                        cell.lblName.text = "-"
                    }

                    if let run = firstBatsmanDetails[kRunsCcored] as? String {
                        cell.lblRun.text = run == "" ? "-" : run
                    }
                    else {
                        cell.lblRun.text = "-"
                    }
                    
                    if let ball = firstBatsmanDetails[kBallsFaced] as? String {
                        cell.lblBall.text = ball == "" ? "-" : ball
                    }
                    else {
                        cell.lblBall.text = "-"
                    }
                    
                    if let four = firstBatsmanDetails[kFoursScored] as? String {
                        cell.lbl4s.text = four == "" ? "-" : four
                    }
                    else {
                        cell.lbl4s.text = "-"
                    }
                    
                    if let six = firstBatsmanDetails[kSixesScored] as? String {
                        cell.lbl6s.text = six == "" ? "-" : six
                    }
                    else {
                        cell.lbl6s.text = "-"
                    }
                    
                    if let strikeRate = firstBatsmanDetails[kStrikeRate1] as? String {
                        cell.lblSR.text = strikeRate == "" ? "-" : strikeRate
                    }
                    else {
                        cell.lblSR.text = "-"
                    }
                }
                else {
                    if let id = secondBatsmanDetails[kNameC] as? String {
                        cell.lblName.text = id
                    }
                    else {
                        cell.lblName.text = "-"
                    }

                    if let run = secondBatsmanDetails[kRunsCcored] as? String {
                        cell.lblRun.text = run == "" ? "-" : run
                    }
                    else {
                        cell.lblRun.text = "-"
                    }
                    
                    if let ball = secondBatsmanDetails[kBallsFaced] as? String {
                        cell.lblBall.text = ball == "" ? "-" : ball
                    }
                    else {
                        cell.lblBall.text = "-"
                    }
                    
                    if let four = secondBatsmanDetails[kFoursScored] as? String {
                        cell.lbl4s.text = four == "" ? "-" : four
                    }
                    else {
                        cell.lbl4s.text = "-"
                    }
                    
                    if let six = secondBatsmanDetails[kSixesScored] as? String {
                        cell.lbl6s.text = six == "" ? "-" : six
                    }
                    else {
                        cell.lbl6s.text = "-"
                    }
                    
                    if let strikeRate = secondBatsmanDetails[kStrikeRate1] as? String {
                        cell.lblSR.text = strikeRate == "" ? "-" : strikeRate
                    }
                    else {
                        cell.lblSR.text = "-"
                    }
                }
                
                return cell
            }
            else if indexPath.row < 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BowlerTitleCell") as! BowlerTitleCell
                
                cell.lblTitle.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Bowler")
                
                return cell
            }
            else if indexPath.row < 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BowlerCell") as! BowlerCell
                
                if let id = bowlerDetails[kNameC] as? String {
                    cell.lblName.text = id
                }
                else {
                    cell.lblName.text = "-"
                }
                
                if let over = bowlerDetails[kOversBowled] as? String {
                    if let balls = bowlerDetails[kBallsBowled] as? String {
                        cell.lblOver.text = over + "." + balls
                    }
                }
                else {
                    cell.lblOver.text = "-"
                }
                
                if let maiden = bowlerDetails[kMaidensBowled] as? String {
                    cell.lblMaiden.text = maiden
                }
                else {
                    cell.lblMaiden.text = "-"
                }
                
                if let run = bowlerDetails[kRunsConceded] as? String {
                    cell.lblRun.text = run
                }
                else {
                    cell.lblRun.text = "-"
                }
                
                if let wicket = bowlerDetails[kWicketsTaken] as? String {
                    cell.lblWicket.text = wicket
                }
                else {
                    cell.lblWicket.text = "-"
                }
                
                if let economy = bowlerDetails[kEconomy1] as? String {
                    cell.lblEco.text = economy
                }
                else {
                    cell.lblEco.text = "-"
                }
                
                return cell
            }
            else if indexPath.row < 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CommentaryHeaderCell") as! CommentaryHeaderCell
                
                cell.lblCommentary.text = self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Commentary")
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CommentaryCell") as! CommentaryCell
                
                cell.lblTitle.layer.cornerRadius = cell.lblTitle.frame.height / 2
                cell.lblTitle.layer.masksToBounds = true
                
                if commentryList.count > indexPath.row - 5 {
                    if commentryList[indexPath.row - 5][kIsWicket] as? String == "1" {
                        cell.lblTitle.text = "W"
                        cell.lblTitle.backgroundColor = UIColor(named: "Pink_Color")
                        cell.lblTitle.layer.borderWidth = 1
                        cell.lblTitle.layer.borderColor = UIColor.clear.cgColor
                    }
                    else {
                        if let runs = commentryList[indexPath.row - 5][kRuns] as? String {
                            cell.lblTitle.text = runs
                            if runs == "6" {
                                cell.lblTitle.backgroundColor = UIColor(named: "Pink_Color")
                                cell.lblTitle.layer.borderWidth = 1
                                cell.lblTitle.layer.borderColor = UIColor.clear.cgColor
                            }
                            else if runs == "4" {
                                cell.lblTitle.backgroundColor = UIColor(named: "Blue_Color")
                                cell.lblTitle.layer.borderWidth = 1
                                cell.lblTitle.layer.borderColor = UIColor.clear.cgColor
                            }
                            else {
                                cell.lblTitle.layer.borderWidth = 1
                                cell.lblTitle.layer.borderColor = UIColor.white.cgColor
                                cell.lblTitle.backgroundColor = .clear
                            }
                        }
                    }
                    
                    if let comment = commentryList[indexPath.row - 5][kComment] as? String {
                        cell.lblDescription.text = comment
                    }
                }
                
                return cell
            }
        }
    }
    
    @objc func btnHeader_Clicked(_ sender: UIButton) {
        self.selectedIndex = sender.tag
    
        self.inningObject = self.inningsList[self.selectedIndex]
        
        //Batsmen List
        if let batsmenObject = self.inningObject[kBatsmen] as? [String : AnyObject] {
            if let batsmenList = batsmenObject[kBatsman] as? [[String : AnyObject]] {
                self.batsmenList = batsmenList
            }
        }
        
        //Bowler List
        if let bowlerObject = self.inningObject[kBowlers] as? [String : AnyObject] {
            if let bowlerList = bowlerObject[kBowler] as? [[String : AnyObject]] {
                self.bowlerList = bowlerList
            }
        }
        
        self.tblLiveScore.reloadData()
        self.tblLiveScore.scrollToRow(at: IndexPath.init(row: 0, section: self.selectedIndex), at: .top, animated: true)
    }
}

extension LiveScoreViewController {
    func getMatchDetails() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kMatch_Id: self.matchId]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETMATCHESDETAIL.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.matchDetails = data
                        
                        self.setMatchDetails()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getLiveScore(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kMatchId: self.matchId]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETLIVESCORE.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        if let matchDetail = data[kMatchDetail] as? [String : AnyObject] {
                            if let currentInningsId = matchDetail[kCurrentInningsId] as? String {
                                let intCurrentInningsId = Int(currentInningsId) ?? 1
                                self.selectedIndex = intCurrentInningsId - 1
                            }
                        }
                        
                        if let innings = data[kInnings] as? [[String : AnyObject]] {
                            self.inningsList = innings
                        }
                        
                        self.inningObject = self.inningsList[self.selectedIndex]
                        
                        //Batsmen List
                        if let batsmenObject = self.inningObject[kBatsmen] as? [String : AnyObject] {
                            if let batsmenList = batsmenObject[kBatsman] as? [[String : AnyObject]] {
                                self.batsmenList = batsmenList
                            }
                        }
                        
                        //Bowler List
                        if let bowlerObject = self.inningObject[kBowlers] as? [String : AnyObject] {
                            if let bowlerList = bowlerObject[kBowler] as? [[String : AnyObject]] {
                                self.bowlerList = bowlerList
                            }
                        }
                        
                        let array = self.batsmenList as Array
                        let onStrikeMatchIndex = array.index {
                            if $0[kOnStrike] as? String == "1" {
                                return true
                            }
                            return false
                        }
                        
                        let nonStrikeMatchIndex = array.index {
                            if $0[kNonStrike] as? String == "1" {
                                return true
                            }
                            return false
                        }
                        
                        let array1 = self.bowlerList as Array
                        let onStrikeMatchIndex1 = array1.index {
                            if $0[kOnStrike] as? String == "1" {
                                return true
                            }
                            return false
                        }
                        
                        if onStrikeMatchIndex != nil {
                            self.firstBatsmanDetails = self.batsmenList[onStrikeMatchIndex!]
                        }
                        else {
                            self.firstBatsmanDetails.removeAll()
                        }
                        
                        if nonStrikeMatchIndex != nil {
                            self.secondBatsmanDetails = self.batsmenList[nonStrikeMatchIndex!]
                        }
                        else {
                            self.secondBatsmanDetails.removeAll()
                        }
                        
                        if onStrikeMatchIndex1 != nil {
                            self.bowlerDetails = self.bowlerList[onStrikeMatchIndex1!]
                        }
                        else {
                            self.bowlerDetails.removeAll()
                        }
                        
                        self.tblLive.reloadData()
                        self.tblLiveScore.reloadData()
                        
                        self.displayBothTeamScore()
                        
                        self.getLiveCommentary(isShowHud: isShowHud)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getLiveCommentary(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kMatchId: self.matchId]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETLIVECOMMENTARY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        if let ballbyball = data[kBallbyball] as? [String : AnyObject] {
                            if let commentry = ballbyball[kltball] as? [[String : AnyObject]] {
                                self.commentryList = commentry.reversed()
                            }
                        }
                    }
                    
                    self.tblLive.reloadData()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    //Display Both Team Score
    func displayBothTeamScore() {
        //First Inning
        if self.inningsList.count > 0 {
            let inningObject = self.inningsList[0]
            
            let ourFirstTeamName = self.matchDetails[kFirstTeamShortName] as? String ?? ""
            let firstTeamName = inningObject[kBattingTeamShortName] as? String ?? ""
            
            if ourFirstTeamName.lowercased() == firstTeamName.lowercased() {
                if let imageUrl = self.matchDetails[kFirstImage] as? String {
                    if let url = URL(string: imageUrl) {
                        self.imgFirstTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
            }
            else {
                if let imageUrl = self.matchDetails[kSecondImage] as? String {
                    if let url = URL(string: imageUrl) {
                        self.imgSecondTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
            }
            
            if let total = inningObject[kTotal] as? [String: AnyObject] {
                if let runs = total[kRunsCcored] as? String {
                    if let wickets = total[kWicketsC] as? String {
                        if runs != "" {
                            self.lblFirstInningsScore.text = runs + "/" + wickets
                        }
                        else {
                            self.lblFirstInningsScore.text = "0/0"
                        }
                    }
                }
                
                if let overs = total[kOversC] as? String {
                    if overs != "" {
                        self.lblFirstInningsOvers.text = "\(overs) \(self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Overs"))"
                    }
                    else {
                        self.lblFirstInningsOvers.text = "0.0 \(self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Overs"))"
                    }
                }
            }
        }
        
        //Second Inning
        if self.inningsList.count > 1 {
            let inningObject = self.inningsList[1]
            
            let ourSecondTeamName = self.matchDetails[kSecondTeamShortName] as? String ?? ""
            let secondTeamName = inningObject[kBattingTeamShortName] as? String ?? ""
            
            if ourSecondTeamName.lowercased() == secondTeamName.lowercased() {
                if let imageUrl = self.matchDetails[kSecondImage] as? String {
                    if let url = URL(string: imageUrl) {
                        self.imgSecondTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
            }
            else {
                if let imageUrl = self.matchDetails[kFirstImage] as? String {
                    if let url = URL(string: imageUrl) {
                        self.imgFirstTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
            }
            
            if let total = inningObject[kTotal] as? [String: AnyObject] {
                if let runs = total[kRunsCcored] as? String {
                    if let wickets = total[kWicketsC] as? String {
                        if runs != "" {
                            self.lblSecondInningsScore.text = runs + "/" + wickets
                        }
                        else {
                            self.lblSecondInningsScore.text = "0/0"
                        }
                    }
                }
                
                if let overs = total[kOversC] as? String {
                    if overs != "" {
                        self.lblSecondInningsOvers.text = "\(overs) \(self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Overs"))"
                    }
                    else {
                        self.lblSecondInningsOvers.text = "0.0 \(self.getLocalizeTextForKey(keyName: "LiveScore_lbl_Overs"))"
                    }
                }
            }
        }
    }
}

class LiveScoreHeaderCell: UITableViewCell {
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnHeader: UIButton!
}

class BatsmanTitleCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRun: UILabel!
    @IBOutlet weak var lblBall: UILabel!
    @IBOutlet weak var lbl4s: UILabel!
    @IBOutlet weak var lbl6s: UILabel!
    @IBOutlet weak var lblSR: UILabel!
}

class BatsmanCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblOutby: UILabel!
    @IBOutlet weak var lblRun: UILabel!
    @IBOutlet weak var lblBall: UILabel!
    @IBOutlet weak var lbl4s: UILabel!
    @IBOutlet weak var lbl6s: UILabel!
    @IBOutlet weak var lblSR: UILabel!
    
    @IBOutlet weak var lblNameTopConstraint: NSLayoutConstraint!
}

class BowlerTitleCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOver: UILabel!
    @IBOutlet weak var lblMaiden: UILabel!
    @IBOutlet weak var lblRun: UILabel!
    @IBOutlet weak var lblWicket: UILabel!
    @IBOutlet weak var lblEco: UILabel!
}

class BowlerCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblOver: UILabel!
    @IBOutlet weak var lblMaiden: UILabel!
    @IBOutlet weak var lblRun: UILabel!
    @IBOutlet weak var lblWicket: UILabel!
    @IBOutlet weak var lblEco: UILabel!
}

class ExtraTotalCell: UITableViewCell {
    @IBOutlet weak var lblExtra: UILabel!
    @IBOutlet weak var lblExtraValue: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
}

class CommentaryHeaderCell: UITableViewCell {
    @IBOutlet weak var lblCommentary: UILabel!
}

class CommentaryCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
}
