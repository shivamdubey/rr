//
//  ViewController.swift
//  <Project Name>
//
//  Created by Sagar Nandha
//  Copyright © 2021 Sagar's MacBookAir. All rights reserved.
//

import UIKit

protocol RatingDelegate {
    func submitRating(rating: Double)
}

class RatingPopupViewController: BaseViewController {
    
    @IBOutlet weak var imgLogo: CustomImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var btnCancel: CustomButton!
    @IBOutlet weak var btnSubmit: CustomButton!

    var delegate: RatingDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnSubmit.isEnabled = false
        self.btnSubmit.setTitleColor(UIColor.lightGray, for: .normal)
        self.ratingView.delegate = self
    }
    
    @IBAction func btnCancel_Clicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnSubmit_Clicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        if self.delegate != nil {
            self.delegate.submitRating(rating: ratingView.rating)
        }
    }
}

extension RatingPopupViewController: FloatRatingViewDelegate {
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        if rating > 0 {
            self.btnSubmit.isEnabled = true
            self.btnSubmit.setTitleColor(UIColor().alertButtonColor, for: .normal)
        }
        else {
            self.btnSubmit.isEnabled = false
            self.btnSubmit.setTitleColor(UIColor.lightGray, for: .normal)
        }
    }
}
