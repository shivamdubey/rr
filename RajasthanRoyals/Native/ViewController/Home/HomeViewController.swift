//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class HomeViewController: BaseViewController, TabItem {
    
    var indexOfPlayer:Int = 0
    var tabImage: UIImage? {
        return UIImage(named: "tab_matches")
    }
    var viewController: UIViewController!
    @IBOutlet weak var tblHome: UITableView!
    
    var isMatchCellFirstTime = true
    var homeList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "HomeScreen")
        
        self.tblHome.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
        
        self.appSetting()
        self.getHomeScreenData(isShowHud: true)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReloadHomeScreen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getHomeScreenData), name: NSNotification.Name(rawValue: "ReloadHomeScreen"), object: nil)
        
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Home Page")
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if homeList[indexPath.row][kType] as? String == ITEMTYPE.Banner.rawValue {
            if let itemList = homeList[indexPath.row][kData] as? [[String: AnyObject]], itemList.count > 0 {
                let imageHeight = itemList[0]["height"] as? Double ?? 0.0
                let imageWidth = itemList[0]["width"] as? Double ?? 0.0
                if imageHeight > 0.0 && imageWidth > 0.0 {
                    let height = (imageHeight * Double(SCREEN_WIDTH)) / imageWidth
                    
                    if itemList.count == 1 {
                        return CGFloat(height)
                    }
                    else {
                        return CGFloat(height + 28)
                    }
                }
            }
            return 210
        }
        else if homeList[indexPath.row][kType] as? String == ITEMTYPE.Players.rawValue
        {
            return 240
        }
        else if homeList[indexPath.row][kType] as? String == ITEMTYPE.News.rawValue
        {
            return 240
        }
        else if homeList[indexPath.row][kType] as? String == ITEMTYPE.Gallery.rawValue
        {
            return 300
        }
        else if homeList[indexPath.row][kType] as? String == ITEMTYPE.Video.rawValue
        {
            if homeList[indexPath.row][kSubType] as? String == ITEMTYPE.Royal.rawValue{
                return 300
            }
            else{
                return 240
            }
        }
        else {
            return tableView.estimatedRowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if homeList[indexPath.row][kType] as? String == ITEMTYPE.Banner.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeBannerCell") as! HomeBannerCell
            cell.viewController = self
            
            cell.pageaControl.isHidden = false
            cell.pageaControlTopConstraint.constant = -80
            if let data = homeList[indexPath.row][kData] as? [[String: AnyObject]] {
                if data.count == 1 {
                    cell.pageaControl.isHidden = true
                    cell.pageaControlTopConstraint.constant = -28
                }
            }
            
            cell.setCollectionViewData(type: homeList[indexPath.row][kType] as? String ?? "", itemList: homeList[indexPath.row][kData] as? [[String: AnyObject]] ?? [[:]])

            return cell
        }
        else if homeList[indexPath.row][kType] as? String == ITEMTYPE.Matches.rawValue && homeList[indexPath.row][kSubType] as? String == ITEMTYPE.Matches.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeMatchCell") as! HomeMatchCell
            cell.viewController = self
            
            if self.isMatchCellFirstTime {
                self.isMatchCellFirstTime = false
                cell.setCollectionViewData(subType: homeList[indexPath.row][kSubType] as? String ?? "")
            }
            
            return cell
        }
        else if homeList[indexPath.row][kType] as? String == ITEMTYPE.Matches.rawValue && homeList[indexPath.row][kSubType] as? String == MATCHSTATUS.UPCOMING.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeMatchCell") as! HomeMatchCell
            cell.viewController = self
            
            cell.setCollectionViewData(subType: homeList[indexPath.row][kSubType] as? String ?? "")
            
            return cell
        }
        else if homeList[indexPath.row][kType] as? String == ITEMTYPE.Podcast.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeItemPodcastCell") as! HomeItemPodcastCell
            cell.viewController = self
            
            cell.lblTitle.text = homeList[indexPath.row][kTitle] as? String ?? ""
            
            cell.btnSeeAll.setTitle(homeList[indexPath.row][kButtonTitle] as? String ?? "", for: .normal)
            let image = UIImage(named: "arrow-left-circle")
//image.withRenderingMode(.alwaysOriginal)
            cell.btnSeeAll.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
            cell.btnSeeAll.imageEdgeInsets.right = -70
            cell.btnSeeAll.titleEdgeInsets.left = -70
            cell.btnSeeAll.addTarget(self, action: #selector(btnSeeAll_Clicked(_:)), for: .touchUpInside)
            
            cell.setCollectionViewData(type: homeList[indexPath.row][kType] as? String ?? "", itemList: homeList[indexPath.row][kData] as? [[String: AnyObject]] ?? [[:]])
            
            return cell
        }
        else if homeList[indexPath.row][kType] as? String == ITEMTYPE.Story.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeStoryCell") as! HomeStoryCell
            cell.viewController = self
            
            cell.lblTitle.text = homeList[indexPath.row][kTitle] as? String ?? ""
            
            cell.btnSeeAll.setTitle(homeList[indexPath.row][kButtonTitle] as? String ?? "", for: .normal)
            let image = UIImage(named: "arrow-left-circle")
//image.withRenderingMode(.alwaysOriginal)
            cell.btnSeeAll.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
            cell.btnSeeAll.addTarget(self, action: #selector(btnSeeAll_Clicked(_:)), for: .touchUpInside)
            cell.btnSeeAll.imageEdgeInsets.right = -70
            cell.btnSeeAll.titleEdgeInsets.left = -70
            cell.isCreateStory = homeList[indexPath.row]["is_create_story"] as? String ?? ""
            cell.setCollectionViewData(type: homeList[indexPath.row][kType] as? String ?? "", itemList: homeList[indexPath.row][kData] as? [[String: AnyObject]] ?? [[:]])
            
            return cell
        }
       else if homeList[indexPath.row][kType] as? String == ITEMTYPE.Players.rawValue
       {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomePlayerCustomCell") as! HomePlayerCustomCell
            cell.viewController = self
            
            cell.lblTitle.text = homeList[indexPath.row][kTitle] as? String ?? ""
            
            cell.btnSeeAll.setTitle(homeList[indexPath.row][kButtonTitle] as? String ?? "", for: .normal)
            let image = UIImage(named: "arrow-left-circle")

            cell.btnSeeAll.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
            cell.btnSeeAll.imageEdgeInsets.right = -80
            cell.btnSeeAll.titleEdgeInsets.left = -80
            cell.btnSeeAll.addTarget(self, action: #selector(btnSeeAll_Clicked(_:)), for: .touchUpInside)
           
           cell.btnNext.setTitle("", for: .normal)
           cell.btnPrevious.setTitle("", for: .normal)
           
           cell.btnNext.tag = indexPath.row
           cell.btnPrevious.tag = indexPath.row
           
           cell.btnNext.addTarget(self, action: #selector(btnNext_Clicked(_:)), for: .touchUpInside)
           cell.btnPrevious.addTarget(self, action: #selector(btnPrevious_Clicked(_:)), for: .touchUpInside)
           cell.itemList = homeList[indexPath.row][kData] as? [[String: AnyObject]] ?? [[:]]
           let itemListData = homeList[indexPath.row][kData] as? [[String: AnyObject]] ?? [[:]]
           if let imageUrl = itemListData[indexOfPlayer][kImage] as? String {
               if let url = URL(string: imageUrl) {
                   cell.imgvPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
               }
           }
           cell.lblPlayerName.text = itemListData[indexOfPlayer][kTitle] as? String ?? ""
           
         
           
            return cell
        }
        else if homeList[indexPath.row][kType] as? String == ITEMTYPE.Gallery.rawValue
        {
             let cell = tableView.dequeueReusableCell(withIdentifier: "HomeGalleryCustomCell") as! HomeGalleryCustomCell
             cell.viewController = self
             
             cell.lblTitle.text = homeList[indexPath.row][kTitle] as? String ?? ""
             
             cell.btnSeeAll.setTitle(homeList[indexPath.row][kButtonTitle] as? String ?? "", for: .normal)
             let image = UIImage(named: "arrow-left-circle")
 
             cell.btnSeeAll.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
             cell.btnSeeAll.imageEdgeInsets.right = -80
             cell.btnSeeAll.titleEdgeInsets.left = -80
             cell.btnSeeAll.addTarget(self, action: #selector(btnSeeAll_Clicked(_:)), for: .touchUpInside)
            
            let itemListData = homeList[indexPath.row][kData] as? [[String: AnyObject]] ?? [[:]]
            if itemListData.count>=1 {
            if let imageUrl = itemListData[0][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgvFirst.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            }
            if itemListData.count>=2 {
            if let imageUrl = itemListData[1][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgvSecond.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            }
            if itemListData.count>=3 {
            if let imageUrl = itemListData[2][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgvThird.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            }
            if itemListData.count>=4 {
            if let imageUrl = itemListData[3][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgvFour.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            }
            if itemListData.count>=5 {
            if let imageUrl = itemListData[4][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgvFive.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeItemCell") as! HomeItemCell
            cell.viewController = self
            
            cell.lblTitle.text = homeList[indexPath.row][kTitle] as? String ?? ""
            
            cell.btnSeeAll.setTitle(homeList[indexPath.row][kButtonTitle] as? String ?? "", for: .normal)
            let image = UIImage(named: "arrow-left-circle")
//image.withRenderingMode(.alwaysOriginal)
            cell.btnSeeAll.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
            cell.btnSeeAll.imageEdgeInsets.right = -80
            cell.btnSeeAll.titleEdgeInsets.left = -80
            cell.btnSeeAll.addTarget(self, action: #selector(btnSeeAll_Clicked(_:)), for: .touchUpInside)
            
            cell.setCollectionViewData(type: homeList[indexPath.row][kSubType] as? String ?? "", itemList: homeList[indexPath.row][kData] as? [[String: AnyObject]] ?? [[:]])
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if homeList[indexPath.row][kType] as? String == ITEMTYPE.Players.rawValue
        {
            let cell = tblHome.cellForRow(at: indexPath) as? HomePlayerCustomCell
            let playerDetailsVC = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "PlayerDetailsViewController") as! PlayerDetailsViewController
            playerDetailsVC.slug = cell?.itemList[indexOfPlayer][kSlug] as? String ?? ""
            self.navigationController?.pushViewController(playerDetailsVC, animated: true)
        }
    }
    
    @objc func tappedOnBanner(gesture: UIGestureRecognizer) {
        let position = gesture.location(in: tblHome)
        if let indexPath = tblHome.indexPathForRow(at: position) {
            if let data = homeList[indexPath.row][kData] as? [[String: AnyObject]], data.count > 0 {
                if data[0][kType] as? String == "Empty" {
                    if let redirectScreen = homeList[indexPath.row][kRedirectScreenios] as? String, !redirectScreen.isEmpty {
                        do {
                            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                            let range = NSRange(redirectScreen.startIndex..<redirectScreen.endIndex, in: redirectScreen)
                            if let _ = detector.firstMatch(in: redirectScreen, options: [], range: range) {
                                self.openUrlInSafariViewController(urlString: redirectScreen)
                            }
                            else {
                                if let viewController = self.storyBoard.instantiateViewController(withIdentifier: redirectScreen) as? UIViewController {
                                    if let buttonRedirect = homeList[indexPath.row][kButtonRedirectios] as? String {
                                        if buttonRedirect == REDIRECTTYPE.Tabbar.rawValue {
                                            NotificationCenter.default.post(name: NSNotification.Name("SelectSpacificTab"), object: nil, userInfo: [kTabbarIndexios: homeList[indexPath.row][kTabbarIndexios] as? String ?? ""])
                                        }
                                        else if buttonRedirect == REDIRECTTYPE.Menu.rawValue {
                                            self.slideMenuController()?.changeMainViewController(viewController, close: true)
                                        }
                                        else {
                                            self.navigationController?.pushViewController(viewController, animated: true)
                                        }
                                    }
                                }
                            }
                        }
                        catch {
                        }
                    }
                }
                else if data[0][kType] as? String == VIDEOTYPE.LOCAL.rawValue {
                    let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "videoName": homeList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.VIDEOS.rawValue]
                    BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
                    
                    if let video = data[0][kFiles] as? String {
                        if let videoURL = URL(string: video) {
                            let player = AVPlayer(url: videoURL)
                            let playerViewController = AVPlayerViewController()
                            playerViewController.player = player
                            self.present(playerViewController, animated: true) {
                                playerViewController.player!.play()
                            }
                        }
                    }
                }
                else if data[0][kType] as? String == VIDEOTYPE.YOUTUBE.rawValue {
                    let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "videoName": data[0][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.VIDEOS.rawValue]
                    BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
                    
                    if let videoUrl = data[0][kFiles] as? String {
                        let array = videoUrl.components(separatedBy: "=")
                        if array.count > 1 {
                            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "YouTubePlayerViewController") as! YouTubePlayerViewController
                            viewController.videoId = array[1]
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }
                }
                else if data[0][kType] as? String == ITEMTYPE.News.rawValue {
                    if let redirectScreen = homeList[indexPath.row][kRedirectScreenios] as? String, !redirectScreen.isEmpty {
                        let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "newsArticleName": redirectScreen, "eventType": POINTSEVENTTYPE.LATEST_NEWS.rawValue]
                        BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
                        
                        let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as! NewsDetailsViewController
                        viewController.slug = redirectScreen
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
                else if data[0][kType] as? String == "Gallery" {
                    if let redirectScreen = homeList[indexPath.row][kRedirectScreenios] as? String, !redirectScreen.isEmpty {
                        let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "galleryName": redirectScreen, "eventType": POINTSEVENTTYPE.GALLERY_VIEW.rawValue]
                        BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
                        
                        let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "GalleryImageViewController") as! GalleryImageViewController
                        viewController.slug = redirectScreen
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
            }
        }
    }
    
    @objc func btnSeeAll_Clicked(_ sender: AnyObject) {
        let position = sender.convert(CGPoint.zero, to: tblHome)
        if let indexPath = tblHome.indexPathForRow(at: position) {
            if let redirectScreen = homeList[indexPath.row][kRedirectScreenios] as? String, !redirectScreen.isEmpty {
                if let viewController = self.storyBoard.instantiateViewController(withIdentifier: redirectScreen) as? UIViewController {
                    if let buttonRedirect = homeList[indexPath.row][kButtonRedirectios] as? String {
                        if buttonRedirect == REDIRECTTYPE.Tabbar.rawValue {
                            NotificationCenter.default.post(name: NSNotification.Name("SelectSpacificTab"), object: nil, userInfo: [kTabbarIndexios: homeList[indexPath.row][kTabbarIndexios] as? String ?? ""])
                        }
                        else if buttonRedirect == REDIRECTTYPE.Menu.rawValue {
                            if homeList[indexPath.row][kType] as? String == ITEMTYPE.News.rawValue || homeList[indexPath.row][kType] as? String == ITEMTYPE.Video.rawValue || homeList[indexPath.row][kType] as? String == ITEMTYPE.Gallery.rawValue {
                                if let tmpViewController = viewController as? MediaViewController {
                                    self.slideMenuController()?.closeLeft()
                                    tmpViewController.type = homeList[indexPath.row][kType] as? String ?? ""
                                   // self.slideMenuController()?.changeMainViewController(tmpViewController, close: true)
                                    self.navigationController?.pushViewController(tmpViewController, animated: true)
                                }
                                else {
                                    self.slideMenuController()?.changeMainViewController(viewController, close: true)
                                }
                            }
                            else {
//                                if homeList[indexPath.row][kType] as? String == ITEMTYPE.Players.rawValue{
//                                    self.navigationController?.pushViewController(viewController, animated: true)
//                                }
//                                else{
                                    self.slideMenuController()?.changeMainViewController(viewController, close: true)
                                //}
                            }
                        }
                        else {
                            if homeList[indexPath.row][kType] as? String == ITEMTYPE.Story.rawValue {
                                if let tmpViewController = viewController as? AllStoriesViewController {
                                    tmpViewController.isCreateStory = homeList[indexPath.row]["is_create_story"] as? String ?? ""
                                    self.navigationController?.pushViewController(tmpViewController, animated: true)
                                }
                                else {
                                    self.navigationController?.pushViewController(viewController, animated: true)
                                }
                            }
                            else {
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    @objc func btnNext_Clicked(_ sender: AnyObject) {
        //let cell = tableView.cellForRowAtIndexPath(IndexPath(forRow: 0, inSection: sender.tag)) as! HomePlayerCustomCell
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tblHome.cellForRow(at: indexPath) as? HomePlayerCustomCell
        guard let totalRecords = cell?.itemList.count else { return }
        if indexOfPlayer < totalRecords - 1 {
        indexOfPlayer = indexOfPlayer+1
        }
        
       
        if let imageUrl = cell?.itemList[indexOfPlayer][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell?.imgvPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        cell?.lblPlayerName.text = cell?.itemList[indexOfPlayer][kTitle] as? String ?? ""
        
      
        //cell?.lblPlayerName.text = "bhupendra"
        
    }
    @objc func btnPrevious_Clicked(_ sender: AnyObject) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tblHome.cellForRow(at: indexPath) as? HomePlayerCustomCell
        if indexOfPlayer > 0{
        indexOfPlayer = indexOfPlayer-1
        }
        if let imageUrl = cell?.itemList[indexOfPlayer][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell?.imgvPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        cell?.lblPlayerName.text = cell?.itemList[indexOfPlayer][kTitle] as? String ?? ""
        
    }

}

class HomeItemCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak var itemCV: UICollectionView!
    
    @IBOutlet weak var itemCVHeightConstraint: NSLayoutConstraint!
    
    var viewController: UIViewController!
    var type = ""
    var itemList = [[String: AnyObject]]()
    
    func setCollectionViewData(type: String, itemList: [[String: AnyObject]]) {
        self.type = type
        self.itemList = itemList
        
        let layout = itemCV.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.minimumLineSpacing = 20
        
        if self.type == ITEMTYPE.News.rawValue {
            self.itemCVHeightConstraint.constant = 240
        }
        else if self.type == ITEMTYPE.Video.rawValue {
            //self.itemCVHeightConstraint.constant = 150
        }
        else if self.type == ITEMTYPE.Gallery.rawValue {
            self.itemCVHeightConstraint.constant = 175
        }
        else if self.type == ITEMTYPE.Royal.rawValue {
            self.itemCVHeightConstraint.constant = 250
        }
        
        self.itemCV.dataSource = self
        self.itemCV.delegate = self
        self.itemCV.reloadData()
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if type == ITEMTYPE.News.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVideoNewCell", for: indexPath) as! HomeVideoNewCell
            
            if let imageUrl = itemList[indexPath.item][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgVideoNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
          
            cell.lblTitle.text = itemList[indexPath.item][kTitle] as? String
            cell.btnPlayVideo.isHidden = true
            cell.btnShareVideo .setTitle("", for: .normal)
            cell.btnShareVideo.tag = indexPath.item
            cell.btnShareVideo.addTarget(self, action: #selector(btnShareSocial_Clicked(_:)), for: .touchUpInside)
            return cell
        }
        else if type == ITEMTYPE.Video.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVideoNewCell", for: indexPath) as! HomeVideoNewCell
            
            if let imageUrl = itemList[indexPath.item][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgVideoNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            cell.imgVideoNews.layer.borderWidth = 1.5
            cell.imgVideoNews.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
            cell.imgVideoNews.layer.cornerRadius = 5
            cell.lblTitle.text = itemList[indexPath.item][kTitle] as? String
            cell.btnShareVideo .setTitle("", for: .normal)
            cell.btnShareVideo.tag = indexPath.item
            cell.btnPlayVideo.isHidden = false
            cell.btnShareVideo.addTarget(self, action: #selector(btnShareSocial_Clicked(_:)), for: .touchUpInside)

            
            return cell
        }
        else if type == ITEMTYPE.Gallery.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsCell
            
            if let imageUrl = itemList[indexPath.item][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
           
            cell.lblTitle.text = itemList[indexPath.item][kTitle] as? String
            
            return cell
        }
        else if type == ITEMTYPE.Royal.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePlayerCell", for: indexPath) as! HomePlayerCell
            
            if let imageUrl = itemList[indexPath.item][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            cell.lblName.text = itemList[indexPath.item][kTitle] as? String
            //cell.lblNumber.text = "Episode 4"
            
            cell.contentView.layer.borderWidth = 3
            cell.contentView.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
            cell.contentView.layer.cornerRadius = 15
            
            return cell
        }
        else if type == ITEMTYPE.Podcast.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePodcastCell", for: indexPath) as! HomePodcastCell
            
            if let imageUrl = itemList[indexPath.row][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgPodcast.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            
            if let title = itemList[indexPath.row][kTitle] as? String {
                cell.lblTitle.text = title
            }
            cell.viewCustomPodBg.layer.borderWidth = 3
            cell.viewCustomPodBg.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
            cell.viewCustomPodBg.layer.cornerRadius = 5
            cell.lblOwner.text = ""
            cell.lblDuration.text = ""
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVideoNewCell", for: indexPath) as! HomeVideoNewCell
            
            if let imageUrl = itemList[indexPath.item][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgVideoNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
//            cell.imgVideoNews.layer.borderWidth = 1.5
//            cell.imgVideoNews.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
//            cell.imgVideoNews.layer.cornerRadius = 4
            cell.lblTitle.text = itemList[indexPath.item][kTitle] as? String
            
            cell.btnPlayVideo.isHidden = true
            
            return cell
        }
    }
    
    @objc func btnShareSocial_Clicked(_ sender: UIButton) {
        
        if type == ITEMTYPE.News.rawValue {
            // itemList[indexPath.item][kTitle] as? Stringif let imageUrl = itemList[indexPath.item][kImage] as? String {
        let slug = self.itemList[sender.tag][kSlug] as? String ?? ""
        let url = URL.init(string: "https://www.rajasthanroyals.com/latest-news/\(slug)")!
        let objectsToShare: [Any] = [url]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender
            self.viewController.present(activityViewController, animated: true, completion: nil)

            BaseViewController.sharedInstance.callTMAnalyticsAPI(category: "Page", action: "View", label: "Home: News: \(itemList[sender.tag][kTitle] as? String ?? "")")
        }
        else {
            // itemList[indexPath.item][kTitle] as? Stringif let imageUrl = itemList[indexPath.item][kImage] as? String {
        let slug = self.itemList[sender.tag][kSlug] as? String ?? ""
        let url = URL.init(string: "https://www.rajasthanroyals.com/latest-news/\(slug)")!
        let objectsToShare: [Any] = [url]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender
            self.viewController.present(activityViewController, animated: true, completion: nil)

            BaseViewController.sharedInstance.callTMAnalyticsAPI(category: "Video", action: "Watched", label: "\(itemList[sender.tag][kTitle] as? String ?? "")")
        }
//        else if type == ITEMTYPE.Royal.rawValue {
//            // itemList[indexPath.item][kTitle] as? Stringif let imageUrl = itemList[indexPath.item][kImage] as? String {
//        let slug = self.itemList[sender.tag][kSlug] as? String ?? ""
//        let url = URL.init(string: "https://www.rajasthanroyals.com/latest-news/\(slug)")!
//        let objectsToShare: [Any] = [url]
//        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//        activityViewController.popoverPresentationController?.sourceView = sender
//        self.present(activityViewController, animated: true, completion: nil)
//
//        self.callTMAnalyticsAPI(category: "Royal Tv", action: "Share", label: self.itemList[sender.tag][kTitle] as? String ?? "")
//        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.type == ITEMTYPE.News.rawValue {
            BaseViewController.sharedInstance.callTMAnalyticsAPI(category: "Page", action: "View", label: "Home: News: \(itemList[indexPath.row][kTitle] as? String ?? "")")
            
            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "newsArticleName": itemList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.LATEST_NEWS.rawValue]
            BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
            
            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as! NewsDetailsViewController
            viewController.slug = itemList[indexPath.item][kSlug] as? String ?? ""
            self.viewController.navigationController?.pushViewController(viewController, animated: true)
        }
        else if self.type == ITEMTYPE.Video.rawValue {
            BaseViewController.sharedInstance.callTMAnalyticsAPI(category: "Video", action: "Watched", label: "\(itemList[indexPath.row][kTitle] as? String ?? "")")
            
            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "videoName": itemList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.VIDEOS.rawValue]
            BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
            
            if let type = itemList[indexPath.item][kType] as? String {
                if type == VIDEOTYPE.LOCAL.rawValue {
                    if let video = itemList[indexPath.row][kFiles] as? String {
                        if let videoURL = URL(string: video) {
                            let player = AVPlayer(url: videoURL)
                            let playerViewController = AVPlayerViewController()
                            playerViewController.player = player
                            self.viewController.present(playerViewController, animated: true) {
                                playerViewController.player!.play()
                            }
                        }
                    }
                }
                else {
                    if let videoUrl = itemList[indexPath.item][kFiles] as? String {
                        let array = videoUrl.components(separatedBy: "=")
                        if array.count > 1 {
                            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "YouTubePlayerViewController") as! YouTubePlayerViewController
                            viewController.videoId = array[1]
                            self.viewController.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }
                }
            }
        }
        else if self.type == ITEMTYPE.Gallery.rawValue {
            BaseViewController.sharedInstance.callTMAnalyticsAPI(category: "Gallery", action: "View", label: "\(itemList[indexPath.row][kTitle] as? String ?? "")")
            
            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "galleryName": itemList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.GALLERY_VIEW.rawValue]
            BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
            
            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "GalleryImageViewController") as! GalleryImageViewController
            viewController.slug = itemList[indexPath.item][kSlug] as? String ?? ""
            self.viewController.navigationController?.pushViewController(viewController, animated: true)
        }
        else if self.type == ITEMTYPE.Players.rawValue {
            let playerDetailsVC = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "PlayerDetailsViewController") as! PlayerDetailsViewController
            playerDetailsVC.slug = itemList[indexPath.item][kSlug] as? String ?? ""
            self.viewController.navigationController?.pushViewController(playerDetailsVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.type == ITEMTYPE.News.rawValue {
            return CGSize(width: 300, height: 200)
        }
        else if self.type == ITEMTYPE.Video.rawValue {
            return CGSize(width: 200, height: 150)
        }
        else if self.type == ITEMTYPE.Gallery.rawValue {
            return CGSize(width: 175, height: 175)
        }
        else if self.type == ITEMTYPE.Royal.rawValue {
            return CGSize(width: 160, height: 220)
        }
        else if self.type == ITEMTYPE.Podcast.rawValue {
            return CGSize(width: SCREEN_WIDTH - 10, height: 100)
        }
        else {
            return CGSize(width: 180, height: 150)
        }
        
    }
}

class HomeItemPodcastCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak var podcastCV: UICollectionView!
    
    var viewController: UIViewController!
    var type = ""
    var itemList = [[String: AnyObject]]()
    
    func setCollectionViewData(type: String, itemList: [[String: AnyObject]]) {
        self.type = type
        self.itemList = itemList
        
        if let layout = self.podcastCV.collectionViewLayout as? UPCarouselFlowLayout {
            layout.itemSize = CGSize(width: SCREEN_WIDTH - 20, height: self.podcastCV.frame.size.height)
            layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 5)
        }
        
        self.podcastCV.dataSource = self
        self.podcastCV.delegate = self
        self.podcastCV.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePodcastCell", for: indexPath) as! HomePodcastCell
        
        if let imageUrl = itemList[indexPath.row][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgPodcast.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let title = itemList[indexPath.row][kTitle] as? String {
            cell.lblTitle.text = title
        }
        cell.viewCustomPodBg.layer.borderWidth = 3
        cell.viewCustomPodBg.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
        cell.viewCustomPodBg.layer.cornerRadius = 5
        
        cell.lblOwner.text = ""
        cell.lblDuration.text = ""
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        BaseViewController.sharedInstance.callTMAnalyticsAPI(category: "Podcast", action: "Listen", label: "\(itemList[indexPath.row][kTitle] as? String ?? "")")
        
        let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "podcastTitle": itemList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.PODCAST.rawValue]
        BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
        
        BaseViewController.sharedInstance.appDelegate.podcastIndex = indexPath.item
        let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "NewPodcastPlayViewController") as! NewPodcastPlayViewController
        self.viewController.navigationController?.pushViewController(viewController, animated: true)
    }
}

class HomeBannerCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var bannerCV: UICollectionView!
    @IBOutlet weak var pageaControl: UIPageControl!
    
    @IBOutlet weak var pageaControlTopConstraint: NSLayoutConstraint!
    
    var viewController: UIViewController!
    var type = ""
    var itemList = [[String: AnyObject]]()
    
    func setCollectionViewData(type: String, itemList: [[String: AnyObject]]) {
        self.type = type
        self.itemList = itemList
        self.pageaControl.numberOfPages = self.itemList.count
        
        if let layout = bannerCV.collectionViewLayout as? UPCarouselFlowLayout {
            layout.itemSize = CGSize(width: SCREEN_WIDTH - 20, height: bannerCV.frame.size.height)
            layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 5)
        }
        
        self.bannerCV.dataSource = self
        self.bannerCV.delegate = self
        self.bannerCV.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCell", for: indexPath) as! BannerCell
        
        cell.imgPlay.isHidden = true
        
        if let imageUrl = itemList[indexPath.row][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgBanner.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if itemList[indexPath.row][kType] as? String == VIDEOTYPE.LOCAL.rawValue || itemList[indexPath.row][kType] as? String == VIDEOTYPE.OTHER.rawValue {
            cell.imgPlay.isHidden = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        BaseViewController.sharedInstance.callTMAnalyticsAPI(category: "HomeBanner", action: "Clicked", label: itemList[indexPath.row][kTitle] as? String ?? "")
        
        if itemList[indexPath.row][kType] as? String == "Empty" {
            if let redirectScreen = itemList[indexPath.row][kRedirectScreenios] as? String, !redirectScreen.isEmpty {
                do {
                    let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                    let range = NSRange(redirectScreen.startIndex..<redirectScreen.endIndex, in: redirectScreen)
                    if let _ = detector.firstMatch(in: redirectScreen, options: [], range: range) {
                        self.viewController.openUrlInSafariViewController(urlString: redirectScreen)
                    }
                    else {
                        if let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: redirectScreen) as? UIViewController {
                            if let buttonRedirect = itemList[indexPath.row][kButtonRedirectios] as? String {
                                if buttonRedirect == REDIRECTTYPE.Tabbar.rawValue {
                                    NotificationCenter.default.post(name: NSNotification.Name("SelectSpacificTab"), object: nil, userInfo: [kTabbarIndexios: itemList[indexPath.row][kTabbarIndexios] as? String ?? ""])
                                }
                                else if buttonRedirect == REDIRECTTYPE.Menu.rawValue {
                                    self.viewController.slideMenuController()?.changeMainViewController(viewController, close: true)
                                }
                                else {
                                    self.viewController.navigationController?.pushViewController(viewController, animated: true)
                                }
                            }
                        }
                    }
                }
                catch {
                }
            }
        }
        else if itemList[indexPath.row][kType] as? String == VIDEOTYPE.LOCAL.rawValue {
            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "videoName": itemList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.VIDEOS.rawValue]
            BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
            
            if let video = itemList[indexPath.row][kFiles] as? String {
                if let videoURL = URL(string: video) {
                    let player = AVPlayer(url: videoURL)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.viewController.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            }
        }
        else if itemList[indexPath.row][kType] as? String == VIDEOTYPE.OTHER.rawValue {
            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "videoName": itemList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.VIDEOS.rawValue]
            BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
            
            if let videoUrl = itemList[indexPath.row][kFiles] as? String {
                let array = videoUrl.components(separatedBy: "=")
                if array.count > 1 {
                    let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "YouTubePlayerViewController") as! YouTubePlayerViewController
                    viewController.videoId = array[1]
                    self.viewController.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
        else if itemList[indexPath.row][kType] as? String == ITEMTYPE.News.rawValue {
            if let redirectScreen = itemList[indexPath.row][kRedirectScreenios] as? String, !redirectScreen.isEmpty {
                let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "newsArticleName": redirectScreen, "eventType": POINTSEVENTTYPE.LATEST_NEWS.rawValue]
                BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
                
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as! NewsDetailsViewController
                viewController.slug = redirectScreen
                self.viewController.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        else if itemList[indexPath.row][kType] as? String == "Gallery" {
            if let redirectScreen = itemList[indexPath.row][kRedirectScreenios] as? String, !redirectScreen.isEmpty {
                let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "galleryName": redirectScreen, "eventType": POINTSEVENTTYPE.GALLERY_VIEW.rawValue]
                BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
                
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "GalleryImageViewController") as! GalleryImageViewController
                viewController.slug = redirectScreen
                self.viewController.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.bannerCV.contentOffset
        visibleRect.size = self.bannerCV.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.bannerCV.indexPathForItem(at: visiblePoint) else { return }
        self.pageaControl.currentPage = indexPath.item
    }
}

class BannerCell: UICollectionViewCell {
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var imgPlay: UIImageView!
}

class HomeMatchCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblDaysValue: UILabel!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblHoursValue: UILabel!
    @IBOutlet weak var lblMinutes: UILabel!
    @IBOutlet weak var lblMinutesValue: UILabel!
    @IBOutlet weak var lblSeconds: UILabel!
    @IBOutlet weak var lblSecondsValue: UILabel!
    
    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnLive: CustomButton!
    @IBOutlet weak var btnUpcoming: CustomButton!
    @IBOutlet weak var btnPast: CustomButton!
    
    @IBOutlet weak var matchCollectionView: UICollectionView!
    @IBOutlet weak var lblMatchEmptyMessage: UILabel!
    
    @IBOutlet weak var lblDaysTopConstraint: NSLayoutConstraint!
    
    var viewController: UIViewController!
    var subType = ""
    var nextMatchDate: Date!
    var timer: Timer!
    var matcheStatus = MATCHSTATUS.LIVE.rawValue
    var currentPage = 1
    var nextPage = kN
    var matchList = [[String: AnyObject]]()
    
    func setCollectionViewData(subType: String) {
        self.subType = subType
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        
        if let layout = self.matchCollectionView.collectionViewLayout as? UPCarouselFlowLayout {
            layout.itemSize = CGSize(width: SCREEN_WIDTH - 20, height: self.matchCollectionView.frame.size.height)
            layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 5)
        }
        
        self.matchCollectionView.dataSource = self
        self.matchCollectionView.delegate = self
        
        self.lblDays.text = BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "Home_lbl_Days")
        self.lblHours.text = BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "Home_lbl_Hours")
        self.lblMinutes.text = BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "Home_lbl_Minutes")
        self.lblSeconds.text = BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "Home_lbl_Seconds")
        
        self.btnLive.setTitle(BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "Home_btn_Live"), for: .normal)
        self.btnUpcoming.setTitle(BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "Home_btn_Upcoming"), for: .normal)
        self.btnPast.setTitle(BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "Home_btn_Past"), for: .normal)
        
        self.btnLive.addTarget(self, action: #selector(btnLive_Clicked(_:)), for: .touchUpInside)
        self.btnUpcoming.addTarget(self, action: #selector(btnUpcoming_Clicked(_:)), for: .touchUpInside)
        self.btnPast.addTarget(self, action: #selector(btnPast_Clicked(_:)), for: .touchUpInside)
        
        if self.subType == MATCHSTATUS.UPCOMING.rawValue {
            self.lblDaysTopConstraint.constant = -100
            self.timerView.isHidden = true
            self.bgViewForButtons.isHidden = true
            
            self.matcheStatus = MATCHSTATUS.UPCOMING.rawValue
            self.getMatchList(isShowHud: false)
        }
        else {
            self.btnLive_Clicked(self.btnLive)
        }
    }
    
    //Start Timer
    func startTimer() {
        if self.timer == nil {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimeValue), userInfo: nil, repeats: true)
        }
    }
    
    //Stop Timer
    func stopTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    //Update Time
    @objc func updateTimeValue() {
        if self.nextMatchDate != nil {
            let currentDate = Date().convertToSpacificTimeZone(timeZone: "Asia/Kolkata")
            let strTimer = self.nextMatchDate.offset(from: currentDate)
                
            if !strTimer.isEmpty {
                let strDay: String = "\((Int(strTimer)! % 31536000) / 86400)"
                let strHour: String = "\((Int(strTimer)! % 86400) / 3600)"
                let strMin: String = "\((Int(strTimer)! % 3600) / 60)"
                let strSec: String = "\(Int(strTimer)! % 60)"
                
                self.lblDaysValue.text = strDay
                self.lblHoursValue.text = strHour
                self.lblMinutesValue.text = strMin
                self.lblSecondsValue.text = strSec
            }
            else {
                self.lblDaysValue.text = "-"
                self.lblHoursValue.text = "-"
                self.lblMinutesValue.text = "-"
                self.lblSecondsValue.text = "-"
            }
        }
    }
    
    @objc func btnLive_Clicked(_ sender: UIButton) {
        self.btnLive.backgroundColor = SelectedButtonBGColor
        self.btnUpcoming.backgroundColor = DeSelectedButtonBGColor
        self.btnPast.backgroundColor = DeSelectedButtonBGColor
        self.btnLive.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnUpcoming.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnPast.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.matcheStatus = MATCHSTATUS.LIVE.rawValue
        self.currentPage = 1
        self.nextPage = kN
        self.getMatchList(isShowHud: true)
    }
    
    @objc func btnUpcoming_Clicked(_ sender: UIButton) {
        self.btnLive.backgroundColor = DeSelectedButtonBGColor
        self.btnUpcoming.backgroundColor = SelectedButtonBGColor
        self.btnPast.backgroundColor = DeSelectedButtonBGColor
        self.btnLive.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnUpcoming.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnPast.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.matcheStatus = MATCHSTATUS.UPCOMING.rawValue
        self.currentPage = 1
        self.nextPage = kN
        self.getMatchList(isShowHud: true)
    }
    
    @objc func btnPast_Clicked(_ sender: UIButton) {
        self.btnLive.backgroundColor = DeSelectedButtonBGColor
        self.btnUpcoming.backgroundColor = DeSelectedButtonBGColor
        self.btnPast.backgroundColor = SelectedButtonBGColor
        self.btnLive.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnUpcoming.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnPast.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.matcheStatus = MATCHSTATUS.PAST.rawValue
        self.currentPage = 1
        self.nextPage = kN
        self.getMatchList(isShowHud: true)
    }
    
    func getMatchList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kMatchStatus: matcheStatus, kPage: self.currentPage, kPageSize: PAGESIZE] as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETMATCHLIST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.matchList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }

                    if let matchList = responseData[kData] as? [[String : AnyObject]] {
                        for match in matchList {
                            self.matchList.append(match)
                        }
                    }
                    
                    if self.matchList.count > 0 {
                        self.lblMatchEmptyMessage.isHidden = true
                    }
                    else {
                        self.lblMatchEmptyMessage.isHidden = false
                        self.lblMatchEmptyMessage.text = responseData[kMessage] as? String ?? ""
                    }
                    
                    if let dateTime = responseData[kUpcomingMatch] as? String {
                        if dateTime != "" {
                            if let date = dateTime.toDate(dateFormat: "yyyy-MM-dd HH:mm") {
                                self.nextMatchDate = date.toLocalTime()
                            }
                            
                            self.startTimer()
                        }
                        else {
                            self.stopTimer()
                            
                            self.lblDaysValue.text = "-"
                            self.lblHoursValue.text = "-"
                            self.lblMinutesValue.text = "-"
                            self.lblSecondsValue.text = "-"
                        }
                    }
                    
                    self.matchCollectionView.contentOffset.x = 0
                    self.matchCollectionView.reloadData()
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return matchList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == matchList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getMatchList(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeMatchesCell", for: indexPath) as! HomeMatchesCell
        
        if let imageUrl = matchList[indexPath.item][kFirstImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgViewTeamLeft.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let imageUrl = matchList[indexPath.item][kSecondImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgViewTeamRight.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let location = matchList[indexPath.item][kLocation] as? String {
            cell.lblAddress.text = location
        }
        
        if self.matcheStatus == MATCHSTATUS.PAST.rawValue {
            cell.lblDateTime.text = matchList[indexPath.item][kResult] as? String ?? ""
            
            if let isWinner = matchList[indexPath.item][kResultId] as? String {
                if isWinner == "1" {
                    cell.imgFirstWinner.isHidden = false
                    cell.imgSecondWinner.isHidden = true
                }
                else if isWinner == "3" {
                    cell.imgFirstWinner.isHidden = true
                    cell.imgSecondWinner.isHidden = false
                }
                else {
                    cell.imgFirstWinner.isHidden = true
                    cell.imgSecondWinner.isHidden = true
                }
            }
            else {
                cell.imgFirstWinner.isHidden = true
                cell.imgSecondWinner.isHidden = true
            }
        }
        else {
            cell.imgFirstWinner.isHidden = true
            cell.imgSecondWinner.isHidden = true
            
            if var strDateTime = matchList[indexPath.item][kDateTime] as? String {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
                
                if let dateTime = dateFormatter.date(from: strDateTime) {
                    dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
                    strDateTime = dateFormatter.string(from: dateTime)
                    cell.lblDateTime.text = strDateTime
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.matcheStatus != MATCHSTATUS.UPCOMING.rawValue {
            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "LiveScoreViewController") as! LiveScoreViewController
            viewController.matchDetails = self.matchList[indexPath.item]
            viewController.isFromLive = self.matcheStatus == MATCHSTATUS.LIVE.rawValue ? true : false
            self.viewController.navigationController?.pushViewController(viewController, animated: true)
        }
        else {
            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "AllFixtureListViewController") as! AllFixtureListViewController
            self.viewController.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

class HomeStoryCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, StoryViewDelegate {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak var storyCV: UICollectionView!
    
    var viewController: UIViewController!
    var type = ""
    var isCreateStory = ""
    var itemList = [[String: AnyObject]]()
    
    func setCollectionViewData(type: String, itemList: [[String: AnyObject]]) {
        self.type = type
        self.itemList = itemList
        BaseViewController.sharedInstance.appDelegate.storiesList = itemList
        
        self.storyCV.dataSource = self
        self.storyCV.delegate = self
        self.storyCV.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isCreateStory == kYes {
            return itemList.count + 1
        }
        return itemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isCreateStory == kYes {
            if indexPath.item == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyStoryCell", for: indexPath) as! MyStoryCell
                
                if let image = BaseViewController.sharedInstance.appDelegate.userDetails[kImage] as? String {
                    if let url = URL(string: image) {
                        cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                return cell
            }
            else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoryImageCell", for: indexPath) as! StoryImageCell
                
                if let imageUrl = itemList[indexPath.row - 1][kImage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                if let stories = itemList[indexPath.row - 1]["is_story"] as? [[String: AnyObject]], stories.count > 0 {
                    if stories[0]["type"] as? String == "image" {
                        if let imageUrl = stories[0]["files"] as? String {
                            if let url = URL(string: imageUrl) {
                                cell.imgStory.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                            }
                        }
                    }
                    else {
                        if let imageUrl = stories[0]["thumbnail"] as? String {
                            if let url = URL(string: imageUrl) {
                                cell.imgStory.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                            }
                        }
                    }
                }
                
                return cell
            }
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoryImageCell", for: indexPath) as! StoryImageCell
            
            if let imageUrl = itemList[indexPath.row][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            
            if let stories = itemList[indexPath.row]["is_story"] as? [[String: AnyObject]], stories.count > 0 {
                if stories[0]["type"] as? String == "image" {
                    if let imageUrl = stories[0]["files"] as? String {
                        if let url = URL(string: imageUrl) {
                            cell.imgStory.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                        }
                    }
                }
                else {
                    if let imageUrl = stories[0]["thumbnail"] as? String {
                        if let url = URL(string: imageUrl) {
                            cell.imgStory.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                        }
                    }
                }
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isCreateStory == kYes {
            if indexPath.item == 0 {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                viewController.delegate = self
                let navigationViewController = UINavigationController(rootViewController: viewController)
                navigationViewController.isNavigationBarHidden = true
                navigationViewController.modalPresentationStyle = .overFullScreen
                self.viewController.present(navigationViewController, animated: true, completion: nil)
            }
            else {
                if let storyList = BaseViewController.sharedInstance.appDelegate.storiesList[indexPath.row - 1]["is_story"] as? [[String: AnyObject]] {
                    BaseViewController.sharedInstance.appDelegate.storyList = storyList
                    let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "StoryViewController") as! StoryViewController
                    viewController.delegate = self
                    viewController.userDetails = BaseViewController.sharedInstance.appDelegate.storiesList[indexPath.row - 1]
                    viewController.modalPresentationStyle = .fullScreen
                    self.viewController.present(viewController, animated: true, completion: nil)
                }
            }
        }
        else {
            if let storyList = BaseViewController.sharedInstance.appDelegate.storiesList[indexPath.row]["is_story"] as? [[String: AnyObject]] {
                BaseViewController.sharedInstance.appDelegate.storyList = storyList
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "StoryViewController") as! StoryViewController
                viewController.delegate = self
                viewController.userDetails = BaseViewController.sharedInstance.appDelegate.storiesList[indexPath.row]
                viewController.modalPresentationStyle = .fullScreen
                self.viewController.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
    func storyView(userId: Int, stories: [[String: AnyObject]]) {
        let array = BaseViewController.sharedInstance.appDelegate.storiesList as Array
        let matchIndex = array.index {
            if $0["appuser_id"] as? Int == userId {
                return true
            }
            return false
        }
        
        if matchIndex != nil {
            if BaseViewController.sharedInstance.appDelegate.storiesList.count > matchIndex! {
                BaseViewController.sharedInstance.appDelegate.storiesList[matchIndex!]["is_story"] = stories as AnyObject
                if stories.count == 0 {
                    BaseViewController.sharedInstance.appDelegate.storiesList.remove(at: matchIndex!)
                }
            }
        }
    }
}

class MyStoryCell : UICollectionViewCell {
    @IBOutlet weak var imgUser: CustomImageView!
    @IBOutlet weak var lblCreateStory: UILabel!
}

class StoryImageCell : UICollectionViewCell {
    @IBOutlet weak var imgStory: CustomImageView!
    @IBOutlet weak var imgUser: CustomImageView!
}

extension HomeViewController {
    func appSetting() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: false, URL: baseURLWithoutAuth, apiName: APINAME.APPSETTING.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                        let editor = BatchUser.editor()
                        editor.setIdentifier(email)
                        editor.save()
                    }
                    
                    if let generalSettings = responseData[kData] as? [String : AnyObject] {
                        BaseViewController.sharedInstance.appDelegate.generalSettings = generalSettings
                    }
                    
                    if BaseViewController.sharedInstance.appDelegate.generalSettings[kIsBirthdate] as? String == kYes {
                        if BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneNumber] as? String == "" {
                            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "UpdateMobileNumberViewController") as! UpdateMobileNumberViewController
                            viewController.delegate = self
                            viewController.modalPresentationStyle = .overFullScreen
                            self.present(viewController, animated: true, completion: nil)
                        }
                        else if BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneVerify] as? String == kNo {
                            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            viewController.modalPresentationStyle = .overFullScreen
                            self.present(viewController, animated: true, completion: nil)
                        }
                        else if BaseViewController.sharedInstance.appDelegate.userDetails["email_verify"] as? String == kNo {
                            if !BaseViewController.sharedInstance.appDelegate.isEmailVerifyFirstTime {
                                BaseViewController.sharedInstance.appDelegate.isEmailVerifyFirstTime = true
                                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "VerifyEmailViewController") as! VerifyEmailViewController
                                viewController.modalPresentationStyle = .overFullScreen
                                self.present(viewController, animated: true, completion: nil)
                            }
                        }
                        else if BaseViewController.sharedInstance.appDelegate.userDetails["is_rating"] as? String == kNo {
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                                if let createdAt = BaseViewController.sharedInstance.appDelegate.userDetails["created_at"] as? String {
                                    if let createdAtDate = self.convertStringToDateNew(format: "yyyy-MM-dd HH:mm:ss", strDate: createdAt) {
                                        let days = Date().days(from: createdAtDate)
                                        if days > 5 {
                                            let viewController = self.newStoryBoard.instantiateViewController(withIdentifier: "RatingPopupViewController") as! RatingPopupViewController
                                            viewController.modalPresentationStyle = .overFullScreen
                                            viewController.delegate = self
                                            self.present(viewController, animated: true, completion: nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    @objc func getHomeScreenData(isShowHud: Bool = false) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "is_banner_array": "Yes"]
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.GETHOMESCREENNEW.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.homeList = data
                        self.tblHome.reloadData()
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                            self.tblHome.reloadData()
                        }
                        
                        if !BaseViewController.sharedInstance.appDelegate.isCheckVersionFirstTimeCall {
                            BaseViewController.sharedInstance.appDelegate.isCheckVersionFirstTimeCall = true
                            self.checkVersion()
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func appRatingAPI(rating: Double) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "Rating[rating]": rating] as [String : Any]
            APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.RATING.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    BaseViewController.sharedInstance.appDelegate.userDetails["is_rating"] = kYes as AnyObject
                }
            }
        }
    }
}

extension HomeViewController: UpdateMobileNumberDelegate {
    func updateMobileNumber() {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
        let navigationViewController = UINavigationController(rootViewController: viewController)
        navigationViewController.isNavigationBarHidden = true
        navigationViewController.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(navigationViewController, animated: true, completion: nil)
    }
}

extension HomeViewController: RatingDelegate {
    func submitRating(rating: Double) {
        let appID = "1460354209"
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(appID)?action=write-review") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        self.appRatingAPI(rating: rating)
    }
}

class HomeMatchesCell: UICollectionViewCell {
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var imgViewTeamLeft: UIImageView!
    @IBOutlet weak var imgViewTeamRight: UIImageView!
    @IBOutlet weak var imgFirstWinner: UIImageView!
    @IBOutlet weak var imgSecondWinner: UIImageView!
}

class HomeVideoNewCell: UICollectionViewCell {
    @IBOutlet weak var imgVideoNewsBg: UIImageView!
    @IBOutlet weak var imgVideoNews: CustomImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnShareVideo: UIButton!
    
}

class HomePlayerCell: UICollectionViewCell {
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblName: UILabel!
}

class HomePodcastCell: UICollectionViewCell {
    @IBOutlet weak var imgPodcast: CustomImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOwner: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var viewCustomPodBg: UIView!
    
}



class HomePlayerCustomCell: UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var imgvPlayer: UIImageView!
    
    
    var viewController: UIViewController!
    var type = ""
    var itemList = [[String: AnyObject]]()
    
    func setCollectionViewData(type: String, itemList: [[String: AnyObject]]) {
        self.type = type
        self.itemList = itemList
        
       
    }
    
   
}

class HomeGalleryCustomCell: UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var imgvFirst: UIImageView!
    @IBOutlet weak var imgvSecond: UIImageView!
    @IBOutlet weak var imgvThird: UIImageView!
    @IBOutlet weak var imgvFour: UIImageView!
    @IBOutlet weak var imgvFive: UIImageView!
    @IBOutlet weak var lblImagesCount: UILabel!
    
    
    var viewController: UIViewController!
    var type = ""
    var itemList = [[String: AnyObject]]()
    
    func setCollectionViewData(type: String, itemList: [[String: AnyObject]]) {
        self.type = type
        self.itemList = itemList
        
       
    }
    
   
}


