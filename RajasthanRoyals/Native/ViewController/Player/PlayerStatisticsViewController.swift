//
//  ReferFriendViewController.swift
//  CV Builder
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 AmitMacBook. All rights reserved.
//

import UIKit
import RPCircularProgress

enum ROLE : String {
    case BATSMAN = "Batsman"
    case BOWLER = "Bowler"
    case ALLROUNDER = "AllRounder"
    case WICKETKEEPER = "WicketKeeper"
}

class PlayerStatisticsViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnBatting: CustomButton!
    @IBOutlet weak var btnBowling: CustomButton!
    
    @IBOutlet weak var firstCircularProgressView: RPCircularProgress!
    @IBOutlet weak var lblFirstTitle: UILabel!
    @IBOutlet weak var lblFirstText: UILabel!
    
    @IBOutlet weak var secondCircularProgressView: RPCircularProgress!
    @IBOutlet weak var lblSecondTitle: UILabel!
    @IBOutlet weak var lblSecondText: UILabel!
    
    @IBOutlet weak var thirdCircularProgressView: RPCircularProgress!
    @IBOutlet weak var lblThirdTitle: UILabel!
    @IBOutlet weak var lblThirdText: UILabel!
    
    @IBOutlet weak var fourthCircularProgressView: RPCircularProgress!
    @IBOutlet weak var lblFourthTitle: UILabel!
    @IBOutlet weak var lblFourthText: UILabel!
    
    @IBOutlet weak var fifthCircularProgressView: RPCircularProgress!
    @IBOutlet weak var lblFifthTitle: UILabel!
    @IBOutlet weak var lblFifthText: UILabel!
    
//    @IBOutlet weak var sixCircularProgressView: RPCircularProgress!
//    @IBOutlet weak var lblSixTitle: UILabel!
//    @IBOutlet weak var lblSixText: UILabel!
    
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var lblBattingStyleTitle: UILabel!
    @IBOutlet weak var lblBattingStyleText: UILabel!
    @IBOutlet weak var lblBowlingStyleTitle: UILabel!
    @IBOutlet weak var lblBowlingStyleText: UILabel!
    
    var mainRole = ""
    var playerDetails = [String: AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        self.btnBatting_Clicked(sender: self.btnBatting)
        
        self.addFirebaseCustomeEvent(eventName: "RRPlayerStatisticsScreen")
        
        let playerName = "\(playerDetails[kFirstName] as? String ?? "") \(playerDetails[kLastName] as? String ?? "")"
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "PlayersDetails_btn_Statistics")
        self.btnBatting.setTitle(self.getLocalizeTextForKey(keyName: "PlayersStatistics_btn_Batting"), for: .normal)
        self.btnBowling.setTitle(self.getLocalizeTextForKey(keyName: "PlayersStatistics_btn_Bowling"), for: .normal)
        self.lblBattingStyleTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_BattingStyle")
        self.lblBowlingStyleTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_BowlingStyle")
        
        if let mainRole = playerDetails["main_role"] as? String {
            self.mainRole = mainRole
            if self.mainRole == ROLE.WICKETKEEPER.rawValue {
                self.btnBowling.setTitle(self.getLocalizeTextForKey(keyName: "PlayersStatistics_btn_WicketKeeper"), for: .normal)
            }
        }
        
        self.setPlayerDetails()
        
        self.setProgressAnimation(progress: firstCircularProgressView)
        self.setProgressAnimation(progress: secondCircularProgressView)
        self.setProgressAnimation(progress: thirdCircularProgressView)
        self.setProgressAnimation(progress: fourthCircularProgressView)
        self.setProgressAnimation(progress: fifthCircularProgressView)
//        self.setProgressAnimation(progress: sixCircularProgressView)
    }
    
    func setProgressAnimation(progress : RPCircularProgress) {
        progress.innerTintColor = .clear
        progress.progressTintColor = UIColor().alertButtonColor
        progress.trackTintColor = .white
        progress.thicknessRatio = 0.1
        progress.indeterminateDuration = 5.0
        progress.indeterminateProgress = 0.5
        progress.enableIndeterminate()
    }
    
    //Set Details
    func setPlayerDetails() {
        if let imageUrl = playerDetails[kHalfTransparentImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "playerplaceholder"))
            }
        }
        
        if let fullName = playerDetails[kFullName] as? String {
            self.lblPlayerName.text = fullName
        }
        
        if let battingStyle = playerDetails["batting_style"] as? String {
            self.lblBattingStyleText.text = battingStyle
        }
        
        if let bowlingStyle = playerDetails["bowling_style"] as? String {
            self.lblBowlingStyleText.text = bowlingStyle
        }
        
        self.setBatsmanDetails()
    }
    
    func setBatsmanDetails() {
        self.thirdCircularProgressView.isHidden = false
        self.lblThirdTitle.isHidden = false
        self.lblThirdText.isHidden = false
        
        self.fourthCircularProgressView.isHidden = false
        self.lblFourthTitle.isHidden = false
        self.lblFourthText.isHidden = false
        
        self.lblFirstTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Matches")
        self.lblSecondTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Runs")
        self.lblThirdTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_HS")
        self.lblFourthTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_50s100s")
        self.lblFifthTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_SR")
//        self.lblSixTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Avg")
        
        if let matches = playerDetails["matches"] as? Int {
            self.lblFirstText.text = "\(matches)".englishToHindiDigits
        }
        
        if let totalRuns = playerDetails["total_runs"] as? Int {
            self.lblSecondText.text = "\(totalRuns)".englishToHindiDigits
        }
        
        if let highestScore = playerDetails["highest_score"] as? Int {
            self.lblThirdText.text = "\(highestScore)".englishToHindiDigits
        }
        
        if let halfCentury = playerDetails["half_century"] as? Int {
            if let century = playerDetails["century"] as? Int {
                self.lblFourthText.text = "\(halfCentury)/\(century)".englishToHindiDigits
            }
        }
        
        if let strikeRate = playerDetails["strike_rate"] as? Double {
            self.lblFifthText.text = "\(strikeRate)".englishToHindiDigits
        }
        
//        if let avgBatsman = playerDetails["avg_batsman"] as? String {
//            self.lblSixText.text = avgBatsman == "" ? "-" : avgBatsman
//        }
    }
    
    func setBowlerDetails() {
        self.thirdCircularProgressView.isHidden = false
        self.lblThirdTitle.isHidden = false
        self.lblThirdText.isHidden = false
        
        self.fourthCircularProgressView.isHidden = false
        self.lblFourthTitle.isHidden = false
        self.lblFourthText.isHidden = false
        
        self.lblFirstTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Matches")
        self.lblSecondTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Wickets")
        self.lblThirdTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_BBF")
        self.lblFourthTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Eco")
        self.lblFifthTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_4W")
//        self.lblSixTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Avg")
        
        if let matches = playerDetails["matches"] as? Int {
            self.lblFirstText.text = "\(matches)".englishToHindiDigits
        }
        
        if let wickets = playerDetails["wickets"] as? Int {
            self.lblSecondText.text = "\(wickets)".englishToHindiDigits
        }
        
        if let bestBowlingFigures = playerDetails["best_bowling_figures"] as? String {
            self.lblThirdText.text = bestBowlingFigures == "" ? "-" : bestBowlingFigures.englishToHindiDigits
        }
        
        if let economy = playerDetails["economy"] as? String {
            self.lblFourthText.text = economy.englishToHindiDigits
        }
        
        if let fourWicketCount = playerDetails["wicket_count_4"] as? String {
            self.lblFifthText.text = fourWicketCount == "" ? "-" : fourWicketCount.englishToHindiDigits
        }
        
//        if let avgBowler = playerDetails["avg_bowler"] as? String {
//            self.lblSixText.text = avgBowler == "" ? "-" : avgBowler
//        }
    }
    
    func setWicketKeeperDetails() {
        self.thirdCircularProgressView.isHidden = true
        self.lblThirdTitle.isHidden = true
        self.lblThirdText.isHidden = true
        
        self.fourthCircularProgressView.isHidden = true
        self.lblFourthTitle.isHidden = true
        self.lblFourthText.isHidden = true
        
        self.lblFirstTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Matches")
        self.lblSecondTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Catches")
        self.lblThirdTitle.text = ""
        self.lblFourthTitle.text = ""
        self.lblFifthTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Stumping")
//        self.lblSixTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Dismissal")
        
        if let matches = playerDetails["matches"] as? Int {
            self.lblFirstText.text = "\(matches)".englishToHindiDigits
        }
        
        if let catches = playerDetails["catches"] as? String {
            self.lblSecondText.text = catches == "" ? "-" : catches.englishToHindiDigits
        }
        
        if let stumping = playerDetails["stumping"] as? String {
            self.lblFifthText.text = stumping == "" ? "-" : stumping.englishToHindiDigits
        }
        
//        if let dismissal = playerDetails["dismissal"] as? String {
//            self.lblSixText.text = dismissal == "" ? "-" : dismissal
//        }
    }
    
    @IBAction func btnBatting_Clicked(sender: UIButton) {
        self.btnBatting.backgroundColor = SelectedButtonBGColor
        self.btnBowling.backgroundColor = DeSelectedButtonBGColor
        self.btnBatting.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnBowling.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.setBatsmanDetails()
    }
    
    @IBAction func btnBowling_Clicked(sender: UIButton) {
        self.btnBatting.backgroundColor = DeSelectedButtonBGColor
        self.btnBowling.backgroundColor = SelectedButtonBGColor
        self.btnBatting.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnBowling.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        if self.mainRole == ROLE.WICKETKEEPER.rawValue {
            self.setWicketKeeperDetails()
        }
        else {
            self.setBowlerDetails()
        }
    }
}
