//
//  PlayerDetailsViewController.swift
//  RR
//
//  Created by Sagar on 29/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class PlayerDetailsViewController: BaseViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPlayerType: UILabel!
    @IBOutlet weak var lblTShirtNumber: UILabel!
    @IBOutlet weak var lblTotalRun: UILabel!
    @IBOutlet weak var lblTotalMatch: UILabel!
    @IBOutlet weak var lblTotalHs: UILabel!
    @IBOutlet weak var imgPlayer: UIImageView!
  
    @IBOutlet weak var statisticsView: UIView!
    @IBOutlet weak var btnStatistics: UIButton!
    @IBOutlet weak var btnProfiles: UIButton!
    
    @IBOutlet weak var btnForiegn: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    
    @IBOutlet weak var mediaView: UIView!

    
    @IBOutlet weak var txtViewDetails: UITextView!
    
    var slug = ""
    var playerDetails = [String: AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "RRPlayerDetailsScreen")
        
        self.btnForiegn.setTitle("", for: .normal)
        self.btnFav.setTitle("", for: .normal)
        self.btnProfiles.layer.borderWidth = 1.5
        self.btnProfiles.layer.borderColor = UIColor.black.cgColor
        self.btnProfiles.layer.cornerRadius = 5
        
        self.btnStatistics.layer.borderWidth = 1.5
        self.btnStatistics.layer.borderColor = UIColor.gray.cgColor
        self.btnStatistics.layer.cornerRadius = 5
        
        self.getPlayerDetails()
    }
    
    //Set Player Details
    func setPlayerDetails() {
        if let firstName = playerDetails[kFirstName] as? String {
            self.lblName.text = firstName
        }
        
        if let lastName = playerDetails[kLastName] as? String {
            self.lblName.text = "\(self.lblName.text!) \(lastName)"
        }
        
        if let role = playerDetails[kRole] as? String {
            self.lblPlayerType.text = role
        }
        
        if let match = playerDetails[kMatches] as? Int {
            self.lblTotalMatch.text = "Match\n\(match)"
        }
        
        if let run = playerDetails[kTotalRuns] as? Int {
            self.lblTotalRun.text = "Run\n\(run)"
        }
        
        if let hscore = playerDetails[kHighestScore] as? Int {
            self.lblTotalHs.text = "HS\n\(hscore)"
        }
        
        if let pnumber = playerDetails[kPlayersNumber] as? Int {
            self.lblTShirtNumber.text = "\(pnumber)"
        }
        
        if let run = playerDetails[kCountry] {
           
            if let playerType = run[kName] as? String{
                if playerType == "India"
                {
                    self.btnForiegn.isHidden = true
                }
                else{
                    self.btnForiegn.isHidden = false
                }
            }
        }
        
        if let imageUrl = playerDetails[kTransparentImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "playerplaceholder"))
            }
        }
        
       
        
        if let description = playerDetails[kDescription] as? String {
            self.txtViewDetails.attributedText = description.html2Attributed
        }
        
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Players: \(self.lblName.text!)")
    }
    
    @IBAction func btnMedia_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PlayerMediaViewController") as! PlayerMediaViewController
        viewController.playerId = self.playerDetails[kPlayerId] as? Int ?? 0
        viewController.playerName = "\(self.playerDetails[kFirstName] as? String ?? "") \(self.playerDetails[kLastName] as? String ?? "")"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnStatistics_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PlayerStatisticsViewController") as! PlayerStatisticsViewController
        viewController.playerDetails = playerDetails
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension PlayerDetailsViewController {
    func getPlayerDetails() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kSlug: self.slug, "is_html": kYes]
            
            APIManager().apiCall(isShowHud: false, URL: baseURLWithoutAuth, apiName: APINAME.GETPLAYERDETAILS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.playerDetails = data
                        self.setPlayerDetails()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
