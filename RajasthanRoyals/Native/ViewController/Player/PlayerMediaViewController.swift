//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class PlayerMediaViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnVideos: CustomButton!
    @IBOutlet weak var btnPhotos: CustomButton!
    @IBOutlet weak var videoCV: UICollectionView!
    @IBOutlet weak var photoCV: UICollectionView!
    @IBOutlet weak var lblEmpty: UILabel!

    var playerId = 0
    var playerName = ""
    var type = ITEMTYPE.Video.rawValue
    
    var currentPageForVideos = 1
    var nextPageForVideos = kN
    var videoList = [[String: AnyObject]]()
    
    var currentPageForPhotos = 1
    var nextPageForPhotos = kN
    var photoList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        
        self.addFirebaseCustomeEvent(eventName: "PlayerMediaVideoScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "PlayerMedia_lbl_Media")
        self.btnVideos.setTitle(self.getLocalizeTextForKey(keyName: "PlayerMedia_btn_Videos"), for: .normal)
        self.btnPhotos.setTitle(self.getLocalizeTextForKey(keyName: "PlayerMedia_btn_Photos"), for: .normal)
        
        self.setupLayout()
        
        self.btnVideos_Clicked(sender: self.btnVideos)
        self.getVideosList(isShowHud: true)
    }
    
    func setupLayout() {
        let layout = PinterestLayout()
        layout.delegate = self
        layout.cellPadding = 5
        layout.numberOfColumns = 2
        self.photoCV.collectionViewLayout = layout
    }
    
    @IBAction func btnVideos_Clicked(sender: UIButton) {
        self.addFirebaseCustomeEvent(eventName: "PlayerMediaVideoScreen")
        
        self.type = ITEMTYPE.Video.rawValue
        
        self.btnVideos.backgroundColor = SelectedButtonBGColor
        self.btnPhotos.backgroundColor = DeSelectedButtonBGColor
        
        self.btnVideos.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnPhotos.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.videoCV.isHidden = false
        self.photoCV.isHidden = true
        
        if self.videoList.count <= 0 {
            self.currentPageForVideos = 1
            self.nextPageForVideos = kN
            self.getVideosList(isShowHud: true)
        }
        else {
            self.videoCV.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
            self.videoCV.reloadData()
        }
    }
    
    @IBAction func btnPhotos_Clicked(sender: UIButton) {
        self.addFirebaseCustomeEvent(eventName: "PlayerMediaPhotosScreen")
        
        self.type = ITEMTYPE.Gallery.rawValue
        
        self.btnVideos.backgroundColor = DeSelectedButtonBGColor
        self.btnPhotos.backgroundColor = SelectedButtonBGColor
        
        self.btnVideos.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnPhotos.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.videoCV.isHidden = true
        self.photoCV.isHidden = false
        
        if self.photoList.count <= 0 {
            self.currentPageForPhotos = 1
            self.nextPageForPhotos = kN
            self.getPhotoList(isShowHud: true)
        }
        else {
            self.photoCV.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
            self.photoCV.reloadData()
        }
    }
}

extension PlayerMediaViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == videoCV {
            return videoList.count
        }
        else {
            return photoList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == videoCV {
            if indexPath.row == videoList.count - 1 && nextPageForVideos == kY {
                currentPageForVideos = currentPageForVideos + 1
                self.getVideosList(isShowHud: false)
            }
        }
        else {
            if indexPath.row == photoList.count - 1 && nextPageForPhotos == kY {
                currentPageForPhotos = currentPageForPhotos + 1
                self.getPhotoList(isShowHud: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == videoCV {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsCell
            
            if let imageUrl = videoList[indexPath.item][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
            
            if let title = videoList[indexPath.item][kTitle] as? String {
                cell.lblTitle.text = title
            }
            
            cell.imgPlay.isHidden = false
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsCell
            
            if let imageUrl = photoList[indexPath.item][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            
            if let title = photoList[indexPath.item][kTitle] as? String {
                cell.lblTitle.text = title
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == videoCV {
            if let type = videoList[indexPath.row][kType] as? String {
                if type == VIDEOTYPE.LOCAL.rawValue {
                    if let video = videoList[indexPath.row][kVideo] as? String {
                        if let videoURL = URL(string: video) {
                            let player = AVPlayer(url: videoURL)
                            let playerViewController = AVPlayerViewController()
                            playerViewController.player = player
                            self.present(playerViewController, animated: true) {
                                playerViewController.player!.play()
                            }
                        }
                    }
                }
                else {
                    if let videoUrl = videoList[indexPath.row][kVideo] as? String {
                        let array = videoUrl.components(separatedBy: "=")
                        if array.count > 1 {
                            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "YouTubePlayerViewController") as! YouTubePlayerViewController
                            viewController.videoId = array[1]
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }
                }
            }
        }
        else {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
            viewController.imageUrl = photoList[indexPath.row][kImage] as? String ?? ""
            self.present(viewController, animated: true, completion: nil)
        }
    }
}

extension PlayerMediaViewController {
    func getVideosList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPlayerId: self.playerId, kPage: self.currentPageForVideos, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETPLAYERVIDEO.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPageForVideos == 1 {
                        self.videoList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPageForVideos = nextPage
                    }
                    
                    if let videosData = responseData[kData] as? [[String : AnyObject]] {
                        for videos in videosData {
                            self.videoList.append(videos)
                        }
                    }
                    
                    self.videoCV.reloadData()
                    
                    self.lblEmpty.text = responseData[kMessage] as? String ?? ""
                    if self.videoList.count > 0 {
                        self.lblEmpty.isHidden = true
                    }
                    else {
                        self.lblEmpty.isHidden = false
                    }
                }
                else {
                    if self.currentPageForVideos > 1 {
                        self.currentPageForVideos = self.currentPageForVideos - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getPhotoList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPlayerId: self.playerId, kPage: self.currentPageForPhotos, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETPLAYERIMAGE.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPageForPhotos == 1 {
                        self.photoList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPageForPhotos = nextPage
                    }
                    
                    if let videosData = responseData[kData] as? [[String : AnyObject]] {
                        for videos in videosData {
                            self.photoList.append(videos)
                        }
                    }
                    
                    self.photoCV.reloadData()
                    
                    self.lblEmpty.text = responseData[kMessage] as? String ?? ""
                    if self.videoList.count > 0 {
                        self.lblEmpty.isHidden = true
                    }
                    else {
                        self.lblEmpty.isHidden = false
                    }
                }
                else {
                    if self.currentPageForPhotos > 1 {
                        self.currentPageForPhotos = self.currentPageForPhotos - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension PlayerMediaViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 1
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}

extension PlayerMediaViewController: PinterestLayoutDelegate {
    func collectionView(collectionView: UICollectionView, heightForImageAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        return numberArray.randomElement() ?? 220.0
    }
    
    func collectionView(collectionView: UICollectionView, heightForAnnotationAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        return 0.0
    }
}
