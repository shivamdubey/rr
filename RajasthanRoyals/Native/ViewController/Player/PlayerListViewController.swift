//
//  PlayerListViewController.swift
//  RR
//
//  Created by Sagar on 24/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PlayerListViewController: BaseViewController, AVAudioPlayerDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnMenu: CustomButton!
    @IBOutlet weak var CVPlayerList: UICollectionView!
    @IBOutlet weak var pageControl: FlexiblePageControl!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var imgCountry: CustomImageView!
    @IBOutlet weak var imgCaption: CustomImageView!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnInstagram: UIButton!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var btnPodcast: UIButton!
    @IBOutlet weak var btnFavourite: UIButton!
    
    var isFromProfile = false
    var currentPage = 1
    var nextPage = kN
    var playerList = [[String: AnyObject]]()

    var visibleIndex = 0
    var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "RRPlayerScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_RRPlayers")
        
        if self.isFromProfile {
            self.btnBack.isHidden = false
            self.btnMenu.isHidden = true
        }
        else {
            self.btnBack.isHidden = true
            self.btnMenu.isHidden = false
        }
        
//        self.lblFirstName.rotate(degrees: -15)
//        self.lblLastName.rotate(degrees: -15)
        
        self.setupLayout()
        
        self.pageControl.pageIndicatorTintColor = UIColor.white.withAlphaComponent(0.5)
        self.pageControl.currentPageIndicatorTintColor = UIColor.white
        
        self.getPlayerList(isShowHud: true)
    }
    
    fileprivate func setupLayout() {
        if let layout = self.CVPlayerList.collectionViewLayout as? UPCarouselFlowLayout {
            layout.itemSize = CGSize(width: SCREEN_WIDTH, height: self.CVPlayerList.frame.size.height)
            layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: -20)
        }
    }
    
    @IBAction func btnFacebook_Clicked(_ sender: Any) {
        if playerList.count > visibleIndex {
            if let facebookUrl = playerList[visibleIndex][kFacebookUrl] as? String {
                if let url = URL(string: facebookUrl) {
                    UIApplication.shared.open(url, options: [:])
                }
            }
        }
    }
    
    @IBAction func btnInstagram_Clicked(_ sender: Any) {
        if playerList.count > visibleIndex {
            if let facebookUrl = playerList[visibleIndex][kInstagramUrl] as? String {
                if let url = URL(string: facebookUrl) {
                    UIApplication.shared.open(url, options: [:])
                }
            }
        }
    }
    
    @IBAction func btnTwitter_Clicked(_ sender: Any) {
        if playerList.count > visibleIndex {
            if let facebookUrl = playerList[visibleIndex][kTwitterUrl] as? String {
                if let url = URL(string: facebookUrl) {
                    UIApplication.shared.open(url, options: [:])
                }
            }
        }
    }
    
    @IBAction func btnPodcast_Clicked(_ sender: Any) {
        self.btnPodcast.isSelected = !self.btnPodcast.isSelected
        if self.btnPodcast.isSelected {
            self.playAudio()
        }
        else {
            if audioPlayer != nil {
                audioPlayer?.pause()
            }
        }
    }
    
    @IBAction func btnFavourite_Clicked(_ sender: Any) {
        if !self.btnFavourite.isSelected {
            if playerList.count > visibleIndex {
                if let playersId = playerList[visibleIndex]["players_id"] as? Int {
                    self.favouritePlayer(playersId: playersId)
                }
            }
        }
    }
    
    //UIScrollView Delegate Method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.CVPlayerList.contentOffset
        visibleRect.size = self.CVPlayerList.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.CVPlayerList.indexPathForItem(at: visiblePoint) else { return }
        
        self.visibleIndex = indexPath.item
        pageControl.setCurrentPage(at: indexPath.item)
        
        if let playerNumber = playerList[self.visibleIndex][kPlayersNumber] as? Int {
            self.lblNumber.text = "\(playerNumber)".englishToHindiDigits
        }
        
        if let firstName = playerList[self.visibleIndex][kFirstName] as? String {
            self.lblFirstName.text = firstName
        }
        
        if let lastName = playerList[self.visibleIndex][kLastName] as? String {
            self.lblLastName.text = lastName
        }
        
        if let country = playerList[self.visibleIndex][kCountry] as? [String: AnyObject] {
            if let imageUrl = country[kImage] as? String {
                if let url = URL(string: imageUrl) {
                    self.imgCountry.kf.setImage(with: url, placeholder:  #imageLiteral(resourceName: "playerplaceholder"))
                }
            }
        }
        
        if let isCaption = self.playerList[self.visibleIndex][kIsCaptain] as? String {
            self.imgCaption.isHidden = isCaption == kYes ? false : true
        }
        
        self.btnFavourite.isSelected = false
        if let playersId = playerList[self.visibleIndex]["players_id"] as? Int {
            if let favourites = BaseViewController.sharedInstance.appDelegate.userDetails["favourites"] as? [[String: AnyObject]] {
                if favourites.count > 0 {
                    if let favouritePlayersId = favourites[0]["players_id"] as? Int {
                        if playersId == favouritePlayersId {
                            self.btnFavourite.isSelected = true
                        }
                    }
                }
            }
        }
        
        if let audioUrlStr = playerList[visibleIndex][kAudioFile] as? String {
            if audioUrlStr.isEmpty {
                self.btnPodcast.isHidden = true
            }
            else {
                self.btnPodcast.isHidden = false
            }
        }
        
        if audioPlayer != nil {
            audioPlayer?.pause()
        }
        self.btnPodcast.isSelected = false
    }
    
    func playAudio() {
        if audioPlayer != nil {
            audioPlayer?.pause()
        }
        
        if let audioUrlStr = playerList[visibleIndex][kAudioFile] as? String {
            if let audioUrl = URL(string: audioUrlStr) {
                do {
                    if let data = try? Data(contentsOf: audioUrl) {
                        audioPlayer = try AVAudioPlayer(data: data)
                        audioPlayer?.delegate = self
                        audioPlayer?.prepareToPlay()
                        
                        audioPlayer?.addObserver(self, forKeyPath: "playbackBufferEmpty", options: .new, context: nil)
                        audioPlayer?.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: .new, context: nil)
                        audioPlayer?.addObserver(self, forKeyPath: "playbackBufferFull", options: .new, context: nil)
                        
                        let audioSession = AVAudioSession.sharedInstance()
                        do {
                            try audioSession.setCategory(.playback)
                        }
                        catch let sessionError {
                            print(sessionError.localizedDescription)
                        }
                        audioPlayer?.play()
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.btnPodcast.isSelected = false
    }
    
    //Audio Player Observer Method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        print("Call Audio Player Observer...!")
        if object is AVAudioPlayer {
            switch keyPath {
            case "playbackBufferEmpty"?:
                // Show loader
                print("Show loader")
                break
            case "playbackLikelyToKeepUp"?:
                // Hide loader
                print("Hide loader")
                break
            case "playbackBufferFull"?:
                // Hide loader
                print("Hide loader")
                break
            default:
                break
            }
        }
    }
}

extension PlayerListViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playerList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == playerList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getPlayerList(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerListCVCell", for: indexPath) as! PlayerListCVCell
        
        if let imageUrl = playerList[indexPath.item][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "playerplaceholder"))
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let playerDetailsVC = self.storyBoard.instantiateViewController(withIdentifier: "PlayerDetailsViewController") as! PlayerDetailsViewController
        playerDetailsVC.slug = self.playerList[indexPath.row]["slug"] as? String ?? ""
        self.navigationController?.pushViewController(playerDetailsVC, animated: true)
    }
}

class PlayerListCVCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
}

extension PlayerListViewController {
    func getPlayerList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETPLAYERLIST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.playerList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let players = responseData[kData] as? [[String : AnyObject]] {
                        for player in players {
                            self.playerList.append(player)
                        }
                    }
                    
                    self.CVPlayerList.reloadData()
                    
                    self.pageControl.numberOfPages = self.playerList.count
                    
                    if self.playerList.count > 0 && self.currentPage == 1 {
                        if let playerNumber = self.playerList[self.visibleIndex][kPlayersNumber] as? Int {
                            self.lblNumber.text = "\(playerNumber)".englishToHindiDigits
                        }
                        
                        if let firstName = self.playerList[self.visibleIndex][kFirstName] as? String {
                            self.lblFirstName.text = firstName
                        }
                        
                        if let lastName = self.playerList[self.visibleIndex][kLastName] as? String {
                            self.lblLastName.text = lastName
                        }
                        
                        if let country = self.playerList[self.visibleIndex][kCountry] as? [String: AnyObject] {
                            if let imageUrl = country[kImage] as? String {
                                if let url = URL(string: imageUrl) {
                                    self.imgCountry.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                                }
                            }
                        }
                        
                        if let isCaption = self.playerList[self.visibleIndex][kIsCaptain] as? String {
                            self.imgCaption.isHidden = isCaption == kYes ? false : true
                        }
                        
                        self.btnFavourite.isSelected = false
                        if let playersId = self.playerList[self.visibleIndex]["players_id"] as? Int {
                            if let favourites = BaseViewController.sharedInstance.appDelegate.userDetails["favourites"] as? [[String: AnyObject]] {
                                if favourites.count > 0 {
                                    if let favouritePlayersId = favourites[0]["players_id"] as? Int {
                                        if playersId == favouritePlayersId {
                                            self.btnFavourite.isSelected = true
                                        }
                                    }
                                }
                            }
                        }
                        
                        if let audioUrlStr = self.playerList[self.visibleIndex][kAudioFile] as? String {
                            if audioUrlStr.isEmpty {
                                self.btnPodcast.isHidden = true
                            }
                            else {
                                self.btnPodcast.isHidden = false
                            }
                        }
                    }
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func favouritePlayer(playersId: Int) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "players_id": playersId] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.FAVOURITEPLAYER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                    }
                    
                    self.btnFavourite.isSelected = true
                    if self.isFromProfile {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
