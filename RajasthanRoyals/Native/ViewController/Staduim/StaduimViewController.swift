//
//  ReferFriendViewController.swift
//  RR
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 Sagar Nandha. All rights reserved.
//

import UIKit

class StaduimViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblStaduim: UITableView!
    
    var currentPage = 1
    var nextPage = kN
    var staduimList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "VenuesScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Venues")
        
        self.getStaduimList(isShowHud: true)
    }
}

extension StaduimViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return staduimList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if staduimList.count == 2 || staduimList.count == 3 {
            return (tableView.frame.height / CGFloat(staduimList.count)) - 10
        }
        return 220
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == staduimList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getStaduimList(isShowHud: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == staduimList.count - 1 && nextPage == kY {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.clear
            
            let activityIndicator = UIActivityIndicatorView(style: .white)
            activityIndicator.color = UIColor().alertButtonColor
            activityIndicator.center = CGPoint(x: tableView.frame.size.width / 2, y: 20)
            
            cell.addSubview(activityIndicator)
            
            activityIndicator.startAnimating()
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StaduimCell") as! StaduimCell
        
        if let imageUrl = staduimList[indexPath.row][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgStaduim.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if let title = staduimList[indexPath.row][kTitle] as? String {
            cell.lblName.text = title
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "StaduimDetailsViewController") as! StaduimDetailsViewController
        viewController.staduimDetails = staduimList[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

class StaduimCell: UITableViewCell {
    @IBOutlet weak var imgStaduim: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}

extension StaduimViewController {
    func getStaduimList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETSTADIUM.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.staduimList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let players = responseData[kData] as? [[String : AnyObject]] {
                        for player in players {
                            self.staduimList.append(player)
                        }
                    }
                    
                    self.tblStaduim.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
