//
//  ReferFriendViewController.swift
//  RR
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 Sagar Nandha. All rights reserved.
//

import UIKit

class StaduimDetailsViewController: BaseViewController {

    @IBOutlet weak var imgStaduim: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblOpened: UILabel!
    @IBOutlet weak var lblOpenedValue: UILabel!
    @IBOutlet weak var lblCapacity: UILabel!
    @IBOutlet weak var lblCapacityValue: UILabel!
    @IBOutlet weak var lblOwner: UILabel!
    @IBOutlet weak var lblOwnerValue: UILabel!
     
    var staduimDetails = [String: AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "VenueDetailsScreen")
        
        self.lblOpened.text = self.getLocalizeTextForKey(keyName: "StadiumInfo_lbl_Opened")
        self.lblCapacity.text = self.getLocalizeTextForKey(keyName: "StadiumInfo_lbl_Capacity")
        self.lblOwner.text = self.getLocalizeTextForKey(keyName: "StadiumInfo_lbl_Owner")
        
        self.setStaduimDetails()
    }
    
    //Set Staduim Details
    func setStaduimDetails() {
        if let imageUrl = staduimDetails[kImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgStaduim.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if let imageUrl = staduimDetails[kMapImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgLocation.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if let title = staduimDetails[kTitle] as? String {
            self.lblName.text = title
        }
        
        if let address = staduimDetails[kAddress] as? String {
            self.lblAddress.text = address
        }
        
        if let description = staduimDetails[kDescription] as? String {
            self.txtDescription.attributedText = description.html2Attributed
//            self.txtDescription.font = UIFont.init(name: "Futura-Medium", size: 14.0)
        }
        
        if let opened = staduimDetails[kOpened] as? String {
            self.lblOpenedValue.text = opened.englishToHindiDigits
        }
        
        if let capacity = staduimDetails[kCapacity] as? String {
            self.lblCapacityValue.text = capacity.englishToHindiDigits
        }
        
        if let owner = staduimDetails[kOwner] as? String {
            self.lblOwnerValue.text = owner
        }
    }
}
