//
//  NewsVideosViewController.swift
//  RR
//
//  Created by Sagar on 23/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PressViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnClippings: CustomButton!
    @IBOutlet weak var btnArticles: CustomButton!
    @IBOutlet weak var btnOnline: CustomButton!
    @IBOutlet weak var pressCV: UICollectionView!
    
    var type = PRESSTYPE.Articles.rawValue
    var refreshControl: UIRefreshControl!
    
    var currentPage = 1
    var nextPage = kN
    var pressClippingsList = [[String: AnyObject]]()
    
    var currentPage1 = 1
    var nextPage1 = kN
    var pressArticlesList = [[String: AnyObject]]()
    
    var currentPage2 = 1
    var nextPage2 = kN
    var pressOnlineList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "PressArticlesScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Press")
        self.btnArticles.setTitle(self.getLocalizeTextForKey(keyName: "Press_btn_Articles"), for: .normal)
        self.btnClippings.setTitle(self.getLocalizeTextForKey(keyName: "Press_btn_Clippings"), for: .normal)
        self.btnOnline.setTitle(self.getLocalizeTextForKey(keyName: "Press_btn_Online"), for: .normal)
        
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        
        refreshControl = UIRefreshControl.init()
        refreshControl.tintColor = .white
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshTableView(sender:)), for: .valueChanged)
        pressCV.refreshControl = refreshControl
        
        if type == PRESSTYPE.Articles.rawValue {
            self.btnArticles_Clicked(sender: self.btnArticles)
        }
        else if type == PRESSTYPE.Clippings.rawValue {
            self.btnClippings_Clicked(sender: self.btnClippings)
        }
        else {
            self.btnOnline_Clicked(sender: self.btnOnline)
        }
    }
    
    //UIRefreshControl method
    @objc func refreshTableView(sender: AnyObject) {
        if self.type == PRESSTYPE.Clippings.rawValue {
            self.currentPage = 1
            self.nextPage = kN
            self.getPressClippingsList(isShowHud: false)
        }
        else if self.type == PRESSTYPE.Articles.rawValue {
            self.currentPage1 = 1
            self.nextPage1 = kN
            self.getPressArticlesList(isShowHud: false)
        }
        else {
            self.currentPage2 = 1
            self.nextPage2 = kN
            self.getPressOnlineList(isShowHud: false)
        }
    }
    
    @IBAction func btnArticles_Clicked(sender: UIButton) {
        self.addFirebaseCustomeEvent(eventName: "PressArticlesScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Press: Articles")
        
        self.type = PRESSTYPE.Articles.rawValue
        
        self.btnArticles.backgroundColor = SelectedButtonBGColor
        self.btnClippings.backgroundColor = DeSelectedButtonBGColor
        self.btnOnline.backgroundColor = DeSelectedButtonBGColor
        
        self.btnArticles.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnClippings.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnOnline.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        if self.pressArticlesList.count <= 0 {
            self.currentPage1 = 1
            self.nextPage1 = kN
            self.getPressArticlesList(isShowHud: true)
        }
        else {
            self.pressCV.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
            self.pressCV.reloadData()
        }
    }
    
    @IBAction func btnClippings_Clicked(sender: UIButton) {
        self.addFirebaseCustomeEvent(eventName: "PressClippingsScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Press: Clippings")
        
        self.type = PRESSTYPE.Clippings.rawValue
        
        self.btnArticles.backgroundColor = DeSelectedButtonBGColor
        self.btnClippings.backgroundColor = SelectedButtonBGColor
        self.btnOnline.backgroundColor = DeSelectedButtonBGColor
        
        self.btnArticles.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnClippings.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnOnline.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        if self.pressClippingsList.count <= 0 {
            self.currentPage = 1
            self.nextPage = kN
            self.getPressClippingsList(isShowHud: true)
        }
        else {
            self.pressCV.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
            self.pressCV.reloadData()
        }
    }
    
    @IBAction func btnOnline_Clicked(sender: UIButton) {
        self.addFirebaseCustomeEvent(eventName: "PressOnlineScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Press: Online")
        
        self.type = PRESSTYPE.Online.rawValue
        
        self.btnArticles.backgroundColor = DeSelectedButtonBGColor
        self.btnClippings.backgroundColor = DeSelectedButtonBGColor
        self.btnOnline.backgroundColor = SelectedButtonBGColor
        
        self.btnArticles.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnClippings.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnOnline.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        if self.pressOnlineList.count <= 0 {
            self.currentPage2 = 1
            self.nextPage2 = kN
            self.getPressOnlineList(isShowHud: true)
        }
        else {
            self.pressCV.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
            self.pressCV.reloadData()
        }
    }
}

extension PressViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.type == PRESSTYPE.Clippings.rawValue {
            return pressClippingsList.count
        }
        else if self.type == PRESSTYPE.Articles.rawValue {
            return pressArticlesList.count
        }
        return pressOnlineList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.type == PRESSTYPE.Clippings.rawValue {
            if indexPath.item == pressClippingsList.count - 1 && nextPage == kY {
                currentPage = currentPage + 1
                self.getPressClippingsList(isShowHud: false)
            }
        }
        else if self.type == PRESSTYPE.Articles.rawValue {
            if indexPath.item == pressArticlesList.count - 1 && nextPage1 == kY {
                currentPage1 = currentPage1 + 1
                self.getPressArticlesList(isShowHud: false)
            }
        }
        else {
            if indexPath.item == pressOnlineList.count - 1 && nextPage2 == kY {
                currentPage2 = currentPage2 + 1
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.type == PRESSTYPE.Clippings.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsCell
            
            if let imageUrl = pressClippingsList[indexPath.row][kThumbnailImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
            
            if let title = pressClippingsList[indexPath.row][kTitle] as? String {
                cell.lblTitle.text = title
            }
            
            return cell
        }
        else if self.type == PRESSTYPE.Articles.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsCell
            
            if let imageUrl = pressArticlesList[indexPath.row][kThumbnailImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
            
            if let title = pressArticlesList[indexPath.row][kTitle] as? String {
                cell.lblTitle.text = title
            }
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsCell
            
            if let imageUrl = pressOnlineList[indexPath.row][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
            
            if let title = pressOnlineList[indexPath.row][kTitle] as? String {
                cell.lblTitle.text = title
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.type == PRESSTYPE.Clippings.rawValue {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
            viewController.imageUrl = pressClippingsList[indexPath.row][kImage] as? String ?? ""
            self.present(viewController, animated: true, completion: nil)
        }
        else if self.type == PRESSTYPE.Articles.rawValue {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PressDetailsViewController") as! PressDetailsViewController
            viewController.slug = pressArticlesList[indexPath.row][kSlug] as? String ?? ""
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else {
            self.openUrlInSafariViewController(urlString: pressOnlineList[indexPath.row][kUrl] as? String ?? "")
        }
    }
}

extension PressViewController {
    func getPressClippingsList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETPRESSCLIPPINGLIST.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.pressClippingsList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let pressData = responseData[kData] as? [[String : AnyObject]] {
                        for press in pressData {
                            self.pressClippingsList.append(press)
                        }
                    }
                    
                    self.pressCV.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getPressArticlesList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage1, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETPRESSARTICLESLIST.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage1 == 1 {
                        self.pressArticlesList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage1 = nextPage
                    }
                    
                    if let pressData = responseData[kData] as? [[String : AnyObject]] {
                        for press in pressData {
                            self.pressArticlesList.append(press)
                        }
                    }
                    
                    self.pressCV.reloadData()
                }
                else {
                    if self.currentPage1 > 1 {
                        self.currentPage1 = self.currentPage1 - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getPressOnlineList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage2, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETONLINEPRESS.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage2 == 1 {
                        self.pressOnlineList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage2 = nextPage
                    }
                    
                    if let pressData = responseData[kData] as? [[String : AnyObject]] {
                        for press in pressData {
                            self.pressOnlineList.append(press)
                        }
                    }
                    
                    self.pressCV.reloadData()
                }
                else {
                    if self.currentPage2 > 1 {
                        self.currentPage2 = self.currentPage2 - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension PressViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 1
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        
//        if indexPath.item == 0 {
//            return CGSize(width: widthPerItem * 2, height: widthPerItem * 1.25)
//        }
//        return CGSize(width: widthPerItem, height: widthPerItem)
        
        return CGSize(width: widthPerItem, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}
