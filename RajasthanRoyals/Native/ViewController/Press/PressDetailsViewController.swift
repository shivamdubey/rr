//
//  NewsDetailsViewController.swift
//  RR
//
//  Created by Sagar on 29/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit
import Auk
import moa

class PressDetailsViewController: BaseViewController {
    
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UITextView!
    
    var slug = ""
    var pressDetails = [String: AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageScrollView.auk.settings.placeholderImage = #imageLiteral(resourceName: "placeholder")
        self.imageScrollView.auk.settings.contentMode = .scaleAspectFill
        self.imageScrollView.auk.settings.pagingEnabled = true
        self.imageScrollView.auk.settings.pageControl.backgroundColor = .clear
        self.imageScrollView.auk.settings.pageControl.pageIndicatorTintColor = .white
        self.imageScrollView.auk.settings.pageControl.currentPageIndicatorTintColor = UIColor().themeDarkPinkColor.withAlphaComponent(0.8)
        self.imageScrollView.auk.startAutoScroll(delaySeconds: 3.0)
        
        self.addFirebaseCustomeEvent(eventName: "PressArticlesDetailsScreen")
        self.getPressArticleDetails()
    }
    
    //Set Press Details
    func setPressDetails() {
        if let image = pressDetails[kImage] as? String {
            self.imageScrollView.auk.show(url: image)
        }
        
        if let title = pressDetails[kTitle] as? String {
            self.lblTitle.text = title
        }
        
        if let description = pressDetails[kMobileDescription] as? String {
            self.lblDescription.attributedText = description.html2Attributed
        }
    }
}

extension PressDetailsViewController {
    func getPressArticleDetails() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kSlug: self.slug]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETPRESSARTICLESDETAILS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.pressDetails = data
                    }
                    self.setPressDetails()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
