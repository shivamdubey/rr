//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class LoyalityPointsViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgRoyalsCoins: UIImageView!
    @IBOutlet weak var lblYourPoints: UILabel!
    @IBOutlet weak var tblProduct: UITableView!
    
    var totalPoints = 0
    var productList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "RRCoins")
        
        if let imageUrl = BaseViewController.sharedInstance.appDelegate.generalSettings["royals_coins_image"] as? String {
            if let url = URL(string: imageUrl) {
                self.imgRoyalsCoins.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Profile_btn_EarnedPoints")
        
        self.getProductList()
        self.getPoints()
    }
    
    @IBAction func btnInfo_Clicked(_ sender: Any) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "LoyalityPointsFAQViewController") as! LoyalityPointsFAQViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension LoyalityPointsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoyalityProductCell") as! LoyalityProductCell
        
        cell.imgProduct.layer.borderWidth = 3
        cell.imgProduct.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
        cell.imgProduct.layer.cornerRadius = 37.5
        
        if let imageUrl = productList[indexPath.row][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgProduct.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        cell.lblName.text = productList[indexPath.row]["productName"] as? String ?? ""
        
        if let points = productList[indexPath.row]["points"] as? Int {
            cell.lblPoints.text = "\(points) \(self.getLocalizeTextForKey(keyName: "RedeemPoints_lbl_Coins"))"
            
            if self.totalPoints >= points {
                cell.btnRedeem.isUserInteractionEnabled = true
                cell.btnRedeem.backgroundColor = UIColor().alertButtonColor
            }
            else {
                cell.btnRedeem.isUserInteractionEnabled = false
                cell.btnRedeem.backgroundColor = UIColor( red: CGFloat(234/255.0), green: CGFloat(24/255.0), blue: CGFloat(133/255.0), alpha: CGFloat(1.0) )
            }
        }
        
        cell.btnRedeem.setTitle(self.getLocalizeTextForKey(keyName: "RedeemPoints_btn_Redeem"), for: .normal)
        cell.btnRedeem.addTarget(self, action: #selector(btnRedeem_Clicked(_:)), for: .touchUpInside)
        
        cell.lblOutofStock.text = self.getLocalizeTextForKey(keyName: "RedeemPoints_lbl_OutOfStock")
        
        if let ourOfStock = productList[indexPath.row]["outOfStock"] as? Bool {
            if ourOfStock {
                cell.btnRedeem.isHidden = true
                cell.lblOutofStock.isHidden = false
            }
            else {
                cell.btnRedeem.isHidden = false
                cell.lblOutofStock.isHidden = true
            }
        }
        
        cell.customView.layer.borderWidth = 3
        cell.customView.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
        cell.customView.layer.cornerRadius = 10
        
        return cell
    }
    
    @objc func btnRedeem_Clicked(_ sender: AnyObject) {
        let position = sender.convert(CGPoint.zero, to: tblProduct)
        if let indexPath = tblProduct.indexPathForRow(at: position) {
            if let itemId = self.productList[indexPath.row]["id"] as? Int {
                let viewController = self.storyBoard.instantiateViewController(identifier: "LoyalityPointsAddressViewController") as! LoyalityPointsAddressViewController
                viewController.totalPoints = self.totalPoints
                viewController.itemId = itemId
                viewController.itemName = self.productList[indexPath.row]["productName"] as? String ?? ""
                viewController.itemPoints = self.productList[indexPath.row]["points"] as? Int ?? 0
                viewController.itemImageUrl = self.productList[indexPath.row][kImage] as? String ?? ""
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
}

extension LoyalityPointsViewController {
    func getPoints() {
        if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
            APIManager.sharedInstance.apiCallForPoints(isShowHud: false, URL: POINT_API_BASE_URL, apiName: "points/\(email)", method: .get, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    if let data = responseData[kData] as? [String : AnyObject] {
                        if let points = data["points"] as? Int {
                            self.totalPoints = points
                            
                            self.lblYourPoints.text = "\(self.getLocalizeTextForKey(keyName: "RedeemPoints_lbl_Coins")): \(self.totalPoints)"
                            
                            if self.productList.count > 0 {
                                self.tblProduct.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getProductList() {
        APIManager.sharedInstance.apiCallForPoints(isShowHud: true, URL: POINT_API_BASE_URL, apiName: "products", method: .get, parameters: [:]) { (response, error) in
            if error == nil {
                let responseData = response as! [String : AnyObject]
                if let data = responseData[kData] as? [String : AnyObject] {
                    if let product = data["products"] as? [[String : AnyObject]] {
                        self.productList = product
                        self.tblProduct.reloadData()
                    }
                }
            }
        }
    }
}

class LoyalityProductCell: UITableViewCell {
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var btnRedeem: CustomButton!
    @IBOutlet weak var lblOutofStock: UILabel!
    @IBOutlet weak var customView: CustomView!
}
