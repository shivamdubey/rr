//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class LoyalityPointsAddressViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtSize: CustomTextField!
    @IBOutlet weak var imgSize: UIImageView!
    @IBOutlet weak var sizeBottomView: UIView!
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtCountryCode: CustomTextField!
    @IBOutlet weak var txtMobileNo: CustomTextField!
    @IBOutlet weak var txtAddress: CustomTextView!
    @IBOutlet weak var txtState: CustomTextField!
    @IBOutlet weak var txtCity: CustomTextField!
    @IBOutlet weak var txtPinCode: CustomTextField!
    @IBOutlet weak var txtLandmark: CustomTextField!
    @IBOutlet weak var privacyPolicyTermsConditionsEnglishView: UIView!
    @IBOutlet weak var privacyPolicyTermsConditionsHindiView: UIView!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    @IBOutlet weak var txtSizeTopConstraint: NSLayoutConstraint!
    
    var itemId = 0
    var itemName = ""
    var itemImageUrl = ""
    var itemPoints = 0
    var totalPoints = 0
    var userCountry = ""
    
    var stateList = [[String: AnyObject]]()
    var cityList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if BaseViewController.sharedInstance.appLanguage == APPLANGUAGE.ENGLISH.rawValue {
            self.privacyPolicyTermsConditionsEnglishView.isHidden = false
            self.privacyPolicyTermsConditionsHindiView.isHidden = true
        }
        else {
            self.privacyPolicyTermsConditionsEnglishView.isHidden = true
            self.privacyPolicyTermsConditionsHindiView.isHidden = false
        }
        
        self.setupLocalizationText()
        
        self.txtAddress.textColor = UIColor.white.withAlphaComponent(0.5)
        self.txtAddress.text = self.getLocalizeTextForKey(keyName: "Address_lbl_Title")
        
        if let firstName = BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String {
            if let lastName = BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String {
                self.txtName.text = "\(firstName) \(lastName)"
            }
        }
        
        if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
            self.txtEmail.text = email
        }
        
        if let phoneCode = BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneCode] as? Int {
            self.txtCountryCode.text = "+\(phoneCode)"
            
            self.userCountry = BaseViewController.sharedInstance.appDelegate.userDetails["country_name"] as? String ?? ""
        }
        else {
            let selIndex = CountryManager.shared.countries.firstIndex { (($0 as Country).countryName == "India")}!
            CountryManager.shared.lastCountrySelected = CountryManager.shared.countries[selIndex]
            self.txtCountryCode.text = CountryManager.shared.countries[selIndex].dialingCode
        }
        
        if let phoneNo = BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneNumber] as? String {
            self.txtMobileNo.text = phoneNo
        }
        
        if self.itemId == 1 {
            self.txtSize.isHidden = true
            self.imgSize.isHidden = true
            self.sizeBottomView.isHidden = true
            self.txtSizeTopConstraint.constant = -(self.txtSize.frame.height + 5)
        }
        
        self.getStateList()
    }
    
    func setupLocalizationText() {
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "RedeemPoints_lbl_YourDetails")
        self.txtSize.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "MyCart_lbl_Size"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtName.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "AddAddress_placeholder_FullName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtEmail.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Login_lbl_EmailAddress"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtMobileNo.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_lbl_MobileNumber"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtState.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "AddAddress_placeholder_State"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtCity.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "AddAddress_placeholder_City"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtPinCode.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "RedeemPoints_placeholder_PinCode"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtLandmark.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "AddAddress_placeholder_Landmark"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "btn_Submit"), for: .normal)
    }
    
    @IBAction func btnCountryCode_Clicked(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
            self.txtCountryCode.text = country.dialingCode
            self.userCountry = country.countryName
        }
        countryController.detailColor = UIColor.black
        countryController.labelColor = UIColor.black
        countryController.isHideFlagImage = false
        countryController.isHideDiallingCode = false
    }
    
    @IBAction func btnPrivacyPolicy_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        viewController.strTitle = self.getLocalizeTextForKey(keyName: "Settings_lbl_PrivacyPolicy")
        viewController.strValueForAPI = "Privacy Policy"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnTermsAndCondition_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        viewController.strTitle = self.getLocalizeTextForKey(keyName: "Settings_lbl_TCs")
        viewController.strValueForAPI = "T&Cs"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnSubmit_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        if txtSize.text?.trim().count == 0 && self.itemId != 1 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Size"))
        }
        else if txtName.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_FullName"))
        }
        else if txtEmail.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_EmailAddress"))
        }
        else if !self.isValidEmail(str: txtEmail.text!) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidEmailAddress"))
        }
        else if txtMobileNo.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_MobileNumber"))
        }
        else if txtMobileNo.text!.count < 8 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidMobileNumber"))
        }
        else if txtAddress.text!.trim().count == 0 || self.txtAddress.textColor == UIColor.white.withAlphaComponent(0.5) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Address"))
        }
        else if txtState.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_State"))
        }
        else if txtCity.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_City"))
        }
        else if txtPinCode.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_PinCodeNew"))
        }
        else {
            self.submitRedeemPointForm()
        }
    }
}

extension LoyalityPointsAddressViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtName.isFirstResponder {
            txtEmail.becomeFirstResponder()
        }
        else if txtEmail.isFirstResponder {
            txtMobileNo.becomeFirstResponder()
        }
        else if txtMobileNo.isFirstResponder {
            txtAddress.becomeFirstResponder()
        }
        else if txtState.isFirstResponder {
            txtCity.becomeFirstResponder()
        }
        else if txtCity.isFirstResponder {
            txtPinCode.becomeFirstResponder()
        }
        else if txtPinCode.isFirstResponder {
            txtLandmark.becomeFirstResponder()
        }
        else if txtLandmark.isFirstResponder {
            txtLandmark.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtSize {
            let sizeList = ["S", "M", "L", "XL", "XXL"]
            RPicker.pickOption(title: self.getLocalizeTextForKey(keyName: "MyCart_lbl_Size"), cancelText: self.getLocalizeTextForKey(keyName: "btn_Cancel"), doneText: self.getLocalizeTextForKey(keyName: "btn_Done"), dataArray: sizeList, selectedIndex: 0) { value, index in
                self.txtSize.text = value
            }
            return false
        }
        else if textField == txtState {
            let stateList = self.stateList.map({$0["state_name"] as? String ?? ""})
            RPicker.pickOption(title: self.getLocalizeTextForKey(keyName: "AddAddress_placeholder_State"), cancelText: self.getLocalizeTextForKey(keyName: "btn_Cancel"), doneText: self.getLocalizeTextForKey(keyName: "btn_Done"), dataArray: stateList, selectedIndex: 0) { value, index in
                self.txtState.text = value
                
                self.txtCity.text = ""
                if self.stateList.count > index {
                    if let stateId = self.stateList[index]["state_code"] as? Int {
                        self.getCityList(stateId: stateId)
                    }
                }
            }
            return false
        }
        else if textField == txtCity {
            let cityList = self.cityList.map({$0["city_name"] as? String ?? ""})
            RPicker.pickOption(title: self.getLocalizeTextForKey(keyName: "AddAddress_placeholder_City"), cancelText: self.getLocalizeTextForKey(keyName: "btn_Cancel"), doneText: self.getLocalizeTextForKey(keyName: "btn_Done"), dataArray: cityList, selectedIndex: 0) { value, index in
                self.txtCity.text = value
            }
            return false
        }
        return true
    }
}

extension LoyalityPointsAddressViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.white.withAlphaComponent(0.5) {
            self.txtAddress.textColor = UIColor.white
            self.txtAddress.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trim().isEmpty {
            self.txtAddress.textColor = UIColor.white.withAlphaComponent(0.5)
            self.txtAddress.text = self.getLocalizeTextForKey(keyName: "Address_lbl_Title")
        }
    }
}

extension LoyalityPointsAddressViewController {
    func getStateList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: false, URL: baseURLWithoutAuth, apiName: APINAME.GETSTATE.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [[String: AnyObject]] {
                        self.stateList = data
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getCityList(stateId: Int) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["state_code": stateId]
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETCITY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [[String: AnyObject]] {
                        self.cityList = data
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func submitRedeemPointForm() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let phoneCode = txtCountryCode.text!.replace("+", replacement: "")
            
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "Redeempoints[size]": txtSize.text!,
                         "Redeempoints[full_name]": txtName.text!,
                         "Redeempoints[email]": txtEmail.text!,
                         "Redeempoints[country_code]": phoneCode,
                         "Redeempoints[mobile_no]": txtMobileNo.text!,
                         "Redeempoints[address]": txtAddress.text!,
                         "Redeempoints[statename]": txtState.text!,
                         "Redeempoints[city]": txtCity.text!,
                         "Redeempoints[country]": self.userCountry,
                         "Redeempoints[zip_code]": txtPinCode.text!,
                         "Redeempoints[landmark]": txtLandmark.text!,
                         "Redeempoints[product_name]": self.itemName,
                         "Redeempoints[product_id]": self.itemId,
                         "Redeempoints[points_before_redeem]": "\(self.totalPoints)",
                         "Redeempoints[points_after_redeem]": "\(self.totalPoints - self.itemPoints)"] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.REDEEMPOINTS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String: AnyObject] {
                        if let redeemPointId = data["redeem_points_id"] as? Int {
                            self.redeemPoints(iteamId: self.itemId, redeemPointId: redeemPointId, message: responseData[kMessage] as? String ?? "")
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func redeemPoints(iteamId: Int, redeemPointId: Int, message: String) {
        if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
            let param = ["itemID": iteamId]
            
            APIManager.sharedInstance.apiCallForPoints(isShowHud: false, URL: POINT_API_BASE_URL, apiName: "points/redeem/\(email)", method: .post, parameters: param) { (response, error) in
                if error == nil {
                    self.updateRedeemPointStatus(redeemPointId: redeemPointId, message: message)
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
    }
    
    func updateRedeemPointStatus(redeemPointId: Int, message: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "redeem_points_id": redeemPointId] as [String : Any]
            
            APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.UPDATEREDEEMPOINTSSTATUS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String: AnyObject] {
                        if let redemptionId = data["redemption_id"] as? String {
                            self.callRedeemPointTMAPI(redemptionId: redemptionId)
                        }
                        else {
                            self.callRedeemPointTMAPI(redemptionId: "")
                        }
                    }
                    
                    let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                    alert.view.tintColor = UIColor().alertButtonColor

                    let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                    alert.addAction(hideAction)
                    self.present(alert, animated: true, completion: nil)
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callRedeemPointTMAPI(redemptionId: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let phoneCode = txtCountryCode.text!.replace("+", replacement: "")
            
            let customerId = BatchUser.identifier() ?? ""
            let installId = BatchUser.installationID() ?? ""
            
            let redemptionDate = self.convertDateToString(format: "yyyy-MM-dd", date: Date())
            let redemptionTime = self.convertDateToString(format: "HH:mm:ss", date: Date())
            
            let param = ["app_id": "FanApp_IOS",
                         "custom_id": customerId,
                         "install_id": installId,
                         "authkey": BatchAuthkey,
                         "push_token": BaseViewController.sharedInstance.appDelegate.deviceToken,
                         "event": ["eventcategory": "Rcoin",
                                   "eventlabel": "coin",
                                   "eventaction": "Redemption",
                                   "value": "\(self.itemPoints)",
                                   "valuetype": "string",
                                   "contact_variables": ["firstname": self.txtName.text!,
                                                         "lastname": "",
                                                         "phone": self.txtMobileNo.text!,
                                                         "email": self.txtEmail.text!,
                                                         "RcoinRedemptionDate": redemptionDate,
                                                         "RcoinRedemptionTime": redemptionTime,
                                                         "RcoinAddr": self.txtAddress.text!,
                                                         "RcoinState": self.txtState.text!,
                                                         "RcoinCity": self.txtCity.text!,
                                                         "RcoinZip": self.txtPinCode.text!,
                                                         "RcoinLandmark": self.txtLandmark.text!,
                                                         "RcoinProduct": self.itemName,
                                                         "RcoinImageURL": self.itemImageUrl,
                                                         "RcoinSize": self.txtSize.text!,
                                                         "RcoinCountry": self.userCountry,
                                                         "RcoinCountryCode": phoneCode,
                                                         "RcoinBeforeRedemp": "\(self.totalPoints)",
                                                         "RcoingAfterRedemp": "\(self.totalPoints - self.itemPoints)",
                                                         "RcoinRedeemID": redemptionId],
                                   "variables": ["RcoinRedemptionDate": redemptionDate,
                                                         "RcoinRedemptionTime": redemptionTime,
                                                         "RcoinAddr": self.txtAddress.text!,
                                                         "RcoinState": self.txtState.text!,
                                                         "RcoinCity": self.txtCity.text!,
                                                         "RcoinZip": self.txtPinCode.text!,
                                                         "RcoinLandmark": self.txtLandmark.text!,
                                                         "RcoinProduct": self.itemName,
                                                         "RcoinImageURL": self.itemImageUrl,
                                                         "RcoinSize": self.txtSize.text!,
                                                         "RcoinCountry": self.userCountry,
                                                         "RcoinCountryCode": phoneCode,
                                                         "RcoinBeforeRedemp": "\(self.totalPoints)",
                                                         "RcoingAfterRedemp": "\(self.totalPoints - self.itemPoints)",
                                                         "RcoinRedeemID": redemptionId]]] as [String : Any]
            
            APIManager().apiCall(isShowHud: false, URL: CRM_API_BASE_URL, apiName: APINAME.REGISTERCUSTOMEVENT.rawValue, method: .post, parameters: param) { (response, error) in
            }
        }
    }
}
