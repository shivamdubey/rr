
//
//  TabbarViewController.swift
//  RR
//
//  Created by Sagar on 15/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class TabbarViewController: AMTabsViewController {

    var newSlideMenuController: SlideMenuController!
    var parameters: [String: String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReceivedDeepLinkingData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(receivedDeepLinkingData), name: NSNotification.Name(rawValue: "ReceivedDeepLinkingData"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ClickOnPushNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(clickOnPushNotification), name: NSNotification.Name(rawValue: "ClickOnPushNotification"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SelectSpacificTab"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectSpacifiTab(notification:)), name: NSNotification.Name(rawValue: "SelectSpacificTab"), object: nil)
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.setTabsControllers()
        
        if !BaseViewController.sharedInstance.appDelegate.isLoadHomeScreen {
            SocketIOManager.sharedInstance.establishConnection { (data) in
                print("establishConnection...!")
            }
        }
        
        if !BaseViewController.sharedInstance.appDelegate.isCallPointsAPI {
            BaseViewController.sharedInstance.appDelegate.isCallPointsAPI = true
            
            if BaseViewController.sharedInstance.appDelegate.userDetails[kLoginType] as? String == "Normal" {
                let param = ["firstName": BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String ?? "",
                             "lastName": BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String ?? "",
                             "email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "",
                             "phone": "\(BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneCode] as? String ?? "")\(BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneNumber] as? String ?? "")",
                             "eventType": POINTSEVENTTYPE.NORMAL_REGISTER.rawValue]
                BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
            }
            else {
                let param = ["firstName": BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String ?? "",
                             "lastName": BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String ?? "",
                             "email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "",
                             "phone": "\(BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneCode] as? String ?? "")\(BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneNumber] as? String ?? "")",
                             "socialMedia": BaseViewController.sharedInstance.appDelegate.userDetails[kLoginType] as? String ?? "",
                             "eventType": POINTSEVENTTYPE.SOCIAL_REGISTER.rawValue]
                BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
            }
            
            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "eventType": POINTSEVENTTYPE.BONUS_POINTS.rawValue]
            BaseViewController.sharedInstance.callEarnPointsAPI(param: param)
            
            BaseViewController.sharedInstance.callRegisterCustomEventAPI()
        }
        
        if let webpageURL = BaseViewController.sharedInstance.appDelegate.webpageURL {
            BaseViewController.sharedInstance.appDelegate.webpageURL = nil
            
            URLComponents(url: webpageURL as URL, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
                self.parameters[$0.name] = $0.value
            }
            
            if let webpageUrlString = webpageURL.absoluteString {
                let array = webpageUrlString.components(separatedBy: "link/")
                if array.count > 1 {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                        self.moveToRespectedScreen(type: array[1])
                    }
                }
            }
        }
        
        self.clickOnPushNotification()
    }

    func setTabsControllers() {
        //(UIApplication.shared.delegate as! UnityAppController).rootViewController
        let viewController1 = self.storyboard?.instantiateViewController(withIdentifier: "AllStoriesViewController")
        let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "NewGamesViewController")
        let viewController3 = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController")
        let viewController4 = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController")
        let viewController5 = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController")

        viewControllers = [viewController1!, viewController2!, viewController3!, viewController4!, viewController5!]
        
        self.selectedTabIndex = 2
    }
    
    @objc func selectSpacifiTab(notification: NSNotification) {
        if let userInfo = notification.userInfo as? [String: AnyObject] {
            if let spacificIndexStr = userInfo[kTabbarIndexios] as? String, !spacificIndexStr.isEmpty {
                if let spacificIndex = Int(spacificIndexStr) {
                    self.selectedTabIndex = spacificIndex
                }
            }
        }
    }
    
    @objc func receivedDeepLinkingData() {
        if let webpageURL = BaseViewController.sharedInstance.appDelegate.webpageURL {
            BaseViewController.sharedInstance.appDelegate.webpageURL = nil
            URLComponents(url: webpageURL as URL, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
                self.parameters[$0.name] = $0.value
            }
            
            if let webpageUrlString = webpageURL.absoluteString {
                let array = webpageUrlString.components(separatedBy: "link/")
                if array.count > 1 {
                    self.moveToRespectedScreen(type: array[1])
                }
            }
        }
    }
    
    @objc func clickOnPushNotification() {
        if !BaseViewController.sharedInstance.appDelegate.userInfo.isEmpty {
            if let notificationType = BaseViewController.sharedInstance.appDelegate.userInfo["type_new"] as? String, !notificationType.isEmpty {
                if let navigationController = BaseViewController.sharedInstance.appDelegate.window?.rootViewController as? UINavigationController {
                    if notificationType.contains("Gallery") {
                        if let slug = BaseViewController.sharedInstance.appDelegate.userInfo["slug"] as? String, !slug.isEmpty {
                            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "GalleryImageViewController") as! GalleryImageViewController
                            viewController.slug = slug
                            navigationController.pushViewController(viewController, animated: true)
                        }
                    }
                    else if notificationType.contains("News") {
                        if let slug = BaseViewController.sharedInstance.appDelegate.userInfo["slug"] as? String, !slug.isEmpty {
                            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as! NewsDetailsViewController
                            viewController.slug = slug
                            navigationController.pushViewController(viewController, animated: true)
                        }
                    }
                    else if notificationType.contains("Chat") {
                        NotificationCenter.default.post(name: NSNotification.Name("SelectSpacificTab"), object: nil, userInfo: [kTabbarIndexios: "3"])
                        NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
                    }
                    else if notificationType.contains("Predictor") {
                        let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(identifier: "PredictionMatchListViewController") as! PredictionMatchListViewController
                        navigationController.pushViewController(viewController, animated: true)
                    }
                    else if notificationType.contains("Housie") {
                        let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(identifier: "HousieHomeViewController") as! HousieHomeViewController
                        navigationController.pushViewController(viewController, animated: true)
                    }
                    else if notificationType.contains("Wallpaper") {
                        let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "WallpaperViewController") as! WallpaperViewController
                        viewController.isShowBack = true
                        navigationController.pushViewController(viewController, animated: true)
                    }
                    else if notificationType.contains("LiveScore") {
                        if let matcheId = BaseViewController.sharedInstance.appDelegate.userInfo["matche_id"] as? String, !matcheId.isEmpty {
                            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "LiveScoreViewController") as! LiveScoreViewController
                            viewController.matchId = matcheId
                            viewController.isFromLive = true
                            navigationController.pushViewController(viewController, animated: true)
                        }
                    }
                    else if notificationType.contains("WebURL") {
                        if let redirectUrl = BaseViewController.sharedInstance.appDelegate.userInfo["redirect_url"] as? String, !redirectUrl.isEmpty {
                            navigationController.openUrlInSafariViewController(urlString: redirectUrl)
                        }
                    }
                    
                    if let notificationId = BaseViewController.sharedInstance.appDelegate.userInfo["gcm.notification.id"] as? String, !notificationId.isEmpty {
                        let type = BaseViewController.sharedInstance.appDelegate.userInfo["gcm.notification.type"] as? String ?? ""
                        if let apsInfo = BaseViewController.sharedInstance.appDelegate.userInfo["aps"] as? [String: AnyObject] {
                            if let alertInfo = apsInfo["alert"] as? [String: AnyObject] {
                                let title = alertInfo["title"] as? String ?? ""
                                let message = alertInfo["body"] as? String ?? ""
                                
                                BaseViewController.sharedInstance.storePushNotificationInfo(id: notificationId, type: type, title: title, message: message)
                            }
                        }
                    }
                    
                    BaseViewController.sharedInstance.appDelegate.userInfo = [:]
                }
            }
        }
    }
    
    func moveToRespectedScreen(type: String) {
        if let navigationController = BaseViewController.sharedInstance.appDelegate.window?.rootViewController as? UINavigationController {
            if newSlideMenuController == nil {
                newSlideMenuController = self.slideMenuController()
            }
            
            if type.contains("Predictor")  {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(identifier: "PredictionMatchListViewController") as! PredictionMatchListViewController
                navigationController.pushViewController(viewController, animated: true)
            }
            else if type.contains("Housie") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(identifier: "HousieHomeViewController") as! HousieHomeViewController
                navigationController.pushViewController(viewController, animated: true)
            }
            else if type.contains("Stories") {
                NotificationCenter.default.post(name: NSNotification.Name("SelectSpacificTab"), object: nil, userInfo: [kTabbarIndexios: "0"])
            }
            else if type.contains("Game") {
                NotificationCenter.default.post(name: NSNotification.Name("SelectSpacificTab"), object: nil, userInfo: [kTabbarIndexios: "1"])
            }
            else if type.contains("Chat") {
                NotificationCenter.default.post(name: NSNotification.Name("SelectSpacificTab"), object: nil, userInfo: [kTabbarIndexios: "3"])
                NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
            }
            else if type.contains("Profile") {
                NotificationCenter.default.post(name: NSNotification.Name("SelectSpacificTab"), object: nil, userInfo: [kTabbarIndexios: "4"])
            }
            else if type.contains("IndianStore") {
                if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                    let shopUrl = "https://merchandise.rajasthanroyals.com/?c_email=\(email)"
                    navigationController.openUrlInSafariViewController(urlString: shopUrl)
                }
            }
            else if type.contains("InternationalStore") {
                navigationController.openUrlInSafariViewController(urlString: "https://www.rajasthanroyals.com/adminpanel/internationalstore")
            }
            else if type.contains("FanLounge") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(identifier: "SuperRoyalsViewController") as! SuperRoyalsViewController
                navigationController.pushViewController(viewController, animated: true)
            }
            else if type.contains("Podcast") {
                if let strPodcastId = self.parameters["podcast_id"] {
                    if let podcastId = Int(strPodcastId) {
                        let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "SinglePodcastPlayViewController") as! SinglePodcastPlayViewController
                        viewController.podcastId = podcastId
                        navigationController.pushViewController(viewController, animated: true)
                    }
                }
                else {
                    let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "PodcastViewController") as! PodcastViewController
                    if let menuController = newSlideMenuController {
                        menuController.changeMainViewController(viewController, close: true)
                    }
                }
            }
            else if type.contains("RoyalsCoins") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "LoyalityPointsViewController") as! LoyalityPointsViewController
                navigationController.pushViewController(viewController, animated: true)
            }
            else if type.contains("News") {
                if let slug = self.parameters["slug"] {
                    let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as! NewsDetailsViewController
                    viewController.slug = slug
                    navigationController.pushViewController(viewController, animated: true)
                }
                else {
                    let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
                    viewController.type = ITEMTYPE.News.rawValue
                    if let menuController = newSlideMenuController {
                        menuController.changeMainViewController(viewController, close: true)
                    }
                }
            }
            else if type.contains("MatchdayVideos") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "MomentsOfTheMatchViewController") as! MomentsOfTheMatchViewController
                if let menuController = newSlideMenuController {
                    menuController.changeMainViewController(viewController, close: true)
                }
            }
            else if type.contains("Videos") {
                if let url = self.parameters["URL"], !url.isEmpty {
                    if let videoType = self.parameters["type"] {
                        if videoType == VIDEOTYPE.LOCAL.rawValue {
                            if let videoURL = URL(string: url) {
                                let player = AVPlayer(url: videoURL)
                                let playerViewController = AVPlayerViewController()
                                playerViewController.player = player
                                navigationController.present(playerViewController, animated: true) {
                                    playerViewController.player!.play()
                                }
                            }
                        }
                        else {
                            let array = url.components(separatedBy: "=")
                            if array.count > 1 {
                                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "YouTubePlayerViewController") as! YouTubePlayerViewController
                                viewController.videoId = array[1]
                                navigationController.pushViewController(viewController, animated: true)
                            }
                        }
                    }
                }
                else {
                    let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
                    viewController.type = ITEMTYPE.Video.rawValue
                    if let menuController = newSlideMenuController {
                        menuController.changeMainViewController(viewController, close: true)
                    }
                }
            }
            else if type.contains("Gallery") {
                if let slug = self.parameters["slug"] {
                    let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "GalleryImageViewController") as! GalleryImageViewController
                    viewController.slug = slug
                    navigationController.pushViewController(viewController, animated: true)
                }
                else {
                    let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
                    viewController.type = ITEMTYPE.Gallery.rawValue
                    if let menuController = newSlideMenuController {
                        menuController.changeMainViewController(viewController, close: true)
                    }
                }
            }
            else if type.contains("Education") {
                navigationController.openUrlInSafariViewController(urlString: "https://www.rajasthanroyals.com/education")
            }
            else if type.contains("Foundation") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "FoundationViewController") as! FoundationViewController
                if let menuController = newSlideMenuController {
                    menuController.changeMainViewController(viewController, close: true)
                }
            }
            else if type.contains("ThePavilion") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "ThePavillionViewController") as! ThePavillionViewController
                if let menuController = newSlideMenuController {
                    menuController.changeMainViewController(viewController, close: true)
                }
            }
            else if type.contains("Press") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "PressViewController") as! PressViewController
                if let menuController = newSlideMenuController {
                    menuController.changeMainViewController(viewController, close: true)
                }
            }
            else if type.contains("Players") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "PlayerListViewController") as! PlayerListViewController
                if let menuController = newSlideMenuController {
                    menuController.changeMainViewController(viewController, close: true)
                }
            }
            else if type.contains("Fixtures") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "FixtureListViewController") as! FixtureListViewController
                if let menuController = newSlideMenuController {
                    menuController.changeMainViewController(viewController, close: true)
                }
            }
            else if type.contains("Wallpaper") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "WallpaperViewController") as! WallpaperViewController
                if let menuController = newSlideMenuController {
                    menuController.changeMainViewController(viewController, close: true)
                }
            }
            else if type.contains("QuizRR") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "QuizHomeViewController") as! QuizHomeViewController
                navigationController.pushViewController(viewController, animated: true)
            }
            else if type.contains("RoyalsInAMinute") {
                let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "RoyalsInAMinuteViewController") as! RoyalsInAMinuteViewController
                if let menuController = newSlideMenuController {
                    menuController.changeMainViewController(viewController, close: true)
                }
            }
            else if type.contains("LiveScore") {
                if let matchId = self.parameters["match_id"] {
                    let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "LiveScoreViewController") as! LiveScoreViewController
                    viewController.matchId = matchId
                    viewController.isFromLive = true
                    navigationController.pushViewController(viewController, animated: true)
                }
            }
        }
    }
}

extension TabbarViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
