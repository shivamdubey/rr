//
//  SplashViewController.swift
//  RR
//
//  Created by Sagar Nandha on 26/06/2019.
//

import UIKit
import AVKit

class SplashViewController: BaseViewController {

    var player: AVPlayer?
    var isOneTimeCallFinishPlaying = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "UpdateFirebseDeviceToken"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateFirebseDeviceToken), name: NSNotification.Name(rawValue: "UpdateFirebseDeviceToken"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "DidEnterBackground"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(movetoRespectiveScreen), name: NSNotification.Name(rawValue: "DidEnterBackground"), object: nil)
        
        BaseViewController.sharedInstance.appDelegate.isInSplashScreen = true
        
        self.loadSplashVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        BaseViewController.sharedInstance.appDelegate.isInSplashScreen = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "DidEnterBackground"), object: nil)
    }
    
    @objc func updateFirebseDeviceToken() {
        if let authKey = defaults.value(forKey: kAuthKey) as? String {
            BaseViewController.sharedInstance.appDelegate.authKey = authKey
            if !BaseViewController.sharedInstance.appDelegate.authKey.isEmpty {
                BaseViewController.sharedInstance.updateFirebaseToken()
            }
        }
    }
    
    func loadSplashVideo() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.ambient)
        }
        catch {
        }
        
        var fileName = "Normal"
        if UIScreen.main.bounds.size.height >= 812.0 {
            fileName = "iPhoneX"
        }
        
        if let path = Bundle.main.path(forResource: fileName, ofType: "mp4") {
            self.player = AVPlayer(url: URL(fileURLWithPath: path))
            let playerLayer = AVPlayerLayer(player: self.player)
            playerLayer.frame = self.view.frame
            playerLayer.videoGravity = .resizeAspectFill
            playerLayer.zPosition = -1
            
            self.view.layer.addSublayer(playerLayer)
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlayingSplash), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

            self.player?.play()
        }
    }
    
    @objc func playerDidFinishPlayingSplash(note: NSNotification) {
        print("playerDidFinishPlayingSplash")
        self.movetoRespectiveScreen()
    }
    
    @objc func movetoRespectiveScreen() {
        if !self.isOneTimeCallFinishPlaying {
            self.isOneTimeCallFinishPlaying = true
            
            BatchPush.refreshToken()
            
            if let appLanguage = defaults.value(forKey: kAppLanguage) as? String {
                BaseViewController.sharedInstance.appLanguage = appLanguage
                
                AlertViewTitle = self.getLocalizeTextForKey(keyName: "AppName")
                NoInternet = self.getLocalizeTextForKey(keyName: "lbl_NoInternet")
                ErrorMessage = self.getLocalizeTextForKey(keyName: "lbl_ErrorMessage")
            }
            
            if let authKey = defaults.value(forKey: kAuthKey) as? String {
                BaseViewController.sharedInstance.appDelegate.authKey = authKey
                
                if let data = defaults.object(forKey: kUserDetails) as? Data {
                    if let userDetails = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String: AnyObject] {
                        BaseViewController.sharedInstance.appDelegate.userDetails = userDetails
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                    if !BaseViewController.sharedInstance.appDelegate.authKey.isEmpty {
                        BaseViewController.sharedInstance.autoLogin(isCallAnalyticsSessionAPI: true)
                    }
                }
                
                let viewController = self.newStoryBoard.instantiateViewController(withIdentifier: "WelcomeBackViewController") as! WelcomeBackViewController
                self.navigationController?.pushViewController(viewController, animated: false)
                //BaseViewController.sharedInstance.navigateToHomeScreen()
            }
            else {
                BaseViewController.sharedInstance.navigateToLoginScreen()
            }
        }
    }
}
