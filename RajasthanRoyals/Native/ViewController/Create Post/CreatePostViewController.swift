//
//  CreatePostViewController.swift
//  RaniCircle
//
//  Created by Apple on 21/09/20.
//  Copyright © 2020 YellowPanther. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer
import Mantis
import GiphyUISDK
import GiphyCoreSDK

class CreatePostViewController: BaseViewController {
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var lblAddPhoto: UILabel!
    
    @IBOutlet weak var imgViewUser: CustomImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var txtViewWriteHere: CustomTextView!
    @IBOutlet weak var cvUploadPhotoVideo: UICollectionView!
    @IBOutlet weak var viewPhoto: UIView!
    @IBOutlet weak var btnCloseReset: UIButton!
    @IBOutlet weak var btnPost: CustomButton!

    @IBOutlet weak var constHeightForViewPhoto: NSLayoutConstraint!
    @IBOutlet weak var constTopForTxtViewWriteHere: NSLayoutConstraint!
    @IBOutlet weak var constTopForBtnPost: NSLayoutConstraint!

    var arrPhotoList = [UIImage]()
    var uploadType = FEEDTYPE.TEXT.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        self.txtViewWriteHere.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        self.setupData(str: "text")
        
        self.txtViewWriteHere.text = self.getLocalizeTextForKey(keyName: "CreatePost_lbl_WhatsOnYourMind")
        self.txtViewWriteHere.textColor = UIColor.lightGray
        
        self.setUserDetails()
    }
    
    func setupLocalizationText() {
        self.navItem.title = self.getLocalizeTextForKey(keyName: "CreatePost_lbl_Title")
        self.lblAddPhoto.text = self.getLocalizeTextForKey(keyName: "CreatePost_lbl_AddPhoto")
        self.btnPost.setTitle(self.getLocalizeTextForKey(keyName: "CreatePost_btn_Post"), for: .normal)
    }
    
    func setUserDetails() {
        if let image = BaseViewController.sharedInstance.appDelegate.userDetails[kImage] as? String {
            if let url = URL(string: image) {
                self.imgViewUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        self.lblName.text = "\(BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String ?? "") \(BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String ?? "")"
    }
    
    func setupData(str: String) {
        if str == "image" {
            self.uploadType = FEEDTYPE.IMAGE.rawValue

            self.viewPhoto.isHidden = false
            self.btnCloseReset.isHidden = false

            self.constHeightForViewPhoto.constant = self.viewPhoto.frame.size.height
            
            self.constTopForTxtViewWriteHere.constant = 10
            self.txtViewWriteHere.isHidden = false

            self.cvUploadPhotoVideo.isHidden = false
            self.constTopForBtnPost.constant = 10
        }
        else {
            self.uploadType = FEEDTYPE.TEXT.rawValue

            self.viewPhoto.isHidden = false
            self.btnCloseReset.isHidden = true

            self.constHeightForViewPhoto.constant = 45
            
            self.constTopForTxtViewWriteHere.constant = 10
            self.txtViewWriteHere.isHidden = false
            
            self.cvUploadPhotoVideo.isHidden = true
            self.constTopForBtnPost.constant = -50
        }
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.view.tintColor = UIColor().alertButtonColor
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_UsingCamera"), style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_ChooseExistingPhoto"), style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .camera
        myPickerController.allowsEditing = false
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .savedPhotosAlbum
        myPickerController.allowsEditing = false
        myPickerController.modalPresentationStyle = .overCurrentContext
        myPickerController.addStatusBarBackgroundView()
        myPickerController.view.tintColor = UIColor().alertButtonColor
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    @IBAction func btnAddPhoto_clicked(_ sender: Any) {
        self.showActionSheet()
    }
    
    @IBAction func btnClose_clicked(_ sender: Any) {
        self.arrPhotoList.removeAll()
        self.cvUploadPhotoVideo.reloadData()
    
        self.setupData(str: "text")
    }
    
    @IBAction func btnPost_clicked(_ sender: Any) {
        self.view.endEditing(true)
        if self.uploadType == FEEDTYPE.TEXT.rawValue {
            if self.txtViewWriteHere.text?.trim().count == 0 || self.txtViewWriteHere.text == self.getLocalizeTextForKey(keyName: "CreatePost_lbl_WhatsOnYourMind") {
                UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_AddCaption"))
            }
            else {
                self.callCreatePostAPI()
            }
        }
        else {
            if self.txtViewWriteHere.text == self.getLocalizeTextForKey(keyName: "CreatePost_lbl_WhatsOnYourMind") {
                self.txtViewWriteHere.text = ""
            }            
            self.callCreatePostAPI()
        }
    }
}

extension CreatePostViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = self.getLocalizeTextForKey(keyName: "CreatePost_lbl_WhatsOnYourMind")
            textView.textColor = UIColor.lightGray
        }
    }
}

extension CreatePostViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPhotoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadPhotoVideoCVCell", for: indexPath) as! UploadPhotoVideoCVCell
        
        cell.imgViewPhoto.isHidden = true
        
        cell.imgViewPhoto.isHidden = false
        cell.imgViewPhoto.image = self.arrPhotoList[indexPath.item]
        
        cell.btnDelete.tag = indexPath.item
        cell.btnDelete.addTarget(self, action: #selector(btnDelete_clicked), for: .touchUpInside)
        
        return cell
    }
    
    @objc func btnDelete_clicked(sender : UIButton) {
        self.arrPhotoList.remove(at: sender.tag)
        if self.arrPhotoList.count == 0 {
            self.setupData(str: "text")
        }
        self.cvUploadPhotoVideo.reloadData()
    }
}

extension CreatePostViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.cvUploadPhotoVideo.bounds.height, height: self.cvUploadPhotoVideo.bounds.height)
    }
}

extension CreatePostViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            var config = Mantis.Config()
            config.ratioOptions = [.original, .custom]
            config.addCustomRatio(byVerticalWidth: 1, andVerticalHeight: 1)
            config.addCustomRatio(byVerticalWidth: 3, andVerticalHeight: 4)
            config.addCustomRatio(byVerticalWidth: 3, andVerticalHeight: 2)
            config.addCustomRatio(byVerticalWidth: 16, andVerticalHeight: 9)
            let cropViewController = Mantis.cropViewController(image: image,  config: config)
            cropViewController.modalPresentationStyle = .fullScreen
            cropViewController.delegate = self
            self.present(cropViewController, animated: true)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CreatePostViewController: CropViewControllerDelegate {
    func cropViewControllerDidCrop(_ cropViewController: CropViewController, cropped: UIImage, transformation: Transformation) {
        self.dismiss(animated: true)
        self.setupData(str: "image")
        self.arrPhotoList.append(cropped)
        self.cvUploadPhotoVideo.reloadData()
    }
    
    func cropViewControllerDidCancel(_ cropViewController: CropViewController, original: UIImage) {
        self.dismiss(animated: true)
    }
}

extension CreatePostViewController {
    func callCreatePostAPI() {
        var param = [kAuthKey : BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feed[description]" : self.txtViewWriteHere.text!,
                     "Feed[is_platform]" : "FanApp"] as [String : Any]
        
        if uploadType == FEEDTYPE.IMAGE.rawValue  {
            var strType = ""
            for _ in 0...self.arrPhotoList.count - 1 {
                if strType == "" {
                    strType = FEEDTYPE.IMAGE.rawValue
                }
                else {
                    strType = strType + "," + FEEDTYPE.IMAGE.rawValue
                }
            }
            param["Feed[type]"] = strType
        }
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCallWithImageVideoList(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.CREATEPOST.rawValue, parameters: param, images: self.arrPhotoList, imageParameterName: "Feed[file]", imageName: "Feed[file]", videoParameterName: "Feed[file]", uploadType: self.uploadType) { (response, error) in
                if error == nil {
                    if let response = response {
                        if let response = response.dictData {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callGetAllPostAPI"), object: nil, userInfo: nil)
                            self.navigationController?.popViewController(animated: true)
                            
                            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "postID": "\(response.feed_id ?? 0)", "eventType": POINTSEVENTTYPE.FEED_POST.rawValue]
                            self.callEarnPointsAPI(param: param)
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

class UploadPhotoVideoCVCell: UICollectionViewCell, SwiftyGifDelegate {
    @IBOutlet weak var imgViewPhoto: UIImageView!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
