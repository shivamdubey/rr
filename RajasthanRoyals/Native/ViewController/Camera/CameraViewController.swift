//
//  CameraViewController.swift
//  Ranicircle
//
//  Created by Apple on 15/09/20.
//  Copyright © 2020 YellowPanther. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class CameraViewController: SwiftyCamViewController {
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgCameraPhoto: CustomImageView!
    @IBOutlet weak var btnPhotos: UIButton!
    @IBOutlet weak var btnCapture: SwiftyRecordButton!
    @IBOutlet weak var btnFlipCamera: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    
    var delegate: StoryViewDelegate!
    var initialTouchPoint: CGPoint = CGPoint(x: 0, y: 0)
    
	override func viewDidLoad() {
		super.viewDidLoad()
        
        let imgClose = #imageLiteral(resourceName: "close_big_ic").imageWithColor(color: UIColor.white)
        self.btnClose.setImage(imgClose, for: .normal)
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.shouldPrompToAppSettings = true
        self.cameraDelegate = self
        self.maximumVideoDuration = 30.0
        self.shouldUseDeviceOrientation = true
        self.allowAutoRotate = true
        self.audioEnabled = true
        self.swipeToZoom = false
        self.pinchToZoom = true
        self.flashMode = .auto
        self.btnFlash.setImage(#imageLiteral(resourceName: "flashauto"), for: UIControl.State())
        self.btnCapture.buttonEnabled = false
        
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        self.view.addGestureRecognizer(panGesture)
	}
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .darkContent
    }

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
        self.btnCapture.delegate = self
        self.fetchLastPhoto()
	}
    
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)

        if sender.state == .began {
            initialTouchPoint = touchPoint
        }
        else if sender.state == .changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        }
        else if sender.state == .ended || sender.state == .cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            }
            else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    func fetchLastPhoto() {
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchOptions.fetchLimit = 1

        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
        if fetchResult.count > 0 {
            self.fetchPhotoAtIndex(fetchResult: fetchResult)
        }
    }

    func fetchPhotoAtIndex(fetchResult: PHFetchResult<PHAsset>) {
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        PHImageManager.default().requestImage(for: fetchResult.object(at: 0) as PHAsset, targetSize: self.imgCameraPhoto.frame.size, contentMode: PHImageContentMode.aspectFill, options: requestOptions, resultHandler: { (image, _) in
            if let image = image {
                self.imgCameraPhoto.image = image
            }
        })
    }
    
    @IBAction func btnClose_Clicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPhotos_Clicked(_ sender: Any) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .photoLibrary
        myPickerController.mediaTypes = ["public.image"]
        myPickerController.modalPresentationStyle = .overCurrentContext
        myPickerController.addStatusBarBackgroundView()
        myPickerController.view.tintColor = UIColor().alertButtonColor
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    @IBAction func btnFlipCamera_Clicked(_ sender: Any) {
        self.switchCamera()
    }
    
    @IBAction func btnFlash_Clicked(_ sender: Any) {
        self.toggleFlashAnimation()
    }
}

extension CameraViewController {
    fileprivate func hideButtons() {
        UIView.animate(withDuration: 0.25) {
            self.btnFlash.alpha = 0.0
            self.btnFlipCamera.alpha = 0.0
        }
    }
    
    fileprivate func showButtons() {
        UIView.animate(withDuration: 0.25) {
            self.btnFlash.alpha = 1.0
            self.btnFlipCamera.alpha = 1.0
        }
    }
    
    fileprivate func focusAnimationAt(_ point: CGPoint) {
        let focusView = UIImageView(image: #imageLiteral(resourceName: "focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }) { (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }) { (success) in
                focusView.removeFromSuperview()
            }
        }
    }
    
    fileprivate func toggleFlashAnimation() {
        if self.flashMode == .auto {
            self.flashMode = .on
            self.btnFlash.setImage(#imageLiteral(resourceName: "flash"), for: UIControl.State())
        }
        else if self.flashMode == .on {
            self.flashMode = .off
            self.btnFlash.setImage(#imageLiteral(resourceName: "flashOutline"), for: UIControl.State())
        }
        else if self.flashMode == .off {
            self.flashMode = .auto
            self.btnFlash.setImage(#imageLiteral(resourceName: "flashauto"), for: UIControl.State())
        }
    }
}

extension CameraViewController: SwiftyCamViewControllerDelegate {
    func swiftyCamSessionDidStartRunning(_ swiftyCam: SwiftyCamViewController) {
        print("Session did start running")
        self.btnCapture.buttonEnabled = true
    }
    
    func swiftyCamSessionDidStopRunning(_ swiftyCam: SwiftyCamViewController) {
        print("Session did stop running")
        self.btnCapture.buttonEnabled = false
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
        let storyboard = UIStoryboard(name: "PhotoEditor", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
        viewController.delegate = self.delegate
        viewController.closeDelegate = self
        viewController.photo = photo
        viewController.isPhoto = true
        viewController.modalPresentationStyle = .overFullScreen
        self.present(viewController, animated: false, completion: nil)
    }

    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        print("Did Begin Recording")
        self.btnCapture.growButton()
        self.hideButtons()
    }

    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        print("Did finish Recording")
        self.btnCapture.shrinkButton()
        self.showButtons()
    }

    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
        let storyboard = UIStoryboard(name: "PhotoEditor", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
        viewController.delegate = self.delegate
        viewController.closeDelegate = self
        viewController.videoURL = url
        viewController.isPhoto = false
        viewController.modalPresentationStyle = .overFullScreen
        self.present(viewController, animated: false, completion: nil)
    }

    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
        print("Did focus at point: \(point)")
        self.focusAnimationAt(point)
    }
    
    func swiftyCamDidFailToConfigure(_ swiftyCam: SwiftyCamViewController) {
//        let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
//        let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
//        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
//        present(alertController, animated: true, completion: nil)
    }

    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
        print("Zoom level did change. Level: \(zoom)")
    }

    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
        print("Camera did change to \(camera.rawValue)")
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error) {
        print(error)
    }
}

extension CameraViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
        
        if let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String {
            if mediaType.contains("image") {
                if let photo = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    let storyboard = UIStoryboard(name: "PhotoEditor", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
                    viewController.delegate = self.delegate
                    viewController.closeDelegate = self
                    viewController.photo = photo
                    viewController.isPhoto = true
                    viewController.modalPresentationStyle = .overFullScreen
                    self.present(viewController, animated: false, completion: nil)
                }
            }
            else {
                if let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
                    let storyboard = UIStoryboard(name: "PhotoEditor", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
                    viewController.delegate = self.delegate
                    viewController.closeDelegate = self
                    viewController.videoURL = url
                    viewController.isPhoto = false
                    viewController.modalPresentationStyle = .overFullScreen
                    self.present(viewController, animated: false, completion: nil)
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CameraViewController: CloseMyStoryViewsDelegate {
    func closeMyStoryViews() {
        self.dismiss(animated: true, completion: nil)
    }
}
