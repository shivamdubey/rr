//
//  UploadStoryViewController.swift
//  Ranicircle
//
//  Created by Apple on 15/09/20.
//  Copyright © 2020 YellowPanther. All rights reserved.
//

import UIKit
import AVKit
import Foundation
import AVFoundation
import KRProgressHUD
import Alamofire

class UploadStoryViewController: BaseViewController {
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var mainImageView: UIView!
    @IBOutlet weak var imgCaptureBg: UIImageView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var imgCapture: UIImageView!
    @IBOutlet weak var videoView: VideoPlay!
    @IBOutlet weak var btnShareToStory: CustomButton!
    
    var delegate: StoryViewDelegate!
    var closeDelegate: CloseMyStoryViewsDelegate!
    var captureImage: UIImage!
    var videoUrl: URL!
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.blurView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurView.addSubview(blurEffectView)
        
        if self.captureImage == nil {
            self.mainImageView.isHidden = true
            self.videoView.isHidden = false
            
            if videoUrl != nil {
                self.videoView.playVideoWithURL(url: videoUrl)
                self.videoView.player.play()
                
                NotificationCenter.default.addObserver(self, selector: #selector(self.videoPlayerEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.videoView.player.currentItem)
            }
        }
        else {
            self.mainImageView.isHidden = false
            self.videoView.isHidden = true
            
            self.imgCaptureBg.image = captureImage
            self.imgCapture.image = captureImage
        }
        
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        self.view.addGestureRecognizer(panGesture)
    }
    
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)

        if sender.state == .began {
            initialTouchPoint = touchPoint
        }
        else if sender.state == .changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        }
        else if sender.state == .ended || sender.state == .cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            }
            else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
   
    @objc func videoPlayerEnd(notification: NSNotification) {
        self.videoView.player.seek(to: CMTime.zero)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            self.videoView.player.play()
        }
    }
    
    @IBAction func btnClose_Clicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnShareToStory_Clicked(_ sender: Any) {
        self.uploadStory()
    }
}

extension UploadStoryViewController {
    func uploadStory() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey : appDelegate.authKey, "Foundationstory[type]": self.captureImage != nil ? "image" : "video", "Foundationstory[is_platform]": "FanApp"]
            KRProgressHUD.show(withMessage: "Please wait")
            Alamofire.upload(multipartFormData: {
                multipartFormData in
                //Append Images
                if self.captureImage != nil {
                    if let imageData = self.captureImage.jpegData(compressionQuality: 0.6) {
                        multipartFormData.append(imageData, withName: "Foundationstory[files]", fileName: "StoryImage.png", mimeType: "image/png")
                    }
                }
                //Append Video
                if self.videoUrl != nil {
                    guard let compressedData = NSData(contentsOf: self.videoUrl) else {
                        return
                    }
                    let selectedVideoData = compressedData as Data?
                    if selectedVideoData != nil {
                        multipartFormData.append(selectedVideoData!, withName: "Foundationstory[files]", fileName: "StoryVideo.mov", mimeType: "video/mov")
                    }
                }
                //Append Param
                for (key, value) in param {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, usingThreshold: 1, to: "\(baseURLWithAuth)\(APINAME.CREATESTORY.rawValue)", method: .post, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: {
                        response in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            KRProgressHUD.dismiss()
                        }
                        switch response.result {
                        case .success:
                            if let json: NSDictionary = response.result.value as? NSDictionary {
                                print(json)
                                if json[kStatus] as? Int == kIsSuccess {
                                    self.dismiss(animated: true, completion: nil)
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name("ReloadStoryScreen"), object: nil, userInfo: nil)
                                    
                                    let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "eventType": POINTSEVENTTYPE.STORIES_CREATE.rawValue]
                                    self.callEarnPointsAPI(param: param)
                                }
                                else if json[kStatus] as? Int == kUserNotFound {
                                    if let message = json[kMessage] as? String {
                                        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                                        alert.view.tintColor = UIColor().alertButtonColor
                                        
                                        let hideAction: UIAlertAction = UIAlertAction.init(title: kOk, style: .default, handler: { (action) in
                                            BaseViewController.sharedInstance.clearAllUserDataFromPreference()
                                            BaseViewController.sharedInstance.navigateToLoginScreen()
                                        })
                                        alert.addAction(hideAction)
                                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else {
                                    UIAlertController().alertViewWithTitleAndMessage(self, message: json[kMessage] as? String ?? ErrorMessage)
                                }
                            }
                            else {
                                UIAlertController().alertViewWithTitleAndMessage(self, message: ErrorMessage)
                            }
                        case .failure(let error):
                            print (error)
                            UIAlertController().alertViewWithTitleAndMessage(self, message: error.localizedDescription)
                        }
                    })
                case .failure(let error):
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        KRProgressHUD.dismiss()
                    }
                    print (error)
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error.localizedDescription)
                }
            })
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
