//
//  PhotoEditorViewController.swift
//  Ranicircle
//
//  Created by Apple on 15/09/20.
//  Copyright © 2020 YellowPanther. All rights reserved.
//

import UIKit
import AVKit
import Foundation
import AVFoundation
import KRProgressHUD
import Alamofire
import Photos

extension UIView {
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    
    func toImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImageFromMyView!
    }
    
    @objc func toImageView() -> UIImageView {
        let tempImageView = UIImageView()
        tempImageView.image = toImage()
        tempImageView.frame = frame
        tempImageView.contentMode = .scaleAspectFit
        return tempImageView
    }
}

extension UIImageView {
    func alphaAtPoint(_ point: CGPoint) -> CGFloat {
        var pixel: [UInt8] = [0, 0, 0, 0]
        let colorSpace = CGColorSpaceCreateDeviceRGB();
        let alphaInfo = CGImageAlphaInfo.premultipliedLast.rawValue
        
        guard let context = CGContext(data: &pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: alphaInfo) else {
            return 0
        }
        
        context.translateBy(x: -point.x, y: -point.y)
        layer.render(in: context)
        let floatAlpha = CGFloat(pixel[3])
        return floatAlpha
    }
}

protocol CloseMyStoryViewsDelegate {
    func closeMyStoryViews()
}

protocol DeleteMyStoryDelegate {
    func deleteMyStory(storyId: Int)
}

public final class PhotoEditorViewController: UIViewController {
    @IBOutlet weak var canvasView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tempImageView: UIImageView!
    @IBOutlet weak var topToolbar: UIView!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var btnShareToStory: UIButton!
    
    var delegate: StoryViewDelegate!
    var closeDelegate: CloseMyStoryViewsDelegate!
    var isPhoto = true
    var photo: UIImage?
    var videoURL = URL(string: "")
    
    //Video
    var player: AVPlayer?
    var playerController : AVPlayerViewController?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isPhoto {
            self.imageView.contentMode = UIView.ContentMode.scaleAspectFit
            self.canvasView.frame = CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height)
            self.tempImageView.frame = CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height)
            
            self.imageView.isHidden = false
            self.imageView.image = self.photo
        }
        else {
            self.imageView.isHidden = true
            
            self.player = AVPlayer(url: videoURL!)
            self.playerController = AVPlayerViewController()
            
            guard self.player != nil && self.playerController != nil else {
                return
            }
            self.playerController!.showsPlaybackControls = false
            
            self.playerController!.player = player!
            self.addChild(self.playerController!)
            self.view.addSubview(self.playerController!.view)
            self.tempImageView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
            
            self.playerController!.view.frame = view.frame
       
            self.view.insertSubview(self.playerController!.view, belowSubview: self.canvasView)
            NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player!.currentItem)
        }
        
        self.deleteView.layer.cornerRadius = self.deleteView.bounds.height / 2
        self.deleteView.layer.borderWidth = 2.0
        self.deleteView.layer.borderColor = UIColor.white.cgColor
        self.deleteView.clipsToBounds = true
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.setAnimationsEnabled(true)
        if !self.isPhoto {
            self.player?.play()
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.player != nil {
            self.player!.pause()
        }
    }

    @IBAction func btnShareToStory_Clicked(_ sender: AnyObject) {
        self.uploadStory()
    }
    
    @IBAction func btnClose_Clicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func playerItemDidReachEnd(_ notification: Notification) {
        if self.player != nil {
            self.player!.seek(to: CMTime.zero)
            self.player!.play()
        }
    }
    
    func hideToolbar(hide: Bool) {
        self.topToolbar.isHidden = hide
        self.btnShareToStory.isHidden = hide
    }
}

extension PhotoEditorViewController {
    func uploadStory() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey : BaseViewController.sharedInstance.appDelegate.authKey, "Foundationstory[type]": self.photo != nil ? "image" : "video"]
            KRProgressHUD.show(withMessage: "Please wait")
            Alamofire.upload(multipartFormData: {
                multipartFormData in
                //Append Images
                if self.photo != nil {
                    if let imageData = self.photo!.jpegData(compressionQuality: 0.6) {
                        multipartFormData.append(imageData, withName: "Foundationstory[files]", fileName: "StoryImage.png", mimeType: "image/png")
                    }
                }
                //Append Video
                if self.videoURL != nil {
                    guard let compressedData = NSData(contentsOf: self.videoURL!) else {
                        return
                    }
                    if let selectedVideoData = compressedData as Data? {
                        multipartFormData.append(selectedVideoData, withName: "Foundationstory[files]", fileName: "StoryVideo.mov", mimeType: "video/mov")
                    }
                }
                //Append Param
                for (key, value) in param {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, usingThreshold: 1, to: "\(baseURLWithAuth)\(APINAME.CREATESTORY.rawValue)", method: .post, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: {
                        response in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            KRProgressHUD.dismiss()
                        }
                        switch response.result {
                        case .success:
                            if let json: NSDictionary = response.result.value as? NSDictionary {
                                print(json)
                                if json[kStatus] as? Int == kIsSuccess {
                                    self.dismiss(animated: true, completion: nil)
                                    if self.delegate != nil {
                                        if let stories = json[kData] as? [[String: AnyObject]] {
                                            self.delegate.storyView(userId: BaseViewController.sharedInstance.appDelegate.userDetails["appuser_id"] as? Int ?? 0, stories: stories)
                                        }
                                    }
                                    if self.closeDelegate != nil {
                                        self.closeDelegate.closeMyStoryViews()
                                    }
                                }
                                else if json[kStatus] as? Int == kUserNotFound {
                                    if let message = json[kMessage] as? String {
                                        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                                        alert.view.tintColor = UIColor().alertButtonColor
                                        
                                        let hideAction: UIAlertAction = UIAlertAction.init(title: kOk, style: .default, handler: { (action) in
                                            BaseViewController.sharedInstance.clearAllUserDataFromPreference()
                                            BaseViewController.sharedInstance.navigateToLoginScreen()
                                        })
                                        alert.addAction(hideAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else {
                                    UIAlertController().alertViewWithTitleAndMessage(self, message: json[kMessage] as? String ?? ErrorMessage)
                                }
                            }
                            else {
                                UIAlertController().alertViewWithTitleAndMessage(self, message: ErrorMessage)
                            }
                        case .failure(let error):
                            print (error)
                            UIAlertController().alertViewWithTitleAndMessage(self, message: error.localizedDescription)
                        }
                    })
                case .failure(let error):
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        KRProgressHUD.dismiss()
                    }
                    print (error)
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error.localizedDescription)
                }
            })
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
