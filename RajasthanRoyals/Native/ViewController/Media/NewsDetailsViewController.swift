//
//  NewsDetailsViewController.swift
//  RR
//
//  Created by Sagar on 29/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit
import Auk
import moa

class NewsDetailsViewController: BaseViewController {
    
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var customBgView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UITextView!
    @IBOutlet weak var btnShare: UIButton!

    var isFromSuperRoyalsMedia = false
    var slug = ""
    var newsDetails = [String: AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageScrollView.auk.settings.placeholderImage = #imageLiteral(resourceName: "placeholder")
        self.imageScrollView.auk.settings.contentMode = .scaleAspectFill
        self.imageScrollView.auk.settings.pagingEnabled = true
        self.imageScrollView.auk.settings.pageControl.backgroundColor = .clear
        self.imageScrollView.auk.settings.pageControl.pageIndicatorTintColor = .white
        self.imageScrollView.auk.settings.pageControl.currentPageIndicatorTintColor = UIColor().themeDarkPinkColor.withAlphaComponent(0.8)
        self.imageScrollView.auk.startAutoScroll(delaySeconds: 3.0)
        
//        let rightBorder: CALayer = CALayer()
//        rightBorder.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
//        rightBorder.borderWidth = 10
//        rightBorder.frame = CGRect(x:0, y: 0, width: self.customBgView.frame.size.width, height:10)
//        rightBorder.cornerRadius = 20
//
//
//        self.customBgView.layer.addSublayer(rightBorder)


        self.addFirebaseCustomeEvent(eventName: "NewsDetailsScreen")
        
        if !self.isFromSuperRoyalsMedia {
            self.getNewsDetails()
        }
        else {
            self.setNewsDetails()
        }
    }
    
    @IBAction func btnShare_Clicked(_ sender: UIButton) {
        let url = URL.init(string: "https://www.rajasthanroyals.com/latest-news/\(slug)")!
        let objectsToShare: [Any] = [url]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender
        self.present(activityViewController, animated: true, completion: nil)
        
        self.callTMAnalyticsAPI(category: "News", action: "Share", label: newsDetails[kTitle] as? String ?? "")
    }
    
    //Set News Details
    func setNewsDetails() {
        if self.isFromSuperRoyalsMedia {
            self.btnShare.isHidden = true
            if let image = newsDetails[kImage] as? String {
                self.imageScrollView.auk.show(url: image)
            }
            
            if let title = newsDetails[kTitle] as? String {
                self.lblTitle.text = title
            }
            
            if let description = newsDetails[kDescription] as? String {
                self.lblDescription.attributedText = description.html2Attributed
            }
        }
        else {
            self.btnShare.isHidden = false
            
            if let images = newsDetails[kImages] as? [[String: AnyObject]] {
                self.imageScrollView.auk.removeAll()
                for image in images {
                    if let image = image[kImage] as? String {
                        self.imageScrollView.auk.show(url: image)
                    }
                }
            }
            
            if let title = newsDetails[kTitle] as? String {
                self.lblTitle.text = title
            }
            
            if let description = newsDetails[kMobileDescription] as? String {
                self.lblDescription.attributedText = description.html2Attributed
            }
        }
    }
}

extension NewsDetailsViewController {
    func getNewsDetails() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kSlug: self.slug]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETNEWSDETAILS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.newsDetails = data
                    }
                    self.setNewsDetails()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
