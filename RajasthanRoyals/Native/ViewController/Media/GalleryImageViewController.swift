//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class GalleryImageViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var galleryCV: UICollectionView!

    var slug = ""
    var currentPage = 1
    var nextPage = kN
    var imagesList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addFirebaseCustomeEvent(eventName: "GalleryScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Media_btn_Gallery")
        
        self.setupLayout()
        
        self.getGalleryGroupImages(isShowHud: true)
    }
    
    func setupLayout() {
        let layout = PinterestLayout()
        layout.delegate = self
        layout.cellPadding = 5
        layout.numberOfColumns = 2
        self.galleryCV.collectionViewLayout = layout
    }
}

extension GalleryImageViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == imagesList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getGalleryGroupImages(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCell", for: indexPath) as! StickerCell
        
        if let imageUrl = imagesList[indexPath.item]["files"] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgSticker.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        cell.imgSticker.layer.borderWidth = 5
        cell.imgSticker.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
        cell.imgSticker.layer.cornerRadius = 5
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PhotosViewController") as! PhotosViewController
        viewController.slug = self.slug
        viewController.currentPage = currentPage
        viewController.nextPage = nextPage
        viewController.imagesList = imagesList
        viewController.visibleIndex = indexPath.item
        self.present(viewController, animated: true, completion: nil)
    }
}

extension GalleryImageViewController {
    func getGalleryGroupImages(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kSlug: self.slug, kPage: self.currentPage, kPageSize: PAGESIZE] as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETGALLARY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.imagesList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let videosData = responseData[kData] as? [[String : AnyObject]] {
                        for videos in videosData {
                            self.imagesList.append(videos)
                        }
                    }
                    
                    self.galleryCV.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension GalleryImageViewController: PinterestLayoutDelegate {
    func collectionView(collectionView: UICollectionView, heightForImageAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        return numberArray.randomElement() ?? 220.0
    }
    
    func collectionView(collectionView: UICollectionView, heightForAnnotationAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        return 0.0
    }
}
