//
//  NewsVideosViewController.swift
//  RR
//
//  Created by Sagar on 23/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

let numberArray: [CGFloat] = [190.0, 195.0, 200.0, 205.0, 210.0, 215.0, 220.0, 225.0, 230.0, 235.0, 240.0]

class MediaViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnNews: CustomButton!
    @IBOutlet weak var btnVideos: CustomButton!
    @IBOutlet weak var btnGallery: CustomButton!
    @IBOutlet weak var videoCV: UICollectionView!
    @IBOutlet weak var galleryCV: UICollectionView!
    @IBOutlet weak var newsCV: UICollectionView!
  
    
    var type = ITEMTYPE.News.rawValue
    
    var currentPageForNews = 1
    var nextPageForNews = kN
    var newsList = [[String: AnyObject]]()
    
    var currentPageForVideos = 1
    var nextPageForVideos = kN
    var videoList = [[String: AnyObject]]()
    
    var currentPageForGallery = 1
    var nextPageForGallery = kN
    var imagesList = [[String: AnyObject]]()
    
    var refreshControl: UIRefreshControl!
    var refreshControl1: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "NewsScreen")
        
        self.setupLayout()
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Latest")
        self.btnNews.setTitle(self.getLocalizeTextForKey(keyName: "Media_btn_News"), for: .normal)
        self.btnVideos.setTitle(self.getLocalizeTextForKey(keyName: "Media_btn_Videos"), for: .normal)
        self.btnGallery.setTitle(self.getLocalizeTextForKey(keyName: "Media_btn_Gallery"), for: .normal)
        
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        self.bgViewForButtons.isHidden=true
        refreshControl = UIRefreshControl.init()
        refreshControl.tintColor = .white
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshCollectionView(sender:)), for: .valueChanged)
        videoCV.refreshControl = refreshControl
        
        refreshControl1 = UIRefreshControl.init()
        refreshControl1.tintColor = .white
        refreshControl1.backgroundColor = .clear
        refreshControl1.addTarget(self, action: #selector(refreshGalleryCV(sender:)), for: .valueChanged)
        galleryCV.refreshControl = refreshControl1
        
        
        refreshControl = UIRefreshControl.init()
        refreshControl.tintColor = .white
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshNewsCV(sender:)), for: .valueChanged)
        newsCV.refreshControl = refreshControl
        
        
        if type == ITEMTYPE.News.rawValue {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Latest: News")
            
            self.videoCV.isHidden = true
            self.galleryCV.isHidden = true
            self.newsCV.isHidden = false
            
            self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Media_btn_News")
            self.btnNews_Clicked(sender: self.btnNews)
        }
        else if type == ITEMTYPE.Video.rawValue {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Latest: Videos")
            
            self.videoCV.isHidden = false
            self.galleryCV.isHidden = true
            self.newsCV.isHidden = true
            self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Media_btn_Videos")
            self.btnVideos_Clicked(sender: self.btnVideos)
        }
        else {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Latest: Gallery")
            
            self.videoCV.isHidden = true
            self.galleryCV.isHidden = false
            self.newsCV.isHidden = true
            self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Media_btn_Gallery")
            self.btnGallery_Clicked(sender: self.btnGallery)
        }
    }
    
    func setupLayout() {
        let layout = PinterestLayout()
        layout.delegate = self
        layout.cellPadding = 0
        layout.numberOfColumns = 1
        self.galleryCV.collectionViewLayout = layout
    }
    
    //UIRefreshControl method
    @objc func refreshCollectionView(sender: AnyObject) {
        
            self.currentPageForVideos = 1
            self.getVideosList(isShowHud: false)
        
    }
    
    @objc func refreshNewsCV(sender: AnyObject) {
        self.currentPageForNews = 1
        self.getNewsList(isShowHud: false)
    }
    
    @objc func refreshGalleryCV(sender: AnyObject) {
        self.currentPageForGallery = 1
        self.getGalleryGroups(isShowHud: false)
    }
    
    @IBAction func btnNews_Clicked(sender: UIButton) {
        self.addFirebaseCustomeEvent(eventName: "NewsScreen")
        
        self.type = ITEMTYPE.News.rawValue
        
        self.btnNews.backgroundColor = SelectedButtonBGColor
        self.btnVideos.backgroundColor = DeSelectedButtonBGColor
        self.btnGallery.backgroundColor = DeSelectedButtonBGColor
        
        self.btnNews.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnVideos.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnGallery.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.videoCV.isHidden = true
        self.galleryCV.isHidden = true
        self.newsCV.isHidden = false
        
        if self.newsList.count <= 0 {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Latest: News")
            
            self.currentPageForNews = 1
            self.nextPageForNews = kN
            self.getNewsList(isShowHud: true)
        }
        else {
            self.newsCV.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
            self.newsCV.reloadData()
        }
    }
    
    @IBAction func btnVideos_Clicked(sender: UIButton) {
        self.addFirebaseCustomeEvent(eventName: "VideoScreen")
        
        self.type = ITEMTYPE.Video.rawValue
        
        self.btnNews.backgroundColor = DeSelectedButtonBGColor
        self.btnVideos.backgroundColor = SelectedButtonBGColor
        self.btnGallery.backgroundColor = DeSelectedButtonBGColor
        
        self.btnNews.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnVideos.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnGallery.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.videoCV.isHidden = false
        self.galleryCV.isHidden = true
        self.newsCV.isHidden = true
        
        if self.videoList.count <= 0 {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Latest: Videos")
            
            self.currentPageForVideos = 1
            self.nextPageForVideos = kN
            self.getVideosList(isShowHud: true)
        }
        else {
            self.videoCV.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
            self.videoCV.reloadData()
        }
    }
    
    @IBAction func btnGallery_Clicked(sender: UIButton) {
        self.addFirebaseCustomeEvent(eventName: "GalleryGroupScreen")
        
        self.type = ITEMTYPE.Gallery.rawValue
        
        self.btnNews.backgroundColor = DeSelectedButtonBGColor
        self.btnVideos.backgroundColor = DeSelectedButtonBGColor
        self.btnGallery.backgroundColor = SelectedButtonBGColor
        
        self.btnNews.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnVideos.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnGallery.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.videoCV.isHidden = true
        self.galleryCV.isHidden = false
        self.newsCV.isHidden = true
        
        if self.imagesList.count <= 0 {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Latest: Gallery")
            
            self.currentPageForGallery = 1
            self.nextPageForGallery = kN
            self.getGalleryGroups(isShowHud: true)
        }
        else {
            self.galleryCV.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
            self.galleryCV.reloadData()
        }
    }
}

extension MediaViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == videoCV {
           return videoList.count
        }
        else if collectionView == newsCV {
            return newsList.count
        }
        else {
            return imagesList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == videoCV {
            
                if indexPath.row == videoList.count - 1 && nextPageForVideos == kY {
                    currentPageForVideos = currentPageForVideos + 1
                    self.getVideosList(isShowHud: false)
                }
            
        }
        else if collectionView == newsCV
        {
            if indexPath.row == newsList.count - 1 && nextPageForNews == kY {
                currentPageForNews = currentPageForNews + 1
                self.getNewsList(isShowHud: false)
            }
        }
        else {
            if indexPath.row == imagesList.count - 1 && nextPageForGallery == kY {
                currentPageForGallery = currentPageForGallery + 1
                self.getGalleryGroups(isShowHud: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == videoCV {
           
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsCell
                
                if let imageUrl = videoList[indexPath.item][kImage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                    }
                }
                
                if let title = videoList[indexPath.item][kTitle] as? String {
                    cell.lblTitle.text = title
                }
                cell.imgNews.layer.borderWidth = 5
                cell.imgNews.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
                cell.imgNews.layer.cornerRadius = 5
                cell.imgPlay.isHidden = false
                
                cell.btnShareSocial.setTitle("", for: .normal)
                ///cell.
                cell.btnShareSocial.tag = indexPath.item
                cell.btnShareSocial.addTarget(self, action: #selector(btnShareSocial_Clicked(_:)), for: .touchUpInside)
                
                return cell
            
        }
        else if collectionView == newsCV {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsDataCell", for: indexPath) as! NewsDataCell
            
            if let imageUrl = newsList[indexPath.item][kThumbnailImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
            cell.imgBg.layer.borderWidth = 5
            cell.imgBg.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
            cell.imgBg.layer.cornerRadius = 5
            if let title = newsList[indexPath.item][kTitle] as? String {
                cell.lblTitle.text = title
            }
            if let date = newsList[indexPath.item][kCreatedAt] as? String {
                //cell.lblDate.text = date
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "dd MMM mm"

                if let strDate = dateFormatterGet.date(from: date) {
                    cell.lblDate.text = "\(dateFormatterPrint.string(from: strDate))min"
                } else {
                   print("There was an error decoding the string")
                }
            }
            
            cell.btnShareSocial.setTitle("", for: .normal)
            ///cell.
            cell.btnShareSocial.tag = indexPath.item
            cell.btnShareSocial.addTarget(self, action: #selector(btnShareSocial_Clicked(_:)), for: .touchUpInside)
            
            
            return cell
        }
                    
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsCell
            
            if let imageUrl = imagesList[indexPath.item][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            cell.imgNews.layer.borderWidth = 5
            cell.imgNews.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
            cell.imgNews.layer.cornerRadius = 5
            if let title = imagesList[indexPath.item][kTitle] as? String {
                cell.lblTitle.text = title
            }
            cell.btnShareSocial.setTitle("", for: .normal)
            cell.btnShareSocial.isHidden=true
            ///cell.
            cell.btnShareSocial.tag = indexPath.item
            cell.btnShareSocial.addTarget(self, action: #selector(btnShareSocial_Clicked(_:)), for: .touchUpInside)
            return cell
        }
    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == videoCV {
            
                self.callTMAnalyticsAPI(category: "Video", action: "Watched", label: "\(self.videoList[indexPath.row][kTitle] as? String ?? "")")
                
                let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "videoName": self.videoList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.VIDEOS.rawValue]
                self.callEarnPointsAPI(param: param)
                
                if let type = self.videoList[indexPath.row][kType] as? String {
                    if type == VIDEOTYPE.LOCAL.rawValue {
                        if let video = self.videoList[indexPath.row][kVideo] as? String {
                            if let videoURL = URL(string: video) {
                                let player = AVPlayer(url: videoURL)
                                let playerViewController = AVPlayerViewController()
                                playerViewController.player = player
                                self.present(playerViewController, animated: true) {
                                    playerViewController.player!.play()
                                }
                            }
                        }
                    }
                    else {
                        if let videoUrl = videoList[indexPath.row][kVideo] as? String {
                            let array = videoUrl.components(separatedBy: "=")
                            if array.count > 1 {
                                let viewController = self.storyBoard.instantiateViewController(withIdentifier: "YouTubePlayerViewController") as! YouTubePlayerViewController
                                viewController.videoId = array[1]
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                        }
                    }
                }
            
        }
        else if collectionView == newsCV {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Latest: News: \(self.newsList[indexPath.row][kTitle] as? String ?? "")")
            
            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "newsArticleName": self.newsList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.LATEST_NEWS.rawValue]
            self.callEarnPointsAPI(param: param)
            
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as! NewsDetailsViewController
            viewController.slug = self.newsList[indexPath.row][kSlug] as? String ?? ""
            self.navigationController?.pushViewController(viewController, animated: true)
        }
       
        else {
            self.callTMAnalyticsAPI(category: "Gallery", action: "View", label: "\(self.imagesList[indexPath.row][kTitle] as? String ?? "")")
            
            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "galleryName": self.imagesList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.GALLERY_VIEW.rawValue]
            self.callEarnPointsAPI(param: param)
            
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "GalleryImageViewController") as! GalleryImageViewController
            viewController.slug = self.imagesList[indexPath.item][kSlug] as? String ?? ""
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func btnShareSocial_Clicked(_ sender: UIButton) {
       
    if self.type == ITEMTYPE.News.rawValue {
        let slug = self.newsList[sender.tag][kSlug] as? String ?? ""
        let url = URL.init(string: "https://www.rajasthanroyals.com/latest-news/\(slug)")!
        let objectsToShare: [Any] = [url]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender
        self.present(activityViewController, animated: true, completion: nil)
        
        self.callTMAnalyticsAPI(category: "News", action: "Share", label: self.newsList[sender.tag][kTitle] as? String ?? "")
            }
        else{
            if self.type == ITEMTYPE.Gallery.rawValue
            {
//                let videoUrl = imagesList[sender.tag][kVideo] as? String
//                let objectsToShare: [Any] = [videoUrl]
//                let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//                activityViewController.popoverPresentationController?.sourceView = sender
//                self.present(activityViewController, animated: true, completion: nil)
//
//                self.callTMAnalyticsAPI(category: "Video", action: "Share", label: self.imagesList[sender.tag][kTitle] as? String ?? "")
            }
            else{
            let videoUrl = videoList[sender.tag][kVideo] as? String
            let objectsToShare: [Any] = [videoUrl]
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = sender
            self.present(activityViewController, animated: true, completion: nil)
            
            self.callTMAnalyticsAPI(category: "Video", action: "Share", label: self.videoList[sender.tag][kTitle] as? String ?? "")
            }
        }
    }
}

extension MediaViewController {
    func getNewsList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPageForNews, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETNEWSLIST.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPageForNews == 1 {
                        self.newsList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPageForNews = nextPage
                    }
                    
                    if let newsData = responseData[kData] as? [[String : AnyObject]] {
                        for news in newsData {
                            self.newsList.append(news)
                        }
                    }
                    
                    self.newsCV.reloadData()
                }
                else {
                    if self.currentPageForNews > 1 {
                        self.currentPageForNews = self.currentPageForNews - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getVideosList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPageForVideos, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETVIDEOLIST.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPageForVideos == 1 {
                        self.videoList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPageForVideos = nextPage
                    }
                    
                    if let videosData = responseData[kData] as? [[String : AnyObject]] {
                        for videos in videosData {
                            self.videoList.append(videos)
                        }
                    }
                    
                    self.videoCV.reloadData()
                }
                else {
                    if self.currentPageForVideos > 1 {
                        self.currentPageForVideos = self.currentPageForVideos - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getGalleryGroups(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPageForGallery, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETGALLARYGROUP.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl1.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPageForGallery == 1 {
                        self.imagesList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPageForGallery = nextPage
                    }
                    
                    if let videosData = responseData[kData] as? [[String : AnyObject]] {
                        for videos in videosData {
                            self.imagesList.append(videos)
                        }
                    }
                    
                    self.galleryCV.reloadData()
                }
                else {
                    if self.currentPageForGallery > 1 {
                        self.currentPageForGallery = self.currentPageForGallery - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension MediaViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 1
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        if collectionView == newsCV {
            return CGSize(width: widthPerItem, height: 150)
        }
        else{
            return CGSize(width: widthPerItem, height: 220)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}

extension MediaViewController: PinterestLayoutDelegate {
    func collectionView(collectionView: UICollectionView, heightForImageAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        if collectionView == newsCV{
            return 150.0
        }else{
            return 220.0
        }
    }
    
    func collectionView(collectionView: UICollectionView, heightForAnnotationAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        return 0.0
    }
}

class NewsCell: UICollectionViewCell {
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var btnShareSocial: UIButton!
}

class NewsDataCell: UICollectionViewCell {
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnShareSocial: UIButton!
}
