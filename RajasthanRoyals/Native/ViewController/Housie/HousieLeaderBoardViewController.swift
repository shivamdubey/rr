//
//  HousieLeaderBoardViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 11/03/21.
//

import UIKit

class HousieLeaderBoardViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblLeaderboard: UITableView!
    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnFirstInning: CustomButton!
    @IBOutlet weak var btnSecondInning: CustomButton!
    @IBOutlet weak var lblEmptyMessage: UILabel!
    
    var firstInningUserList = [[String: AnyObject]]()
    var secondInningUserList = [[String: AnyObject]]()
    
    var isFirstInning = false
    var matchID = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Leaderboard_lbl_Title")
        self.btnFirstInning.setTitle(self.getLocalizeTextForKey(keyName: "LiveScore_lbl_FirstInnings"), for: .normal)
        self.btnSecondInning.setTitle(self.getLocalizeTextForKey(keyName: "LiveScore_lbl_SecondInnings"), for: .normal)
        
        self.btnFirstInning_Clicked(sender: self.btnFirstInning)
        self.getHousieLeaderboardList()
    }
    
    @IBAction func btnFirstInning_Clicked(sender: UIButton) {
        self.isFirstInning = true
        
        self.btnFirstInning.backgroundColor = SelectedButtonBGColor
        self.btnSecondInning.backgroundColor = DeSelectedButtonBGColor
        
        self.btnFirstInning.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnSecondInning.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.tblLeaderboard.reloadData()
    }
    
    @IBAction func btnSecondInning_Clicked(sender: UIButton) {
        self.isFirstInning = false
        
        self.btnFirstInning.backgroundColor = DeSelectedButtonBGColor
        self.btnSecondInning.backgroundColor = SelectedButtonBGColor
        
        self.btnFirstInning.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnSecondInning.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.tblLeaderboard.reloadData()
    }
}

extension HousieLeaderBoardViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isFirstInning {
            return self.firstInningUserList.count
        }
        else {
            return self.secondInningUserList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HousieLeaderboardCell") as! HousieLeaderboardCell
        
        if self.isFirstInning {
            let userDetails = self.firstInningUserList[indexPath.row]
            
            if let full_name = userDetails[kFullName] as? String {
                cell.lblName.text = full_name
            }
            
            if let type = userDetails[kType] as? String {
                cell.lblType.text = type
            }
            
            if let imageUrl = userDetails[kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            
            if (BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int) == (userDetails[kAppUserId] as? Int) {
                cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
            }
            else {
                cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            }
            return cell
        }
        else {
            let userDetails = self.secondInningUserList[indexPath.row]
            
            if let full_name = userDetails[kFullName] as? String {
                cell.lblName.text = full_name
            }
            
            if let type = userDetails[kType] as? String {
                cell.lblType.text = type
            }
            
            if let imageUrl = userDetails[kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }

            if (BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int) == (userDetails[kAppUserId] as? Int) {
                cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
            }
            else {
                cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            }
            
            return cell
        }
    }
}

extension HousieLeaderBoardViewController {
    func getHousieLeaderboardList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "match_id": self.matchID] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.DECLAREHOUSIEQUIZRESULTS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    self.lblEmptyMessage.text = responseData[kMessage] as? String ?? ""
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        if let inning1 = data["inning1"]  as? [[String : AnyObject]] {
                            self.firstInningUserList = inning1
                        }
                        
                        if let inning2 = data["inning2"]  as? [[String : AnyObject]] {
                            self.secondInningUserList = inning2
                        }
                        
                        self.tblLeaderboard.reloadData()
                        
                        if data.isEmpty {
                            self.lblEmptyMessage.isHidden = true
                        }
                        else {
                            self.lblEmptyMessage.isHidden = false
                        }
                    }
                    else {
                        self.lblEmptyMessage.isHidden = false
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

class HousieLeaderboardCell: UITableViewCell {
    @IBOutlet weak var bgView: CustomView!
    @IBOutlet weak var imgUser: CustomImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
}

