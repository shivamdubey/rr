//
//  RoyalHousieFixtureListViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 12/03/21.
//

import UIKit

class RoyalHousieFixtureListViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblMatchList: UITableView!
    @IBOutlet weak var lblEmptyMessage: UILabel!
    
    var currentPage = 1
    var nextPage = kN
    var matchList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "PlayersStatistics_lbl_Matches")
        
        self.getMatchList(isShowHud: true)
    }
}

extension RoyalHousieFixtureListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchList.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == matchList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getMatchList(isShowHud: false)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeMatchesTblCell") as! HomeMatchesTblCell
        
        if let imageUrl = matchList[indexPath.row][kFirstImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgViewTeamLeft.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let imageUrl = matchList[indexPath.row][kSecondImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgViewTeamRight.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let location = matchList[indexPath.row][kLocation] as? String {
            cell.lblAddress.text = location
        }
        
        if var strDateTime = matchList[indexPath.row][kDateTime] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            
            if let dateTime = dateFormatter.date(from: strDateTime) {
                dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
                strDateTime = dateFormatter.string(from: dateTime)
                cell.lblDateTime.text = strDateTime
            }
        }
        
        cell.imgFirstWinner.isHidden = true
        cell.imgSecondWinner.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let matchId = matchList[indexPath.row][kMatch_Id] as? Int {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "HousieLeaderBoardViewController") as! HousieLeaderBoardViewController
            viewController.matchID = "\(matchId)"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension RoyalHousieFixtureListViewController {
    func getMatchList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE] as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETRRALLPASTMATCH.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.matchList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }

                    if let matchList = responseData[kData] as? [[String : AnyObject]] {
                        for match in matchList {
                            self.matchList.append(match)
                        }
                    }
                    
                    self.tblMatchList.reloadData()
                    
                    self.lblEmptyMessage.text = responseData[kMessage] as? String ?? ""
                    if self.matchList.count > 0 {
                        self.lblEmptyMessage.isHidden = true
                    }
                    else {
                        self.lblEmptyMessage.isHidden = false
                    }
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
