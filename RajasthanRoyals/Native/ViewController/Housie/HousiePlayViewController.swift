//
//  HousiePlayViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 11/03/21.
//

import UIKit

class HousiePlayViewController: BaseViewController {
    
    @IBOutlet weak var imgRoyalHousie: UIImageView!
    @IBOutlet weak var segmentView: CustomView!
    @IBOutlet weak var btnFirstInning: CustomButton!
    @IBOutlet weak var btnSecondInning: CustomButton!
    
    @IBOutlet weak var imgFirstTeam: UIImageView!
    @IBOutlet weak var imgSecondTeam: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var imgHousieBg: UIImageView!
    @IBOutlet weak var lblWeAlso: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var imgSponser: UIImageView!
    @IBOutlet weak var btnTitcket1: UIButton!
    @IBOutlet weak var btnTitcket2: UIButton!
    
    @IBOutlet weak var CVHousie: UICollectionView!

    var refreshControl: UIRefreshControl!
    var timer: Timer!
    var dictGenreral = [String:AnyObject]()
    var matchDetails = [String: AnyObject]()
    var matchId = 0
    
    var isFirstInning = false
    var isFirstTicket = false
    
    var strSecondInningStart = ""
    
    var ticketsList = [String: AnyObject]()
    var firstInningTicket1List = [[String: Any]]()
    var firstInningTicket2List = [[String: Any]]()
    
    var secondInningTicket1List = [[String: Any]]()
    var secondInningTicket2List = [[String: Any]]()
    
    var firstInningsWinners = [String: Any]()
    var secondInningsWinners = [String: Any]()
        
    var inningNumber = ""
    var ticketNumber = ""
    
    let imageTicket1Sel = UIImage(named: "ticket1_sel")
    let imageTicket1DeSel = UIImage(named: "ticket1_unsel")
    let imageTicket2Sel = UIImage(named: "ticket2_sel")
    let imageTicket2DeSel = UIImage(named: "ticket2_unsel")
    
    var selBgRandomColor = UIColor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.segmentView.backgroundColor = SegmentViewBGColor
        
        self.matchId = self.matchDetails[kMatch_Id] as? Int ?? 0
        self.setMatchDetails()
        
        self.btnFirstInning.setTitle(self.getLocalizeTextForKey(keyName: "LiveScore_lbl_FirstInnings"), for: .normal)
        self.btnSecondInning.setTitle(self.getLocalizeTextForKey(keyName: "LiveScore_lbl_SecondInnings"), for: .normal)

        refreshControl = UIRefreshControl.init()
        refreshControl.tintColor = .white
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshCollectionView(sender:)), for: .valueChanged)
        CVHousie.refreshControl = refreshControl
        
        let colorsArray = [UIColor(hex: "EFFFEF"), UIColor(hex: "F9E5FD"), UIColor(hex: "FFE7D7"), UIColor(hex: "FFFFFF")]
        self.selBgRandomColor = colorsArray.randomElement() ?? .white
        
        self.btnFirstInning_Clicked(sender: self.btnFirstInning)
        
        if !self.dictGenreral.isEmpty {
            if let image3 = self.dictGenreral["image3"] as? String {
                if let url = URL(string: image3) {
                    self.imgRoyalHousie.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder.png"))
                }
            }
            
            if let image1 = self.dictGenreral["image1"] as? String {
                if let url = URL(string: image1) {
                    self.imgHousieBg.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder.png"))
                }
            }
            
            if let image2 = self.dictGenreral["image2"] as? String {
                if let url = URL(string: image2) {
                    self.imgSponser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder.png"))
                }
            }
            
            if let second_inning_description = self.dictGenreral["second_inning_description"] as? String {
                self.lblWeAlso.attributedText = second_inning_description.html2Attributed
            }
        }
        
        self.createRandomHousieQuizList(isShowHud: true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            self.bottomView.roundCorners([.topLeft,.topRight], radius: 25)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !self.ticketsList.isEmpty {
            self.stopTimer()
            self.startTimer()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimer()
    }
    
    //Set Match Details
    func setMatchDetails() {
        if let imageUrl = self.matchDetails[kFirstImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgFirstTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let imageUrl = self.matchDetails[kSecondImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgSecondTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        self.lblLocation.text = self.matchDetails[kLocation] as? String ?? ""
    }
    
    func showHideSecondInningData() {
        if !self.isFirstInning {
            if self.strSecondInningStart == kY {
                self.CVHousie.isHidden = false
                self.imgHousieBg.isHidden = true
                self.lblWeAlso.isHidden = true
                self.btnTitcket1.isHidden = false
                self.btnTitcket2.isHidden = false
            }
            else {
                self.CVHousie.isHidden = true
                self.imgHousieBg.isHidden = false
                self.lblWeAlso.isHidden = false
                self.btnTitcket1.isHidden = true
                self.btnTitcket2.isHidden = true
            }
        }
        else {
            self.CVHousie.isHidden = false
            self.imgHousieBg.isHidden = true
            self.lblWeAlso.isHidden = true
            self.btnTitcket1.isHidden = false
            self.btnTitcket2.isHidden = false
        }
    }
    
    //UIRefreshControl method
    @objc func refreshCollectionView(sender: AnyObject) {
        self.createRandomHousieQuizList(isShowHud: false)
    }
    
    //Start Timer
    func startTimer() {
        if self.timer == nil {
            self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.updateTimeValue), userInfo: nil, repeats: true)
        }
    }
    
    //Stop Timer
    func stopTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    //Update Time
    @objc func updateTimeValue() {
        self.createRandomHousieQuizList(isShowHud: false)
    }
    
    @IBAction func btnFAQ_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "HousieFAQViewController") as! HousieFAQViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnEvents_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AllEventsViewController") as! AllEventsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnFirstInning_Clicked(sender: UIButton) {
        self.inningNumber = "1"
        
        self.isFirstInning = true
        self.isFirstTicket = true
        
        self.btnTitcket1_Clicked(sender: self.btnTitcket1)
        
        self.btnFirstInning.backgroundColor = SelectedButtonBGColor
        self.btnSecondInning.backgroundColor = DeSelectedButtonBGColor
        
        self.btnFirstInning.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnSecondInning.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.CVHousie.reloadData()
        
        self.showHideSecondInningData()
    }
    
    @IBAction func btnSecondInning_Clicked(sender: UIButton) {
        self.inningNumber = "2"

        self.isFirstInning = false
        self.isFirstTicket = false
        
        self.btnTitcket1_Clicked(sender: self.btnTitcket1)
        
        self.btnFirstInning.backgroundColor = DeSelectedButtonBGColor
        self.btnSecondInning.backgroundColor = SelectedButtonBGColor
        
        self.btnFirstInning.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnSecondInning.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.CVHousie.reloadData()
        self.showHideSecondInningData()
    }
    
    @IBAction func btnTitcket1_Clicked(sender: UIButton) {
        self.ticketNumber = "1"
        
        self.isFirstTicket = true

        self.btnTitcket1.setImage(imageTicket1Sel, for: .normal)
        self.btnTitcket2.setImage(imageTicket2DeSel, for: .normal)
        
        self.CVHousie.reloadData()
    }
    
    @IBAction func btnTitcket2_Clicked(sender: UIButton) {
        self.ticketNumber = "2"

        self.isFirstTicket = false
        
        self.btnTitcket1.setImage(imageTicket1DeSel, for: .normal)
        self.btnTitcket2.setImage(imageTicket2Sel, for: .normal)
        
        self.CVHousie.reloadData()
    }
}

extension HousiePlayViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.isFirstInning {
            if self.isFirstTicket {
                return self.firstInningTicket1List.count
            }
            else {
                return self.firstInningTicket2List.count
            }
        }
        else {
            if self.isFirstTicket {
                return self.secondInningTicket1List.count
            }
            else {
                return self.secondInningTicket2List.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HousiePlayCell", for: indexPath) as! HousiePlayCell
        if self.isFirstInning {
            if self.isFirstTicket {
                let dict = self.firstInningTicket1List[indexPath.item]
                
                cell.imgDot.image = #imageLiteral(resourceName: "full_image").imageWithColor(color: self.selBgRandomColor)
                
                if let title = dict["title"] as? String {
                    cell.lblTitle.text = title
                }
                
                if let imageUrl = dict[kImage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgHousie.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                if dict["is_answered"] as? String == kN {
                    cell.imgParticipate.isHidden = true
                }
                else {
                    cell.imgParticipate.isHidden = false
                }
            }
            else {
                let dict = self.firstInningTicket2List[indexPath.item]
                
                cell.imgDot.image = #imageLiteral(resourceName: "full_image").imageWithColor(color: self.selBgRandomColor)
                
                if let title = dict["title"] as? String {
                    cell.lblTitle.text = title
                }
                
                if let imageUrl = dict[kImage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgHousie.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                if dict["is_answered"] as? String == kN {
                    cell.imgParticipate.isHidden = true
                }
                else {
                    cell.imgParticipate.isHidden = false
                }
            }
        }
        else {
            if self.isFirstTicket {
                let dict = self.secondInningTicket1List[indexPath.item]
                
                cell.imgDot.image = #imageLiteral(resourceName: "full_image").imageWithColor(color: self.selBgRandomColor)
                
                if let title = dict["title"] as? String {
                    cell.lblTitle.text = title
                }
                
                if let imageUrl = dict[kImage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgHousie.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                if dict["is_answered"] as? String == kN {
                    cell.imgParticipate.isHidden = true
                }
                else {
                    cell.imgParticipate.isHidden = false
                }
            }
            else {
                let dict = self.secondInningTicket2List[indexPath.item]
                
                cell.imgDot.image = #imageLiteral(resourceName: "full_image").imageWithColor(color: self.selBgRandomColor)
                
                if let title = dict["title"] as? String {
                    cell.lblTitle.text = title
                }
                
                if let imageUrl = dict[kImage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgHousie.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                if dict["is_answered"] as? String == kN {
                    cell.imgParticipate.isHidden = true
                }
                else {
                    cell.imgParticipate.isHidden = false
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.isFirstInning {
            if self.isFirstTicket {
                let dict = self.firstInningTicket1List[indexPath.item]
                if dict["is_locked"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventLock"))
                }
                else if dict["is_timeout"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventTimeout"))
                }
                else if dict["is_answered"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventAlreadyParticipate"))
                }
                else if dict["is_answered"] as? String == kN {
                    self.userPlayHousieQuiz(indexPath: indexPath)
                }
            }
            else {
                let dict = self.firstInningTicket2List[indexPath.item]
                if dict["is_locked"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventLock"))
                }
                else if dict["is_timeout"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventTimeout"))
                }
                else if dict["is_answered"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventAlreadyParticipate"))
                }
                else if dict["is_answered"] as? String == kN {
                    self.userPlayHousieQuiz(indexPath: indexPath)
                }
            }
        }
        else {
            if self.isFirstTicket {
                let dict = self.secondInningTicket1List[indexPath.item]
                if dict["is_locked"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventLock"))
                }
                else if dict["is_timeout"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventTimeout"))
                }
                else if dict["is_answered"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventAlreadyParticipate"))
                }
                else if dict["is_answered"] as? String == kN {
                    self.userPlayHousieQuiz(indexPath: indexPath)
                }
            }
            else {
                let dict = self.secondInningTicket2List[indexPath.item]
                if dict["is_locked"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventLock"))
                }
                else if dict["is_timeout"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventTimeout"))
                }
                else if dict["is_answered"] as? String == kY {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_HousieEventAlreadyParticipate"))
                }
                else if dict["is_answered"] as? String == kN {
                    self.userPlayHousieQuiz(indexPath: indexPath)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.init(width: SCREEN_WIDTH, height: 70)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            case UICollectionView.elementKindSectionHeader:
                let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HousieFooterCell", for: indexPath) as! HousieFooterCell
                return cell
            case UICollectionView.elementKindSectionFooter:
                let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HousieFooterCell", for: indexPath) as! HousieFooterCell
                
                if self.isFirstInning {
                    if let firstRow = self.firstInningsWinners["First_Raw"] as? String {
                        cell.btnFirstRow.isSelected = firstRow == kY ? true : false
                    }
                    if let secondRow = self.firstInningsWinners["Second_Raw"] as? String {
                        cell.btnSecondRow.isSelected = secondRow == kY ? true : false
                    }
                    if let thirdRow = self.firstInningsWinners["Third_Raw"] as? String {
                        cell.btnThirdRow.isSelected = thirdRow == kY ? true : false
                    }
                    if let firstFive = self.firstInningsWinners["First_Five"] as? String {
                        cell.btnFabFourRow.isSelected = firstFive == kY ? true : false
                    }
                    if let fullHouse = self.firstInningsWinners["Full_House"] as? String {
                        cell.btnFullHousieRow.isSelected = fullHouse == kY ? true : false
                    }
                }
                else {
                    if let firstRow = self.secondInningsWinners["First_Raw"] as? String {
                        cell.btnFirstRow.isSelected = firstRow == kY ? true : false
                    }
                    if let secondRow = self.secondInningsWinners["Second_Raw"] as? String {
                        cell.btnSecondRow.isSelected = secondRow == kY ? true : false
                    }
                    if let thirdRow = self.secondInningsWinners["Third_Raw"] as? String {
                        cell.btnThirdRow.isSelected = thirdRow == kY ? true : false
                    }
                    if let firstFive = self.secondInningsWinners["First_Five"] as? String {
                        cell.btnFabFourRow.isSelected = firstFive == kY ? true : false
                    }
                    if let fullHouse = self.secondInningsWinners["Full_House"] as? String {
                        cell.btnFullHousieRow.isSelected = fullHouse == kY ? true : false
                    }
                }

                cell.btnFirstRow.addTarget(self, action: #selector(btnFooter_Clicked(_:)), for: .touchUpInside)
                cell.btnSecondRow.addTarget(self, action: #selector(btnFooter_Clicked(_:)), for: .touchUpInside)
                cell.btnThirdRow.addTarget(self, action: #selector(btnFooter_Clicked(_:)), for: .touchUpInside)
                cell.btnFabFourRow.addTarget(self, action: #selector(btnFooter_Clicked(_:)), for: .touchUpInside)
                cell.btnFullHousieRow.addTarget(self, action: #selector(btnFooter_Clicked(_:)), for: .touchUpInside)
                
                return cell
            default:
                return UICollectionReusableView.init()
        }
    }
    
    //UIButton Action
    @objc func btnFooter_Clicked(_ sender: UIButton) {
        UIAlertController().alertViewWithTitleAndMessage(self, message: sender.isSelected ? self.getLocalizeTextForKey(keyName: "ValidationMessage_EventGone") : self.getLocalizeTextForKey(keyName: "ValidationMessage_EventOpen"))
    }
}

extension HousiePlayViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 3
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}

extension HousiePlayViewController {
    func createRandomHousieQuizList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "match_id": self.matchId] as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.CREATERANDOMHOUSIEQUIZ.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.ticketsList = data
                        if let inning1 = data["inning1"]  as? [String : AnyObject] {
                            if let ticket1 = inning1["ticket1"]  as? [[String : AnyObject]] {
                                self.firstInningTicket1List = ticket1
                            }
                            
                            if let ticket2 = inning1["ticket2"]  as? [[String : AnyObject]] {
                                self.firstInningTicket2List = ticket2
                            }
                            
                            if let winners = inning1["winners"]  as? [String : AnyObject] {
                                self.firstInningsWinners = winners
                            }
                        }
                        
                        if let inning2 = data["inning2"]  as? [String : AnyObject] {
                            if let ticket1 = inning2["ticket1"]  as? [[String : AnyObject]] {
                                self.secondInningTicket1List = ticket1
                            }
                            
                            if let ticket2 = inning2["ticket2"]  as? [[String : AnyObject]] {
                                self.secondInningTicket2List = ticket2
                            }
                            
                            if let winners = inning2["winners"]  as? [String : AnyObject] {
                                self.secondInningsWinners = winners
                            }
                        }
                        
                        if !self.ticketsList.isEmpty {
                            self.stopTimer()
                            self.startTimer()
                        }
                        
                        self.CVHousie.reloadData()
                    }
                    
                    if let secondInningStart = responseData["second_inning_start"]  as? String {
                        self.strSecondInningStart = secondInningStart
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func userPlayHousieQuiz(indexPath: IndexPath) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            var housieQuizEventId = ""
            var positionId = ""
            
            if self.isFirstInning {
                if self.isFirstTicket {
                    let dict = self.firstInningTicket1List[indexPath.item]
                    housieQuizEventId = "\(dict["housie_quiz_event_id"] ?? 0)"
                    positionId = "\(dict["position_id"] ?? 0)"
                }
                else {
                    let dict = self.firstInningTicket2List[indexPath.item]
                    housieQuizEventId = "\(dict["housie_quiz_event_id"] ?? 0)"
                    positionId = "\(dict["position_id"] ?? 0)"
                }
            }
            else {
                if self.isFirstTicket {
                    let dict = self.secondInningTicket1List[indexPath.item]
                    housieQuizEventId = "\(dict["housie_quiz_event_id"] ?? 0)"
                    positionId = "\(dict["position_id"] ?? 0)"
                }
                else {
                    let dict = self.secondInningTicket2List[indexPath.item]
                    housieQuizEventId = "\(dict["housie_quiz_event_id"] ?? 0)"
                    positionId = "\(dict["position_id"] ?? 0)"
                }
            }
            
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "match_id": self.matchId,
                         "match_inning_id": self.inningNumber,
                         "housie_quiz_event_id": housieQuizEventId,
                         "position_id": positionId,
                         "ticket_number": self.ticketNumber] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.USERPLAYHOUSIEQUIZ.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if self.isFirstInning {
                        if self.isFirstTicket {
                            var dict = self.firstInningTicket1List[indexPath.item]
                            dict["is_answered"] = kY
                            self.firstInningTicket1List[indexPath.item] = dict
                        }
                        else {
                            var dict = self.firstInningTicket2List[indexPath.item]
                            dict["is_answered"] = kY
                            self.firstInningTicket2List[indexPath.item] = dict
                        }
                    }
                    else{
                        if self.isFirstTicket {
                            var dict = self.secondInningTicket1List[indexPath.item]
                            dict["is_answered"] = kY
                            self.secondInningTicket1List[indexPath.item] = dict
                        }
                        else {
                            var dict = self.secondInningTicket2List[indexPath.item]
                            dict["is_answered"] = kY
                            self.secondInningTicket2List[indexPath.item] = dict
                        }
                    }
                    
                    self.CVHousie.reloadItems(at: [indexPath])
                    
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

class HousiePlayCell: UICollectionViewCell {
    @IBOutlet weak var bgView: CustomView!
    @IBOutlet weak var imgDot: UIImageView!
    @IBOutlet weak var imgHousie: UIImageView!
    @IBOutlet weak var imgParticipate: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}

class HousieFooterCell: UICollectionReusableView {
    @IBOutlet weak var btnFirstRow: UIButton!
    @IBOutlet weak var btnSecondRow: UIButton!
    @IBOutlet weak var btnThirdRow: UIButton!
    @IBOutlet weak var btnFabFourRow: UIButton!
    @IBOutlet weak var btnFullHousieRow: UIButton!
}
