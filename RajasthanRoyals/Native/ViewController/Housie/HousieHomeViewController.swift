//
//  HousieHomeViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 11/03/21.
//

import UIKit

class HousieHomeViewController: BaseViewController {
    
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var imgSponser: UIImageView!
    @IBOutlet weak var imgRoyalHousie: UIImageView!
   
    @IBOutlet weak var lblWelcome: UILabel!
    
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblDaysValue: UILabel!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblHoursValue: UILabel!
    @IBOutlet weak var lblMinutes: UILabel!
    @IBOutlet weak var lblMinutesValue: UILabel!
    @IBOutlet weak var lblSeconds: UILabel!
    @IBOutlet weak var lblSecondsValue: UILabel!
    @IBOutlet weak var lblOnlyForRR: UILabel!
    
    @IBOutlet weak var tblMatchList: UITableView!
    
    var currentPage = 1
    var nextPage = kN
    var matchList = [[String: AnyObject]]()
    var timer: Timer!
    var nextMatchDate: Date!
    
    var dictGenreral = [String: AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "HousieGameScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Housie Game")
        
        self.lblDays.text = self.getLocalizeTextForKey(keyName: "Home_lbl_Days")
        self.lblHours.text = self.getLocalizeTextForKey(keyName: "Home_lbl_Hours")
        self.lblMinutes.text = self.getLocalizeTextForKey(keyName: "Home_lbl_Minutes")
        self.lblSeconds.text = self.getLocalizeTextForKey(keyName: "Home_lbl_Seconds")
        
        self.getHousieGeneralList(isShowHud: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimer()
    }
    
    //Start Timer
    func startTimer() {
        if self.timer == nil {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimeValue), userInfo: nil, repeats: true)
        }
    }
    
    //Stop Timer
    func stopTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    //Update Time
    @objc func updateTimeValue() {
        if self.nextMatchDate != nil {
            let currentDate = Date().convertToSpacificTimeZone(timeZone: "Asia/Kolkata")
            let strTimer = self.nextMatchDate.offset(from: currentDate)
                
            if !strTimer.isEmpty {
                let strDay: String = "\((Int(strTimer)! % 31536000) / 86400)"
                let strHour: String = "\((Int(strTimer)! % 86400) / 3600)"
                let strMin: String = "\((Int(strTimer)! % 3600) / 60)"
                let strSec: String = "\(Int(strTimer)! % 60)"
                
                self.lblDaysValue.text = strDay
                self.lblHoursValue.text = strHour
                self.lblMinutesValue.text = strMin
                self.lblSecondsValue.text = strSec
            }
            else {
                self.lblDaysValue.text = "-"
                self.lblHoursValue.text = "-"
                self.lblMinutesValue.text = "-"
                self.lblSecondsValue.text = "-"
            }
        }
    }
    
    @IBAction func btnMatches_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "RoyalHousieFixtureListViewController") as! RoyalHousieFixtureListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension HousieHomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchList.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == matchList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getMatchList(isShowHud: false)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeMatchesTblCell") as! HomeMatchesTblCell
        
        if let imageUrl = matchList[indexPath.row][kFirstImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgViewTeamLeft.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let imageUrl = matchList[indexPath.row][kSecondImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgViewTeamRight.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let location = matchList[indexPath.row][kLocation] as? String {
            cell.lblAddress.text = location
        }
        
        if var strDateTime = matchList[indexPath.row][kDateTime] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            
            if let dateTime = dateFormatter.date(from: strDateTime) {
                dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
                strDateTime = dateFormatter.string(from: dateTime)
                cell.lblDateTime.text = strDateTime
            }
        }
        
        cell.imgFirstWinner.isHidden = true
        cell.imgSecondWinner.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "eventType": POINTSEVENTTYPE.HOUSE_PLAY.rawValue]
        self.callEarnPointsAPI(param: param)
        
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "HousiePlayViewController") as! HousiePlayViewController
        viewController.dictGenreral = self.dictGenreral
        viewController.matchDetails = self.matchList[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension HousieHomeViewController {
    @objc func getHousieGeneralList(isShowHud:Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["is_description": kNo]
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETHOUSIEQUIZEVENTGENERAL.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.dictGenreral = data
                        
                        if let image1 = data["image1"] as? String {
                            if let url = URL(string: image1) {
                                self.imgHeader.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder.png"))
                            }
                        }
                        if let image2 = data["image2"] as? String {
                            if let url = URL(string: image2) {
                                self.imgSponser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder.png"))
                            }
                        }
                        if let image3 = data["image3"] as? String {
                            if let url = URL(string: image3) {
                                self.imgRoyalHousie.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder.png"))
                            }
                        }
                        if let emptyMessage = data["empty_message"] as? String, !emptyMessage.isEmpty {
                            self.lblWelcome.attributedText = emptyMessage.html2Attributed
                        }
                        if let emptyMessage1 = data["empty_message_1"] as? String, !emptyMessage1.isEmpty {
                            self.lblOnlyForRR.attributedText = emptyMessage1.html2Attributed
                        }
                    }
                    
                    self.getMatchList(isShowHud: isShowHud)
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getMatchList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE] as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETHOUSIEMATCH.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.matchList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }

                    if let matchList = responseData[kData] as? [[String : AnyObject]] {
                        for match in matchList {
                            self.matchList.append(match)
                        }
                    }
                    
                    if self.matchList.count > 0 {
                        self.tblMatchList.isHidden = false
                        self.lblWelcome.isHidden = true
                        self.timerView.isHidden = true
                        self.lblOnlyForRR.isHidden = true
                    }
                    else {
                        self.tblMatchList.isHidden = true
                        self.lblWelcome.isHidden = false
                        self.timerView.isHidden = false
                        self.lblOnlyForRR.isHidden = false
                        
                        if let dateTime = responseData[kUpcomingMatchNew] as? String {
                            if dateTime != "" {
                                if let date = dateTime.toDate(dateFormat: "yyyy-MM-dd HH:mm") {
                                    self.nextMatchDate = date.toLocalTime()
                                }
                                self.startTimer()
                            }
                            else {
                                self.stopTimer()
                                
                                self.lblDaysValue.text = "-"
                                self.lblHoursValue.text = "-"
                                self.lblMinutesValue.text = "-"
                                self.lblSecondsValue.text = "-"
                            }
                        }
                    }
                    
                    self.tblMatchList.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
