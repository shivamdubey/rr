//
//  NewsVideosViewController.swift
//  RR
//
//  Created by Sagar on 23/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MomentsOfTheMatchViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var videoCV: UICollectionView!
    
    var currentPage = 1
    var nextPage = kN
    var videoList = [[String: AnyObject]]()
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "MomentsOfTheMatchScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_MomentsOfTheMatch")
        
        refreshControl = UIRefreshControl.init()
        refreshControl.tintColor = .white
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshCollectionView(sender:)), for: .valueChanged)
        videoCV.refreshControl = refreshControl
        
        self.getHighlightVideosList(isShowHud: true)
    }
    
    //UIRefreshControl method
    @objc func refreshCollectionView(sender: AnyObject) {
        self.currentPage = 1
        self.nextPage = kN
        self.getHighlightVideosList(isShowHud: false)
    }
}

extension MomentsOfTheMatchViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == videoList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getHighlightVideosList(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsCell
        
        if let imageUrl = videoList[indexPath.item][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if let title = videoList[indexPath.item][kTitle] as? String {
            cell.lblTitle.text = title
        }
        
        cell.imgPlay.isHidden = false
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "videoName": self.videoList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.VIDEOS.rawValue]
        self.callEarnPointsAPI(param: param)
        
        if let type = videoList[indexPath.row][kType] as? String {
            if type == VIDEOTYPE.LOCAL.rawValue {
                if let video = videoList[indexPath.row][kVideo] as? String {
                    if let videoURL = URL(string: video) {
                        let player = AVPlayer(url: videoURL)
                        let playerViewController = AVPlayerViewController()
                        playerViewController.player = player
                        self.present(playerViewController, animated: true) {
                            playerViewController.player!.play()
                        }
                    }
                }
            }
            else {
                if let videoUrl = videoList[indexPath.row][kVideo] as? String {
                    let array = videoUrl.components(separatedBy: "=")
                    if array.count > 1 {
                        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "YouTubePlayerViewController") as! YouTubePlayerViewController
                        viewController.videoId = array[1]
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
            }
        }
    }
}

extension MomentsOfTheMatchViewController {
    func getHighlightVideosList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE]
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETMATCHHIGHLIGHTS.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.videoList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let videosData = responseData[kData] as? [[String : AnyObject]] {
                        for videos in videosData {
                            self.videoList.append(videos)
                        }
                    }
                    
                    self.videoCV.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension MomentsOfTheMatchViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 1
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        
//        if indexPath.item == 0 {
//            return CGSize(width: widthPerItem * 2, height: widthPerItem * 1.25)
//        }
//        return CGSize(width: widthPerItem, height: widthPerItem)
        
        return CGSize(width: widthPerItem, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}
