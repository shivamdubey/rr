//
//  ReferFriendViewController.swift
//  RR
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 Sagar Nandha. All rights reserved.
//

import UIKit

class PlayerStatsViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnMostRuns: CustomButton!
    @IBOutlet weak var btnMostWickets: CustomButton!
    
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblTotalRuns: UILabel!
    @IBOutlet weak var lblNoOfRunsAndWickets: UILabel!
    
    @IBOutlet weak var tblStats: UITableView!
    
    var isMostRun = true
    var mostRuns = [[String: AnyObject]]()
    var mostWickets = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblNumber.text = self.lblNumber.text!.englishToHindiDigits
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        
        self.addFirebaseCustomeEvent(eventName: "RRPlayerStatsScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Players Stats")
        
        self.lblFirstName.rotate(degrees: -15)
        self.lblLastName.rotate(degrees: -15)
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_RRPlayerStats")
        self.btnMostRuns.setTitle(self.getLocalizeTextForKey(keyName: "PlayersStats_btn_MostRuns"), for: .normal)
        self.btnMostWickets.setTitle(self.getLocalizeTextForKey(keyName: "PlayersStats_btn_MostWickets"), for: .normal)
        self.lblTotalRuns.text = self.getLocalizeTextForKey(keyName: "PlayersStats_lbl_TotalRuns")
        
        self.getPlayerStats(isShowHud: true)
        self.btnMostRuns_Clicked(sender: self.btnMostRuns)
    }
    
    //Set First Player Details
    func setFirstPlayerDetails() {
        if isMostRun {
            if mostRuns.count > 0 {
                if let imageUrl = mostRuns[0][kImage] as? String {
                    if let url = URL(string: imageUrl) {
                        self.imgPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "playerplaceholder"))
                    }
                }
                
                if let firstName = mostRuns[0][kFirstName] as? String {
                    self.lblFirstName.text = firstName
                }
                
                if let lastName = mostRuns[0][kLastName] as? String {
                    self.lblLastName.text = lastName
                }
                
                if let totalRuns = mostRuns[0][kTotalRuns] as? Int {
                    self.lblNoOfRunsAndWickets.text = "\(totalRuns)".englishToHindiDigits
                }
            }
        }
        else {
            if mostWickets.count > 0 {
                if let imageUrl = mostWickets[0][kImage] as? String {
                    if let url = URL(string: imageUrl) {
                        self.imgPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "playerplaceholder"))
                    }
                }
                
                if let firstName = mostWickets[0][kFirstName] as? String {
                    self.lblFirstName.text = firstName
                }
                
                if let lastName = mostWickets[0][kLastName] as? String {
                    self.lblLastName.text = lastName
                }
                
                if let wickets = mostWickets[0][kWickets] as? Int {
                    self.lblNoOfRunsAndWickets.text = "\(wickets)".englishToHindiDigits
                }
            }
        }
    }
    
    @IBAction func btnMostRuns_Clicked(sender: UIButton) {
        self.isMostRun = true
        self.btnMostRuns.backgroundColor = SelectedButtonBGColor
        self.btnMostWickets.backgroundColor = DeSelectedButtonBGColor
        self.btnMostRuns.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnMostWickets.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.lblTotalRuns.text = self.getLocalizeTextForKey(keyName: "PlayersStats_lbl_TotalRuns")
        
        self.setFirstPlayerDetails()
        self.tblStats.reloadData()
    }
    
    @IBAction func btnMostWickets_Clicked(sender: UIButton) {
        self.isMostRun = false
        self.btnMostRuns.backgroundColor = DeSelectedButtonBGColor
        self.btnMostWickets.backgroundColor = SelectedButtonBGColor
        self.btnMostRuns.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnMostWickets.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.lblTotalRuns.text = self.getLocalizeTextForKey(keyName: "PlayersStats_lbl_Totalwickets")
        
        self.setFirstPlayerDetails()
        self.tblStats.reloadData()
    }
}

extension PlayerStatsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isMostRun {
            return mostRuns.count - 1
        }
        return mostWickets.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatsCell") as! StatsCell
        
        cell.lblNo.text = "0\(indexPath.row + 2)".englishToHindiDigits
        
        if isMostRun {
            if let fullName = mostRuns[indexPath.row + 1][kFullName] as? String {
                cell.lblName.text = fullName
            }
            
            if let totalRuns = mostRuns[indexPath.row + 1][kTotalRuns] as? Int {
                cell.lblNoOfRunsAndWickets.text = "\(totalRuns)".englishToHindiDigits
            }
        }
        else {
            if let fullName = mostWickets[indexPath.row + 1][kFullName] as? String {
                cell.lblName.text = fullName
            }
            
            if let wickets = mostWickets[indexPath.row + 1][kWickets] as? Int {
                cell.lblNoOfRunsAndWickets.text = "\(wickets)".englishToHindiDigits
            }
        }
        
        return cell
    }
}

class StatsCell: UITableViewCell {
    @IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNoOfRunsAndWickets: UILabel!
}

extension PlayerStatsViewController {
    func getPlayerStats(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETPLAYERSTATS.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        if let mostRun = data["most_run"] as? [[String : AnyObject]] {
                            self.mostRuns = mostRun
                        }
                        
                        if let mostWickets = data["most_wickets"] as? [[String : AnyObject]] {
                            self.mostWickets = mostWickets
                        }
                    }
                    
                    self.setFirstPlayerDetails()
                    self.tblStats.reloadData()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
