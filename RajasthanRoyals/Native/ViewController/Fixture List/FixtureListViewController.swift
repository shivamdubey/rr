//
//  CalendarFixtureViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 12/03/21.
//

import UIKit
import FSCalendar_Persian
import Kingfisher
import Alamofire
import KRProgressHUD

class FixtureListViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var calenderView: FSCalendar!
    @IBOutlet weak var matchCollectionView: UICollectionView!
    
    @IBOutlet weak var btnAllFixture: UIButton!
    
    var currentPage = 1
    var nextPage = kN
    var matchList = [[String: AnyObject]]()
    var datesWithEvent = [String]()
    var eventImages = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.calenderView.locale = Locale.init(identifier: BaseViewController.sharedInstance.appLanguage)
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_RRFixtureList")
        self.btnAllFixture.setTitle(self.getLocalizeTextForKey(keyName: "Fixture_btn_ViewFullFixture"), for: .normal)
        
        if let layout = self.matchCollectionView.collectionViewLayout as? UPCarouselFlowLayout {
            layout.itemSize = CGSize(width: SCREEN_WIDTH - 30, height: self.matchCollectionView.frame.size.height)
            layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 5)
        }
        
        self.calenderView.appearance.eventDefaultColor = UIColor.init(named: "Pink_Color")
        self.calenderView.appearance.eventSelectionColor = UIColor.init(named: "Pink_Color")
        
        self.getMatchList(isShowHud: true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            self.btnAllFixture.roundCorners([.topLeft,.topRight], radius: 25)
        }
    }
    
    @IBAction func btnAllFixture_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AllFixtureListViewController") as! AllFixtureListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension FixtureListViewController: FSCalendarDelegate, FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = dateFormateForSend
//        let dateString = dateFormatter.string(from: date)
//
//        if self.datesWithEvent.contains(dateString) {
//            //Find the index
//            let array = self.datesWithEvent as Array
//            let matchIndex = array.firstIndex {
//                if $0 == dateString {
//                    return true
//                }
//                return false
//            }
//
//            if matchIndex != nil {
//                if self.teamShortName.count > matchIndex! {
//                    return UIImage.init(named: self.teamShortName[matchIndex!].lowercased())
//                }
//            }
//        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormateForSend
        let dateString = dateFormatter.string(from: date)

        if self.datesWithEvent.contains(dateString) {
            return 1
        }
        return 0
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormateForSend
        let dateString = dateFormatter.string(from: date)
        
        for matchDetails in self.matchList {
            let date_time = self.convertStringToDate(format: "yyyy-MM-dd HH:mm", strDate: matchDetails[kDateTime] as? String ?? "")
            let dateTime = self.convertDateToString(format: dateFormateForSend, date: date_time)
            
            if dateTime == dateString {
                //Find the index
                let array = self.datesWithEvent as Array
                let matchIndex = array.firstIndex {
                    if $0 == dateString {
                        return true
                    }
                    return false
                }
                
                if matchIndex != nil {
                    self.matchCollectionView.scrollToItem(at: IndexPath.init(item: matchIndex!, section: 0), at: .centeredHorizontally, animated: true)
                }
            }
        }
    }
}

extension FixtureListViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return matchList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == matchList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getMatchList(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeMatchesCell", for: indexPath) as! HomeMatchesCell
        
        if let imageUrl = matchList[indexPath.row][kFirstImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgViewTeamLeft.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let imageUrl = matchList[indexPath.row][kSecondImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgViewTeamRight.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let location = matchList[indexPath.row][kLocation] as? String {
            cell.lblAddress.text = location
        }
        
        if var strDateTime = matchList[indexPath.row][kDateTime] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            
            if let dateTime = dateFormatter.date(from: strDateTime) {
                dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
                strDateTime = dateFormatter.string(from: dateTime)
                cell.lblDateTime.text = strDateTime
            }
        }
        
        cell.imgFirstWinner.isHidden = true
        cell.imgSecondWinner.isHidden = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if matchList[indexPath.row][kMatchStatus] as? String == MATCHSTATUS.LIVE.rawValue {
            let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "LiveScoreViewController") as! LiveScoreViewController
            viewController.matchDetails = matchList[indexPath.item]
            viewController.isFromLive = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.matchCollectionView.contentOffset
        visibleRect.size = self.matchCollectionView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.matchCollectionView.indexPathForItem(at: visiblePoint) else { return }
        
        let date_time = self.convertStringToDate(format: "yyyy-MM-dd HH:mm", strDate: self.matchList[indexPath.item][kDateTime] as? String ?? "")
        self.calenderView.select(date_time)
    }
}

extension FixtureListViewController {
    func getMatchList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE] as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETRRALLMATCH.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.matchList.removeAll()
                        self.datesWithEvent.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }

                    if let matchList = responseData[kData] as? [[String : AnyObject]] {
                        for match in matchList {
                            self.matchList.append(match)
                        }
                    }
                    
                    var showFirstDateCalenderDate = false
                    for matchDetails in self.matchList {
                        let date_time = self.convertStringToDate(format: "yyyy-MM-dd HH:mm", strDate: matchDetails[kDateTime] as? String ?? "")
                        let dateTime = self.convertDateToString(format: dateFormateForSend, date: date_time)
                        self.datesWithEvent.append(dateTime)
                        
                        if showFirstDateCalenderDate == false {
                            showFirstDateCalenderDate = true
                            self.calenderView.select(date_time)
                        }
                    }
                    
                    self.calenderView.reloadData()
                    self.matchCollectionView.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
