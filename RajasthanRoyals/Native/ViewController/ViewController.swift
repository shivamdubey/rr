//
//  ReferFriendViewController.swift
//  CV Builder
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 AmitMacBook. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("ViewController")
        
        BaseViewController.sharedInstance.appDelegate.startApplication()
        
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
        let navigationController: UINavigationController = UINavigationController.init(rootViewController: viewController)
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.isHidden = true
        
        if BaseViewController.sharedInstance.appDelegate.window == nil && UIApplication.shared.windows.count > 0 {
           BaseViewController.sharedInstance.appDelegate.window = UIApplication.shared.windows[0]
        }
        
        BaseViewController.sharedInstance.appDelegate.window?.rootViewController = navigationController
    }
}
