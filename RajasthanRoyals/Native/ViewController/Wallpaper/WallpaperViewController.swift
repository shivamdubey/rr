//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class WallpaperViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var wallpaperCV: UICollectionView!
    @IBOutlet weak var pageControl: FlexiblePageControl!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var btnShare: UIButton!

    var isShowBack = false
    var currentPage = 1
    var nextPage = kN
    var wallpaperList = [[String: AnyObject]]()
    var visibleIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnMenu.isHidden = self.isShowBack
        self.btnBack.isHidden = !self.isShowBack
        
        self.btnDownload.isHidden = true
        self.btnShare.isHidden = true
        
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Wallpapers")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Wallpaper")
        
        self.addFirebaseCustomeEvent(eventName: "WallpaperScreen")
        
        self.pageControl.pageIndicatorTintColor = UIColor.white.withAlphaComponent(0.5)
        self.pageControl.currentPageIndicatorTintColor = UIColor.white
        
        self.setupLayout()
        
        self.getWallpaperList(isShowHud: true)
    }
    
    fileprivate func setupLayout() {
        if let layout = self.wallpaperCV.collectionViewLayout as? UPCarouselFlowLayout {
            layout.itemSize = CGSize(width: self.wallpaperCV.frame.size.width, height: self.wallpaperCV.frame.size.height)
            layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 0)
        }
    }
    
    @IBAction func btnShare_Clicked(_ sender: UIButton) {
        let indexPath = IndexPath.init(row: visibleIndex, section: 0)
        if let cell = wallpaperCV.cellForItem(at: indexPath) as? WallpaperCell {
            if let image = cell.imgWallpaper.image {
                let objectsToShare: [AnyObject] = [image]
                let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = sender
                self.present(activityViewController, animated: true, completion: nil)
                
                self.callTMAnalyticsAPI(category: "Wallaper", action: "Share", label: "\(self.wallpaperList[self.visibleIndex][kTitle] as? String ?? "")")
            }
        }
    }
    
    @IBAction func btnDownload_Clicked(_ sender: Any) {
        let indexPath = IndexPath.init(row: visibleIndex, section: 0)
        if let cell = wallpaperCV.cellForItem(at: indexPath) as? WallpaperCell {
            if let image = cell.imgWallpaper.image {
                self.callTMAnalyticsAPI(category: "Wallaper", action: "Download", label: "\(self.wallpaperList[self.visibleIndex][kTitle] as? String ?? "")")
                
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            print(error.localizedDescription)
        }
        else {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_WallpaperDownoad"))
        }
    }
}

extension WallpaperViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return wallpaperList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == wallpaperList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getWallpaperList(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WallpaperCell", for: indexPath) as! WallpaperCell
        
        if let imageUrl = wallpaperList[indexPath.item][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgWallpaper.kf.setImage(with: url)
            }
        }
        
        return cell
    }
}

extension WallpaperViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.wallpaperCV.contentOffset
        visibleRect.size = self.wallpaperCV.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.wallpaperCV.indexPathForItem(at: visiblePoint) else { return }
        self.visibleIndex = indexPath.item
        self.pageControl.setCurrentPage(at: indexPath.item)
    }
}

class WallpaperCell: UICollectionViewCell {
    @IBOutlet weak var imgWallpaper: UIImageView!
}

extension WallpaperViewController {
    func getWallpaperList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETPWALLPAPER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.wallpaperList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let players = responseData[kData] as? [[String : AnyObject]] {
                        for player in players {
                            self.wallpaperList.append(player)
                        }
                    }
                    
                    self.wallpaperCV.reloadData()
                    
                    self.pageControl.numberOfPages = self.wallpaperList.count
                    
                    if self.wallpaperList.count > 0 {
                        self.btnDownload.isHidden = false
                        self.btnShare.isHidden = false
                    }
                    else {
                        self.btnDownload.isHidden = true
                        self.btnShare.isHidden = true
                    }
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
