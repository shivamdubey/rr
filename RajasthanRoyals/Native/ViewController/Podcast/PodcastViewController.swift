//
//  ReferFriendViewController.swift
//  RR
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 Sagar Nandha. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PodcastViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblPodcast: UITableView!
    
    var currentPage = 1
    var nextPage = kN
    var podcastList = [[String: AnyObject]]()
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "PodcastScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Podcast")
        
        refreshControl = UIRefreshControl.init()
        refreshControl.tintColor = .white
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshTableView(sender:)), for: .valueChanged)
        tblPodcast.refreshControl = refreshControl
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Podcast")
        
        self.getPodcastList(isShowHud: true)
    }
    
    //UIRefreshControl method
    @objc func refreshTableView(sender: AnyObject) {
        self.currentPage = 1
        self.nextPage = kN
        self.getPodcastList(isShowHud: false)
    }
}

extension PodcastViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return podcastList.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == podcastList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getPodcastList(isShowHud: false)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == podcastList.count - 1 && nextPage == kY {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.clear
            
            let activityIndicator = UIActivityIndicatorView(style: .white)
            activityIndicator.color = UIColor().alertButtonColor
            activityIndicator.center = CGPoint(x: tableView.frame.size.width / 2, y: 20)
            
            cell.addSubview(activityIndicator)
            
            activityIndicator.startAnimating()
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastCell") as! PodcastCell
        
        if let imageUrl = podcastList[indexPath.row][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgPodcast.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let title = podcastList[indexPath.row][kTitle] as? String {
            cell.lblTitle.text = title
        }
        cell.btnShare .setTitle("", for: .normal)
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action: #selector(btnShareSocial_Clicked(_:)), for: .touchUpInside)
        
        cell.viewCustomPodBg.layer.borderWidth = 3
        cell.viewCustomPodBg.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
        cell.viewCustomPodBg.layer.cornerRadius = 5
//        if let players = podcastList[indexPath.row][kPlayers] as? [String: AnyObject] {
//            if let fullName = players[kFullName] as? String {
//                cell.lblOwner.text = fullName == "" ? "\(AlertViewTitle)" : "\(fullName)"
//            }
//            else {
//                cell.lblOwner.text = "\(AlertViewTitle)"
//            }
//        }
//        else {
//            cell.lblOwner.text = "\(AlertViewTitle)"
//        }
        
        cell.lblOwner.text = ""
        cell.lblDuration.text = ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        BaseViewController.sharedInstance.appDelegate.podcastList.removeAll()
        BaseViewController.sharedInstance.appDelegate.podcastIndex = 0
        BaseViewController.sharedInstance.appDelegate.podcastList = self.podcastList
        BaseViewController.sharedInstance.appDelegate.podcastIndex = indexPath.row
        
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "NewPodcastPlayViewController") as! NewPodcastPlayViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    @objc func btnShareSocial_Clicked(_ sender: UIButton) {
       
   
        let slug = self.podcastList[sender.tag][kSlug] as? String ?? ""
        let url = URL.init(string: "https://www.rajasthanroyals.com/latest-news/\(slug)")!
        let objectsToShare: [Any] = [url]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender
        self.present(activityViewController, animated: true, completion: nil)
        
        self.callTMAnalyticsAPI(category: "Podcast", action: "Share", label: self.podcastList[sender.tag][kTitle] as? String ?? "")
            }
        
    
}

class PodcastCell: UITableViewCell {
    @IBOutlet weak var imgPodcast: CustomImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOwner: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var viewCustomPodBg: UIView!
}

extension PodcastViewController {
    func getPodcastList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: 50]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETPODCAST.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.podcastList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let players = responseData[kData] as? [[String : AnyObject]] {
                        for player in players {
                            self.podcastList.append(player)
                        }
                    }
                    
                    self.tblPodcast.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
