//
//  ViewController.swift
//  Jack Parsons
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 Sagar's MacBook Air. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Foundation
import MediaPlayer

class SinglePodcastPlayViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgPodcast: CustomImageView!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var lblPodcastTitle: UILabel!
    @IBOutlet weak var lblPodcastDescription: UILabel!
    @IBOutlet weak var lblCurrentDuration: UILabel!
    @IBOutlet weak var lblTotalDuration: UILabel!
    @IBOutlet weak var podcastSlider: CustomSlider!
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    
    var podcastId = 0
    var podcastDetails = [String: AnyObject]()
    var isScrubbing = false
    var lastLoadFailed = false
    var isFirstTime = true
    
    var audioPlayer = QueuedAudioPlayer.init()
    let audioSessionController = AudioSessionController.shared
    var podcastList = [AudioItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
            try AVAudioSession.sharedInstance().setActive(true)
        }
        catch {
            print(error.localizedDescription)
        }
        
        self.addFirebaseCustomeEvent(eventName: "PodcastPlayScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Podcast")
        
        self.podcastSlider.setThumbImage(#imageLiteral(resourceName: "slider_thum"), for: .normal)
        
        self.getPodcastDetails()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.audioPlayer.stop()
    }
    
    @IBAction func btnPlayPause_Clicked(_ sender: UIButton) {
        if !self.audioSessionController.audioSessionIsActive {
            try? self.audioSessionController.activateSession()
        }
        if lastLoadFailed, let item = self.audioPlayer.currentItem {
            lastLoadFailed = false
            try? self.audioPlayer.load(item: item, playWhenReady: true)
        }
        else {
            self.audioPlayer.togglePlaying()
        }
    }
    
    @IBAction func startScrubbing(_ sender: UISlider) {
        isScrubbing = true
    }
    
    @IBAction func scrubbing(_ sender: UISlider) {
        self.audioPlayer.seek(to: Double(podcastSlider.value))
    }
    
    @IBAction func scrubbingValueChanged(_ sender: UISlider) {
        let value = Double(podcastSlider.value)
        lblCurrentDuration.text = value.secondsToString()
        lblTotalDuration.text = (self.audioPlayer.duration - value).secondsToString()
    }
}

extension SinglePodcastPlayViewController {
    func setPodcastDetails() {
        self.callTMAnalyticsAPI(category: "Podcast", action: "Listen", label: self.podcastDetails[kTitle] as? String ?? "")
        
        let controller = RemoteCommandController()
        self.audioPlayer = QueuedAudioPlayer(remoteCommandController: controller)
        audioPlayer.remoteCommands = [
            .stop,
            .play,
            .pause,
            .togglePlayPause,
            .next,
            .previous,
            .changePlaybackPosition
        ]
        try? audioSessionController.set(category: .playback)
        
        if let url = URL.init(string: self.podcastDetails[kImage] as? String ?? "") {
            let imgPhoto = UIImageView()
            imgPhoto.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"), options: nil, progressBlock: nil) { (image, error, type, url) in
                if let downloadImage = image {
                    var podcastTitle = ""
                    var podcastArtis = ""
                    var podcastUrl = ""
                    
                    if let title = self.podcastDetails[kTitle] as? String {
                        podcastTitle = title
                    }
                    
                    if let players = self.podcastDetails[kPlayers] as? [String: AnyObject] {
                        if let fullName = players[kFullName] as? String {
                            podcastArtis = fullName == "" ? "by \(AlertViewTitle)" : "by \(fullName)"
                        }
                        else {
                            podcastArtis = "by \(AlertViewTitle)"
                        }
                    }
                    else {
                        podcastArtis = "by \(AlertViewTitle)"
                    }
                    
                    if let audioUrlStr = self.podcastDetails[kAudioFile] as? String {
                        podcastUrl = audioUrlStr
                    }
                    
                    let audioItem = DefaultAudioItem(audioUrl: podcastUrl, artist: podcastArtis, title: podcastTitle, sourceType: .stream, artwork: downloadImage)
                    
                    self.podcastList.append(audioItem)
                    
                    try? self.audioPlayer.add(items: self.podcastList, playWhenReady: false)
                    
                    self.audioPlayer.event.stateChange.addListener(self, self.handleAudioPlayerStateChange)
                    self.audioPlayer.event.secondElapse.addListener(self, self.handleAudioPlayerSecondElapsed)
                    self.audioPlayer.event.seek.addListener(self, self.handleAudioPlayerDidSeek)
                    self.audioPlayer.event.updateDuration.addListener(self, self.handleAudioPlayerUpdateDuration)
                    self.audioPlayer.event.didRecreateAVPlayer.addListener(self, self.handleAVPlayerRecreated)
                    self.audioPlayer.event.fail.addListener(self, self.handlePlayerFailure)
                    self.updateMetaData()
                    self.handleAudioPlayerStateChange(data: self.audioPlayer.playerState)
                }
            }
        }
    }
    
    func updateTimeValues() {
        self.podcastSlider.maximumValue = Float(self.audioPlayer.duration)
        self.podcastSlider.setValue(Float(self.audioPlayer.currentTime), animated: true)
        self.lblCurrentDuration.text = self.audioPlayer.currentTime.secondsToString()
        self.lblTotalDuration.text = (self.audioPlayer.duration - self.audioPlayer.currentTime).secondsToString()
    }
    
    func updateMetaData() {
        if let item = self.audioPlayer.currentItem {
            self.lblPodcastTitle.text = item.getTitle()
            self.lblPodcastDescription.text = item.getArtist()
            
            if let url = URL(string: item.getAlbumTitle() ?? "") {
                self.imgPodcast.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
    }
    
    func setPlayButtonState(forAudioPlayerState state: AudioPlayerState) {
        if state == .playing {
            self.btnPlayPause.isSelected = true
        }
        else {
            self.btnPlayPause.isSelected = false
        }
    }
    
    // MARK: - AudioPlayer Event Handlers
    func handleAudioPlayerStateChange(data: AudioPlayer.StateChangeEventData) {
        print(data)
        DispatchQueue.main.async {
            self.setPlayButtonState(forAudioPlayerState: data)
            switch data {
            case .loading:
                self.loadIndicator.startAnimating()
                self.updateMetaData()
                self.updateTimeValues()
            case .buffering:
                self.loadIndicator.startAnimating()
            case .ready:
                self.loadIndicator.stopAnimating()
                self.updateMetaData()
                self.updateTimeValues()
                if self.isFirstTime {
                    self.isFirstTime = false
                    self.btnPlayPause_Clicked(self.btnPlayPause)
                }
            case .playing, .paused, .idle:
                self.loadIndicator.stopAnimating()
                self.updateTimeValues()
            }
        }
    }
    
    func handleAudioPlayerSecondElapsed(data: AudioPlayer.SecondElapseEventData) {
        if !isScrubbing {
            DispatchQueue.main.async {
                self.updateTimeValues()
            }
        }
    }
    
    func handleAudioPlayerDidSeek(data: AudioPlayer.SeekEventData) {
        isScrubbing = false
    }
    
    func handleAudioPlayerUpdateDuration(data: AudioPlayer.UpdateDurationEventData) {
        DispatchQueue.main.async {
            self.updateTimeValues()
        }
    }
    
    func handleAVPlayerRecreated() {
        try? self.audioSessionController.set(category: .playback)
    }
    
    func handlePlayerFailure(data: AudioPlayer.FailEventData) {
        if let error = data as NSError? {
            if error.code == -1009 {
                lastLoadFailed = true
            }
        }
    }
}

extension SinglePodcastPlayViewController {
    func getPodcastDetails() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["podcast_id": self.podcastId]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETPODCASTDETAIL.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.podcastDetails = data
                        self.setPodcastDetails()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
