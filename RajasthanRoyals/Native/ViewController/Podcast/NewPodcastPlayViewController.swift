//
//  ViewController.swift
//  Jack Parsons
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 Sagar's MacBook Air. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Foundation
import MediaPlayer

class NewPodcastPlayViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgPodcast: CustomImageView!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblPodcastTitle: UILabel!
    @IBOutlet weak var lblPodcastDescription: UILabel!
    @IBOutlet weak var lblCurrentDuration: UILabel!
    @IBOutlet weak var lblTotalDuration: UILabel!
    @IBOutlet weak var podcastSlider: CustomSlider!
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    
    var isScrubbing = false
    var lastLoadFailed = false
    var isFirstTime = true
    
    var audioPlayer = QueuedAudioPlayer.init()
    let audioSessionController = AudioSessionController.shared
    var podcastList = [AudioItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
            try AVAudioSession.sharedInstance().setActive(true)
        }
        catch {
            print(error.localizedDescription)
        }
        
        self.addFirebaseCustomeEvent(eventName: "PodcastPlayScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Podcast")
        
        self.podcastSlider.setThumbImage(#imageLiteral(resourceName: "slider_thum"), for: .normal)
        
        if BaseViewController.sharedInstance.appDelegate.podcastList.count == 0 {
            self.getPodcastList(isShowHud: true)
        }
        else {
            self.setPodcastDetails()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.audioPlayer.stop()
    }
    
    @IBAction func btnPrevious_Clicked(_ sender: Any) {
        if BaseViewController.sharedInstance.appDelegate.podcastIndex != 0 {
            BaseViewController.sharedInstance.appDelegate.podcastIndex = BaseViewController.sharedInstance.appDelegate.podcastIndex - 1
            try? self.audioPlayer.previous()
            
            self.callTMAnalyticsAPI(category: "Podcast", action: "Listen", label: "\(BaseViewController.sharedInstance.appDelegate.podcastList[BaseViewController.sharedInstance.appDelegate.podcastIndex][kTitle] as? String ?? "")")
            
            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "podcastTitle": BaseViewController.sharedInstance.appDelegate.podcastList[BaseViewController.sharedInstance.appDelegate.podcastIndex][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.PODCAST.rawValue]
            self.callEarnPointsAPI(param: param)
        }
    }
    
    @IBAction func btnNext_Clicked(_ sender: Any) {
        if BaseViewController.sharedInstance.appDelegate.podcastIndex != BaseViewController.sharedInstance.appDelegate.podcastList.count {
            BaseViewController.sharedInstance.appDelegate.podcastIndex = BaseViewController.sharedInstance.appDelegate.podcastIndex + 1
            try? self.audioPlayer.next()
            
            self.callTMAnalyticsAPI(category: "Podcast", action: "Listen", label: "\(BaseViewController.sharedInstance.appDelegate.podcastList[BaseViewController.sharedInstance.appDelegate.podcastIndex][kTitle] as? String ?? "")")
            
            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "podcastTitle": BaseViewController.sharedInstance.appDelegate.podcastList[BaseViewController.sharedInstance.appDelegate.podcastIndex][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.PODCAST.rawValue]
            self.callEarnPointsAPI(param: param)
        }
    }
    
    @IBAction func btnPlayPause_Clicked(_ sender: Any) {
        if !self.audioSessionController.audioSessionIsActive {
            try? self.audioSessionController.activateSession()
        }
        if lastLoadFailed, let item = self.audioPlayer.currentItem {
            lastLoadFailed = false
            try? self.audioPlayer.load(item: item, playWhenReady: true)
        }
        else {
            self.audioPlayer.togglePlaying()
        }
    }
    
    @IBAction func startScrubbing(_ sender: UISlider) {
        isScrubbing = true
    }
    
    @IBAction func scrubbing(_ sender: UISlider) {
        self.audioPlayer.seek(to: Double(podcastSlider.value))
    }
    
    @IBAction func scrubbingValueChanged(_ sender: UISlider) {
        let value = Double(podcastSlider.value)
        lblCurrentDuration.text = value.secondsToString()
        lblTotalDuration.text = (self.audioPlayer.duration - value).secondsToString()
    }
}

extension NewPodcastPlayViewController {
    func setPodcastDetails() {
        self.callTMAnalyticsAPI(category: "Podcast", action: "Listen", label: "\(BaseViewController.sharedInstance.appDelegate.podcastList[BaseViewController.sharedInstance.appDelegate.podcastIndex][kTitle] as? String ?? "")")
        
        let controller = RemoteCommandController()
        self.audioPlayer = QueuedAudioPlayer(remoteCommandController: controller)
        audioPlayer.remoteCommands = [
            .stop,
            .play,
            .pause,
            .togglePlayPause,
            .next,
            .previous,
            .changePlaybackPosition
        ]
        try? audioSessionController.set(category: .playback)
        
        for podcastDetails in BaseViewController.sharedInstance.appDelegate.podcastList {
            if let url = URL.init(string: podcastDetails[kImage] as? String ?? "") {
                let imgPhoto = UIImageView()
                imgPhoto.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"), options: nil, progressBlock: nil) { (image, error, type, url) in
                    if let downloadImage = image {
                        var podcastTitle = ""
                        var podcastArtis = ""
                        var podcastUrl = ""
                        
                        if let title = podcastDetails[kTitle] as? String {
                            podcastTitle = title
                        }
                        
                        if let players = podcastDetails[kPlayers] as? [String: AnyObject] {
                            if let fullName = players[kFullName] as? String {
                                podcastArtis = fullName == "" ? "by \(AlertViewTitle)" : "by \(fullName)"
                            }
                            else {
                                podcastArtis = "by \(AlertViewTitle)"
                            }
                        }
                        else {
                            podcastArtis = "by \(AlertViewTitle)"
                        }
                        
                        if let audioUrlStr = podcastDetails[kAudioFile] as? String {
                            podcastUrl = audioUrlStr
                        }
                        
                        let audioItem = DefaultAudioItem(audioUrl: podcastUrl, artist: podcastArtis, title: podcastTitle, sourceType: .stream, artwork: downloadImage)
                        
                        self.podcastList.append(audioItem)
                    }
                }
            }
        }
        try? self.audioPlayer.add(items: self.podcastList, playWhenReady: false)
        
        self.audioPlayer.event.stateChange.addListener(self, self.handleAudioPlayerStateChange)
        self.audioPlayer.event.secondElapse.addListener(self, self.handleAudioPlayerSecondElapsed)
        self.audioPlayer.event.seek.addListener(self, self.handleAudioPlayerDidSeek)
        self.audioPlayer.event.updateDuration.addListener(self, self.handleAudioPlayerUpdateDuration)
        self.audioPlayer.event.didRecreateAVPlayer.addListener(self, self.handleAVPlayerRecreated)
        self.audioPlayer.event.fail.addListener(self, self.handlePlayerFailure)
        self.updateMetaData()
        self.handleAudioPlayerStateChange(data: self.audioPlayer.playerState)
    }
    
    func updateTimeValues() {
        self.podcastSlider.maximumValue = Float(self.audioPlayer.duration)
        self.podcastSlider.setValue(Float(self.audioPlayer.currentTime), animated: true)
        self.lblCurrentDuration.text = self.audioPlayer.currentTime.secondsToString()
        self.lblTotalDuration.text = (self.audioPlayer.duration - self.audioPlayer.currentTime).secondsToString()
    }
    
    func updateMetaData() {
        if BaseViewController.sharedInstance.appDelegate.podcastList.count == 1 {
            self.btnNext.isHidden = true
            self.btnPrevious.isHidden = true
        }
        else if BaseViewController.sharedInstance.appDelegate.podcastIndex == 0 {
            self.btnNext.isHidden = false
            self.btnPrevious.isHidden = true
        }
        else if BaseViewController.sharedInstance.appDelegate.podcastIndex == BaseViewController.sharedInstance.appDelegate.podcastList.count - 1 {
            self.btnNext.isHidden = true
            self.btnPrevious.isHidden = false
        }
        else {
            self.btnNext.isHidden = false
            self.btnPrevious.isHidden = false
        }
        
        if let item = self.audioPlayer.currentItem {
            self.lblPodcastTitle.text = item.getTitle()
            self.lblPodcastDescription.text = item.getArtist()
            
            item.getArtwork({ image in
                self.imgPodcast.image = image
            })
        }
    }
    
    func setPlayButtonState(forAudioPlayerState state: AudioPlayerState) {
        if state == .playing {
            self.btnPlayPause.isSelected = true
        }
        else {
            self.btnPlayPause.isSelected = false
        }
    }
    
    // MARK: - AudioPlayer Event Handlers
    func handleAudioPlayerStateChange(data: AudioPlayer.StateChangeEventData) {
        DispatchQueue.main.async {
            self.setPlayButtonState(forAudioPlayerState: data)
            switch data {
            case .loading:
                self.loadIndicator.startAnimating()
                self.updateMetaData()
                self.updateTimeValues()
            case .buffering:
                self.loadIndicator.startAnimating()
            case .ready:
                self.loadIndicator.stopAnimating()
                self.updateMetaData()
                self.updateTimeValues()
                if self.isFirstTime {
                    self.isFirstTime = false
                    self.btnPlayPause_Clicked(self.btnPlayPause)
                }
            case .playing, .paused, .idle:
                self.loadIndicator.stopAnimating()
                self.updateTimeValues()
            }
        }
    }
    
    func handleAudioPlayerSecondElapsed(data: AudioPlayer.SecondElapseEventData) {
        if !isScrubbing {
            DispatchQueue.main.async {
                self.updateTimeValues()
            }
        }
    }
    
    func handleAudioPlayerDidSeek(data: AudioPlayer.SeekEventData) {
        isScrubbing = false
    }
    
    func handleAudioPlayerUpdateDuration(data: AudioPlayer.UpdateDurationEventData) {
        DispatchQueue.main.async {
            self.updateTimeValues()
        }
    }
    
    func handleAVPlayerRecreated() {
        try? self.audioSessionController.set(category: .playback)
    }
    
    func handlePlayerFailure(data: AudioPlayer.FailEventData) {
        if let error = data as NSError? {
            if error.code == -1009 {
                lastLoadFailed = true
            }
        }
    }
}

extension NewPodcastPlayViewController {
    func getPodcastList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: 1, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETPODCAST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        BaseViewController.sharedInstance.appDelegate.podcastList = data
                        self.setPodcastDetails()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

