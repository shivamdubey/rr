//
//  ReferFriendViewController.swift
//  RR
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 Sagar Nandha. All rights reserved.
//

import UIKit

class ReferFriendViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnInviteNow: CustomButton!
    @IBOutlet weak var btnBack: CustomButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "ReferFriendScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Refer a Friend")
        //self.btnBack.setTitle("", for: .normal)
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "RightMenu_lbl_ReferFriend")
        self.lblMessage.text = self.getLocalizeTextForKey(keyName: "ReferAFriend_lbl_Message")
        self.btnInviteNow.setTitle(self.getLocalizeTextForKey(keyName: "btn_InviteNow"), for: .normal)
    }
    @IBAction func btnBackPRessed(_ sender: Any)
    {
        self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnInviteNow_Clicked(_ sender: Any) {
        if let appUrl = URL.init(string: "https://bit.ly/3hyuLYs") {
            let textToShare = "The Rajasthan Royals app has some cool games, exclusive behind-the-scenes content & much more. Download now by clicking the link.\n" + "\(appUrl)"
            let objectsToShare = [textToShare] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.present(activityVC, animated: true, completion: nil)
        }
        else {
            let textToShare = "The Rajasthan Royals app has some cool games, exclusive behind-the-scenes content & much more. Download now by clicking the link."
            let objectsToShare = [textToShare] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}
