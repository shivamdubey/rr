//
//  LoginViewController.swift
//  Demo
//
//  Created by Hardy on 3/2/19.
//  Copyright © 2019 Sagar Nandha. All rights reserved.
//

import UIKit

class PhotosViewController: BaseViewController, EFImageViewZoomDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imageCV: UICollectionView!
    
    var slug = ""
    var currentPage = 1
    var nextPage = kN
    var imagesList = [[String: AnyObject]]()
    var visibleIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "lbl_Photo")
        
        self.setupLayout()
        
        self.lblDescription.text = imagesList[self.visibleIndex]["media_title"] as? String ?? ""
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            self.imageCV.scrollToItem(at: IndexPath.init(item: self.visibleIndex, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)
        }
    }
    
    fileprivate func setupLayout() {
        if let layout = self.imageCV.collectionViewLayout as? UPCarouselFlowLayout {
            layout.itemSize = CGSize(width: self.imageCV.frame.size.width, height: self.imageCV.frame.size.height)
            layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 0)
        }
    }
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnShare_Clicked(_ sender: Any) {
        let indexPath = IndexPath.init(item: self.visibleIndex, section: 0)
        if let cell = imageCV.cellForItem(at: indexPath) as? PhotoCell {
            if let image = cell.imgGallery.image {
                let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
                
                let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "galleryName": self.slug, "photoName": self.imagesList[indexPath.row][kTitle] as? String ?? "", "eventType": POINTSEVENTTYPE.GALLERY_SHARE.rawValue]
                self.callEarnPointsAPI(param: param)
            }
        }
    }
    
    func showHelperCircle() {
        let center = CGPoint(x: view.bounds.width * 0.5, y: 100)
        let small = CGSize(width: 30, height: 30)
        let circle = UIView(frame: CGRect(origin: center, size: small))
        circle.layer.cornerRadius = circle.frame.width/2
        circle.backgroundColor = UIColor.white
        circle.layer.shadowOpacity = 0.8
        circle.layer.shadowOffset = CGSize()
        view.addSubview(circle)

        UIView.animate(withDuration: 0.5, delay: 0.25, options: [],
            animations: {
                circle.frame.origin.y += 200
                circle.layer.opacity = 0
        }, completion: { _ in
                circle.removeFromSuperview()
        })
    }
    
    //UIScrollView Delegate Method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.imageCV.contentOffset
        visibleRect.size = self.imageCV.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.imageCV.indexPathForItem(at: visiblePoint) else { return }
        
        self.visibleIndex = indexPath.item
        self.lblDescription.text = imagesList[self.visibleIndex]["media_title"] as? String ?? ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension PhotosViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        imagesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == imagesList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getGalleryGroupImages(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        
        cell.imgGallery._delegate = self
        
        if let imageUrl = imagesList[indexPath.item]["files"] as? String {
            if let url = URL(string: imageUrl) {
                let imgPhoto = UIImageView()
                imgPhoto.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"), options: nil, progressBlock: nil) { (image, error, type, url) in
                    if let downloadImage = image {
                        cell.imgGallery.image = downloadImage
                    }
                }
            }
        }
        
        return cell
    }
}

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var imgGallery: EFImageViewZoom!
}

extension PhotosViewController {
    func getGalleryGroupImages(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kSlug: self.slug, kPage: self.currentPage, kPageSize: PAGESIZE] as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETGALLARY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.imagesList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let videosData = responseData[kData] as? [[String : AnyObject]] {
                        for videos in videosData {
                            self.imagesList.append(videos)
                        }
                    }
                    
                    self.imageCV.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
