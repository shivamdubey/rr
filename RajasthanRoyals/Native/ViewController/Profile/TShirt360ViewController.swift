//
//  TShirt360ViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 09/03/20.
//

import UIKit

class TShirt360ViewController: BaseViewController, EFImageViewZoomDelegate {
    
    @IBOutlet weak var viewTShirt: EFImageViewZoom!
    
    var currentIndex: Int = 0 {
        didSet {
            viewTShirt.image = images[currentIndex]
        }
    }
    
    var images = [UIImage]()
    var lastPoint = CGPoint.zero
    let sensitivity: CGFloat = 5.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "360ImageScreen")
        
        for i in (1...150).reversed() {
            images.append(UIImage(named: "min_\(i)") ?? UIImage())
        }
        currentIndex = 0
        
        self.viewTShirt._delegate = self
    }
    
    @IBAction func gesture2(_ sender: UIPanGestureRecognizer) {
        let currentPoint = sender.location(in: viewTShirt)
        if sender.state == .began {
            lastPoint = currentPoint
        }
        else if sender.state == .changed {
            let velocity = sender.velocity(in: viewTShirt)
            if velocity.x > 0 && currentPoint.x > lastPoint.x + sensitivity{
                currentIndex = currentIndex > 0 ? currentIndex - 1 : images.count - 1
                lastPoint = currentPoint
            }
            else {
                if currentPoint.x < lastPoint.x - sensitivity {
                    currentIndex = currentIndex < images.count - 1 ? currentIndex + 1 : 0
                    lastPoint = currentPoint
                }
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
