//
//  EditProfileViewController.swift
//  RR
//
//  Created by Sagar on 04/02/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController {
    
    @IBOutlet weak var txtFirstName: CustomTextField!
    @IBOutlet weak var txtLastName: CustomTextField!
    @IBOutlet weak var txtBirthDay: CustomTextField!
    @IBOutlet weak var txtTshirtName: CustomTextField!
    @IBOutlet weak var btnUploadPic: UIButton!
    @IBOutlet weak var imgViewUser: CustomImageView!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    var selectedImage: UIImage!
    var selectedDOB = ""
    var selBirthDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "EditProfileScreen")
        
        self.setLeftSideImageInTextField(textField: self.txtFirstName, image: #imageLiteral(resourceName: "person_ic"))
        self.setLeftSideImageInTextField(textField: self.txtLastName, image: #imageLiteral(resourceName: "person_ic"))
        self.setLeftSideImageInTextField(textField: self.txtBirthDay, image: #imageLiteral(resourceName: "password_ic"))
        self.setLeftSideImageInTextField(textField: self.txtTshirtName, image: #imageLiteral(resourceName: "tshirt_ic"))

        self.setupLocalizationText()
        
        self.setUserDetails()
    }
    
    func setupLocalizationText() {
        self.txtFirstName.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_placeholder_FirstName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtLastName.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_placeholder_LastName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtBirthDay.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_placeholder_DOB"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtTshirtName.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Profile_lbl_EnterTShirtName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "btn_Submit"), for: .normal)
    }
    
    //Set User Details
    func setUserDetails() {
        if let image = BaseViewController.sharedInstance.appDelegate.userDetails[kImage] as? String {
            if let url = URL(string: image) {
                self.imgViewUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let firstName = BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String {
            self.txtFirstName.text = firstName
        }
        
        if let lastName = BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String {
            self.txtLastName.text = lastName
        }
        
        if let dob = BaseViewController.sharedInstance.appDelegate.userDetails[kBirthDate] as? String {
            if !dob.isEmpty {
                self.selBirthDate = self.convertStringToDate(format: "yyyy-MM-dd", strDate: dob)
                self.txtBirthDay.text = self.convertDateToString(format: "dd-MM-yyyy", date: self.selBirthDate)
                self.selectedDOB = self.convertDateToString(format: dateFormateForSend, date: self.selBirthDate)
            }
        }
        
        if let tshirtName = BaseViewController.sharedInstance.appDelegate.userDetails[kTShirtName] as? String {
            self.txtTshirtName.text = tshirtName
        }
    }
    
    @IBAction func btnSubmit_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        if txtFirstName.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_FirstName"))
        }
        else if txtLastName.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_LastName"))
        }
        else if self.txtBirthDay.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_DOB"))
        }
        else if self.txtTshirtName.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_TShirtNumber"))
        }
        else {
            self.editProfile()
        }
    }
    
    @IBAction func btnUploadPic_Clicked(_ button: UIButton) {
        self.showActionSheet()
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.view.tintColor = UIColor().alertButtonColor
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_UsingCamera"), style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_ChooseExistingPhoto"), style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .camera
        myPickerController.allowsEditing = true
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .photoLibrary
        myPickerController.allowsEditing = true
        myPickerController.modalPresentationStyle = .overCurrentContext
        myPickerController.addStatusBarBackgroundView()
        myPickerController.view.tintColor = UIColor().alertButtonColor
        self.present(myPickerController, animated: true, completion: nil)
    }
}

extension EditProfileViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.txtBirthDay {
            let maxDate = Calendar.current.date(byAdding: .year, value: -10, to: Date())
            RPicker.selectDate(title: self.getLocalizeTextForKey(keyName: "Profile_lbl_DateOfBirth"), cancelText: self.getLocalizeTextForKey(keyName: "btn_Cancel"), doneText: self.getLocalizeTextForKey(keyName: "btn_Done"), datePickerMode: .date, selectedDate: self.selBirthDate, minDate: nil, maxDate: maxDate, didSelectDate: { date in
                self.txtBirthDay.text = self.convertDateToString(format: "dd-MM-yyyy", date: date)
                self.selectedDOB = self.convertDateToString(format: dateFormateForSend, date: date)
            })
            return false
       }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtTshirtName {
            return range.location < 10
        }
        return true
    }
}

extension EditProfileViewController: UIAdaptivePresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

extension EditProfileViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        self.imgViewUser.image = image.makeFixOrientation()
        self.selectedImage = self.imgViewUser.image
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension EditProfileViewController {
    func editProfile() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            if selectedImage == nil {
                let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                             "Appuser[\(kFirstName)]": self.txtFirstName.text!,
                             "Appuser[\(kLastName)]": self.txtLastName.text!,
                             "Appuser[\(kBirthDate)]": self.selectedDOB,
                             "Appuser[\(kTShirtName)]": self.txtTshirtName.text!] as [String : Any]
                
                APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.UPDATEPROFILE.rawValue, method: .post, parameters: param) { (response, error) in
                    if error == nil {
                        let responseData = response as! [String : AnyObject]
                        
                        if let userDetails = responseData[kData] as? [String : AnyObject] {
                            self.saveUserDetails(userDetails: userDetails)
                            BaseViewController.sharedInstance.callRegisterCustomEventAPI()
                        }
                        
                        if let message = responseData[kMessage] as? String {
                            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                            alert.view.tintColor = UIColor().alertButtonColor
                            
                            let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                self.navigationController?.popViewController(animated: true)
                            })
                            alert.addAction(hideAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                    }
                }
            }
            else {
                let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                             "Appuser[\(kFirstName)]": self.txtFirstName.text!,
                             "Appuser[\(kLastName)]": self.txtLastName.text!,
                             "Appuser[\(kBirthDate)]": self.txtBirthDay.text!,
                             "Appuser[\(kTShirtName)]": self.txtTshirtName.text!] as [String : Any]
                
                APIManager().apiCallWithImage(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.UPDATEPROFILE.rawValue, parameters: param, images: [selectedImage], imageParameterName: "Appuser[image]", imageName: "User.png") { (response, error) in
                    if error == nil {
                        let responseData = response as! [String : AnyObject]
                        
                        if let userDetails = responseData[kData] as? [String : AnyObject] {
                            self.saveUserDetails(userDetails: userDetails)
                            BaseViewController.sharedInstance.updateUserDataOnCRM()
                        }
                        
                        if let message = responseData[kMessage] as? String {
                            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                            alert.view.tintColor = UIColor().alertButtonColor
                            
                            let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                            self.navigationController?.popViewController(animated: true)
                            })
                            alert.addAction(hideAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                    }
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
