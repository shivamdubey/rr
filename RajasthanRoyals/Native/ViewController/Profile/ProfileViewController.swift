//
//  ProfileViewController.swift
//  RR
//
//  Created by Sagar Nandha on 28/01/2020.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

extension UIView {
    func rotate(degrees: CGFloat) {
        rotate(radians: CGFloat.pi * degrees / 180.0)
    }

    func rotate(radians: CGFloat) {
        self.transform = CGAffineTransform(rotationAngle: radians)
    }
}

class ProfileViewController: BaseViewController, TabItem {

    @IBOutlet weak var btnMenu: CustomButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgTShirt: UIImageView!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var btn360: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var imgRoyalsCoins: UIImageView!
    @IBOutlet weak var btnEarnedPoints: CustomButton!
    @IBOutlet weak var imgProfile: CustomImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblFavouritePlayer: UILabel!
    @IBOutlet weak var btnEditFavouritePlayer: CustomButton!
    @IBOutlet weak var imgFavouritePlayer: UIImageView!
    @IBOutlet weak var lblFavouriteFirstName: UILabel!
    @IBOutlet weak var lblFavouriteLastName: UILabel!
    @IBOutlet weak var imgFavouritePlayerCountryFlag: CustomImageView!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblDOBValue: UILabel!
    @IBOutlet weak var lblShirt: UILabel!
    @IBOutlet weak var lblShirtValue: UILabel!
    @IBOutlet weak var btnEditShirtNumber: CustomButton!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var btnMale: CustomButton!
    @IBOutlet weak var btnFemale: CustomButton!
    @IBOutlet weak var btnOther: CustomButton!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var txtCountry: CustomTextField!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var txtState: CustomTextField!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var txtCity: CustomTextField!
    
    @IBOutlet weak var dobUpdateView: UIView!
    @IBOutlet weak var lblEnterDOB: UILabel!
    @IBOutlet weak var lblDOB1: UILabel!
    @IBOutlet weak var txtDOB: CustomTextField!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    @IBOutlet weak var tShirtView: UIView!
    
    var tabImage: UIImage? {
        return UIImage(named: "tab_profile")
    }
    var txtTShirtNumber: UITextField?
    var selectedDOB = ""
    var stateList = [[String: AnyObject]]()
    var cityList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addFirebaseCustomeEvent(eventName: "ProfileScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Profile Page")
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.btnEarnedPoints_Clicked(sender:)))
        tapGesture.numberOfTapsRequired = 1
        self.imgRoyalsCoins.isUserInteractionEnabled = true
        self.imgRoyalsCoins.addGestureRecognizer(tapGesture)
        
        self.btn360.isHidden = true
        
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
        
        self.lblFavouriteFirstName.rotate(degrees: -15)
        self.lblFavouriteLastName.rotate(degrees: -15)
        
        self.setupLocalizationText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let imageUrl = BaseViewController.sharedInstance.appDelegate.generalSettings["royals_coins_image"] as? String {
            if let url = URL(string: imageUrl) {
                self.imgRoyalsCoins.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if BaseViewController.sharedInstance.appDelegate.generalSettings[kIsBirthdate] as? String == kYes {
            if BaseViewController.sharedInstance.appDelegate.userDetails[kBirthDate] as? String == "" {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                    self.dobUpdateView.isHidden = false
                }
            }
            else {
                self.dobUpdateView.isHidden = true
            }
        }
        else {
            self.dobUpdateView.isHidden = true
        }
        
        self.setUserDetails()
    }
    
    func setupLocalizationText() {
        self.lblFavouritePlayer.text = self.getLocalizeTextForKey(keyName: "Profile_lbl_FavouritePlayer")
        self.btnEditFavouritePlayer.setTitle(self.getLocalizeTextForKey(keyName: "Profile_btn_Edit"), for: .normal)
        self.lblDOB.text = self.getLocalizeTextForKey(keyName: "Profile_lbl_DateOfBirth")
        self.lblShirt.text = self.getLocalizeTextForKey(keyName: "Profile_lbl_Shirt")
        self.btnEditShirtNumber.setTitle(self.getLocalizeTextForKey(keyName: "Profile_btn_Edit"), for: .normal)
        self.btnEarnedPoints.setTitle(self.getLocalizeTextForKey(keyName: "Profile_btn_EarnedPoints"), for: .normal)
        
        self.lblEnterDOB.text = self.getLocalizeTextForKey(keyName: "Profile_lbl_EnterDateOfBirth")
        self.lblDOB1.text = self.getLocalizeTextForKey(keyName: "Profile_lbl_DOB")
        self.lblDOB1.text = self.getLocalizeTextForKey(keyName: "Profile_lbl_DOB")
        self.txtDOB.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Profile_placeholder_EnterDOB"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "btn_Submit"), for: .normal)
        self.lblGender.text = self.getLocalizeTextForKey(keyName: "Profile_lbl_SelectGender")
        self.lblCountry.text = self.getLocalizeTextForKey(keyName: "Profile_lbl_Country")
        self.btnMale.setTitle(self.getLocalizeTextForKey(keyName: "Profile_btn_Male"), for: .normal)
        self.btnFemale.setTitle(self.getLocalizeTextForKey(keyName: "Profile_btn_Female"), for: .normal)
        self.btnOther.setTitle(self.getLocalizeTextForKey(keyName: "Profile_btn_Other"), for: .normal)
        self.lblState.text = self.getLocalizeTextForKey(keyName: "Profile_lbl_SelectState")
        self.txtState.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Profile_lbl_SelectState"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.lblCity.text = self.getLocalizeTextForKey(keyName: "Profile_lbl_SelectCity")
        self.txtCity.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Profile_lbl_SelectCity"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    //Set User Details
    func setUserDetails() {
//        if BaseViewController.sharedInstance.appDelegate.generalSettings["is_360"] as? String == kYes {
//            self.btn360.isHidden = false
//            self.imgTShirt.image = #imageLiteral(resourceName: "tshirt_new")
//        }
//        else {
//            self.btn360.isHidden = true
//            self.imgTShirt.image = #imageLiteral(resourceName: "tshirt_old")
//        }
        
        if let image = BaseViewController.sharedInstance.appDelegate.userDetails[kImage] as? String {
            if let url = URL(string: image) {
                self.imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let tshirtName = BaseViewController.sharedInstance.appDelegate.userDetails[kTShirtName] as? String {
            self.lblFirstName.text = tshirtName.uppercased()
        }
        
        if let userLuckyNumber = BaseViewController.sharedInstance.appDelegate.userDetails[kUserLuckynumber] as? String {
            self.lblNumber.text = userLuckyNumber
            self.lblShirtValue.text = userLuckyNumber
        }
        
        //Set Favourite Player
        if let favourites = BaseViewController.sharedInstance.appDelegate.userDetails[kFavourites] as? [[String: AnyObject]] {
            if favourites.count > 0 {
                if let image = favourites[0][kImage] as? String {
                    if let url = URL(string: image) {
                        self.imgFavouritePlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "playerplaceholder"))
                    }
                }
                
                if let firstName = favourites[0][kFirstName] as? String {
                    self.lblFavouriteFirstName.text = firstName
                }
                
                if let lastName = favourites[0][kLastName] as? String {
                    self.lblFavouriteLastName.text = lastName
                }
                
                if let imageUrl = favourites[0][kCountryImage] as? String {
                    if let url = URL(string: imageUrl) {
                        self.imgFavouritePlayerCountryFlag.kf.setImage(with: url, placeholder:  #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
            }
        }
        
        self.lblDescription.text = BaseViewController.sharedInstance.appDelegate.userDetails["profile_message"] as? String ?? ""
        self.lblMessage.text = BaseViewController.sharedInstance.appDelegate.userDetails["profile_message_new"] as? String ?? ""

        if let dob = BaseViewController.sharedInstance.appDelegate.userDetails[kBirthDate] as? String {
            if !dob.isEmpty {
                if let birthDate = self.convertStringToDateNew(format: "yyyy-MM-dd", strDate: dob) {
                    let birthDateStr = self.convertDateToString(format: "dd MMM yyyy", date: birthDate)
                    self.lblDOBValue.text = birthDateStr
                }
                else {
                    if BaseViewController.sharedInstance.appDelegate.generalSettings[kIsBirthdate] as? String == kYes {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                            self.dobUpdateView.isHidden = false
                        }
                    }
                    else {
                        self.dobUpdateView.isHidden = true
                    }
                }
            }
            else {
                if BaseViewController.sharedInstance.appDelegate.generalSettings[kIsBirthdate] as? String == kYes {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                        self.dobUpdateView.isHidden = false
                    }
                }
                else {
                    self.dobUpdateView.isHidden = true
                }
            }
        }
        
        if BaseViewController.sharedInstance.appDelegate.userDetails[kGender] as? String == "Male" {
            self.btnMale.backgroundColor = UIColor().alertButtonColor
            self.btnFemale.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            self.btnOther.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        }
        else if BaseViewController.sharedInstance.appDelegate.userDetails[kGender] as? String == "Female" {
            self.btnMale.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            self.btnFemale.backgroundColor = UIColor().alertButtonColor
            self.btnOther.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        }
        else if BaseViewController.sharedInstance.appDelegate.userDetails[kGender] as? String == "Other" {
            self.btnMale.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            self.btnFemale.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            self.btnOther.backgroundColor = UIColor().alertButtonColor
        }
        else {
            self.btnMale.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            self.btnFemale.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            self.btnOther.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        }
        
        self.txtCountry.text = BaseViewController.sharedInstance.appDelegate.userDetails[kCountryName] as? String ?? ""
        self.txtState.text = BaseViewController.sharedInstance.appDelegate.userDetails[kStateName] as? String ?? ""
        self.txtCity.text = BaseViewController.sharedInstance.appDelegate.userDetails[kCitiesName] as? String ?? ""
        
        self.getStates()
        if let stateId = BaseViewController.sharedInstance.appDelegate.userDetails["state_id"] as? Int {
            self.getCity(stateId: stateId)
        }
        
//        var userAge = "-"
//        var signupDate = ""
//        if let dob = BaseViewController.sharedInstance.appDelegate.userDetails[kBirthDate] as? String {
//            if !dob.isEmpty {
//                if let birthDate = self.convertStringToDateNew(format: "yyyy-MM-dd", strDate: dob) {
//                    let birthDateStr = self.convertDateToString(format: "dd MMM yyyy", date: birthDate)
//                    self.lblDOBValue.text = birthDateStr
//
//                    let calendar = Calendar.current
//                    let ageComponents = calendar.dateComponents([.year], from: birthDate, to: Date())
//                    if let age = ageComponents.year {
//                        userAge = "\(age == 0 ? 1 : age)"
//                    }
//                }
//                else {
//                    if BaseViewController.sharedInstance.appDelegate.generalSettings[kIsBirthdate] as? String == kYes {
//                        self.dobUpdateView.isHidden = false
//                    }
//                    else {
//                        self.dobUpdateView.isHidden = true
//                    }
//                }
//            }
//        }
//
//        if let createdAt = BaseViewController.sharedInstance.appDelegate.userDetails[kCreatedAt] as? String {
//            let createdAtDate = self.convertStringToDate(format: "yyyy-MM-dd HH:mm:ss", strDate: createdAt)
//            let createdAtDateStr = self.convertDateToString(format: "dd MMM yyyy", date: createdAtDate)
//            signupDate = createdAtDateStr
//        }
//
//        if let firstName = BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String {
//            if let lastName = BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String {
//                if var countryName = BaseViewController.sharedInstance.appDelegate.userDetails[kCountryName] as? String {
//                    countryName = countryName == "" ? "INDIA" : countryName.uppercased()
//                    //self.lblDescription.text = "\(firstName) \(lastName) is one of the most passionate fans from \(countryName). The \(userAge) years old's Rajasthan Royals debut came on \(signupDate). \(firstName)'s favourite player is \(self.lblFavouriteFirstName.text!) \(self.lblFavouriteLastName.text!)"
//
//                    if BaseViewController.sharedInstance.appLanguage == APPLANGUAGE.ENGLISH.rawValue {
//                        self.lblDescription.text = "A proud member of the Royals family from \(countryName), \(firstName) \(lastName) is a fan who not only cheers for the Rajasthan Royals in front of the television but also supports the team here. These all-round abilities make the \(userAge) year old a valuable asset for the team. \(firstName)'s day is made when \(self.lblFavouriteFirstName.text!) \(self.lblFavouriteLastName.text!) is in action."
//                    }
//                    else {
//                        self.lblDescription.text = "\(countryName) से रॉयल्स परिवार के एक गर्वित सदस्य, \(firstName) \(lastName) एक प्रशंसक है जो न केवल टेलीविजन के सामने राजस्थान रॉयल्स के लिए खुश है, बल्कि यहां टीम का समर्थन भी करता है। \(userAge) साल के फ़ैन की ये सर्वांगीण क्षमताएं टीम के लिए मूल्यवान संपत्ति हैं। \(firstName) का दिन तब बनता है जब \(self.lblFavouriteFirstName.text!) \(self.lblFavouriteLastName.text!) एक्शन में होता है।"
//                    }
//                }
//            }
//        }
    }
    
    func openTshirtNumberEditAlert() {
       let alertController = UIAlertController(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "Profile_lbl_EnterTShirtNumber"), preferredStyle: .alert)
        alertController.view.tintColor = UIColor().alertButtonColor
        
        let withdrawAction = UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_Update"), style: .default) { (_) in
            guard let textFields = alertController.textFields,
                textFields.count > 0 else {
                return
            }
            
            let txtNumber = textFields[0]
            if txtNumber.text?.count == 0 {
                UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_TShirtNumber"))
            }
            else {
                self.editProfile(tShirtNumber: txtNumber.text!)
            }
        }
        
        let cancelAction = UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = self.getLocalizeTextForKey(keyName: "Profile_lbl_EnterTShirtNumber")
            textField.keyboardType = .numberPad
            textField.delegate = self
            textField.text = self.lblNumber.text
            
            self.txtTShirtNumber = textField
        }
        
        alertController.addAction(withdrawAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnSubmit_Clicked(sender: UIButton) {
        self.view.endEditing(true)
        if self.txtDOB.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_DOB"))
        }
        else {
            self.editDOB()
        }
    }
    
    @IBAction func btnClose_Clicked(sender: UIButton) {
        self.dobUpdateView.isHidden = true
    }
    
    @IBAction func btnMenu_Clicked(sender: UIButton) {
    }
    
    @IBAction func btnShare_Clicked(sender: UIButton) {
        let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "eventType": POINTSEVENTTYPE.SHARE_CUSTOMISED_JERSEY.rawValue]
        self.callEarnPointsAPI(param: param)
        
        // set up activity view controller
        let image = self.takeScreenshot(theView: self.tShirtView)
        let objectsToShare: [AnyObject] = [image]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.completionWithItemsHandler = { activity, success, items, error in
            if success {
                // Success handling here
                self.imgProfile.isHidden = false
                self.btnEarnedPoints.isHidden = false
            }
        }
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func takeScreenshot(theView: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(theView.frame.size, true, 0.0)
        theView.drawHierarchy(in: theView.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage()
    }
    
    @IBAction func btn360_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "TShirt360ViewController") as! TShirt360ViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnEarnedPoints_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "LoyalityPointsViewController") as! LoyalityPointsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnEditProfile_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnEditFavouritePlayer_Clicked(sender: UIButton) {
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Favourite Player")
        
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PlayerListViewController") as! PlayerListViewController
        viewController.isFromProfile = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnEditShirtNumber_Clicked(sender: UIButton) {
        self.openTshirtNumberEditAlert()
    }
    
    @IBAction func btnMale_Clicked(sender: UIButton) {
        if BaseViewController.sharedInstance.appDelegate.userDetails[kGender] as? String == "" {
            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "Profile_lbl_MaleMessage"), preferredStyle: .alert)
            alert.view.tintColor = UIColor().alertButtonColor
            
            let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Confirm"), style: .default, handler: { (action) in
                self.editGener(gender: "Male")
            })
            
            let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: .default, handler: { (action) in
            })
            
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "Profile_lbl_GenderAlreadyConfirmed"))
        }
    }
    
    @IBAction func btnFemale_Clicked(sender: UIButton) {
        if BaseViewController.sharedInstance.appDelegate.userDetails[kGender] as? String == "" {
            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "Profile_lbl_FemaleMessage"), preferredStyle: .alert)
            alert.view.tintColor = UIColor().alertButtonColor
            
            let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Confirm"), style: .default, handler: { (action) in
                self.editGener(gender: "Female")
            })
            
            let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: .default, handler: { (action) in
            })
            
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "Profile_lbl_GenderAlreadyConfirmed"))
        }
    }
    
    @IBAction func btnOther_Clicked(sender: UIButton) {
        if BaseViewController.sharedInstance.appDelegate.userDetails[kGender] as? String == "" {
            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "Profile_lbl_OtherMessage"), preferredStyle: .alert)
            alert.view.tintColor = UIColor().alertButtonColor
            
            let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Confirm"), style: .default, handler: { (action) in
                self.editGener(gender: "Other")
            })
            
            let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: .default, handler: { (action) in
            })
            
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "Profile_lbl_GenderAlreadyConfirmed"))
        }
    }
}

extension ProfileViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.txtDOB {
            let maxDate = Calendar.current.date(byAdding: .year, value: -10, to: Date())
            RPicker.selectDate(title: self.getLocalizeTextForKey(keyName: "Profile_lbl_DateOfBirth"), cancelText: self.getLocalizeTextForKey(keyName: "btn_Cancel"), doneText: self.getLocalizeTextForKey(keyName: "btn_Done"), datePickerMode: .date, minDate: nil, maxDate: maxDate, didSelectDate: { date in
                self.txtDOB.text = self.convertDateToString(format: "dd-MM-yyyy", date: date)
                self.selectedDOB = self.convertDateToString(format: dateFormateForSend, date: date)
            })
            return false
        }
        else if textField == self.txtState {
            let stateList = self.stateList.map({$0["name"] as? String ?? "-"})
            RPicker.selectOption(title: self.getLocalizeTextForKey(keyName: "Profile_lbl_SelectState"), cancelText: self.getLocalizeTextForKey(keyName: "btn_Cancel"), doneText: self.getLocalizeTextForKey(keyName: "btn_Done"), dataArray: stateList, selectedIndex: 0) { value, index in
                self.txtState.text = value
                self.txtCity.text = ""
                
                if self.stateList.count > index {
                    if let stateId = self.stateList[index]["state_id"] as? Int {
                        self.saveStatesAndCity(stateId: stateId, cityId: 0)
                    }
                }
            }
            return false
        }
        else if textField == self.txtCity {
            let cityList = self.cityList.map({$0["name"] as? String ?? "-"})
            RPicker.selectOption(title: self.getLocalizeTextForKey(keyName: "Profile_lbl_SelectCity"), cancelText: self.getLocalizeTextForKey(keyName: "btn_Cancel"), doneText: self.getLocalizeTextForKey(keyName: "btn_Done"), dataArray: cityList, selectedIndex: 0) { value, index in
                self.txtCity.text = value
                
                if self.cityList.count > index {
                    if let cityId = self.cityList[index]["cities_id"] as? Int {
                        self.saveStatesAndCity(stateId: 0, cityId: cityId)
                    }
                }
            }
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          textField.resignFirstResponder()
          return true
      }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 3
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

class StickerCell: UICollectionViewCell {
    @IBOutlet weak var imgSticker: CustomImageView!
}

extension ProfileViewController {
    func getStates() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["country_id": self.txtCountry.text ?? ""] as [String : Any]
             
            APIManager().apiCall(isShowHud: false, URL: baseURLWithoutAuth, apiName: APINAME.GETSTATES.rawValue, method: .post, parameters: param) {
                    (response, error) in
                if (error == nil) {
                    let responseData = response as! [String : AnyObject]
                     
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.stateList = data
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(error!.domain)")
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getCity(stateId: Int, isCallSaveCityAPI: Bool = false) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["state_id": stateId] as [String : Any]
             
            APIManager().apiCall(isShowHud: false, URL: baseURLWithoutAuth, apiName: APINAME.GETCITIES.rawValue, method: .post, parameters: param) {
                    (response, error) in
                if (error == nil) {
                    let responseData = response as! [String : AnyObject]
                     
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.cityList = data
                        
                        if isCallSaveCityAPI {
                            if self.cityList.count > 0 {
                                self.txtCity.text = self.cityList[0]["name"] as? String ?? ""
                                if let cityId = self.cityList[0]["cities_id"] as? Int {
                                    self.saveStatesAndCity(stateId: 0, cityId: cityId)
                                }
                            }
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(error!.domain)")
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func saveStatesAndCity(stateId: Int, cityId: Int) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            var param: [String: String] = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey]
            if stateId != 0 {
                param["Appusercountrieswiseuser[state_id]"] = "\(stateId)"
            }
            else if cityId != 0 {
                param["Appusercountrieswiseuser[cities_id]"] = "\(cityId)"
            }
             
            APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.APPUSERCOUNTRIESWISEUSER.rawValue, method: .post, parameters: param) {
                    (response, error) in
                if (error == nil) {
                    let responseData = response as! [String : AnyObject]
                     
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                        self.setUserDetails()
                        
                        BaseViewController.sharedInstance.callRegisterCustomEventAPI()
                        
                        if stateId != 0 {
                            self.getCity(stateId: stateId, isCallSaveCityAPI: true)
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(error!.domain)")
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func editProfile(tShirtNumber: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                        "Appuser[\(kUserLuckynumber)]": tShirtNumber] as [String : Any]
             
            APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.UPDATEPROFILE.rawValue, method: .post, parameters: param) {
                    (response, error) in
                if (error == nil) {
                    let responseData = response as! [String : AnyObject]
                     
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                        self.setUserDetails()
                        
                        BaseViewController.sharedInstance.callRegisterCustomEventAPI()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(error!.domain)")
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func editDOB() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                        "Appuser[\(kBirthDate)]": self.selectedDOB] as [String : Any]
            
            APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.UPDATEPROFILE.rawValue, method: .post, parameters: param) {
                   (response, error) in
                
                if (error == nil) {
                    let responseData = response as! [String : AnyObject]
                    
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                        self.setUserDetails()
                        
                        BaseViewController.sharedInstance.callRegisterCustomEventAPI()
                        
                        self.dobUpdateView.isHidden = true
                        
                        let param = ["email": userDetails[kEmail] as? String ?? "", "birthDate": userDetails[kBirthDate] as? String ?? "", "eventType": POINTSEVENTTYPE.CREATE_PROFILE.rawValue]
                        self.callEarnPointsAPI(param: param)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(error!.domain)")
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func editGener(gender: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "Appuser[\(kGender)]": gender] as [String : Any]
             
            APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.UPDATEPROFILE.rawValue, method: .post, parameters: param) {
                    (response, error) in
                if (error == nil) {
                    let responseData = response as! [String : AnyObject]
                     
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                        self.setUserDetails()
                        
                        BaseViewController.sharedInstance.callRegisterCustomEventAPI()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(error!.domain)")
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension ProfileViewController: UIAdaptivePresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
