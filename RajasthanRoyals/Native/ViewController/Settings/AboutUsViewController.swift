//
//  AboutUsViewController.swift
//  RR
//
//  Created by Sagar on 23/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class AboutUsViewController: BaseViewController {
    
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var txtViewDescription: UITextView!

    var strTitle = ""
    var strValueForAPI = ""
    var strDescription = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblNavTitle.text = self.strTitle
        
        if BaseViewController.sharedInstance.appDelegate.generalSettings.isEmpty {
            self.appSetting()
        }
        else {
            self.setStaticData()
        }
    }
    
    func setStaticData() {
        if strDescription.isEmpty {
            if self.strValueForAPI == "T&Cs" {
                self.addFirebaseCustomeEvent(eventName: "TermsConditionScreen")
                
                if let value = BaseViewController.sharedInstance.appDelegate.generalSettings["terms_conditions"] as? String {
                    self.txtViewDescription.attributedText = value.html2Attributed
                }
            }
            else if self.strValueForAPI == "About Us" {
                self.addFirebaseCustomeEvent(eventName: "AboutUsScreen")
                
                if let value = BaseViewController.sharedInstance.appDelegate.generalSettings["about_us"] as? String {
                    self.txtViewDescription.attributedText = value.html2Attributed
                }
            }
            else if self.strValueForAPI == "Privacy Policy" {
                self.addFirebaseCustomeEvent(eventName: "PrivacyPolicyScreen")
                
                if let value = BaseViewController.sharedInstance.appDelegate.generalSettings["privacy_policy"] as? String {
                    self.txtViewDescription.attributedText = value.html2Attributed
                }
            }
            else if self.strValueForAPI == "Whatsapp Policy" {
                self.addFirebaseCustomeEvent(eventName: "WhatsappPolicyScreen")
                if let value = BaseViewController.sharedInstance.appDelegate.generalSettings["whatapp_policy"] as? String {
                    self.txtViewDescription.attributedText = value.html2Attributed
                }
            }
            else if self.strValueForAPI == "Fans Corner" {
                if let value = BaseViewController.sharedInstance.appDelegate.generalSettings["fans_corners"] as? String {
                    self.txtViewDescription.attributedText = value.html2Attributed
                }
            }
        }
        else {
            self.txtViewDescription.attributedText = strDescription.html2Attributed
        }
    }
}

extension AboutUsViewController {
    func appSetting() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.APPSETTING.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let generalSettings = responseData[kData] as? [String : AnyObject] {
                        BaseViewController.sharedInstance.appDelegate.generalSettings = generalSettings
                        
                        self.setStaticData()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
