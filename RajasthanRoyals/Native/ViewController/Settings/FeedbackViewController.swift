//
//  FeedbackViewController.swift
//  RR
//
//  Created by Sagar on 23/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class FeedbackViewController: BaseViewController {

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtFullName: CustomTextField!
    @IBOutlet weak var txtEmailAddress: CustomTextField!
    @IBOutlet weak var txtMessage: CustomTextView!
    @IBOutlet weak var btnSubmit: CustomButton!
    @IBOutlet weak var viewName:UIView!
    @IBOutlet weak var viewMail:UIView!
    @IBOutlet weak var viewTextDesc:UIView!
    @IBOutlet weak var imgBgOffeedback:UIImageView!
    
    var isFromContactUs = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.setLeftSideImageInTextField(textField: self.txtFullName, image: #imageLiteral(resourceName: "person_ic"))
//        self.setLeftSideImageInTextField(textField: self.txtEmailAddress, image: #imageLiteral(resourceName: "email_ic"))
        
        self.setupLocalizationText()
        
        self.setUserDetails()
        viewName.layer.cornerRadius = 10
        viewMail.layer.cornerRadius = 10
        
        self.viewTextDesc.layer.cornerRadius = 10
        self.imgBgOffeedback.layer.cornerRadius = 10
        
        
        self.txtMessage.textColor = UIColor.white.withAlphaComponent(0.5)
        self.txtMessage.text = self.getLocalizeTextForKey(keyName: "Feedback_lbl_WriteYourMessage")
    }
    
    func setupLocalizationText() {
        if isFromContactUs {
            self.navTitle.text = self.getLocalizeTextForKey(keyName: "Settings_lbl_ContactUs")
        }
        else {
            self.navTitle.text = self.getLocalizeTextForKey(keyName: "Settings_lbl_Feedback")
        }
        
        self.txtFullName.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Feedback_placeholder_FullName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtEmailAddress.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Feedback_placeholder_EmailAddress"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "btn_Submit"), for: .normal)
    }
    
    //Set User Details
    func setUserDetails() {
        if let firstName = BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String {
            if let lastName = BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String {
                self.txtFullName.text = "\(firstName) \(lastName)"
            }
        }
        
        if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
            self.txtEmailAddress.text = email
        }
    }
    
    @IBAction func btnSubmit_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        if txtFullName.text?.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_FullName"))
        }
        else if txtEmailAddress.text?.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_EmailAddress"))
        }
        else if !self.isValidEmail(str: txtEmailAddress.text!) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidEmailAddress"))
        }
        else if txtMessage.text!.trim().count == 0 && self.txtMessage.textColor != UIColor.white.withAlphaComponent(0.5) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Message"))
        }
        else {
            self.contactUsAndFeecbak()
        }
    }
}

extension FeedbackViewController {
    func contactUsAndFeecbak() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            var param = ["Contactus[name]": txtFullName.text!,
                         "Contactus[email]": txtEmailAddress.text!,
                         "Contactus[message]": txtMessage.text!] as [String : Any]
            
            if !isFromContactUs {
                param = ["Feedback[name]": txtFullName.text!,
                         "Feedback[email]": txtEmailAddress.text!,
                         "Feedback[message]": txtMessage.text!] as [String : Any]
            }
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: self.isFromContactUs ? APINAME.CONTACTUS.rawValue : APINAME.FEEDBACK.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let message = responseData[kMessage] as? String {
                        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                        alert.view.tintColor = UIColor().alertButtonColor
                        
                        let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                            self.navigationController?.popViewController(animated: true)
                        })
                        alert.addAction(hideAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension FeedbackViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtFullName.isFirstResponder {
            _ = txtEmailAddress.becomeFirstResponder()
        }
        else if txtEmailAddress.isFirstResponder {
            _ = txtMessage.becomeFirstResponder()
        }
        else if txtMessage.isFirstResponder {
            _ = txtMessage.becomeFirstResponder()
        }
        return true
    }
}

extension FeedbackViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.white.withAlphaComponent(0.5) {
            self.txtMessage.textColor = UIColor.white
            self.txtMessage.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trim().isEmpty {
            self.txtMessage.textColor = UIColor.white.withAlphaComponent(0.5)
            self.txtMessage.text = self.getLocalizeTextForKey(keyName: "Feedback_lbl_WriteYourMessage")
        }
    }
}
