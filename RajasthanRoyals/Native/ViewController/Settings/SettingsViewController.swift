//
//  SettingsViewController.swift
//  RR
//
//  Created by Sagar on 23/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var CVSettings: UICollectionView!

    var settingImages = ["notification_ic", "about_ic", "contact_ic", "privacy_ic", "terms_ic", "chnagepassword_ic"]
    var settingsList = ["Notifications", "About Us", "Contact Us", "Privacy Policy", "T&Cs", "Change Password"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "SettingsScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Settings")
        
        if BaseViewController.sharedInstance.appDelegate.userDetails[kLoginType] as? String == "Normal" {
            settingsList = [self.getLocalizeTextForKey(keyName: "Settings_lbl_Notification"), self.getLocalizeTextForKey(keyName: "Settings_lbl_AboutUs"), self.getLocalizeTextForKey(keyName: "Settings_lbl_ContactUs"), self.getLocalizeTextForKey(keyName: "Settings_lbl_PrivacyPolicy"), self.getLocalizeTextForKey(keyName: "Settings_lbl_TCs"), self.getLocalizeTextForKey(keyName: "RightMenu_lbl_ChangePassword")]
            settingImages = ["notification_ic", "about_ic", "contact_ic", "privacy_ic", "terms_ic", "chnagepassword_ic"]
        }
        else {
            settingsList = [self.getLocalizeTextForKey(keyName: "Settings_lbl_Notification"), self.getLocalizeTextForKey(keyName: "Settings_lbl_AboutUs"), self.getLocalizeTextForKey(keyName: "Settings_lbl_ContactUs"), self.getLocalizeTextForKey(keyName: "Settings_lbl_PrivacyPolicy"), self.getLocalizeTextForKey(keyName: "Settings_lbl_TCs")]
            settingImages = ["notification_ic", "about_ic", "contact_ic", "privacy_ic", "terms_ic"]
        }
        
        if BaseViewController.sharedInstance.appDelegate.generalSettings.isEmpty {
            self.appSetting()
        }
    }
}

extension SettingsViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settingsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SettingsCVCell", for: indexPath) as! SettingsCVCell

        cell.imgView.image = UIImage(named: self.settingImages[indexPath.item])
        cell.lblTitle.text = self.settingsList[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let strTitle = self.settingsList[indexPath.item]
        
        if strTitle == self.getLocalizeTextForKey(keyName: "Settings_lbl_Notification") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if strTitle == self.getLocalizeTextForKey(keyName: "Settings_lbl_AboutUs") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            viewController.strTitle = strTitle
            viewController.strValueForAPI = "About Us"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if strTitle == self.getLocalizeTextForKey(keyName: "Settings_lbl_Feedback") || strTitle == self.getLocalizeTextForKey(keyName: "Settings_lbl_ContactUs") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
            viewController.isFromContactUs = strTitle == self.getLocalizeTextForKey(keyName: "Settings_lbl_Feedback") ? false : true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if strTitle == self.getLocalizeTextForKey(keyName: "Settings_lbl_FAQs") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FAQsViewController") as! FAQsViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if strTitle == self.getLocalizeTextForKey(keyName: "Settings_lbl_PrivacyPolicy") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            viewController.strTitle = strTitle
            viewController.strValueForAPI = "Privacy Policy"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if strTitle == self.getLocalizeTextForKey(keyName: "Settings_lbl_TCs") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            viewController.strTitle = strTitle
            viewController.strValueForAPI = "T&Cs"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if strTitle == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_ChangePassword") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension SettingsViewController : UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 1
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: 50)
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}

class SettingsCVCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}

extension SettingsViewController {
    func appSetting() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.APPSETTING.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let generalSettings = responseData[kData] as? [String : AnyObject] {
                        BaseViewController.sharedInstance.appDelegate.generalSettings = generalSettings
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
