//
//  NotificationsViewController.swift
//  RR
//
//  Created by Sagar on 23/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblNotifications: UITableView!
    
    var currentPage = 1
    var nextPage = kN
    var notificationList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "NotificationsScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Settings_lbl_Notification")
        
        self.getNotificationList(isShowHud: true)
    }
}

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return notificationList.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == notificationList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getNotificationList(isShowHud: false)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == notificationList.count - 1 && nextPage == kY {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.clear
            
            let activityIndicator = UIActivityIndicatorView(style: .white)
            activityIndicator.color = UIColor().alertButtonColor
            activityIndicator.center = CGPoint(x: tableView.frame.size.width / 2, y: 20)
            
            cell.addSubview(activityIndicator)
            
            activityIndicator.startAnimating()
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTblCell") as! NotificationsTblCell
        
        if let description = notificationList[indexPath.row][kMessage] as? String {
            cell.lblDescription.text = description
        }
        
        if var time = notificationList[indexPath.row][kTime] as? Double {
            time = time / 1000
            let createdDate = Date(timeIntervalSince1970: time)
            cell.lblDateTime.text = self.convertDateToString(format: "dd MMM yyyy, hh:mm a", date: createdDate)
        }
        
        return cell
    }
}

class NotificationsTblCell: UITableViewCell {
    @IBOutlet weak var bgViewForCell: CustomView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
}

extension NotificationsViewController {
    func getNotificationList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETNOTIFICATIONLIST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.notificationList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let dataList = responseData[kData] as? [[String : AnyObject]] {
                        for data in dataList {
                            self.notificationList.append(data)
                        }
                    }
                    
                    self.tblNotifications.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
