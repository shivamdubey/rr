//
//  ChangePasswordViewController.swift
//  RR
//
//  Created by Sagar on 04/02/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseViewController {
    
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var txtCurrentPassword: CustomTextField!
    @IBOutlet weak var txtNewPassword: CustomTextField!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    @IBOutlet weak var btnNewPassword: UIButton!
    @IBOutlet weak var btnConfirmPas: UIButton!
    @IBOutlet weak var btnCurrentPass: UIButton!
    
    @IBOutlet weak var viewCurrentPassword: UIView!
    @IBOutlet weak var viewNewPassword: UIView!
    @IBOutlet weak var viewConfirmPassword: UIView!
    @IBOutlet weak var imgPasswordBg: UIImageView!
       
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "ChangePasswordScreen")

//        self.setLeftSideImageInTextField(textField: self.txtCurrentPassword, image: #imageLiteral(resourceName: "password_ic"))
//        self.setLeftSideImageInTextField(textField: self.txtNewPassword, image: #imageLiteral(resourceName: "password_ic"))
//        self.setLeftSideImageInTextField(textField: self.txtConfirmPassword, image: #imageLiteral(resourceName: "password_ic"))
        
       
        viewCurrentPassword.layer.cornerRadius = 10
        viewNewPassword.layer.cornerRadius = 10
        viewConfirmPassword.layer.cornerRadius = 10
        imgPasswordBg.layer.cornerRadius = 10
        
        btnConfirmPas.setTitle("", for: .normal)
        btnCurrentPass.setTitle("", for: .normal)
        btnNewPassword.setTitle("", for: .normal)
        
       // viewConfirmPassword.layer.cornerRadius = 10
        
        self.setupLocalizationText()
    }
    
    func setupLocalizationText() {
        self.lblNavTitle.text = self.getLocalizeTextForKey(keyName: "RightMenu_lbl_ChangePassword")
        self.txtCurrentPassword.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "ChangePassword_placeholder_CurrentPassword"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtNewPassword.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "ChangePassword_placeholder_NewPassword"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtConfirmPassword.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "ChangePassword_placeholder_ConfirmNewPassword"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "btn_Submit"), for: .normal)
    }
    
    @IBAction func btnSubmit_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        if txtCurrentPassword.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_CurrentPassword"))
        }
        else if txtNewPassword.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_NewPassword"))
        }
        else if txtNewPassword.text!.count < 6 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_NewPasswordToShort"))
        }
        else if txtConfirmPassword.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ConfirmPassword"))
        }
        else if txtConfirmPassword.text!.count < 6 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ConfirmPasswordToShort"))
        }
        else if txtNewPassword.text! != txtConfirmPassword.text! {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_NewPasswordAndConfirmPasswordNotMatch"))
        }
        else if txtCurrentPassword.text! == txtNewPassword.text! {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_OldPasswordAndNewPasswordCanNotBeSame"))
        }
        else {
            self.callChangePasswordAPI()
        }
    }
    @IBAction func btnShowPassword_Clicked(_ button: UIButton) {
        if button.tag == 10{
            txtCurrentPassword.isSecureTextEntry.toggle()
        }
        else if button.tag == 20{
            txtNewPassword.isSecureTextEntry.toggle()
        }
        else if button.tag == 30{
            txtConfirmPassword.isSecureTextEntry.toggle()
        }
    }
}

extension ChangePasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtCurrentPassword.isFirstResponder {
            _ = txtNewPassword.becomeFirstResponder()
        }
        else if txtNewPassword.isFirstResponder {
            _ = txtConfirmPassword.becomeFirstResponder()
        }
        else if txtConfirmPassword.isFirstResponder {
            _ = txtConfirmPassword.becomeFirstResponder()
        }
        return true
    }
}

extension ChangePasswordViewController {
    func callChangePasswordAPI() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "password": txtCurrentPassword.text!,
                         "new_password": txtNewPassword.text!] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.CHANGEPASSWORD.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let message = responseData[kMessage] as? String {
                        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                        alert.view.tintColor = UIColor().alertButtonColor
                        
                        let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                            self.navigationController?.popViewController(animated: true)
                        })
                        alert.addAction(hideAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
