//
//  FAQsViewController.swift
//  RR
//
//  Created by Sagar on 23/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class FAQsViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblFAQs: UITableView!
    
    var faqList = [[String: AnyObject]]()
    var selectedFAQIndex = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Settings_lbl_FAQs")
        
        self.getFAQList()
    }
}

extension FAQsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell") as! FAQCell
         
        if let question = faqList[indexPath.row][kQuestion] as? String {
            cell.lblQuestion.text = question
        }
        
        cell.lblAnswer.text = ""
        cell.imgArrow.image = #imageLiteral(resourceName: "arrow_left_ic").imageWithColor(color: .white)
        cell.lblAnswerTopConstraint.constant = -20
        
        if selectedFAQIndex.contains(indexPath.row) {
            cell.lblAnswerTopConstraint.constant = 5
            cell.imgArrow.image = #imageLiteral(resourceName: "arrow_down_ic").imageWithColor(color: .white)
            
            if let answer = faqList[indexPath.row][kAnswer] as? String {
                cell.lblAnswer.text = answer
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedFAQIndex.contains(indexPath.row) {
            selectedFAQIndex = selectedFAQIndex.filter({ $0 != indexPath.row })
        }
        else {
            selectedFAQIndex.append(indexPath.row)
        }
        self.tblFAQs.reloadRows(at: [indexPath], with: .none)
    }
}

class FAQCell: UITableViewCell {
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblAnswer: UILabel!
    
    @IBOutlet weak var lblAnswerTopConstraint: NSLayoutConstraint!
}

extension FAQsViewController {
    func getFAQList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["type": "App"]
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETFAQLIST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let faqs = responseData[kData] as? [[String : AnyObject]] {
                        self.faqList = faqs
                    }
                    
                    self.tblFAQs.reloadData()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
