//
//  LoginViewController.swift
//  Demo
//
//  Created by Hardy on 3/2/19.
//  Copyright © 2019 Sagar Nandha. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MobileCoreServices
import Alamofire
import KRProgressHUD

enum MESSAGETYPE: String {
    case Image = "Image"
    case Text = "Text"
}

class ChatViewController: BaseViewController, TabItem {

    var tabImage: UIImage? {
        return UIImage(named: "tab_chat")
    }
        
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var viewSend: UIView!
    @IBOutlet weak var btnPhoto: UIButton!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var btnSend: UIButton!
    
    @IBOutlet weak var tblChatHeightConstant: NSLayoutConstraint!
    
    var placeholder = "Message"
    var selectedImage: UIImage!
    var topBarHeight = UIApplication.shared.statusBarFrame.size.height
    var keyboardFrame = CGRect()
    
    var currentPage = 1
    var nextPage = kN
    var chatList = [[String: AnyObject]]()
    var appUserId = 0
    
    var lastScrolledRow = 0
    var scrollToRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "ChatScreen")
        
        self.setupLocalizationText()
        
        if let userId = BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int {
            self.appUserId = userId
        }
        
        tblChat.rowHeight = UITableView.automaticDimension
        tblChat.estimatedRowHeight = 250
        
        self.topBarHeight = UIApplication.shared.statusBarFrame.size.height + (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        if IS_IPHONEX || IS_IPHONEXMax {
            self.tblChatHeightConstant.constant = self.view.frame.height - (self.topBarHeight + 49.0 + self.viewSend.frame.height + 74)
        }
        else {
            self.tblChatHeightConstant.constant = self.view.frame.height - (self.topBarHeight + 49.0 + self.viewSend.frame.height + 40)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardShown), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NewMessage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(newMessage(notification:)), name: NSNotification.Name(rawValue: "NewMessage"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "DeleteMessage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deleteMessage(notification:)), name: NSNotification.Name(rawValue: "DeleteMessage"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReloadData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData(notification:)), name: NSNotification.Name(rawValue: "ReloadData"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Chat")
    }
    
    func setupLocalizationText() {
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Chat_lbl_Title")
        self.placeholder = self.getLocalizeTextForKey(keyName: "Chat_placeholder_Message")
        
        self.txtMessage.text = self.placeholder
        self.txtMessage.textColor = .lightGray
    }
    
    //NSNotification Method
    @objc func keyboardShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        self.keyboardFrame = keyboardFrame
        
        if IS_IPHONEX || IS_IPHONEXMax {
            self.tblChatHeightConstant.constant = self.view.frame.size.height - (self.topBarHeight + 90 + keyboardFrame.height - 34)
        }
        else {
            self.tblChatHeightConstant.constant = self.view.frame.size.height - (self.topBarHeight + 90 + keyboardFrame.height) - 15
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            self.scrollToBottom()
        }
    }
    
    @objc func keyboardHide(notification: NSNotification) {
        if IS_IPHONEX || IS_IPHONEXMax {
            self.tblChatHeightConstant.constant = self.view.frame.height - (self.topBarHeight + 90 + self.viewSend.frame.height)
        }
        else {
            self.tblChatHeightConstant.constant = self.view.frame.height - (self.topBarHeight + 90 + self.viewSend.frame.height)
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            self.scrollToBottom()
        }
    }
    
    @objc func newMessage(notification: NSNotification) {
        if let chat = notification.userInfo as? [String: AnyObject] {
            self.chatList.append(chat)
            self.tblChat.reloadData()
        }
    }
    
    @objc func deleteMessage(notification: NSNotification) {
        if let info = notification.userInfo as? [String: AnyObject] {
            if let deleteMessageId = info[kMessageId] as? Int {
                //Find the index
                let array = chatList as Array
                let matchIndex = array.index {
                    if $0[kMessageId] as? Int == deleteMessageId {
                        return true
                    }
                    return false
                }
                
                if matchIndex != nil {
                    let indexPath = IndexPath.init(row: matchIndex!, section: 0)
                    self.chatList.remove(at: indexPath.row)
                    self.tblChat.deleteRows(at: [indexPath], with: .left)
                }
            }
        }
    }
    
    @objc func reloadData(notification: NSNotification) {
        self.currentPage = 1
        self.getChatList(isShowHud: true, isScrollToBottom: true)
    }
    
    //UIImagePickerController Method
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.view.tintColor = UIColor().alertButtonColor
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_UsingCamera"), style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_ChooseExistingPhoto"), style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .camera
        myPickerController.allowsEditing = false
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .photoLibrary
        myPickerController.allowsEditing = false
        myPickerController.modalPresentationStyle = .overCurrentContext
        myPickerController.addStatusBarBackgroundView()
        myPickerController.view.tintColor = UIColor().alertButtonColor
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    //TableView Scroll To Bottom
    func scrollToBottom(animate: Bool, row: Int, scrollPosition: UITableView.ScrollPosition) {
        DispatchQueue.main.async {
            if self.chatList.count > 0 {
                let indexPath = IndexPath(row: row, section: 0)
                self.tblChat.scrollToRow(at: indexPath, at: scrollPosition, animated: animate)
            }
        }
    }
    
    //TableView Scroll To Bottom
    func scrollToBottom() {
        if self.chatList.count > 0 {
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: self.chatList.count - 1, section: 0)
                self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: true)
                self.lastScrolledRow = self.chatList.count - 1
            }
        }
    }
    
    @IBAction func btnPhoto_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        self.showActionSheet()
    }
    
    @IBAction func btnSend_Clicked(_ sender: Any) {
        if txtMessage.text.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Message"))
        }
        else if txtMessage.textColor == UIColor.lightGray {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Message"))
        }
        else {
            self.sendMessage(message: txtMessage.text, messageType: MESSAGETYPE.Text.rawValue)
            self.txtMessage.text = ""
        }
    }
    
    //UIScrollView Delegate Method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let firstVisibleIndexPath = self.tblChat.indexPathsForVisibleRows?.last {
            self.lastScrolledRow = firstVisibleIndexPath.row
        }
        
        if self.tblChat.contentOffset.y <= 0.0 && self.nextPage == kY {
            self.currentPage = self.currentPage + 1
            self.getChatList(isShowHud: false, isScrollToBottom: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ChatViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        self.uploadMedia(mediaType: MESSAGETYPE.Image.rawValue, selectedMedia: image.makeFixOrientation())
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chatDetails = chatList[indexPath.row]
        
        if chatDetails[kAppUserId] as? Int == self.appUserId {
            if chatDetails[kMessageType] as? String == MESSAGETYPE.Image.rawValue {
                let cellIdentifier = "OwnerImageCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! OwnerImageCell
                
                if let imageUrl = chatDetails[kMessage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgPhoto.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                if var time = chatDetails[kTime] as? Double {
                    time = time / 1000
                    let createdDate = Date(timeIntervalSince1970: time)
                    cell.lblDateTime.text = self.convertDateToString(format: "dd MMM yyyy hh:mm a", date: createdDate)
                }
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.05, execute: {
                    cell.bgView.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 5)
                })
                
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressCell(tapGesture:)))
                cell.bgView.addGestureRecognizer(longGesture)
                
                return cell
            }
            else {
                let cellIdentifier = "OwnerCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! OwnerCell
                
                if let message = chatDetails[kMessage] as? String {
                    cell.lblMessage.text = message
                }
                
                if var time = chatDetails[kTime] as? Double {
                    time = time / 1000
                    let createdDate = Date(timeIntervalSince1970: time)
                    cell.lblDateTime.text = self.convertDateToString(format: "dd MMM yyyy hh:mm a", date: createdDate)
                }
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.05, execute: {
                    cell.bgView.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 5)
                })
                
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressCell(tapGesture:)))
                cell.bgView.addGestureRecognizer(longGesture)
                
                return cell
            }
        }
        else {
            if chatDetails[kMessageType] as? String == MESSAGETYPE.Image.rawValue {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OpponentImageCell") as! OpponentImageCell
                
                if let imageUrl = chatDetails[kUserImage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                if let userName = chatDetails[kUserName] as? String {
                    cell.lblUserName.text = userName
                }
                
                if let imageUrl = chatDetails[kMessage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgPhoto.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                if var time = chatDetails[kTime] as? Double {
                    time = time / 1000
                    let createdDate = Date(timeIntervalSince1970: time)
                    cell.lblDateTime.text = self.convertDateToString(format: "dd MMM yyyy hh:mm a", date: createdDate)
                }
                
                cell.imgVerify.isHidden = (chatDetails["is_verified"] as? String ?? "") == kYes ? false : true
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.05, execute: {
                    cell.bgView.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 5)
                })
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OpponentCell") as! OpponentCell
                
                if let imageUrl = chatDetails[kUserImage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                if let userName = chatDetails[kUserName] as? String {
                    cell.lblUserName.text = userName
                }
                
                if let message = chatDetails[kMessage] as? String {
                    cell.lblMessage.text = message
                }
                
                if var time = chatDetails[kTime] as? Double {
                    time = time / 1000
                    let createdDate = Date(timeIntervalSince1970: time)
                    cell.lblDateTime.text = self.convertDateToString(format: "dd MMM yyyy hh:mm a", date: createdDate)
                }
                
                cell.imgVerify.isHidden = (chatDetails["is_verified"] as? String ?? "") == kYes ? false : true
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.05, execute: {
                    cell.bgView.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 5)
                })
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatDetails = chatList[indexPath.row]
        
        if chatDetails[kMessageType] as? String == MESSAGETYPE.Image.rawValue {
            if let imageUrl = chatDetails[kMessage] as? String {
                let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
                viewController.imageUrl = imageUrl
                self.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
    //UITableView Tap Gesture Action
    @objc func longPressCopyText(tapGesture: UIGestureRecognizer) {
        let position = tapGesture.location(in: tblChat)
        if let indexPath = tblChat.indexPathForRow(at: position) {
            if tapGesture.state == .ended {
                if let cell = tblChat.cellForRow(at: indexPath) as? OpponentCell {
                    UIPasteboard.general.string = cell.lblMessage.text ?? ""
                    Toast.show(message: self.getLocalizeTextForKey(keyName: "lbl_TextCopied"), controller: self)
                }
            }
        }
    }
    
    @objc func longPressCell(tapGesture: UIGestureRecognizer) {
        let position = tapGesture.location(in: tblChat)
        if let indexPath = tblChat.indexPathForRow(at: position) {
            if tapGesture.state == .began {
                let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "lbl_DeleteMessage"), preferredStyle: .alert)
                alert.view.tintColor = UIColor().alertButtonColor
                
                let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Yes"), style: .default, handler: { (action) in
                    if let messageId = self.chatList[indexPath.row][kMessageId] as? Int {
                        print(messageId)
                        self.deleteMessage(messageId: messageId, indexPath: indexPath)
                    }
                })
                
                let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_No"), style: .default, handler: { (action) in
                })
                alert.addAction(noAction)
                alert.addAction(yesAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

extension ChatViewController: UIGestureRecognizerDelegate {
    private func gestureRecognizerShouldBegin(_ panGestureRecognizer: UIPanGestureRecognizer) -> Bool {
        let velocity = panGestureRecognizer.velocity(in: tblChat)
        print("velocity:\(velocity)")
        if velocity.x < 0 {
            print("return false")
            return false
        }
        print(abs(Float(velocity.x)) > abs(Float(velocity.y)))
        return abs(Float(velocity.x)) > abs(Float(velocity.y))
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension ChatViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
//        self.btnSend.isHidden = false
//        self.btnSticker.isHidden = true
//        self.btnAudio.isHidden = true
        if textView.textColor == UIColor.lightGray {
            textView.textColor = .black
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
//        self.btnSend.isHidden = true
//        self.btnSticker.isHidden = false
//        self.btnAudio.isHidden = false
        
        if textView.text == "" {
            textView.textColor = .lightGray
            textView.text = placeholder
        }
    }
}

extension ChatViewController {
    func getChatList(isShowHud: Bool, isScrollToBottom: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kPage: currentPage, kPageSize: PAGESIZECHAT] as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.CHATLIST.rawValue, method: .post, parameters: param, completion: { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.chatList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let chats = responseData[kData] as? [[String : AnyObject]] {
                        for chat in chats {
                            self.chatList.insert(chat, at: 0)
                        }
                    }
                    
                    self.scrollToRow = self.chatList.count
                    
                    self.tblChat.reloadData()
                    
                    if isScrollToBottom {
                        self.scrollToBottom()
                    }
                    else {
                        self.scrollToBottom(animate: false, row: (self.chatList.count / self.currentPage) - 1, scrollPosition: .top)
                    }
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            })
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func uploadMedia(mediaType: String, selectedMedia: UIImage) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "Groupchat[\(kMessageType)]": mediaType]
            
            KRProgressHUD.show()
            Alamofire.upload(multipartFormData: { multipartFormData in
                if let imageData = selectedMedia.jpegData(compressionQuality: 0.9) {
                    multipartFormData.append(imageData, withName: "Groupchat[\(kMessage)]", fileName: "UploadMedia.png", mimeType: "image/png")
                }
                
                for (key, value) in param {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, usingThreshold: 1, to: baseURLWithAuth + APINAME.SENDMESSAGE.rawValue, method: .post, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress { progress in
                        print(progress.fractionCompleted)
                    }
                    upload.responseJSON(completionHandler: { response in
                        KRProgressHUD.dismiss()
                        print(response)
                        switch response.result {
                        case .success:
                            if let json: NSDictionary = response.result.value as? NSDictionary {
                                if json.value(forKey: kStatus) as? Int == 1 {
                                    if let data = json.value(forKey: kData) as? [String: AnyObject] {
                                        self.chatList.append(data)
                                        
                                        self.tblChat.reloadData()
                                        self.scrollToBottom()
                                        
                                        //Send in Socket
                                        self.sendMessageSocket(data: data)
                                    }
                                }
                                else {
                                    if let message = json[kMessage] as? String {
                                        UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                                    }
                                    else {
                                        UIAlertController().alertViewWithTitleAndMessage(self, message: ErrorMessage)
                                    }
                                }
                            }
                            else {
                                UIAlertController().alertViewWithTitleAndMessage(self, message: ErrorMessage)
                            }
                        case .failure(let error):
                            KRProgressHUD.dismiss()
                            UIAlertController().alertViewWithTitleAndMessage(self, message: error.localizedDescription)
                        }
                    })
                case .failure(let error):
                    KRProgressHUD.dismiss()
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error.localizedDescription)
                }
            })
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func sendMessage(message: String, messageType: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "Groupchat[message_type]": messageType, "Groupchat[message]": message] as [String : Any]
            
            APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.SENDMESSAGE.rawValue, method: .post, parameters: param, completion: { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String: AnyObject] {
                        self.chatList.append(data)
                        
                        self.tblChat.reloadData()
                        self.scrollToBottom()
                        
                        //Send in Socket
                        self.sendMessageSocket(data: data)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            })
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func deleteMessage(messageId: Int, indexPath: IndexPath) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kMessageId: messageId] as [String : Any]
            
            APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.DELETEMESSAGE.rawValue, method: .post, parameters: param, completion: { (response, error) in
                if error == nil {
                    self.chatList.remove(at: indexPath.row)
                    self.tblChat.deleteRows(at: [indexPath], with: .right)
                    
                    self.deleteMessageSocket(messageId: messageId, indexPath: indexPath)
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            })
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    //Socket API Call
    func sendMessageSocket(data: [String: AnyObject]) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kData: data] as [String : Any]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: param, options: [])
            let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            
            if jsonString != nil {
                SocketIOManager.sharedInstance.socketApiCall(isShowHud: false, methodName: APINAME.SOCKET_SENDMESSAGE.rawValue, paramJsonString: jsonString!, { (response) in
                    print(response)
                })
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func deleteMessageSocket(messageId: Int, indexPath: IndexPath) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kMessageId: messageId] as [String : Any]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: param, options: [])
            let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            
            if jsonString != nil {
                SocketIOManager.sharedInstance.socketApiCall(isShowHud: false, methodName: APINAME.SOCKET_DELETEMESSAGE.rawValue, paramJsonString: jsonString!, { (response) in
                    print(response)
                })
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}

class OwnerCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblMessage: UITextView!
    @IBOutlet weak var lblDateTime: UILabel!
}

class OwnerImageCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblDateTime: UILabel!
}

class OpponentCell: UITableViewCell {
    @IBOutlet weak var imgUser: CustomImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblMessage: UITextView!
    @IBOutlet weak var lblDateTime: UILabel!
}

class OpponentImageCell: UITableViewCell {
    @IBOutlet weak var imgUser: CustomImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblDateTime: UILabel!
}

extension TimeInterval {
    var minuteSecondMS: String {
        return String(format:"%2d:%02d", minute, second)
    }
    
    var minute: Int {
        return Int((self/60).truncatingRemainder(dividingBy: 60))
    }
    
    var second: Int {
        return Int(truncatingRemainder(dividingBy: 60))
    }
    
    var millisecond: Int {
        return Int((self*1000).truncatingRemainder(dividingBy: 100))
    }
}

extension Int {
    var msToSeconds: Double {
        return Double(self) / 1000
    }
}
