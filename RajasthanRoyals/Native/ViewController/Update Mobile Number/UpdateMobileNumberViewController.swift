//
//  ReferFriendViewController.swift
//  CV Builder
//
//  Created by Sagar Nandha on 26/06/2019.
//  Copyright © 2019 AmitMacBook. All rights reserved.
//

import UIKit

protocol UpdateMobileNumberDelegate {
    func updateMobileNumber()
}

class UpdateMobileNumberViewController: BaseViewController {

    @IBOutlet weak var lblEnterMobileNo: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var txtCountryCode: CustomTextField!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var lblPrivacyPolicy: UILabel!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    var delegate: UpdateMobileNumberDelegate!
    var countryName = "INDIA"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let selIndex = CountryManager.shared.countries.firstIndex { (($0 as Country).countryName == "India")}!
        CountryManager.shared.lastCountrySelected = CountryManager.shared.countries[selIndex]
        txtCountryCode.text = CountryManager.shared.countries[selIndex].dialingCode
        
        self.setupLocalizationText()
    }
    
    func setupLocalizationText() {
        self.lblEnterMobileNo.text = self.getLocalizeTextForKey(keyName: "Signup_placeholder_MobileNumber")
        self.lblMobileNo.text = self.getLocalizeTextForKey(keyName: "Signup_lbl_MobileNumber")
        self.txtMobileNumber.placeholder = self.getLocalizeTextForKey(keyName: "Signup_placeholder_MobileNumber")
        self.lblPrivacyPolicy.text = self.getLocalizeTextForKey(keyName: "ValidationMessage_MobileNoMessage")
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "btn_Submit"), for: .normal)
    }
    
    @IBAction func btnClose_Clicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCountryCode_Clicked(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
            self.txtCountryCode.text = country.dialingCode
            self.countryName = country.countryName.uppercased()
        }
        countryController.detailColor = UIColor.black
        countryController.labelColor = UIColor.black
        countryController.isHideFlagImage = false
        countryController.isHideDiallingCode = false
    }
    
    @IBAction func btnPrivacyPolicy_Clicked(_ sender: Any) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        viewController.strTitle = "Privacy Policy"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnSubmit_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        if txtMobileNumber.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_MobileNumber"))
        }
        else if txtMobileNumber.text!.count < 8 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidMobileNumber"))
        }
        else {
            self.updateMobileNumber()
        }
    }
}

extension UpdateMobileNumberViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtCountryCode.isFirstResponder {
            _ = txtMobileNumber.becomeFirstResponder()
        }
        else if txtMobileNumber.isFirstResponder {
            _ = txtMobileNumber.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtMobileNumber {
            return range.location < 14
        }
        return true
    }
}

extension UpdateMobileNumberViewController {
    func updateMobileNumber() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let phoneCode = txtCountryCode.text!.replace("+", replacement: "")
            
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "Appuser[\(kPhoneCode)]": phoneCode,
                         "Appuser[\(kPhoneNumber)]": txtMobileNumber.text!,
                         "Appuser[\(kCountryName)]": countryName,
                         "Appuser[is_phonenumber]": "Yes"]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.UPDATEPROFILE.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                            BaseViewController.sharedInstance.callRegisterCustomEventAPI()
                        }
                        
                        self.dismiss(animated: true, completion: nil)
                        if self.delegate != nil {
                            self.delegate.updateMobileNumber()
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
