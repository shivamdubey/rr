//
//  SlideMenuViewController.swift
//  RR
//
//  Created by Sagar on 27/02/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class LeftMenuViewController: BaseViewController {
    
    @IBOutlet weak var btnLanguage: UIButton!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var lblFollowRR: UILabel!
    
    var menuList = [String]()
    var menuImagesList = [String]()
    
    var homeViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "LeftMenuScreen")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if BaseViewController.sharedInstance.appLanguage == APPLANGUAGE.ENGLISH.rawValue {
            self.btnLanguage.isSelected = false
        }
        else {
            self.btnLanguage.isSelected = true
        }
        
        self.setupLocalizationText()
        
        self.tblMenu.separatorStyle = .none
        self.tblMenu.reloadData()
    }
    
    func setupLocalizationText() {
        self.lblFollowRR.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_FollowRR")
        
        menuList = [self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Home"), self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Latest"), self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_FanLounge"), self.getLocalizeTextForKey(keyName: "Profile_btn_EarnedPoints"), self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Wallpaper"),  self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_RRPlayers"), self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_RRFixtureList"), self.getLocalizeTextForKey(keyName: "RightMenu_lbl_IndianStore"), self.getLocalizeTextForKey(keyName: "RightMenu_lbl_InternationalStore")]
        
        menuImagesList = ["home_ic", "media_ic", "super_royals_ic", "royals_coins_ic", "wallpaper_ic",  "player_ic", "fixture_ic", "shop_ic", "shop_ic"]
    }
    
    @IBAction func btnChangeLanguage_Clicked(_ sender: UIButton) {
        if BaseViewController.sharedInstance.appLanguage == APPLANGUAGE.ENGLISH.rawValue {
            BaseViewController.sharedInstance.appLanguage = APPLANGUAGE.HINDI.rawValue
        }
        else {
            BaseViewController.sharedInstance.appLanguage = APPLANGUAGE.ENGLISH.rawValue
        }
        
        AlertViewTitle = self.getLocalizeTextForKey(keyName: "AppName")
        NoInternet = self.getLocalizeTextForKey(keyName: "lbl_NoInternet")
        ErrorMessage = self.getLocalizeTextForKey(keyName: "lbl_ErrorMessage")
        
        defaults.setValue(BaseViewController.sharedInstance.appLanguage, forKey: kAppLanguage)
        defaults.synchronize()
        
        self.autoLogin()
        self.navigateToHomeScreen()
    }
    
    @IBAction func btnSocialMedia_Clicked(_ sender: UIButton) {
        if sender.tag == 101 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["facebook"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
        else if sender.tag == 102 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["instagram"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
        else if sender.tag == 103 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["tiktok"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
        else if sender.tag == 104 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["youtube"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
        else if sender.tag == 105 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["twitter"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
        else if sender.tag == 106 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["snapchat"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension LeftMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblMenu.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        cell.lblMenu.text = self.menuList[indexPath.row]
        cell.imgMenu.image = UIImage(named: self.menuImagesList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuName = self.menuList[indexPath.row]
        if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Home") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Latest") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_MomentsOfTheMatch") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "MomentsOfTheMatchViewController") as! MomentsOfTheMatchViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Press") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PressViewController") as! PressViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_RRPlayers") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PlayerListViewController") as! PlayerListViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_RRPlayerStats") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PlayerStatsViewController") as! PlayerStatsViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_RRFixtureList") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FixtureListViewController") as! FixtureListViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_PointsTable") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PointsTableViewController") as! PointsTableViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Venues") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "StaduimViewController") as! StaduimViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_FanLounge") {
            self.slideMenuController()?.closeLeft()
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SuperRoyalsViewController") as! SuperRoyalsViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Podcast") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PodcastViewController") as! PodcastViewController
            //self.slideMenuController()?.changeMainViewController(viewController, close: true)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_Wallpaper") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "WallpaperViewController") as! WallpaperViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "Profile_btn_EarnedPoints") {
            self.slideMenuController()?.closeLeft()
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "LoyalityPointsViewController") as! LoyalityPointsViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_IndianStore") {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "IndianStore")
            if let shopUrl = BaseViewController.sharedInstance.appDelegate.generalSettings["shop_url"] as? String {
                if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                    let newShopUrl = "\(shopUrl)?c_email=\(email)"
                    self.openUrlInSafariViewController(urlString: newShopUrl)
                }
            }
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_InternationalStore") {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "InternationalStore")
            self.openUrlInSafariViewController(urlString: BaseViewController.sharedInstance.appDelegate.generalSettings["international_shop_url"] as? String ?? "")
        }
    }
        
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
}
