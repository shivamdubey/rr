//
//  SlideMenuViewController.swift
//  RR
//
//  Created by Sagar on 27/02/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class RightMenuViewController: BaseViewController {
    
    @IBOutlet weak var btnLanguage: UIButton!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var lblFollowRR: UILabel!
    
    var menuList = [String]()
    var menuImagesList = [String]()
    
    var homeViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "RIghtMenuScreen")
        
        self.tblMenu.separatorStyle = .none
        self.tblMenu.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if BaseViewController.sharedInstance.appLanguage == APPLANGUAGE.ENGLISH.rawValue {
            self.btnLanguage.isSelected = false
        }
        else {
            self.btnLanguage.isSelected = true
        }
        
        self.setupLocalizationText()
    }
    
    func setupLocalizationText() {
        self.lblFollowRR.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_FollowRR")
//        "RightMenu_lbl_Home" = "Home";
//        "RightMenu_lbl_Royals" = "Royals Coins";
//        "RightMenu_lbl_Fan" = "Fan Lounge";
//        "RightMenu_lbl_Podcast" = "Podcasts";
//        "RightMenu_lbl_Wallpaper" = "Wallpapers";
//        "RightMenu_lbl_ArFilter" = "AR Filters";
//        "RightMenu_lbl_Fixture" = "Fixtures";
//        "RightMenu_lbl_Player" = "Players";
        menuList = [self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Home"), self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Royals"), self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Fan"),  self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Wallpaper"),  self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Fixture"), self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Education"), self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Foundation"),self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Player"), self.getLocalizeTextForKey(keyName: "RightMenu_lbl_ReferFriend"), self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Settings"), self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Logout")]
        
        menuImagesList = ["Group 16", "noun-coins-2165718", "Symbol 77 – 6", "Photos", "Icon material-date-range",  "university_ic", "foundation_ic", "noun-player-2062361", "referfriend_ic", "setting_ic", "logout_ic"]
        
        self.tblMenu.reloadData()
    }
    
    @IBAction func btnChangeLanguage_Clicked(_ sender: UIButton) {
        if BaseViewController.sharedInstance.appLanguage == APPLANGUAGE.ENGLISH.rawValue {
            BaseViewController.sharedInstance.appLanguage = APPLANGUAGE.HINDI.rawValue
        }
        else {
            BaseViewController.sharedInstance.appLanguage = APPLANGUAGE.ENGLISH.rawValue
        }
        
        AlertViewTitle = self.getLocalizeTextForKey(keyName: "AppName")
        NoInternet = self.getLocalizeTextForKey(keyName: "lbl_NoInternet")
        ErrorMessage = self.getLocalizeTextForKey(keyName: "lbl_ErrorMessage")
        
        defaults.setValue(BaseViewController.sharedInstance.appLanguage, forKey: kAppLanguage)
        defaults.synchronize()
        
        self.autoLogin()
        self.navigateToHomeScreen()
    }
    
    @IBAction func btnSocialMedia_Clicked(_ sender: UIButton) {
        if sender.tag == 101 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["facebook"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
        else if sender.tag == 102 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["instagram"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
        else if sender.tag == 103 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["tiktok"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
        else if sender.tag == 104 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["youtube"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
        else if sender.tag == 105 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["twitter"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
        else if sender.tag == 106 {
            if let urlString = BaseViewController.sharedInstance.appDelegate.generalSettings["snapchat"] as? String {
                self.openUrlInSafariViewController(urlString: urlString)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension RightMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblMenu.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        cell.lblMenu.text = self.menuList[indexPath.row]
        cell.imgMenu.image = UIImage(named: self.menuImagesList[indexPath.row])
        
        return cell
    }
   
    //        "RightMenu_lbl_Home" = "Home";
    //        "RightMenu_lbl_Royals" = "Royals Coins";
    //        "RightMenu_lbl_Fan" = "Fan Lounge";
    //        "RightMenu_lbl_Podcast" = "Podcasts";
    //        "RightMenu_lbl_Wallpaper" = "Wallpapers";
    //        "RightMenu_lbl_ArFilter" = "AR Filters";
    //        "RightMenu_lbl_Fixture" = "Fixtures";
    //        "RightMenu_lbl_Player" = "Players";
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuName = self.menuList[indexPath.row]
        if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Home") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_IndianStore") {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "IndianStore")
            if let shopUrl = BaseViewController.sharedInstance.appDelegate.generalSettings["shop_url"] as? String {
                if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                    let newShopUrl = "\(shopUrl)?c_email=\(email)"
                    self.openUrlInSafariViewController(urlString: newShopUrl)
                }
            }
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Royals") {
           // self.callTMAnalyticsAPI(category: "Page", action: "View", label: "IndianStore")
            self.slideMenuController()?.closeLeft()
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "LoyalityPointsViewController") as! LoyalityPointsViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Fan") {
           // self.callTMAnalyticsAPI(category: "Page", action: "View", label: "IndianStore")
            self.slideMenuController()?.closeLeft()
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SuperRoyalsViewController") as! SuperRoyalsViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Podcast") {
           // self.callTMAnalyticsAPI(category: "Page", action: "View", label: "IndianStore")
            self.slideMenuController()?.closeLeft()
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PodcastViewController") as! PodcastViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
           // self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Wallpaper") {
            //self.callTMAnalyticsAPI(category: "Page", action: "View", label: "IndianStore")
            self.slideMenuController()?.closeLeft()
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "WallpaperViewController") as! WallpaperViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
           // self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_ArFilter") {
            //self.callTMAnalyticsAPI(category: "Page", action: "View", label: "IndianStore")
        
        }
        
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Fixture") {
            self.slideMenuController()?.closeLeft()
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FixtureListViewController") as! FixtureListViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Player") {
            self.slideMenuController()?.closeLeft()
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PlayerListViewController") as! PlayerListViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
            
        }
        
        
        
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_InternationalStore") {
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "InternationalStore")
            self.openUrlInSafariViewController(urlString: BaseViewController.sharedInstance.appDelegate.generalSettings["international_shop_url"] as? String ?? "")
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Foundation") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FoundationViewController") as! FoundationViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Education") {
            self.addFirebaseCustomeEvent(eventName: "EducationScreen")
            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Education")
            self.openUrlInSafariViewController(urlString: "https://www.rajasthanroyals.com/education")
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_ThePavillion") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "ThePavillionViewController") as! ThePavillionViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_ChangePassword") {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_ReferFriend") {
            let settingsVC = self.storyBoard.instantiateViewController(withIdentifier: "ReferFriendViewController") as! ReferFriendViewController
            self.slideMenuController()?.changeMainViewController(settingsVC, close: true)
           // self.navigationController?.pushViewController(settingsVC, animated: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Settings") {
            let settingsVC = self.storyBoard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.slideMenuController()?.changeMainViewController(settingsVC, close: true)
        }
        else if menuName == self.getLocalizeTextForKey(keyName: "RightMenu_lbl_Logout") {
            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "lbl_LogoutMessage"), preferredStyle: .alert)
            alert.view.tintColor = UIColor().alertButtonColor
            
            let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Logout"), style: .default, handler: { (action) in
                self.userLogout()
            })
            
            let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: .default, handler: { (action) in
            })
            
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
}

class MenuCell: UITableViewCell {
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!
}

extension RightMenuViewController {
    func userLogout() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.LOGOUT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    self.addFirebaseCustomeEvent(eventName: "Logout")
                    
                    self.clearAllUserDataFromPreference()
                    self.navigateToLoginScreen()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
