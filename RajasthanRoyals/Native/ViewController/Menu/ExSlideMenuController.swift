//
//  ExSlideMenuController.swift
//  RR
//
//  Created by Sagar on 27/02/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class ExSlideMenuController : SlideMenuController {
    
    /**
     List of View Controllers which will open on click from Left Side Menu
     */
//    override func isTagetViewController() -> Bool {
//        if let vc = UIApplication.topViewController() {
//            if vc is HomeViewController {
//                return true
//            }
//        }
//        return false
//    }

    /**
     This method is used to track events that are associated with left side menu. It will **Track Action** in console. 
     */
    override func track(_ trackAction: TrackAction) {
        switch trackAction {
        case .leftTapOpen:
            print("TrackAction: left tap open.")
        case .leftTapClose:
            print("TrackAction: left tap close.")
        case .leftFlickOpen:
            print("TrackAction: left flick open.")
        case .leftFlickClose:
            print("TrackAction: left flick close.")
        case .rightTapOpen:
            print("TrackAction: right tap open.")
        case .rightTapClose:
            print("TrackAction: right tap close.")
        case .rightFlickOpen:
            print("TrackAction: right flick open.")
        case .rightFlickClose:
            print("TrackAction: right flick close.")
        }
    }
}
