//
//  CustomMenuViewController.swift
//  RaniCircle
//
//  Created by Apple on 23/09/20.
//  Copyright © 2020 YellowPanther. All rights reserved.
//

import UIKit
@objc protocol CustomMenuDelegate {
    @objc optional func editDeleteReportFeedData(strAction:String, selIndex: Int, comment: String)
    @objc optional func editDeleteReportFeedCommentRowData(strAction:String, selIndex: Int,  selSection: Int, comment: String)
    @objc optional func editDeleteReportFeedCommentSectionData(strAction:String, selIndex: Int, comment: String)
    @objc optional func otherUserActionData(strAction:String)
    @objc optional func dismissCustomMenu()
    @objc optional func actionForChatDetailsData(strAction:String)
}

class CustomMenuViewController: BaseViewController {
    
    var delegateCustomMenu: CustomMenuDelegate?
    
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var constHeightForTblMenu: NSLayoutConstraint!
    @IBOutlet weak var constWidthForTblMenu: NSLayoutConstraint!
    
    var menuList = [String]()
    var selectedIndex = -1
    var selectedSection = -1
    var strFromVC = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblMenu.reloadData()
        self.constHeightForTblMenu.constant = CGFloat(self.menuList.count * Int(VERTICALMENU_SIZE.HEIGHT.rawValue))
        
        if self.strFromVC == "Other Profile" || self.strFromVC == "FeedListNaviMenu" {
            self.constWidthForTblMenu.constant = VERTICALMENU_SIZE.WIDTH.rawValue + 25
        }
        else {
            self.constWidthForTblMenu.constant = VERTICALMENU_SIZE.WIDTH.rawValue
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.delegateCustomMenu?.dismissCustomMenu?()
    }
    
}

//MARK: - UITableView Delegate & Data source Methods -
extension CustomMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomMenuCell", for: indexPath) as! CustomMenuCell
        
        let menuTitle = self.menuList[indexPath.row]
        cell.lblMenu.text = menuTitle
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuTitle = self.menuList[indexPath.row]
        let presentVC = self.presentingViewController
        self.dismiss(animated: true) {
            if self.strFromVC == "FeedList" {
                if menuTitle == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete") {
                    self.delegateCustomMenu?.editDeleteReportFeedData?(strAction: menuTitle, selIndex: self.selectedIndex, comment: "")
                }
                else if menuTitle == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportPost") {
                    let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AddReportViewController") as? AddReportViewController
                    viewController!.modalPresentationStyle = .overCurrentContext
                    viewController!.delegateCustomMenu = self.delegateCustomMenu
                    viewController!.pickerShowStr = "4"
                    viewController!.strReport = REPORT_TYPE.POSTREPORT.rawValue
                    viewController!.selectedIndex = self.selectedIndex
                    presentVC!.present(viewController!, animated: true)
                }
            }
            else if self.strFromVC == "FeedDetails" {
                if menuTitle == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete") {
                    self.delegateCustomMenu?.editDeleteReportFeedData?(strAction: menuTitle, selIndex: self.selectedIndex, comment: "")
                }
                else if menuTitle == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportPost") {
                    let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AddReportViewController") as? AddReportViewController
                    viewController!.modalPresentationStyle = .overCurrentContext
                    viewController!.delegateCustomMenu = self.delegateCustomMenu
                    viewController!.pickerShowStr = "4"
                    viewController!.strReport = REPORT_TYPE.POSTREPORT.rawValue
                    viewController!.selectedIndex = self.selectedIndex
                    presentVC!.present(viewController!, animated: true)
                }
            }
            else if self.strFromVC == "FeedDetailsCellRow" {
                if menuTitle == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete") {
                    self.delegateCustomMenu?.editDeleteReportFeedCommentRowData?(strAction: menuTitle, selIndex: self.selectedIndex, selSection: self.selectedSection, comment: "")
                }
                else if menuTitle == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportReply") {
                    let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AddReportViewController") as? AddReportViewController
                    viewController!.modalPresentationStyle = .overCurrentContext
                    viewController!.delegateCustomMenu = self.delegateCustomMenu
                    viewController!.pickerShowStr = "4"
                    viewController!.strReport = REPORT_TYPE.COMMENTREPORT.rawValue
                    viewController!.selectedIndex = self.selectedIndex
                    viewController!.selectedSection = self.selectedSection
                    presentVC!.present(viewController!, animated: true)
                }
            }
            else if self.strFromVC == "FeedDetailsCellSection" {
                if menuTitle == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete") {
                    self.delegateCustomMenu?.editDeleteReportFeedCommentSectionData?(strAction: menuTitle, selIndex: self.selectedIndex, comment: "")
                }
                else if menuTitle == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportComment") {
                    let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AddReportViewController") as? AddReportViewController
                    viewController!.modalPresentationStyle = .overCurrentContext
                    viewController!.delegateCustomMenu = self.delegateCustomMenu
                    viewController!.pickerShowStr = "4"
                    viewController!.strReport = REPORT_TYPE.COMMENTREPORT.rawValue
                    viewController!.selectedIndex = self.selectedIndex
                    presentVC!.present(viewController!, animated: true)
                }
            }
            else if self.strFromVC == "CommentReplyRow" {
                if menuTitle == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete") {
                    self.delegateCustomMenu?.editDeleteReportFeedCommentRowData?(strAction: menuTitle, selIndex: self.selectedIndex, selSection: self.selectedSection, comment: "")
                }
                else if menuTitle == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportReply") {
                    let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AddReportViewController") as? AddReportViewController
                    viewController!.modalPresentationStyle = .overCurrentContext
                    viewController!.delegateCustomMenu = self.delegateCustomMenu
                    viewController!.pickerShowStr = "4"
                    viewController!.strReport = REPORT_TYPE.COMMENTREPORT.rawValue
                    viewController!.selectedIndex = self.selectedIndex
                    viewController!.selectedSection = self.selectedSection
                    presentVC!.present(viewController!, animated: true)
                }
            }
            else if self.strFromVC == "StoryView" {
                self.delegateCustomMenu?.editDeleteReportFeedData?(strAction: menuTitle, selIndex: self.selectedIndex, comment: "")
            }
            else if self.strFromVC == "ChatDetails" {
                self.delegateCustomMenu?.actionForChatDetailsData?(strAction: menuTitle)
            }
            else if self.strFromVC == "FeedListNaviMenu" {
                self.delegateCustomMenu?.editDeleteReportFeedData?(strAction: menuTitle, selIndex: self.selectedIndex, comment: "")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 44
    }
    
}

class CustomMenuCell: UITableViewCell {
    @IBOutlet weak var lblMenu: UILabel!
}
