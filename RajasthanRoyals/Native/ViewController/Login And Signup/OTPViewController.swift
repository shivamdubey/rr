//
//  OTPViewController.swift
//  RR
//
//  Created by Sagar on 15/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class OTPViewController: BaseViewController {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblTitleOTP: UILabel!
    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var btnSubmit: CustomButton!
    @IBOutlet weak var btnResendOTP: UIButton!
    @IBOutlet weak var btnChangeMobiileNumber: UIButton!
    @IBOutlet weak var verifyMessageEnglishView: UIView!
    @IBOutlet weak var verifyMessageHindiView: UIView!
    
    var isOTPEntered = false
    var otpNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if BaseViewController.sharedInstance.appLanguage == APPLANGUAGE.ENGLISH.rawValue {
            self.verifyMessageEnglishView.isHidden = false
            self.verifyMessageHindiView.isHidden = true
        }
        else {
            self.verifyMessageEnglishView.isHidden = true
            self.verifyMessageHindiView.isHidden = false
        }
        
        self.addFirebaseCustomeEvent(eventName: "OTPScreen")
        
        self.setupLocalizationText()
        
        self.setOTPView()
    }
    
    func setupLocalizationText() {
        self.lblTitleOTP.text = self.getLocalizeTextForKey(keyName: "OTP_lbl_Title")
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "OTP_btn_Verify"), for: .normal)
        self.btnResendOTP.setTitle(self.getLocalizeTextForKey(keyName: "OTP_btn_ResedOTP"), for: .normal)
        self.btnChangeMobiileNumber.setTitle(self.getLocalizeTextForKey(keyName: "OTP_btn_ChangeMobiileNumber"), for: .normal)
    }
    
    func setOTPView() {
        otpView.otpFieldSize = 60
        otpView.otpFieldsCount = 4
        otpView.otpFieldDisplayType = .circular
        otpView.cursorColor = UIColor.white
        otpView.otpFieldDefaultBorderColor = UIColor.white
        otpView.otpFieldEnteredBorderColor = UIColor.white
        otpView.tintColor = UIColor.white
        otpView.otpFieldFont = UIFont(name: "Futura-Medium", size: 28)!
        otpView.delegate = self
        otpView.becomeFirstResponder()
        
        //Create the UI
        otpView.initalizeUI()
    }
    
    @IBAction func btnWhatsApp_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        viewController.strTitle = self.getLocalizeTextForKey(keyName: "Settings_lbl_WhatsappPolicy")
        viewController.strValueForAPI = "Whatsapp Policy"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnSubmit_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        if !self.isOTPEntered {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_OTP"))
        }
        else {
            self.verifyOtp()
        }
    }
    
    @IBAction func btnResendOTP_Clicked(_ button: UIButton) {
        self.resendOtp()
    }
    
    @IBAction func btnChangeMobiileNumber_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "UpdateMobileNumberViewController") as! UpdateMobileNumberViewController
        viewController.modalPresentationStyle = .overCurrentContext
        self.present(viewController, animated: true, completion: nil)
    }
}

extension OTPViewController {
    func verifyOtp() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kOTP: otpNumber]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.OTPVERIFY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func resendOtp() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey]
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.RESENDOTP.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    if let message = responseData[kMessage] as? String {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension OTPViewController: VPMOTPViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) {
        self.isOTPEntered = hasEntered
    }
    
    func enteredOTP(otpString: String) {
        print("OTPString: \(otpString)")
        otpNumber = otpString
    }
}
