//
//  ViewController.swift
//  RR
//
//  Created by Sagar on 10/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit
import KRProgressHUD
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookCore
import FacebookLogin
import Firebase
import TwitterKit
import Alamofire
import KRProgressHUD
import Google
import GoogleSignIn
import AuthenticationServices
import Batch

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblTitleLogin: UILabel!
    @IBOutlet weak var lblLoginVia: UILabel!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var btnApple: UIButton!
    @IBOutlet weak var lblOR: UILabel!
    @IBOutlet weak var txtEmailAddress: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLogin: CustomButton!
    @IBOutlet weak var privacyPolicyTermsConditionsEnglishView: UIView!
    @IBOutlet weak var privacyPolicyTermsConditionsHindiView: UIView!
    @IBOutlet weak var btnSignUp: CustomButton!
    
    @IBOutlet weak var btnGoogleCenterHorizontalConstraint: NSLayoutConstraint!
    
    //Social Login
    var socailLoginType = ""
    var socialId = ""
    var firstName = ""
    var lastName = ""
    var emailId = ""
    var profilePicture = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
   /*
        if BaseViewController.sharedInstance.appLanguage == APPLANGUAGE.ENGLISH.rawValue {
            self.privacyPolicyTermsConditionsEnglishView.isHidden = false
            self.privacyPolicyTermsConditionsHindiView.isHidden = true
        }
        else {
            self.privacyPolicyTermsConditionsEnglishView.isHidden = true
            self.privacyPolicyTermsConditionsHindiView.isHidden = false
        }
        */
        self.addFirebaseCustomeEvent(eventName: "LoginScreen")
        
        if #available(iOS 13.0, *) {
        }
        else {
            self.btnGoogleCenterHorizontalConstraint.constant = 0
            self.btnApple.isHidden = true
        }
        
       // self.setLeftSideImageInTextField(textField: self.txtEmailAddress, image: #imageLiteral(resourceName: "email_ic"))
        //self.setLeftSideImageInTextField(textField: self.txtPassword, image: #imageLiteral(resourceName: "password_ic"))
        //self.btnSignUp.roundCorners([.topLeft, .topRight], radius: 10)
        
        self.setupLocalizationText()
        
        //Social Media
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    

    
    func setupLocalizationText() {
        self.lblTitleLogin.text = self.getLocalizeTextForKey(keyName: "Login_lbl_Login")
       // self.lblLoginVia.text = self.getLocalizeTextForKey(keyName: "Login_lbl_LoginVia")
        self.lblOR.text = self.getLocalizeTextForKey(keyName: "Login_lbl_Or")
        self.txtEmailAddress.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Login_placeholder_EmailAddress"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        self.txtPassword.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Login_placeholder_Password"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        self.btnForgotPassword.setTitle(self.getLocalizeTextForKey(keyName: "Login_btn_ForgoPassword"), for: .normal)
        self.btnLogin.setTitle(self.getLocalizeTextForKey(keyName: "Login_btn_LetsPlay"), for: .normal)
        self.btnSignUp.setTitle(self.getLocalizeTextForKey(keyName: "Login_btn_SignupNow"), for: .normal)
    }
    
    @IBAction func btnFacebook_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        self.socailLoginType = "Facebook"
        self.loginWithFacebook()
    }
    
    @IBAction func btnGoogle_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        self.socailLoginType = "Google"
        GIDSignIn.sharedInstance().clientID = googleClientId
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func btnTwitter_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        self.socailLoginType = "Twitter"
        TWTRTwitter.sharedInstance().logIn { session, error in
            if session != nil {
                print("signed in as \(session!.userName)")
                print("signed in userid is \(session!.userID)")

                self.getEmailOfCurrentUserFromTwitter()
            }
            else {
                print("error: \(error!.localizedDescription)");
            }
        }
    }
    
    @IBAction func btnApple_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        self.socailLoginType = "Apple"
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
        else {
        }
    }
    
    @IBAction func btnLogin_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        if txtEmailAddress.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_EmailAddress"))
        }
        else if !self.isValidEmail(str: txtEmailAddress.text!) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidEmailAddress"))
        }
        else if txtPassword.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Password"))
        }
        else if txtPassword.text!.count < 6 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_PasswordToShort"))
        }
        else {
            self.userLogin()
        }
    }
    
    @IBAction func btnForgotPassword_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnPrivacyPolicy_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        viewController.strTitle = self.getLocalizeTextForKey(keyName: "Settings_lbl_PrivacyPolicy")
        viewController.strValueForAPI = "Privacy Policy"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnTermsAndCondition_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        viewController.strTitle = self.getLocalizeTextForKey(keyName: "Settings_lbl_TCs")
        viewController.strValueForAPI = "T&Cs"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnSignUp_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension LoginViewController {
    func userLogin() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["Appuser[\(kEmail)]": txtEmailAddress.text!,
                         "Appuser[\(kPassword)]": txtPassword.text!,
                         "Appuser[\(kDeviceId)]": UIDevice().deviceID,
                         "Appuser[\(kDeviceName)]": UIDevice().modelName,
                         "Appuser[\(kAppVersion)]": APP_VERSION,
                         "Appuser[\(kDeviceType)]": "Iphone",
                         "Appuser[\(kDeviceTokenAPI)]": BaseViewController.sharedInstance.appDelegate.deviceToken,
                         "Appuser[batch_pushtoken]": BatchPush.lastKnownPushToken() ?? ""]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.LOGIN.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                        
                        if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                            let editor = BatchUser.editor()
                            editor.setIdentifier(email)
                            editor.save()
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                            if userDetails[kIsUserDataStoreInCRM] as? String == kNo {
                                self.storeUserDataOnCRM()
                            }
                            else {
                                self.updateUserDataOnCRM()
                            }
                        }
                        
                        self.navigateToHomeScreen()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func userSocialLogin(socialId: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["Appuser[\(socialId)]": self.socialId,
                         "Appuser[\(kEmail)]": self.emailId,
                         "Appuser[\(kLoginType)]": self.socailLoginType,
                         "Appuser[\(kDeviceId)]": UIDevice().deviceID,
                         "Appuser[\(kDeviceName)]": UIDevice().modelName,
                         "Appuser[\(kAppVersion)]": APP_VERSION,
                         "Appuser[\(kDeviceType)]": "Iphone",
                         "Appuser[\(kDeviceTokenAPI)]": BaseViewController.sharedInstance.appDelegate.deviceToken,
                         "Appuser[batch_pushtoken]": BatchPush.lastKnownPushToken() ?? ""]
            
            KRProgressHUD.show(withMessage: "Please wait")
            
            print("API URL = \(baseURLWithoutAuth + APINAME.SOCIALLOGIN.rawValue) parameters = \(param)")
            
            Alamofire.request(baseURLWithoutAuth + APINAME.SOCIALLOGIN.rawValue, method: .post, parameters: param).validate().responseJSON { response in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    KRProgressHUD.dismiss()
                }
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary {
                        print(json)
                        if json[kStatus] as? Int == kIsSuccess {
                            if let userDetails = json[kData] as? [String : AnyObject] {
                                self.saveUserDetails(userDetails: userDetails)
                                
                                if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                                    let editor = BatchUser.editor()
                                    editor.setIdentifier(email)
                                    editor.save()
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                                    if userDetails[kIsUserDataStoreInCRM] as? String == kNo {
                                        self.storeUserDataOnCRM()
                                    }
                                    else {
                                        self.updateUserDataOnCRM()
                                    }
                                }
                                
                                self.navigateToHomeScreen()
                            }
                        }
                        else if json[kStatus] as? Int == kUserNotFound {
                            if let message = json[kMessage] as? String {
                                let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                                alert.view.tintColor = UIColor().alertButtonColor
                                
                                let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                    BaseViewController.sharedInstance.clearAllUserDataFromPreference()
                                    BaseViewController.sharedInstance.navigateToLoginScreen()
                                })
                                alert.addAction(hideAction)
                                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                            }
                        }
                        else if json[kStatus] as? Int == kNewUser {
                            self.userSignup()
                        }
                        else {
                            UIAlertController().alertViewWithTitleAndMessage(self, message: json[kMessage] as? String ?? ErrorMessage)
                        }
                    }
                    else {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: ErrorMessage)
                    }
                case .failure(let error):
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error.localizedDescription)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func userSignup() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            var param = ["Appuser[\(kEmail)]": self.emailId,
                         "Appuser[\(kPassword)]": "",
                         "Appuser[\(kFirstName)]": self.firstName,
                         "Appuser[\(kLastName)]": self.lastName,
                         "Appuser[\(kLoginType)]": self.socailLoginType,
                         "Appuser[\(kDeviceId)]": UIDevice().deviceID,
                         "Appuser[\(kDeviceName)]": UIDevice().modelName,
                         "Appuser[\(kAppVersion)]": APP_VERSION,
                         "Appuser[\(kDeviceType)]": "Iphone",
                         "Appuser[\(kDeviceTokenAPI)]": BaseViewController.sharedInstance.appDelegate.deviceToken,
                         "Appuser[batch_pushtoken]": BatchPush.lastKnownPushToken() ?? ""]
            
            if self.socailLoginType != "Normal" {
                param["Appuser[\(self.socailLoginType.lowercased())_id]"] = self.socialId
            }
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.SIGNUP.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                        
                        if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                            let editor = BatchUser.editor()
                            editor.setIdentifier(email)
                            editor.save()
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                            if userDetails[kIsUserDataStoreInCRM] as? String == kNo {
                                self.storeUserDataOnCRM()
                            }
                            else {
                                self.updateUserDataOnCRM()
                            }
                        }
                        
                        let viewController = self.newStoryBoard.instantiateViewController(withIdentifier: "FavouritePlayerViewController") as! FavouritePlayerViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtEmailAddress.isFirstResponder {
            _ = txtPassword.becomeFirstResponder()
        }
        else if txtPassword.isFirstResponder {
            _ = txtPassword.resignFirstResponder()
        }
        return true
    }
}

//Facebook
extension LoginViewController {
    func loginWithFacebook() {
        if AccessToken.current == nil {
            let loginManager = LoginManager()
            loginManager.logIn(permissions: [.publicProfile, .email], viewController: self, completion: { (loginResult) in
                switch loginResult {
                case .failed(let error):
                    print(error.localizedDescription)
                case .cancelled:
                    print("User cancelled login.")
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    print("\(grantedPermissions)")
                    print("\(declinedPermissions)")
                    print("\(accessToken)")
                    
                    if grantedPermissions.contains("email") || grantedPermissions.contains("publicProfile") {
                        print("Logged in!")
                        
                        self.getFBUserData()
                    }
                }
            })
        }
        else {
            print("Current Access Token is:\(String(describing: AccessToken.current))")
            self.getFBUserData()
        }
    }
    
    func getFBUserData() {
        let params = ["fields": "email, id, name, first_name, last_name, picture"]
        let request = GraphRequest.init(graphPath: "me", parameters: params)
        request.start { (connection, result, error) in
            if error != nil {
                UIAlertController().alertViewWithTitleAndMessage(self, message: error!.localizedDescription)
                return
            }
            
            // Handle vars
            if let result = result as? [String: Any] {
                if let socialId = result["id"] as? String {
                    self.socialId = socialId
                }
                
                if let firstName = result["first_name"] as? String {
                    self.firstName = firstName
                }
                
                if let lastName = result["last_name"] as? String {
                    self.lastName = lastName
                }
                
                if let email = result["email"] as? String {
                    self.emailId = email
                }
                
                // Add this lines for get image
                if let picture = result["picture"] as? NSDictionary {
                    if let data = picture["data"] as? NSDictionary {
                        if let profilePicture = data[kUrl] as? String {
                            self.profilePicture = profilePicture
                        }
                    }
                }
                
                self.userSocialLogin(socialId: "facebook_id")
            }
        }
    }
}

//Twitter
extension LoginViewController {
    func getEmailOfCurrentUserFromTwitter() {
        let client = TWTRAPIClient.withCurrentUser()
        let request = client.urlRequest(withMethod: "GET", urlString: "https://api.twitter.com/1.1/account/verify_credentials.json",
                                        parameters: ["include_email": "true", "skip_status": "true"],
                                        error: nil)
        
        client.requestEmail(forCurrentUser: { email, error in
            if error == nil {
                print("Email of Current User is: \(String(describing: email))")
            }
            else {
                print("Error in Getting Email: \(String(describing: error?.localizedDescription))")
            }
        })
        
        client.sendTwitterRequest(request, completion: { response, data, error in
            if error == nil {
                print("Response for Email is \(String(describing: response))")
                if data == nil {
                    return
                }
                
                if let datastring = String(data: data!, encoding: String.Encoding.utf8) {
                    print(datastring)
                    if let dataDictionary = self.convertStringToDictionary(text: datastring) {
                        if let twitterID = dataDictionary["id_str"] as? String {
                            self.socialId = twitterID
                        }
                        
                        if let fullName = dataDictionary["name"] as? String {
                            let array = fullName.components(separatedBy: " ")
                            if array.count > 1 {
                                self.firstName = array[0]
                            }
                            if array.count >= 2 {
                                self.lastName = array[1]
                            }
                        }
                        
                        if let twitterProfileURL = dataDictionary["profile_image_url_https"] as? String {
                            self.profilePicture = twitterProfileURL
                        }
                        
                        self.userSocialLogin(socialId: "twitter_id")
                    }
                }
            }
            else {
                print("Error is \(String(describing: error?.localizedDescription))")
                UIAlertController().alertViewWithTitleAndMessage(self, message: error!.localizedDescription)
            }
        })
    }
}

extension LoginViewController: GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            if let userId = user.userID {
                self.socialId = userId
            }
            
            if let firstName = user.profile.givenName {
                self.firstName = firstName
            }
            
            if let lastName = user.profile.familyName {
                self.lastName = lastName
            }
            
            if let email = user.profile.email {
                self.emailId = email
            }
            
            if GIDSignIn.sharedInstance()!.currentUser.profile.hasImage {
                self.profilePicture = user.profile.imageURL(withDimension: 300)!.absoluteString
            }
            
            self.userSocialLogin(socialId: "google_id")
        }
        else {
            print("\(error.localizedDescription)")
            UIAlertController().alertViewWithTitleAndMessage(self, message: "\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
    }
}

@available(iOS 13.0, *)
extension LoginViewController: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            self.socialId = appleIDCredential.user
            self.firstName = appleIDCredential.fullName?.givenName ?? ""
            self.lastName = appleIDCredential.fullName?.familyName ?? ""
            self.emailId = appleIDCredential.email ?? ""
            
            if self.firstName != "" {
                KeychainService.saveInfo(key: "FirstName", value: self.firstName as NSString)
            }
            else {
                self.firstName = KeychainService.getInfo(key: "FirstName") as String? ?? ""
            }
            
            if self.lastName != "" {
                KeychainService.saveInfo(key: "LastName", value: self.lastName as NSString)
            }
            else {
                self.lastName = KeychainService.getInfo(key: "LastName") as String? ?? ""
            }
            
            if self.emailId != "" {
                KeychainService.saveInfo(key: "Email", value: self.emailId as NSString)
            }
            else {
                self.emailId = KeychainService.getInfo(key: "Email") as String? ?? ""
            }
            
            self.userSocialLogin(socialId: "apple_id")
        }
        else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            let userName = passwordCredential.user
            let password = passwordCredential.password

            print(userName, password)
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
}

@available(iOS 13.0, *)
extension LoginViewController: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
