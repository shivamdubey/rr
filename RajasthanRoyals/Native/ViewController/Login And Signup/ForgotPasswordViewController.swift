//
//  ForgotPasswordViewController.swift
//  RR
//
//  Created by Sagar on 13/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {

    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblTitleForgotPassword: UILabel!
    @IBOutlet weak var lblEmailDescription: UILabel!
    @IBOutlet weak var txtEmailAddress: CustomTextField!
    @IBOutlet weak var btnSubmit: CustomButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "ForgotPasswordScreen")
        
        self.setLeftSideImageInTextField(textField: self.txtEmailAddress, image: #imageLiteral(resourceName: "email_ic"))
        
        self.setupLocalizationText()
    }
    
    func setupLocalizationText() {
        self.lblTitleForgotPassword.text = self.getLocalizeTextForKey(keyName: "ForgotPassword_lbl_ForgotPassword")
        self.lblEmailDescription.text = self.getLocalizeTextForKey(keyName: "ForgotPassword_lbl_Message")
        self.txtEmailAddress.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Login_placeholder_EmailAddress"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "btn_Submit"), for: .normal)
    }

    @IBAction func btnSubmit_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        if self.txtEmailAddress.text?.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_EmailAddress"))
        }
        else if !self.isValidEmail(str: self.txtEmailAddress.text!) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidEmailAddress"))
        }
        else {
            self.forgotPassword()
        }
    }
}

extension ForgotPasswordViewController {
    func forgotPassword() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["Appuser[\(kEmail)]": self.txtEmailAddress.text!]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.FORGOTPASSWORD.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let message = responseData[kMessage] as? String {
                        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                        alert.view.tintColor = UIColor().alertButtonColor
                        
                        let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                            self.navigationController?.popViewController(animated: true)
                        })
                        alert.addAction(hideAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtEmailAddress.isFirstResponder {
            txtEmailAddress.resignFirstResponder()
        }
        return true
    }
}
