//
//  SignUpViewController.swift
//  RR
//
//  Created by Sagar on 13/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblTitleSignUp: UILabel!
    @IBOutlet weak var txtFirstName: CustomTextField!
    @IBOutlet weak var txtLastName: CustomTextField!
    @IBOutlet weak var txtEmailAddress: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var passwordSeparetorView: UIView!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var btnNext: CustomButton!
    @IBOutlet weak var privacyPolicyTermsConditionsEnglishView: UIView!
    @IBOutlet weak var privacyPolicyTermsConditionsHindiView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var txtPasswordTopConstraint: NSLayoutConstraint!
    
    //Captcha
    @IBOutlet weak var viewCaptcha: CustomView!
    @IBOutlet weak var txtCaptcha: UITextField!
    @IBOutlet weak var btnReloadCaptcha: UIButton!
    @IBOutlet weak var viewInnerCaptcha: UIView!
    @IBOutlet weak var lblCaptcha: UILabel!
    
    var captchaElements = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    
    var loginType = "Normal"
    
    var socialId = ""
    var firstName = ""
    var lastName = ""
    var emailId = ""
    var profilePicture = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if BaseViewController.sharedInstance.appLanguage == APPLANGUAGE.ENGLISH.rawValue {
            self.privacyPolicyTermsConditionsEnglishView.isHidden = false
            self.privacyPolicyTermsConditionsHindiView.isHidden = true
        }
        else {
            self.privacyPolicyTermsConditionsEnglishView.isHidden = true
            self.privacyPolicyTermsConditionsHindiView.isHidden = false
        }
        
        self.addFirebaseCustomeEvent(eventName: "SignupScreen")
        
        self.setLeftSideImageInTextField(textField: self.txtFirstName, image: #imageLiteral(resourceName: "person_ic"))
        self.setLeftSideImageInTextField(textField: self.txtLastName, image: #imageLiteral(resourceName: "person_ic"))
        self.setLeftSideImageInTextField(textField: self.txtEmailAddress, image: #imageLiteral(resourceName: "email_ic"))
        self.setLeftSideImageInTextField(textField: self.txtPassword, image: #imageLiteral(resourceName: "password_ic"))
        
        self.setupLocalizationText()
        
        if self.loginType != "Normal" {
            self.txtPasswordTopConstraint.constant = -40
            self.txtPassword.isHidden = true
            self.passwordSeparetorView.isHidden = true
            
            self.txtFirstName.text = firstName
            self.txtLastName.text = lastName
            self.txtEmailAddress.text = emailId
        }
        
        self.loadCaptcha()
    }
    
    func setupLocalizationText() {
        self.lblTitleSignUp.text = self.getLocalizeTextForKey(keyName: "Signup_lbl_Signup")
        self.txtFirstName.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_placeholder_FirstName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtLastName.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_placeholder_LastName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtEmailAddress.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Login_placeholder_EmailAddress"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtPassword.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Login_placeholder_Password"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.txtCaptcha.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_placeholder_CaptchaCode"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.btnNext.setTitle(self.getLocalizeTextForKey(keyName: "Login_btn_Signup"), for: .normal)
    }
    
    @IBAction func btnPassword_Clicked(_ button: UIButton) {
        self.btnPassword.isSelected = !self.btnPassword.isSelected
        self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
    }
    
    @IBAction func btnReloadCaptcha_Clicked(_ sender: Any) {
        self.loadCaptcha()
    }
    
    @IBAction func btnPrivacyPolicy_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        viewController.strTitle = self.getLocalizeTextForKey(keyName: "Settings_lbl_PrivacyPolicy")
        viewController.strValueForAPI = "Privacy Policy"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnTermsAndCondition_Clicked(_ button: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        viewController.strTitle = self.getLocalizeTextForKey(keyName: "Settings_lbl_TCs")
        viewController.strValueForAPI = "T&Cs"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnNext_Clicked(_ button: UIButton) {
        self.view.endEditing(true)
        if txtFirstName.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_FirstName"))
        }
        else if txtLastName.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_LastName"))
        }
        else if txtEmailAddress.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_EmailAddress"))
        }
        else if !self.isValidEmail(str: txtEmailAddress.text!) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidEmailAddress"))
        }
        else if txtPassword.text?.trim().count == 0 && self.loginType == "Normal" {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Password"))
        }
        else if txtPassword.text!.count < 6 && self.loginType == "Normal" {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_PasswordToShort"))
        }
        else if txtCaptcha.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Captcha"))
        }
        else if txtCaptcha.text != lblCaptcha.text {
            self.loadCaptcha()
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidCaptcha"))
        }
        else {
            self.userSignup()
        }
    }
    
    func loadCaptcha() {
        self.txtCaptcha.text = ""
        
        var elmt1: Int = Int(arc4random()) % captchaElements.count
        var elmt2: Int = Int(arc4random()) % captchaElements.count
        var elmt3: Int = Int(arc4random()) % captchaElements.count
        var elmt4: Int = Int(arc4random()) % captchaElements.count
        var elmt5: Int = Int(arc4random()) % captchaElements.count
        var elmt6: Int = Int(arc4random()) % captchaElements.count
        
        if elmt1 <= 0 {
            elmt1 = 1
        }
        if elmt2 <= 0 {
            elmt2 = 1
        }
        if elmt3 <= 0 {
            elmt3 = 1
        }
        if elmt4 <= 0 {
            elmt4 = 1
        }
        if elmt5 <= 0 {
            elmt5 = 1
        }
        if elmt6 <= 0 {
            elmt6 = 1
        }
        
        let captchaStr = "\(captchaElements[elmt1 - 1])\(captchaElements[elmt2 - 1])\(captchaElements[elmt3 - 1])\(captchaElements[elmt4 - 1])\(captchaElements[elmt5 - 1])\(captchaElements[elmt6 - 1])"
        self.lblCaptcha.text = captchaStr
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtFirstName.isFirstResponder {
            _ = txtLastName.becomeFirstResponder()
        }
        else if txtLastName.isFirstResponder {
            _ = txtEmailAddress.becomeFirstResponder()
        }
        else if txtEmailAddress.isFirstResponder {
            _ = txtPassword.becomeFirstResponder()
        }
        else if txtPassword.isFirstResponder {
            _ = txtCaptcha.becomeFirstResponder()
        }
        else if txtCaptcha.isFirstResponder {
            _ = txtCaptcha.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCaptcha {
            if string == " " {
                return false
            }
        }
        return true
    }
}

extension SignUpViewController {
    func userSignup() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            var param = ["Appuser[\(kEmail)]": txtEmailAddress.text!,
                         "Appuser[\(kPassword)]": txtPassword.text!,
                         "Appuser[\(kFirstName)]": txtFirstName.text!,
                         "Appuser[\(kLastName)]": txtLastName.text!,
                         "Appuser[\(kLoginType)]": self.loginType,
                         "Appuser[\(kDeviceId)]": UIDevice().deviceID,
                         "Appuser[\(kDeviceName)]": UIDevice().modelName,
                         "Appuser[\(kAppVersion)]": APP_VERSION,
                         "Appuser[\(kDeviceType)]": "Iphone",
                         "Appuser[\(kDeviceTokenAPI)]": BaseViewController.sharedInstance.appDelegate.deviceToken]
            
            if self.loginType != "Normal" {
                param["Appuser[\(loginType.lowercased())_id]"] = self.socialId
            }
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.SIGNUP.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                        
                        if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                            let editor = BatchUser.editor()
                            editor.setIdentifier(email)
                            editor.save()
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                            if userDetails[kIsUserDataStoreInCRM] as? String == kNo {
                                self.storeUserDataOnCRM()
                            }
                            else {
                                self.updateUserDataOnCRM()
                            }
                        }
                        
                        let viewController = self.newStoryBoard.instantiateViewController(withIdentifier: "FavouritePlayerViewController") as! FavouritePlayerViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
