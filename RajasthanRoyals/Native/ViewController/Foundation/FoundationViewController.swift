//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class FoundationViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lblBottomText: UILabel!
    @IBOutlet weak var btnContatUs: CustomButton!
    @IBOutlet weak var btnDonateNow: CustomButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "FoundationScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Foundation")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Foundation_lbl_Title")
        self.btnContatUs.setTitle(self.getLocalizeTextForKey(keyName: "Foundation_btn_ContactUs"), for: .normal)
        self.btnDonateNow.setTitle(self.getLocalizeTextForKey(keyName: "Foundation_btn_DonateNow"), for: .normal)
        self.lblBottomText.text = self.getLocalizeTextForKey(keyName: "Foundation_lbl_Description")
    }
    
    @IBAction func btnUrl_Clicked(_ sender: Any) {
        self.openUrlInSafariViewController(urlString: "https://www.rajasthanroyals.com/foundation")
    }
    
    @IBAction func btnContatUs_Clicked(_ sender: Any) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FoundationContactUsViewController") as! FoundationContactUsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnDonateNow_Clicked(_ sender: Any) {
        self.openUrlInSafariViewController(urlString: "https://www.rajasthanroyals.com/donation")
    }
}
