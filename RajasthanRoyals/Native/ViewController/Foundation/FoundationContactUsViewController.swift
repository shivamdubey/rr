//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class FoundationContactUsViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtFirstName: CustomTextField!
    @IBOutlet weak var txtLastName: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtCountryCode: CustomTextField!
    @IBOutlet weak var txtMobileNo: CustomTextField!
    @IBOutlet weak var txtOrganization: CustomTextField!
    @IBOutlet weak var txtMessage: CustomTextView!
    @IBOutlet weak var btnSubmit: CustomButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "FoundationContactUsScreen")
        
        self.txtMessage.textContainerInset = UIEdgeInsets(top: 5, left: 15, bottom: 0, right: 0)
        
        self.setupLocalizationText()
        
        self.setUserDetails()
        
        self.txtMessage.textColor = UIColor.white.withAlphaComponent(0.5)
        self.txtMessage.text = self.getLocalizeTextForKey(keyName: "Feedback_lbl_WriteYourMessage")
    }
    
    func setupLocalizationText() {
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Foundation_lbl_Title")
        self.txtFirstName.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_lbl_FirstName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtLastName.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_lbl_LastName"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtEmail.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Login_lbl_EmailAddress"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtMobileNo.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Signup_lbl_MobileNumber"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.txtOrganization.attributedPlaceholder = NSAttributedString(string: self.getLocalizeTextForKey(keyName: "Foundation_lbl_Organization"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "btn_Submit"), for: .normal)
    }
    
    //Set User Details
    func setUserDetails() {
        if let firstName = BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String {
            self.txtFirstName.text = firstName
        }
        
        if let lastName = BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String {
            self.txtLastName.text = lastName
        }
        
        if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
            self.txtEmail.text = email
        }
        
        if let phoneCode = BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneCode] as? Int {
            self.txtCountryCode.text = "+\(phoneCode)"
        }
        else {
            let selIndex = CountryManager.shared.countries.firstIndex { (($0 as Country).countryName == "India")}!
            CountryManager.shared.lastCountrySelected = CountryManager.shared.countries[selIndex]
            self.txtCountryCode.text = CountryManager.shared.countries[selIndex].dialingCode
        }
        
        if let phoneNo = BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneNumber] as? String {
            self.txtMobileNo.text = phoneNo
        }
    }
    
    @IBAction func btnCountryCode_Clicked(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
            self.txtCountryCode.text = country.dialingCode
        }
        countryController.detailColor = UIColor.black
        countryController.labelColor = UIColor.black
        countryController.isHideFlagImage = false
        countryController.isHideDiallingCode = false
    }
    
    @IBAction func btnSubmit_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        if txtFirstName.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_FirstName"))
        }
        else if txtLastName.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_LastName"))
        }
        else if txtEmail.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_EmailAddress"))
        }
        else if !self.isValidEmail(str: txtEmail.text!) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidEmailAddress"))
        }
        else if txtMobileNo.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_MobileNumber"))
        }
        else if txtMobileNo.text!.count < 8 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_ValidMobileNumber"))
        }
        else if txtOrganization.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_OrganizationName"))
        }
        else if txtMessage.text!.trim().count == 0 || self.txtMessage.textColor == UIColor.white.withAlphaComponent(0.5) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Message"))
        }
        else {
            self.foundationContactUs()
        }
    }
}

extension FoundationContactUsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtFirstName.isFirstResponder {
            _ = txtLastName.becomeFirstResponder()
        }
        else if txtLastName.isFirstResponder {
            _ = txtEmail.becomeFirstResponder()
        }
        else if txtEmail.isFirstResponder {
            _ = txtMobileNo.becomeFirstResponder()
        }
        else if txtMobileNo.isFirstResponder {
            _ = txtOrganization.becomeFirstResponder()
        }
        return true
    }
}

extension FoundationContactUsViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.white.withAlphaComponent(0.5) {
            self.txtMessage.textColor = UIColor.white
            self.txtMessage.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trim().isEmpty {
            self.txtMessage.textColor = UIColor.white.withAlphaComponent(0.5)
            self.txtMessage.text = self.getLocalizeTextForKey(keyName: "Feedback_lbl_WriteYourMessage")
        }
    }
}

extension FoundationContactUsViewController {
    func foundationContactUs() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let phoneCode = txtCountryCode.text!.replace("+", replacement: "")
            
            let param = ["Foundationcontactusinquiry[first_name]": txtFirstName.text!,
                         "Foundationcontactusinquiry[last_name]": txtLastName.text!,
                         "Foundationcontactusinquiry[email]": txtEmail.text!,
                         "Foundationcontactusinquiry[mobile_no]": "\(phoneCode)\(txtMobileNo.text!)",
                         "Foundationcontactusinquiry[organisation]": txtOrganization.text!,
                         "Foundationcontactusinquiry[message]": txtMessage.text!] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.FOUNDATIONCONTACTUS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let message = responseData[kMessage] as? String {
                        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                        alert.view.tintColor = UIColor().alertButtonColor
                        
                        let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                            self.navigationController?.popViewController(animated: true)
                        })
                        alert.addAction(hideAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
