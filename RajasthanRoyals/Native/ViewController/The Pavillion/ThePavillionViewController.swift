//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class ThePavillionViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "ThePavilionScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Pavillion")
    }
    
    @IBAction func btnUrl_Clicked(_ sender: Any) {
        self.openUrlInSafariViewController(urlString: "https://bit.ly/2G8WCBL")
    }
}
