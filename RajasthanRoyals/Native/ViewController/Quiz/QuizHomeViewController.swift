//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class QuizHomeViewController: BaseViewController {
    
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var btnLeaderboard: CustomButton!
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var imgQuizRRLogo: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnHowToPlay: UIButton!
    @IBOutlet weak var btnTermsAndCondition: UIButton!
    @IBOutlet weak var imgPrice: UIImageView!
    @IBOutlet weak var lblWinPrizes: UILabel!
    @IBOutlet weak var btnPlayNow: CustomButton!

    var sponsorImage = ""
    var quizLogo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "QuizRRGameScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "QuizRR Game")
        
        self.lblWelcome.text = self.getLocalizeTextForKey(keyName: "Quiz_lbl_WelcomeTo")
        self.lblWinPrizes.text = self.getLocalizeTextForKey(keyName: "Quiz_lbl_WinRoyalMerchandise")
        self.btnHowToPlay.setTitle(self.getLocalizeTextForKey(keyName: "Quiz_Btn_HowToPlay"), for: .normal)
        self.btnTermsAndCondition.setTitle(self.getLocalizeTextForKey(keyName: "Settings_lbl_TCs"), for: .normal)
        self.btnPlayNow.setTitle(self.getLocalizeTextForKey(keyName: "Quiz_Btn_PlayNow"), for: .normal)
        
        self.getQuizGeneralDetails()
    }
    
    @IBAction func btnLeaderboard_Clicked(_ sender: Any) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "QuizLeaderboardViewController") as! QuizLeaderboardViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnHowToPlay_Clicked(_ sender: Any) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "QuizFAQViewController") as! QuizFAQViewController
        viewController.isHowToPlay = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnTermsAndCondition_Clicked(_ sender: Any) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "QuizFAQViewController") as! QuizFAQViewController
        viewController.isHowToPlay = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnPlayNow_Clicked(_ sender: Any) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "QuizCategoryViewController") as! QuizCategoryViewController
        viewController.quizLogo = self.quizLogo
        viewController.sponsorImage = self.sponsorImage
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension QuizHomeViewController {
    func getQuizGeneralDetails() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETQUIZAPPGENERAL.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        if let imageUrl = data["header_image"] as? String {
                            if let url = URL(string: imageUrl) {
                                self.imgHeader.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                            }
                        }
                        
                        if let imageUrl = data["quiz_logo"] as? String {
                            self.quizLogo = imageUrl
                            if let url = URL(string: imageUrl) {
                                self.imgQuizRRLogo.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                            }
                        }
                        
                        if let message = data["empty_message"] as? String {
                            self.lblDescription.attributedText = message.html2Attributed
                        }
                        
                        self.sponsorImage = data["sponsor_image"] as? String ?? ""
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
