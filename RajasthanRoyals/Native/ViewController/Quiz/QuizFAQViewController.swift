//
//  HousieFAQViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 11/03/21.
//

import UIKit

class QuizFAQViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnHowToPlay: CustomButton!
    @IBOutlet weak var btnRules: CustomButton!
    @IBOutlet weak var btnFaqs: CustomButton!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var tblFAQs: UITableView!
    
    var isHowToPlay = true
    var faqList = [[String: AnyObject]]()
    var selectedFAQIndex = [Int]()
    var howToPlay = ""
    var rules = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        if self.isHowToPlay {
            self.btnHowToPlay_Clicked(sender: self.btnHowToPlay)
        }
        else {
            self.btnRules_Clicked(sender: self.btnRules)
        }
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Settings_lbl_FAQs")
        self.btnHowToPlay.setTitle(self.getLocalizeTextForKey(keyName: "Settings_lbl_HowToPlay"), for: .normal)
        self.btnRules.setTitle(self.getLocalizeTextForKey(keyName: "Settings_lbl_TCs"), for: .normal)
        self.btnFaqs.setTitle(self.getLocalizeTextForKey(keyName: "Settings_lbl_FAQs"), for: .normal)
        self.txtViewDescription.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        self.getFAQList()
    }
    
    @IBAction func btnHowToPlay_Clicked(sender: UIButton) {
        self.btnHowToPlay.backgroundColor = SelectedButtonBGColor
        self.btnRules.backgroundColor = DeSelectedButtonBGColor
        self.btnFaqs.backgroundColor = DeSelectedButtonBGColor
        
        self.btnHowToPlay.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnRules.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnFaqs.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.txtViewDescription.attributedText = howToPlay.html2Attributed
        
        self.tblFAQs.isHidden = true
        self.txtViewDescription.isHidden = false
    }
    
    @IBAction func btnRules_Clicked(sender: UIButton) {
        self.btnHowToPlay.backgroundColor = DeSelectedButtonBGColor
        self.btnRules.backgroundColor = SelectedButtonBGColor
        self.btnFaqs.backgroundColor = DeSelectedButtonBGColor
        
        self.btnHowToPlay.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnRules.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnFaqs.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.txtViewDescription.attributedText = self.rules.html2Attributed
        
        self.tblFAQs.isHidden = true
        self.txtViewDescription.isHidden = false
    }
    
    @IBAction func btnFaqs_Clicked(sender: UIButton) {
        self.btnHowToPlay.backgroundColor = DeSelectedButtonBGColor
        self.btnRules.backgroundColor = DeSelectedButtonBGColor
        self.btnFaqs.backgroundColor = SelectedButtonBGColor
        
        self.btnHowToPlay.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnRules.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnFaqs.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.tblFAQs.isHidden = false
        self.txtViewDescription.isHidden = true
    }
}

extension QuizFAQViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell") as! FAQCell
         
        if let question = faqList[indexPath.row][kQuestion] as? String {
            cell.lblQuestion.text = question
        }
        
        cell.lblAnswer.text = ""
        cell.imgArrow.image = #imageLiteral(resourceName: "arrow_left_ic").imageWithColor(color: .white)
        cell.lblAnswerTopConstraint.constant = -20
        
        if selectedFAQIndex.contains(indexPath.row) {
            cell.lblAnswerTopConstraint.constant = 5
            cell.imgArrow.image = #imageLiteral(resourceName: "arrow_down_ic").imageWithColor(color: .white)
            
            if let answer = faqList[indexPath.row][kAnswer] as? String {
                cell.lblAnswer.attributedText = answer.html2Attributed
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedFAQIndex.contains(indexPath.row) {
            selectedFAQIndex = selectedFAQIndex.filter({ $0 != indexPath.row })
        }
        else {
            selectedFAQIndex.append(indexPath.row)
        }
        self.tblFAQs.reloadRows(at: [indexPath], with: .none)
    }
}

extension QuizFAQViewController {
    func getFAQList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETQUIZAPPFAQ.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let faqs = responseData[kData] as? [[String : AnyObject]] {
                        self.faqList = faqs
                    }
                    
                    self.howToPlay = responseData["description"] as? String ?? ""
                    
                    self.rules = responseData["terms_conditions"] as? String ?? ""
                    
                    if self.isHowToPlay {
                        self.txtViewDescription.attributedText = self.howToPlay.html2Attributed
                    }
                    else {
                        self.txtViewDescription.attributedText = self.rules.html2Attributed
                    }
                    
                    self.tblFAQs.reloadData()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
