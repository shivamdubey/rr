//
//  QuizQuestionsViewController.swift
//  RR_New
//
//  Created by Apple on 12/06/21.
//

import UIKit
import SRCountdownTimer

class QuizQuestionsViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblQuestion: UITableView!
    
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var lblHoursValue: UILabel!
    @IBOutlet weak var lblMinutesValue: UILabel!
    @IBOutlet weak var lblSecondsValue: UILabel!
    
    @IBOutlet weak var lblQuestionNumber: UILabel!
    @IBOutlet weak var countdownTimerView: SRCountdownTimer!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomView1: UIView!
    @IBOutlet weak var imgSponsor: UIImageView!
    
    @IBOutlet weak var successPopupView: UIView!
    @IBOutlet weak var successInnerView: UIView!

    @IBOutlet weak var imgQuizRRLogo: UIImageView!
    @IBOutlet weak var lblCongratulations: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblScoreValue: UILabel!
    @IBOutlet weak var lblTimeTaken: UILabel!
    @IBOutlet weak var lblTimeTakenValue: UILabel!
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var lblShare: UILabel!
    @IBOutlet weak var leaderboardView: UIView!
    @IBOutlet weak var lblLeaderboard: UILabel!
    @IBOutlet weak var btnPlayAnotherCategory: CustomButton!
    
    @IBOutlet weak var zoomTimerView: UIView!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var timerViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewBottomConstraint: NSLayoutConstraint!

    var sponsorImage = ""
    var quizLogo = ""
    var isLock = ""
    var quizCategoryId = ""
    var selCategoryTitle = ""
    var quizAppTime = 0
    
    var questionList = [[String: Any]]()
    var selectedQuestionIndex = -1
    var timer: Timer!
    var counter = 1
    var startDate: Date?
    var userPlayedQuestion = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.shareView.setShadowOnView()
        self.leaderboardView.setShadowOnView()
        
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Quiz: Participation")
        
        self.lblQuestionNumber.isHidden = true
        self.countdownTimerView.isHidden = true
        self.tblQuestion.isHidden = true
        
        self.countdownTimerView.lineWidth = 5
        self.countdownTimerView.lineColor = .white
        self.countdownTimerView.trailLineColor = UIColor().themeDarkPinkColor
        self.countdownTimerView.isLabelHidden = false
        self.countdownTimerView.labelFont = UIFont.init(name: String().themeSemiBoldFontName, size: 22)
        self.countdownTimerView.labelTextColor = .white
        self.countdownTimerView.delegate = self
        
        self.lblScore.text = self.getLocalizeTextForKey(keyName: "Quiz_lbl_Score")
        self.lblTimeTaken.text = self.getLocalizeTextForKey(keyName: "Quiz_lbl_Time")
        self.lblShare.text = self.getLocalizeTextForKey(keyName: "Quiz_Btn_ShareResult")
        self.lblLeaderboard.text = self.getLocalizeTextForKey(keyName: "Quiz_Btn_ViewLeaderboard")
        self.btnPlayAnotherCategory.setTitle(self.getLocalizeTextForKey(keyName: "Quiz_Btn_PlayAnotherCategory"), for: .normal)
        
        self.lblTitle.text = self.selCategoryTitle.uppercased()
        
        if !self.sponsorImage.isEmpty {
            if let url = URL(string: self.sponsorImage) {
                self.imgSponsor.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        else {
            self.bottomViewBottomConstraint.constant = -self.bottomView.frame.height
            self.bottomView.isHidden = true
            self.bottomView1.isHidden = true
        }
        
        if let url = URL(string: self.quizLogo) {
            self.imgQuizRRLogo.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
        }
        
        if self.startDate != nil {
            let currentDate = Date().convertToSpacificTimeZone(timeZone: "Asia/Kolkata")
            self.counter = currentDate.seconds(from: self.startDate!)
        }

        self.getQuizQuestionList()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.bottomView.roundCorners([.topLeft, .topRight], radius: 20)
        }
    }
    
    @objc func updateTimer() {
        if self.counter > 0 {
            let (hours, minutes, seconds) = self.secondsToHoursMinutesSeconds(seconds: self.counter)
            
            self.lblHoursValue.text = String(format: "%02d", hours)
            self.lblMinutesValue.text = String(format: "%02d", minutes)
            self.lblSecondsValue.text = String(format: "%02d", seconds)
            
            self.counter = self.counter + 1
        }
    }
    
    func takeScreenshot(theView: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(theView.frame.size, true, 0.0)
        theView.drawHierarchy(in: theView.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage()
    }
     
    @IBAction func btnLeaderboard_Clicked(_ button: Any) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "QuizLeaderboardViewController") as! QuizLeaderboardViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnShare_Clicked(_ button: Any) {
        let activityVC = UIActivityViewController(activityItems: [self.takeScreenshot(theView: self.successInnerView)], applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
        
        let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "eventType": POINTSEVENTTYPE.QUIZ_SHARE.rawValue]
        self.callEarnPointsAPI(param: param)
        
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Quiz: Leaderboard Share on Social media")
    }
}

extension QuizQuestionsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.selectedQuestionIndex != -1 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.selectedQuestionIndex != -1 {
            if let answerList = questionList[self.selectedQuestionIndex]["quiz_app_question_answer"] as? [[String: Any]] {
                return answerList.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuizQuestionHeaderCell") as! QuizQuestionHeaderCell
        
        if self.isLock == kYes {
            self.lblQuestionNumber.text = "\(self.selectedQuestionIndex + 1)/\(questionList.count)"
        }
        else {
            self.lblQuestionNumber.text = "\(self.userPlayedQuestion + self.selectedQuestionIndex + 1)/\(questionList.count + self.userPlayedQuestion)"
        }
        
        cell.lblQuestion.text = questionList[self.selectedQuestionIndex]["title"] as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuizQuestionRowCell") as! QuizQuestionRowCell
        
        if let answerList = questionList[self.selectedQuestionIndex]["quiz_app_question_answer"] as? [[String: Any]] {
            if let answer = answerList[indexPath.row][kAnswer] as? String {
                cell.lblAnswer.text = answer
                cell.lblAnswer.textColor = UIColor.white
            }
        }
        
        if self.isLock == kNo {
            cell.bgView.backgroundColor = UIColor.white
            cell.lblAnswer.textColor = UIColor.black
            
            if let answerList = questionList[self.selectedQuestionIndex]["quiz_app_question_answer"] as? [[String: Any]] {
                if let quiz_app_question_id = (questionList[self.selectedQuestionIndex])["quiz_app_question_id"] as? Int,
                   let quiz_app_question_answer_id = (questionList[self.selectedQuestionIndex])["quiz_app_question_answer_id"] as? Int {
                    if let quiz_app_question_id_inner = answerList[indexPath.row]["quiz_app_question_id"] as? Int, let quiz_app_question_answer_id_inner = answerList[indexPath.row]["quiz_app_question_answer_id"] as? Int {
                        if quiz_app_question_id == quiz_app_question_id_inner && quiz_app_question_answer_id == quiz_app_question_answer_id_inner {
                            cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
                        }
                    }
                }
                
                cell.imgView.isHidden = true
            }
        }
        else {
            cell.bgView.backgroundColor = UIColor.white
            cell.lblAnswer.textColor = UIColor.black
            cell.imgView.isHidden = true
            
            if let answerList = questionList[self.selectedQuestionIndex]["quiz_app_question_answer"] as? [[String: Any]] {
                if answerList[indexPath.row]["is_trueanswer"] as? String == kYes && answerList[indexPath.row]["is_user_answer"] as? String == kYes {
                    cell.imgView.isHidden = false
                    cell.imgView.image = #imageLiteral(resourceName: "correct_green_ic")
                    cell.lblAnswer.textColor = UIColor.white
                    cell.bgView.backgroundColor = UIColor().themeDarkPinkColor
                }
                else if answerList[indexPath.row]["is_trueanswer"] as? String == kYes {
                    cell.imgView.isHidden = false
                    cell.imgView.image = #imageLiteral(resourceName: "correct_green_ic")
                    cell.lblAnswer.textColor = UIColor.white
                    cell.bgView.backgroundColor = UIColor().themeDarkPinkColor
                }
                else if answerList[indexPath.row]["is_user_answer"] as? String == kYes {
                    cell.imgView.isHidden = false
                    cell.imgView.image = #imageLiteral(resourceName: "wrong_ic")
                    cell.lblAnswer.textColor = UIColor.black
                    cell.bgView.backgroundColor = UIColor.white
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isLock == kNo {
            if let answerList = questionList[self.selectedQuestionIndex]["quiz_app_question_answer"] as? [[String: Any]] {
                let isLastQuestion = (self.questionList.count - 1) == self.selectedQuestionIndex ? true : false
                if let questionId = answerList[indexPath.row]["quiz_app_question_id"] as? Int, let answerId = answerList[indexPath.row]["quiz_app_question_answer_id"] as? Int {
                    self.countdownTimerView.pause()
                    self.setQuizAnswer(indexPath: indexPath, questionId: "\(questionId)", answerId: "\(answerId)", isLastQuestion: isLastQuestion)
                }
            }
        }
    }
}

class QuizQuestionHeaderCell: UITableViewCell {
    @IBOutlet weak var bgView: CustomView!
    @IBOutlet weak var lblQuestion: UILabel!
}

class QuizQuestionRowCell: UITableViewCell {
    @IBOutlet weak var bgView: CustomView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblAnswer: UILabel!
}

extension QuizQuestionsViewController {
    func startEndQuizTimer(isStartQuiz: Bool, isShowHud: Bool = false) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            var param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "Quizapptime[quiz_app_category_id]": self.quizCategoryId]

            if !isStartQuiz {
                param["quiz_app_time"] = "\(self.quizAppTime)"
            }
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.ADDUPDATEQUIZAPPTIME.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : Any]
                    
                    if isStartQuiz {
                        if let data = responseData[kData] as? [String: AnyObject] {
                            self.quizAppTime = data["quiz_app_time"] as? Int ?? 0
                        }
                    }
                    else {
                        if let data = responseData[kData] as? [String: AnyObject] {
                            let quizPoints = data["points"] as? Int ?? 0
                            let quizTime = data["total_time"] as? Int ?? 0
                            
                            self.lblScoreValue.text = "\(quizPoints)"
                            let (hours, minutes, seconds) = self.secondsToHoursMinutesSeconds(seconds: quizTime)
                            self.lblTimeTakenValue.text = String(format: "%02d", hours) + ":" + String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
                            
                            let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "categoryId": "\(self.quizCategoryId)", "quizPoints": "\(quizPoints)", "eventType": POINTSEVENTTYPE.QUIZ_COMPLETE.rawValue]
                            self.callEarnPointsAPI(param: param)
                            
                            self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Quiz: Completion")
                            
                            if let message = responseData[kMessage] as? String {
                                self.lblCongratulations.text = responseData[kTitle] as? String ?? ""
                                
                                self.lblResult.text = message
                                
                                self.successPopupView.isHidden = false
                                
                                if self.timer != nil {
                                    self.timer.invalidate()
                                    self.timer = nil
                                }
                            }
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getQuizQuestionList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "quiz_app_category_id": self.quizCategoryId]
                as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETQUIZAPPQUESTION.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : Any]
                    
                    if let dataList = responseData[kData] as? [[String : Any]] {
                        self.questionList = dataList
                        self.selectedQuestionIndex = 0
                    }
                    
                    self.tblQuestion.reloadData()
                    
                    if self.isLock == kNo {
                        self.zoomTimerView.isHidden = false
                        
                        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                            self.lblTime.transform = CGAffineTransform.identity.scaledBy(x: 1.5, y: 1.5)
                         }) { (finished) in
                             UIView.animate(withDuration: 1, animations: {
                                self.lblTime.transform = CGAffineTransform.identity.scaledBy(x: 0.5, y: 0.5)
                             }) { (finished) in
                                self.lblTime.text = "2"
                                UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                                       self.lblTime.transform = CGAffineTransform.identity.scaledBy(x: 1.5, y: 1.5)
                                 }) { (finished) in
                                     UIView.animate(withDuration: 1, animations: {
                                        self.lblTime.transform = CGAffineTransform.identity.scaledBy(x: 0.5, y: 0.5)
                                     }) { (finished) in
                                        self.lblTime.text = "1"
                                        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                                               self.lblTime.transform = CGAffineTransform.identity.scaledBy(x: 1.5, y: 1.5)
                                         }) { (finished) in
                                             UIView.animate(withDuration: 1, animations: {
                                                self.lblTime.transform = CGAffineTransform.identity.scaledBy(x: 0.5, y: 0.5)
                                             }) { (finished) in
                                                self.zoomTimerView.isHidden = true

                                                if self.startDate == nil {
                                                    self.startEndQuizTimer(isStartQuiz: true)
                                                }
                                                
                                                self.lblQuestionNumber.isHidden = false
                                                self.countdownTimerView.isHidden = false
                                                self.tblQuestion.isHidden = false
                                                
                                                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
                                                self.countdownTimerView.start(beginingValue: 15, interval: 1)
                                             }
                                        }
                                     }
                                }
                             }
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func setQuizAnswer(indexPath: IndexPath, questionId: String, answerId: String, isLastQuestion: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "quiz_app_question_id": questionId,
                         "quiz_app_question_answer_id": answerId,
                         "quiz_app_time": self.quizAppTime] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.SETQUIZAPPANSWER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if isLastQuestion {
                        if var answerList = self.questionList[self.selectedQuestionIndex]["quiz_app_question_answer"] as? [[String: Any]] {
                            answerList[indexPath.row]["is_user_answer"] = kYes as AnyObject
                            
                            for i in 0..<answerList.count {
                                let tmpIndexPath = IndexPath.init(row: i, section: indexPath.section)
                                if let cell = self.tblQuestion.cellForRow(at: tmpIndexPath) as? QuizQuestionRowCell {
                                    cell.imgView.isHidden = true
                                    cell.lblAnswer.textColor = UIColor.black
                                    
                                    if answerList[tmpIndexPath.row]["is_trueanswer"] as? String == kYes && answerList[tmpIndexPath.row]["is_user_answer"] as? String == kYes {
                                        cell.imgView.isHidden = false
                                        cell.imgView.image = #imageLiteral(resourceName: "correct_green_ic")
                                        cell.lblAnswer.textColor = UIColor.white
                                        cell.bgView.backgroundColor = UIColor().themeDarkPinkColor
                                    }
                                    else if answerList[tmpIndexPath.row]["is_trueanswer"] as? String == kYes {
                                        cell.imgView.isHidden = false
                                        cell.imgView.image = #imageLiteral(resourceName: "correct_green_ic")
                                        cell.lblAnswer.textColor = UIColor.white
                                        cell.bgView.backgroundColor = UIColor().themeDarkPinkColor
                                    }
                                    else if answerList[tmpIndexPath.row]["is_user_answer"] as? String == kYes {
                                        cell.imgView.isHidden = false
                                        cell.imgView.image = #imageLiteral(resourceName: "wrong_ic")
                                        cell.lblAnswer.textColor = UIColor.black
                                        cell.bgView.backgroundColor = UIColor.white
                                    }
                                }
                            }
                        }
                        
                        self.startEndQuizTimer(isStartQuiz: false)
                    }
                    else {
                        if var answerList = self.questionList[self.selectedQuestionIndex]["quiz_app_question_answer"] as? [[String: Any]] {
                            answerList[indexPath.row]["is_user_answer"] = kYes as AnyObject
                            
                            for i in 0..<answerList.count {
                                let tmpIndexPath = IndexPath.init(row: i, section: indexPath.section)
                                
                                if let cell = self.tblQuestion.cellForRow(at: tmpIndexPath) as? QuizQuestionRowCell {
                                    cell.imgView.isHidden = true
                                    cell.lblAnswer.textColor = UIColor.black
                                    
                                    if answerList[tmpIndexPath.row]["is_trueanswer"] as? String == kYes && answerList[tmpIndexPath.row]["is_user_answer"] as? String == kYes {
                                        cell.imgView.isHidden = false
                                        cell.imgView.image = #imageLiteral(resourceName: "correct_green_ic")
                                        cell.lblAnswer.textColor = UIColor.white
                                        cell.bgView.backgroundColor = UIColor().themeDarkPinkColor
                                    }
                                    else if answerList[tmpIndexPath.row]["is_trueanswer"] as? String == kYes {
                                        cell.imgView.isHidden = false
                                        cell.imgView.image = #imageLiteral(resourceName: "correct_green_ic")
                                        cell.lblAnswer.textColor = UIColor.white
                                        cell.bgView.backgroundColor = UIColor().themeDarkPinkColor
                                    }
                                    else if answerList[tmpIndexPath.row]["is_user_answer"] as? String == kYes {
                                        cell.imgView.isHidden = false
                                        cell.imgView.image = #imageLiteral(resourceName: "wrong_ic")
                                        cell.lblAnswer.textColor = UIColor.black
                                        cell.bgView.backgroundColor = UIColor.white
                                    }
                                }
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
                                if self.questionList.count > 0 && self.selectedQuestionIndex != -1 {
                                    if self.selectedQuestionIndex < self.questionList.count - 1 {
                                        self.selectedQuestionIndex = self.selectedQuestionIndex + 1
                                        self.tblQuestion.reloadData()
                                        
                                        self.countdownTimerView.start(beginingValue: 15, interval: 1)
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension QuizQuestionsViewController: SRCountdownTimerDelegate {
    func timerDidEnd(sender: SRCountdownTimer, elapsedTime: TimeInterval) {
        let isLastQuestion = (self.questionList.count - 1) == self.selectedQuestionIndex ? true : false
        if !isLastQuestion {
            if self.questionList.count > 0 && self.selectedQuestionIndex != -1 {
                if self.selectedQuestionIndex < self.questionList.count - 1 {
                    self.selectedQuestionIndex = self.selectedQuestionIndex + 1
                    self.tblQuestion.reloadData()
                    
                    self.countdownTimerView.start(beginingValue: 15, interval: 1)
                }
            }
        }
        else {
            self.startEndQuizTimer(isStartQuiz: false, isShowHud: true)
        }
    }
}
