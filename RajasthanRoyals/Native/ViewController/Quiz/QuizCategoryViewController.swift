//
//  QuizCategoryViewController.swift
//  RR_New
//
//  Created by Apple on 12/06/21.
//

import UIKit

class QuizCategoryViewController: BaseViewController {
  
    @IBOutlet weak var imgQuiz: UIImageView!
    @IBOutlet weak var lblSelectCategory: UILabel!
    @IBOutlet weak var CVCategory: UICollectionView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomView1: UIView!
    @IBOutlet weak var imgSponsor: UIImageView!
    
    @IBOutlet weak var bottomViewBottomConstraint: NSLayoutConstraint!

    var sponsorImage = ""
    var quizLogo = ""
    var currentPage = 1
    var nextPage = kN
    var categoryList = [[String: AnyObject]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblSelectCategory.text = self.getLocalizeTextForKey(keyName: "Quiz_lbl_SelectCategory")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.bottomView.roundCorners([.topLeft, .topRight], radius: 20)
        }
        
        if let url = URL(string: self.quizLogo) {
            self.imgQuiz.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
        }
        
        if !self.sponsorImage.isEmpty {
            if let url = URL(string: self.sponsorImage) {
                self.imgSponsor.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        else {
            self.bottomViewBottomConstraint.constant = -self.bottomView.frame.height
            self.bottomView.isHidden = true
            self.bottomView1.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getQuizCategoryList(isShowHud: true)
    }
    
    @IBAction func btnLeaderboard_Clicked(_ sender: Any) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "QuizLeaderboardViewController") as! QuizLeaderboardViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension QuizCategoryViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == categoryList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getQuizCategoryList(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizCategoryCVCell", for: indexPath) as! QuizCategoryCVCell
        
        cell.btnPlay.setTitle(self.getLocalizeTextForKey(keyName: "Quiz_Btn_Play"), for: .normal)
        
        if let imageUrl = categoryList[indexPath.item][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let title = categoryList[indexPath.item][kTitle] as? String {
            cell.lblCategoryTitle.text = title
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let quizAppTime = self.categoryList[indexPath.row]["quiz_app_time"] as? [String: AnyObject] {
            if let startTimeStr = quizAppTime["end_time"] as? String, !startTimeStr.isEmpty {
                UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_AlreadyPlayed"))
                return
            }
        }
        
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "QuizQuestionsViewController") as! QuizQuestionsViewController
        viewController.sponsorImage = self.sponsorImage
        viewController.quizLogo = self.quizLogo
        viewController.quizCategoryId = "\(self.categoryList[indexPath.row]["quiz_app_category_id"] as? Int ?? 0)"
        viewController.selCategoryTitle = self.categoryList[indexPath.row]["title"] as? String ?? ""
        viewController.isLock = self.categoryList[indexPath.row]["is_lock"] as? String ?? ""
        viewController.userPlayedQuestion = self.categoryList[indexPath.row]["user_play_question"] as? Int ?? 0
        
        if let quizAppTime = self.categoryList[indexPath.row]["quiz_app_time"] as? [String: AnyObject] {
            if quizAppTime.isEmpty {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else {
                if let startTimeStr = quizAppTime["start_time"] as? String, !startTimeStr.isEmpty {
                    viewController.startDate = self.convertStringToDate(format: "yyyy-MM-dd HH:mm:ss", strDate: startTimeStr).convertToSpacificTimeZone(timeZone: "Asia/Kolkata")
                    viewController.quizAppTime = quizAppTime["quiz_app_time"] as? Int ?? 0
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                else {
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
        else {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

class QuizCategoryCVCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var btnPlay: CustomButton!
}

extension QuizCategoryViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 2
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}

extension QuizCategoryViewController {
    func getQuizCategoryList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kPage: self.currentPage, kPageSize: PAGESIZE]
                as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.GETQUIZAPPCATEGORY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                  
                    if self.currentPage == 1 {
                        self.categoryList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let dataList = responseData[kData] as? [[String : AnyObject]] {
                        for data in dataList {
                            self.categoryList.append(data)
                        }
                    }
                    
                    self.CVCategory.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
