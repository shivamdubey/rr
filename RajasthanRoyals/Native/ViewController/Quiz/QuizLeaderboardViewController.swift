//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class QuizLeaderboardViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblLeaderboard: UITableView!
    @IBOutlet weak var lblEmptyMessage: UILabel!
    
    var userList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "QuizLeaderboardViewController")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Leaderboard_lbl_Title").uppercased()
        
        self.getLeaderboardList()
    }
    
    @IBAction func btnBack_Clicked(_ button: Any) {
        for viewController in self.navigationController!.viewControllers {
            if viewController.isKind(of: QuizCategoryViewController.self) {
                self.navigationController?.popToViewController(viewController, animated: true)
                break
            }
        }
    }
    
    @IBAction func btnShare_Clicked(_ button: Any) {
        let activityVC = UIActivityViewController(activityItems: [self.takeScreenshot(theView: self.view)], applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
        
        let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "eventType": POINTSEVENTTYPE.QUIZ_SHARE.rawValue]
        self.callEarnPointsAPI(param: param)
        
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Quiz: Leaderboard Share on Social media")
    }
    
    func takeScreenshot(theView: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(theView.frame.size, true, 0.0)
        theView.drawHierarchy(in: theView.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage()
    }
}

extension QuizLeaderboardViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderboardHeaderCell") as! LeaderboardHeaderCell
        
        cell.lblName.text = self.getLocalizeTextForKey(keyName: "Quiz_lbl_Name")
        cell.lblPoints.text = self.getLocalizeTextForKey(keyName: "Quiz_lbl_Points")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderboardCell") as! LeaderboardCell
        
        let userDetails = userList[indexPath.row]
        
        cell.lblRank.text = "\(userDetails[kRank] as? Int ?? 0)"
        
        if let imageUrl = userDetails[kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        cell.lblName.text = userDetails[kFullName] as? String ?? ""
        
        if let totalPoints = userDetails[kTotalPoints] as? Int {
            cell.lblPointsValue.text = "\(totalPoints)"
        }
        
        if userDetails[kAppUserId] as? Int == BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int {
            cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
        }
        else {
            cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        }
        
        return cell
    }
}

extension QuizLeaderboardViewController {
    func getLeaderboardList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "is_new": kYes] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETQUIZAPPUSERANSWERLEADERBOARD.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    self.lblEmptyMessage.text = responseData[kMessage] as? String ?? ""
                    
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.userList = data
                    }
                    
                    self.tblLeaderboard.reloadData()
                    
                    if self.userList.count > 0 {
                        self.lblEmptyMessage.isHidden = true
                    }
                    else {
                        self.lblEmptyMessage.isHidden = false
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
