//
//  HousieFAQViewController.swift
//  Unity-iPhone
//
//  Created by Apple on 11/03/21.
//

import UIKit

class PredictionFAQViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnRules: CustomButton!
    @IBOutlet weak var btnFaqs: CustomButton!
    @IBOutlet weak var btnTC: CustomButton!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var tblFAQs: UITableView!
    
    var descriptionText = ""
    var termsConditions = ""
    var faqList = [[String: AnyObject]]()
    var selectedFAQIndex = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        self.btnRules_Clicked(sender: self.btnRules)
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Settings_lbl_FAQs")
        self.btnRules.setTitle(self.getLocalizeTextForKey(keyName: "Settings_lbl_Rules"), for: .normal)
        self.btnFaqs.setTitle(self.getLocalizeTextForKey(keyName: "Settings_lbl_FAQs"), for: .normal)
        self.btnTC.setTitle(self.getLocalizeTextForKey(keyName: "Settings_lbl_TCs"), for: .normal)
        
        self.txtViewDescription.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        self.getFAQList()
    }
    
    @IBAction func btnRules_Clicked(sender: UIButton) {
        self.btnFaqs.backgroundColor = DeSelectedButtonBGColor
        self.btnRules.backgroundColor = SelectedButtonBGColor
        self.btnTC.backgroundColor = DeSelectedButtonBGColor
        
        self.btnFaqs.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnRules.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnTC.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.tblFAQs.isHidden = true
        self.txtViewDescription.isHidden = false
        
        self.txtViewDescription.attributedText = self.descriptionText.html2Attributed
    }
    
    @IBAction func btnFaqs_Clicked(sender: UIButton) {
        self.btnFaqs.backgroundColor = SelectedButtonBGColor
        self.btnRules.backgroundColor = DeSelectedButtonBGColor
        self.btnTC.backgroundColor = DeSelectedButtonBGColor
        
        self.btnFaqs.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnRules.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnTC.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.tblFAQs.isHidden = false
        self.txtViewDescription.isHidden = true
    }
    
    @IBAction func btnTC_Clicked(sender: UIButton) {
        self.btnFaqs.backgroundColor = DeSelectedButtonBGColor
        self.btnRules.backgroundColor = DeSelectedButtonBGColor
        self.btnTC.backgroundColor = SelectedButtonBGColor
        
        self.btnFaqs.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnRules.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnTC.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.tblFAQs.isHidden = true
        self.txtViewDescription.isHidden = false
        
        self.txtViewDescription.attributedText = self.termsConditions.html2Attributed
    }
}

extension PredictionFAQViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell") as! FAQCell
         
        if let question = faqList[indexPath.row][kQuestion] as? String {
            cell.lblQuestion.text = question
        }
        
        cell.lblAnswer.text = ""
        cell.imgArrow.image = #imageLiteral(resourceName: "arrow_left_ic").imageWithColor(color: .white)
        cell.lblAnswerTopConstraint.constant = -20
        
        if selectedFAQIndex.contains(indexPath.row) {
            cell.lblAnswerTopConstraint.constant = 5
            cell.imgArrow.image = #imageLiteral(resourceName: "arrow_down_ic").imageWithColor(color: .white)
            
            if let answer = faqList[indexPath.row][kAnswer] as? String {
                cell.lblAnswer.attributedText = answer.html2Attributed
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedFAQIndex.contains(indexPath.row) {
            selectedFAQIndex = selectedFAQIndex.filter({ $0 != indexPath.row })
        }
        else {
            selectedFAQIndex.append(indexPath.row)
        }
        self.tblFAQs.reloadRows(at: [indexPath], with: .none)
    }
}

extension PredictionFAQViewController {
    func getFAQList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: true, URL: baseURLWithoutAuth, apiName: APINAME.GETPREDICTIONFAQ.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let faqs = responseData[kData] as? [[String : AnyObject]] {
                        self.faqList = faqs
                    }

                    if let description = responseData[kDescription] as? String {
                        self.descriptionText = description
                        self.txtViewDescription.attributedText = description.html2Attributed
                    }
                    
                    if let termsConditions = responseData["terms_conditions"] as? String {
                        self.termsConditions = termsConditions
                    }
                    
                    self.tblFAQs.reloadData()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
