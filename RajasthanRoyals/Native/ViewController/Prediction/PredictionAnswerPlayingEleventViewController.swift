//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class PredictionAnswerPlayingEleventViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblQuestionCount: UILabel!
    @IBOutlet weak var imgFirstTeam: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgSecondTeam: UIImageView!
    
    @IBOutlet weak var lblQuestion: UILabel!
    
    @IBOutlet weak var btnPlayer: UIButton!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblRuns: UILabel!
    @IBOutlet weak var txtRuns: CustomTextField!
    @IBOutlet weak var lblFours: UILabel!
    @IBOutlet weak var txtFours: CustomTextField!
    @IBOutlet weak var lblSixs: UILabel!
    @IBOutlet weak var txtSixs: CustomTextField!
    
    @IBOutlet weak var btnSubmit: CustomButton!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var imgSponsor: UIImageView!
    
    var categoryName = ""
    var predictionQuestionId = 0
    var matchDetails = [String: AnyObject]()
    var questionDetails = [String: AnyObject]()
    var answerList = [[String: AnyObject]]()
    var selectedQuestionAnswerId = 0
    var matchIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addFirebaseCustomeEvent(eventName: "PredictorAnswerScreen")
        
        self.lblTitle.text = self.categoryName
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "btn_Submit"), for: .normal)
        
        self.setMatchDetails()
        
        self.getAnswerList()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.bottomView.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    //Set Match Details
    func setMatchDetails() {
        if let imageUrl = BaseViewController.sharedInstance.appDelegate.generalSettings["prediction_sponsor_image"] as? String {
            if let url = URL(string: imageUrl) {
                self.imgSponsor.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if let imageUrl = self.matchDetails[kFirstImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgFirstTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let imageUrl = self.matchDetails[kSecondImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgSecondTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        self.lblLocation.text = self.matchDetails[kLocation] as? String ?? ""
    }
    
    func setAnswerTitle() {
        self.lblQuestion.text = self.questionDetails[kQuestion] as? String ?? ""
        self.lblRuns.text = self.questionDetails["text_box_title_1"] as? String ?? ""
        self.lblFours.text = self.questionDetails["text_box_title_2"] as? String ?? ""
        self.lblSixs.text = self.questionDetails["text_box_title_3"] as? String ?? ""
    }
    
    func setAnswer() {
        if self.answerList.count > 0 {
            let array = self.answerList as Array
            let matchIndex = array.index {
                if $0["prediction_question_answer_id"] as? Int == selectedQuestionAnswerId {
                    return true
                }
                return false
            }
            
            if matchIndex != nil {
                self.matchIndex = matchIndex!
                if let imageUrl = self.answerList[matchIndex!][kImage] as? String {
                    if let url = URL(string: imageUrl) {
                        self.imgPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                
                self.lblPlayerName.text = self.answerList[matchIndex!][kAnswer] as? String ?? ""
                self.txtRuns.text = self.answerList[matchIndex!]["answer1"] as? String ?? ""
                self.txtFours.text = self.answerList[matchIndex!]["answer2"] as? String ?? ""
                self.txtSixs.text = self.answerList[matchIndex!]["answer3"] as? String ?? ""
                
                if self.txtRuns.text?.trim().count == 0 || self.txtFours.text?.trim().count == 0 || self.txtSixs.text?.trim().count == 0 {
                    self.btnSubmit.isHidden = false
                    self.txtRuns.isEnabled = true
                    self.txtFours.isEnabled = true
                    self.txtSixs.isEnabled = true
                    self.btnPlayer.isEnabled = true
                }
                else {
                    self.btnSubmit.isHidden = true
                    self.txtRuns.isEnabled = false
                    self.txtFours.isEnabled = false
                    self.txtSixs.isEnabled = false
                    self.btnPlayer.isEnabled = false
                }
            }
        }
    }
    
    @IBAction func btnPlayer_Clicked(_ sender: Any) {
        let playerName = self.answerList.map({$0[kAnswer] as? String ?? ""})
        RPicker.pickOption(title: self.getLocalizeTextForKey(keyName: "ValidationMessage_SelectPlayer"), cancelText: self.getLocalizeTextForKey(keyName: "btn_Cancel"), doneText: self.getLocalizeTextForKey(keyName: "btn_Done"), dataArray: playerName, selectedIndex: 0) { value, index in
            if self.answerList.count > index {
                self.selectedQuestionAnswerId = self.answerList[index]["prediction_question_answer_id"] as? Int ?? 0
                self.setAnswer()
            }
        }
    }
    
    @IBAction func btnSubmit_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        if self.txtRuns.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please enter \(self.lblRuns.text ?? "")")
        }
        else if self.txtFours.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please enter \(self.lblFours.text ?? "")")
        }
        else if self.txtSixs.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please enter \(self.lblSixs.text ?? "")")
        }
        else {
            if let answer = self.answerList[self.matchIndex][kAnswer] as? String {
                self.submitAnswer(answer: answer)
            }
        }
    }
}

extension PredictionAnswerPlayingEleventViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtRuns.isFirstResponder {
            txtFours.becomeFirstResponder()
        }
        else if txtFours.isFirstResponder {
            txtSixs.becomeFirstResponder()
        }
        else if txtSixs.isFirstResponder {
            txtSixs.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet(charactersIn:"0123456789")
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}

extension PredictionAnswerPlayingEleventViewController {
    func getAnswerList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kPredictionQuestionId: self.predictionQuestionId] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETPREDICTIONQUESTIONANSWERDETAILS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.questionDetails = data
                        
                        if let answerOption = self.questionDetails[kAnswerOption]  as? [[String : AnyObject]] {
                            self.answerList = answerOption
                            
                            if self.answerList.count > 0 {
                                let array = self.answerList as Array
                                let matchIndex = array.index {
                                    if $0[kUserAnswer] as? String == kYes {
                                        return true
                                    }
                                    return false
                                }
                                
                                if matchIndex != nil {
                                    if self.answerList.count > matchIndex! {
                                        self.selectedQuestionAnswerId = self.answerList[matchIndex!]["prediction_question_answer_id"] as? Int ?? 0
                                        
                                        self.setAnswerTitle()
                                        self.setAnswer()
                                    }
                                }
                                else {
                                    self.selectedQuestionAnswerId = self.answerList[0]["prediction_question_answer_id"] as? Int ?? 0
                                    
                                    self.setAnswerTitle()
                                    self.setAnswer()
                                }
                            }
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func submitAnswer(answer: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kPredictionQuestionId: self.predictionQuestionId, "answer": answer, "answer1": self.txtRuns.text!, "answer2": self.txtFours.text!, "answer3": self.txtSixs.text!, kIsChoice: "multiple"] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.SETPREDICTIONANSWER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String: AnyObject]
                    if responseData[kStatus] as? Int == kIsSuccess {
                        if let message = responseData[kMessage] as? String, !message.isEmpty {
                            Toast.show(message: message, controller: self)
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                            self.navigationController?.popViewController(animated: true)
                        }
                        
//                        if self.answerList.count > self.matchIndex {
//                            self.answerList[self.matchIndex]["answer1"] = self.txtRuns.text! as AnyObject
//                            self.answerList[self.matchIndex]["answer2"] = self.txtFours.text! as AnyObject
//                            self.answerList[self.matchIndex]["answer3"] = self.txtSixs.text! as AnyObject
                            
//                            self.txtRuns.text = ""
//                            self.txtFours.text = ""
//                            self.txtSixs.text = ""
//
//                            self.btnSubmit.isHidden = true
//                        }
                    }
                    else {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: responseData[kMessage] as? String ?? "")
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
