//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class PredictionCategoryViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgFirstTeam: UIImageView!
    @IBOutlet weak var imgSecondTeam: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var tblCategory: UITableView!

    var matchDetails = [String: AnyObject]()
    var matchId = 0
    var categoryList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addFirebaseCustomeEvent(eventName: "PredictorCategoryScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Predictor_lbl_MakYourPredictions")
        
        self.matchId = self.matchDetails[kMatch_Id] as? Int ?? 0
        
        self.setMatchDetails()
        
        self.getCategoryList()
    }
    
    //Set Match Details
    func setMatchDetails() {
        if let imageUrl = self.matchDetails[kFirstImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgFirstTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let imageUrl = self.matchDetails[kSecondImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgSecondTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        self.lblLocation.text = self.matchDetails[kLocation] as? String ?? ""
    }
    
    @IBAction func btnInfo_Clicked(sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PredictionFAQViewController") as! PredictionFAQViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension PredictionCategoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PredictionQuestionCell") as! PredictionQuestionCell
         
        cell.lblQuestion.text = categoryList[indexPath.row][kTitle] as? String ?? ""
        
        if categoryList[indexPath.row][kIsLock] as? String == kYes {
            cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            cell.imgLock.isHidden = false
            cell.imgLockTrailingConstraint.constant = 15
        }
        else {
            cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            cell.imgLock.isHidden = true
            cell.imgLockTrailingConstraint.constant = -15
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if categoryList[indexPath.row][kIsLock] as? String == kNo {
            if let questionList = categoryList[indexPath.row][kPredictionQuestion] as? [[String: AnyObject]], questionList.count > 0 {
                if questionList[0]["question_type"] as? String == "predict_stats" {
                    let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PredictionAnswerPlayingEleventViewController") as! PredictionAnswerPlayingEleventViewController
                    viewController.matchDetails = self.matchDetails
                    viewController.categoryName = self.categoryList[indexPath.row][kTitle] as? String ?? ""
                    viewController.predictionQuestionId = questionList[0][kPredictionQuestionId] as? Int ?? 0
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                else {
                    if questionList.count == 1 {
                        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PredictionAnswerViewController") as! PredictionAnswerViewController
                        viewController.matchDetails = self.matchDetails
                        viewController.categoryName = self.categoryList[indexPath.row][kTitle] as? String ?? ""
                        viewController.questionList = questionList
                        viewController.currentQuestionIndex = 0
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                    else {
                        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PredictionQuestionsViewController") as! PredictionQuestionsViewController
                        viewController.matchDetails = self.matchDetails
                        viewController.categoryId = self.categoryList[indexPath.row][kPredictionCategoryId] as? Int ?? 0
                        viewController.categoryName = self.categoryList[indexPath.row][kTitle] as? String ?? ""
                        viewController.questionList = questionList
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
            }
        }
    }
}

extension PredictionCategoryViewController {
    func getCategoryList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kMatch_Id: self.matchId] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETPREDICTIONQUESTIONCATEGORY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.categoryList = data
                    }
                    
                    self.tblCategory.reloadData()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
