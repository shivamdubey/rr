//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class LeaderboardViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblLeaderboard: UITableView!
    @IBOutlet weak var lblEmptyMessage: UILabel!
    
    var userDetails = [String: AnyObject]()
    var userList = [[String: AnyObject]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "PredictorLeaderboardScreen")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Leaderboard_lbl_Title")
        
        self.getLeaderboardList()
    }
}

extension LeaderboardViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderboardHeaderCell") as! LeaderboardHeaderCell
        
        cell.lblName.text = self.getLocalizeTextForKey(keyName: "Quiz_lbl_Name")
        cell.lblPoints.text = self.getLocalizeTextForKey(keyName: "Quiz_lbl_Points")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderboardCell") as! LeaderboardCell
        
        let userDetails = userList[indexPath.row]
        
        cell.lblRank.text = "\(userDetails[kRank] as? Int ?? 0)"
        
        if let imageUrl = userDetails[kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        cell.lblName.text = userDetails[kFullName] as? String ?? ""
        
        if let totalPoints = userDetails[kTotalPoints] as? Int {
            cell.lblPointsValue.text = "\(totalPoints)"
        }
        
        if userDetails[kAppUserId] as? Int == BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int {
            cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
        }
        else {
            cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        }
        
        return cell
    }
}

extension LeaderboardViewController {
    func getLeaderboardList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "is_new": kYes] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETPREDICTIONQUESTIONLEADERBOARD.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    self.lblEmptyMessage.text = responseData[kMessage] as? String ?? ""
                    
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.userList = data
                    }
                    
                    self.tblLeaderboard.reloadData()
                    
                    if self.userList.count > 0 {
                        self.lblEmptyMessage.isHidden = true
                    }
                    else {
                        self.lblEmptyMessage.isHidden = false
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

class LeaderboardHeaderCell: UITableViewCell {
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
}

class LeaderboardCell: UITableViewCell {
    @IBOutlet weak var bgView: CustomView!
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var imgUser: CustomImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblPointsValue: UILabel!
    @IBOutlet weak var imgPrice: CustomImageView!
}
