//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class PredictionQuestionsViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgFirstTeam: UIImageView!
    @IBOutlet weak var imgSecondTeam: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var tblQuestion: UITableView!
    
    var categoryName = ""
    var categoryId = 0
    var matchDetails = [String: AnyObject]()
    var matchId = 0
    var questionList = [[String: AnyObject]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addFirebaseCustomeEvent(eventName: "PredictorQuestionScreen")
        
        self.lblTitle.text = self.categoryName
        
        self.matchId = self.matchDetails[kMatch_Id] as? Int ?? 0
        
        self.setMatchDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getQuestionList()
    }
    
    //Set Match Details
    func setMatchDetails() {
        if let imageUrl = self.matchDetails[kFirstImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgFirstTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let imageUrl = self.matchDetails[kSecondImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgSecondTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        self.lblLocation.text = self.matchDetails[kLocation] as? String ?? ""
    }
}

extension PredictionQuestionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PredictionQuestionCell") as! PredictionQuestionCell
         
        cell.lblQuestion.text = questionList[indexPath.row][kQuestion] as? String ?? ""
        
        if questionList[indexPath.row][kIsLock] as? String == kYes {
            cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            cell.imgLock.isHidden = false
            cell.imgLockTrailingConstraint.constant = 15
        }
        else {
            cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            cell.imgLock.isHidden = true
            cell.imgLockTrailingConstraint.constant = -15
        }
        
        if let userAnswer = questionList[indexPath.row][kUserAnswer] as? [String: AnyObject], !userAnswer.isEmpty {
            cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
            cell.imgLock.isHidden = true
            cell.imgLockTrailingConstraint.constant = -15
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PredictionAnswerViewController") as! PredictionAnswerViewController
        viewController.matchDetails = self.matchDetails
        viewController.categoryName = self.categoryName
        viewController.questionList = self.questionList
        viewController.currentQuestionIndex = indexPath.row
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension PredictionQuestionsViewController {
    func getQuestionList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kPredictionCategoryId: self.categoryId, kMatch_Id: self.matchId] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETPREDICTIONQUESTIONANSWER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [[String : AnyObject]] {
                        self.questionList = data
                    }
                    
                    self.tblQuestion.reloadData()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

class PredictionQuestionCell: UITableViewCell {
    @IBOutlet weak var bgView: CustomView!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
    
    @IBOutlet weak var imgLockTrailingConstraint: NSLayoutConstraint!
}
