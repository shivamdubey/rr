//
//  ViewController.swift
//  Utalan
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class PredictionAnswerViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblQuestionCount: UILabel!
    @IBOutlet weak var imgFirstTeam: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgSecondTeam: UIImageView!
    
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var tblAnswers: UITableView!
    @IBOutlet weak var answersCV: UICollectionView!
    
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnSubmit: CustomButton!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var imgSponsor: UIImageView!
    
    var categoryName = ""
    var matchDetails = [String: AnyObject]()
    var questionList = [[String: AnyObject]]()
    var questionDetails = [String: AnyObject]()
    var currentQuestionIndex = 0
    var predictionQuestionId = 0
    
    var answerList = [[String: AnyObject]]()
    var type = ""
    var selectedAnswers = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addFirebaseCustomeEvent(eventName: "PredictorAnswerScreen")
        
        self.lblTitle.text = self.categoryName
        self.btnPrevious.setTitle(self.getLocalizeTextForKey(keyName: "btn_Previous"), for: .normal)
        self.btnSubmit.setTitle(self.getLocalizeTextForKey(keyName: "btn_Submit"), for: .normal)
        self.btnNext.setTitle(self.getLocalizeTextForKey(keyName: "btn_Next"), for: .normal)
        
        self.setMatchDetails()
        
        self.predictionQuestionId = self.questionList[self.currentQuestionIndex][kPredictionQuestionId] as? Int ?? 0
        
        self.getAnswerList()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.bottomView.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    //Set Match Details
    func setMatchDetails() {
        if let imageUrl = BaseViewController.sharedInstance.appDelegate.generalSettings["prediction_sponsor_image"] as? String {
            if let url = URL(string: imageUrl) {
                self.imgSponsor.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if let imageUrl = self.matchDetails[kFirstImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgFirstTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        if let imageUrl = self.matchDetails[kSecondImage] as? String {
            if let url = URL(string: imageUrl) {
                self.imgSecondTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        self.lblLocation.text = self.matchDetails[kLocation] as? String ?? ""
    }
    
    //Set Answer
    func setAnswer() {
        self.selectedAnswers.removeAll()
        if self.questionList.count == 1 {
            self.btnNext.isHidden = true
            self.btnPrevious.isHidden = true
        }
        else if self.currentQuestionIndex == 0 {
            self.btnNext.isHidden = false
            self.btnPrevious.isHidden = true
        }
        else if self.currentQuestionIndex == self.questionList.count - 1 {
            self.btnNext.isHidden = true
            self.btnPrevious.isHidden = false
        }
        else {
            self.btnNext.isHidden = false
            self.btnPrevious.isHidden = false
        }
        
        if self.questionList.count == 1 {
            self.lblQuestionCount.isHidden = true
            self.lblQuestionCount.text = ""
        }
        else {
            self.lblQuestionCount.isHidden = false
            self.lblQuestionCount.text = "\(self.currentQuestionIndex + 1) / \(self.questionList.count)"
        }
        
        self.lblQuestion.text = self.questionDetails[kQuestion] as? String ?? ""
        
        self.type = self.questionDetails[kType] as? String ?? ""
        
        if self.type == "1" || self.type == "2" {
            self.tblAnswers.isHidden = false
            self.answersCV.isHidden = true
        }
        else {
            self.tblAnswers.isHidden = true
            self.answersCV.isHidden = false
        }
        
        self.tblAnswers.reloadData()
        self.answersCV.reloadData()
    }
    
    @IBAction func btnPrevious_Clicked(_ sender: Any) {
        if self.currentQuestionIndex != 0 {
            self.currentQuestionIndex = self.currentQuestionIndex - 1
            self.predictionQuestionId = self.questionList[self.currentQuestionIndex][kPredictionQuestionId] as? Int ?? 0
            self.getAnswerList()
        }
    }
    
    @IBAction func btnNext_Clicked(_ sender: Any) {
        if self.currentQuestionIndex != self.questionList.count {
            self.currentQuestionIndex = self.currentQuestionIndex + 1
            self.predictionQuestionId = self.questionList[self.currentQuestionIndex][kPredictionQuestionId] as? Int ?? 0
            self.getAnswerList()
        }
    }
    
    @IBAction func btnSubmit_Clicked(_ sender: Any) {
        if self.selectedAnswers.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_SelectYourAnswer"))
        }
        else {
            if let numberOfChoice = questionDetails[kNumberOfChoice] as? Int {
                if numberOfChoice > self.selectedAnswers.count {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(self.getLocalizeTextForKey(keyName: "ValidationMessage_Select")) \(numberOfChoice) \(self.getLocalizeTextForKey(keyName: "ValidationMessage_AnswerForThisQuestion"))")
                }
                else {
                    let paramName = self.selectedAnswers.count == 1 ? "answer" : "multipleanswer"
                    let paramValue = self.selectedAnswers.joined(separator: ",")
                    let isChoice = numberOfChoice == 1 ? "single" : "multiple"
                    
                    self.submitAnswer(answerParam: paramName, answerValue: paramValue, isChoice: isChoice)
                }
            }
        }
    }
}

extension PredictionAnswerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answerList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if type == "1" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell") as! AnswerCell
            
            let answerDetails = answerList[indexPath.row]
            
            cell.lblAnswerNo.text = "\(indexPath.row + 1)"
            
            cell.lblAnswer.text = answerDetails[kAnswer] as? String ?? ""
            
            cell.lblTotalVotes.text = "\(answerDetails[kCount] as? String ?? "")\n\(self.getLocalizeTextForKey(keyName: "Predictor_lbl_Votes"))"
            
            cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            if self.selectedAnswers.contains(cell.lblAnswer.text!) {
                cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
            }
            
            if answerDetails[kUserAnswer] as? String == kYes {
                cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
            }
            else {
                if questionDetails[kIsLock] as? String == kYes {
                    cell.lblTotalVotes.isHidden = true
                    cell.imgLock.isHidden = false
                    self.btnSubmit.isEnabled = false
                }
                else {
                    cell.lblTotalVotes.isHidden = false
                    cell.imgLock.isHidden = true
                    self.btnSubmit.isEnabled = true
                }
            }
            
            let array = answerList as Array
            let matchIndex = array.index {
                if $0[kUserAnswer] as? String == kYes {
                    return true
                }
                return false
            }
            
            if matchIndex != nil {
                self.btnSubmit.isHidden = true
            }
            else {
                self.btnSubmit.isHidden = false
            }
            
            if questionDetails[kIsTimeout] as? String == kYes {
                self.btnSubmit.isEnabled = false
            }
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerTeamCell") as! AnswerTeamCell
            
            let answerDetails = answerList[indexPath.row]
            
            if let imageUrl = answerDetails[kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
            
            cell.lblTotalVotes.text = "\(answerDetails[kCount] as? String ?? "") \(self.getLocalizeTextForKey(keyName: "Predictor_lbl_Votes"))"
            
            cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            
            if let answer = answerDetails[kAnswer] as? String {
                if self.selectedAnswers.contains(answer) {
                    cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
                }
            }
            
            if answerDetails[kUserAnswer] as? String == kYes {
                cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
            }
            else {
                if questionDetails[kIsLock] as? String == kYes {
                    cell.lblTotalVotes.isHidden = true
                    cell.imgLock.isHidden = false
                    self.btnSubmit.isEnabled = false
                }
                else {
                    cell.lblTotalVotes.isHidden = false
                    cell.imgLock.isHidden = true
                    self.btnSubmit.isEnabled = true
                }
            }
            
            let array = answerList as Array
            let matchIndex = array.index {
                if $0[kUserAnswer] as? String == kYes {
                    return true
                }
                return false
            }
            
            if matchIndex != nil {
                self.btnSubmit.isHidden = true
            }
            else {
                self.btnSubmit.isHidden = false
            }
            
            if questionDetails[kIsTimeout] as? String == kYes {
                self.btnSubmit.isEnabled = false
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let category = questionDetails[kPredictionCategory] as? [String: AnyObject] {
            if category[kIsTimeout] as? String == kYes {
                UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "Predictor_lbl_TimeoutText"))
                return
            }
        }
        
        if questionDetails[kIsTimeout] as? String == kYes {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "Predictor_lbl_TimeoutText"))
            return
        }
        
        if questionDetails[kIsLock] as? String == kYes {
            return
        }
        
        let array = answerList as Array
        let matchIndex = array.index {
            if $0[kUserAnswer] as? String == kYes {
                return true
            }
            return false
        }
        
        if matchIndex == nil {
            if let answer = answerList[indexPath.row][kAnswer] as? String {
                if self.selectedAnswers.contains(answer) {
                    self.selectedAnswers = self.selectedAnswers.filter({ $0 != answer })
                }
                else {
                    if let numberOfChoice = questionDetails[kNumberOfChoice] as? Int {
                        if numberOfChoice == 1 {
                            self.selectedAnswers.removeAll()
                            self.selectedAnswers.append(answer)
                        }
                        else if numberOfChoice > self.selectedAnswers.count {
                            self.selectedAnswers.append(answer)
                        }
                        else {
                            UIAlertController().alertViewWithTitleAndMessage(self, message: "\(self.getLocalizeTextForKey(keyName: "ValidationMessage_YouCanSelectMaximum")) \(numberOfChoice) \(self.getLocalizeTextForKey(keyName: "ValidationMessage_AnswerForThisQuestionForMiximum"))")
                        }
                    }
                }
            }
            self.tblAnswers.reloadData()
        }
    }
}

extension PredictionAnswerViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.type == "3" || self.type == "4" {
            return answerList.count
        }
        else {
            if answerList.count > 0 {
                return answerList.count + 2
            }
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.type == "3" || self.type == "4" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnswerPlayerCell", for: indexPath) as! AnswerPlayerCell
            
            if self.type == "4" {
                cell.bgViewLeadingConstraint.constant = 20
                cell.bgViewTrailingConstraint.constant = 20
            }
            else {
                cell.bgViewLeadingConstraint.constant = 5
                cell.bgViewTrailingConstraint.constant = 5
            }
            
            let answerDetails = answerList[indexPath.item]
            
            if let imageUrl = answerDetails[kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgPlayer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "playerplaceholder"))
                }
            }
            
            cell.lblPlayerName.text = answerDetails[kAnswer] as? String ?? ""
            
            cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            if self.selectedAnswers.contains(cell.lblPlayerName.text!) {
                cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
            }
            
            if answerDetails[kUserAnswer] as? String == kYes {
                cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
            }
            
            let array = answerList as Array
            let matchIndex = array.index {
                if $0[kUserAnswer] as? String == kYes {
                    return true
                }
                return false
            }
            
            if matchIndex != nil {
                self.btnSubmit.isHidden = true
            }
            else {
                self.btnSubmit.isHidden = false
            }
            
            if questionDetails[kIsTimeout] as? String == kYes {
                self.btnSubmit.isEnabled = false
            }
            
            return cell
        }
        else {
            if indexPath.item < 2 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnswerTeamCVCell", for: indexPath) as! AnswerTeamCVCell
                
                let answerDetails = answerList[indexPath.item]
                
                if let imageUrl = answerDetails[kImage] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "playerplaceholder"))
                    }
                }
                
                return cell
            }
            else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnswerCVCell", for: indexPath) as! AnswerCVCell
                
                let answerDetails = answerList[indexPath.item - 2]
                
                let answer = answerDetails[kAnswer] as? String ?? ""
                let answerArray = answer.components(separatedBy: " ")
                if answerArray.count > 1 {
                    cell.lblAnswer.text = answerArray[1]
                }
                else {
                    cell.lblAnswer.text = answerArray[0]
                }
                
                cell.lblTotalVotes.text = "\(answerDetails[kCount] as? String ?? "")\n\(self.getLocalizeTextForKey(keyName: "Predictor_lbl_Votes"))"
                
                cell.bgView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
                if self.selectedAnswers.contains(answer) {
                    cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
                }
                
                if answerDetails[kUserAnswer] as? String == kYes {
                    cell.bgView.backgroundColor = UIColor.init(named: "Pink_Color")
                }
                
                let array = answerList as Array
                let matchIndex = array.index {
                    if $0[kUserAnswer] as? String == kYes {
                        return true
                    }
                    return false
                }
                
                if matchIndex != nil {
                    self.btnSubmit.isHidden = true
                }
                else {
                    self.btnSubmit.isHidden = false
                }
                
                if questionDetails[kIsTimeout] as? String == kYes {
                    self.btnSubmit.isEnabled = false
                }
                
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let category = questionDetails[kPredictionCategory] as? [String: AnyObject] {
            if category[kIsTimeout] as? String == kYes {
                UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "Predictor_lbl_TimeoutText"))
                return
            }
        }
        
        if questionDetails[kIsTimeout] as? String == kYes {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "Predictor_lbl_TimeoutText"))
            return
        }
        
        if questionDetails[kIsLock] as? String == kYes {
            return
        }
        
        let array = answerList as Array
        let matchIndex = array.index {
            if $0[kUserAnswer] as? String == kYes {
                return true
            }
            return false
        }
        
        if matchIndex == nil {
            if self.type == "3" || self.type == "4" {
                if let answer = answerList[indexPath.row][kAnswer] as? String {
                    if self.selectedAnswers.contains(answer) {
                        self.selectedAnswers = self.selectedAnswers.filter({ $0 != answer })
                    }
                    else {
                        if let numberOfChoice = questionDetails[kNumberOfChoice] as? Int {
                            if numberOfChoice == 1 {
                                self.selectedAnswers.removeAll()
                                self.selectedAnswers.append(answer)
                            }
                            else if numberOfChoice > self.selectedAnswers.count {
                                if self.type == "3" && self.answerList[indexPath.row]["is_indian"] as? String == kNo {
                                    var maxCount = 0
                                    for selectedAnswer in self.selectedAnswers {
                                        //Find the index
                                        let array = self.answerList as Array
                                        let matchIndex = array.index {
                                            if $0[kAnswer] as? String == selectedAnswer {
                                                return true
                                            }
                                            return false
                                        }
                                        
                                        if matchIndex != nil {
                                            if self.answerList[matchIndex!]["is_indian"] as? String == kNo {
                                                maxCount = maxCount + 1
                                            }
                                        }
                                    }
                                    
                                    if maxCount > 3 {
                                        UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_MiximumOutsidePlayer"))
                                    }
                                    else {
                                        self.selectedAnswers.append(answer)
                                    }
                                }
                                else {
                                    self.selectedAnswers.append(answer)
                                }
                            }
                            else {
                                UIAlertController().alertViewWithTitleAndMessage(self, message: "\(self.getLocalizeTextForKey(keyName: "ValidationMessage_YouCanSelectMaximum")) \(numberOfChoice) \(self.getLocalizeTextForKey(keyName: "ValidationMessage_AnswerForThisQuestionForMiximum"))")
                            }
                        }
                    }
                }
            }
            else {
                if indexPath.row > 1 {
                    if let answer = answerList[indexPath.row - 2][kAnswer] as? String {
                        if self.selectedAnswers.contains(answer) {
                            self.selectedAnswers = self.selectedAnswers.filter({ $0 != answer })
                        }
                        else {
                            if let numberOfChoice = questionDetails[kNumberOfChoice] as? Int {
                                if numberOfChoice == 1 {
                                    self.selectedAnswers.removeAll()
                                    self.selectedAnswers.append(answer)
                                }
                                else if numberOfChoice > self.selectedAnswers.count {
                                    self.selectedAnswers.append(answer)
                                }
                                else {
                                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(self.getLocalizeTextForKey(keyName: "ValidationMessage_YouCanSelectMaximum")) \(numberOfChoice) \(self.getLocalizeTextForKey(keyName: "ValidationMessage_AnswerForThisQuestionForMiximum"))")
                                }
                            }
                        }
                    }
                }
            }
            self.answersCV.reloadData()
        }
    }
}

extension PredictionAnswerViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var itemsPerRow: CGFloat = 2
        if self.type == "3" {
            itemsPerRow = 3
        }
        else {
            itemsPerRow = 2
        }
        
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        
        if self.type == "3" {
            return CGSize(width: widthPerItem, height: widthPerItem * 1.25)
        }
        else {
            if self.type == "4" {
                return CGSize(width: widthPerItem, height: widthPerItem)
            }
            else {
                if indexPath.item < 2 {
                    return CGSize(width: widthPerItem, height: 125)
                }
                return CGSize(width: widthPerItem, height: 60)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}

class AnswerCell: UITableViewCell {
    @IBOutlet weak var bgView: CustomView!
    @IBOutlet weak var lblAnswerNo: UILabel!
    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var lblTotalVotes: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
}

class AnswerTeamCell: UITableViewCell {
    @IBOutlet weak var bgView: CustomView!
    @IBOutlet weak var imgTeam: CustomImageView!
    @IBOutlet weak var lblTotalVotes: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
}

class AnswerTeamCVCell: UICollectionViewCell {
    @IBOutlet weak var imgTeam: CustomImageView!
}

class AnswerCVCell: UICollectionViewCell {
    @IBOutlet weak var bgView: CustomView!
    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var lblTotalVotes: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
}

class AnswerPlayerCell: UICollectionViewCell {
    @IBOutlet weak var bgView: CustomView!
    @IBOutlet weak var imgPlayer: CustomImageView!
    @IBOutlet weak var lblPlayerName: UILabel!
    
    @IBOutlet weak var bgViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgViewTrailingConstraint: NSLayoutConstraint!
}

extension PredictionAnswerViewController {
    func getAnswerList() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kPredictionQuestionId: self.predictionQuestionId] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETPREDICTIONQUESTIONANSWERDETAILS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.questionDetails = data
                        
                        if let answerOption = self.questionDetails[kAnswerOption]  as? [[String : AnyObject]] {
                            self.answerList = answerOption
                        }
                    }
                    
                    self.setAnswer()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func submitAnswer(answerParam: String, answerValue: String, isChoice: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kPredictionQuestionId: self.predictionQuestionId, answerParam: answerValue, kIsChoice: isChoice] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.SETPREDICTIONANSWER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String: AnyObject]
                    if responseData[kStatus] as? Int == kIsSuccess {
                        if let message = responseData[kMessage] as? String, !message.isEmpty {
                            Toast.show(message: message, controller: self)
                        }
                        
                        if self.questionList.count > 1 {
                            if self.currentQuestionIndex < self.questionList.count - 1 {
                                self.currentQuestionIndex = self.currentQuestionIndex + 1
                                self.predictionQuestionId = self.questionList[self.currentQuestionIndex][kPredictionQuestionId] as? Int ?? 0
                                self.getAnswerList()
                            }
                            else {
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                        else {
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                    else if responseData[kStatus] as? Int == kNewUser {
                        if let message = responseData[kMessage] as? String {
                            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                            alert.view.tintColor = UIColor().alertButtonColor
                            
                            let hideAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                if self.questionList.count > 1 {
                                    if self.currentQuestionIndex < self.questionList.count - 1 {
                                        self.currentQuestionIndex = self.currentQuestionIndex + 1
                                        self.predictionQuestionId = self.questionList[self.currentQuestionIndex][kPredictionQuestionId] as? Int ?? 0
                                        self.getAnswerList()
                                    }
                                    else {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                                else {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            })
                            alert.addAction(hideAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
