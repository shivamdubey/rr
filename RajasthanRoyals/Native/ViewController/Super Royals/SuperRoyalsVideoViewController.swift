//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class SuperRoyalsVideoViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var videoCV: UICollectionView!
    
    var currentPage = 1
    var nextPage = kN
    var videoList = [[String: AnyObject]]()
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Home_lbl_Videos")
        
        refreshControl = UIRefreshControl.init()
        refreshControl.tintColor = .white
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshCollectionView(sender:)), for: .valueChanged)
        videoCV.refreshControl = refreshControl
        
        self.getSuperRoyalsVideos(isShowHud: true)
    }
    
    func setupLocalizationText() {
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Home_lbl_Videos")
    }
    
    //UIRefreshControl method
    @objc func refreshCollectionView(sender: AnyObject) {
        self.currentPage = 1
        self.nextPage = kN
        self.getSuperRoyalsVideos(isShowHud: false)
    }
}

extension SuperRoyalsVideoViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == videoList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getSuperRoyalsVideos(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVideoNewCell", for: indexPath) as! HomeVideoNewCell
        
        if let imageUrl = videoList[indexPath.row][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgVideoNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if let title = videoList[indexPath.row][kTitle] as? String {
            cell.lblTitle.text = title
        }
        
        cell.btnPlayVideo.isHidden = false
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let type = videoList[indexPath.row][kType] as? String {
            if type == VIDEOTYPE.LOCAL.rawValue {
                if let video = videoList[indexPath.row][kVideo] as? String {
                    if let videoURL = URL(string: video) {
                        let player = AVPlayer(url: videoURL)
                        let playerViewController = AVPlayerViewController()
                        playerViewController.player = player
                        self.present(playerViewController, animated: true) {
                            playerViewController.player!.play()
                        }
                    }
                }
            }
            else {
                if let videoUrl = videoList[indexPath.row][kVideo] as? String {
                    let array = videoUrl.components(separatedBy: "=")
                    if array.count > 1 {
                        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "YouTubePlayerViewController") as! YouTubePlayerViewController
                        viewController.videoId = array[1]
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
            }
        }
    }
}

extension SuperRoyalsVideoViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 2
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}

extension SuperRoyalsVideoViewController {
    func getSuperRoyalsVideos(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE]
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETSUPERROYALSVIDEO.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.videoList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let videosData = responseData[kData] as? [[String : AnyObject]] {
                        for videos in videosData {
                            self.videoList.append(videos)
                        }
                    }
                    
                    self.videoCV.reloadData()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
