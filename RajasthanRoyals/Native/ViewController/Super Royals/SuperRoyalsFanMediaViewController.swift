//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class SuperRoyalsFanMediaViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var mediaCV: UICollectionView!
    @IBOutlet weak var lblEmptyMessage: UILabel!
    
    var superRoyalName = ""
    var superRoyalId = 0
    
    var currentPage = 1
    var nextPage = kN
    var mediaList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.lblTitle.text = superRoyalName
        
        self.getSuperRoyalsMediaList(isShowHud: true)
    }
}

extension SuperRoyalsFanMediaViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mediaList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == mediaList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getSuperRoyalsMediaList(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsCell
        
        if let imageUrl = mediaList[indexPath.item][kThumbnailImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if let title = mediaList[indexPath.item][kTitle] as? String {
            cell.lblTitle.text = title
        }
        
        cell.imgPlay.isHidden = true
        if mediaList[indexPath.item][kMediaType] as? String == ITEMTYPE.Video.rawValue {
            cell.imgPlay.isHidden = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if mediaList[indexPath.item][kMediaType] as? String == ITEMTYPE.Image.rawValue {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
            viewController.imageUrl = mediaList[indexPath.item][kThumbnailImage] as? String ?? ""
            self.present(viewController, animated: true, completion: nil)
        }
        else if mediaList[indexPath.item][kMediaType] as? String == ITEMTYPE.Video.rawValue {
            if let type = mediaList[indexPath.row][kVideoType] as? String {
                if type == VIDEOTYPE.LOCAL.rawValue {
                    if let video = mediaList[indexPath.row][kVideo] as? String {
                        if let videoURL = URL(string: video) {
                            let player = AVPlayer(url: videoURL)
                            let playerViewController = AVPlayerViewController()
                            playerViewController.player = player
                            self.present(playerViewController, animated: true) {
                                playerViewController.player!.play()
                            }
                        }
                    }
                }
                else {
                    if let videoUrl = mediaList[indexPath.row][kVideo] as? String {
                        let array = videoUrl.components(separatedBy: "=")
                        if array.count > 1 {
                            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "YouTubePlayerViewController") as! YouTubePlayerViewController
                            viewController.videoId = array[1]
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }
                }
            }
        }
        else {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as! NewsDetailsViewController
            viewController.isFromSuperRoyalsMedia = true
            viewController.newsDetails = mediaList[indexPath.row]
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension SuperRoyalsFanMediaViewController {
    func getSuperRoyalsMediaList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["super_royal_id": self.superRoyalId, kPage: self.currentPage, kPageSize: PAGESIZE]
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETSUPERROYALFOCUSDETAILS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.mediaList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let videosData = responseData[kData] as? [[String : AnyObject]] {
                        for videos in videosData {
                            self.mediaList.append(videos)
                        }
                    }
                    
                    self.lblEmptyMessage.text = responseData[kMessage] as? String ?? ""
                    if self.mediaList.count > 0 {
                        self.lblEmptyMessage.isHidden = true
                    }
                    else {
                        self.lblEmptyMessage.isHidden = false
                    }
                    
                    self.mediaCV.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension SuperRoyalsFanMediaViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 1
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}
