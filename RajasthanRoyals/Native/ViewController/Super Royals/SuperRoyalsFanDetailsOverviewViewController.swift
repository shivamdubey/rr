//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class SuperRoyalsFanDetailsOverviewViewController: BaseViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var txtBio: UITextView!

    var superRoyalsDetails = [String: AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUserDetails()
    }
    
    func setUserDetails() {
        self.lblName.text = superRoyalsDetails[kName] as? String ?? ""
        self.lblCountry.text = superRoyalsDetails[kCountry] as? String ?? ""
        
        if let bio = superRoyalsDetails[kBio] as? String {
            self.txtBio.attributedText = bio.html2Attributed
        }
        
        if superRoyalsDetails[kTwitterUrl] as? String == "" {
            self.btnTwitter.isHidden = true
        }
        else {
            self.btnTwitter.isHidden = false
        }
    }
    
    @IBAction func btnTwitter_Clicked(_ sender: Any) {
        if let url = superRoyalsDetails[kTwitterUrl] as? String {
            self.openUrlInSafariViewController(urlString: url)
        }
    }
}
