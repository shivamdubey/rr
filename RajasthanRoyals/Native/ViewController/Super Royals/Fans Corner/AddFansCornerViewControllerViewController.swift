//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD
import MessageUI

class AddFansCornerViewControllerViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtTitle: CustomTextField!
    @IBOutlet weak var lblTitleCount: UILabel!
    @IBOutlet weak var txtMessage: CustomTextView!
    @IBOutlet weak var lblAddPhoto: UILabel!
    @IBOutlet weak var lblAddVideo: UILabel!
    @IBOutlet weak var mediaCV: UICollectionView!
    @IBOutlet weak var lblSendBlog: UILabel!
    @IBOutlet weak var btnSendBlog: UIButton!
    @IBOutlet weak var btnSubmit: CustomButton!

    var mediaList = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_CreateYourBlog")
        self.txtTitle.placeholder = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_AddTitle")
        self.lblAddPhoto.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_AddPhoto")
        self.lblAddVideo.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_AddVideo")
        self.lblSendBlog.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_SendBlog")
        
        self.txtMessage.textColor = UIColor.lightGray
        self.txtMessage.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_WriteHere")
    }
    
    func showActionSheet(isPhoto: Bool) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.view.tintColor = UIColor().alertButtonColor
        
        actionSheet.addAction(UIAlertAction(title: isPhoto ? self.getLocalizeTextForKey(keyName: "btn_UsingCamera") : self.getLocalizeTextForKey(keyName: "btn_CaptureVideo"), style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera(isPhoto: isPhoto)
        }))
        
        actionSheet.addAction(UIAlertAction(title: isPhoto ? self.getLocalizeTextForKey(keyName: "btn_ChooseExistingPhoto") : self.getLocalizeTextForKey(keyName: "btn_ChooseExistingVideo"), style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary(isPhoto: isPhoto)
        }))
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera(isPhoto: Bool) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        
        if isPhoto {
            myPickerController.sourceType = .camera
        }
        else {
            myPickerController.sourceType = .camera
            myPickerController.mediaTypes = ["public.movie"]
            myPickerController.videoMaximumDuration = 30.0
        }
        
        myPickerController.modalPresentationStyle = .overCurrentContext
        myPickerController.addStatusBarBackgroundView()
        myPickerController.view.tintColor = UIColor().alertButtonColor
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary(isPhoto: Bool) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        
        if isPhoto {
            myPickerController.sourceType = .savedPhotosAlbum
        }
        else {
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = ["public.movie"]
            myPickerController.videoMaximumDuration = 30.0
        }
        
        myPickerController.modalPresentationStyle = .overCurrentContext
        myPickerController.addStatusBarBackgroundView()
        myPickerController.view.tintColor = UIColor().alertButtonColor
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    @IBAction func btnInfo_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        viewController.strTitle = self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_FansCorner")
        viewController.strValueForAPI = "Fans Corner"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnAddPhoto_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        self.showActionSheet(isPhoto: true)
    }
    
    @IBAction func btnAddVideo_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        self.showActionSheet(isPhoto: false)
    }
    
    @IBAction func btnSendBlog_Clicked(_ sender: Any) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.view.tintColor = UIColor().alertButtonColor
        
        actionSheet.addAction(UIAlertAction(title: "Apple Mail", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            if MFMailComposeViewController.canSendMail() {
                let toEmail = "rr.superroyals@gmail.com"
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients([toEmail])
                mail.setSubject("Fan's Corner")
                self.present(mail, animated: true)
            }
            else {
                UIAlertController().alertViewWithTitleAndMessage(self, message: "Default apple mail application is not installed in you iPhone.")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gmail", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            let toEmail = "rr.superroyals@gmail.com"
            let subject = "Fan's%20Corner"
            if let gmailUrl = URL(string: "googlegmail://co?to=\(toEmail)&subject=\(subject)") {
                if UIApplication.shared.canOpenURL(gmailUrl) {
                    UIApplication.shared.open(gmailUrl, options: [:], completionHandler: nil)
                }
            }
            else {
                UIAlertController().alertViewWithTitleAndMessage(self, message: "Gmail application is not installed in you iPhone.")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func btnSubmit_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        if self.txtTitle.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Title"))
        }
        else if self.txtMessage.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Message"))
        }
        else if self.mediaList.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_SelectMedia"))
        }
        else {
            self.addFansCorner()
        }
    }
}

extension AddFansCornerViewControllerViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mediaList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddFansCorner", for: indexPath) as! AddFansCorner
        cell.imgPlay.isHidden = true
        
        if let image = mediaList[indexPath.item] as? UIImage {
            cell.imgMedia.image = image
        }
        else if let url = mediaList[indexPath.item] as? URL {
            cell.imgPlay.isHidden = false
            self.getThumbnailImageFromVideoUrl(url: url, completion: { image in
                cell.imgMedia.image = image
            })
        }
        
        cell.btnDelete.addTarget(self, action: #selector(btnDelete_Cliked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let image = mediaList[indexPath.item] as? UIImage {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
            viewController.image = image
            self.present(viewController, animated: true, completion: nil)
        }
        else if let url = mediaList[indexPath.item] as? URL {
            let player = AVPlayer(url: url)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    @objc func btnDelete_Cliked(_ sender: AnyObject) {
        let position = sender.convert(CGPoint.zero, to: mediaCV)
        if let indexPath = mediaCV.indexPathForItem(at: position) {
            self.mediaList.remove(at: indexPath.item)
            self.mediaCV.deleteItems(at: [indexPath])
        }
    }
}

extension AddFansCornerViewControllerViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
        if let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String {
            if mediaType == "public.movie" {
                for i in 0..<self.mediaList.count {
                    if (self.mediaList[i] as? URL) != nil {
                        self.mediaList.remove(at: i)
                        break
                    }
                }
                let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as! URL
                self.mediaList.append(videoURL)
                self.mediaCV.reloadData()
            }
            else {
                if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                    self.mediaList.append(image)
                    self.mediaCV.reloadData()
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddFansCornerViewControllerViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtMessage.becomeFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength: Int = 200
        if textField.text!.count + (string.count - range.length) <= maxLength {
            self.lblTitleCount.text = "\(textField.text!.count + (string.count - range.length))/\(maxLength)"
        }
        return textField.text!.count + (string.count - range.length) <= maxLength
    }
}

extension AddFansCornerViewControllerViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            self.txtMessage.textColor = UIColor.black
            self.txtMessage.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trim().isEmpty {
            self.txtMessage.textColor = UIColor.lightGray
            self.txtMessage.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_WriteHere")
        }
    }
}

class AddFansCorner: UICollectionViewCell {
    @IBOutlet weak var imgMedia: UIImageView!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
}

extension AddFansCornerViewControllerViewController {
    func addFansCorner() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "Fancorner[title]": self.txtTitle.text!,
                         "Fancorner[description]": self.txtMessage.text!,
                         "lang": BaseViewController.sharedInstance.appLanguage] as [String : Any]
                
            KRProgressHUD.show(withMessage: "Please wait")
            
            Alamofire.upload(multipartFormData: {
                multipartFormData in
                for index in 0..<self.mediaList.count {
                    if let image = self.mediaList[index] as? UIImage {
                        print("Image")
                        //Image
                        if let imageData = image.jpegData(compressionQuality: 0.6) {
                            print("Image Data")
                            multipartFormData.append(imageData, withName: "\("Fancorner[file]")[\(index)]", fileName: "\("FansCornerImage")\(index).png", mimeType: "image/png")
                        }
                    }
                    else if let url = self.mediaList[index] as? URL {
                        //Video
                        print("Video")
                        do {
                            let mediaData = try Data(contentsOf: url)
                            print("Video Data")
                            multipartFormData.append(mediaData, withName: "\("Fancorner[file]")[\(index)]", fileName: "\("FansCornerVideo")\(index).mp4", mimeType: "video/mp4")
                        }
                        catch let error {
                            print("Error while convert video data: \(error.localizedDescription)")
                            return
                        }
                    }
                }
                
                //Append Param
                for (key, value) in param {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, usingThreshold: 1, to: baseURLWithAuth + APINAME.ADDFANCORNER.rawValue, method: .post, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: {
                        response in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            KRProgressHUD.dismiss()
                        }
                        switch response.result {
                        case .success:
                            if let json: NSDictionary = response.result.value as? NSDictionary {
                                print(json)
                                if json[kStatus] as? Int == kIsSuccess {
                                    if let message = json[kMessage] as? String {
                                        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                                        alert.view.tintColor = UIColor().alertButtonColor
                                        
                                        let hideAction: UIAlertAction = UIAlertAction.init(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                            self.navigationController?.popViewController(animated: true)
                                        })
                                        alert.addAction(hideAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else if json[kStatus] as? Int == kUserNotFound {
                                    if let message = json[kMessage] as? String {
                                        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                                        alert.view.tintColor = UIColor().alertButtonColor
                                        
                                        let hideAction: UIAlertAction = UIAlertAction.init(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                            BaseViewController.sharedInstance.clearAllUserDataFromPreference()
                                            BaseViewController.sharedInstance.navigateToLoginScreen()
                                        })
                                        alert.addAction(hideAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else {
                                    UIAlertController().alertViewWithTitleAndMessage(self, message: json[kMessage] as? String ?? "")
                                }
                            }
                            else {
                                UIAlertController().alertViewWithTitleAndMessage(self, message: ErrorMessage)
                            }
                        case .failure(let error):
                            UIAlertController().alertViewWithTitleAndMessage(self, message: error.localizedDescription)
                        }
                    })
                case .failure(let encodingError):
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        KRProgressHUD.dismiss()
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: encodingError.localizedDescription)
                }
            })
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension AddFansCornerViewControllerViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}
