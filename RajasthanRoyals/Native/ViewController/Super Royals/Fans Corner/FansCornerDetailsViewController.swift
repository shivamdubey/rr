//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class FansCornerDetailsViewController: BaseViewController {
    
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgUser: CustomImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblTotalLikes: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblShare: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    
    var slug = ""
    var fansCornerDetails = [String: AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblShare.text = self.getLocalizeTextForKey(keyName: "btn_Share")
        
        self.imageScrollView.auk.settings.placeholderImage = #imageLiteral(resourceName: "placeholder")
        self.imageScrollView.auk.settings.contentMode = .scaleAspectFill
        self.imageScrollView.auk.settings.pagingEnabled = false
        self.imageScrollView.auk.startAutoScroll(delaySeconds: 3.0)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnBanner))
        self.imageScrollView.isUserInteractionEnabled = true
        self.imageScrollView.addGestureRecognizer(tapGesture)
        
        self.getFansCornerDetails()
    }
    
    @objc func tappedOnBanner() {
        if let images = fansCornerDetails[kImages] as? [[String: AnyObject]] {
            if images.count > self.imageScrollView.auk.currentPageIndex ?? 0 {
                if images[self.imageScrollView.auk.currentPageIndex ?? 0][kType] as? String == "Video" {
                    if let url = images[self.imageScrollView.auk.currentPageIndex ?? 0][kFiles] as? String, !url.isEmpty {
                        let videoURL = URL(string: url)
                        let player = AVPlayer(url: videoURL!)
                        let playerViewController = AVPlayerViewController()
                        playerViewController.player = player
                        self.present(playerViewController, animated: true) {
                            playerViewController.player!.play()
                        }
                    }
                }
                else {
                    let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
                    viewController.imageUrl = images[self.imageScrollView.auk.currentPageIndex ?? 0][kFiles] as? String ?? ""
                    self.present(viewController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func setFansCorderDetails() {
        if let images = fansCornerDetails[kImages] as? [[String: AnyObject]] {
            self.imageScrollView.auk.removeAll()
            for image in images {
                if image[kType] as? String == "Image" {
                    self.imageScrollView.auk.show(url: image[kFiles] as? String ?? "")
                }
                else {
                    self.imageScrollView.auk.show(url: image["thumb"] as? String ?? "")
                }
            }
        }
        
        self.lblTitle.text = fansCornerDetails[kTitle] as? String ?? ""
        
        if let user = fansCornerDetails["user"] as? [String: AnyObject] {
            self.lblUserName.text = user[kFullName] as? String ?? ""
            
            if let imgUrl = URL.init(string: user[kImage] as? String ?? "") {
                self.imgUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        let createdDate = self.convertStringToDate(format: "yyyy-MM-dd HH:mm:ss", strDate: fansCornerDetails["created_at"] as? String ?? "")
        self.lblDate.text = self.convertDateToString(format: "dd MMM yyyy", date: createdDate)
        
        self.lblTotalLikes.text = "\(fansCornerDetails["like_count"] as? Int ?? 0) \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes"))"
        
        if fansCornerDetails["is_like"] as? String == kYes {
            self.btnLike.isSelected = true
        }
        else {
            self.btnLike.isSelected = false
        }
        
        self.txtDescription.attributedText = (fansCornerDetails[kDescription] as? String ?? "").html2Attributed
    }
    
    @IBAction func btnLike_clicked(_ sender: Any) {
        if let fansCornerId = self.fansCornerDetails["fan_corner_id"] as? Int {
            self.likeUnlikeFansCorner(fansCornerId: fansCornerId)
        }
    }
    
    @IBAction func btnShare_clicked(_ sender: Any) {
        if let fansCornerUrl = URL.init(string: "https://rajasthanroyals.page.link/FansCorner?slug=\(self.slug)") {
            let objectsToShare = [fansCornerUrl] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}

extension FansCornerDetailsViewController {
    func getFansCornerDetails() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kSlug: self.slug, "is_html": kYes]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETFANCORNERDETAILS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    if let data = responseData[kData] as? [String : AnyObject] {
                        self.fansCornerDetails = data
                    }
                    
                    self.callTMAnalyticsAPI(category: "Fan's Corner", action: "View", label: self.fansCornerDetails[kTitle] as? String ?? "")
                    
                    self.setFansCorderDetails()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func likeUnlikeFansCorner(fansCornerId: Int) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "Fancornerlike[fan_corner_id]": fansCornerId] as [String : Any]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.ADDFANCORNERLIKE.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if let likeCount = self.fansCornerDetails["like_count"] as? Int {
                        if self.fansCornerDetails["is_like"] as? String == kYes {
                            self.fansCornerDetails["is_like"] = kNo as AnyObject
                            self.btnLike.isSelected = false
                            
                            self.fansCornerDetails["like_count"] = (likeCount - 1) as AnyObject
                            self.lblTotalLikes.text = "\(self.fansCornerDetails["like_count"] as? Int ?? 0) \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes"))"
                        }
                        else {
                            self.fansCornerDetails["is_like"] = kYes as AnyObject
                            self.btnLike.isSelected = true
                            
                            self.fansCornerDetails["like_count"] = (likeCount + 1) as AnyObject
                            self.lblTotalLikes.text = "\(self.fansCornerDetails["like_count"] as? Int ?? 0) \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes"))"
                        }
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension FansCornerDetailsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == imageScrollView {
            if let images = self.fansCornerDetails[kImages] as? [[String: AnyObject]] {
                if images.count > self.imageScrollView.auk.currentPageIndex ?? 0 {
                    if images[self.imageScrollView.auk.currentPageIndex ?? 0][kType] as? String == "Image" {
                        self.imgPlay.isHidden = true
                    }
                    else {
                        self.imgPlay.isHidden = false
                    }
                }
            }
        }
    }
}
