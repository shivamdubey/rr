//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class SuperRoyalsFanDetailsViewController: BaseViewController {
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrev: UIButton!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var viewBottomSheet: UIView!

    var currentIndex = 0
    var superRoyalsList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addFirebaseCustomeEvent(eventName: "SuperRoyalsFanDetailsScreen")
        
        if currentIndex == 0 && self.superRoyalsList.count == 1 {
            self.btnPrev.isHidden = true
            self.btnNext.isHidden = true
        }
        else if currentIndex == 0 {
            self.btnPrev.isHidden = true
        }
        else if currentIndex == self.superRoyalsList.count - 1 {
            self.btnNext.isHidden = true
        }
        
        self.setUserDetails()
    }
    
    func setUserDetails() {
        if superRoyalsList.count > currentIndex {
            if let imageUrl = superRoyalsList[currentIndex][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    self.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "playerplaceholder"))
                }
            }
            
            if self.children.count > 0 {
                let viewControllers:[UIViewController] = self.children
                for viewContoller in viewControllers {
                    viewContoller.willMove(toParent: nil)
                    viewContoller.view.removeFromSuperview()
                    viewContoller.removeFromParent()
                }
            }
            
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SuperRoyalsFanDetailsOverviewViewController") as! SuperRoyalsFanDetailsOverviewViewController
            viewController.superRoyalsDetails = superRoyalsList[currentIndex]
            let sheet = SheetViewController(controller: viewController, sizes: [.fixed(100.0), .fullscreen], options: SheetOptions(useInlineMode: true))
            sheet.overlayColor = .clear
            sheet.dismissOnPull = false
            sheet.dismissOnOverlayTap = false

            // Add child
            sheet.willMove(toParent: self)
            self.addChild(sheet)
            self.viewBottomSheet.addSubview(sheet.view)
            sheet.didMove(toParent: self)
            
            sheet.view.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                sheet.view.topAnchor.constraint(equalTo: self.viewBottomSheet.topAnchor),
                sheet.view.bottomAnchor.constraint(equalTo: self.viewBottomSheet.bottomAnchor),
                sheet.view.leadingAnchor.constraint(equalTo: self.viewBottomSheet.leadingAnchor),
                sheet.view.trailingAnchor.constraint(equalTo: self.viewBottomSheet.trailingAnchor)
            ])
            
            sheet.didDismiss = { [weak self] _ in
                print("did dismiss")
            }
            
            sheet.shouldDismiss = { _ in
                print("should dismiss")
                return true
            }
            sheet.animateIn()
        }
    }
    
    @IBAction func btnNext_Clicked(_ sender: Any) {
        currentIndex = currentIndex + 1
        self.btnPrev.isHidden = false
        
        self.setUserDetails()
        
        if currentIndex == superRoyalsList.count - 1 {
            self.btnNext.isHidden = true
        }
        else {
            self.btnNext.isHidden = false
        }
    }
    
    @IBAction func btnPrev_Clicked(_ sender: Any) {
        currentIndex = currentIndex - 1
        self.btnNext.isHidden = false
        
        self.setUserDetails()
        
        if currentIndex == 0 {
            self.btnPrev.isHidden = true
        }
        else {
            self.btnPrev.isHidden = false
        }
    }
}
