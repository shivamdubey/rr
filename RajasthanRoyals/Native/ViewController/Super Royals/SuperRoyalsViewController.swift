//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit
import AVKit
import LinkPresentation

enum SUPERROYALSTYPE : String {
    case FANWALL = "Fanwall"
    case FANSCORNER  = "FansCorner"
    case SUPERROYALS  = "SuperRoyals"
}

class SuperRoyalsViewController: BaseViewController {
    
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitle1: UILabel!
    @IBOutlet weak var bgViewForButtons: CustomView!
    @IBOutlet weak var btnFeeds: CustomButton!
    @IBOutlet weak var btnSuperRoyals: CustomButton!
    @IBOutlet weak var btnFansCorner: CustomButton!
    @IBOutlet weak var superRoyalsScrollView: UIScrollView!
    @IBOutlet weak var lblVideo: UILabel!
    @IBOutlet weak var btnVideoViewMore: UIButton!
    @IBOutlet weak var videoCV: UICollectionView!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblSuperRoyals: UILabel!
    @IBOutlet weak var btnSuperRoyalsViewMore: UIButton!
    @IBOutlet weak var superRoyalsCV: UICollectionView!
    @IBOutlet weak var tblFeed: UITableView!
    @IBOutlet weak var btnCreatePost: UIButton!
    @IBOutlet weak var superRoyalsBottomView: UIView!
    @IBOutlet weak var superRoyalsBottomView1: UIView!
    @IBOutlet weak var lblFollowSuperRoyals: UILabel!

    var type = SUPERROYALSTYPE.FANWALL.rawValue
    var superRoyalName = ""
    var superRoyalId = 0
    
    var videoList = [[String: AnyObject]]()
    var superRoyalsList = [[String: AnyObject]]()
    var bannerUrl = ""
    
    var refreshControl: UIRefreshControl!
    var feedList = [FeedListModel]()
    var lastFeedId = ""
    
    var fansCornerList = [[String: AnyObject]]()
    var currentPage = 1
    var nextPage = kN
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgViewForButtons.backgroundColor = SegmentViewBGColor
        
        self.tblFeed.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
        
        
        self.setupLocalizationText()
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnBanner))
        tapGesture.numberOfTapsRequired = 1
        self.imgBanner.isUserInteractionEnabled = true
        self.imgBanner.addGestureRecognizer(tapGesture)
        
        refreshControl = UIRefreshControl.init()
        refreshControl.tintColor = UIColor().themeDarkPinkColor
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(self.refreshTableView(sender:)), for: .valueChanged)
        tblFeed.refreshControl = refreshControl
        
        self.btnFeeds_Clicked(sender: self.btnFeeds)
        
        self.getFeedList(isShowHud: true)
        self.getSuperRoyalsData()
        self.getFansCornerList(isShowHud: false)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "playVideoFromCell"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.playVideoFromCell(_:)), name: NSNotification.Name(rawValue: "playVideoFromCell"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "callGetAllPostAPI"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.callGetAllPostAPI(_:)), name: NSNotification.Name(rawValue: "callGetAllPostAPI"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "OpenPhotoVCFromCell"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.OpenPhotoVCFromCell(_:)), name: NSNotification.Name(rawValue: "OpenPhotoVCFromCell"), object: nil)
    }
    
    func setupLocalizationText() {
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Home_lbl_SuperRoyals")
        self.lblTitle1.text = self.getLocalizeTextForKey(keyName: "Home_lbl_FanLounge")
        self.lblFollowSuperRoyals.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_FollowSuperRoyals")
        
        self.btnFeeds.setTitle(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Feeds"), for: .normal)
        self.btnFansCorner.setTitle(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_FansCorner"), for: .normal)
        self.btnSuperRoyals.setTitle(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_SuperRoyals"), for: .normal)
        
        self.lblVideo.text = self.getLocalizeTextForKey(keyName: "Home_lbl_Videos")
        self.btnVideoViewMore.setTitle(self.getLocalizeTextForKey(keyName: "Home_btn_SeeAll"), for: .normal)
        
        self.lblSuperRoyals.text = self.getLocalizeTextForKey(keyName: "Home_lbl_SuperRoyals")
        self.btnSuperRoyalsViewMore.setTitle(self.getLocalizeTextForKey(keyName: "Home_btn_SeeAll"), for: .normal)
    }
    
    @objc func tappedOnBanner() {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SuperRoyalsFanMediaViewController") as! SuperRoyalsFanMediaViewController
        viewController.superRoyalId = self.superRoyalId
        viewController.superRoyalName = self.superRoyalName
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //UIRefreshControl method
    @objc func refreshTableView(sender: AnyObject) {
        if self.type == SUPERROYALSTYPE.FANWALL.rawValue {
            self.lastFeedId = ""
            self.getFeedList(isShowHud: false)
        }
        else {
            self.currentPage = 1
            self.getFansCornerList(isShowHud: false)
        }
    }
    
    @objc func callGetAllPostAPI(_ notification: NSNotification) {
        self.tblFeed.setContentOffset(.zero, animated: true)
        self.lastFeedId = ""
        self.getFeedList(isShowHud: false)
    }
    
    @objc func playVideoFromCell(_ notification: NSNotification) {
        if let videoUrl = notification.userInfo?["dictVideo"] as? String {
            if let videoURL = URL(string: videoUrl) {
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }
    }
    
    @objc func OpenPhotoVCFromCell(_ notification: NSNotification) {
        if let visibleIndex = notification.userInfo?["visibleIndex"] as? Int {
            if let multiplePhoto = notification.userInfo?["multiplePhoto"] as? [FeedImages] {
                let imagesList = multiplePhoto.map({$0.file!})
                let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FeedPhotosViewController") as! FeedPhotosViewController
                viewController.imagesList = imagesList
                viewController.visibleIndex = visibleIndex
                self.present(viewController, animated: true, completion: nil)
            }
        }
        
        if let singlePhoto = notification.userInfo?["singlePhoto"] as? String {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
            viewController.imageUrl = singlePhoto
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnFeeds_Clicked(sender: UIButton) {
        self.type = SUPERROYALSTYPE.FANWALL.rawValue
        
        self.superRoyalsBottomView.isHidden = true
        self.superRoyalsBottomView1.isHidden = true
        
        self.imgHeader.isHidden = true
        self.lblTitle.isHidden = true
        self.lblTitle1.isHidden = false
        
        self.addFirebaseCustomeEvent(eventName: "SuperRoyalsFeedsScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "SuperRoyalsPage: Feeds")
        
        self.btnFeeds.backgroundColor = SelectedButtonBGColor
        self.btnFansCorner.backgroundColor = DeSelectedButtonBGColor
        self.btnSuperRoyals.backgroundColor = DeSelectedButtonBGColor
        
        self.btnFeeds.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnFansCorner.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnSuperRoyals.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.tblFeed.isHidden = false
        self.btnCreatePost.isHidden = false
        self.superRoyalsScrollView.isHidden = true
        
        self.tblFeed.backgroundColor = .white
        self.tblFeed.reloadData()
    }
    
    @IBAction func btnFansCorner_Clicked(sender: UIButton) {
        self.type = SUPERROYALSTYPE.FANSCORNER.rawValue
        
        self.superRoyalsBottomView.isHidden = true
        self.superRoyalsBottomView1.isHidden = true
        
        self.imgHeader.isHidden = true
        self.lblTitle.isHidden = true
        self.lblTitle1.isHidden = false
        
        self.addFirebaseCustomeEvent(eventName: "SuperRoyalsFansCornerScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "SuperRoyalsPage: Fans Corner")
        
        self.btnFeeds.backgroundColor = DeSelectedButtonBGColor
        self.btnFansCorner.backgroundColor = SelectedButtonBGColor
        self.btnSuperRoyals.backgroundColor = DeSelectedButtonBGColor
        
        self.btnFeeds.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnFansCorner.setTitleColor(SelectedButtonTextColor, for: .normal)
        self.btnSuperRoyals.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        
        self.tblFeed.isHidden = false
        self.btnCreatePost.isHidden = false
        self.superRoyalsScrollView.isHidden = true
        
        self.tblFeed.backgroundColor = .clear
        self.tblFeed.reloadData()
    }
    
    @IBAction func btnSuperRoyals_Clicked(sender: UIButton) {
        self.type = SUPERROYALSTYPE.SUPERROYALS.rawValue
        
        self.superRoyalsBottomView.isHidden = false
        self.superRoyalsBottomView1.isHidden = false
        
        self.imgHeader.isHidden = false
        self.lblTitle.isHidden = false
        self.lblTitle1.isHidden = true
        
        self.addFirebaseCustomeEvent(eventName: "SuperRoyalsDetailsScreen")
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "SuperRoyalsPage: Super Royals")
        
        self.btnFeeds.backgroundColor = DeSelectedButtonBGColor
        self.btnFansCorner.backgroundColor = DeSelectedButtonBGColor
        self.btnSuperRoyals.backgroundColor = SelectedButtonBGColor
        
        self.btnFeeds.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnFansCorner.setTitleColor(DeSelectedButtonTextColor, for: .normal)
        self.btnSuperRoyals.setTitleColor(SelectedButtonTextColor, for: .normal)
        
        self.tblFeed.isHidden = true
        self.btnCreatePost.isHidden = true
        self.superRoyalsScrollView.isHidden = false
    }
    
    @IBAction func btnViewMore_Clicked(_ sender: UIButton) {
        if sender.tag == 100 {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SuperRoyalsVideoViewController") as! SuperRoyalsVideoViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SuperRoyalsFanViewController") as! SuperRoyalsFanViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func btnInsta_Clicked(_ sender: UIButton) {
        self.openUrlInSafariViewController(urlString: "https://www.instagram.com/rr_superroyals/?hl=en")
    }
    
    @IBAction func btnFacebook_Clicked(_ sender: UIButton) {
        self.openUrlInSafariViewController(urlString: "https://www.facebook.com/groups/rajasthanroyalsfanclub")
    }
    
    @IBAction func btnCreatePost_clicked(_ sender: Any) {
        if self.type == SUPERROYALSTYPE.FANWALL.rawValue {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else {
            let viewController = self.newStoryBoard.instantiateViewController(withIdentifier: "AddFansCornerViewControllerViewController") as! AddFansCornerViewControllerViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension SuperRoyalsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == videoCV {
            return videoList.count
        }
        else {
            return superRoyalsList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == videoCV {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVideoNewCell", for: indexPath) as! HomeVideoNewCell
            
            if let imageUrl = videoList[indexPath.row][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgVideoNews.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
            cell.imgVideoNews.layer.borderWidth = 2
            cell.imgVideoNews.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
            cell.imgVideoNews.layer.cornerRadius = 5
            
            if let title = videoList[indexPath.row][kTitle] as? String {
                cell.lblTitle.text = title
            }
            
            cell.btnPlayVideo.isHidden = false
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuperRoyalsCell", for: indexPath) as! SuperRoyalsCell
            
            if let imageUrl = superRoyalsList[indexPath.row][kImage] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
            cell.imgUser.layer.borderWidth = 2
            cell.imgUser.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
            cell.imgUser.layer.cornerRadius = 5
            
            cell.lblName.text = superRoyalsList[indexPath.row][kName] as? String ?? ""
            cell.lblCountry.text = superRoyalsList[indexPath.row][kCountry] as? String ?? ""
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == videoCV {
            if let type = videoList[indexPath.row][kType] as? String {
                if type == VIDEOTYPE.LOCAL.rawValue {
                    if let video = videoList[indexPath.row][kVideo] as? String {
                        if let videoURL = URL(string: video) {
                            let player = AVPlayer(url: videoURL)
                            let playerViewController = AVPlayerViewController()
                            playerViewController.player = player
                            self.present(playerViewController, animated: true) {
                                playerViewController.player!.play()
                            }
                        }
                    }
                }
                else {
                    if let videoUrl = videoList[indexPath.row][kVideo] as? String {
                        let array = videoUrl.components(separatedBy: "=")
                        if array.count > 1 {
                            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "YouTubePlayerViewController") as! YouTubePlayerViewController
                            viewController.videoId = array[1]
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }
                }
            }
        }
        else {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "SuperRoyalsFanDetailsViewController") as! SuperRoyalsFanDetailsViewController
            viewController.superRoyalsList = self.superRoyalsList
            viewController.currentIndex = indexPath.item
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension SuperRoyalsViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.type == SUPERROYALSTYPE.FANWALL.rawValue {
            return self.feedList.count
        }
        else {
            return self.fansCornerList.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.type == SUPERROYALSTYPE.FANWALL.rawValue {
            return tableView.estimatedRowHeight
        }
        else {
            return tableView.estimatedRowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.type == SUPERROYALSTYPE.FANWALL.rawValue {
            if indexPath.row == self.feedList.count - 1 {
                if let feed_id = self.feedList.last?.feed_id {
                    self.lastFeedId = "\(feed_id)"
                    self.getFeedList(isShowHud: false)
                }
            }
        }
        else {
            if indexPath.row == self.fansCornerList.count - 1 && self.nextPage == kY {
                self.currentPage = self.currentPage + 1
                self.getFansCornerList(isShowHud: false)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.type == SUPERROYALSTYPE.FANWALL.rawValue {
            let objModel = self.feedList[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTextMediaCell", for: indexPath) as! FeedTextMediaCell

            cell.lblName.text = objModel.user?.full_name
            
            cell.txtViewDesc.isHidden = (objModel.description == "") ? true : false
            cell.txtViewDesc.text = objModel.description ?? ""
            cell.constTopForTxtViewDesc.constant = (cell.txtViewDesc.text == "") ? -25 : 5
            cell.txtViewDesc.textContainer.lineBreakMode = .byTruncatingTail
            cell.txtViewDesc.textContainer.maximumNumberOfLines = 3
            
            cell.btnViewMoreText.setTitle(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ViewMore"), for: .normal)
            cell.btnViewMoreText.isHidden = true
            
            cell.constTopForBtnViewMoreText.constant = -cell.btnViewMoreText.frame.size.height
            
            let numOfLine = cell.txtViewDesc.maxNumberOfLines
            if numOfLine > 3 {
                cell.btnViewMoreText.isHidden = false
                cell.constTopForBtnViewMoreText.constant = 0
                cell.btnViewMoreText.addTarget(self, action: #selector(btnViewMoreText_Clicked(sender:)), for: .touchUpInside)
            }
            
            if objModel.isReadMore {
                cell.txtViewDesc.textContainer.maximumNumberOfLines = 0
                cell.btnViewMoreText.isHidden = true
                cell.constTopForBtnViewMoreText.constant = -cell.btnViewMoreText.frame.size.height
            }
            
            let createdDate = self.convertStringToDate(format: dateFormateForGet, strDate: objModel.created_at ?? "")
            cell.lblTime.text = self.convertDateToString(format: dateFormateForDisplay, date: createdDate)
            
            if let url = objModel.user?.image {
                if let imgUrl = URL.init(string: url) {
                    cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
            
            cell.imgVerify.isHidden = (objModel.user?.is_verified ?? "") == kYes ? false : true
            
            if objModel.is_like == kYes {
                cell.btnLike.setImage(UIImage(named: "heart"), for: .normal)
            }
            else {
                cell.btnLike.setImage(UIImage(named: "heart-empty"), for: .normal)
            }
            
            if objModel.is_bookmark == kYes {
                cell.btnBookmark.setImage(UIImage(named: "bookmark-fill"), for: .normal)
            }
            else {
                cell.btnBookmark.setImage(UIImage(named: "Bookmark"), for: .normal)
            }
            
            cell.lblTotalLikes.text = "\(objModel.like_count ?? "0") \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes"))"
            cell.lblTotalComments.text = "\(objModel.comment_count ?? "0") \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Comments"))"
            
            cell.btnUserProfile.addTarget(self, action: #selector(self.btnUserProfile_Clicked(_:)), for: .touchUpInside)
            cell.btnLike.addTarget(self, action: #selector(self.btnLike_Clicked(_:)), for: .touchUpInside)
            cell.btnTotalLikeUser.addTarget(self, action: #selector(self.btnTotalLikeUser_Clicked(_:)), for: .touchUpInside)
            cell.btnBookmark.addTarget(self, action: #selector(self.btnBookmark_Clicked(_:)), for: .touchUpInside)
            cell.btnMore.addTarget(self, action: #selector(self.btnMore_Clicked(_:)), for: .touchUpInside)
            
            cell.constHeightForViewLink.constant = 0
            cell.constHeightForCVImageVideo.constant = 0
            if objModel.images!.count > 0 {
                cell.cvImageVideo.tag = indexPath.row
                cell.setCollectionViewWith(objModel: objModel)
                
                cell.constHeightForViewLink.constant = 0
                cell.constHeightForCVImageVideo.constant = objModel.collectionViewHeight
                cell.viewLink.isHidden = true
            }
            else{
                cell.constHeightForCVImageVideo.constant = 0
                cell.pageControlView.isHidden = true
                cell.viewLink.isHidden = false
                
                for subview in cell.viewLink.subviews {
                    if subview is LPLinkView {
                        subview.removeFromSuperview()
                    }
                }
                
                do {
                    let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                    let range = NSRange(objModel.description!.startIndex..<objModel.description!.endIndex, in: objModel.description!)
                    if let firstMatch = detector.firstMatch(in: objModel.description!, options: [], range: range) {
                        cell.activityIndicator.startAnimating()
                        var url = firstMatch.url!
                        if url.scheme == "http" {
                            if var urlComps = URLComponents(url: url, resolvingAgainstBaseURL: false) {
                                urlComps.scheme = "https"
                                url = urlComps.url!
                            }
                        }
                        let file_name = "\(url)"
                        if self.metaDataDict.keys.contains(file_name) {
                            cell.activityIndicator.stopAnimating()
                            cell.constHeightForViewLink.constant = 250
                            let linkView = LPLinkView(metadata: self.metaDataDict[file_name]!)
                            cell.viewLink.addSubview(linkView)
                            
                            linkView.translatesAutoresizingMaskIntoConstraints = false
                            let horizontalConstraint = NSLayoutConstraint(item: linkView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.viewLink, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
                            let verticalConstraint = NSLayoutConstraint(item: linkView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.viewLink, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
                            let widthConstraint = NSLayoutConstraint(item: linkView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: SCREEN_WIDTH - 25)
                            let heightConstraint = NSLayoutConstraint(item: linkView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: cell.constHeightForViewLink.constant)
                            NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
                            
                            if (objModel.description ?? "").validURL {
                                cell.txtViewDesc.text = ""
                                cell.constTopForTxtViewDesc.constant = -25
                                cell.txtViewDesc.isHidden = true
                            }
                            else {
                                cell.constTopForTxtViewDesc.constant = 5
                                cell.txtViewDesc.isHidden = false
                            }
                        }
                        else {
                            cell.activityIndicator.stopAnimating()
                            cell.constHeightForViewLink.constant = 0
                        }
                    }
                    else {
                        cell.activityIndicator.stopAnimating()
                        cell.constHeightForViewLink.constant = 0
                    }
                }
                catch {
                }
            }
            
           // cell.contentView.layer.borderWidth = 5
            //cell.contentView.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
            cell.contentView.layer.cornerRadius = 10
            
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FansCornerCell", for: indexPath) as! FansCornerCell
            
            let fansCornerDetails = self.fansCornerList[indexPath.row]
            
            cell.lblTitle.text = fansCornerDetails[kTitle] as? String ?? ""
            
            if let user = fansCornerDetails["user"] as? [String: AnyObject] {
                cell.lblCreatedBy.text = "by \(user[kFullName] as? String ?? "")"
            }
            
            if let images = fansCornerDetails[kImages] as? [[String: AnyObject]], images.count > 0 {
                if images[0][kType] as? String == "Image" {
                    if let imgUrl = URL.init(string: images[0][kFiles] as? String ?? "") {
                        cell.imgBanner.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
                else {
                    if let imgUrl = URL.init(string: images[0]["thumb"] as? String ?? "") {
                        cell.imgBanner.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                    }
                }
            }
            
            let createdDate = self.convertStringToDate(format: "yyyy-MM-dd HH:mm:ss", strDate: fansCornerDetails["created_at"] as? String ?? "")
            cell.lblDate.text = self.convertDateToString(format: "dd MMM yyyy", date: createdDate)
            //cell.
           // cell.contentView.layer.borderWidth = 5
            //cell.contentView.layer.borderColor = UIColor(patternImage:UIImage(named:"borderCell")!).cgColor
            cell.contentView.layer.cornerRadius = 10
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.type == SUPERROYALSTYPE.FANWALL.rawValue {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FeedDetailsViewController") as! FeedDetailsViewController
            viewController.postID = self.feedList[indexPath.row].feed_id
            viewController.selectedIndex = indexPath.row
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FansCornerDetailsViewController") as! FansCornerDetailsViewController
            viewController.slug = self.fansCornerList[indexPath.row]["slug"] as? String ?? ""
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func btnViewMoreText_Clicked(sender: AnyObject) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: tblFeed)
        if let indexPath = tblFeed.indexPathForRow(at: buttonPosition) {
            self.feedList[indexPath.row].isReadMore = true
            self.tblFeed.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    @objc func btnUserProfile_Clicked(_ sender: UIButton)  {
    }
    
    @objc func btnTotalLikeUser_Clicked(_ sender: UIButton)  {
        let position = sender.convert(CGPoint.zero, to: tblFeed)
        if let indexPath = tblFeed.indexPathForRow(at: position) {
            if let intLikeCount = Int(self.feedList[indexPath.row].like_count ?? "0"), intLikeCount > 0 {
                let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FeedLikeViewController") as! FeedLikeViewController
                viewController.feedId = self.feedList[indexPath.row].feed_id ?? 0
                let sheetController = SheetViewController(controller: viewController, sizes: [.percent(0.80)], options: SheetOptions(useInlineMode: false))
                self.present(sheetController, animated: true, completion: nil)
            }
        }
    }
    
    @objc func btnLike_Clicked(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tblFeed)
        if let indexPath = tblFeed.indexPathForRow(at: position) {
            self.callLikeDislikePostAPI(indexVal: indexPath.row)
        }
    }
    
    @objc func btnBookmark_Clicked(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tblFeed)
        if let indexPath = tblFeed.indexPathForRow(at: position) {
            self.callBookmarkPostAPI(indexVal: indexPath.row)
        }
    }
    
    @objc func btnMore_Clicked(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tblFeed)
        if let indexPath = tblFeed.indexPathForRow(at: position) {
            if BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int == self.feedList[indexPath.row].appuser_id {
                guard let popVC = self.storyBoard.instantiateViewController(withIdentifier: "CustomMenuViewController") as? CustomMenuViewController else { return }
                popVC.modalPresentationStyle = .popover
                popVC.strFromVC = "FeedList"
                popVC.selectedIndex = indexPath.row
                popVC.delegateCustomMenu = self
                popVC.menuList = [self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete")]
                popVC.popoverPresentationController?.dimmingView?.backgroundColor = UIColor(hex: "ECE8E5").withAlphaComponent(0.27)
                popVC.preferredContentSize = CGSize(width: Int(VERTICALMENU_SIZE.WIDTH.rawValue), height: Int(VERTICALMENU_SIZE.HEIGHT.rawValue) * popVC.menuList.count)
                
                let popOverVC = popVC.popoverPresentationController
                popOverVC?.delegate = self
                popOverVC?.sourceView = sender
                popOverVC?.sourceRect = sender.bounds
                popOverVC?.permittedArrowDirections = .up
                self.present(popVC, animated: true)
            }
            else {
                guard let popVC = self.storyBoard.instantiateViewController(withIdentifier: "CustomMenuViewController") as? CustomMenuViewController else { return }
                popVC.modalPresentationStyle = .popover
                popVC.strFromVC = "FeedList"
                popVC.selectedIndex = indexPath.row
                popVC.delegateCustomMenu = self
                popVC.menuList = [self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportPost")]
                popVC.popoverPresentationController?.dimmingView?.backgroundColor = UIColor(hex: "ECE8E5").withAlphaComponent(0.27)
                popVC.preferredContentSize = CGSize(width: Int(VERTICALMENU_SIZE.WIDTH.rawValue), height: Int(VERTICALMENU_SIZE.HEIGHT.rawValue) * popVC.menuList.count)
                
                let popOverVC = popVC.popoverPresentationController
                popOverVC?.delegate = self
                popOverVC?.sourceView = sender
                popOverVC?.sourceRect = sender.bounds
                popOverVC?.permittedArrowDirections = .up
                self.present(popVC, animated: true)
            }
        }
    }
}

extension SuperRoyalsViewController : UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension SuperRoyalsViewController: CustomMenuDelegate {
    func editDeleteReportFeedData(strAction: String, selIndex: Int, comment: String) {
        if strAction == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete") {
            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "SuperRoyals_DeletePost"), preferredStyle: .alert)
            alert.view.tintColor = UIColor().alertButtonColor
            let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Yes"), style: .default, handler: { (action) in
                self.callDeletePostAPI(indexVal: selIndex)
            })
            let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_No"), style: .default, handler: { (action) in
            })
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        }
        else if strAction == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportPost") {
            self.callAddPostReportAPI(indexVal: selIndex, comment: comment)
        }
    }
}

extension SuperRoyalsViewController: EditDeleteFeedObjectDelegate {
    func editDeleteFeedObject(isDelete: Bool, selIndex: Int, objFeedModel: FeedListModel?) {
        if isDelete {
            self.feedList.remove(at: selIndex)
            self.tblFeed.deleteRows(at: [IndexPath.init(item: selIndex, section: 0)], with: .right)
        }
        else {
            if var model = objFeedModel {
                model.isReadMore = self.feedList[selIndex].isReadMore
                model.collectionViewHeight = self.feedList[selIndex].collectionViewHeight
                self.feedList[selIndex] = model
                self.tblFeed.reloadRows(at: [IndexPath(row: selIndex, section: 0)], with: .none)
            }
        }
    }
}

//Audio Finish Observer
extension FeedTextMediaCell {
    @objc func finishedPlaying( _ myNotification: NSNotification) {
        if self.playedAudioIndexPath != nil {
            if let cell = self.cvImageVideo.cellForItem(at: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                cell.btnPlay.isSelected = false
                cell.slider.value = 0.0
                cell.slider.setNeedsLayout()
                cell.slider.layoutIfNeeded()
            }
            else {
                if let cell = self.cvImageVideo.dataSource?.collectionView(self.cvImageVideo, cellForItemAt: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                    cell.btnPlay.isSelected = false
                    cell.slider.value = 0.0
                    cell.slider.setNeedsLayout()
                    cell.slider.layoutIfNeeded()
                }
            }
            self.playedAudioIndexPath = nil
        }
        
        if self.player != nil {
            if self.timeObserver != nil {
                self.player!.removeTimeObserver(self.timeObserver!)
                self.timeObserver = nil
            }
            
            let targetTime: CMTime = CMTimeMake(value: 0, timescale: 1)
            self.player!.seek(to: targetTime)
            self.player = nil
        }
    }
}

class FeedTextMediaCell : UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UITextViewDelegate {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var btnUserProfile: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTotalLikes: UILabel!
    @IBOutlet weak var btnTotalLikeUser: UIButton!
    @IBOutlet weak var lblTotalComments: UILabel!
    @IBOutlet weak var txtViewDesc: UITextView!
    @IBOutlet weak var btnViewMoreText: UIButton!
    @IBOutlet weak var viewLink: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var cvImageVideo: UICollectionView!
    @IBOutlet weak var pageControlView: CHIPageControlJaloro!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnBookmark: UIButton!
    
    @IBOutlet weak var constTopForBtnViewMoreText: NSLayoutConstraint!
    @IBOutlet weak var constTopForTxtViewDesc: NSLayoutConstraint!
    @IBOutlet weak var constHeightForViewLink: NSLayoutConstraint!
    @IBOutlet weak var constHeightForCVImageVideo: NSLayoutConstraint!
    
    var playedAudioIndexPath: IndexPath!
    var player: AVPlayer?
    var timeObserver: Any?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtViewDesc.delegate = self
    }
    
    var arrDocument = [FeedImages]()
    var currentPageForPageControlView = 0
    
    func setCollectionViewWith(objModel: FeedListModel) {
        self.pageControlView.radius = 4
        self.pageControlView.tintColor = UIColor().themeDarkPinkColor.withAlphaComponent(0.5)
        self.pageControlView.currentPageTintColor = UIColor().themeDarkPinkColor
        self.pageControlView.padding = 6
        self.pageControlView.progress = 0.5
        self.pageControlView.set(progress: 0, animated: true)
        self.pageControlView.numberOfPages = objModel.images!.count
        
        if self.pageControlView.numberOfPages == 0 ||  self.pageControlView.numberOfPages == 1 {
            self.pageControlView.isHidden = true
        }
        else {
            self.pageControlView.isHidden = false
        }
        
        self.arrDocument.removeAll()
        if let documents = objModel.images {
            self.arrDocument = documents
        }
        self.cvImageVideo.isPagingEnabled = true
        self.cvImageVideo.dataSource = self
        self.cvImageVideo.delegate = self
        self.cvImageVideo.setContentOffset(CGPoint.zero, animated: true)
        
        DispatchQueue.main.async {
            self.constHeightForCVImageVideo.constant = objModel.collectionViewHeight
            self.cvImageVideo.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.cvImageVideo.bounds.width, height: self.constHeightForCVImageVideo.constant)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrDocument.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.playedAudioIndexPath != nil {
            if let cell = self.cvImageVideo.cellForItem(at: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                cell.btnPlay.isSelected = false
                cell.slider.value = 0.0
                cell.slider.setNeedsLayout()
                cell.slider.layoutIfNeeded()
            }
            else {
                if let cell = self.cvImageVideo.dataSource?.collectionView(self.cvImageVideo, cellForItemAt: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                    cell.btnPlay.isSelected = false
                    cell.slider.value = 0.0
                    cell.slider.setNeedsLayout()
                    cell.slider.layoutIfNeeded()
                }
            }
            self.playedAudioIndexPath = nil
        }
        
        if self.player != nil {
            if self.timeObserver != nil {
                self.player!.removeTimeObserver(self.timeObserver!)
                self.timeObserver = nil
            }
            
            let targetTime: CMTime = CMTimeMake(value: 0, timescale: 1)
            self.player!.seek(to: targetTime)
            self.player = nil
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.arrDocument.count > indexPath.item {
            let objModel = self.arrDocument[indexPath.item]
            if objModel.type == FEEDTYPE.AUDIO.rawValue {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedMultiAudioCell", for: indexPath) as! FeedMultiAudioCell
                
                cell.btnPlay.addTarget(self, action: #selector(btnPlayAudio_Clicked(sender:)), for: .touchUpInside)
                
                return cell
            }
            else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedMultiImagesCell", for: indexPath) as! FeedMultiImagesCell
                cell.imgViewPostGIF.tag += 1
                       
                cell.imgViewPost.image = #imageLiteral(resourceName: "placeholder")
                cell.imgViewPostGIF.image = #imageLiteral(resourceName: "placeholder")
                cell.imgViewPost.isHidden = true
                cell.imgViewPostGIF.isHidden = true
                   
                if objModel.type == FEEDTYPE.GIF.rawValue {
                    cell.btnPlay.isHidden = true
                    cell.viewGIF.isHidden = false
                       
                    cell.imgViewPost.isHidden = true
                    cell.imgViewPostGIF.isHidden = false
                    cell.imgViewPostGIF.clear()
                       
                    if let fileName = objModel.file {
                        DispatchQueue.main.async {
                            if let image = try? UIImage(imageName: fileName) {
                                cell.imgViewPostGIF.setImage(image, manager: gifManager, loopCount: -1)
                            }
                            else if let url = URL.init(string: fileName) {
                                let loader = UIActivityIndicatorView.init(style: .medium)
                                cell.imgViewPostGIF.setGifFromURL(url, customLoader: loader)
                            }
                            else {
                                cell.imgViewPostGIF.clear()
                            }
                        }
                    }
                }
                else if objModel.type == FEEDTYPE.IMAGE.rawValue {
                    cell.btnPlay.isHidden = true
                    cell.viewGIF.isHidden = true
                       
                    cell.imgViewPost.isHidden = false
                    cell.imgViewPostGIF.isHidden = true
                       
                    if let url = objModel.file {
                        if let imgUrl = URL.init(string: url) {
                            DispatchQueue.main.async {
                                cell.imgViewPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
                            }
                        }
                    }
                }
                else if objModel.type == FEEDTYPE.VIDEO.rawValue {
                    cell.btnPlay.isHidden = false
                    cell.viewGIF.isHidden = true
                       
                    cell.imgViewPost.isHidden = false
                    cell.imgViewPostGIF.isHidden = true
                       
                    if let url = objModel.thumb {
                        if let imgUrl = URL.init(string: url) {
                            DispatchQueue.main.async {
                                cell.imgViewPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
                            }
                        }
                    }
                    cell.btnPlay.addTarget(self, action: #selector(btnPlayVideo_clicked), for: .touchUpInside)
                }
                return cell
            }
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedMultiImagesCell", for: indexPath) as! FeedMultiImagesCell
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objModel = self.arrDocument[indexPath.item]
        if objModel.type == FEEDTYPE.IMAGE.rawValue || objModel.type == FEEDTYPE.GIF.rawValue {
            if arrDocument.count > 0 {
                var dict = [String:Any]()
                dict["multiplePhoto"] = self.arrDocument
                dict["visibleIndex"] = self.currentPageForPageControlView
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OpenPhotoVCFromCell"), object: nil, userInfo: dict)
            }
            else {
                var dict = [String:Any]()
                dict["singlePhoto"] = objModel
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OpenPhotoVCFromCell"), object: nil, userInfo: dict)
            }
        }
        else if objModel.type == FEEDTYPE.VIDEO.rawValue {
            var dict = [String:Any]()
            dict["dictVideo"] = objModel.file
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playVideoFromCell"), object: nil, userInfo: dict)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.width
        self.currentPageForPageControlView = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        self.pageControlView.set(progress: self.currentPageForPageControlView, animated: true)
    }
    
    @objc func btnPlayVideo_clicked(sender : UIButton) {
        var dict = [String:Any]()
        dict["dictVideo"] = self.arrDocument[self.currentPageForPageControlView].file
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playVideoFromCell"), object: nil, userInfo: dict)
    }
    
    @objc func btnPlayAudio_Clicked(sender: AnyObject) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: self.cvImageVideo)
           if let indexPath = self.cvImageVideo.indexPathForItem(at: buttonPosition) {
               if let cell = self.cvImageVideo.cellForItem(at: indexPath) as? FeedMultiAudioCell {
                let chatDetails = self.arrDocument[indexPath.item]
                   
                   if self.playedAudioIndexPath == nil {
                       if self.timeObserver != nil {
                           print("removeTimeObserver")
                           if self.player != nil {
                               self.player!.removeTimeObserver(self.timeObserver!)
                               self.timeObserver = nil
                           }
                       }
                       else {
                           print("timeObserver nil")
                       }
                       
                       self.playedAudioIndexPath = indexPath
                    if let content = chatDetails.file {
                           self.playAudio(content: content, cell: cell)
                       }
                   }
                   else {
                       if self.playedAudioIndexPath != indexPath {
                           if self.player != nil {
                               if let previousPlayCell = self.cvImageVideo.cellForItem(at: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                                   self.player!.pause()
                                   previousPlayCell.slider.value = 0.0
                                   previousPlayCell.slider.setNeedsLayout()
                                   previousPlayCell.slider.layoutIfNeeded()
                                   previousPlayCell.btnPlay.isSelected = false
                                   previousPlayCell.activityIndicator.stopAnimating()
                                   
                                   if self.timeObserver != nil {
                                       print("removeTimeObserver")
                                       self.player!.removeTimeObserver(self.timeObserver!)
                                       self.timeObserver = nil
                                   }
                                   else {
                                       print("timeObserver nil")
                                   }
                                   
                                   self.playedAudioIndexPath = indexPath
                                   if let content = chatDetails.file {
                                       self.playAudio(content: content, cell: cell)
                                   }
                               }
                               else {
                                   if let previousPlayCell = self.cvImageVideo.dataSource?.collectionView(self.cvImageVideo, cellForItemAt: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                                       self.player!.pause()
                                       previousPlayCell.slider.value = 0.0
                                       previousPlayCell.slider.setNeedsLayout()
                                       previousPlayCell.slider.layoutIfNeeded()
                                       previousPlayCell.btnPlay.isSelected = false
                                       previousPlayCell.activityIndicator.stopAnimating()
                                       
                                       if self.timeObserver != nil {
                                           print("removeTimeObserver")
                                           self.player!.removeTimeObserver(self.timeObserver!)
                                           self.timeObserver = nil
                                       }
                                       else {
                                           print("timeObserver nil")
                                       }
                                       
                                       self.playedAudioIndexPath = indexPath
                                       if let content = chatDetails.file {
                                           self.playAudio(content: content, cell: cell)
                                       }
                                   }
                               }
                           }
                       }
                       else {
                           if self.player != nil && self.player!.rate == 1 {
                               self.player!.pause()
                               cell.btnPlay.isSelected = false
                           }
                           else {
                               self.player!.play()
                               cell.btnPlay.isSelected = true
                           }
                       }
                   }
               }
           }
       }
       
    func playAudio(content: String, cell: FeedMultiAudioCell) {
        guard let url = URL.init(string: content) else {
            print("Invalid URL")
            return
        }
        let playerItem: AVPlayerItem = AVPlayerItem(url: url)
        self.player = AVPlayer(playerItem: playerItem)
        
        cell.slider.isUserInteractionEnabled = false
        cell.slider.minimumValue = 0.0
        
        let duration: CMTime = playerItem.asset.duration
        let seconds: Float64 = CMTimeGetSeconds(duration)
        cell.slider.maximumValue = Float(seconds)
        cell.slider.value = 0.0
        cell.slider.isContinuous = true
        DispatchQueue.main.async {
            cell.activityIndicator.startAnimating()
        }
        
        if self.player != nil {
            self.timeObserver = self.player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
                if self.player == nil {
                    return
                }
            if let previousPlayCell = self.cvImageVideo.cellForItem(at: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                    if self.player!.currentItem?.status == .readyToPlay {
                        let time: Float64 = CMTimeGetSeconds(self.player!.currentTime())
                        print("time: \(time)")
                        previousPlayCell.slider.value = Float(time)
                        previousPlayCell.slider.setNeedsLayout()
                        previousPlayCell.slider.layoutIfNeeded()
                    }
                    let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
                    if playbackLikelyToKeepUp == false {
                        print("IsBuffering")
                        previousPlayCell.activityIndicator.startAnimating()
                    }
                    else {
                        print("Buffering completed")
                        previousPlayCell.activityIndicator.stopAnimating()
                    }
                }
                else {
                    if let previousPlayCell = self.cvImageVideo.dataSource?.collectionView(self.cvImageVideo, cellForItemAt: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                        if self.player!.currentItem?.status == .readyToPlay {
                            let time: Float64 = CMTimeGetSeconds(self.player!.currentTime())
                            print("time1: \(time)")
                            previousPlayCell.slider.value = Float(time)
                            previousPlayCell.slider.setNeedsLayout()
                            previousPlayCell.slider.layoutIfNeeded()
                        }
                        let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
                        if playbackLikelyToKeepUp == false {
                            print("IsBuffering")
                            previousPlayCell.activityIndicator.startAnimating()
                        }
                        else {
                            print("Buffering completed")
                            previousPlayCell.activityIndicator.stopAnimating()
                        }
                    }
                }
            }
            
            if self.player!.rate == 0 {
                self.player!.play()
                cell.btnPlay.isSelected = true
            }
            else {
                self.player!.pause()
                cell.btnPlay.isSelected = false
            }
        }
        else {
            cell.activityIndicator.stopAnimating()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.finishedPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
}

class FeedMultiImagesCell : UICollectionViewCell {
    @IBOutlet weak var imgViewPost: UIImageView!
    @IBOutlet weak var imgViewPostGIF: UIImageView!
    @IBOutlet weak var btnPlay: CustomButton!
    @IBOutlet weak var viewGIF: UIView!
}

class FeedMultiAudioCell : UICollectionViewCell {
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}

class SuperRoyalsCell: UICollectionViewCell {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
}

class FansCornerCell: UITableViewCell {
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblDate: UILabel!
}

extension SuperRoyalsViewController {
    func getSuperRoyalsData() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(isShowHud: false, URL: baseURLWithoutAuth, apiName: APINAME.GETSUPERROYALSHOMESCREEN.rawValue, method: .post, parameters: [:]) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let data = responseData[kData] as? [String : AnyObject] {
                        if let superRoyals = data["super_royal_video"] as? [[String : AnyObject]] {
                            self.videoList = superRoyals
                        }
                        
                        if let superRoyal = data["super_royal"] as? [[String : AnyObject]] {
                            self.superRoyalsList = superRoyal
                        }
                        
                        if let superRoyalFocus = data["super_royal_focus"] as? [[String : AnyObject]], superRoyalFocus.count > 0 {
                            if let imageUrl = superRoyalFocus[0][kImage] as? String {
                                if let url = URL(string: imageUrl) {
                                    self.imgBanner.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                                }
                                self.superRoyalId = superRoyalFocus[0]["super_royal_id"] as? Int ?? 0
                                self.superRoyalName = superRoyalFocus[0][kName] as? String ?? ""
                            }
                            self.bannerUrl = superRoyalFocus[0][kUrl] as? String ?? ""
                        }

                        self.videoCV.reloadData()
                        self.superRoyalsCV.reloadData()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getFeedList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "Feed[feed_id]": self.lastFeedId,
                         "Feed[is_platform]" : "FanApp"]
            
            APIManager().apiCall(of: FeedListModel.self, isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.GETFEEDLIST.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    if let response = response {
                        if self.lastFeedId == "" {
                            self.feedList.removeAll()
                            self.tblFeed.setContentOffset(.zero, animated: true)
                        }
                        
                        if let arrayData = response.arrayData {
                            for data in arrayData {
                                self.feedList.append(data)
                            }
                        }
                        self.tblFeed.reloadData()
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.02) {
                            self.storeLinkPreivewAndCVHeight()
                        }
                    }
                }
                else {
                    if error?.code == kFailureCancelCode {
                        return
                    }
                    else if error?.code == kRecordNotFound {
                        
                    }
                    else {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: error?.domain ?? ErrorMessage)
                    }
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getFansCornerList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kPage: self.currentPage, kPageSize: PAGESIZE] as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.GETFANCORNER.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.fansCornerList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let dataList = responseData[kData] as? [[String : AnyObject]] {
                        for data in dataList {
                            self.fansCornerList.append(data)
                        }
                    }
                    
                    self.tblFeed.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callLikeDislikePostAPI(indexVal: Int) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feedlike[feed_id]" : self.feedList[indexVal].feed_id ?? 0] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.LIKEDISLIKEPOST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        let counterForLikes = Int(self.feedList[indexVal].like_count ?? "0")
                        if self.feedList[indexVal].is_like == kYes {
                            self.feedList[indexVal].is_like = kNo
                            self.feedList[indexVal].like_count = "\(counterForLikes! - 1)"
                        }
                        else {
                            self.feedList[indexVal].is_like = kYes
                            self.feedList[indexVal].like_count = "\(counterForLikes! + 1)"
                        }
                        
                        let objModel = self.feedList[indexVal]
                        
                        if let cell = self.tblFeed.cellForRow(at: IndexPath.init(row: indexVal, section: 0)) as? FeedTextMediaCell {
                            if objModel.is_like == kYes {
                                cell.btnLike.setImage(UIImage(named: "heart"), for: .normal)
                            }
                            else {
                                cell.btnLike.setImage(UIImage(named: "heart-empty"), for: .normal)
                            }
                            cell.lblTotalLikes.text = "\(objModel.like_count ?? "0") \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes"))"
                        }
                        
                        let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "postID": "\(objModel.feed_id ?? 0)", "eventType": POINTSEVENTTYPE.FEED_LIKE.rawValue]
                        self.callEarnPointsAPI(param: param)
                    }
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callBookmarkPostAPI(indexVal: Int) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Bookmarkfeed[feed_id]" : self.feedList[indexVal].feed_id ?? 0] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.BOOKMARKPOST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        if self.feedList[indexVal].is_bookmark == kYes {
                            self.feedList[indexVal].is_bookmark = kNo
                        }
                        else {
                            self.feedList[indexVal].is_bookmark = kYes
                        }
                        
                        let objModel = self.feedList[indexVal]
                        
                        if let cell = self.tblFeed.cellForRow(at: IndexPath.init(row: indexVal, section: 0)) as? FeedTextMediaCell {
                            if objModel.is_bookmark == kYes {
                                cell.btnBookmark.setImage(UIImage(named: "bookmark-fill"), for: .normal)
                            }
                            else {
                                cell.btnBookmark.setImage(UIImage(named: "Bookmark"), for: .normal)
                            }
                        }
                    }
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callDeletePostAPI(indexVal: Int) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feed[feed_id]" : self.feedList[indexVal].feed_id ?? 0] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.DELETEPOST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        self.feedList.remove(at: indexVal)
                        self.tblFeed.deleteRows(at: [IndexPath.init(row: indexVal, section: 0)], with: .right)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callAddPostReportAPI(indexVal: Int, comment: String) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Reportfeed[feed_id]" : self.feedList[indexVal].feed_id ?? 0,
                     "Reportfeed[reson]": comment] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.ADDPOSTREPORT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if let response = response {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: response.message ?? "")
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    //Store Link Preview Data and Collection Height
    func storeLinkPreivewAndCVHeight() {
        for i in 0..<self.feedList.count {
            var objModel = self.feedList[i]
            if let images = objModel.images, images.count > 0 {
                if objModel.getType() == FEEDTYPE.IMAGE.rawValue || objModel.getType() == FEEDTYPE.VIDEO.rawValue || objModel.getType() == FEEDTYPE.GIF.rawValue {
                    let imageHeightStr = images[0].height ?? ""
                    let imageWidthStr = images[0].width ?? ""
                    if imageHeightStr.isEmpty || imageWidthStr.isEmpty {
                        return
                    }

                    if let imageHeight = Double.init(imageHeightStr) {
                        if let imageWidth = Double.init(imageWidthStr) {
                            var height = (imageHeight * Double(SCREEN_WIDTH) - 25) / imageWidth
                            if height > 450 {
                                height = 450
                            }
                            
                            objModel.collectionViewHeight = CGFloat(height)
                            self.feedList[i] = objModel
                            DispatchQueue.main.async {
                                if self.type == SUPERROYALSTYPE.FANWALL.rawValue {
                                    self.tblFeed.reloadRows(at: [IndexPath(row: i, section: 0)], with: .none)
                                }
                            }
                        }
                    }
                }
                else if objModel.getType() == FEEDTYPE.AUDIO.rawValue {
                    objModel.collectionViewHeight = 200
                    self.feedList[i] = objModel
                    DispatchQueue.main.async {
                        if self.type == SUPERROYALSTYPE.FANWALL.rawValue {
                            self.tblFeed.reloadRows(at: [IndexPath(row: i, section: 0)], with: .none)
                        }
                    }
                }
            }
            else {
                do {
                    let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                    let range = NSRange(objModel.description!.startIndex..<objModel.description!.endIndex, in: objModel.description!)
                    if let firstMatch = detector.firstMatch(in: objModel.description!, options: [], range: range) {
                        var url = firstMatch.url!
                        if url.scheme == "http" {
                            if var urlComps = URLComponents(url: url, resolvingAgainstBaseURL: false) {
                                urlComps.scheme = "https"
                                url = urlComps.url!
                            }
                        }
                        let file_name = "\(url)"
                        if !self.metaDataDict.keys.contains(file_name) {
                            let metadataProvider = LPMetadataProvider()
                            metadataProvider.startFetchingMetadata(for: URL(string: file_name)!) { (metadata, error) in
                                guard let data = metadata, error == nil else {
                                    return
                                }
                                self.metaDataDict[file_name] = data
                                
                                for j in 0..<self.feedList.count {
                                    let objModelInner = self.feedList[j]
                                    do {
                                        let detector1 = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                                        let range1 = NSRange(objModelInner.description!.startIndex..<objModelInner.description!.endIndex, in: objModelInner.description!)
                                        if let firstMatch1 = detector1.firstMatch(in: objModelInner.description!, options: [], range: range1) {
                                            var url1 = firstMatch1.url!
                                            if url1.scheme == "http" {
                                                if var urlComps1 = URLComponents(url: url1, resolvingAgainstBaseURL: false) {
                                                    urlComps1.scheme = "https"
                                                    url1 = urlComps1.url!
                                                }
                                            }
                                            let file_nameInner = "\(url1)"
                                            if file_name == file_nameInner {
                                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
                                                    if self.type == SUPERROYALSTYPE.FANWALL.rawValue {
                                                        self.tblFeed.reloadRows(at: [IndexPath(row: j, section: 0)], with: .none)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch {
                                    }
                                }
                            }
                        }
                    }
                }
                catch {
                }
            }
        }
    }
}


extension UISegmentedControl {

    func removeBorder(){

        self.tintColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        self.setTitleTextAttributes( [NSAttributedString.Key.foregroundColor : UIColor.systemPink], for: .selected)
        self.setTitleTextAttributes( [NSAttributedString.Key.foregroundColor : UIColor.gray], for: .normal)
        if #available(iOS 13.0, *) {
            self.selectedSegmentTintColor = UIColor.clear
        }

    }

    func setupSegment() {
        self.removeBorder()
        let segmentUnderlineWidth: CGFloat = self.bounds.width
        let segmentUnderlineHeight: CGFloat = 2.0
        let segmentUnderlineXPosition = self.bounds.minX
        let segmentUnderLineYPosition = self.bounds.size.height - 1.0
        let segmentUnderlineFrame = CGRect(x: segmentUnderlineXPosition, y: segmentUnderLineYPosition, width: segmentUnderlineWidth, height: segmentUnderlineHeight)
        let segmentUnderline = UIView(frame: segmentUnderlineFrame)
        segmentUnderline.backgroundColor = UIColor.clear

        self.addSubview(segmentUnderline)
        self.addUnderlineForSelectedSegment()
    }

    func addUnderlineForSelectedSegment(){

        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor.systemPink
        underline.tag = 1
        self.addSubview(underline)


    }

    func changeUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        underline.frame.origin.x = underlineFinalXPosition

    }
}
