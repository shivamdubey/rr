//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit

class NewGamesViewController: BaseViewController, TabItem {
    
    @IBOutlet weak var imgHallbol: UIImageView!
    @IBOutlet weak var tblGames: UITableView!

    var tabImage: UIImage? {
        return UIImage(named: "tab_game")
    }
    
    var quizList = [[String: AnyObject]]()
    var languages = ["Predi", "Housie", "Quizzerr"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "GameScreen")
        
        self.tblGames.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
        
        
        //self.getQuizBanner()
    }
}

extension NewGamesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count//quizList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell") as! GameCell
        
//        if let imageUrl = quizList[indexPath.item][kImage] as? String {
//            if let url = URL(string: imageUrl) {
//                cell.imgGame.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
//            }
//        }
        var imageOfData: UIImage = UIImage(named:languages[indexPath.row])!
        cell.imgGame.setImage(imageOfData)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let redirectScreen = quizList[indexPath.row][kRedirect_screen_ios] as? String, !redirectScreen.isEmpty {
//            if let viewController = self.storyBoard.instantiateViewController(withIdentifier: redirectScreen) as? UIViewController {
//                self.navigationController?.pushViewController(viewController, animated: true)
//            }
//        }
        if indexPath.row == 0 {
            if let viewController = self.storyBoard.instantiateViewController(withIdentifier: "PredictionMatchListViewController") as? UIViewController {
           self.navigationController?.pushViewController(viewController, animated: true)
            }
           
        }
        else if indexPath.row == 1{
            if let viewController = self.storyBoard.instantiateViewController(withIdentifier: "HousieHomeViewController") as? UIViewController {
           self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        else if indexPath.row == 2{
            if let viewController = self.storyBoard.instantiateViewController(withIdentifier: "QuizHomeViewController") as? UIViewController {
           self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
}

class GameCell: UITableViewCell {
    @IBOutlet weak var imgGame: UIImageView!
}

extension NewGamesViewController {
    func getQuizBanner() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey]
            
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETQUIZBANNER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if let list = responseData[kData] as? [[String : AnyObject]] {
                        self.quizList = list
                    }
                    
                    self.tblGames.reloadData()
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
