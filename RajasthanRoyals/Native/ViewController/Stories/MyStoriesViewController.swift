//
//  MyStoriesViewController.swift
//  RoyalFoundation
//
//  Created by Chirag on 01/01/21.
//  Copyright © 2021 hardik nandha. All rights reserved.
//

import UIKit

class MyStoriesViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var CVMyStory: UICollectionView!
    @IBOutlet weak var lblEmptyMessage: UILabel!
    
    var currentPage = 1
    var nextPage = "N"
    var storyList = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Story_btn_MyStories")
        
        self.getMyStoryListDate(isShowHud: true)
    }
}

extension MyStoriesViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storyList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == storyList.count && nextPage == kY {
            currentPage = currentPage + 1
            self.getMyStoryListDate(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyStory", for: indexPath) as! MyStory
        
        if storyList[indexPath.row]["type"] as? String == "image" {
            if let imageUrl = storyList[indexPath.row]["files"] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgStory.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
        }
        else {
            if let imageUrl = storyList[indexPath.row]["thumbnail"] as? String {
                if let url = URL(string: imageUrl) {
                    cell.imgStory.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
                }
            }
        }
        
        if let isStates = storyList[indexPath.row]["is_approve"] as? String {
            cell.lblState.textColor = UIColor.white
            cell.lblState.text = isStates
        }
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressCell(tapGesture:)))
        cell.bgView.addGestureRecognizer(longGesture)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        BaseViewController.sharedInstance.appDelegate.storyList = [self.storyList[indexPath.row]]
        let userDetails = ["appuser_id": BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int ?? 0, "first_name": BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String ?? "", "last_name": BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String ?? "", "user_image": BaseViewController.sharedInstance.appDelegate.userDetails[kImage] ?? ""] as [String : Any]
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "StoryViewController") as! StoryViewController
        viewController.userDetails = userDetails as [String: AnyObject]
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
    @objc func longPressCell(tapGesture: UIGestureRecognizer) {
        let position = tapGesture.location(in: CVMyStory)
        if let indexPath = CVMyStory.indexPathForItem(at: position) {
            if tapGesture.state == .began {
                let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "lbl_DeleteStory"), preferredStyle: .alert)
                alert.view.tintColor = UIColor().themeColor
                
                let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Yes"), style: .default, handler: { (action) in
                    if let storyId = self.storyList[indexPath.row]["foundation_story_id"] as? Int {
                        self.deleteMyStoryListDate(storyId: storyId, indexPath: indexPath)
                    }
                })
                
                let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_No"), style: .default, handler: { (action) in
                })
                alert.addAction(noAction)
                alert.addAction(yesAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

class MyStory: UICollectionViewCell {
    @IBOutlet weak var imgStory: UIImageView!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var bgView: CustomView!
}

extension MyStoriesViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 2.0
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
            let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
            let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
            let availableWidth = CVMyStory.bounds.width - sectionPadding - interitemPadding
            let widthPerItem = (availableWidth ) / itemsPerRow
            return CGSize(width: widthPerItem, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}

extension MyStoriesViewController {
    func getMyStoryListDate(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kPage: self.currentPage, kPageSize: PAGESIZE, "is_platform": "FanApp"] as [String : Any]
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.GETMYSTORY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.storyList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let storyData = responseData[kData] as? [[String : AnyObject]] {
                        for story in storyData {
                            self.storyList.append(story)
                        }
                    }
                    
                    self.CVMyStory.reloadData()
                    
                    self.lblEmptyMessage.text = responseData[kMessage] as? String ?? ""
                    if self.storyList.count > 0 {
                        self.lblEmptyMessage.isHidden = true
                    }
                    else {
                        self.lblEmptyMessage.isHidden = false
                    }
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func deleteMyStoryListDate(storyId: Int, indexPath: IndexPath) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "Foundationstory[foundation_story_id]": storyId] as [String : Any]
            APIManager().apiCall(isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.DELETESTORY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    self.storyList.remove(at: indexPath.row)
                    self.CVMyStory.reloadData()
                    
                    if self.storyList.count > 0 {
                        self.lblEmptyMessage.isHidden = true
                    }
                    else {
                        self.lblEmptyMessage.isHidden = false
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
