//
//  StoryViewController.swift
//  Ranicircle
//
//  Created by Apple on 30/10/20.
//  Copyright © 2020 YellowPanther. All rights reserved.
//

import UIKit
import AVKit

protocol StoryViewDelegate {
    func storyView(userId: Int, stories: [[String: AnyObject]])
}

class StoryViewController: BaseViewController {
    
    @IBOutlet weak var imgStoryBackground: UIImageView!
    @IBOutlet weak var imgStory: UIImageView!
    @IBOutlet weak var videoView: VideoPlay!
    @IBOutlet weak var previousView: UIView!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgUser: CustomImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgEye: UIImageView!
    @IBOutlet weak var lblCount: UILabel!
    
    var delegate: StoryViewDelegate!
    var segmentedProgressBar: SegmentedProgressBar!
    var userDetails = [String: AnyObject]()
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    var storyId = 0
    var timeObserver: Any!
    var isStartAnimation = false
    var isPressMoreButton = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imgClose = #imageLiteral(resourceName: "close_big_ic").imageWithColor(color: UIColor.white)
        self.btnClose.setImage(imgClose, for: .normal)
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
        }
        catch {
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.insertSubview(blurEffectView, aboveSubview: imgStoryBackground)
        
        segmentedProgressBar = SegmentedProgressBar.init(numberOfSegments: BaseViewController.sharedInstance.appDelegate.storyList.count, duration: STORYIMAGEDURATION)
        segmentedProgressBar.frame = CGRect(x: 10, y: UIApplication.shared.statusBarFrame.size.height - 10, width: self.view.frame.width - 20, height: 2)
        segmentedProgressBar.delegate = self
        segmentedProgressBar.topColor = UIColor.white
        segmentedProgressBar.bottomColor = UIColor.white.withAlphaComponent(0.25)
        segmentedProgressBar.padding = 2
        self.view.addSubview(segmentedProgressBar)
        
        self.setUserDetials()
        
        if BaseViewController.sharedInstance.appDelegate.storyList.count > 0 {
            if let createdDateStr = BaseViewController.sharedInstance.appDelegate.storyList[0]["created_at"] as? String, !createdDateStr.isEmpty {
                let createdDate = self.convertStringToDate(format: "yyyy-MM-dd HH:mm:ss", strDate: createdDateStr).toLocalTime()
                self.lblTime.text = Date().toLocalTime().offsetnew(from: createdDate)
            }
            
            if let fileName = BaseViewController.sharedInstance.appDelegate.storyList[0]["files"] as? String, !fileName.isEmpty {
                if let url = URL(string: fileName) {
                    if BaseViewController.sharedInstance.appDelegate.storyList[0]["type"] as? String == "image" {
                        self.imgStory.isHidden = false
                        self.videoView.isHidden = true
                        
                        self.imgStoryBackground.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil) { (image, error, type, url) in
                            if error == nil {
                                self.imgStory.image = image
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01, execute: {
                                    self.segmentedProgressBar.startAnimation()
                                })
                            }
                        }
                    }
                    else {
                        self.imgStory.isHidden = true
                        self.videoView.isHidden = false
                        
                        self.videoView.playVideoWithURL(url: url)
                        self.videoView.player.play()
                        
                        self.segmentedProgressBar.duration = self.getMediaDuration(url: url)
                        
                        self.addTimeObserverOnVideoPlayer()
                    }
                    
                    if self.userDetails["appuser_id"] as? Int != BaseViewController.sharedInstance.appDelegate.userDetails["appuser_id"] as? Int {
                        if BaseViewController.sharedInstance.appDelegate.storyList[0]["is_view"] as? String == kNo {
                            if let storyId = BaseViewController.sharedInstance.appDelegate.storyList[0]["foundation_story_id"] as? Int {
                                self.readStory(storyId: storyId, index: 0)
                            }
                        }
                    }
                    
                    self.lblCount.text = "\(BaseViewController.sharedInstance.appDelegate.storyList[0]["story_view_count"] as? String ?? "0")"
                }
            }
        }
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.15
        longPressGesture.delaysTouchesBegan = true
        self.view.addGestureRecognizer(longPressGesture)
        
        let previousTapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnPreviousView))
        previousTapGesture.numberOfTapsRequired = 1
        previousTapGesture.numberOfTouchesRequired = 1
        self.previousView.addGestureRecognizer(previousTapGesture)
        
        let nextTapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnNextView))
        nextTapGesture.numberOfTapsRequired = 1
        nextTapGesture.numberOfTouchesRequired = 1
        self.nextView.addGestureRecognizer(nextTapGesture)
        
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        self.view.addGestureRecognizer(panGesture)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .darkContent
        if self.videoView != nil && self.videoView.player != nil {
            self.videoView.player.pause()
            
            if self.timeObserver != nil {
                self.videoView.player.removeTimeObserver(self.timeObserver!)
                self.timeObserver = nil
            }
        }
    }
    
    //Set User Detials
    func setUserDetials() {
        if let url = self.userDetails["user_image"] as? String {
            if let imgUrl = URL.init(string: url) {
                self.imgUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
       
        self.lblUserName.text = "\(self.userDetails["first_name"] as? String ?? "") \(self.userDetails["last_name"]  as? String ?? "")"
    }
    
    func addTimeObserverOnVideoPlayer() {
        if self.timeObserver == nil {
            weak var weakSelf = self
            self.timeObserver = self.videoView.player.addPeriodicTimeObserver(forInterval: CMTime(value: 1, timescale: 1), queue: DispatchQueue.main) {
                (time: CMTime) -> Void in
                if let strongSelf = weakSelf {
                    if strongSelf.videoView == nil || strongSelf.videoView.player == nil {
                        return
                    }
                    let playbackLikelyToKeepUp = strongSelf.videoView.player.currentItem?.isPlaybackLikelyToKeepUp
                    if playbackLikelyToKeepUp == false {
                        print("IsBuffering")
                        if strongSelf.isStartAnimation {
                            if !strongSelf.segmentedProgressBar.isPaused {
                                strongSelf.segmentedProgressBar.isPaused = true
                            }
                        }
                    }
                    else {
                        print("Buffering completed")
                        if !strongSelf.isStartAnimation {
                            strongSelf.isStartAnimation = true
                            strongSelf.segmentedProgressBar.startAnimation()
                            strongSelf.segmentedProgressBar.isPaused = false
                        }
                        else {
                            if !self.isPressMoreButton {
                                if strongSelf.segmentedProgressBar.isPaused {
                                    strongSelf.segmentedProgressBar.isPaused = false
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc private func tappedOnPreviousView() {
        segmentedProgressBar.rewind()
    }
    
    @objc private func tappedOnNextView() {
        segmentedProgressBar.skip()
    }
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state == .began {
            self.segmentedProgressBar.isPaused = true
            if self.videoView.player != nil {
                self.videoView.player.pause()
            }
        }
        else if gestureReconizer.state == .ended {
            self.segmentedProgressBar.isPaused = false
            if self.videoView.player != nil {
                self.videoView.player.play()
            }
        }
    }
    
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)

        if sender.state == .began {
            initialTouchPoint = touchPoint
        }
        else if sender.state == .changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        }
        else if sender.state == .ended || sender.state == .cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                if self.delegate != nil {
                    self.delegate.storyView(userId: self.userDetails["appuser_id"] as? Int ?? 0, stories: BaseViewController.sharedInstance.appDelegate.storyList)
                }
                self.dismiss(animated: true, completion: nil)
            }
            else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    @IBAction func btnClose_Clicked(_ sender: Any) {
        if self.delegate != nil {
            self.delegate.storyView(userId: self.userDetails["appuser_id"] as? Int ?? 0, stories: BaseViewController.sharedInstance.appDelegate.storyList)
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension StoryViewController: SegmentedProgressBarDelegate {
    func segmentedProgressBarChangedIndex(index: Int) {
        if self.timeObserver != nil {
            self.videoView.player.removeTimeObserver(self.timeObserver!)
            self.timeObserver = nil
        }
        self.segmentedProgressBar.isPaused = true
        
        if BaseViewController.sharedInstance.appDelegate.storyList.count > index {
            if let createdDateStr = BaseViewController.sharedInstance.appDelegate.storyList[index]["created_at"] as? String, !createdDateStr.isEmpty {
                let createdDate = self.convertStringToDate(format: "yyyy-MM-dd HH:mm:ss", strDate: createdDateStr)
                self.lblTime.text = Date().toLocalTime().offsetnew(from: createdDate)
            }
            
            if let fileName = BaseViewController.sharedInstance.appDelegate.storyList[index]["files"] as? String, !fileName.isEmpty {
                if let url = URL(string: fileName) {
                    if self.videoView.player != nil {
                        if self.videoView.isPlaying() {
                            self.videoView.player.pause()
                        }
                    }
                    
                    if BaseViewController.sharedInstance.appDelegate.storyList[index]["type"] as? String == "image" {
                        self.imgStory.isHidden = false
                        self.videoView.isHidden = true
                        
                        self.imgStoryBackground.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil) { (image, error, type, url) in
                            if error == nil {
                                self.imgStory.image = image
                                self.segmentedProgressBar.isPaused = false
                            }
                        }
                    }
                    else {
                        self.imgStory.isHidden = true
                        self.videoView.isHidden = false
                        
                        self.videoView.playVideoWithURL(url: url)
                        self.videoView.player.play()
                        
                        self.addTimeObserverOnVideoPlayer()
                    }
                    
                    if self.userDetails["appuser_id"] as? Int != BaseViewController.sharedInstance.appDelegate.userDetails["appuser_id"]  as? Int {
                        if BaseViewController.sharedInstance.appDelegate.storyList[index]["is_view"] as? String == kNo {
                            if let storyId = BaseViewController.sharedInstance.appDelegate.storyList[index]["foundation_story_id"] as? Int {
                                self.readStory(storyId: storyId, index: index)
                            }
                        }
                    }
                    
                    self.lblCount.text = "\(BaseViewController.sharedInstance.appDelegate.storyList[index]["story_view_count"] as? String ?? "0")"
                }
            }
        }
    }
    
    func segmentedProgressBarFinished() {
        print("Finished!")
        if self.delegate != nil {
            self.delegate.storyView(userId: self.userDetails["appuser_id"] as? Int ?? 0, stories: BaseViewController.sharedInstance.appDelegate.storyList)
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension StoryViewController {
    func readStory(storyId: Int, index: Int) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, "Foundationstoryview[foundation_story_id]": storyId] as [String : Any]
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: UserDetailsModel.self, isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.STORIEVIEW.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    BaseViewController.sharedInstance.appDelegate.storyList[index]["is_view"] = kYes as AnyObject
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension StoryViewController: CloseMyStoryViewsDelegate {
    func closeMyStoryViews() {
        self.segmentedProgressBar.isPaused = false
        if self.videoView.player != nil {
            self.videoView.player.play()
        }
    }
}

extension StoryViewController : UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
