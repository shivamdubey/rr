//
//  StoreTabViewController.swift
//  Unity-iPhone
//
//  Created by Developer1 on 3/28/22.
//

import Foundation
class StoreTabViewController: BaseViewController, TabItem {

    var tabImage: UIImage? {
        return UIImage(named: "store")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "IndianStore")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReloadStoryScreen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadStory), name: NSNotification.Name(rawValue: "ReloadStoryScreen"), object: nil)
        
        if let shopUrl = BaseViewController.sharedInstance.appDelegate.generalSettings["shop_url"] as? String {
            if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                let newShopUrl = "\(shopUrl)?c_email=\(email)"
                self.openUrlInSafariViewController(urlString: newShopUrl)
            }
        }
    }
    @objc func reloadStory() {
      //  self.currentPage = 1
       
    }
}


