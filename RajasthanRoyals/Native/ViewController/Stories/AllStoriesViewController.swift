//
//  AllStoriesViewController.swift
//  RoyalFoundation
//
//  Created by Chirag on 01/01/21.
//  Copyright © 2021 hardik nandha. All rights reserved.
//

import UIKit

class AllStoriesViewController: BaseViewController, TabItem {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnMyStory: CustomButton!
    @IBOutlet weak var CVAllStory: UICollectionView!
    @IBOutlet weak var lblEmptyMessage: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    
    var tabImage: UIImage? {
        return UIImage(named: "tab_story")
    }
    
    var isCreateStory = ""
    var currentPage = 1
    var nextPage = "N"
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.callTMAnalyticsAPI(category: "Page", action: "View", label: "Stories")
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "Story_lbl_Stories")
        self.btnMyStory.setTitle(self.getLocalizeTextForKey(keyName: "Story_btn_MyStories"), for: .normal)
        
        self.CVAllStory.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
        
        refreshControl = UIRefreshControl.init()
        refreshControl.tintColor = .white
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshCollectionView(sender:)), for: .valueChanged)
        CVAllStory.refreshControl = refreshControl
        
        self.getStoryListDate(isShowHud: true)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReloadStoryScreen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadStory), name: NSNotification.Name(rawValue: "ReloadStoryScreen"), object: nil)
    }
    
    //UIRefreshControl method
    @objc func refreshCollectionView(sender: AnyObject) {
        self.currentPage = 1
        self.getStoryListDate(isShowHud: false)
    }
    
    @objc func reloadStory() {
        self.currentPage = 1
        self.getStoryListDate(isShowHud: false)
    }
    
    @IBAction func btnMyStory_clicked(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(identifier: "MyStoriesViewController") as! MyStoriesViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnAdd_clicked(_ sender: Any) {
        let viewController = BaseViewController.sharedInstance.storyBoard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
        viewController.delegate = self
        let navigationViewController = UINavigationController(rootViewController: viewController)
        navigationViewController.isNavigationBarHidden = true
        navigationViewController.modalPresentationStyle = .overFullScreen
        self.present(navigationViewController, animated: true, completion: nil)
    }
}

extension AllStoriesViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return BaseViewController.sharedInstance.appDelegate.storiesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == BaseViewController.sharedInstance.appDelegate.storiesList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getStoryListDate(isShowHud: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllStory", for: indexPath) as! AllStory
        
        if let imageUrl = BaseViewController.sharedInstance.appDelegate.storiesList[indexPath.row]["user_image"] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }
        
        cell.whiteView.isHidden = (BaseViewController.sharedInstance.appDelegate.storiesList[indexPath.row]["is_verified"] as? String ?? "") == kYes ? false : true
        cell.imgVerify.isHidden = (BaseViewController.sharedInstance.appDelegate.storiesList[indexPath.row]["is_verified"] as? String ?? "") == kYes ? false : true
        
        if let story = BaseViewController.sharedInstance.appDelegate.storiesList[indexPath.row]["is_story"] as? [[String: AnyObject]], story.count > 0 {
            if story[0]["type"] as? String == "image" {
                if let imageUrl = story[0]["files"] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgStory.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                    }
                }
            }
            else {
                if let imageUrl = story[0]["thumbnail"] as? String {
                    if let url = URL(string: imageUrl) {
                        cell.imgStory.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"))
                    }
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let storyList = BaseViewController.sharedInstance.appDelegate.storiesList[indexPath.row]["is_story"] as? [[String: AnyObject]] {
            BaseViewController.sharedInstance.appDelegate.storyList = storyList
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "StoryViewController") as! StoryViewController
            viewController.delegate = self
            viewController.userDetails = BaseViewController.sharedInstance.appDelegate.storiesList[indexPath.row]
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
    }
}

extension AllStoriesViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 2.0
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
        let availableWidth = CVAllStory.bounds.width - sectionPadding - interitemPadding
        let widthPerItem = (availableWidth ) / itemsPerRow
        return CGSize(width: widthPerItem, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
}

class AllStory: UICollectionViewCell {
    @IBOutlet weak var imgStory: UIImageView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var imgVerify: UIImageView!
}

extension AllStoriesViewController {
    @objc func getStoryListDate(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey, kPage: self.currentPage, kPageSize: PAGESIZE, "is_platform": "FanApp", "is_app_story": kYes] as [String : Any]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.GETSTORY.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        BaseViewController.sharedInstance.appDelegate.storiesList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let storyData = responseData[kData] as? [[String : AnyObject]] {
                        for story in storyData {
                            BaseViewController.sharedInstance.appDelegate.storiesList.append(story)
                        }
                    }
                    
                    self.lblEmptyMessage.text = responseData[kMessage] as? String ?? ""
                    if BaseViewController.sharedInstance.appDelegate.storiesList.count > 0 {
                        self.lblEmptyMessage.isHidden = true
                    }
                    else {
                        self.lblEmptyMessage.isHidden = false
                    }
                    
                    self.CVAllStory.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension AllStoriesViewController: StoryViewDelegate {
    func storyView(userId: Int, stories: [[String: AnyObject]]) {
        let array = BaseViewController.sharedInstance.appDelegate.storiesList as Array
        let matchIndex = array.index {
            if $0["appuser_id"] as? Int == userId {
                return true
            }
            return false
        }
        
        if matchIndex != nil {
            if BaseViewController.sharedInstance.appDelegate.storiesList.count > matchIndex! {
                BaseViewController.sharedInstance.appDelegate.storiesList[matchIndex!]["is_story"] = stories as AnyObject
                if stories.count == 0 {
                    BaseViewController.sharedInstance.appDelegate.storiesList.remove(at: matchIndex!)
                    self.CVAllStory.reloadData()
                }
            }
        }
        else {
            self.currentPage = 1
            self.getStoryListDate(isShowHud: true)
        }
    }
}
