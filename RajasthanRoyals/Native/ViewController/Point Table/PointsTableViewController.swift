//
//  PointsTableViewController.swift
//  RR
//
//  Created by Sagar on 23/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//


import UIKit

class PointsTableViewController: BaseViewController {

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var btnMenu: CustomButton!
    @IBOutlet weak var tblPointsTable: UITableView!
    
    var currentPage = 1
    var nextPage = kN
    var teamList = [[String: AnyObject]]()
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addFirebaseCustomeEvent(eventName: "PointTableScreen")
        
        self.navTitle.text = self.getLocalizeTextForKey(keyName: "LeftMenu_lbl_PointsTable")
        
        refreshControl = UIRefreshControl.init()
        refreshControl.tintColor = .white
        refreshControl.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshTableView(sender:)), for: .valueChanged)
        tblPointsTable.refreshControl = refreshControl
        
        self.getTeamList(isShowHud: true)
    }
    
    //UIRefreshControl method
    @objc func refreshTableView(sender: AnyObject) {
        self.currentPage = 1
        self.nextPage = kN
        self.getTeamList(isShowHud: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension PointsTableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamList.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellIdentifier = "PointsTableHeaderCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PointsTableHeaderCell
        
        cell.lblTeam.text = self.getLocalizeTextForKey(keyName: "PointsTable_lbl_Team")
        cell.lblMatch.text = self.getLocalizeTextForKey(keyName: "PointsTable_lbl_M")
        cell.lblNR.text = self.getLocalizeTextForKey(keyName: "PointsTable_lbl_NR")
        cell.lblPoints.text = self.getLocalizeTextForKey(keyName: "PointsTable_lbl_P")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == teamList.count - 1 && nextPage == kY {
            currentPage = currentPage + 1
            self.getTeamList(isShowHud: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == teamList.count - 1 && nextPage == kY {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.clear
            
            let activityIndicator = UIActivityIndicatorView(style: .white)
            activityIndicator.color = UIColor().alertButtonColor
            activityIndicator.center = CGPoint(x: tableView.frame.size.width / 2, y: 20)
            
            cell.addSubview(activityIndicator)
            
            activityIndicator.startAnimating()
            
            return cell
        }
        
        let cellIdentifier = "PointsTableCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PointsTableCell
        cell.lblPosition.text = "\(indexPath.row + 1)"
        
        if let imageUrl = teamList[indexPath.row][kImage] as? String {
            if let url = URL(string: imageUrl) {
                cell.imgTeam.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "image_placeholder"))
            }
        }

        if let name = teamList[indexPath.row][kName] as? String {
            cell.lblTeam.text = name
        }
        
        if let matche = teamList[indexPath.row][kMatch] as? Int {
            cell.lblMatch.text = "\(matche)"
        }
        
        if let nrr = teamList[indexPath.row][kNRR] as? String {
            cell.lblNR.text = nrr
        }
        
        if let pts = teamList[indexPath.row][kPTS] as? Int {
            cell.lblPoints.text = "\(pts)"
        }
        
        cell.contentView.backgroundColor = UIColor.clear
        if let name = teamList[indexPath.row][kName] as? String {
            if name == "Rajasthan Royals" {
                cell.contentView.backgroundColor = UIColor(named: "Pink_Color")
            }
        }

        return cell
    }
}

class PointsTableHeaderCell: UITableViewCell {
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblMatch: UILabel!
    @IBOutlet weak var lblNR: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
}

class PointsTableCell: UITableViewCell {
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var imgTeam: UIImageView!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblMatch: UILabel!
    @IBOutlet weak var lblNR: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
}

extension PointsTableViewController {
    func getTeamList(isShowHud: Bool) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kPage: self.currentPage, kPageSize: PAGESIZE]
            
            APIManager().apiCall(isShowHud: isShowHud, URL: baseURLWithoutAuth, apiName: APINAME.GETTEAMLIST.rawValue, method: .post, parameters: param) { (response, error) in
                self.refreshControl.endRefreshing()
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    
                    if self.currentPage == 1 {
                        self.teamList.removeAll()
                    }
                    
                    if let nextPage = responseData[kIsNextPage] as? String {
                        self.nextPage = nextPage
                    }
                    
                    if let teamList = responseData[kData] as? [[String : AnyObject]] {
                        for team in teamList {
                            self.teamList.append(team)
                        }
                    }
                    
                    self.tblPointsTable.reloadData()
                }
                else {
                    if self.currentPage > 1 {
                        self.currentPage = self.currentPage - 1
                    }
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}
