//
//  CommentReplyViewMoreViewController.swift
//  RaniCircle
//
//  Created by Apple on 13/10/20.
//  Copyright © 2020 YellowPanther. All rights reserved.
//

import UIKit

protocol ReplaceFeedCommentObjectDelegate {
    func updateFeedCommentObject(selSection: Int, objFeedCommentUserModel: FeedCommentUserModel?)
}

class CommentReplyViewMoreViewController: BaseViewController {

    var delegate: ReplaceFeedCommentObjectDelegate?

    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var topCommentView: UIView!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var txtViewDesc: UITextView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnTopReply: UIButton!
    @IBOutlet weak var lblTotalLikes: UILabel!
    @IBOutlet weak var tblViewFeedCommentReply: UITableView!
    @IBOutlet weak var txtEnterMessage: GrowingTextView!
    @IBOutlet weak var btnReply: UIButton!
    
    @IBOutlet weak var constHeightForTxtEnterMessage: NSLayoutConstraint!
    
    var objFeedCommentModel: FeedCommentUserModel?
    var commentReplyList = [FeedCommentUserModel]()
    var currentPage = 1
    var nextPage = ""
    var selectedIndex = -1
    var selectedSection = -1
    var lastPostCommentId = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        self.txtEnterMessage.layer.borderWidth = 1
        self.txtEnterMessage.layer.borderColor = UIColor().themeDarkPinkColor.cgColor
        
        self.tblViewFeedCommentReply.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
                
        self.callGetPostCommentsReplyAPI(isShowHud: true)
        
        self.setComments()
    }
    
    func setupLocalizationText() {
        self.navItem.title = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Replies")
        self.btnTopReply.setTitle(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Reply"), for: .normal)
        self.btnReply.setTitle(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Reply"), for: .normal)
    }
    
    func setComments() {
        if let url = self.objFeedCommentModel?.user?.image {
            if let imgUrl = URL.init(string: url) {
                self.imgViewUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if let full_name = self.objFeedCommentModel?.user?.full_name {
            self.lblUserName.text = full_name
        }
        
        let createdDate = self.convertStringToDate(format: dateFormateForGet, strDate: self.objFeedCommentModel!.created_at!)
        self.lblTime.text = self.convertDateToString(format: dateFormateForDisplay, date: createdDate)
        
        self.txtViewDesc.text = self.objFeedCommentModel?.comment
        
        self.lblTotalLikes.text = "\(self.objFeedCommentModel?.comment_like_count ?? "0") \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes"))"
        
        if self.objFeedCommentModel!.is_like == kYes {
            self.btnLike.setImage(UIImage(named: "like_sel_ic"), for: .normal)
        }
        else {
            self.btnLike.setImage(UIImage(named: "like_unsel_ic"), for: .normal)
        }
    }
    
    @IBAction func btnBack_clicked(_ sender: Any) {
        if self.commentReplyList.count > 2 {
            self.objFeedCommentModel?.reply_comment = Array(self.commentReplyList.prefix(upTo: 2))
        }
        else {
            self.objFeedCommentModel?.reply_comment = self.commentReplyList
        }
        self.delegate?.updateFeedCommentObject(selSection: self.selectedSection, objFeedCommentUserModel: self.objFeedCommentModel)
        self.onBackClicked(self)
    }
    
    @IBAction func btnLike_clicked(_ sender: Any) {
        self.view.endEditing(true)
        self.callLikeDislikePostCommentAPI()
    }
    
    @IBAction func btnReplySmall_clicked(_ sender: Any) {
        self.txtEnterMessage.becomeFirstResponder()
    }
    
    @IBAction func btnReply_clicked(_ sender: Any) {
        self.view.endEditing(true)
        if self.txtEnterMessage.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Comment"))
        }
        else {
            self.callAddCommentAPI()
        }
    }
    
    @IBAction func btnUserProfile_Clicked(_ sender: UIButton)  {
    }
}

extension CommentReplyViewMoreViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentReplyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedDetailsCommentCell", for: indexPath) as! FeedDetailsCommentCell
       
        let objModel = self.commentReplyList[indexPath.row]
        cell.backgroundColor = .clear
        cell.separetorView.backgroundColor = .clear
       
        cell.constLeadingForImgViewUser.constant = 50
       
        cell.lblName.text = objModel.user?.full_name
        cell.txtViewDesc.text = objModel.comment
       
        let createdDate = self.convertStringToDate(format: dateFormateForGet, strDate: objModel.created_at ?? "")
        cell.lblTime.text = self.convertDateToString(format: dateFormateForDisplay, date: createdDate)
       
        if let url = objModel.user?.image {
            if let imgUrl = URL.init(string: url) {
                cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        if objModel.is_like == kYes {
            cell.btnLike.setImage(UIImage(named: "like_sel_ic"), for: .normal)
        }
        else {
            cell.btnLike.setImage(UIImage(named: "like_unsel_ic"), for: .normal)
        }
        
        cell.lblTotalLikes.text = "\(objModel.comment_like_count ?? "0") \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes"))"
        
        cell.btnMore.tag = indexPath.row
        cell.btnReply.setTitle(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Reply"), for: .normal)
        cell.btnReply.addTarget(self, action: #selector(self.btnReply_clicked(_:)), for: .touchUpInside)
        
        cell.btnMore.addTarget(self, action: #selector(self.btnMore_Clicked(_:)), for: .touchUpInside)
        cell.btnLike.addTarget(self, action: #selector(self.btnLikeRow_Clicked(_:)), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.commentReplyList.count - 1 {
            if let feed_comment_id = self.commentReplyList.last?.feed_comment_id {
                self.lastPostCommentId = "\(feed_comment_id)"
                self.callGetPostCommentsReplyAPI(isShowHud: false)
            }
        }
    }
    
    @objc func btnMore_Clicked(_ sender: UIButton) {
        if BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int == self.commentReplyList[sender.tag].appuser_id {
            guard let popVC = self.storyBoard.instantiateViewController(withIdentifier: "CustomMenuViewController") as? CustomMenuViewController else { return }
            popVC.modalPresentationStyle = .popover
            popVC.strFromVC = "CommentReplyRow"
            popVC.selectedIndex = sender.tag
            popVC.selectedSection = self.selectedSection
            popVC.delegateCustomMenu = self
            popVC.menuList = [self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete")]
            popVC.popoverPresentationController?.dimmingView?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            popVC.preferredContentSize = CGSize(width: Int(VERTICALMENU_SIZE.WIDTH.rawValue), height: Int(VERTICALMENU_SIZE.HEIGHT.rawValue) * popVC.menuList.count)
            
            let popOverVC = popVC.popoverPresentationController
            popOverVC?.delegate = self
            popOverVC?.sourceView = sender
            popOverVC?.sourceRect = sender.bounds
            popOverVC?.permittedArrowDirections = .up
            self.present(popVC, animated: true)
        }
        else{
            guard let popVC = self.storyBoard.instantiateViewController(withIdentifier: "CustomMenuViewController") as? CustomMenuViewController else { return }
            popVC.modalPresentationStyle = .popover
            popVC.strFromVC = "CommentReplyRow"
            popVC.selectedIndex = sender.tag
            popVC.selectedSection = self.selectedSection
            popVC.delegateCustomMenu = self
            popVC.menuList = [self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportReply")]
            popVC.popoverPresentationController?.dimmingView?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            popVC.preferredContentSize = CGSize(width: Int(VERTICALMENU_SIZE.WIDTH.rawValue), height: Int(VERTICALMENU_SIZE.HEIGHT.rawValue) * popVC.menuList.count)
            
            let popOverVC = popVC.popoverPresentationController
            popOverVC?.delegate = self
            popOverVC?.sourceView = sender
            popOverVC?.sourceRect = sender.bounds
            popOverVC?.permittedArrowDirections = .up
            self.present(popVC, animated: true)
        }
    }
    
    @objc func btnLikeRow_Clicked(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: self.tblViewFeedCommentReply)
        if let indexPath = self.tblViewFeedCommentReply.indexPathForRow(at: position) {
            self.callLikeDislikePostCommentReplyAPI(indexVal: indexPath.row, selSection: indexPath.section)
        }
    }
}

extension CommentReplyViewMoreViewController {
    func callAddCommentAPI() {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                    "Feedcomment[feed_id]" : self.objFeedCommentModel!.feed_id ?? "",
                    "Feedcomment[comment]": self.txtEnterMessage.text!,
                    "Feedcomment[parent_comment_id]": self.objFeedCommentModel!.feed_comment_id ?? ""] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedCommentUserModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.ADDCOMMENT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if let response = response {
                        self.txtEnterMessage.text = ""
                        if let dictFeedCommentReply = response.dictData {
                            self.commentReplyList.insert(dictFeedCommentReply, at: 0)
                            
                            if self.commentReplyList.count > 2 {
                                let counterForReply = self.objFeedCommentModel!.reply_comment_count!
                                self.objFeedCommentModel?.reply_comment_count = counterForReply + 1
                            }
                        }
                        self.tblViewFeedCommentReply.reloadData()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callGetPostCommentsReplyAPI(isShowHud: Bool) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                    "Feedcomment[parent_comment_id]" : self.objFeedCommentModel!.feed_comment_id ?? "",
                    "Feedcomment[feed_id]": self.objFeedCommentModel!.feed_id ?? "",
                    "Feedcomment[feed_comment_id]": self.lastPostCommentId] as [String : Any]
           
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedCommentUserModel.self, isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.GETPOSTCOMMENTREPLY.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if let response = response {
                        if let dataList = response.arrayData {
                            for data in dataList {
                                self.commentReplyList.append(data)
                            }
                        }
                        self.tblViewFeedCommentReply.reloadData()
                    }
                }
                else {
                    if error?.code == kFailureCancelCode {
                        return
                    }
                    if error?.code == kRecordNotFound {
                        if self.commentReplyList.count == 0 {
                        }
                    }
                    else {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: error?.domain ?? ErrorMessage)
                    }
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callLikeDislikePostCommentAPI() {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feedcommentlike[feed_id]": self.objFeedCommentModel!.feed_id ?? 0,
                     "Feedcommentlike[feed_comment_id]" : self.objFeedCommentModel!.feed_comment_id!] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.LIKEDISLIKEPOSTCOMMENT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        let counterForLikes = Int(self.objFeedCommentModel!.comment_like_count ?? "0")
                        if self.objFeedCommentModel?.is_like == kYes {
                            self.objFeedCommentModel?.is_like = kNo
                            self.objFeedCommentModel?.comment_like_count = "\(counterForLikes! - 1)"
                        }
                        else {
                            self.objFeedCommentModel?.is_like = kYes
                            self.objFeedCommentModel?.comment_like_count = "\(counterForLikes! + 1)"
                        }
                        
                        self.setComments()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callLikeDislikePostCommentReplyAPI(indexVal: Int, selSection: Int) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                    "Feedcommentlike[feed_id]": self.objFeedCommentModel!.feed_id ?? 0,
                    "Feedcommentlike[feed_comment_id]" : self.commentReplyList[indexVal].feed_comment_id ?? 0] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedCommentUserModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.LIKEDISLIKEPOSTCOMMENT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        let counterForLikes = Int(self.commentReplyList[indexVal].comment_like_count ?? "0")
                        if self.commentReplyList[indexVal].is_like == kYes {
                            self.commentReplyList[indexVal].is_like = kNo
                            self.commentReplyList[indexVal].comment_like_count! = "\(counterForLikes! - 1)"
                        }
                        else {
                            self.commentReplyList[indexVal].is_like = kYes
                            self.commentReplyList[indexVal].comment_like_count! = "\(counterForLikes! + 1)"
                        }
                        self.tblViewFeedCommentReply.reloadData()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callDeleteCommentReplyAPI(indexVal: Int, selSection: Int) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                    "Feedcomment[feed_id]": self.objFeedCommentModel?.feed_id ?? 0,
                    "Feedcomment[feed_comment_id]" : self.commentReplyList[indexVal].feed_comment_id ?? 0] as [String : Any]
           
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedCommentUserModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.DELETECOMMENT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        let counterForReply = self.objFeedCommentModel!.reply_comment_count!
                        self.objFeedCommentModel?.reply_comment_count = counterForReply - 1
                        self.commentReplyList.remove(at: indexVal)
                        self.tblViewFeedCommentReply.reloadData()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
  
    func callAddCommentReplyReportAPI(indexVal: Int, selSection: Int, comment: String) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Reportcomment[feed_id]": self.objFeedCommentModel?.feed_id ?? 0,
                     "Reportcomment[feed_comment_id]": self.commentReplyList[indexVal].feed_comment_id ?? 0,
                     "Reportcomment[reson]": comment] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.ADDCOMMENTREPORT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

extension CommentReplyViewMoreViewController: CustomMenuDelegate {
    func editDeleteReportFeedCommentRowData(strAction: String, selIndex: Int, selSection: Int, comment: String) {
        if strAction == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete") {
            self.callDeleteCommentReplyAPI(indexVal: selIndex, selSection: selSection)
        }
        else {
            self.callAddCommentReplyReportAPI(indexVal: selIndex, selSection: selSection, comment: comment)
        }
    }
}

extension CommentReplyViewMoreViewController : UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension CommentReplyViewMoreViewController: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
            self.constHeightForTxtEnterMessage.constant = height
        }
    }
}
