//
//  FeedLikeViewController.swift
//  RaniCircle
//
//  Created by Apple on 28/09/20.
//  Copyright © 2020 YellowPanther. All rights reserved.
//

import UIKit

class FeedLikeViewController: BaseViewController {
        
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblViewLike: UITableView!
    
    var feedId = 0
    var lastLikeId = ""
    var feedLikeUserList = [FeedLikeUserModel]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes")
        
        self.callGetPostLikeUserAPI(isShowHud: true)
    }
}

extension FeedLikeViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feedLikeUserList.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.feedLikeUserList.count - 1 {
            if let feed_id = self.feedLikeUserList.last?.feed_like_id {
                self.lastLikeId = "\(feed_id)"
                self.callGetPostLikeUserAPI(isShowHud: false)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedLikeTblCell", for: indexPath) as! FeedLikeTblCell
        
        let objModel = self.feedLikeUserList[indexPath.row]
        
        cell.lblName.text = objModel.user?.full_name
        
        if let url = objModel.user?.image {
            if let imgUrl = URL.init(string: url) {
                cell.imgView.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        return cell
    }
}

extension FeedLikeViewController {
    func callGetPostLikeUserAPI(isShowHud: Bool) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feedlike[feed_id]" : self.feedId,
                     "Feedlike[feed_like_id]": self.lastLikeId] as [String : Any]

        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedLikeUserModel.self, isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.GETPOSTLIKEUSER.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if let response = response {
                        if self.lastLikeId == "" {
                            self.feedLikeUserList.removeAll()
                            self.tblViewLike.setContentOffset(.zero, animated: true)
                        }
                        
                        if let arrayData = response.arrayData {
                            for data in arrayData {
                                self.feedLikeUserList.append(data)
                            }
                        }
                        self.tblViewLike.reloadData()
                    }
                }
                else {
                    if error?.code == kFailureCancelCode {
                        return
                    }
                    else if error?.code == kRecordNotFound {
                    }
                    else {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: error?.domain ?? ErrorMessage)
                    }
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

class FeedLikeTblCell : UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}
