//
//  ViewController.swift
//  Ashton
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit
import CHIPageControl
import AVKit
import LinkPresentation

protocol EditDeleteFeedObjectDelegate {
    func editDeleteFeedObject(isDelete: Bool,selIndex: Int, objFeedModel: FeedListModel?)
}

class FeedDetailsViewController: BaseViewController, AVAudioPlayerDelegate {
    
    var delegate: EditDeleteFeedObjectDelegate?
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTotalLikes: UILabel!
    @IBOutlet weak var lblTotalComments: UILabel!
    @IBOutlet weak var cvImageVideo: UICollectionView!
    @IBOutlet weak var pageControlView: CHIPageControlJaloro!
    @IBOutlet weak var txtViewDesc: UITextView!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var tblFeedDetails: UITableView!
    @IBOutlet weak var tblHeaderView: UIView!
    @IBOutlet weak var txtEnterMessage: GrowingTextView!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var viewLink: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var constHeightForTxtEnterMessage: NSLayoutConstraint!
    @IBOutlet weak var constTopForTxtViewDesc: NSLayoutConstraint!
    @IBOutlet weak var constHeightForViewLink: NSLayoutConstraint!
    @IBOutlet weak var constHeightForCVImageVideo: NSLayoutConstraint!
    
    var postID: Int?
    var lastPostCommentID = ""
    var objFeedModel: FeedListModel?
    var selectedIndex = -1
    var feedCommentList = [FeedCommentUserModel]()
    var currentPageForPageControlView = 0
    
    var player: AVPlayer?
    var playedAudioIndexPath: IndexPath!
    var audioPlayer: AVAudioPlayer?
    var audioTimer: Timer!
    var timeObserver: Any?
    
    private lazy var linkView = LPLinkView()
    private var metaData: LPLinkMetadata = LPLinkMetadata() {
        didSet {
            DispatchQueue.main.async {
                self.linkView = LPLinkView(metadata: self.metaData)
                self.viewLink.addSubview(self.linkView)
                self.linkView.frame = self.viewLink.bounds
            }
        }
    }
    
    var hasMoreData = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        self.txtEnterMessage.layer.borderWidth = 1
        self.txtEnterMessage.layer.borderColor = UIColor().themeDarkPinkColor.cgColor
        
        self.tblFeedDetails.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        
        self.callGetPostDetailsAPI()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tblFeedDetails.reloadData()
        self.tblFeedDetails.setAndLayoutTableHeaderView(header: self.tblHeaderView)
    }
    
    func setupLocalizationText() {
        self.lblComments.text = "  \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Comments"))"
        self.btnPost.setTitle(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Post"), for: .normal)
    }
    
    func fetchURLPreview(url: URL) {
        let metadataProvider = LPMetadataProvider()
        metadataProvider.startFetchingMetadata(for: url) { (metadata, error) in
            guard let data = metadata, error == nil else {
                return
            }
            self.metaData = data
            
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    func setFeedDetails() {
        self.lblName.text = self.objFeedModel?.user?.full_name
        self.imgVerify.isHidden = (self.objFeedModel?.user?.is_verified ?? "") == kYes ? false : true
        self.txtViewDesc.text = self.objFeedModel?.description ?? ""
        self.constTopForTxtViewDesc.constant = (self.txtViewDesc.text == "") ? -25 : 0
        
        let createdDate = self.convertStringToDate(format: dateFormateForGet, strDate: self.objFeedModel!.created_at!)
        self.lblTime.text = self.convertDateToString(format: dateFormateForDisplay, date: createdDate)
        
        if let url = self.objFeedModel?.user?.image {
            if let imgUrl = URL.init(string: url) {
                self.imgViewUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
            }
        }
        
        self.lblTotalLikes.text = "\(self.objFeedModel!.like_count!) \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes"))"
        self.lblTotalComments.text = "\(self.objFeedModel!.comment_count!) \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Comments"))"
        
        if self.objFeedModel!.is_like == kYes {
            self.btnLike.setImage(UIImage(named: "heart"), for: .normal)
        }
        else {
            self.btnLike.setImage(UIImage(named: "heart-empty"), for: .normal)
        }
        
        if self.objFeedModel!.is_bookmark == kYes {
            self.btnBookmark.setImage(UIImage(named: "bookmark-fill"), for: .normal)
        }
        else {
            self.btnBookmark.setImage(UIImage(named: "Bookmark"), for: .normal)
        }
        
        self.constHeightForViewLink.constant = 0
        self.constHeightForCVImageVideo.constant = 0
        
        if self.objFeedModel!.images!.count > 0 {
            self.cvImageVideo.reloadData()
            self.pageControlView.radius = 4
            self.pageControlView.tintColor = UIColor.white
            self.pageControlView.currentPageTintColor = UIColor().alertButtonColor
            self.pageControlView.padding = 6
            self.pageControlView.progress = 0.5
            self.pageControlView.set(progress: 0, animated: true)
            self.pageControlView.numberOfPages = (objFeedModel?.images!.count)!
            
            self.constHeightForViewLink.constant = 0
            self.viewLink.isHidden = true
            
            if self.objFeedModel?.getType() == FEEDTYPE.IMAGE.rawValue || self.objFeedModel?.getType() == FEEDTYPE.VIDEO.rawValue || self.objFeedModel?.getType() == FEEDTYPE.GIF.rawValue {
                if self.objFeedModel!.images!.count > 0 {
                    let imageHeightStr = self.objFeedModel?.images![0].height ?? ""
                    let imageWidthStr = self.objFeedModel?.images![0].width ?? ""
                    if imageHeightStr.isEmpty || imageWidthStr.isEmpty {
                        return
                    }
                    
                    if let imageHeight = Double.init(imageHeightStr) {
                        if let imageWidth = Double.init(imageWidthStr) {
                            var height = (imageHeight * Double(SCREEN_WIDTH) - 25) / imageWidth
                            if height > 450 {
                                height = 450
                            }
                            self.constHeightForCVImageVideo.constant = CGFloat(height)
                            DispatchQueue.main.async {
                                self.tblFeedDetails.reloadData()
                                self.tblFeedDetails.setAndLayoutTableHeaderView(header: self.tblHeaderView)
                            }
                        }
                    }
                }
            }
            
        }
        else{
            self.constHeightForCVImageVideo.constant = 0
            self.pageControlView.isHidden = true
            self.viewLink.isHidden = false
            
            for subview in self.viewLink.subviews {
                if subview is LPLinkView {
                    subview.removeFromSuperview()
                }
            }
            
            do {
                let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                let range = NSRange(self.objFeedModel!.description!.startIndex..<self.objFeedModel!.description!.endIndex, in: self.objFeedModel!.description!)
                if let firstMatch = detector.firstMatch(in: self.objFeedModel!.description!, options: [], range: range) {
                    self.activityIndicator.startAnimating()
                    var url = firstMatch.url!
                    if url.scheme == "http" {
                        if var urlComps = URLComponents(url: url, resolvingAgainstBaseURL: false) {
                            urlComps.scheme = "https"
                            url = urlComps.url!
                        }
                    }
                    let file_name = "\(url)"
                    if self.metaDataDict.keys.contains(file_name) {
                        self.activityIndicator.stopAnimating()
                        self.constHeightForViewLink.constant = 250
                        let linkView = LPLinkView(metadata: self.metaDataDict[file_name]!)
                        self.viewLink.addSubview(linkView)
                        
                        linkView.translatesAutoresizingMaskIntoConstraints = false
                        let horizontalConstraint = NSLayoutConstraint(item: linkView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewLink, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
                        let verticalConstraint = NSLayoutConstraint(item: linkView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewLink, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
                        let widthConstraint = NSLayoutConstraint(item: linkView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: SCREEN_WIDTH - 25)
                        let heightConstraint = NSLayoutConstraint(item: linkView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.constHeightForViewLink.constant)
                        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
                        
                        if (self.objFeedModel!.description ?? "").validURL {
                            self.txtViewDesc.text = ""
                            self.constTopForTxtViewDesc.constant = -25
                            self.txtViewDesc.isHidden = true
                        }
                        else {
                            self.constTopForTxtViewDesc.constant = 5
                            self.txtViewDesc.isHidden = false
                        }
                    }
                    else {
                        self.activityIndicator.stopAnimating()
                        self.constHeightForViewLink.constant = 0
                    }
                }
                else {
                    print("No url deteched")
                    self.activityIndicator.stopAnimating()
                    self.constHeightForViewLink.constant = 0
                }
            }
            catch {
            }
        }
        
        self.tblFeedDetails.tableHeaderView?.isHidden = false
    }
    
    @IBAction func btnBack_clicked(_ sender: Any) {
        self.delegate?.editDeleteFeedObject(isDelete: false, selIndex: self.selectedIndex, objFeedModel: self.objFeedModel)
        self.onBackClicked(self)
    }
    
    @IBAction func btnUserProfile_Clicked(_ sender: UIButton)  {
    }
    
    @IBAction func btnTotalLikeUser_Clicked(_ sender: UIButton)  {
        if let intLikeCount = Int(self.objFeedModel?.like_count ?? "0"), intLikeCount > 0 {
            let viewController = self.storyBoard.instantiateViewController(withIdentifier: "FeedLikeViewController") as! FeedLikeViewController
            viewController.feedId = self.objFeedModel?.feed_id ?? 0
            let sheetController = SheetViewController(controller: viewController, sizes: [.percent(0.80)], options: SheetOptions(useInlineMode: false))
            self.present(sheetController, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnLike_clicked(_ sender: Any) {
         self.callLikeDislikePostAPI()
    }
    
    @IBAction func btnComment_clicked(_ sender: Any) {
    }
    
    @IBAction func btnBookmark_clicked(_ sender: Any) {
         self.callBookmarkPostAPI()
    }
    
    @IBAction func btnPost_clicked(_ sender: Any) {
        self.view.endEditing(true)
        if self.txtEnterMessage.text?.trim().count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Comment"))
        }
        else {
            self.callAddCommentAPI()
        }
    }
    
    @IBAction func btnMore_Clicked(_ sender: UIButton) {
        if BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int == self.objFeedModel?.appuser_id {
            guard let popVC = self.storyBoard.instantiateViewController(withIdentifier: "CustomMenuViewController") as? CustomMenuViewController else { return }
            popVC.modalPresentationStyle = .popover
            popVC.strFromVC = "FeedDetails"
            popVC.selectedIndex = self.selectedIndex
            popVC.delegateCustomMenu = self
            popVC.menuList = [self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete")]
            popVC.popoverPresentationController?.dimmingView?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            popVC.preferredContentSize = CGSize(width: Int(VERTICALMENU_SIZE.WIDTH.rawValue), height: Int(VERTICALMENU_SIZE.HEIGHT.rawValue) * popVC.menuList.count)
               
            let popOverVC = popVC.popoverPresentationController
            popOverVC?.delegate = self
            popOverVC?.sourceView = sender
            popOverVC?.sourceRect = sender.bounds
            popOverVC?.permittedArrowDirections = .up
            self.present(popVC, animated: true)
        }
        else {
            guard let popVC = self.storyBoard.instantiateViewController(withIdentifier: "CustomMenuViewController") as? CustomMenuViewController else { return }
            popVC.modalPresentationStyle = .popover
            popVC.strFromVC = "FeedDetails"
            popVC.selectedIndex = self.selectedIndex
            popVC.delegateCustomMenu = self
            popVC.menuList = [self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportPost")]
            popVC.popoverPresentationController?.dimmingView?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            popVC.preferredContentSize = CGSize(width: Int(VERTICALMENU_SIZE.WIDTH.rawValue), height: Int(VERTICALMENU_SIZE.HEIGHT.rawValue) * popVC.menuList.count)
               
            let popOverVC = popVC.popoverPresentationController
            popOverVC?.delegate = self
            popOverVC?.sourceView = sender
            popOverVC?.sourceRect = sender.bounds
            popOverVC?.permittedArrowDirections = .up
            self.present(popVC, animated: true)
        }
    }
}

extension FeedDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.cvImageVideo.bounds.width, height: self.cvImageVideo.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.objFeedModel?.images?.count ?? 0 > 0  {
            return self.objFeedModel!.images!.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objModel = self.objFeedModel!.images![indexPath.item]
        
        if objModel.type == FEEDTYPE.AUDIO.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedMultiAudioCell", for: indexPath) as! FeedMultiAudioCell
            cell.btnPlay.addTarget(self, action: #selector(btnPlayAudio_Clicked(sender:)), for: .touchUpInside)
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedMultiImagesCell", for: indexPath) as! FeedMultiImagesCell
            cell.imgViewPostGIF.tag += 1
            
            cell.imgViewPost.image = #imageLiteral(resourceName: "placeholder")
            cell.imgViewPostGIF.image = #imageLiteral(resourceName: "placeholder")
            cell.imgViewPost.isHidden = true
            cell.imgViewPostGIF.isHidden = true
            
            if objModel.type == FEEDTYPE.GIF.rawValue {
                cell.btnPlay.isHidden = true
                cell.viewGIF.isHidden = false
                
                cell.imgViewPost.isHidden = true
                cell.imgViewPostGIF.isHidden = false
                cell.imgViewPostGIF.clear()
                
                if let fileName = objModel.file {
                    DispatchQueue.main.async {
                        if let image = try? UIImage(imageName: fileName) {
                            cell.imgViewPostGIF.setImage(image, manager: gifManager, loopCount: -1)
                        }
                        else if let url = URL.init(string: fileName) {
                            let loader = UIActivityIndicatorView.init(style: .medium)
                            cell.imgViewPostGIF.setGifFromURL(url, customLoader: loader)
                        }
                        else {
                            cell.imgViewPostGIF.clear()
                        }
                    }
                }
            }
            else if objModel.type == FEEDTYPE.IMAGE.rawValue {
                cell.btnPlay.isHidden = true
                cell.viewGIF.isHidden = true
                
                cell.imgViewPost.isHidden = false
                cell.imgViewPostGIF.isHidden = true
                
                if let url = objModel.file {
                    if let imgUrl = URL.init(string: url) {
                        DispatchQueue.main.async {
                            cell.imgViewPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
                        }
                    }
                }
            }
            else if objModel.type == FEEDTYPE.VIDEO.rawValue {
                cell.btnPlay.isHidden = false
                cell.viewGIF.isHidden = true
                
                cell.imgViewPost.isHidden = false
                cell.imgViewPostGIF.isHidden = true
                
                if let url = objModel.thumb {
                    if let imgUrl = URL.init(string: url) {
                        DispatchQueue.main.async {
                            cell.imgViewPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
                        }
                    }
                }
                cell.btnPlay.addTarget(self, action: #selector(btnPlayVideo_clicked), for: .touchUpInside)
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objModel = self.objFeedModel!.images![indexPath.item]
        if objModel.type == FEEDTYPE.IMAGE.rawValue || objModel.type == FEEDTYPE.GIF.rawValue {
            if self.objFeedModel!.images!.count > 0 {
                var dict = [String:Any]()
                dict["multiplePhoto"] = self.objFeedModel!.images
                dict["visibleIndex"] = self.currentPageForPageControlView
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OpenPhotoVCFromCell"), object: nil, userInfo: dict)
            }
            else {
                var dict = [String:Any]()
                dict["singlePhoto"] = objModel
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OpenPhotoVCFromCell"), object: nil, userInfo: dict)
            }
        }
        else if objModel.type == FEEDTYPE.VIDEO.rawValue {
            var dict = [String:Any]()
            dict["dictVideo"] = objModel.file
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playVideoFromCell"), object: nil, userInfo: dict)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.width
        self.currentPageForPageControlView = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        self.pageControlView.set(progress: self.currentPageForPageControlView, animated: true)
    }
    
    @objc func btnPlayVideo_clicked(sender : UIButton) {
        var dict = [String:Any]()
        dict["dictVideo"] = self.objFeedModel!.images![self.currentPageForPageControlView].file
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playVideoFromCell"), object: nil, userInfo: dict)
    }
    
    @objc func btnPlayAudio_Clicked(sender: AnyObject) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: self.cvImageVideo)
        if let indexPath = self.cvImageVideo.indexPathForItem(at: buttonPosition) {
            if let cell = self.cvImageVideo.cellForItem(at: indexPath) as? FeedMultiAudioCell {
                let chatDetails = self.objFeedModel!.images![indexPath.item]
                
                if self.playedAudioIndexPath == nil {
                    if self.timeObserver != nil {
                        print("removeTimeObserver")
                        if self.player != nil {
                            self.player!.removeTimeObserver(self.timeObserver!)
                            self.timeObserver = nil
                        }
                    }
                    else {
                        print("timeObserver nil")
                    }
                    
                    self.playedAudioIndexPath = indexPath
                    if let content = chatDetails.file {
                        self.playAudio(content: content, cell: cell)
                    }
                }
                else {
                    if self.playedAudioIndexPath != indexPath {
                        if self.player != nil {
                            if let previousPlayCell = self.cvImageVideo.cellForItem(at: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                                self.player!.pause()
                                previousPlayCell.slider.value = 0.0
                                previousPlayCell.slider.setNeedsLayout()
                                previousPlayCell.slider.layoutIfNeeded()
                                previousPlayCell.btnPlay.isSelected = false
                                previousPlayCell.activityIndicator.stopAnimating()
                                
                                if self.timeObserver != nil {
                                    print("removeTimeObserver")
                                    self.player!.removeTimeObserver(self.timeObserver!)
                                    self.timeObserver = nil
                                }
                                else {
                                    print("timeObserver nil")
                                }
                                
                                self.playedAudioIndexPath = indexPath
                                if let content = chatDetails.file {
                                    self.playAudio(content: content, cell: cell)
                                }
                            }
                            else {
                                if let previousPlayCell = self.cvImageVideo.dataSource?.collectionView(self.cvImageVideo, cellForItemAt: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                                    self.player!.pause()
                                    previousPlayCell.slider.value = 0.0
                                    previousPlayCell.slider.setNeedsLayout()
                                    previousPlayCell.slider.layoutIfNeeded()
                                    previousPlayCell.btnPlay.isSelected = false
                                    previousPlayCell.activityIndicator.stopAnimating()
                                    
                                    if self.timeObserver != nil {
                                        print("removeTimeObserver")
                                        self.player!.removeTimeObserver(self.timeObserver!)
                                        self.timeObserver = nil
                                    }
                                    else {
                                        print("timeObserver nil")
                                    }
                                    
                                    self.playedAudioIndexPath = indexPath
                                    if let content = chatDetails.file {
                                        self.playAudio(content: content, cell: cell)
                                    }
                                }
                            }
                        }
                    }
                    else {
                        if self.player != nil && self.player!.rate == 1 {
                            self.player!.pause()
                            cell.btnPlay.isSelected = false
                        }
                        else {
                            self.player!.play()
                            cell.btnPlay.isSelected = true
                        }
                    }
                }
            }
        }
    }
    
    func playAudio(content: String, cell: FeedMultiAudioCell) {
        guard let url = URL.init(string: content) else {
            print("Invalid URL")
            return
        }
        let playerItem: AVPlayerItem = AVPlayerItem(url: url)
        self.player = AVPlayer(playerItem: playerItem)
        
        cell.slider.isUserInteractionEnabled = false
        cell.slider.minimumValue = 0.0
        
        let duration: CMTime = playerItem.asset.duration
        let seconds: Float64 = CMTimeGetSeconds(duration)
        cell.slider.maximumValue = Float(seconds)
        cell.slider.value = 0.0
        cell.slider.isContinuous = true
        DispatchQueue.main.async {
            cell.activityIndicator.startAnimating()
        }
        
        if self.player != nil {
            self.timeObserver = self.player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
                if self.player == nil {
                    return
                }
                if let previousPlayCell = self.cvImageVideo.cellForItem(at: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                    if self.player!.currentItem?.status == .readyToPlay {
                        let time: Float64 = CMTimeGetSeconds(self.player!.currentTime())
                        print("time: \(time)")
                        previousPlayCell.slider.value = Float(time)
                        previousPlayCell.slider.setNeedsLayout()
                        previousPlayCell.slider.layoutIfNeeded()
                    }
                    let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
                    if playbackLikelyToKeepUp == false {
                        print("IsBuffering")
                        previousPlayCell.activityIndicator.startAnimating()
                    }
                    else {
                        print("Buffering completed")
                        previousPlayCell.activityIndicator.stopAnimating()
                    }
                }
                else {
                    if let previousPlayCell = self.cvImageVideo.dataSource?.collectionView(self.cvImageVideo, cellForItemAt: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                        if self.player!.currentItem?.status == .readyToPlay {
                            let time: Float64 = CMTimeGetSeconds(self.player!.currentTime())
                            print("time1: \(time)")
                            previousPlayCell.slider.value = Float(time)
                            previousPlayCell.slider.setNeedsLayout()
                            previousPlayCell.slider.layoutIfNeeded()
                        }
                        let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
                        if playbackLikelyToKeepUp == false {
                            print("IsBuffering")
                            previousPlayCell.activityIndicator.startAnimating()
                        }
                        else {
                            print("Buffering completed")
                            previousPlayCell.activityIndicator.stopAnimating()
                        }
                    }
                }
            }
            
            if self.player!.rate == 0 {
                self.player!.play()
                cell.btnPlay.isSelected = true
            }
            else {
                self.player!.pause()
                cell.btnPlay.isSelected = false
            }
        }
        else {
            cell.activityIndicator.stopAnimating()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.finishedPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
}

//Audio Finish Observer
extension FeedDetailsViewController {
    @objc func finishedPlaying( _ myNotification: NSNotification) {
        if self.playedAudioIndexPath != nil {
            if let cell = self.cvImageVideo.cellForItem(at: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                cell.btnPlay.isSelected = false
                cell.slider.value = 0.0
                cell.slider.setNeedsLayout()
                cell.slider.layoutIfNeeded()
            }
            else {
                if let cell = self.cvImageVideo.dataSource?.collectionView(self.cvImageVideo, cellForItemAt: self.playedAudioIndexPath) as? FeedMultiAudioCell {
                    cell.btnPlay.isSelected = false
                    cell.slider.value = 0.0
                    cell.slider.setNeedsLayout()
                    cell.slider.layoutIfNeeded()
                }
            }
            self.playedAudioIndexPath = nil
        }
        
        if self.player != nil {
            if self.timeObserver != nil {
                self.player!.removeTimeObserver(self.timeObserver!)
                self.timeObserver = nil
            }
            
            let targetTime: CMTime = CMTimeMake(value: 0, timescale: 1)
            self.player!.seek(to: targetTime)
            self.player = nil
        }
    }
}

extension FeedDetailsViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.feedCommentList.count > 0 {
            return self.feedCommentList.count
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.feedCommentList.count > 0 {
            if self.feedCommentList[section].reply_comment_count! > 0 {
                return self.feedCommentList[section].reply_comment!.count + 1
            }
            return self.feedCommentList[section].reply_comment!.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.feedCommentList.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedDetailsCommentCell") as! FeedDetailsCommentCell
            let objModel = self.feedCommentList[section]
            
            cell.separetorView.isHidden = true
                      
            cell.constLeadingForImgViewUser.constant = 10
           
            cell.lblName.text = objModel.user?.full_name
    
            cell.txtViewDesc.text = objModel.comment
           
            let createdDate = self.convertStringToDate(format: dateFormateForGet, strDate: objModel.created_at ?? "")
            cell.lblTime.text = self.convertDateToString(format: dateFormateForDisplay, date: createdDate)
           
            if let url = objModel.user?.image {
                if let imgUrl = URL.init(string: url) {
                    cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
           
            if objModel.is_like == kYes {
                cell.btnLike.setImage(UIImage(named: "heart"), for: .normal)
            }
            else {
                cell.btnLike.setImage(UIImage(named: "heart-empty"), for: .normal)
            }
           
            cell.lblTotalLikes.text = "\(objModel.comment_like_count ?? "0") \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes"))"
           
            cell.btnMore.tag = section
            cell.btnMore.addTarget(self, action: #selector(self.btnMoreSection_Clicked(_:)), for: .touchUpInside)
           
            cell.btnLike.tag = section
            cell.btnLike.addTarget(self, action: #selector(self.btnLikeSection_Clicked(_:)), for: .touchUpInside)
           
            cell.btnReply.isHidden = false
            cell.btnReply.setTitle(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Reply"), for: .normal)
            cell.btnReply.tag = section
            cell.btnReply.addTarget(self, action: #selector(self.btnViewMoreReply_Clicked(_:)), for: .touchUpInside)
           
            cell.btnUserProfile.tag = section
            cell.btnUserProfile.addTarget(self, action: #selector(self.btnUserProfileSection_Clicked), for: .touchUpInside)
           
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCommentNoDataCell") as! FeedCommentNoDataCell
            
            cell.lblEmptyText.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_NoCommentsText")
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let objModel = self.feedCommentList[indexPath.section]
        if objModel.reply_comment_count! > 0 && (self.tblFeedDetails.numberOfRows(inSection: indexPath.section) - 1) == indexPath.row  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCommentViewMoreCell", for: indexPath) as! FeedCommentViewMoreCell
            let viewMoreReplyText = "\(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ViewMore")) \(objModel.reply_comment_count ?? 0) \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Replies").lowercased())"
            cell.btnViewMoreReply.setTitle(viewMoreReplyText, for: .normal)
            cell.btnViewMoreReply.tag = indexPath.section
            cell.btnViewMoreReply.addTarget(self, action: #selector(self.btnViewMoreReply_Clicked(_:)), for: .touchUpInside)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedDetailsCommentCell", for: indexPath) as! FeedDetailsCommentCell
            
            let objModelRow = objModel.reply_comment![indexPath.row]

            cell.separetorView.isHidden = true
            
            cell.constLeadingForImgViewUser.constant = 50
            
            cell.lblName.text = objModel.user?.full_name
            cell.txtViewDesc.text = objModelRow.comment
            
            let createdDate = self.convertStringToDate(format: dateFormateForGet, strDate: objModelRow.created_at!)
            cell.lblTime.text = self.convertDateToString(format: dateFormateForDisplay, date: createdDate)
            
            if let url = objModelRow.user?.image {
                if let imgUrl = URL.init(string: url) {
                    cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "placeholder"))
                }
            }
            
            if objModelRow.is_like == kYes {
                cell.btnLike.setImage(UIImage(named: "heart"), for: .normal)
            }
            else {
                cell.btnLike.setImage(UIImage(named: "heart-empty"), for: .normal)
            }
            
            cell.lblTotalLikes.text = "\(objModelRow.comment_like_count ?? "0") \(self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_Likes"))"
            
            cell.btnReply.isHidden = true
            
            cell.btnMore.addTarget(self, action: #selector(self.btnMoreRow_Clicked(_:)), for: .touchUpInside)
            cell.btnLike.addTarget(self, action: #selector(self.btnLikeRow_Clicked(_:)), for: .touchUpInside)

            cell.btnUserProfile.addTarget(self, action: #selector(self.btnUserProfileRow_Clicked(sender:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if section == self.feedCommentList.count - 1 && self.hasMoreData {
            if let feed_comment_id = self.feedCommentList.last?.feed_comment_id {
                self.lastPostCommentID = "\(feed_comment_id)"
                self.callGetPostCommentsAPI(isShowHud: false)
            }
        }
    }
    
    @objc func btnUserProfileSection_Clicked(sender: UIButton)  {
    }
    
    @objc func btnUserProfileRow_Clicked(sender: AnyObject)  {
    }
    
    @objc func btnViewMoreReply_Clicked(_ sender: UIButton) {
        let viewController = self.storyBoard.instantiateViewController(withIdentifier: "CommentReplyViewMoreViewController") as! CommentReplyViewMoreViewController
        viewController.objFeedCommentModel = self.feedCommentList[sender.tag]
        viewController.selectedSection = sender.tag
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func btnLikeSection_Clicked(_ sender: UIButton) {
        self.callLikeDislikePostCommentAPI(indexVal: sender.tag, selSection: -1)
    }
    
    @objc func btnLikeRow_Clicked(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: self.tblFeedDetails)
        if let indexPath = self.tblFeedDetails.indexPathForRow(at: position) {
            self.callLikeDislikePostCommentReplyAPI(indexVal: indexPath.row, selSection: indexPath.section)
        }
    }
    
    @objc func btnMoreSection_Clicked(_ sender: UIButton) {
        if BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int == self.feedCommentList[sender.tag].appuser_id {
            guard let popVC = self.storyBoard.instantiateViewController(withIdentifier: "CustomMenuViewController") as? CustomMenuViewController else { return }
            popVC.modalPresentationStyle = .popover
            popVC.strFromVC = "FeedDetailsCellSection"
            popVC.selectedIndex = sender.tag
            popVC.delegateCustomMenu = self
            popVC.menuList = [self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete")]
            popVC.popoverPresentationController?.dimmingView?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            popVC.preferredContentSize = CGSize(width: Int(VERTICALMENU_SIZE.WIDTH.rawValue), height: Int(VERTICALMENU_SIZE.HEIGHT.rawValue) * popVC.menuList.count)
            
            let popOverVC = popVC.popoverPresentationController
            popOverVC?.delegate = self
            popOverVC?.sourceView = sender
            popOverVC?.sourceRect = sender.bounds
            popOverVC?.permittedArrowDirections = .up
            self.present(popVC, animated: true)
        }
        else {
            guard let popVC = self.storyBoard.instantiateViewController(withIdentifier: "CustomMenuViewController") as? CustomMenuViewController else { return }
            popVC.modalPresentationStyle = .popover
            popVC.strFromVC = "FeedDetailsCellSection"
            popVC.selectedIndex = sender.tag
            popVC.delegateCustomMenu = self
            popVC.menuList = [self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportComment")]
            popVC.popoverPresentationController?.dimmingView?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            popVC.preferredContentSize = CGSize(width: Int(VERTICALMENU_SIZE.WIDTH.rawValue), height: Int(VERTICALMENU_SIZE.HEIGHT.rawValue) * popVC.menuList.count)
            
            let popOverVC = popVC.popoverPresentationController
            popOverVC?.delegate = self
            popOverVC?.sourceView = sender
            popOverVC?.sourceRect = sender.bounds
            popOverVC?.permittedArrowDirections = .up
            self.present(popVC, animated: true)
        }
    }
    
    @objc func btnMoreRow_Clicked(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: self.tblFeedDetails)
        if let indexPath = self.tblFeedDetails.indexPathForRow(at: position) {
            if BaseViewController.sharedInstance.appDelegate.userDetails[kAppUserId] as? Int == self.feedCommentList[indexPath.section].reply_comment![indexPath.row].user?.appuser_id {
                guard let popVC = self.storyBoard.instantiateViewController(withIdentifier: "CustomMenuViewController") as? CustomMenuViewController else { return }
                popVC.modalPresentationStyle = .popover
                popVC.strFromVC = "FeedDetailsCellRow"
                popVC.selectedIndex = indexPath.row
                popVC.selectedSection = indexPath.section
                popVC.delegateCustomMenu = self
                popVC.menuList = [self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete")]
                popVC.popoverPresentationController?.dimmingView?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
                popVC.preferredContentSize = CGSize(width: Int(VERTICALMENU_SIZE.WIDTH.rawValue), height: Int(VERTICALMENU_SIZE.HEIGHT.rawValue) * popVC.menuList.count)
                
                let popOverVC = popVC.popoverPresentationController
                popOverVC?.delegate = self
                popOverVC?.sourceView = sender
                popOverVC?.sourceRect = sender.bounds
                popOverVC?.permittedArrowDirections = .up
                self.present(popVC, animated: true)
            }
            else {
                guard let popVC = self.storyBoard.instantiateViewController(withIdentifier: "CustomMenuViewController") as? CustomMenuViewController else { return }
                popVC.modalPresentationStyle = .popover
                popVC.strFromVC = "FeedDetailsCellRow"
                popVC.selectedIndex = indexPath.row
                popVC.selectedSection = indexPath.section
                popVC.delegateCustomMenu = self
                popVC.menuList = [self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportReply")]
                popVC.popoverPresentationController?.dimmingView?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
                popVC.preferredContentSize = CGSize(width: Int(VERTICALMENU_SIZE.WIDTH.rawValue), height: Int(VERTICALMENU_SIZE.HEIGHT.rawValue) * popVC.menuList.count)
                
                let popOverVC = popVC.popoverPresentationController
                popOverVC?.delegate = self
                popOverVC?.sourceView = sender
                popOverVC?.sourceRect = sender.bounds
                popOverVC?.permittedArrowDirections = .up
                self.present(popVC, animated: true)
            }
        }
    }
}

extension FeedDetailsViewController: CustomMenuDelegate {
    func editDeleteReportFeedData(strAction: String, selIndex: Int, comment: String) {
        if strAction == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete") {
            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: self.getLocalizeTextForKey(keyName: "SuperRoyals_DeletePost"), preferredStyle: .alert)
            alert.view.tintColor = UIColor().alertButtonColor
            let yesAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Yes"), style: .default, handler: { (action) in
                self.callDeletePostAPI(indexVal: selIndex)
            })
            let noAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_No"), style: .default, handler: { (action) in
            })
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        }
        else if strAction == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportPost") {
            self.callAddPostReportAPI(comment: comment)
        }
    }
    
    func editDeleteReportFeedCommentSectionData(strAction: String, selIndex: Int, comment: String) {
        print("editDeleteReportFeedCommentSectionData: \(strAction) \(selIndex)")
        if strAction == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete") {
            self.callDeleteCommentAPI(indexVal: selIndex, selSection: -1)
        }
        else {
            self.callAddCommentReportAPI(indexVal: selIndex, selSection: -1, comment: comment)
        }
    }
    
    func editDeleteReportFeedCommentRowData(strAction: String, selIndex: Int, selSection: Int, comment: String) {
        print("editDeleteReportFeedCommentRowData: \(strAction) \(selIndex) \(selSection)")
        if strAction == self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Delete") {
            self.callDeleteCommentReplyAPI(indexVal: selIndex, selSection: selSection)
        }
        else {
            self.callAddCommentReplyReportAPI(indexVal: selIndex, selSection: selSection, comment: comment)
        }
    }
}

extension FeedDetailsViewController: ReplaceFeedCommentObjectDelegate {
    func updateFeedCommentObject(selSection: Int, objFeedCommentUserModel: FeedCommentUserModel?) {
        self.feedCommentList[selSection] = objFeedCommentUserModel!
        self.tblFeedDetails.reloadData()
    }
}

extension FeedDetailsViewController : UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension FeedDetailsViewController: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
            self.constHeightForTxtEnterMessage.constant = height
        }
    }
}

extension FeedDetailsViewController {
    func callGetPostDetailsAPI() {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feed[feed_id]" : self.postID!] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.GETPOSTDETAILS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if let response = response {
                        if let dictFeed = response.dictData {
                            self.objFeedModel = dictFeed
                            self.setFeedDetails()
                        }
                        
                        self.callGetPostCommentsAPI(isShowHud: true)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callGetPostCommentsAPI(isShowHud: Bool) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feedcomment[feed_id]" : self.postID!,
                     "Feedcomment[feed_comment_id]": self.lastPostCommentID] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedCommentUserModel.self, isShowHud: isShowHud, URL: baseURLWithAuth, apiName: APINAME.GETPOSTCOMMENTS.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if let response = response {
                        if let dataList = response.arrayData {
                            for data in dataList {
                                self.feedCommentList.append(data)
                            }
                        }
                        
                        self.tblFeedDetails.reloadData()
                    }
                }
                else {
                    if error?.code == kFailureCancelCode {
                        return
                    }
                    if error?.code == kRecordNotFound {
                        self.hasMoreData = false
                    }
                    else {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: error?.domain ?? ErrorMessage)
                    }
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callLikeDislikePostAPI() {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feedlike[feed_id]" : self.postID!] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.LIKEDISLIKEPOST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        let counterForLikes = Int(self.objFeedModel!.like_count ?? "0")
                        if self.objFeedModel?.is_like == kYes {
                            self.objFeedModel?.is_like = kNo
                            self.objFeedModel?.like_count = "\(counterForLikes! - 1)"
                        }
                        else{
                            self.objFeedModel?.is_like = kYes
                            self.objFeedModel?.like_count = "\(counterForLikes! + 1)"
                        }
                        self.setFeedDetails()
                    }
                }
                else {
                    //UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callBookmarkPostAPI() {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Bookmarkfeed[feed_id]" : self.postID!] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.BOOKMARKPOST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        if self.objFeedModel?.is_bookmark == kYes {
                            self.objFeedModel?.is_bookmark = kNo
                        }
                        else {
                            self.objFeedModel?.is_bookmark = kYes
                        }
                        self.setFeedDetails()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callDeletePostAPI(indexVal: Int) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feed[feed_id]" : self.postID!] as [String : Any]
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.DELETEPOST.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        self.delegate?.editDeleteFeedObject(isDelete: true, selIndex: self.selectedIndex, objFeedModel: self.objFeedModel)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callAddPostReportAPI(comment: String) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Reportfeed[feed_id]" : self.postID!,
                     "Reportfeed[reson]": comment] as [String : Any]
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.ADDPOSTREPORT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if let response = response {
                        UIAlertController().alertViewWithTitleAndMessage(self, message: response.message!)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }

    func callAddCommentAPI() {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feedcomment[feed_id]" : self.postID!,
                     "Feedcomment[comment]": self.txtEnterMessage.text!] as [String : Any]
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedCommentUserModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.ADDCOMMENT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if let response = response {
                        self.txtEnterMessage.text = ""
                        if let dictFeedComment = response.dictData {
                            self.feedCommentList.insert(dictFeedComment, at: 0)
                            let countComment = Int(self.objFeedModel!.comment_count!)
                            self.objFeedModel!.comment_count = "\(countComment! + 1)"
                        }
                        self.tblFeedDetails.reloadData()
                        
                        let param = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "", "postID": "\(self.objFeedModel!.feed_id ?? 0)", "eventType": POINTSEVENTTYPE.FEED_COMMENT.rawValue]
                        self.callEarnPointsAPI(param: param)
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }

    func callLikeDislikePostCommentAPI(indexVal: Int, selSection: Int) {
        var param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feedcommentlike[feed_id]": self.postID!] as [String : Any]
        
        if selSection == -1 {
            param["Feedcommentlike[feed_comment_id]"] = "\(self.feedCommentList[indexVal].feed_comment_id ?? 0)"
        }
        else {
            param["Feedcommentlike[feed_comment_id]"] = "\(self.feedCommentList[selSection].reply_comment?[indexVal].feed_comment_id ?? 0)"
        }
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.LIKEDISLIKEPOSTCOMMENT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        if selSection == -1 {
                            let counterForLikes = Int(self.feedCommentList[indexVal].comment_like_count ?? "0")
                            if self.feedCommentList[indexVal].is_like == kYes {
                                self.feedCommentList[indexVal].is_like = kNo
                                self.feedCommentList[indexVal].comment_like_count = "\(counterForLikes! - 1)"
                            }
                            else {
                                self.feedCommentList[indexVal].is_like = kYes
                                self.feedCommentList[indexVal].comment_like_count = "\(counterForLikes! + 1)"
                            }
                        }
                        else {
                            let counterForLikes = Int(self.feedCommentList[selSection].reply_comment?[indexVal].comment_like_count ?? "0")
                            if self.feedCommentList[selSection].reply_comment?[indexVal].is_like == kYes {
                                self.feedCommentList[selSection].reply_comment?[indexVal].is_like = kNo
                                self.feedCommentList[selSection].reply_comment?[indexVal].comment_like_count! = "\(counterForLikes! - 1)"
                            }
                            else {
                                self.feedCommentList[selSection].reply_comment?[indexVal].is_like = kYes
                                self.feedCommentList[selSection].reply_comment?[indexVal].comment_like_count! = "\(counterForLikes! + 1)"
                            }
                        }
                        self.tblFeedDetails.reloadData()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callLikeDislikePostCommentReplyAPI(indexVal: Int, selSection: Int) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feedcommentlike[feed_id]": self.postID!,
                     "Feedcommentlike[feed_comment_id]" : self.feedCommentList[selSection].reply_comment?[indexVal].feed_comment_id ?? 0] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedCommentUserModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.LIKEDISLIKEPOSTCOMMENT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        let counterForLikes = Int(self.feedCommentList[selSection].reply_comment?[indexVal].comment_like_count! ?? "0")
                        if self.feedCommentList[selSection].reply_comment?[indexVal].is_like == kYes {
                            self.feedCommentList[selSection].reply_comment?[indexVal].is_like = kNo
                            self.feedCommentList[selSection].reply_comment?[indexVal].comment_like_count! = "\(counterForLikes! - 1)"
                        }
                        else {
                            self.feedCommentList[selSection].reply_comment?[indexVal].is_like = kYes
                            self.feedCommentList[selSection].reply_comment?[indexVal].comment_like_count! = "\(counterForLikes! + 1)"
                        }
                        self.tblFeedDetails.reloadData()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callDeleteCommentAPI(indexVal: Int, selSection: Int) {
        var param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feedcomment[feed_id]": self.postID!] as [String : Any]
        
        if selSection == -1{
            param["Feedcomment[feed_comment_id]"] = "\(self.feedCommentList[indexVal].feed_comment_id ?? 0)"
        }
        else {
            param["Feedcomment[feed_comment_id]"] = "\(self.feedCommentList[selSection].reply_comment?[indexVal].feed_comment_id ?? 0)"
        }
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.DELETECOMMENT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        if selSection == -1 {
                            //Remove whole comment section
                            self.feedCommentList.remove(at: indexVal)
                        }
                        else {
                            //Remove particular comment(row) from section
                            self.feedCommentList[selSection].reply_comment?.remove(at: indexVal)
                        }
                        self.tblFeedDetails.reloadData()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
   
    func callDeleteCommentReplyAPI(indexVal: Int, selSection: Int) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Feedcomment[feed_id]": self.postID!,
                     "Feedcomment[feed_comment_id]" : self.feedCommentList[selSection].reply_comment?[indexVal].feed_comment_id ?? 0] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedCommentUserModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.DELETECOMMENT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    if response != nil {
                        self.feedCommentList[selSection] = response!.dictData!
                        self.tblFeedDetails.reloadData()
                    }
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
     
    func callAddCommentReportAPI(indexVal: Int, selSection: Int, comment: String) {
        var param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Reportcomment[reson]": comment,
                     "Reportcomment[feed_id]": self.postID!] as [String : Any]
        
        if selSection == -1 {
            param["Reportcomment[feed_comment_id]"] = self.feedCommentList[indexVal].feed_comment_id ?? 0
        }
        else {
            param["Reportcomment[feed_comment_id]"] = self.feedCommentList[selSection].reply_comment?[indexVal].feed_comment_id!
        }
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.ADDCOMMENTREPORT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func callAddCommentReplyReportAPI(indexVal: Int, selSection: Int, comment: String) {
        let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                     "Reportcomment[reson]": comment,
                     "Reportcomment[feed_id]": self.postID!,
                     "Reportcomment[feed_comment_id]": self.feedCommentList[selSection].reply_comment?[indexVal].feed_comment_id ?? 0] as [String : Any]
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            APIManager().apiCall(of: FeedListModel.self, isShowHud: true, URL: baseURLWithAuth, apiName: APINAME.ADDCOMMENTREPORT.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                }
                else {
                    UIAlertController().alertViewWithTitleAndMessage(self, message: error!.domain)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
}

class FeedDetailsCommentCell : UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var separetorView: UIView!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var btnUserProfile: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTotalLikes: UILabel!
    @IBOutlet weak var txtViewDesc: UITextView!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var constLeadingForImgViewUser: NSLayoutConstraint!
    @IBOutlet weak var constTopForBtnReply: NSLayoutConstraint!
}

class FeedCommentViewMoreCell : UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnViewMoreReply: UIButton!
}

class FeedCommentNoDataCell : UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblEmptyText: UILabel!
}
