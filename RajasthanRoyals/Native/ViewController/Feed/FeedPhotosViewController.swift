//
//  LoginViewController.swift
//  Demo
//
//  Created by Hardy on 3/2/19.
//  Copyright © 2019 Sagar Nandha. All rights reserved.
//

import UIKit

class FeedPhotosViewController: BaseViewController, EFImageViewZoomDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imageCV: UICollectionView!

    var imagesList = [String]()
    var visibleIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.setupLayout()
        
        self.lblDescription.text = ""
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            self.imageCV.scrollToItem(at: IndexPath.init(item: self.visibleIndex, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)
        }
    }
    
    fileprivate func setupLayout() {
        if let layout = self.imageCV.collectionViewLayout as? UPCarouselFlowLayout {
            layout.itemSize = CGSize(width: self.imageCV.frame.size.width, height: self.imageCV.frame.size.height)
            layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 0)
        }
    }
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnShare_Clicked(_ sender: Any) {
        let indexPath = IndexPath.init(item: self.visibleIndex, section: 0)
        if let cell = imageCV.cellForItem(at: indexPath) as? PhotoCell {
            if let image = cell.imgGallery.image {
                let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
            }
        }
    }
    
    func showHelperCircle() {
        let center = CGPoint(x: view.bounds.width * 0.5, y: 100)
        let small = CGSize(width: 30, height: 30)
        let circle = UIView(frame: CGRect(origin: center, size: small))
        circle.layer.cornerRadius = circle.frame.width/2
        circle.backgroundColor = UIColor.white
        circle.layer.shadowOpacity = 0.8
        circle.layer.shadowOffset = CGSize()
        view.addSubview(circle)

        UIView.animate(withDuration: 0.5, delay: 0.25, options: [],
            animations: {
                circle.frame.origin.y += 200
                circle.layer.opacity = 0
        }, completion: { _ in
                circle.removeFromSuperview()
        })
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.imageCV.contentOffset
        visibleRect.size = self.imageCV.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.imageCV.indexPathForItem(at: visiblePoint) else { return }
        
        self.visibleIndex = indexPath.item
        self.lblDescription.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension FeedPhotosViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.imagesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        
        cell.imgGallery._delegate = self
        
        if let imageUrl = self.imagesList[indexPath.item] as? String {
            if let url = URL(string: imageUrl) {
                if imageUrl.contains(".gif") {
                    cell.imgGallery.imageView.setGifFromURL(url, manager: gifManager, loopCount: -1, showLoader: true)
                }
                else {
                    let imgPhoto = UIImageView()
                    imgPhoto.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"), options: nil, progressBlock: nil) { (image, error, type, url) in
                        if image != nil {
                            cell.imgGallery.image = image!
                        }
                    }
                }
            }
        }
        
        return cell
    }
}
