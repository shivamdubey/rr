//
//  AddReportViewController.swift
//  Ashton
//
//  Created by Apple on 25/12/20.
//  Copyright © 2020 Sagar Nandha. All rights reserved.
//

import UIKit

@objc protocol PassPickerDataDelegate {
    @objc optional func addSocialData(socialStr: String, linkStr: String)
    @objc optional func selectedCustomPickerData(strPicker: String, selectedIndex: Int, multiId: String)
    @objc optional func blockUserAccountData(selectedIndex: Int)
}

class AddReportViewController: BaseViewController {

    @IBOutlet weak var viewBottomReportPost: UIView!
    @IBOutlet weak var lblReportPost: UILabel!
    @IBOutlet weak var lblReportPostDesc: UILabel!
    @IBOutlet weak var txtViewReason: CustomTextView!
    @IBOutlet weak var btnReport: CustomButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var delegate: PassPickerDataDelegate?
    var delegateCustomMenu: CustomMenuDelegate?
    
    var selectedIndex = -1
    var selectedSection = -1
    var pickerShowStr = ""
    var strReport = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLocalizationText()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
        self.viewBottomReportPost.roundCorners([.topLeft,.topRight], radius: 10)
    }
    
    func setupLocalizationText() {
        self.btnReport.setTitle(self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_Report"), for: .normal)
        self.btnCancel.setTitle(self.getLocalizeTextForKey(keyName: "btn_Cancel"), for: .normal)
        
        self.lblReportPost.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportPost")
        self.lblReportPostDesc.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_ReportPostMessage")
        if strReport == REPORT_TYPE.COMMENTREPORT.rawValue {
            self.lblReportPost.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportComment")
            self.lblReportPostDesc.text = self.getLocalizeTextForKey(keyName: "SuperRoyals_lbl_ReportCommentMessage")
        }
    }
      
    @IBAction func btnReport_clicked(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.view.backgroundColor = UIColor.clear
            self.dismiss(animated: true, completion: nil)
        }
        
        if self.txtViewReason.text!.isEmpty {
            UIAlertController().alertViewWithTitleAndMessage(self, message: self.getLocalizeTextForKey(keyName: "ValidationMessage_Reason"))
        }
        else {
            if self.strReport == REPORT_TYPE.POSTREPORT.rawValue {
                self.delegateCustomMenu?.editDeleteReportFeedData?(strAction: self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportPost"), selIndex: self.selectedIndex, comment: self.txtViewReason.text!)
            }
            else if self.strReport == REPORT_TYPE.COMMENTREPORT.rawValue{
                if self.selectedSection == -1 {
                    self.delegateCustomMenu?.editDeleteReportFeedCommentSectionData?(strAction: self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportPost"), selIndex: self.selectedIndex, comment: self.txtViewReason.text!)
                }
                else {
                    self.delegateCustomMenu?.editDeleteReportFeedCommentRowData?(strAction: self.getLocalizeTextForKey(keyName: "SuperRoyals_btn_ReportPost"), selIndex: self.selectedIndex, selSection: self.selectedSection, comment: self.txtViewReason.text!)
                }
            }
        }
    }
    
    @IBAction func btnCancel_clicked(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.view.backgroundColor = UIColor.clear
            self.dismiss(animated: true, completion: nil)
        }
    }
}
