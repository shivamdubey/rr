//
//  UserDetailsModel.swift
//  RoyalFoundation
//
//  Created by Chirag on 29/12/20.
//  Copyright © 2020 hardik nandha. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserDetailsModel : Mappable {
    var auth_key : String?
    var devices_id : String?
    var appuser_id : Int?
    var first_name : String?
    var last_name : String?
    var full_name : String?
    var country_name : String?
    var phone_code : Int?
    var phone_number : String?
    var phone_verify : String?
    var email : String?
    var birth_date : String?
    var google_id : String?
    var facebook_id : String?
    var instagram_id : String?
    var twitter_id : String?
    var apple_id : String?
    var login_type : String?
    var otp : String?
    var user_status : String?
    var user_lucky_number : String?
    var lang_code : String?
    var is_whatapp : String?
    var image : String?
    var favourites : [FavouritesPlayer]?
    var points : Int?
    var prediction_points : Double?
    var match_notification_remainder : Int?
    var updated_at : String?
    var created_at : String?
    var is_verified : String?
    
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        auth_key <- map["auth_key"]
        devices_id <- map["devices_id"]
        appuser_id <- map["appuser_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        full_name <- map["full_name"]
        country_name <- map["country_name"]
        phone_code <- map["phone_code"]
        phone_number <- map["phone_number"]
        phone_verify <- map["phone_verify"]
        email <- map["email"]
        birth_date <- map["birth_date"]
        google_id <- map["google_id"]
        facebook_id <- map["facebook_id"]
        instagram_id <- map["instagram_id"]
        twitter_id <- map["twitter_id"]
        apple_id <- map["apple_id"]
        login_type <- map["login_type"]
        otp <- map["otp"]
        user_status <- map["user_status"]
        user_lucky_number <- map["user_lucky_number"]
        lang_code <- map["lang_code"]
        is_whatapp <- map["is_whatapp"]
        image <- map["image"]
        favourites <- map["favourites"]
        points <- map["points"]
        prediction_points <- map["prediction_points"]
        match_notification_remainder <- map["match_notification_remainder"]
        updated_at <- map["updated_at"]
        created_at <- map["created_at"]
        is_verified <- map["is_verified"]
    }
}

struct FavouritesPlayer : Mappable {
    var players_id : Int?
    var appuser_id : Int?
    var first_name : String?
    var last_name : String?
    var players_number : Int?
    var country_id : Int?
    var image : String?
    var country_image : String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        players_id <- map["players_id"]
        appuser_id <- map["appuser_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        players_number <- map["players_number"]
        country_id <- map["country_id"]
        image <- map["image"]
        country_image <- map["country_image"]
    }
}
