//
//  FeedCommentUserModel.swift
//  Ashton
//
//  Created by Apple on 24/12/20.
//  Copyright © 2020 Sagar Nandha. All rights reserved.
//

import Foundation
import ObjectMapper

struct FeedCommentUserModel : Mappable {
    var feed_comment_id : Int?
    var feed_id : Int?
    var appuser_id : Int?
    var comment : String?
    var parent_comment_id : String?
    var full_name : String?
    var is_like : String?
    var is_delete : String?
    var created_at : String?
    var reply_comment_count : Int?
    var comment_like_count : String?
    var reply_comment : [FeedCommentUserModel]?
    var user : UserDetailsModel?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        feed_comment_id <- map["feed_comment_id"]
        feed_id <- map["feed_id"]
        appuser_id <- map["appuser_id"]
        comment <- map["comment"]
        parent_comment_id <- map["parent_comment_id"]
        full_name <- map["full_name"]
        is_like <- map["is_like"]
        is_delete <- map["is_delete"]
        created_at <- map["created_at"]
        reply_comment_count <- map["reply_comment_count"]
        comment_like_count <- map["comment_like_count"]
        reply_comment <- map["reply_comment"]
        user <- map["user"]
    }

}
