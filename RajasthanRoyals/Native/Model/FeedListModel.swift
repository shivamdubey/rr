//
//  FeedListModel.swift
//  Ashton
//
//  Created by Apple on 22/12/20.
//  Copyright © 2020 Sagar Nandha. All rights reserved.
//

import Foundation
import ObjectMapper

struct FeedListModel : Mappable {
    var feed_id : Int?
    var bookmark_feed_id : Int?
    var appuser_id : Int?
    var description : String?
    var status : String?
    var created_at : String?
    var like_count : String?
    var comment_count : String?
    var is_bookmark : String?
    var is_like : String?
    var user : UserDetailsModel?
    var images : [FeedImages]?
    var isReadMore : Bool = false
    var collectionViewHeight : CGFloat = 250.0

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        feed_id <- map["feed_id"]
        bookmark_feed_id <- map["bookmark_feed_id"]
        appuser_id <- map["appuser_id"]
        description <- map["description"]
        status <- map["status"]
        created_at <- map["created_at"]
        like_count <- map["like_count"]
        comment_count <- map["comment_count"]
        is_bookmark <- map["is_bookmark"]
        is_like <- map["is_like"]
        user <- map["user"]
        images <- map["images"]
        
    }
    
    func getType() -> String? {
        if (images?.count ?? 0) > 0{
            return images?[0].type
        }
        return "text"
    }
}

struct FeedImages : Mappable {
    var feed_image_id : Int?
    var feed_id : Int?
    var type : String?
    var file : String?
    var thumb : String?
    var height : String?
    var width : String?
    var duration : String?
    var created_at : String?

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        feed_image_id <- map["feed_image_id"]
        feed_id <- map["feed_id"]
        type <- map["type"]
        file <- map["file"]
        thumb <- map["thumb"]
        height <- map["height"]
        width <- map["width"]
        duration <- map["duration"]
        created_at <- map["created_at"]
    }
}
