//
//  FeedLikeUserModel.swift
//  Ashton
//
//  Created by Apple on 25/12/20.
//  Copyright © 2020 Sagar Nandha. All rights reserved.
//

import Foundation
import ObjectMapper

struct FeedLikeUserModel : Mappable {
    var feed_like_id : Int?
    var feed_id : Int?
    var appuser_id : Int?
    var full_name : String?
    var is_delete : String?
    var created_at : String?
    var like_count : String?
    var user : UserDetailsModel?

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        feed_like_id <- map["feed_like_id"]
        feed_id <- map["feed_id"]
        appuser_id <- map["appuser_id"]
        full_name <- map["full_name"]
        is_delete <- map["is_delete"]
        created_at <- map["created_at"]
        like_count <- map["like_count"]
        user <- map["user"]
    }
}
