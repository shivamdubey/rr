//
//  BaseResponseModel.swift
//  RoyalFoundation
//
//  Created by Chirag on 29/12/20.
//  Copyright © 2020 hardik nandha. All rights reserved.
//

import Foundation
import ObjectMapper

struct BaseResponseModel<T> : Mappable where T:Mappable {
    var arrayData : [T]?
    var dictData : T?
    var message : String?
    var status : Int?
    var total_page : Int?
    var current_page : Int?
    var is_next_page : String?
    var overview : String?

    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        message <- map["message"]
        status <- map["status"]

        if let _ = try? map.value("data") as [T] {
           arrayData <- map["data"]
        }
        else {
           dictData <- map["data"]
        }
        total_page <- map["total_page"]
        current_page <- map["current_page"]
        is_next_page <- map["is_next_page"]
        overview <- map["overview"]
    }
}
