//
//  VideoPlay.swift
//  RR Academy
//
//  Created by Sagar Nandha
//  Copyright © 2019 Sagar's MacBookAir. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlay: UIView {
    private var url: URL!
    public var player : AVPlayer!
    private var _pauseCompletionBlock: (()->Void)? = nil
    
    init() {
        super.init(frame: CGRect.zero)
        self.initializePlayerLayer()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializePlayerLayer()
        self.autoresizesSubviews = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializePlayerLayer()
    }
    
    private func initializePlayerLayer() {
        let playerLayer = self.layer as! AVPlayerLayer
        playerLayer.backgroundColor = UIColor.black.cgColor
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
    }
    
    override public class var layerClass: Swift.AnyClass {
        get {
            return AVPlayerLayer.self
        }
    }
    
    func playVideoWithURL(url: URL) {
        self.url = url
        player = AVPlayer(url: url)
        player.addObserver(self, forKeyPath: "status", options:NSKeyValueObservingOptions.new, context:nil);
        player.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions.new, context: nil)
        player.isMuted = false
        (self.layer as! AVPlayerLayer).player = player
    }
    
    func toggleMute() {
        player.isMuted = !player.isMuted
    }
    
    func isMuted() -> Bool {
        return player.isMuted
    }
    
    func togglePlay() {
        if player.rate > 0 {
            player.pause()
        }
        else {
            player.play()
        }
    }
    
    func isPlaying() -> Bool {
        return player.rate > 0
    }
    
    func pause( completion: @escaping ()->Void ) {
        if _pauseCompletionBlock == nil {
            if self.player != nil {
                if self.player.rate == 0 {
                    completion()
                }
                else {
                    _pauseCompletionBlock = completion
                    self.player.pause()
                }
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "status" {
            if player.status == .readyToPlay {
                NSLog("READY TO PLAY")
                NSLog(url!.absoluteString)
            }
            else if player.status == .failed {
                NSLog("FAILED TO PLAY")
                self.playVideoWithURL(url: self.url)
            }
            else if player.status == .unknown {
                NSLog("FAILED TO PLAY: Status Unknown")
                self.playVideoWithURL(url: self.url)
            }
        }
        else if keyPath == "rate" {
            if player.rate == 0 && _pauseCompletionBlock != nil {
                _pauseCompletionBlock!()
                _pauseCompletionBlock = nil
            }
        }
    }
}

