//
//  BaseViewController.swift
//  eLaunch
//
//  Created by Sagar Nandha on 17/11/17.
//  Copyright © 2018 Sagar Nandha. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Kingfisher
import AVKit
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookCore
import FacebookLogin
import GoogleSignIn
import Firebase
import FirebaseAnalytics
import LinkPresentation
import Batch

@objc class BaseViewController: UIViewController, UINavigationBarDelegate, SlideMenuControllerDelegate {
    
    @IBOutlet weak var imgRRLogo: UIImageView!
    
    @objc static let sharedInstance = BaseViewController()
    
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let newStoryBoard = UIStoryboard(name: "Storyboard", bundle: nil)
    
    var metaDataDict = [String: LPLinkMetadata]()
    
    @objc let appDelegate = AppDelegate()
    
    var appLanguage = APPLANGUAGE.ENGLISH.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        else {
            // Fallback on earlier versions
        }
        
        if self.imgRRLogo != nil {
            let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnRRLogo))
            tapGesture.numberOfTapsRequired = 1
            self.imgRRLogo.isUserInteractionEnabled = true
            self.imgRRLogo.addGestureRecognizer(tapGesture)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    
    @objc func tappedOnRRLogo() {
        self.navigateToHomeScreen()
    }
    
    func getAppVersionAndBuildNo() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return version.appending("(\(build))")
    }
    
    func saveUserDetails(userDetails: [String: AnyObject]) {
        BaseViewController.sharedInstance.appDelegate.userDetails = userDetails
        
        if let authKey = userDetails[kAuthKey] as? String {
            defaults.set(authKey, forKey: kAuthKey)
            BaseViewController.sharedInstance.appDelegate.authKey = authKey
        }
        
        let data = NSKeyedArchiver.archivedData(withRootObject: userDetails)
        defaults.set(data, forKey:kUserDetails)
        defaults.synchronize()
    }
    
    func clearAllUserDataFromPreference() {
        BaseViewController.sharedInstance.appDelegate.isInSplashScreen = false
        BaseViewController.sharedInstance.appDelegate.authKey = ""
        BaseViewController.sharedInstance.appDelegate.generalSettings = [:]
        BaseViewController.sharedInstance.appDelegate.userDetails = [:]
        BaseViewController.sharedInstance.appDelegate.isEmailVerifyFirstTime = false
        BaseViewController.sharedInstance.appDelegate.isCallPointsAPI = false
        BaseViewController.sharedInstance.appDelegate.isCheckVersionFirstTimeCall = false
        BaseViewController.sharedInstance.appDelegate.webpageURL = nil
        
        var appIcon = 0
        if let tmpAppIcon = defaults.value(forKey: kAppIcon) as? Int {
            appIcon = tmpAppIcon
        }
        
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
        
        defaults.set(BaseViewController.sharedInstance.appDelegate.deviceToken, forKey: kDeviceToken)
        defaults.set(appIcon, forKey: kAppIcon)
        defaults.set(BaseViewController.sharedInstance.appLanguage, forKey: kAppLanguage)
        defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
        defaults.synchronize()
        
        //Logout From Facebook
        let loginManager = LoginManager()
        loginManager.logOut()
        
        //Logout From Gmail
        GIDSignIn.sharedInstance()?.signOut()
    }
    
    func random(digits: Int) -> String {
        var number = String()
        for _ in 1...digits {
           number += "\(Int.random(in: 1...9))"
        }
        return number
    }
    
    func removeNullValueFromDict(dict: [String : AnyObject]) -> [String : AnyObject] {
        var dictAfterRemoveNull = dict
        for key in dict.keys {
            if (dict[key] as? NSNull) != nil  {
                dictAfterRemoveNull.removeValue(forKey: key)
            }
        }
        return dictAfterRemoveNull
    }
    
    func convertStringToDictionary(text: String) -> [String: AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            }
            catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func getShortAmountSuffix(amount: Float) -> String {
        var shortenedAmount: Float = 100
        var suffix = ""
        if amount >= 10000000.0 {
            suffix = "C"
            shortenedAmount /= 10000000.0
        }
        else if amount >= 1000000.0 {
            suffix = "M"
            shortenedAmount /= 1000000.0
        }
        else if amount >= 100000.0 {
            suffix = "L"
            shortenedAmount /= 100000.0
        }
        else if amount >= 1000.0 {
            suffix = "K"
            shortenedAmount /= 1000.0
        }
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        let numberAsString = numberFormatter.string(from: NSNumber(value: shortenedAmount))

        return "\(numberAsString ?? "")\(suffix)"
    }
    
    func getMediaDuration(url: URL!) -> Float64 {
        let asset: AVURLAsset = AVURLAsset.init(url: url)
        let duration: CMTime = asset.duration
        return CMTimeGetSeconds(duration)
    }
    
    //MARK: - API Method -
    func autoLogin(isCallAnalyticsSessionAPI: Bool = false) {
        if !BaseViewController.sharedInstance.appDelegate.authKey.isEmpty {
            if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
                let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                             "Appuser[\(kDeviceId)]": UIDevice().deviceID,
                             "Appuser[\(kDeviceName)]": UIDevice().modelName,
                             "Appuser[\(kAppVersion)]": APP_VERSION,
                             "Appuser[\(kDeviceType)]": "Iphone",
                             "Appuser[install_id]": BatchUser.installationID() ?? "",
                             "Appuser[\(kDeviceTokenAPI)]": BaseViewController.sharedInstance.appDelegate.deviceToken,
                             "Appuser[current_country_name]": "",
                             "Appuser[batch_pushtoken]": BatchPush.lastKnownPushToken() ?? ""] as [String : Any]
                
                APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.AUTOLOGIN.rawValue, method: .post, parameters: param) { (response, error) in
                    if error == nil {
                        let responseData = response as! [String : AnyObject]
                        
                        if let userDetails = responseData[kData] as? [String : AnyObject] {
                            self.saveUserDetails(userDetails: userDetails)
                            
                            if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                                let editor = BatchUser.editor()
                                editor.setIdentifier(email)
                                editor.save()
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                                if userDetails[kIsUserDataStoreInCRM] as? String == kNo {
                                    self.storeUserDataOnCRM()
                                }
                                else {
                                    self.updateUserDataOnCRM()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func appInstalled() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["Appinstall[\(kDeviceId)]": UIDevice().deviceID,
                         "Appinstall[\(kDeviceName)]": UIDevice().modelName,
                         "Appinstall[\(kAppVersion)]": APP_VERSION,
                         "Appinstall[\(kDeviceType)]": "Iphone",
                         "Appinstall[\(kDeviceTokenAPI)]": BaseViewController.sharedInstance.appDelegate.deviceToken,
                         "Appinstall[batch_pushtoken]": BatchPush.lastKnownPushToken() ?? ""] as [String : Any]
            
            APIManager().apiCall(isShowHud: false, URL: baseURLWithoutAuth, apiName: APINAME.APPINSTALL.rawValue, method: .post, parameters: param) { (response, error) in
            }
        }
    }
    
    //TM
    func storeAppInstallDataOnCRM() {
        defaults.setValue(true, forKey: "isAppAlreadyLaunchedOnce")
        defaults.synchronize()
        
        let installId = BatchUser.installationID() ?? ""
        
        let param = ["app_id": "FanApp_IOS",
                     "install_id": installId,
                     "authkey": BatchAuthkey,
                     "push_token": BaseViewController.sharedInstance.appDelegate.deviceToken,
                     "device_attributes": ["os_type": "IOS", "os_version": self.getOSInfo(), "app_name": "FanApp_IOS", "app_version": self.getVersionNumber(), "device_type": UIDevice().modelName, "country": "", "city": ""]] as [String : Any]
        
        APIManager().apiCall(isShowHud: false, URL: CRM_API_BASE_URL, apiName: APINAME.APPINSTALL.rawValue, method: .post, parameters: param) { (response, error) in
            defaults.setValue(true, forKey: "isAppAlreadyLaunchedOnce")
            defaults.synchronize()
        }
    }
    
    func storeUserDataOnCRM() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            if let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String {
                let customerId = BatchUser.identifier() ?? ""
                let installId = BatchUser.installationID() ?? ""
                let isNewUser = BaseViewController.sharedInstance.appDelegate.userDetails["is_current_date_signup"] as? String ?? ""
                
                let param = ["app_id": "FanApp_IOS",
                             "custom_id": customerId,
                             "install_id": installId,
                             "authkey": BatchAuthkey,
                             "email": email,
                             "firstname": BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String ?? "",
                             "lastname": BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String ?? "",
                             "dob": BaseViewController.sharedInstance.appDelegate.userDetails[kBirthDate] as? String ?? "",
                             "phone": BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneNumber] as? String ?? "",
                             "countrycode": "\(BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneCode] as? Int ?? 0)",
                             "push_token": BaseViewController.sharedInstance.appDelegate.deviceToken,
                             "push_flag": true,
                             "isNewUser": isNewUser == kYes ? true : false,
                             "device_attributes": ["os_type": "IOS", "os_version": self.getOSInfo(), "app_name": "FanApp_IOS", "app_version": self.getVersionNumber(), "device_type": UIDevice().modelName, "country": "", "city": ""]] as [String : Any]
                
                APIManager().apiCall(isShowHud: false, URL: CRM_API_BASE_URL, apiName: APINAME.APPUSERDATA.rawValue, method: .post, parameters: param) { (response, error) in
                    if error == nil {
                        self.updateUserProfile(status: kYes, message: "")
                    }
                    else {
                        self.updateUserProfile(status: kNo, message: error?.domain ?? "")
                    }
                }
            }
        }
    }
    
    func updateUserDataOnCRM() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let customerId = BatchUser.identifier() ?? ""
            let installId = BatchUser.installationID() ?? ""
            
            let param = ["app_id": "FanApp_IOS",
                         "custom_id": customerId,
                         "install_id": installId,
                         "authkey": BatchAuthkey,
                         "push_token": BaseViewController.sharedInstance.appDelegate.deviceToken,
                         "device_attributes": ["os_type": "IOS", "os_version": self.getOSInfo(), "app_name": "FanApp_IOS", "app_version": self.getVersionNumber(), "device_type": UIDevice().modelName, "country": "", "city": ""]] as [String : Any]
            
            APIManager().apiCall(isShowHud: false, URL: CRM_API_BASE_URL, apiName: APINAME.REGISTERCUSTOMATTRIBUTE.rawValue, method: .post, parameters: param) { (response, error) in
            }
        }
    }
    
    func callRegisterCustomEventAPI() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let customerId = BatchUser.identifier() ?? ""
            let installId = BatchUser.installationID() ?? ""
            
            var favouritePlayerName = ""
            if let favourites = BaseViewController.sharedInstance.appDelegate.userDetails[kFavourites] as? [[String: AnyObject]] {
                if favourites.count > 0 {
                    if let firstName = favourites[0][kFirstName] as? String {
                        if let lastName = favourites[0][kLastName] as? String {
                            favouritePlayerName = "\(firstName) \(lastName)"
                        }
                    }
                }
            }
            
            var dateOfBirth = BaseViewController.sharedInstance.appDelegate.userDetails[kBirthDate] as? String ?? ""
            if !dateOfBirth.isEmpty {
                if let birthDate = self.convertStringToDateNew(format: "yyyy-MM-dd", strDate: dateOfBirth) {
                    dateOfBirth = self.convertDateToString(format: "MM/dd/yyyy", date: birthDate)
                }
            }
            
            let contactVariables = ["email": BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "",
                          "firstname": BaseViewController.sharedInstance.appDelegate.userDetails[kFirstName] as? String ?? "",
                          "lastname": BaseViewController.sharedInstance.appDelegate.userDetails[kLastName] as? String ?? "",
                          "dob": dateOfBirth,
                          "countrycode": "\(BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneCode] as? Int ?? 0)",
                          "phone": BaseViewController.sharedInstance.appDelegate.userDetails[kPhoneNumber] as? String ?? "",
                          "favouriteroyal": favouritePlayerName,
                          "jersyno": BaseViewController.sharedInstance.appDelegate.userDetails[kUserLuckynumber] as? String ?? "",
                          "tshirtname": BaseViewController.sharedInstance.appDelegate.userDetails[kTShirtName] as? String ?? "",
                          "gender": BaseViewController.sharedInstance.appDelegate.userDetails[kGender] as? String ?? "",
                          "country": BaseViewController.sharedInstance.appDelegate.userDetails[kCountryName] as? String ?? "",
                          "state": BaseViewController.sharedInstance.appDelegate.userDetails[kStateName] as? String ?? "",
                          "city": BaseViewController.sharedInstance.appDelegate.userDetails[kCitiesName] as? String ?? "",] as [String: Any]
            
            let param = ["app_id": "FanApp_IOS",
                         "custom_id": customerId,
                         "install_id": installId,
                         "authkey": BatchAuthkey,
                         "push_token": BaseViewController.sharedInstance.appDelegate.deviceToken,
                         "event": ["eventcategory": "Contact", "eventaction": "Signup", "eventlabel": "Update", "value": "true", "valuetype": "boolean", "contact_variables": contactVariables, "variables": contactVariables]] as [String : Any]
            
            APIManager().apiCall(isShowHud: false, URL: CRM_API_BASE_URL, apiName: APINAME.REGISTERCUSTOMEVENT.rawValue, method: .post, parameters: param) { (response, error) in
            }
        }
    }
    
    func updateUserProfile(status: String, message: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let installId = BatchUser.installationID() ?? ""
            
            let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                         "Appuser[install_id]": installId,
                         "Appuser[\(kIsUserDataStoreInCRM)]": status,
                         "Appuser[\(kIsUserDataStoreInCRMErrorMessage)]": message] as [String : Any]
            
            APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.UPDATEPROFILE.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String : AnyObject]
                    if let userDetails = responseData[kData] as? [String : AnyObject] {
                        self.saveUserDetails(userDetails: userDetails)
                    }
                }
            }
        }
    }
    
    //Update Firebase Token
    func updateFirebaseToken() {
        if !BaseViewController.sharedInstance.appDelegate.authKey.isEmpty {
            if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
                let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                "Appuser[\(kDeviceId)]": UIDevice().deviceID,
                "Appuser[\(kDeviceName)]": UIDevice().modelName,
                "Appuser[\(kAppVersion)]": APP_VERSION,
                "Appuser[\(kDeviceType)]": "Iphone",
                "Appuser[\(kDeviceTokenAPI)]": BaseViewController.sharedInstance.appDelegate.deviceToken] as [String : Any]
                
                APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.AUTOLOGIN.rawValue, method: .post, parameters: param) { (response, error) in
                    if error == nil {
                        let responseData = response as! [String : AnyObject]
                        if let userDetails = responseData[kData] as? [String : AnyObject] {
                            self.saveUserDetails(userDetails: userDetails)
                        }
                    }
                }
            }
        }
    }
    
    //Check App Update Verstion
    func checkVersion() {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = [kDeviceType: "Iphone", kAppVersion: APP_VERSION]
            APIManager().apiCall(isShowHud: false, URL: baseURLWithoutAuth, apiName: APINAME.CHECKAPPVERSION.rawValue, method: .post, parameters: param) { (response, error) in
                if error == nil {
                    let responseData = response as! [String: AnyObject]
                    if responseData[kForceUpdate] as? String == kYes {
                        let alert: UIAlertController = UIAlertController.init(title: responseData[kTitle] as? String ?? AlertViewTitle, message: responseData[kMessage] as? String ?? self.getLocalizeTextForKey(keyName: "lbl_AppUpdaetMessage"), preferredStyle: .alert)
                        alert.view.tintColor = UIColor().alertButtonColor
                        
                        let updateAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Update"), style: .default, handler: { (action) in
                            self.openUrl(urlString: APP_SHARE_ITUNES_URL)
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                                exit(0)
                            }
                        })
                        
                        alert.addAction(updateAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    else {
                        let alert: UIAlertController = UIAlertController.init(title: responseData[kTitle] as? String ?? AlertViewTitle, message: responseData[kMessage] as? String ?? self.getLocalizeTextForKey(keyName: "lbl_AppUpdaetMessage"), preferredStyle: .alert)
                        alert.view.tintColor = UIColor().alertButtonColor
                        
                        let updateAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Update"), style: .default, handler: { (action) in
                            self.openUrl(urlString: APP_SHARE_ITUNES_URL)
                        })
                        
                        let cancelAction: UIAlertAction = UIAlertAction.init(title: self.getLocalizeTextForKey(keyName: "btn_Cancel"), style: .default, handler: { (action) in
                        })
                        
                        alert.addAction(updateAction)
                        alert.addAction(cancelAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    //Analytics API
    func callTMAnalyticsAPI(category: String = "", action: String = "", label: String = "") {
        let customerId = BatchUser.identifier() ?? ""
        let instterId = BatchUser.installationID() ?? ""
        
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            let param = ["Analyticsfanapp[app_id]": "FanApp_IOS",
                         "Analyticsfanapp[custom_id]": customerId,
                         "Analyticsfanapp[install_id]": instterId,
                         "Analyticsfanapp[push_token]": BaseViewController.sharedInstance.appDelegate.deviceToken,
                         "Analyticsfanapp[event_category]": category,
                         "Analyticsfanapp[event_action]": action,
                         "Analyticsfanapp[event_label]": label,
                         "Analyticsfanapp[device_type]": "Iphone"]
            
            APIManager().apiCall(isShowHud: false, URL: baseURLWithoutAuth, apiName: APINAME.ANALYTICSFANAPP.rawValue, method: .post, parameters: param) { (response, error) in
            }
        }
    }
    
    @objc func storePushNotificationInfo(id: String, type: String, title: String, message: String) {
        if BaseViewController.sharedInstance.appDelegate.reachable.connection != .unavailable {
            if !BaseViewController.sharedInstance.appDelegate.authKey.isEmpty {
                let param = [kAuthKey: BaseViewController.sharedInstance.appDelegate.authKey,
                             "Tracknotification[id]": id,
                             "Tracknotification[type]": type,
                             "Tracknotification[title]": title,
                             "Tracknotification[message]": message]
                
                APIManager().apiCall(isShowHud: false, URL: baseURLWithAuth, apiName: APINAME.TRACKNOTIFICATION.rawValue, method: .post, parameters: param) { (response, error) in
                }
            }
        }
    }
    
    //Loyality Pionts
    func callEarnPointsAPI(param: [String: String]) {
        APIManager.sharedInstance.apiCallForPoints(isShowHud: false, URL: POINT_API_BASE_URL, apiName: "points/earn", method: .post, parameters: param) { (response, error) in
        }
    }
    
    // MARK: - Validation -
    func isValidEmail(str:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: str)
    }
    
    func isStringEmpty(str : String) -> Bool {
        return str.trimmingCharacters(in: .whitespaces).isEmpty
    }
    
    // MARK: - Date and Time Method -
    func getDateInStringFromTimeInterval(timeStamp: String) -> String {
        let dateBooking = NSDate(timeIntervalSince1970: Double(timeStamp)! / 1000.0)
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.timeZone = NSTimeZone.local
        
        let strDate = dateFormatter.string(from: dateBooking as Date)
        
        return strDate
    }
    
    func getDateTimeInStringFromTimeInterval(timeStamp: String) -> String {
        let dateBooking = NSDate(timeIntervalSince1970: Double(timeStamp)! / 1000.0)
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        
        
        let strDate = dateFormatter.string(from: dateBooking as Date)
        
        return strDate
    }
    
    func getTimeInStringFromTimeInterval(timeStamp: String) -> String {
        let dateBooking = NSDate(timeIntervalSince1970: Double(timeStamp)! / 1000.0)
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        
        let strDate = dateFormatter.string(from: dateBooking as Date)
        
        return strDate
    }
    
    func getDateTimeInStringFromTimeIntervalWithFormat(timeStamp: String,format: String) -> String {
        let dateBooking = NSDate(timeIntervalSince1970: Double(timeStamp)! / 1000.0)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone.local
        let strDate = dateFormatter.string(from: dateBooking as Date)
        return strDate
    }
    
    func convertDateToString(format: String, date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dateOfBirth = dateFormatter.string(from: date)
        return dateOfBirth
    }
    
    func convertStringToDateNew(format: String, strDate: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if strDate.isEmpty {
            return nil
        }
        else {
            if let date = dateFormatter.date(from: strDate) {
                return date
            }
            return nil
        }
    }
    
    func convertStringToDate(format: String, strDate: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if strDate.isEmpty {
            return Date()
        }
        else {
            if let date = dateFormatter.date(from: strDate) {
                return date
            }
            return Date()
        }
    }
    
    func setLeftSideImageInTextField(textField: UITextField, image: UIImage) {
        let leftImageView = UIImageView()
        leftImageView.contentMode = .center
        leftImageView.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
        leftImageView.image = image
        
        let leftView = UIView()
        
        leftView.frame = CGRect(x: 0, y: 0, width: textField.frame.size.height, height: textField.frame.size.height)
        textField.leftViewMode = .always
        textField.leftView = leftView
        
        leftView.addSubview(leftImageView)
    }
    
    func setRightSideImageInTextField(textField: UITextField, image: UIImage) {
        let rightImageView = UIImageView()
        rightImageView.contentMode = .center
        rightImageView.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
        rightImageView.image = image
        
        let rightView = UIView()
        
        rightView.frame = CGRect(x: 0, y: 0, width: textField.frame.size.height, height: textField.frame.size.height)
        textField.rightViewMode = .always
        textField.rightView = rightView
        
        rightView.addSubview(rightImageView)
    }
    
    func setNavigationBarToTransparant(navigationBar: UINavigationBar) {
        
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            }
            catch {
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
    //MARK: - Button Actions -
    @IBAction func onBackClicked(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuOpenClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        if sender.tag == 1 {
            self.slideMenuController()?.openRight()
        }
        else {
            self.slideMenuController()?.openLeft()
        }
    }
    
    @objc func navigateToWelcomScreen() {
        let loginViewController = self.newStoryBoard.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
        let navigationController: UINavigationController = UINavigationController.init(rootViewController: loginViewController)
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.isHidden = true
        
        if BaseViewController.sharedInstance.appDelegate.window == nil {
           BaseViewController.sharedInstance.appDelegate.window = UIApplication.shared.keyWindow
        }
        
        UIView.transition(with: BaseViewController.sharedInstance.appDelegate.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            BaseViewController.sharedInstance.appDelegate.window?.rootViewController = navigationController
        })
    }
    
    @objc func navigateToLoginScreen() {
        BaseViewController.sharedInstance.appDelegate.isInSplashScreen = false
        BaseViewController.sharedInstance.appDelegate.authKey = ""
        BaseViewController.sharedInstance.appDelegate.generalSettings = [:]
        BaseViewController.sharedInstance.appDelegate.userDetails = [:]
        
        let loginViewController = self.storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navigationController: UINavigationController = UINavigationController.init(rootViewController: loginViewController)
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.isHidden = true
        
        if BaseViewController.sharedInstance.appDelegate.window == nil {
           BaseViewController.sharedInstance.appDelegate.window = UIApplication.shared.keyWindow
        }
        
        UIView.transition(with: BaseViewController.sharedInstance.appDelegate.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            BaseViewController.sharedInstance.appDelegate.window?.rootViewController = navigationController
        })
    }
    
    @objc func navigateToHomeScreen() {
        BaseViewController.sharedInstance.appDelegate.isInSplashScreen = false
        
        let storyborad = UIStoryboard(name: "Main", bundle: nil)
        let homeViewController = storyborad.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        
        let leftMenuViewController = storyborad.instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
       // leftMenuViewController.homeViewController = homeViewController
        
        let rightMenuViewController = storyborad.instantiateViewController(withIdentifier: "RightMenuViewController") as! RightMenuViewController
        rightMenuViewController.homeViewController = homeViewController

        let slideMenuController = ExSlideMenuController(mainViewController: homeViewController, leftMenuViewController: leftMenuViewController, rightMenuViewController: rightMenuViewController)
        slideMenuController.delegate = self
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        let navigationController: UINavigationController = UINavigationController.init(rootViewController: slideMenuController)
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.isHidden = true
        navigationController.navigationBar.barStyle = .black

        if BaseViewController.sharedInstance.appDelegate.window == nil {
           BaseViewController.sharedInstance.appDelegate.window = UIApplication.shared.keyWindow
        }
        
        UIView.transition(with: BaseViewController.sharedInstance.appDelegate.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            BaseViewController.sharedInstance.appDelegate.window?.rootViewController = navigationController
        })
  }
    
    //MARK: - ALERT Method -
    func showAlertWithTitleAndMessage(title : String, message : String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.view.tintColor = UIColor().alertButtonColor
        alert.addAction(UIAlertAction(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .cancel, handler: nil));
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - MultiLanguage Method -
    func getLocalizeTextForKey(keyName: String) -> String {
        if let path = Bundle.main.path(forResource: BaseViewController.sharedInstance.appLanguage, ofType: "lproj") {
            if let bundle = Bundle(path: path) {
                return bundle.localizedString(forKey: keyName, value: nil, table: nil)
            }
        }
        return ""
    }
    
    func imageWith(name: String?, imageSize: CGFloat = 80, fontSize: CGFloat = 24) -> UIImage? {
        let frame = CGRect(x: 0, y: 0, width: imageSize, height: imageSize)
        let nameLabel = UILabel(frame: frame)
        nameLabel.font = UIFont.init(name: String().themeSemiBoldFontName, size: fontSize)
        nameLabel.textAlignment = .center
        nameLabel.backgroundColor = UIColor().alertButtonColor
        nameLabel.textColor = .white
        var initials = ""
        if let initialsArray = name?.components(separatedBy: " ") {
            if let firstWord = initialsArray.first {
                if let firstLetter = firstWord.first {
                    initials += String(firstLetter).capitalized
                }
            }
            if initialsArray.count > 1, let lastWord = initialsArray.last {
                if let lastLetter = lastWord.first {
                    initials += String(lastLetter).capitalized
                }
            }
        }
        else {
            return nil
        }
        nameLabel.text = initials
        UIGraphicsBeginImageContext(frame.size)
        if let currentContext = UIGraphicsGetCurrentContext() {
            nameLabel.layer.render(in: currentContext)
            let nameImage = UIGraphicsGetImageFromCurrentImageContext()
            return nameImage
        }
        return nil
    }
    
    func setIncludePackageItemList(arrItems: [[String:Any]]) -> String {
        var strWithNames:String = ""
        for i in 0..<arrItems.count {
            let dict = arrItems[i]
            if let name = dict["name"] as? String {
                if strWithNames == "" {
                    strWithNames = name
                }
                else {
                    strWithNames = strWithNames + "\n" + name
                }
                
            }
        }
        return strWithNames
    }
    
    //Add Firebase Custome Event
    func addFirebaseCustomeEvent(eventName: String, param: [String: NSObject] = [:]) {
        if param.isEmpty {
            let email = BaseViewController.sharedInstance.appDelegate.userDetails[kEmail] as? String ?? "Guest"
            let newParam = ["Email": email as NSObject, "DeviceType": "iOS" as NSObject]
            FIRAnalytics.logEvent(withName: eventName, parameters: newParam)
            AppEvents.logEvent(AppEvents.Name.init(eventName), parameters: newParam)
        }
        else {
            FIRAnalytics.logEvent(withName: eventName, parameters: param)
            AppEvents.logEvent(AppEvents.Name.init(eventName))
        }
    }
}
