//
//  APIManager.swift
//  GoHoota
//
//  Created by Sagar Nandha on 28/08/17.
//  Copyright © 2018 Sagar Nandha. All rights reserved.
//

import Foundation
import Alamofire
import KRProgressHUD
import ObjectMapper

public class APIManager {
    
    public class var sharedInstance: APIManager {
        struct Singleton {
            static let instance: APIManager = APIManager()
        }
        return Singleton.instance
    }
    
    init() {}
    
    //Normal
    func apiCall(isShowHud: Bool, URL : String, apiName : String, method: HTTPMethod, parameters : [String : Any]?, completion:@escaping (_ dict: Any?,_ error: NSError?) -> ()) {
        
        let api_url = URL + apiName
        
        var finalParameters = parameters
        if URL != CRM_API_BASE_URL {
            finalParameters?["lang"] = BaseViewController.sharedInstance.appLanguage
        }
        
        if finalParameters == nil {
            print("API URL = \(api_url)")
        }
        else {
            print("API URL = \(api_url) parameters = \(finalParameters!)")
        }
        
        if isShowHud {
            KRProgressHUD.show()
        }
        
        var alamoFireReq = Alamofire.request(api_url, method: method, parameters: finalParameters)
        if URL == CRM_API_BASE_URL {
            alamoFireReq = Alamofire.request(api_url, method: method, parameters: finalParameters, encoding: JSONEncoding.default)
        }
        
        alamoFireReq.validate().responseJSON { response in
            if isShowHud {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    KRProgressHUD.dismiss()
                }
            }
            switch response.result {
            case .success:
                if let json: NSDictionary = response.result.value as? NSDictionary {
                    print(json)
                    if json[kStatus] as? Int == kIsSuccess || json[kStatus] as? Int == kIsSuccess200 || json[kStatus] as? Int == kIsSuccess201 || json[kStatus] as? Int == kNewUser {
                        completion(json, nil)
                    }
                    else if json[kStatus] as? Int == kUserNotFound {
                        if let message = json[kMessage] as? String {
                            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                            alert.view.tintColor = UIColor().alertButtonColor
                            
                            let hideAction: UIAlertAction = UIAlertAction.init(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                BaseViewController.sharedInstance.clearAllUserDataFromPreference()
                                BaseViewController.sharedInstance.navigateToLoginScreen()
                            })
                            alert.addAction(hideAction)
                            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                        }
                    }
                    else {
                        var json_Data : [AnyHashable : Any] = [:]
                        if let jsonData = json as? [AnyHashable : Any] {
                            json_Data = jsonData
                        }
                        completion(nil, NSError.init(domain: "\(json[kMessage] ?? "")", code: json[kStatus] as? Int ?? 0, userInfo: json_Data as? [String : Any]))
                    }
                }
                else {
                    completion(nil, NSError.init(domain: ErrorMessage, code: 0, userInfo: nil))
                }
            case .failure(let error):
                print (error)
                completion(nil, NSError.init(domain: error.localizedDescription, code: 0, userInfo: nil))
            }
        }
    }
    
    //Mark: API call with image upload
    func apiCallWithImage(isShowHud: Bool, URL : String, apiName : String, parameters : [String : Any], images: [UIImage], imageParameterName: String, imageName: String, completion:@escaping (_ dict: Any?,_ error: NSError?) -> ()) {
        
        var finalParameters = parameters
        finalParameters["lang"] = BaseViewController.sharedInstance.appLanguage
        
        let api_url = URL + apiName
        print("API URL = \(api_url) parameters = \(finalParameters)")

        if isShowHud {
            KRProgressHUD.show(withMessage: "Please wait")
        }
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            if images.count > 1 {
                for index in 0...images.count - 1{
                    if let imageData = images[index].jpegData(compressionQuality: 0.6) {
                        multipartFormData.append(imageData, withName: "\(imageParameterName)\(index + 1)", fileName: "\(imageName)\(index + 1)", mimeType: "image/png")
                    }
                }
            }
            else {
                if let imageData = images[0].jpegData(compressionQuality: 0.6) {
                    multipartFormData.append(imageData, withName: imageParameterName, fileName: imageName, mimeType: "image/png")
                }
            }
            
            //Append Param
            for (key, value) in finalParameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, usingThreshold: 1, to: api_url, method: .post, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: {
                    response in
                    if isShowHud {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            KRProgressHUD.dismiss()
                        }
                    }
                    switch response.result {
                    case .success:
                        if let json: NSDictionary = response.result.value as? NSDictionary {
                            print(json)
                            if json[kStatus] as? Int == kIsSuccess {
                                completion(json,nil)
                            }
                            else if json[kStatus] as? Int == kUserNotFound {
                                if let message = json[kMessage] as? String {
                                    let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                                    alert.view.tintColor = UIColor().alertButtonColor
                                    
                                    let hideAction: UIAlertAction = UIAlertAction.init(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                        BaseViewController.sharedInstance.clearAllUserDataFromPreference()
                                        BaseViewController.sharedInstance.navigateToLoginScreen()
                                    })
                                    alert.addAction(hideAction)
                                    UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                                }
                            }
                            else {
                                completion(nil, NSError.init(domain: "\(json[kMessage] ?? "")", code: json[kStatus] as? Int ?? 0, userInfo: nil))
                            }
                        }
                        else {
                            completion(nil, NSError.init(domain: ErrorMessage, code: 0, userInfo: nil))
                        }
                    case .failure(let error):
                        print (error)
                        completion(nil, NSError.init(domain: error.localizedDescription, code: 0, userInfo: nil))
                    }
                })
            case .failure(let encodingError):
                if isShowHud {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        KRProgressHUD.dismiss()
                    }
                }
                print (encodingError)
                completion(nil, NSError.init(domain: encodingError.localizedDescription, code: 0, userInfo: nil))
            }
        })
    }
    
    //Model
    func apiCall<T:Mappable>(of type: T.Type = T.self,isShowHud: Bool, URL : String, apiName : String, method: HTTPMethod, parameters : [String : Any]?, completion:@escaping (_ dict: BaseResponseModel<T>?,_ error: NSError?) -> ()) {
        let api_url = URL + apiName
        
        var finalParameters = parameters
        finalParameters?["lang"] = BaseViewController.sharedInstance.appLanguage
        
        if parameters == nil {
            print("API URL = \(api_url)")
        }
        else {
            print("API URL = \(api_url) parameters = \(finalParameters!)")
        }
        
        if isShowHud {
            KRProgressHUD.show(withMessage: "Please wait")
        }

        Alamofire.request(api_url, method: method, parameters: finalParameters).validate().responseJSON { response in
            if isShowHud {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    KRProgressHUD.dismiss()
                }
            }
            switch response.result {
            case .success:
                let json = try! JSONSerialization.jsonObject(with: response.data!)
                print(response)

                let dataResponse = Mapper<BaseResponseModel<T>>().map(JSON: json as! [String : Any])
                
                if dataResponse?.status == kIsSuccess || dataResponse?.status == kNewUser {
                    completion(dataResponse!, nil)
                }
                else if dataResponse?.status == kUserNotFound {
                    if let message = dataResponse?.message {
                        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                        alert.view.tintColor = UIColor().alertButtonColor
                        
                        let hideAction: UIAlertAction = UIAlertAction.init(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                            BaseViewController.sharedInstance.clearAllUserDataFromPreference()
                            BaseViewController.sharedInstance.navigateToLoginScreen()
                        })
                        alert.addAction(hideAction)
                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
                else {
                    var json_Data : [AnyHashable : Any] = [:]
                    if let jsonData = json as? [AnyHashable : Any] {
                        json_Data = jsonData
                    }
                    completion(nil, NSError.init(domain: dataResponse?.message ?? "", code: dataResponse?.status ?? 0, userInfo: json_Data as? [String : Any]))
                }
            case .failure(let error):
                print (error)
                if let json = try? JSONSerialization.jsonObject(with: response.data!) as? [String : Any] {
                    let dataResponse = Mapper<BaseResponseModel<T>>().map(JSON: json)
                    completion(nil, NSError.init(domain: dataResponse?.message ?? "", code: response.response?.statusCode ?? 0, userInfo: nil))
                }
                else {
                    let isCancelCode = (response.error as NSError?)?.code == NSURLErrorCancelled
                    var code  = 0
                    if isCancelCode {
                        code = -999
                    }
                    completion(nil, NSError.init(domain: ErrorMessage, code: code, userInfo: nil))
                }
            }
        }
    }
    
    //Mark: API call with image upload
    func apiCallWithImage<T:Mappable>(of type: T.Type = T.self,isShowHud: Bool, URL : String, apiName : String, parameters : [String : Any], images: [UIImage], imageParameterName: String, imageName: String, completion:@escaping (_ dict: BaseResponseModel<T>?,_ error: NSError?) -> ()) {
        let api_url = URL + apiName
        
        var finalParameters = parameters
        finalParameters["lang"] = BaseViewController.sharedInstance.appLanguage
        
        print("API URL = \(api_url) parameters = \(finalParameters)")

        if isShowHud {
            KRProgressHUD.show(withMessage: "Please wait")
        }
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            if images.count > 1 {
                for index in 0...images.count - 1 {
                    if let imageData = images[index].jpegData(compressionQuality: 0.6) {
                        multipartFormData.append(imageData, withName: "\(imageParameterName)\(index + 1)", fileName: "\(imageName)\(index + 1)", mimeType: "image/png")
                    }
                }
            }
            else {
                if let imageData = images[0].jpegData(compressionQuality: 0.6) {
                    multipartFormData.append(imageData, withName: imageParameterName, fileName: imageName, mimeType: "image/png")
                }
            }
            
            //Append Param
            for (key, value) in finalParameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, usingThreshold: 1, to: api_url, method: .post, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: {
                    response in
                    if isShowHud {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            KRProgressHUD.dismiss()
                        }
                    }
                    switch response.result {
                    case .success:
                        let json = try! JSONSerialization.jsonObject(with: response.data!)
                        print(response)

                        let dataResponse = Mapper<BaseResponseModel<T>>().map(JSON: json as! [String : Any])
                        
                        if dataResponse?.status == kIsSuccess {
                            completion(dataResponse!, nil)
                        }
                        else if dataResponse?.status == kUserNotFound {
                            if let message = dataResponse?.message {
                                let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                                alert.view.tintColor = UIColor().alertButtonColor
                                
                                let hideAction: UIAlertAction = UIAlertAction.init(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                    BaseViewController.sharedInstance.clearAllUserDataFromPreference()
                                    BaseViewController.sharedInstance.navigateToLoginScreen()
                                })
                                alert.addAction(hideAction)
                                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                            }
                        }
                        else {
                            completion(nil, NSError.init(domain: dataResponse?.message ?? "", code: dataResponse?.status ?? 0, userInfo: nil))
                        }
                    case .failure(let error):
                        print (error)
                        //completion(nil, NSError.init(domain: ErrorMessage, code: 0, userInfo: nil))
                        if let json = try? JSONSerialization.jsonObject(with: response.data!) as? [String : Any] {
                            let dataResponse = Mapper<BaseResponseModel<T>>().map(JSON: json)
                            print (dataResponse?.message ?? "")
                            completion(nil, NSError.init(domain: dataResponse?.message ?? "", code: dataResponse?.status ?? 0, userInfo: nil))
                        }
                        else {
                            completion(nil, NSError.init(domain: error.localizedDescription, code: 0, userInfo: nil))
                        }
                    }
                })
            case .failure(let encodingError):
                if isShowHud {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        KRProgressHUD.dismiss()
                    }
                }
                print (encodingError)
                completion(nil, NSError.init(domain: encodingError.localizedDescription, code: 0, userInfo: nil))
            }
        })
    }
    
    //Mark: API call with image & video upload
    func apiCallWithImageVideoList<T:Mappable>(of type: T.Type = T.self,isShowHud: Bool, URL : String, apiName : String, parameters : [String : Any], images: [UIImage], imageParameterName: String, imageName: String, videoParameterName: String, uploadType: String, completion:@escaping (_ dict: BaseResponseModel<T>?,_ error: NSError?) -> ()) {
        let api_url = URL + apiName
        
        var finalParameters = parameters
        finalParameters["lang"] = BaseViewController.sharedInstance.appLanguage
        
        print("API URL = \(api_url) parameters = \(finalParameters)")
          
        if isShowHud {
            KRProgressHUD.show()
        }
          
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            if uploadType == FEEDTYPE.IMAGE.rawValue && images.count > 0 {
                for index in 0...images.count - 1 {
                    if let imageData = images[index].jpegData(compressionQuality: 0.7) {
                        multipartFormData.append(imageData, withName: "\(imageParameterName)[\(index)]", fileName: "\(imageName)\(index).png", mimeType: "image/png")
                    }
                }
            }
            
            //Append Param
            for (key, value) in finalParameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, usingThreshold: 1, to: api_url, method: .post, headers: nil, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: {
                response in
                if isShowHud {
                    KRProgressHUD.dismiss()
                }
                    
                switch response.result {
                case .success:
                    let json = try! JSONSerialization.jsonObject(with: response.data!)
                    print(response)
                    let dataResponse = Mapper<BaseResponseModel<T>>().map(JSON: json as! [String : Any])
                          
                    if dataResponse?.status == kIsSuccess {
                        completion(dataResponse!, nil)
                    }
                    else if dataResponse?.status == kUserNotFound {
                        if let message = dataResponse?.message {
                            let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
                            alert.view.tintColor = UIColor().alertButtonColor
                               
                            let hideAction: UIAlertAction = UIAlertAction.init(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: { (action) in
                                BaseViewController.sharedInstance.clearAllUserDataFromPreference()
                                BaseViewController.sharedInstance.navigateToLoginScreen()
                            })
                            alert.addAction(hideAction)
                            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                        }
                    }
                    else {
                        completion(nil, NSError.init(domain: dataResponse?.message ?? "", code: dataResponse?.status ?? 0, userInfo: nil))
                    }
                    case .failure(let error):
                        print (error)
                        //completion(nil, NSError.init(domain: ErrorMessage, code: 0, userInfo: nil))
                        if let json = try? JSONSerialization.jsonObject(with: response.data!) as? [String : Any] {
                            let dataResponse = Mapper<BaseResponseModel<T>>().map(JSON: json)
                            print(dataResponse!.message!)
                            completion(nil, NSError.init(domain: dataResponse?.message ?? "", code: dataResponse?.status ?? 0, userInfo: nil))
                        }
                        else {
                            completion(nil, NSError.init(domain: ErrorMessage, code: 0, userInfo: nil))
                        }
                    }
                })
            case .failure(let encodingError):
                if isShowHud {
                    KRProgressHUD.dismiss()
                }
                print (encodingError)
                completion(nil, NSError.init(domain: ErrorMessage, code: 0, userInfo: nil))
            }
        })
    }
    
    func apiCallForPoints(isShowHud: Bool, URL : String, apiName : String, method: HTTPMethod, parameters : [String : Any]?, completion:@escaping (_ dict: Any?,_ error: NSError?) -> ()) {
        let api_url = URL + apiName
        
        let headers = ["x-api-key" : HEADER_API_KEY]
        
        if parameters == nil {
            print("API URL = \(api_url) headers = \(headers)")
        }
        else {
            print("API URL = \(api_url) parameters = \(parameters!) headers = \(headers)")
        }
        
        if isShowHud {
            KRProgressHUD.show(withMessage: "Please wait")
        }
        
        var alamoFireReq = Alamofire.request(api_url, method: method, encoding: JSONEncoding.default, headers: headers)
        if method == .post {
            alamoFireReq = Alamofire.request(api_url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        }
        
        alamoFireReq.validate().responseJSON { response in
            if isShowHud {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    KRProgressHUD.dismiss()
                }
            }
            switch response.result {
            case .success:
                if let json: NSDictionary = response.result.value as? NSDictionary {
                    print(json)
                    if json[kStatus] as? Int == kIsSuccess200 {
                        completion(json, nil)
                    }
                    else {
                        var json_Data : [AnyHashable : Any] = [:]
                        if let jsonData = json as? [AnyHashable : Any] {
                            json_Data = jsonData
                        }
                        completion(nil, NSError.init(domain: "\(json[kMessage] ?? "")", code: json[kStatus] as? Int ?? 0, userInfo: json_Data as? [String : Any]))
                    }
                }
                else {
                    completion(nil, NSError.init(domain: ErrorMessage, code: 0, userInfo: nil))
                }
            case .failure(let error):
                print (error)
                completion(nil, NSError.init(domain: error.localizedDescription, code: 0, userInfo: nil))
            }
        }
    }
}
