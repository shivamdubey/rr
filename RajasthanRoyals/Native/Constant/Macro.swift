
//
//  Macro.swift
//  eLaunch
//
//  Created by Sagar Nandha on 08/07/18.
//  Copyright © 2018 Sagar Nandha. All rights reserved.
//

import UIKit
import Foundation
import SafariServices

let PAGESIZE = 10
let PAGESIZECHAT = 20
let CURRENCYSYMBOL = "₹"
let AUCTIONVALUE = "Cr"
let gifManager = SwiftyGifManager(memoryLimit: 50)
let STORYIMAGEDURATION = 3.0

//MARK: - Device Size Constants -
let IS_IPHONE4s = UIScreen.main.bounds.size.height == 480.0
let IS_IPHONE5 = UIScreen.main.bounds.size.height == 568.0
let IS_IPHONE6 = UIScreen.main.bounds.size.height == 667.0
let IS_IPHONE6P = UIScreen.main.bounds.size.height == 736.0
let IS_IPHONEX = UIScreen.main.bounds.size.height == 812.0
let IS_IPHONEXMax = UIScreen.main.bounds.size.height == 896.0
let IS_IPAD = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad

let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let screenBounds = UIScreen.main.bounds
let screenScale = UIScreen.main.scale
let screenSize = CGSize(width: screenBounds.size.width * screenScale, height: screenBounds.size.height * screenScale)

let screenWidth = screenSize.width
let screenHeight = screenSize.height

//Common DateFormatter
let dateFormateForReceive = "dd MMM yyyy"
let dateFormateForSend = "yyyy-MM-dd"

//Feed Common DateFormatter
let dateFormateForGet = "yyyy-MM-dd HH:mm:ss"
let dateFormateForDisplay = "dd MMM yyyy HH:mm"//"dd MMM yyyy HH:mm"

//App URL Scheme
let facebookScheme = "fb291064039479137" //Client Account
let googleScheme = "com.googleusercontent.apps.778906552452-jodnro7ms1msqq46dp2uufjecscqpj5j" //Client Account
let googleClientId = "778906552452-jodnro7ms1msqq46dp2uufjecscqpj5j.apps.googleusercontent.com" //Client Account
let twitterScheme = "twitterkit-Mm9CyVJAnEmtYoA1LfzMWQ" //Client Account
let twitterConsumerKey = "Mm9CyVJAnEmtYoA1LfzMWQ" //Client Account
let twitterConsumerSecret = "rdgCdQVM1Gqkrjq5caemBMXbNJDom6y6KhaeVzPiPiQ" //Client Account

//User Defaults
let defaults = UserDefaults.standard

//MARK: - API URL and Other Keys
let APP_SHARE_ITUNES_URL = "https://apps.apple.com/in/app/rajasthan-royals/id1460354209"
let APP_SHARE_GOOGLE_URL = "http://play.google.com/store/apps/details?id=com.rajasthanroyals.app"
let GiphyAPIKey = "ztphOmapYyQ2P1dzRjQHHrkIq9i3UYCS"

//App Version
let APP_VERSION = "13"

//Alert Title And Messages
var AlertViewTitle = "Rajasthan Royals"
var NoInternet = "Sorry, no Internet connectivity detected. Please reconnect and try again"
var ErrorMessage = "Oops something went wrong, please try again later"

//Base URL
let SOCKET_URL = "http://3.6.4.57:2030"
let BASE_URL = "http://rrdev.fanextm.com/adminpanel" //"http://rrdev.fanextm.com/adminpanel" //Existing "https://www.rajasthanroyals.com/adminpanel/" //Live
let API_BASE_URL = "\(BASE_URL)api/"

let baseURLWithoutAuth = "\(API_BASE_URL)beforeauth/"
let baseURLWithAuth = "\(API_BASE_URL)userauth/"

//CRM API URL
let CRM_API_BASE_URL = "http://rrdev.fanextm.com/api/v1/" //Live
let BatchAuthkey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbnRlcnByaXNlX2lkIjoyNDQsImlhdCI6MTYxNjkwODQ2MX0.JzyrzZtZtbttQ9v0I4a5_Sp8lk0_CAHcsDOSVHKeAQw"

//Aqilliz
let POINT_API_BASE_URL = "https://rrdev.fanextm.api.staging.aqilliz.com/v1/" // "https://rrdev.fanextm.api.staging.aqilliz.com/v1/" //"https://rajasthanroyals.api.staging.aqilliz.com/v1/" //Live
let HEADER_API_KEY = "oRm4v5euN94TkYdRCUiqy42NIlY4XN4o6JUP3OxC" //Live

//PayU Payment Gateway Details
let merchantKey  = "9jTZCY"
let merchantId   = "134573"
let merchantSalt = "ggTLX3bE"

//Segment Button Colors
let SegmentViewBGColor = UIColor.white
let SelectedButtonBGColor = UIColor().themeDarkPinkColor
let DeSelectedButtonBGColor = UIColor.clear
let SelectedButtonTextColor = UIColor.white
let DeSelectedButtonTextColor = UIColor.lightGray

//MARK: - Extension
enum GradientColorDirection {
    case vertical
    case horizontal
}

extension UIView {
    func setShadowOnView() {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 3.0
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = false
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }

    func showGradientColors(_ colors: [UIColor], opacity: Float = 1, direction: GradientColorDirection = .vertical) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.opacity = opacity
        gradientLayer.colors = colors.map { $0.cgColor }

        if case .horizontal = direction {
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        }

        gradientLayer.bounds = self.bounds
        gradientLayer.anchorPoint = CGPoint.zero
        self.layer.addSublayer(gradientLayer)
    }
}

extension UIViewController {
    func getAppName() -> String {
        let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
        return appName
    }
    
    func getOSInfo() -> String {
        let os = ProcessInfo().operatingSystemVersion
        return String(os.majorVersion) + "." + String(os.minorVersion) + "." + String(os.patchVersion)
    }
    
    func getVersionNumber() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as? String ?? ""
//        let build = dictionary["CFBundleVersion"] as! String
        return version
    }
    
    func addStatusBarBackgroundView() -> Void {
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size:CGSize(width: SCREEN_WIDTH, height: UIApplication.shared.statusBarFrame.height))
        let view : UIView = UIView.init(frame: rect)
        view.backgroundColor = UIColor().alertButtonColor
        self.view.addSubview(view)
    }
    
    func setShadowOfViewWithBorder(view : UIView, borderColor : UIColor) {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 5.0
        
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 2.0
        view.layer.masksToBounds = false
    }
    
    func setShadowOfViewWithOutBorder(view : UIView, isWithCornerRadius : Bool) {
        if isWithCornerRadius {
            view.layer.cornerRadius = 5.0
        }
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 2.0
        view.layer.masksToBounds = false
    }
    
    func setBorderAndRadius(radius : CGFloat, view : UIView, borderWidth : CGFloat, borderColor : UIColor) {
        view.layer.cornerRadius = radius
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.masksToBounds = true
    }
    
    func saveUploadedImageToDocumentDirectory(imgUrl: String, image: UIImage) {
    }
    
    func convertToBlackAndWhite(imge: UIImage) -> UIImage {
        let ciImage = CIImage(image: imge)
        let blackImage = ciImage?.applyingFilter("CIColorControls", parameters: [kCIInputSaturationKey: 0.0])
        return UIImage(ciImage: blackImage!)
    }
    
    func openUrl(urlString: String) {
        if urlString.trim().isEmpty {
            return
        }
        
        guard let url = URL(string: urlString) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func openUrlInSafariViewController(urlString: String) {
        if urlString.trim().isEmpty {
            return
        }
        
        guard let url = URL(string: urlString) else {
            return //be safe
        }
        
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = true

        let vc = SFSafariViewController(url: url, configuration: config)
        self.present(vc, animated: true)
    }
    
    func removeFileFromDocumentDirectory(folderName: String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = documentsDirectory.appendingPathComponent(folderName)
        
        do {
            try FileManager.default.removeItem(at: fileURL)
            print("Files removed successfully.")
        }
        catch {
            print("Error while remove files:", error.localizedDescription)
        }
    }
    
    func secondsToHoursMinutesSeconds(seconds: Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}

extension UIPopoverPresentationController {
    var dimmingView: UIView? {
        return value(forKey: "_dimmingView") as? UIView
    }
}

extension UITableView {
    //set the tableHeaderView so that the required height can be determined, update the header's frame and set it again
    func setAndLayoutTableHeaderView(header: UIView) {
        self.tableHeaderView = header
        header.setNeedsLayout()
        header.layoutIfNeeded()
        header.frame.size = header.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        self.tableHeaderView = header
    }
}

extension UIApplication {
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        return viewController
    }
}

extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 0.5, height: 0.5)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func imageWithColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

//Get Font Family Names
func getFontFamilyName() {
    for family: String in UIFont.familyNames {
        print("\(family)")
        for names: String in UIFont.fontNames(forFamilyName: family) {
            print("== \(names)")
        }
    }
}

//Set Colors
extension UIColor {
    var themeDarkPinkColor: UIColor {
        return UIColor(hex: "EA1985")
    }
    
    var themeColor: UIColor {
        return UIColor(hex: "0F5499")
    }
    
    var themeButtonColor: UIColor {
        return UIColor(hex: "0F5499")
    }
    
    var alertButtonColor: UIColor {
        return UIColor(hex: "EA1885")
    }
    
    var blueBgColor1: UIColor {
        return UIColor(hex: "219EFF")
    }
    
    var blueBgColor2: UIColor {
        return UIColor(hex: "66C4FF")
    }
    
    var blueBgColor3: UIColor {
        return UIColor(hex: "CEECFF")
    }
    
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

extension Dictionary {
    func jsonString() -> NSString? {
        let jsonData = try? JSONSerialization.data(withJSONObject: self, options: [])
        guard jsonData != nil else {return nil}
        let jsonString = String(data: jsonData!, encoding: .utf8)
        guard jsonString != nil else {return nil}
        return jsonString! as NSString
    }
}

extension Double {
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    private var formatter: DateComponentsFormatter {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        return formatter
    }
    
    func secondsToString() -> String {
        return formatter.string(from: self) ?? ""
    }
}

//Set Font Name
extension String {
    var themeRegularFontName : String {
        return "Futura"
    }

    var themeMediumFontName : String {
        return "Futura-Medium"
    }
    
    var themeSemiBoldFontName : String {
        return "Futura-Bold"
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    func replace(_ string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: .literal
            , range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(" ", replacement: "")
    }
    
    var html2Attributed: NSAttributedString? {
        do {
            guard let data = data(using: String.Encoding.utf8) else {
                return nil
            }
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width + 25)
    }
    
    func convertToDictionary() -> [String: AnyObject]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String : AnyObject]
            }
            catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func setStrikeLine() -> NSMutableAttributedString {
        let attributeString = NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }

    func toDate(withFormat format: String = "yyyy-MM-dd") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        guard let date = dateFormatter.date(from: self) else {
            preconditionFailure("Take a look to your format")
        }
        return date
    }
    
    //MARK:- Convert UTC To Local Date by passing date formats value
    func UTCToLocal(incomingFormat: String, outGoingFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = outGoingFormat
        
        return dateFormatter.string(from: dt ?? Date())
    }
    
    //MARK:- Convert Local To UTC Date by passing date formats value
    func localToUTC(incomingFormat: String, outGoingFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = outGoingFormat
        
        return dateFormatter.string(from: dt ?? Date())
    }
    
    func toDate(dateFormat: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        if let date = dateFormatter.date(from: self) {
            return date
        }
        return nil
    }
    
    var validURL: Bool {
        get {
            let regEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
            let predicate = NSPredicate(format: "SELF MATCHES %@", argumentArray: [regEx])
            return predicate.evaluate(with: self)
        }
    }
    
    func convertToHindi() -> String {
        if BaseViewController.sharedInstance.appLanguage == APPLANGUAGE.ENGLISH.rawValue {
            return self
        }
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en")
        guard let number = formatter.number(from: self) else {
            return ""
        }
        formatter.locale = Locale(identifier: "hi")
        guard let number2 = formatter.string(from: number) else {
            return ""
        }
        return number2
    }
    
    var hindiToEnglishDigits: String {
        let farsiNumbers = ["०": "0","१": "1","२": "2","३": "3","४": "4","५": "5","६": "6","७": "7","८": "8","९": "9"]
        var txt = self
        farsiNumbers.map { txt = txt.replacingOccurrences(of: $0, with: $1)}
        return txt
    }
    
    var englishToHindiDigits: String {
        if BaseViewController.sharedInstance.appLanguage == "en" {
            return self
        }
        let englishNumbers = ["0": "०","1": "१","2": "२","3": "३","4": "४","5": "५","6": "६","7": "७","8": "८","9": "९"]
        var txt = self
        englishNumbers.map { txt = txt.replacingOccurrences(of: $0, with: $1)}
        return txt
    }
}

extension UITextView {
    var maxNumberOfLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let text = (self.text ?? "") as NSString
        let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font as Any], context: nil).height
        let lineHeight = font!.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
    
    var numberOfVisibleLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let textHeight = sizeThatFits(maxSize).height
        let lineHeight = font!.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
    
    func sizeFit(width: CGFloat) -> CGSize {
        let fixedWidth = width
        let newSize = sizeThatFits(CGSize(width: fixedWidth, height: .greatestFiniteMagnitude))
        return CGSize(width: fixedWidth, height: newSize.height)
    }
    
    func numberOfLine() -> Int {
        let size = self.sizeFit(width: self.bounds.width)
        let numLines = Int(size.height / (self.font?.lineHeight ?? 1.0))
        return numLines
    }
}

//Add Alerts
extension UIAlertController {
    func alertViewWithTitleAndMessage(_ viewController: UIViewController, message: String) -> Void {
        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
        alert.view.tintColor = UIColor().alertButtonColor
        let hideAction: UIAlertAction = UIAlertAction.init(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: nil)
        alert.addAction(hideAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func alertViewWithErrorMessage(_ viewController: UIViewController) {
        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: ErrorMessage, preferredStyle: .alert)
        alert.view.tintColor = UIColor().alertButtonColor
        let hideAstion: UIAlertAction = UIAlertAction.init(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: nil)
        alert.addAction(hideAstion)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func alertViewWithNoInternet(_ viewController: UIViewController) {
        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: NoInternet, preferredStyle: .alert)
        alert.view.tintColor = UIColor().alertButtonColor
        let hideAstion: UIAlertAction = UIAlertAction.init(title: BaseViewController.sharedInstance.getLocalizeTextForKey(keyName: "btn_Ok"), style: .default, handler: nil)
        alert.addAction(hideAstion)
        viewController.present(alert, animated: true, completion: nil)
    }
}

//Convert Device Token to String
extension Data {
    func hexString() -> String {
        var bytesPointer: UnsafeBufferPointer<UInt8> = UnsafeBufferPointer(start: nil, count: 0)
        self.withUnsafeBytes { (bytes) in
            bytesPointer = UnsafeBufferPointer<UInt8>(start: UnsafePointer(bytes), count:self.count)
        }
        
        let hexBytes = bytesPointer.map { return String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        var result: String = ""
        if days(from: date)    > 0 { result = result + " " + "\(days(from: date)) D" }
        if hours(from: date)   > 0 { result = result + " " + "\(hours(from: date)) H" }
        if minutes(from: date) > 0 { result = result + " " + "\(minutes(from: date)) M" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))" }
        return ""
    }
    
    // Returns the a custom time interval description from another date
    func offsetnew(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))m"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "Just Now" }
        return ""
    }
    
    func toLocalTime() -> Date {
        let timeZone = NSTimeZone.local
        let seconds: TimeInterval = Double(timeZone.secondsFromGMT(for: self as Date))
        let localDate = Date(timeInterval: seconds, since: self as Date)
        return localDate
    }
    
    func getDayOfWeek(_ today:String, tag: Int) -> Int? {
        let formatter = DateFormatter()
        if tag == 0 {
            formatter.dateFormat = "yyyy-MM-dd HH:mm a"
        }
        else {
            formatter.dateFormat = "yyyy-MM-dd"
        }
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    func daySuffix(from date: Date) -> String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        switch dayOfMonth {
        case 1, 21, 31: return "st"
        case 2, 22: return "nd"
        case 3, 23: return "rd"
        default: return "th"
        }
    }
    
    func toString() -> String {
        //March 15, 2017 at 5.30 PM
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy hh:mm a"
        return dateFormatter.string(from: self)
    }
    
    func convertToSpacificTimeZone(timeZone: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.init(identifier: timeZone)
        let strCurrentDate = dateFormatter.string(from: self)
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let currentDate = dateFormatter1.date(from: strCurrentDate) {
            return currentDate.toLocalTime()
        }
        return self
    }
}

extension UIDevice {
    var deviceID : String {
        return UIDevice.current.identifierForVendor!.uuidString
    }
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPhone11,2":                              return "iPhone XS"
        case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
        case "iPhone11,8":                              return "iPhone XR"
        case "iPhone12,1":                              return "iPhone 11"
        case "iPhone12,3":                              return "iPhone 11 Pro"
        case "iPhone12,5":                              return "iPhone 11 Pro Max"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
        case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
        case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
        case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
        case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
        case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
        case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}

extension UIImage {
    func makeFixOrientation() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage;
    }
    
    func imageByCroppingImage(_ size : CGSize) -> UIImage {
        let refWidth : CGFloat = CGFloat(self.cgImage!.width)
        let refHeight : CGFloat =  CGFloat(self.cgImage!.height)
        
        let x = (refWidth - size.width) / 2
        let y = (refHeight - size.height) / 2
        
        let cropRect = CGRect(x: x, y: y, width: size.width, height: size.height)
        let imageRef = self.cgImage!.cropping(to: cropRect)
        
        let cropped : UIImage = UIImage(cgImage: imageRef!, scale: 0, orientation: self.imageOrientation)
        
        return cropped
    }
}

extension UILabel {
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
}

extension UISegmentedControl {
    func selectedSegmentTintColor(_ color: UIColor) {
        self.setTitleTextAttributes([.foregroundColor: color], for: .selected)
    }
    func unselectedSegmentTintColor(_ color: UIColor) {
        self.setTitleTextAttributes([.foregroundColor: color], for: .normal)
    }
    func removeBorders() {
        setBackgroundImage(imageWithColor(color: backgroundColor ?? .clear), for: .normal, barMetrics: .default)
        setBackgroundImage(imageWithColor(color: tintColor!), for: .selected, barMetrics: .default)
        setDividerImage(imageWithColor(color: UIColor.clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }

    // create a 1x1 image with this color
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width:  1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
}

extension UISearchBar {
    func getTextField() -> UITextField? { return value(forKey: "searchField") as? UITextField }
    func setText(color: UIColor) { if let textField = getTextField() { textField.textColor = color } }
    func setPlaceholderText(color: UIColor) { getTextField()?.setPlaceholderText(color: color) }
    func setClearButton(color: UIColor) { getTextField()?.setClearButton(color: color) }
    
    func setTextField(color: UIColor) {
        guard let textField = getTextField() else { return }
        switch searchBarStyle {
        case .minimal:
            textField.layer.backgroundColor = color.cgColor
            textField.layer.cornerRadius = 6
        case .prominent, .default: textField.backgroundColor = color
        @unknown default: break
        }
    }
    
    func setSearchImage(color: UIColor) {
        guard let imageView = getTextField()?.leftView as? UIImageView else { return }
        imageView.tintColor = color
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
    }
}

extension UITextField {
    private class ClearButtonImage {
        static private var _image: UIImage?
        static private var semaphore = DispatchSemaphore(value: 1)
        static func getImage(closure: @escaping (UIImage?)->()) {
            DispatchQueue.global(qos: .userInteractive).async {
                semaphore.wait()
                DispatchQueue.main.async {
                    if let image = _image { closure(image); semaphore.signal(); return }
                    guard let window = UIApplication.shared.windows.first else { semaphore.signal(); return }
                    let searchBar = UISearchBar(frame: CGRect(x: 0, y: -200, width: UIScreen.main.bounds.width, height: 44))
                    window.rootViewController?.view.addSubview(searchBar)
                    searchBar.text = "txt"
                    searchBar.layoutIfNeeded()
                    _image = searchBar.getTextField()?.getClearButton()?.image(for: .normal)
                    closure(_image)
                    searchBar.removeFromSuperview()
                    semaphore.signal()
                }
            }
        }
    }
    
    func setClearButton(color: UIColor) {
        ClearButtonImage.getImage { [weak self] image in
            guard   let image = image,
                let button = self?.getClearButton() else { return }
            button.imageView?.tintColor = color
            button.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }
    
    func setPlaceholderText(color: UIColor) {
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder! : "", attributes: [.foregroundColor: color])
    }
    
    func getClearButton() -> UIButton? { return value(forKey: "clearButton") as? UIButton }
}

extension NSMutableAttributedString {
    public func setAsLink(textToFind: String, linkURL: String) -> Bool {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.foregroundColor, value: UIColor.white, range: foundRange)
            self.addAttribute(.underlineColor, value: UIColor.white, range: foundRange)
            return true
        }
        return false
    }
}

func checkIfDatabaseExists(path: String) -> Bool {
    let fileManager: FileManager = FileManager.default
    let isDBExists: Bool = fileManager.fileExists(atPath: path)
    return isDBExists
}

//MARK: - ENUMS
enum MATCHSTATUS : String {
    case LIVE = "Live"
    case UPCOMING = "Upcoming"
    case PAST = "Past"
}

enum APPLANGUAGE : String {
    case ENGLISH = "en"
    case HINDI  = "hi"
}

enum VERTICALMENU_SIZE : CGFloat {
    case WIDTH = 130
    case HEIGHT = 44
}

enum ITEMTYPE : String {
    case News = "News"
    case Video = "Video"
    case Banner = "Banner"
    case Podcast = "Podcast"
    case Players = "Players"
    case Gallery = "Mediagallerygroup"
    case Royal = "Royals in a minute"
    case Matches = "Matches"
    case Story = "Story"
    case Image = "Image"
}

enum PRESSTYPE : String {
    case Articles = "Articles"
    case Clippings = "Clippings"
    case Online = "Online"
}

enum REDIRECTTYPE : String {
    case Tabbar = "Tabbar"
    case Back = "Back"
    case Menu = "Menu"
}

enum VIDEOTYPE : String {
    case LOCAL                    = "Local"
    case YOUTUBE                  = "YouTube"
    case OTHER                    = "Other"
}

enum FEEDTYPE: String {
    case TEXT                           = "Text"
    case IMAGE                          = "Image"
    case GIF                            = "Gif"
    case VIDEO                          = "Video"
    case AUDIO                          = "Audio"
    case WEBURL                         = "Web_url"
}

enum REPORT_TYPE: String {
    case POSTREPORT                     = "PostReport"
    case COMMENTREPORT                  = "CommentReport"
}

enum APINAME : String {
    //Before Auth API
    case LOGIN                    = "login"
    case SOCIALLOGIN              = "sociallogin"
    case FORGOTPASSWORD           = "forgotpassword"
    case SIGNUP                   = "usersignup"
    case GENERALSETTING           = "generalsetting"
    case TRACKNOTIFICATION        = "tracknotification"
    case AUTOLOGIN                = "autologin"
    case OTPVERIFY                = "otpverify"
    case RESENDOTP                = "resendotp"
    case LOGOUT                   = "logout"
    case APPSETTING               = "appsetting"
    case GETMATCHLIST             = "getmatcheslist"
    case GETMATCHESDETAIL         = "getmatchesdetail"
    case GETRRALLMATCH            = "getrrallmatch"
    case GETHOMESCREENNEW         = "gethomescreennew"
    case GETTEAMLIST              = "geteamlist"
    case GETNEWSLIST              = "getnewslist"
    case GETNEWSDETAILS           = "getnewsdetails"
    case GETPRESSCLIPPINGLIST     = "getpressnotelist"
    case GETPRESSARTICLESLIST     = "getpressreleaselist"
    case GETPRESSARTICLESDETAILS  = "getpressreleasedetails"
    case GETONLINEPRESS           = "getonlinepress"
    case CONTACTUS                = "contactus"
    case FEEDBACK                 = "feedback"
    case FOUNDATIONCONTACTUS      = "foundationcontactus"
    case GETPLAYERLIST            = "getplayerlist"
    case GETPLAYERDETAILS         = "getplayerdetails"
    case GETPLAYERVIDEO           = "getplayervideo"
    case GETPLAYERIMAGE           = "getplayerimage"
    case GETPWALLPAPER            = "getwallpaper"
    case UPDATEPROFILE            = "updateprofile"
    case SENDMEDIA                = "sendmedia"
    case CHATLIST                 = "chatlist"
    case GETFAQLIST               = "getfaqlist"
    case GETVIDEOLIST             = "getvideolist"
    case GETGALLARYGROUP          = "getgallarygroup"
    case GETGALLARY               = "getgallary"
    case GETMATCHHIGHLIGHTS       = "getmatchhighlights"
    case GETMANOFTHEMATCH         = "getmanofthematch"
    case GETNOTIFICATIONLIST      = "getnotificationlist"
    case GETSCOREAPI              = "getscoreapi"
    case CHECKAPPVERSION          = "checkappversion"
    case CHANGEPASSWORD           = "changepassword"
    case FAVOURITEPLAYER          = "favouriteplayer"
    case EMAILVERIFY              = "emailverify"
    case EMAILOTPVERIFY           = "emailotpverify"
    case GETRRALLPASTMATCH        = "getrrallpastmatch"
    case REDEEMPOINTS             = "redeempoints"
    case UPDATEREDEEMPOINTSSTATUS = "updateredeempointsstatus"
    case GETSTATE                 = "getindianstates"
    case GETCITY                  = "getindiancities"
    case GETMYROYALSBENEFITS      = "getmyroyalsbenefits"
    case RATING                   = "rating"
    case GETSTATES                = "getstates"
    case GETCITIES                = "getcities"
    case APPUSERCOUNTRIESWISEUSER = "appusercountrieswiseuser"
    
    //Super Royals
    case GETSUPERROYALSHOMESCREEN = "getsuperroyalhomescreen"
    case GETSUPERROYALSVIDEO      = "getsuperroyalvideo"
    case GETSUPERROYALSINSTAGRAM  = "getsuperroyalinstagram"
    case GETSUPERROYALS           = "getsuperroyal"
    case GETSUPERROYALFOCUSDETAILS = "getsuperroyalfocusdetail"
    
    //Prediction API
    case GETPREDICTIONMATCHLIST             = "getpredictionmatch"
    case GETPREDICTIONQUESTIONCATEGORY      = "getpredictionquestioncategory"
    case GETPREDICTIONQUESTIONANSWER        = "getpredictionquestionanswer"
    case GETPREDICTIONQUESTIONANSWERDETAILS = "getpredictionquestionanswerdetails"
    case SETPREDICTIONANSWER                = "setpredictionanswer"
    case GETPREDICTIONQUESTIONLEADERBOARD   = "getpredicitionleaderboard"
    case GETPREDICTIONMATCHLEADERBOARD      = "getpredicitionmatchleaderboard"
    case GETPREDICTIONFAQ                   = "getpredictionfaq"
    case GETLOYALITYPOINTFAQ                = "getloyalitypointfaq"
    
    //Auction Player API
    case GETAUCTIONROULS          = "getauctionrouls"
    case GETAUCTIONPLAYER         = "getauctionplayer"
    case GETAUCTIONFAQ            = "getauctionfaq"
    case SETAUCTIONUSERANSWER     = "setauctionuseranswer"
    case GETMYACTIONTEAM          = "getmyauctionteam"
    case GETACTIONPLAYERLEADERBOARD = "getauctionplayerleaderboard"
    
    //Live Score New API
    case GETLIVESCORE             = "get-rrlive-score"
    case GETLIVECOMMENTARY        = "get-rrlive-commentary"
    
    case GETPLAYERSTATS           = "getplayerstats"
    case GETPODCAST               = "getpodcast"
    case GETPODCASTDETAIL         = "getpodcastdetail"
    case GETAPPFOUNDATION         = "getappfoundation"
    case GETSTADIUM               = "get-stadium"
    case GETPAVILLIONDATA         = "getpaviliondata"
    
    //Send Message
    case SENDMESSAGE              = "sendmessage"
    case DELETEMESSAGE            = "deletemessage"
    
    //ANALYTICS
    case ANALYTICSFANAPP          = "analyticsfanapp"
    
    //Socket API
    case SOCKET_ADDUSER           = "add user"
    case SOCKET_SENDMESSAGE       = "send message"
    case SOCKET_DELETEMESSAGE     = "delete message"
    
    //Feed API
    case GETFEEDLIST                = "getfeedlist"
    case CREATEPOST                 = "addfeed"
    case GETMYFEEDLIST              = "getmyfeed"
    case GETBOOKMARKFEEDLIST        = "getbookmarkfeedlist"
    case LIKEDISLIKEPOST            = "addfeedlike"
    case DELETEPOST                 = "deletefeed"
    case BOOKMARKPOST               = "addbookmarkfeed"
    case ADDPOSTREPORT              = "addreportfeed"
    case GETPOSTLIKEUSER            = "getfeedlikelist"
    case GETPOSTCOMMENTS            = "getfeedcommentlist"
    case ADDCOMMENT                 = "addfeedcomment"
    case DELETECOMMENT              = "deletefeedcomment"
    case LIKEDISLIKEPOSTCOMMENT     = "addfeedcommentlike"
    case ADDCOMMENTREPORT           = "addreportfeedcomment"
    case GETPOSTCOMMENTREPLY        = "getfeedreplycommentlist"
    case GETPOSTDETAILS             = "getfeeddetails"
    
    //Housie
    case GETHOUSIEQUIZEVENTGENERAL  = "gethousiequizeventgeneral"
    case GETHOUSIEMATCH             = "gethousiematch"
    case GETHOUSIEQUIZEVENTFAQ      = "gethousiequizeventfaq"
    case CREATERANDOMHOUSIEQUIZ     = "createrandomhousiequiz"
    case USERPLAYHOUSIEQUIZ         = "userplayhousiequiz"
    case DECLAREHOUSIEQUIZRESULTS   = "declarehousiequizresults"
    case GETHOUSIEQUIZEVENT         = "gethousiequizevent"
    
    //All Fixture
    case GETALLFIXTUREMATCH         = "getallfixturematch"
    
    //Stories
    case GETSTORY                   = "getfoundationstory"
    case GETMYSTORY                 = "getmyfoundationstory"
    case DELETESTORY                = "deletefoundationstory"
    case CREATESTORY                = "addfoundationstory"
    case STORIEVIEW                 = "viewfoundationstory"
    case GETMYSTORIEVIEWS           = "getviewfoundationstory"
    
    //RRQuiz API
    case GETQUIZBANNER                      = "getquizbanner"
    case GETQUIZAPPGENERAL                  = "getquizappgeneral"
    case GETQUIZAPPCATEGORY                 = "getquizappcategory"
    case ADDUPDATEQUIZAPPTIME               = "addupdatequizapptime"
    case GETQUIZAPPQUESTION                 = "getquizappquestion"
    case SETQUIZAPPANSWER                   = "setquizappuseranswer"
    case GETQUIZAPPUSERANSWERLEADERBOARD    = "getquizappuseranswerleaderboard"
    case GETQUIZAPPFAQ                      = "getquizappfaq"
    
    //Fans Corner
    case GETFANCORNER               = "getfancorner"
    case ADDFANCORNER               = "addfancorner"
    case GETFANCORNERDETAILS        = "getfancornerdetails"
    case ADDFANCORNERLIKE           = "addfancornerlike"
    
    //CRM API
    case APPINSTALL                 = "appinstall"
    case APPUSERDATA                = "appuserdata"
    case REGISTERCUSTOMATTRIBUTE    = "registercustomattribute"
    case REGISTERCUSTOMEVENT        = "registercustomevent"
}

enum POINTSEVENTTYPE : String {
    case SOCIAL_REGISTER            = "social_register"
    case NORMAL_REGISTER            = "normal_register"
    case SHARE_CUSTOMISED_JERSEY    = "share_customised_jersey"
    case CREATE_PROFILE             = "creating_profile"
    case VIDEOS                     = "videos"
    case GALLERY_VIEW               = "gallery_view"
    case GALLERY_SHARE              = "gallery_share"
    case LATEST_NEWS                = "latest_news"
    case PODCAST                    = "podcast"
    case PREDICTOR_GAME_PLAY        = "predictor_game_play"
    case FEED_POST                  = "feed_post"
    case FEED_LIKE                  = "feed_like"
    case FEED_COMMENT               = "feed_comment"
    case HOUSE_PLAY                 = "housie_play"
    case AR_FILTER_SHARE            = "ar_filters_share"
    case STORIES_CREATE             = "stories_create"
    case BONUS_POINTS               = "bonus_points"
    case QUIZ_COMPLETE              = "quiz_complete"
    case QUIZ_SHARE                 = "quiz_share"
    case SECRET_AUCTION             = "secret_auction"
}

//MARK: - API Params -
let kStatus = "status"
let kIsSuccess = 1
let kIsSuccess200 = 200
let kIsSuccess201 = 201
let kNewUser = 2
let kIsFailure = 0
let kUserNotFound = 3
let kRecordNotFound = 411
let kFailureCancelCode = -999
let kForceUpdate = "force_update"

let kY = "Y"
let kN = "N"

var kYes = "Yes"
var kNo = "No"

let kMessage = "message"
let kData = "data"
let kPage = "page"
let kPageSize = "pagesize"
let kIsNextPage = "is_next_page"
let kUserDetails = "userDetails"
var kOk = "Ok"
let kAppIcon = "AppIcon"
let kAppLanguage = "AppLanguage"

let kType = "type"
let kSubType = "sub_type"
let kMediaType = "media_type"
let kVideoType = "video_type"
let kDeviceToken = "DeviceToken"
let kMatchStatus = "matche_status"
let kUpcomingMatch = "upcomingmatch"
let kUpcomingMatchNew = "upcomingmatch_new"
let kIsBirthdate = "is_birthdate"
let kIsUserDataStoreInCRM = "is_user_data_store_in_crm"
let kIsUserDataStoreInCRMErrorMessage = "is_user_data_store_in_crm_error_message"

//Login, Signup & Forgot Password
let kPhoneCode = "phone_code"
let kPhoneNumber = "phone_number"
let kPassword = "password"
let kEmail = "email"
let kFullName = "full_name"
let kDeviceType = "devices_type"
let kDeviceTokenAPI = "devices_token"
let kDeviceName = "devices_name"
let kAppVersion = "app_version"
let kDeviceId = "devices_id"
let kLoginType = "login_type"

let kAppUserId = "appuser_id"
let kImage = "image"
let kTransparentImage = "transparent_image"
let kHalfTransparentImage = "half_transparent_image"
let kMapImage = "map_image"
let kThumbnailImage = "thumbnail_image"
let kJobTitle = "job_title"
let kCompanyName = "company_name"
let kLocation = "location"
let kLatitude = "latitude"
let kLognitude = "longitude"
let kUserType = "user_type"
let kPhoneVerify = "phone_verify"
let kGender = "gender"
let kNotification = "notification"
let kCity = "city"
let kCityName = "city_name"
let kCityId = "city_id"
let kOTP = "otp"
let kUserLuckynumber = "user_lucky_number"
let kFavourites = "favourites"
let kCountryName = "country_name"
let kStateName = "state_name"
let kCitiesName = "cities_name"
let kMatchNotificationRemainder = "match_notification_remainder"
let kTShirtName = "tshirt_name"

//Login, Signup & Forgot Password, Change Password, Send Feedabck
let kAuthKey = "auth_key"
let kPoints = "points"
let kTitle = "title"
let kSubtitle = "subtitle"
let kQuote = "quote"
let kDescription = "description"
let kMobileDescription = "mobile_description"
let kSetting = "setting"
let kMatches = "matches"
let kCentury = "century"
let kHalfCentury = "half_century"
let kWickets = "wickets"
let kTrophies = "trophies"
let kCreatedAt = "created_at"
let kUrl = "url"

//Home Screen
let kMatch_Id = "matche_id"
let kFirstImage = "first_image"
let kFirstTeamName = "first_name"
let kFirstTeamShortName = "first_short_name"
let kSecondImage = "second_image"
let kSecondTeamName = "second_name"
let kSecondTeamShortName = "second_short_name"
let kLongitude = "longitude"
let kDateTime = "date_time"
let kHomeGround = "home_ground"
let kVideo = "video"
let kUniqueId = "unique_id"
let kIsWinner = "is_winner"
let kResult = "result"
let kResultId = "result_id"
let kIsIpl = "is_ipl"
let kSeriesId = "seriesid"
let kVideos = "videos"
let kSeriesType = "series_type"

//PointTable Screen
let kName = "name"
let kMatch = "matche"
let kNRR = "nrr"
let kPTS = "pts"

//Player Screen
let kPlayers = "players"
let kPlayerId = "players_id"
let kFirstName = "first_name"
let kLastName = "last_name"
let kCountry = "country"
let kCountryImage = "country_image"
let kPlayersNumber = "players_number"
let kFacebookUrl = "facebook_url"
let kTwitterUrl = "twitter_url"
let kInstagramUrl = "instagram_url"
let kRole = "role"
let kBirthDate = "birth_date"
let kHighestScore = "highest_score"
let kStrikeRate = "strike_rate"
let kEconomy = "economy"
let kTotalRuns = "total_runs"
let kIsCaptain = "is_captain"

//Chat Screen
let kMessageId = "message_id"
let kMessageType = "message_type"
let kMedia = "media"
let kUserImage = "user_image"
let kUserName = "user_name"
let kImageUrl = "imageurl"
let kTime = "time"
let kMediaDuration = "media_duration"
let kReplyMessageId = "reply_message_id"
let kReplyThread = "reply_thread"

//FAQ Screen
let kQuestion = "question"
let kAnswer = "answer"

//Shop
let kProductMasterId = "product_master_id"
let kProductCategoryId = "product_category_id"
let kShortDescription = "short_description"
let kDeliveryOption = "delivery_option"
let kAmount = "amount"
let kPrice = "price"
let kIsCart = "is_cart"
let kColors = "colors"
let kColor = "color"
let kProductColorMasterId = "product_colors_master_id"
let kColorName = "color_name"
let kColorCode = "color_code"
let kSize = "size"
let kTechnology = "technology"
let kSelectSize = "select_size"
let kProductSizeMasterId = "product_size_master_id"
let kCategory = "category"
let kImages = "images"
let kQuantity = "quantity"
let kUserCartId = "user_cart_id"

//Address
let kUserAddressId = "user_address_id"
let kContact = "contact"
let kPincode = "pincode"
let kState = "state"
let kAddress1 = "address1"
let kAddress2 = "address2"

//Live Score
let kMatchId = "matchid"
let kMatchDetail = "MatchDetail"
let kCurrentInningsId = "Current_Innings_Id"
let kInnings = "Innings"
let kBatsmen = "Batsmen"
let kBatsman = "Batsman"
let kBowlers = "Bowlers"
let kBowler = "Bowler"
let kExtras = "Extras"
let kTotal = "Total"
let kTotalExtras = "Total_extras"
let kWicketsC = "Wickets"
let kOversC = "Overs"
let kNameC = "Name"
let kHowOut = "How_out"
let kRunsCcored = "Runs_scored"
let kBallsFaced = "Balls_faced"
let kFoursScored = "Fours_scored"
let kSixesScored = "Sixes_scored"
let kStrikeRate1 = "StrikeRate"
let kOversBowled = "Overs_bowled"
let kBallsBowled = "Balls_bowled"
let kOnStrike = "On_strike"
let kNonStrike = "Non_strike"
let kBattingTeamName = "Batting_team_name"
let kBattingTeamShortName = "Batting_team_short_name"

let kMaidensBowled = "Maidens_bowled"
let kRunsConceded = "Runs_conceded"
let kWicketsTaken = "Wickets_taken"
let kEconomy1 = "Economy"

//Live Commentry
let kBallbyball = "ballbyball"
let kltball = "ltball"
let kIsWicket = "iswicket"
let kRuns = "runs"
let kOvers = "overs"
let kComment = "comment"

//Stadium
let kAddress = "address"
let kOpened = "opened"
let kCapacity = "capacity"
let kOwner = "owner"
let kHeight = "height"
let kAudioFile = "audio_file"

//Prediction
let kPredictionCategory = "prediction_category"
let kPredictionCategoryId = "prediction_category_id"
let kPredictionQuestion = "prediction_question"
let kIsLock = "is_lock"
let kIsTimeout = "is_timeout"
let kPredictionQuestionId = "prediction_question_id"
let kQuestionAnswerId = "question_answer_id"
let kIsAnswer = "is_answer"
let kAnswerId = "answer_id"
let kIsSendPoint = "is_send_point"
let kUserAnswer = "user_answer"
let kPercentage = "percentage"
let kAnswerOption = "answer_option"
let kCount = "count"
let kNumberOfChoice = "number_of_choice"
let kIsChoice = "is_choice"
let kRank = "rank"
let kPricegameImage = "pricegame_image"
let kTotalPoints = "total_points"

//Super Royals
let kIsVideo = "is_video"
let kDisplayUrl = "display_url"
let kVideoUrl = "video_url"
let kBio = "bio"

//New Home Screen
let kSlug = "slug"
let kFiles = "files"
let kButtonTitle = "button_title"
let kRedirectScreenios = "redirectscreenios"
let kButtonRedirectios = "button_redirect_ios"
let kTabbarIndexios = "tabbar_index_ios"

//Aauction Player
let kTotalAmount = "total_amout"
let kTotalPlayer = "total_player"
let kOverseasPlayerLimit = "overseas_player_limit"
let kPlayerType = "player_type"
let kRols = "rols"
let kPlayersName = "players_name"
let kNationality = "nationality"
let kPlayerRolsImage = "player_rols_image"
let kIsRightAnswer = "is_right_answer"
let kAuctioPlayerId = "auction_player_id"

//RRQuiz
let kRedirect_screen_ios = "redirect_screen_ios"
