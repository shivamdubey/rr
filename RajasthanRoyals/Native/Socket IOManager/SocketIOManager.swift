//
//  SocketIOManager.swift
//  Poker
//
//  Created by Priya on 25/11/18.
//  Copyright © 2016 Ios. All rights reserved.
//

import UIKit
import SocketIO
import KRProgressHUD

enum EVENTNAME : String {
    case GETMESSAGE     = "get message"
    case MESSAGEDELETED = "message deleted"
}

class SocketIOManager: NSObject {
    
    static let sharedInstance = SocketIOManager()
    let manager = SocketManager(socketURL: URL(string: SOCKET_URL)!, config: [.log(true), .compress])
    let socket: SocketIOClient
    
    override init() {
        socket = manager.defaultSocket
        super.init()
    }
    
    //Socket connection
    func establishConnection(completionHandler: @escaping (_ string : String?) -> Void) {
        print("call establishConnection...!")
        
        socket.connect(timeoutAfter: 10) {
            print("Failed to connect")
        }

        socket.on(clientEvent: .connect) {data, ack in
            print("socket connection done")
            completionHandler("socket connection done")

            //Join
            print(BaseViewController.sharedInstance.appDelegate.authKey)
            self.socket.emitWithAck(APINAME.SOCKET_ADDUSER.rawValue, BaseViewController.sharedInstance.appDelegate.authKey).timingOut(after: 15, callback: { data in
                print("Response: \(data)")
            })

            if !BaseViewController.sharedInstance.appDelegate.isLoadHomeScreen {
                BaseViewController.sharedInstance.appDelegate.isLoadHomeScreen = true
                self.listenerForOtherMessages()
            }
        }
    }

    //Close connection
    func closeConnection() {
        socket.disconnect()
    }
    
    //Received messages
    func listenerForOtherMessages() {
        //Handle received messages
        socket.onAny { (socketAck) in
            print(socketAck)

            if socketAck.event == EVENTNAME.GETMESSAGE.rawValue {
                if let items = socketAck.items {
                    if items.count > 0 {
                        if let responseString = items[0] as? String {
                            let response = responseString.convertToDictionary()
                            NotificationCenter.default.post(name: NSNotification.Name("NewMessage"), object: nil, userInfo: response)
                        }
                    }
                }
            }
            else if socketAck.event == EVENTNAME.MESSAGEDELETED.rawValue {
                if let items = socketAck.items {
                    if items.count > 0 {
                        if let responseString = items[0] as? String {
                            if let removeMessageId = Int(responseString) {
                                let response = [kMessageId: removeMessageId]
                                NotificationCenter.default.post(name: NSNotification.Name("DeleteMessage"), object: nil, userInfo: response)
                            }
                        }
                    }
                }
            }
        }

        socket.on("message") { (dataArray, socketAck) -> Void in
            print("new message received with data: \(dataArray)")
        }
    }
    
    //API Calls
    func socketApiCall(isShowHud: Bool, methodName: String, paramJsonString: NSString, _ completionHandler:
        @escaping (_ response: [String: AnyObject]) -> Void) {
        print(baseURLWithAuth + methodName)
        print(paramJsonString)

        if isShowHud {
            KRProgressHUD.show()
        }

        socket.emitWithAck(methodName, paramJsonString).timingOut(after: 15, callback: { data in
            print("Response: \(data)")

            if isShowHud {
                KRProgressHUD.dismiss()
            }

            if data.count > 0 {
                if let responseString = data[0] as? String {
                    print(responseString)
                    if let responseData = responseString.convertToDictionary() {
                        completionHandler(responseData)
                    }
                    else {
                        completionHandler(["message" : "Error while converting JSON string to Dictionary" as AnyObject])
                    }
                }
                else {
                    completionHandler(["message" : "\(ErrorMessage)" as AnyObject])
                }
            }
            else {
                completionHandler(["message" : "\(ErrorMessage)" as AnyObject])
            }
        })
    }
}
